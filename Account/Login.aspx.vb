﻿Imports APPCore.Utility
Imports DALC4NET
Imports APS
Imports System.Data
Imports System.Reflection.Emit

Partial Public Class Account_Login
    Inherits Page

    Private m_CryptoDecrypto As New CryptDecrypt
    Private m_SessionData As MU_SessionData = Nothing

    Protected Sub ValidateUser()
        Dim oDTUtente As DataTable
        Dim sPassword As String = ""
        Dim oUtente As Utente = Nothing
        Dim oCypher As New CryptDecrypt

        Dim sSQL As String

        Try
            ErrorMessage.Visible = False
            If Page.IsValid Then
                sPassword = m_CryptoDecrypto.Encrypt(txtPassword.Text.Trim)

                sSQL = "SELECT * FROM BGOL_VPLAYERS P " _
                     & "WHERE P.Email = '" & txtUserName.Text.Trim & "' " _
                     & "AND P.PASSWORD = '" & sPassword & "' "
                oDTUtente = g_DAL.ExecuteDataTable(sSQL)

                If oDTUtente.Rows.Count = 1 Then
                    For Each oRow As DataRow In oDTUtente.Rows
                        If Nb(oRow("ATTIVO")) Then
                            '
                            ' Inizio sessione autenticata
                            ' Invio cookie per memorizzare lo UserName dell'utente appena collegato... è comodo!
                            '            
                            Dim oLoginCookie As New HttpCookie(m_Cookie)
                            If chkRememberMe.Checked Then
                                Response.Cookies.Remove(m_Cookie)
                                Response.Cookies.Add(oLoginCookie)
                                oLoginCookie.Values.Add("UserName", txtUserName.Text)
                                oLoginCookie.Values.Add("Password", m_CryptoDecrypto.Encrypt(txtPassword.Text.Trim))
                                Dim dtExpiry As DateTime = DateTime.Now.AddDays(60)
                                Response.Cookies(m_Cookie).Expires = dtExpiry
                            Else
                                ' se non è settato il "ricordami" allora faccio scadere il cookie un secondo dopo l'autenticazione
                                Response.Cookies.Remove(m_Cookie)
                                Response.Cookies.Add(oLoginCookie)
                                oLoginCookie.Values.Add("UserName", txtUserName.Text)
                                oLoginCookie.Values.Add("Password", m_CryptoDecrypto.Encrypt(txtPassword.Text.Trim))
                                Dim dtExpiry As DateTime = DateTime.Now.AddSeconds(1)
                                Response.Cookies(m_Cookie).Expires = dtExpiry
                            End If

                            With oUtente
                                .ID = Nni(oRow("IDPLAYER"))
                                .UserName = Nz(oRow("USERNAME"))
                                .Nome = Nz(oRow("NOME")).Trim
                                .Cognome = Nz(oRow("COGNOME")).Trim
                                .Email = Nz(oRow("EMAIL")).Trim
                                .SuperUser = Nb(oRow("SuperUser"))
                                .Ruolo.IDRuolo = Nz(oRow("IDRole"))
                                .Ruolo.Ruolo = Nz(oRow("Role"))
                                .Games.IDGame = Nni(oRow("IDGame"))
                                .IDTeam = GetIDTeam(Nni(oRow("IDGame")), Nni(oRow("IDPLAYER")))
                            End With
                            m_SessionData.Utente = oUtente

                            Session("SessionData") = m_SessionData
                            Session("IDGame") = m_SessionData.Utente.Games.IDGame
                            Session("IDPlayer") = Nni(oRow("IDPLAYER"))
                            Session("IDTeam") = GetIDTeam(Session("IDGame"), Session("IDPlayer"))
                            Session("IDPeriodValid") = HandleGetMaxPeriodValid(Nni(Session("IDGame")))
                            Session("LanguageDefault") = Nz(oRow("DefaultLanguage"))
                            Session("LanguageActive") = Nz(oRow("DefaultLanguage"), g_DefaultLanguage)
                            Session("IDRole") = Nz(oRow("IDRole"))

                            sSQL = "SELECT TOP 1 DBV.Value " _
                                 & "FROM Decisions_Developer D " _
                                 & "LEFT JOIN Decisions_Developer_Value DBV ON D.Id = DBV.IDDecisionDeveloper " _
                                 & "WHERE D.VariableName = 'PeriodLength' AND IDGame = " & Session("IDGame")
                            Session("LunghezzaPeriodo") = Nni(g_DAL.ExecuteScalar(sSQL))

                            g_DataTable_VariableType = HandleLoadComboBox_Variables_DataType()

                            SQLConnClose(g_DAL.GetConnObject)

                            ' E' andato tutto bene, procedo con l'allineamento delle variabili di stato
                            ' Allineamento delle variabili di stato
                            ' Controllo se le variabili di stato sono già state settate correttamente
                            Dim bSettingVariableState As Boolean
                            sSQL = "SELECT SettingVariableState FROM Configuration "
                            bSettingVariableState = Nb(g_DAL.ExecuteScalar(sSQL))
                            If Not bSettingVariableState Then
                                ' Ripulisco eventualmente le tabelle
                                sSQL = "TRUNCATE TABLE Variables_State"
                                g_DAL.ExecuteNonQuery(sSQL)

                                sSQL = "TRUNCATE TABLE Variables_State_Value"
                                g_DAL.ExecuteNonQuery(sSQL)

                                sSQL = "TRUNCATE TABLE Variables_State_Value_List"
                                g_DAL.ExecuteNonQuery(sSQL)

                                Dim oEngine As New Engine_Arkea(Nni(Session("IDGame")), 1, True, 0)
                                bSettingVariableState = oEngine.VariableStateTable_Align(Nni(Session("IDGame")))
                                oEngine = Nothing

                                If bSettingVariableState Then
                                    sSQL = "UPDATE Configuration SET SettingVariableState = 1 "
                                    g_DAL.ExecuteNonQuery(sSQL)
                                End If

                            End If

                            ' Controllo se devo anche inserire le variabili per il controllo e decisioni del boss
                            Dim bSettingVariableBoss As Boolean
                            sSQL = "SELECT SettingVariableBoss FROM Configuration "
                            bSettingVariableBoss = Nb(g_DAL.ExecuteScalar(sSQL))
                            If Not bSettingVariableBoss Then
                                ' Ripulisco eventualmente le tabelle
                                sSQL = "TRUNCATE TABLE Decisions_Boss_Title"
                                g_DAL.ExecuteNonQuery(sSQL)

                                sSQL = "TRUNCATE TABLE Decisions_Boss_SubTitle"
                                g_DAL.ExecuteNonQuery(sSQL)

                                sSQL = "TRUNCATE TABLE Decisions_Boss"
                                g_DAL.ExecuteNonQuery(sSQL)

                                sSQL = "TRUNCATE TABLE Decisions_Boss_Value"
                                g_DAL.ExecuteNonQuery(sSQL)

                                sSQL = "TRUNCATE TABLE Decisions_Boss_Value_List"
                                g_DAL.ExecuteNonQuery(sSQL)

                                Dim oEngine As New Engine_Arkea(Nni(Session("IDGame")), 1, True, 0)
                                bSettingVariableBoss = oEngine.VariableBossTable_Align(Nni(Session("IDGame")))
                                oEngine = Nothing

                                If bSettingVariableBoss Then
                                    sSQL = "UPDATE Configuration SET SettingVariableBoss = 1 "
                                    g_DAL.ExecuteNonQuery(sSQL)
                                End If

                            End If

                            ' Controllo se le variabili di gioco per il player che si sta loggando sono inserite, altrimenti provvedo all'inserimento
                            Dim bSettingVariablePlayer As Boolean
                            sSQL = "SELECT SettingVariables FROM BGOL_Players_Games WHERE IDTeam = " & Nni(Session("IDTeam")) & " AND IDGame = " & Nni(Session("IDGame"))
                            bSettingVariablePlayer = Nb(g_DAL.ExecuteScalar(sSQL))
                            If Not bSettingVariablePlayer Then

                                sSQL = "DELETE FROM Decisions_Players_Value WHERE IDPlayer = " & Nni(Session("IDTeam"))
                                g_DAL.ExecuteNonQuery(sSQL)

                                sSQL = "DELETE FROM Decisions_Players_Value_List WHERE IDPlayer = " & Nni(Session("IDTeam"))
                                g_DAL.ExecuteNonQuery(sSQL)

                                Dim oEngine As New Engine_Arkea(Nni(Session("IDGame")), 1, True, 0)
                                bSettingVariablePlayer = oEngine.VariablePlayersTable_Align(Nni(Session("IDGame")), Nni(Session("IDTeam")))
                                oEngine = Nothing

                                If bSettingVariablePlayer Then
                                    sSQL = "UPDATE BGOL_Players_Games SET SettingVariables = 1 WHERE IDTeam = " & Nni(Session("IDTeam")) & " AND IDGame = " & Nni(Session("IDGame"))
                                    g_DAL.ExecuteNonQuery(sSQL)
                                End If
                            End If
                            ' Cancello eventuali dati sporchi scritti a cavolo nella tabella.
                            sSQL = "DELETE FROM Decisions_Players WHERE ISNULL(IDGame, 0) = 0 "
                            g_DAL.ExecuteNonQuery(sSQL)

                            ' Controllo se le variabili di gioco per il designer/developer sono state settate
                            Dim bSettingVariableDeveloper As Boolean
                            sSQL = "SELECT SettingVariableDeveloper FROM Configuration "
                            bSettingVariableDeveloper = Nb(g_DAL.ExecuteScalar(sSQL))
                            If Not bSettingVariableDeveloper Then
                                sSQL = "DELETE FROM Decisions_Developer_Value WHERE IDPlayer = " & Nni(Session("IDTeam"))
                                g_DAL.ExecuteNonQuery(sSQL)

                                sSQL = "DELETE FROM Decisions_Developer_Value_List WHERE IDPlayer = " & Nni(Session("IDTeam"))
                                g_DAL.ExecuteNonQuery(sSQL)

                                Dim oEngine As New Engine_Arkea(Nni(Session("IDGame")), 1, True, 0)
                                bSettingVariablePlayer = oEngine.VariableDeveloperTable_Align(Nni(Session("IDGame")))
                                oEngine = Nothing

                                If bSettingVariablePlayer Then
                                    sSQL = "UPDATE Configuration SET SettingVariableDeveloper = 1 "
                                    g_DAL.ExecuteNonQuery(sSQL)
                                End If
                            End If

                            ' Carico tutte le informazioni legate ai game del player appena collegato
                            PageRedirect("~/Default.aspx", "", "")
                        Else

                            FailureText.Text = "Your account are expired."
                            ErrorMessage.Visible = True
                        End If
                    Next

                ElseIf oDTUtente.Rows.Count > 1 Then
                    Dim oLoginCookie As New HttpCookie(m_Cookie)
                    If chkRememberMe.Checked Then
                        Response.Cookies.Remove(m_Cookie)
                        Response.Cookies.Add(oLoginCookie)
                        oLoginCookie.Values.Add("UserName", txtUserName.Text)
                        oLoginCookie.Values.Add("Password", m_CryptoDecrypto.Encrypt(txtPassword.Text.Trim))
                        Dim dtExpiry As DateTime = DateTime.Now.AddDays(60)
                        Response.Cookies(m_Cookie).Expires = dtExpiry
                    Else
                        ' se non è settato il "ricordami" allora faccio scadere il cookie un secondo dopo l'autenticazione
                        Response.Cookies.Remove(m_Cookie)
                        Response.Cookies.Add(oLoginCookie)
                        oLoginCookie.Values.Add("UserName", txtUserName.Text)
                        oLoginCookie.Values.Add("Password", m_CryptoDecrypto.Encrypt(txtPassword.Text.Trim))
                        Dim dtExpiry As DateTime = DateTime.Now.AddSeconds(1)
                        Response.Cookies(m_Cookie).Expires = dtExpiry
                    End If

                    With oUtente
                        .ID = Nni(oDTUtente.Rows(0)("IDPLAYER"))
                        .UserName = Nz(oDTUtente.Rows(0)("USERNAME"))
                        .Nome = Nz(oDTUtente.Rows(0)("NOME")).Trim
                        .Cognome = Nz(oDTUtente.Rows(0)("COGNOME")).Trim
                        .Email = Nz(oDTUtente.Rows(0)("EMAIL")).Trim
                        .SuperUser = Nb(oDTUtente.Rows(0)("SuperUser"))
                        .Ruolo.IDRuolo = Nz(oDTUtente.Rows(0)("IDRole"))
                        .Ruolo.Ruolo = Nz(oDTUtente.Rows(0)("Role"))
                        .Games.IDGame = Nni(oDTUtente.Rows(0)("IDGame"))
                    End With
                    m_SessionData.Utente = oUtente

                    Session("SessionData") = m_SessionData
                    Session("IDGame") = m_SessionData.Utente.Games.IDGame
                    Session("IDPlayer") = Nni(oDTUtente.Rows(0)("IDPLAYER"))
                    Session("IDTeam") = GetIDTeam(Session("IDGame"), Session("IDPlayer"))
                    Session("IDPeriodValid") = HandleGetMaxPeriodValid(Nni(Session("IDGame")))
                    Session("LanguageDefault") = Nz(oDTUtente.Rows(0)("DefaultLanguage"))
                    Session("LanguageActive") = Nz(oDTUtente.Rows(0)("DefaultLanguage"), g_DefaultLanguage)
                    Session("IDRole") = Nz(oDTUtente.Rows(0)("IDRole"))

                    ' Sei iscritto a più di un game, ti rimando alla pagina di selezione del game a cui giocare
                    PageRedirect("~/Player_Game_Select.aspx", "", "")

                Else
                    If Not g_DAL Is Nothing Then SQLConnClose(g_DAL.GetConnObject)

                    FailureText.Text = "Username and/or password is incorrect"
                    ErrorMessage.Visible = True
                End If
            End If

            SQLConnClose(g_DAL.GetConnObject)
        Catch ex As Exception
            If Not g_DAL Is Nothing Then SQLConnClose(g_DAL.GetConnObject)
            If ex.Message <> "Thread interrotto." Then
                Throw New ApplicationException("Login.aspx -> ValidateUser", ex)
            End If
        End Try
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Session("PageOrigin") = Me
        Me.Page.Master.FindControl("divSideBar").Visible = False
        Dim oSiteMaster As SiteMaster
        oSiteMaster = Me.Page.Master
        oSiteMaster.LoadMenuSidebarRight(False)
        oSiteMaster.SetImageInvisible()

        g_DAL = New DBHelper
    End Sub

    Protected Sub LogIn(sender As Object, e As EventArgs)
        If IsValid Then
            ValidateUser()
        End If
    End Sub

    'Private Sub btnTranslate_Click(sender As Object, e As EventArgs) Handles btnTranslate.Click
    '    Session("PageOrigin") = Me.ToString().Substring(4, Me.ToString().Substring(4).Length - 5) + ".aspx"
    '    Response.Redirect("PageTranslation.aspx")
    '    Response.End()
    'End Sub

End Class

