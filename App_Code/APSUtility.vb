﻿
Imports System.Web.Configuration
Imports System.Security.Cryptography
Imports System.IO
Imports DALC4NET
Imports System.Data
Imports APPCore.Utility
Imports System.Data.SqlClient
Imports Telerik.Web.UI

Namespace APS
    Public Module APSUtility

        Public Structure MU_SessionData
            Public Utente As Utente
            Public Game As Game
        End Structure

        Public Structure Utente
            Public ID As Integer
            Public UserName As String
            Public Nome As String
            Public Cognome As String
            Public Attivo As Boolean
            Public SuperUser As Boolean
            Public Email As String
            Public Games As Game
            Public Ruolo As Roles
            Public IDTeam As Integer
        End Structure

        Public Structure Roles
            Public IDRuolo As String
            Public Ruolo As String
        End Structure

        Public Structure Game
            Public IDGame As Integer
            Public IDModello As Integer
            Public DescrizioneGame As String
            Public ConnectionStringGame As String
        End Structure

        Public g_ConnString As String = WebConfigurationManager.ConnectionStrings("ConnectString").ToString
        Public g_ConnStringBgol As String = WebConfigurationManager.ConnectionStrings("ConnectStringBGol").ToString
        Public g_DefaultLanguage As String = WebConfigurationManager.AppSettings("DefaultLanguage").ToString

        Public g_DAL As New DALC4NET.DBHelper

        ' Public Const g_ErrorPath As String = "~/Error.aspx"

        Private m_sGridMessage As String = Nothing

        Public m_Cookie As String = "ManagementUtilities"

        ' Varibili di sessione
        Public g_DataTable_VariableType As DataTable

        Public g_MessaggioErrore As String

        Public Function IsPossibleToSaveDataPeriod(IDGame As Integer, IDPeriod As Integer) As Boolean
            Dim sSQL As String = "SELECT DateHourEndEffective FROM BGOL_Periods WHERE IDGame = " & IDGame & " AND ID = " & IDPeriod
            Dim sDataOraFinePeriodo As String = Nz(g_DAL.ExecuteScalar(sSQL))
            If sDataOraFinePeriodo <> "" Then
                ' eseguo la differenza tra adesso e l'ora di fine periodo
                If DateDiff(DateInterval.Minute, Now, ANSIToDate(sDataOraFinePeriodo, True)) < 0 Then
                    Return False
                Else
                    Return True
                End If
            Else
                Return True
            End If

        End Function

        Public Sub PageRedirect(ByVal url As String, ByVal target As String, ByVal windowFeatures As String)
            Dim context As HttpContext = HttpContext.Current

            If ([String].IsNullOrEmpty(target) OrElse target.Equals("_self", StringComparison.OrdinalIgnoreCase)) AndAlso [String].IsNullOrEmpty(windowFeatures) Then
                context.Response.Redirect(url)
            Else
                Dim page As Page = DirectCast(context.Handler, Page)
                If page Is Nothing Then
                    Throw New InvalidOperationException("You can not pipe the selected page, the page is empty or not present")
                End If
                url = page.ResolveClientUrl(url)
                Dim script As String
                If Not [String].IsNullOrEmpty(windowFeatures) Then
                    script = "window.open(""{0}"", ""{1}"", ""{2}"");"
                Else
                    script = "window.open(""{0}"", ""{1}"");"
                End If
                script = [String].Format(script, url, target, windowFeatures)
                ScriptManager.RegisterStartupScript(page, GetType(Page), "Redirect", script, True)
            End If
        End Sub

        Public Class CryptDecrypt

            Public Function Encrypt(TextToEncrypt As String) As String
                Dim sEncryptionKey As String = "APSOLTIONS06091979"
                Dim bClearByte As Byte() = Encoding.Unicode.GetBytes(TextToEncrypt)
                Dim sRet As String

                Using encryptor As Aes = Aes.Create()
                    Dim oPDB As New Rfc2898DeriveBytes(sEncryptionKey, New Byte() {&H49, &H76, &H61, &H6E, &H20, &H4D, &H65, &H64, &H76, &H65, &H64, &H65, &H76})
                    encryptor.Key = oPDB.GetBytes(32)
                    encryptor.IV = oPDB.GetBytes(16)
                    Using ms As New MemoryStream()
                        Using cs As New CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write)
                            cs.Write(bClearByte, 0, bClearByte.Length)
                            cs.Close()
                            cs.Dispose()
                        End Using
                        sRet = Convert.ToBase64String(ms.ToArray())
                    End Using
                End Using
                Return sRet
            End Function

            Public Function Decrypt(TextToDecrypt As String) As String
                Dim EncryptionKey As String = "APSOLTIONS06091979"
                Dim cipherBytes As Byte() = Convert.FromBase64String(TextToDecrypt)
                Dim sRet As String

                Using encryptor As Aes = Aes.Create()
                    Dim pdb As New Rfc2898DeriveBytes(EncryptionKey, New Byte() {&H49, &H76, &H61, &H6E, &H20, &H4D, &H65, &H64, &H76, &H65, &H64, &H65, &H76})
                    encryptor.Key = pdb.GetBytes(32)
                    encryptor.IV = pdb.GetBytes(16)
                    Using ms As New MemoryStream()
                        Using cs As New CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write)
                            cs.Write(cipherBytes, 0, cipherBytes.Length)
                            cs.Close()
                            cs.Dispose()
                        End Using
                        sRet = Encoding.Unicode.GetString(ms.ToArray())
                    End Using
                End Using
                Return sRet
            End Function

        End Class

        Public Function NomePagina(NomeFile() As String) As String
            Dim oRet As String
            oRet = NomeFile(NomeFile.Length - 1)
            Return oRet
        End Function

        Public Function PageOnError(ex As Exception) As String
            Dim sErrorInfo As String = "<br>Source: " & ex.Source &
                                     "<br><br>Message: " & ex.Message &
                                     "<br><br>Stack trace: " & ex.StackTrace
            Return sErrorInfo
        End Function

        Public Sub OpenPageSessionTimeOut()
            PageRedirect("SessionTimeOut.aspx", "_self", "")
        End Sub

        ''' <summary>
        ''' Recupera tutti i periodi attivi per il game selezionato
        ''' </summary>
        ''' <param name="IDGame"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function LoadPeriodGame(IDGame As Integer, Optional AllPeriods As Boolean = False) As DataTable
            Dim sSQL As String

            Try
                If AllPeriods = False Then
                    sSQL = "SELECT P.Id, P.Descrizione, P.DateStart, P.DateEnd, GP.Simulation " _
                         & "FROM BGOL_Games_Periods GP " _
                         & "INNER JOIN BGOL_Periods P ON GP.IDPeriodo = P.Id " _
                         & "WHERE GP.IDGame = " & IDGame & " "

                Else
                    sSQL = "SELECT P.Id, P.Descrizione, P.DateStart, P.DateEnd, DateHourEndEffective, " _
                         & "'' AS DateStartFormatted, '' AS DateEndFormatted, '' AS DateHourEndEffectiveFormatted, 0 AS Simulation " _
                         & "FROM BGOL_Periods P " _
                         & "WHERE P.IDGame = " & IDGame & " "

                End If
                sSQL &= "ORDER BY P.NumStep DESC "

                Return g_DAL.ExecuteDataTable(sSQL)
            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            End Try
        End Function

        Public Function LoadPeriodGameValid(IDGame As Integer, CurrentStep As Integer) As DataTable
            Dim sSQL As String

            Try
                sSQL = "SELECT P.Id, P.Descrizione, P.DateStart, P.DateEnd " _
                     & "FROM BGOL_Games_Periods GP " _
                     & "INNER JOIN BGOL_Periods P ON GP.IDPeriodo = P.Id " _
                     & "WHERE GP.IDGame = " & IDGame & " AND NumStep <= " & CurrentStep - 1 & " " _
                     & "ORDER BY P.Numstep DESC "

                Return g_DAL.ExecuteDataTable(sSQL)
            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            End Try
        End Function

        ''' <summary>
        ''' Recupera tutti i teams attivi per il game selezionato
        ''' </summary>
        ''' <param name="IDGame"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Function LoadTeamsGame(IDGame As Integer) As DataTable
            Dim sSQL As String

            Try
                sSQL = "SELECT DISTINCT T.ID, T.TeamName, Color " _
                     & "FROM BGOL_Teams T " _
                     & "INNER JOIN BGOL_Players_Teams_Games PLT ON T.ID = PLT.IdTeam " _
                     & "INNER JOIN BGOL_Players_Games PG ON T.Id = PG.IDTeam AND PG.IDGame = PLT.IdGame " _
                     & "WHERE PLT.IDGame = " & IDGame & " " _
                     & "AND PG.IDRole = 'P' " _
                     & "ORDER BY T.TeamName ASC "

                ' & "AND T.TeamName NOT LIKE '%M&U%' " _

                Return g_DAL.ExecuteDataTable(sSQL)
            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            End Try
        End Function

        Public Function HandleLoadPlayersGame(IDGame As Integer) As DataTable
            Dim sSQL As String

            Try
                sSQL = "SELECT P.ID, P.Nome, P.Cognome, P.Username, P.Email " _
                     & "FROM BGOL_Players_Games PG  " _
                     & "INNER JOIN BGOL_Players P ON PG.IDPlayer = P.ID " _
                     & "WHERE PG.IDGame = " & IDGame & " "

                Return g_DAL.ExecuteDataTable(sSQL)
            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            End Try
        End Function

        ''' <summary>
        ''' Caricamento dei players partecipanti al game, soltanto i team, senza nessun dato legato al singolo player che lo compone
        ''' </summary>
        Public Function LoadPlayersIDTeam(IDGame As Integer) As DataTable
            Dim sSQL As String

            sSQL = "SELECT DISTINCT T.TeamName, T.ID AS IDTeam " _
                 & "FROM BGOL_PLAYERS P " _
                 & "INNER JOIN BGOL_Players_Games PG On P.ID = PG.IDPlayer " _
                 & "LEFT JOIN BGOL_Players_Teams_Games PTG On P.ID = PTG.IdPlayer And PG.IDGame = PTG.IdGame " _
                 & "LEFT JOIN BGOL_Teams T On PTG.IdTeam = T.Id " _
                 & "WHERE PG.IDGame = " & IDGame

            Return g_DAL.ExecuteDataTable(sSQL)
        End Function

        Public Function GetIDTeam(IdGame As Integer, IDPlayer As Integer) As Integer
            Dim iRet As Integer
            Dim sSQL As String = "SELECT IDTeam FROM BGOL_Players_Teams_Games WHERE IDGame = " & IdGame & " AND IDPlayer = " & IDPlayer
            Dim oDAL As New DBHelper

            Try
                iRet = oDAL.ExecuteScalar(sSQL)

                SQLConnClose(oDAL.GetConnObject)
                oDAL = Nothing

                Return iRet
            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            End Try
        End Function

        Public Function GetTeamName(IDTeam As Integer) As String
            Dim sRet As String
            Dim sSQL As String = "SELECT TeamName FROM BGOL_Teams WHERE ID = " & IDTeam
            Dim oDAL As New DBHelper

            Try
                sRet = oDAL.ExecuteScalar(sSQL)
                SQLConnClose(oDAL.GetConnObject)
                oDAL = Nothing

                Return sRet

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            End Try
        End Function

        Public Function GetTeamName(IDGame As Integer, IDPlayer As Integer) As String
            Dim sRet As String
            Dim sSQL As String

            Try

                sSQL = "SELECT T.TeamName FROM BGOL_Players_Teams_Games PTG " _
                     & "INNER JOIN BGOL_Teams T ON PTG.IdTeam = T.Id " _
                     & "WHERE PTG.IdGame = " & IDGame & " AND PTG.IDTeam = " & IDPlayer

                sRet = g_DAL.ExecuteScalar(sSQL)

                Return sRet

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            End Try
        End Function

        Public Function LoadItemsGame(IDGame As Integer, LanguageActive As String) As DataTable
            Dim sSQL As String

            ' Carico la tabella delle intestazioni da usare
            sSQL = "SELECT * FROM vItems S WHERE S.IDGame = " & IDGame
            Dim oDTTemp As DataTable = g_DAL.ExecuteDataTable(sSQL)

            Dim iIDLanguage As Integer
            sSQL = "SELECT ID FROM Languages WHERE Code = '" & LanguageActive & "' "
            iIDLanguage = Nni(g_DAL.ExecuteScalar(sSQL))

            Dim drTranslations() As DataRow = oDTTemp.Select("IDLanguage = " & iIDLanguage)

            If drTranslations.Length > 0 Then ' ho trovato delle traduzioni, modifico la colonna VariableName con la descrizione trovata nelle traduzioni
                ' Cambio i nomi delle colonne, sostituisco quella translation con variablename, quella usata per il caricamento e l'utilizzo
                oDTTemp.Columns("VariableName").ColumnName = "VariableNameOriginal"
                oDTTemp.Columns("Translation").ColumnName = "VariableName"
            End If

            Return oDTTemp

        End Function

        Public Function HandleLoadAgeData(IDGame As Integer) As DataTable
            Dim sSQL As String

            ' Carico la tabella delle intestazioni da usare
            sSQL = "SELECT * FROM Age WHERE IDGame = " & IDGame

            Return g_DAL.ExecuteDataTable(sSQL)

        End Function

        Public Function HandleLoadVariableState(IDGame As Integer) As DataTable
            Dim sSQL As String

            sSQL = "SELECT * FROM Variables_State WHERE IDGame = " & IDGame

            Return g_DAL.ExecuteDataTable(sSQL)
        End Function

        Public Function HandleLoadVariableStateValue(IDVariableState As Integer) As DataTable
            Dim sSQL As String

            sSQL = "SELECT * FROM Variables_State_Value WHERE IDVariable = " & IDVariableState

            Return g_DAL.ExecuteDataTable(sSQL)
        End Function

        Public Function HandleLoadVariableStateValue(IDVariableState As Integer, IDPeriod As Integer, IDPlayer As Integer) As DataTable
            Dim sSQL As String

            sSQL = "SELECT * FROM Variables_State_Value WHERE IDVariable = " & IDVariableState & " AND ISNULL(IDPeriod, " & IDPeriod & ") = " & IDPeriod _
                 & " AND (ISNULL(IDPlayer, " & IDPlayer & ") = " & IDPlayer & " OR ISNULL(IDPlayer, 0) = 0) "

            Return g_DAL.ExecuteDataTable(sSQL)
        End Function

        Public Function HandleLoadVariableStateValueList(IDVariableStateValue As Integer, IDPeriod As Integer, IDPlayer As Integer) As DataTable
            Dim sSQL As String

            sSQL = "SELECT * FROM Variables_State_Value_List WHERE IDVariableState = " & IDVariableStateValue & " AND ISNULL(IDPeriod, " & IDPeriod & ") = " & IDPeriod _
                 & " AND ISNULL(IDPlayer, " & IDPlayer & ") = " & IDPlayer & " " _
                 & "ORDER BY IDItem, IDAge "

            Return g_DAL.ExecuteDataTable(sSQL)
        End Function

        Public Function HandleLoadDecisionsBoss(IDGame As Integer) As DataTable
            Dim sSQL As String

            sSQL = "SELECT * FROM vDecisions_Boss WHERE IDGame = " & IDGame

            Return g_DAL.ExecuteDataTable(sSQL)

        End Function

        Public Function HandleLoadDecisionsBossTitle(IDGame As Integer) As DataTable
            Dim sSQL As String

            sSQL = "SELECT * FROM Decisions_Boss_Title WHERE IDGame = " & IDGame

            Return g_DAL.ExecuteDataTable(sSQL)

        End Function

        Public Function HandleLoadDecisionsBossSubTitle(IDTitle As Integer) As DataTable
            Dim sSQL As String

            sSQL = "SELECT * FROM Decisions_Boss_SubTitle WHERE IDTitle = " & IDTitle

            Return g_DAL.ExecuteDataTable(sSQL)

        End Function

        Public Function HandleLoadDecisionsBoss(IDSubTitle As Integer, IDGame As Integer) As DataTable
            Dim sSQL As String

            sSQL = "SELECT * FROM Decisions_Boss WHERE IDSubTitle = " & IDSubTitle & " AND IDGame = " & IDGame & " ORDER BY IDSubTitle, Decision "

            Return g_DAL.ExecuteDataTable(sSQL)

        End Function

        Public Function HandleLoadDecisionsBossValue(IDDecisionBoss As Integer, IDPeriod As Integer) As DataTable
            Dim sSQL As String

            sSQL = "SELECT * FROM Decisions_Boss_Value WHERE IDDecisionBoss = " & IDDecisionBoss & " AND ISNULL(IDPeriod, " & IDPeriod & ") = " & IDPeriod

            Return g_DAL.ExecuteDataTable(sSQL)

        End Function

        Public Function HandleLoadDecisionsBossValueList(IDDecisionBossValue As Integer, IDPeriod As Integer, IDTeam As Integer, IDGame As Integer) As DataTable
            Dim sSQL As String
            Dim sWhereClause As String = "WHERE IDDecisionBossValue = " & IDDecisionBossValue & " AND IDPeriod = " & IDPeriod

            If IDTeam <> 0 Then
                Dim iContaRecord As Integer = 1

                sSQL = "SELECT TeamName FROM BGOL_Teams T " _
                     & "INNER JOIN BGOL_Players_Teams_Games BPTG ON T.Id = BPTG.IdTeam AND BPTG.IdGame = " & IDGame & " " _
                     & "WHERE T.Id <> " & IDTeam
                Dim oDTTeams As DataTable = g_DAL.ExecuteDataTable(sSQL)
                For Each drTeam As DataRow In oDTTeams.Rows

                    If iContaRecord = 1 Then ' Primo record
                        sWhereClause &= " AND (VariableName NOT LIKE ('%" & drTeam("TeamName") & "%') "

                    ElseIf iContaRecord = oDTTeams.Rows.Count Then '
                        sWhereClause &= " AND VariableName NOT LIKE ('%" & drTeam("TeamName") & "%'))"

                    Else
                        sWhereClause &= " AND VariableName NOT LIKE ('%" & drTeam("TeamName") & "%') "

                    End If

                    iContaRecord += 1
                Next

            End If
            sSQL = "SELECT * FROM Decisions_Boss_Value_List " & sWhereClause & "ORDER BY VariableName "

            Return g_DAL.ExecuteDataTable(sSQL)

        End Function

        Public Function LoadDeveloperDecisionsLabelGroup(IDGame As Integer) As DataTable
            Dim sSQL As String

            sSQL = "SELECT DISTINCT ISNULL(VariableLabelGroup, 'Decisions developer') AS VariableLabelGroup FROM Decisions_Developer WHERE IDGame = " & IDGame
            Return g_DAL.ExecuteDataTable(sSQL)
        End Function


        ''' <summary>
        ''' Restituisce un datatable che contiene la lista delle possibili decisioni che devono prendere i singoli player
        ''' </summary>
        ''' <param name="IDGame"></param>
        ''' <returns></returns>
        Public Function HandleLoadGameDecisionPlayer(IDGame As Integer, IDCategory As eVariablesCateogry) As DataTable
            Dim sSQL As String

            ' Carico la tabella delle intestazioni da usare
            sSQL = "SELECT S.ID, S.IDGame, S.VariableName, S.VariableLabelGroup, S.VariableLabel AS Label, S.IDDataType, S.IDVariable, ISNULL(S.Enabled, 1) AS Attiva, " _
                 & "V.IDGroup, V.IDDataType AS DataTypeVariable, V.VariableLabel, S.EnabledFromPeriod, P.Descrizione AS PeriodToActivated " _
                 & "FROM Decisions_Players S " _
                 & "LEFT JOIN Variables V ON S.VariableName = V.VariableName AND S.IDGame = V.IDGame " _
                 & "LEFT JOIN BGOL_Periods P ON S.EnabledFromPeriod = P.Id " _
                 & "WHERE S.IDGame = " & IDGame

            If IDCategory <> 0 Then
                sSQL &= " AND V.IDCategory = " & IDCategory & " "
            End If

            sSQL &= "ORDER BY OrderVisibility, VariableLabelGroup "

            Return g_DAL.ExecuteDataTable(sSQL)
        End Function

        ''' <summary>
        ''' Restituisce un datatable che contiene la lista dei valori delle variabili dei player in un singolo periodo
        ''' </summary>
        ''' <param name="IDPeriod"></param>
        ''' <returns></returns>
        Public Function HandleLoadGamePeriodDecisionPlayerValue(IDPeriod As Integer, IDDecisionPlayer As Integer) As DataTable
            Dim sSQL As String

            ' Carico la tabella delle intestazioni da usare
            sSQL = "SELECT S.ID, S.IDDecisionPlayer, S.IDPeriod, S.VariableValue, S.IDPlayer, S.Simulation, " _
                 & "D.Id, D.IDGame, D.VariableName, D.VariableLabelGroup, D.VariableLabel, D.IDDataType, V.Id AS IDVariable " _
                 & "FROM Decisions_Players_Value S " _
                 & "LEFT JOIN Decisions_Players D ON S.IDDecisionPlayer = D.ID " _
                 & "LEFT JOIN Variables V ON D.VariableName = V.VariableName AND V.IDGame = D.IDGame " _
                 & "WHERE ISNULL(S.IDPeriod, " & IDPeriod & ") = " & IDPeriod & " AND IDDecisionPlayer = " & IDDecisionPlayer

            Return g_DAL.ExecuteDataTable(sSQL)
        End Function

        ''' <summary>
        ''' Ritorna il valore di una decisione specifica
        ''' Quando carico la pagina delle decisioni recupero il valore corretto
        ''' </summary>
        ''' <param name="IDPeriod"></param>
        ''' <param name="IDPlayer"></param>
        ''' <returns></returns>
        Public Function HandleLoadGamePeriodDecisionPlayerValueSingle(IDPeriod As Integer, IDPlayer As Integer, IDDecisionPlayer As Integer) As String
            Dim sSQL As String
            Dim sRet As String = ""

            ' Carico la tabella delle intestazioni da usare
            sSQL = "SELECT VariableValue FROM Decisions_Players_Value S " _
                 & "WHERE ISNULL(S.IDPeriod, " & IDPeriod & ") = " & IDPeriod & " AND IDPlayer = " & IDPlayer _
                 & " AND IDDecisionPlayer = " & IDDecisionPlayer

            Return Nz(g_DAL.ExecuteScalar(sSQL))
        End Function

        ''' <summary>
        ''' Restituisce un datatable che contiene la lista delle variabili decise nello scenario del game
        ''' queste possono appartenere anche ad una lista, vale a dire una lista di prodotti a cui i player associeranno i valori durante il game
        ''' </summary>
        ''' <param name="IDGame"></param>
        ''' <returns></returns>
        Public Function HandleGetItemsPeriodVariables(IDGame As Integer, LanguageActive As String) As DataTable
            Dim sSQL As String
            Dim oDAL As New DBHelper

            ' Carico la tabella delle intestazioni da usare
            sSQL = "SELECT * FROM vItems S WHERE S.IDGame = " & IDGame

            Dim oDTTemp As DataTable = oDAL.ExecuteDataTable(sSQL)

            Dim iIDLanguage As Integer
            sSQL = "SELECT ID FROM Languages WHERE Code = '" & LanguageActive & "' "
            iIDLanguage = Nni(oDAL.ExecuteScalar(sSQL))

            Dim drTranslations() As DataRow = oDTTemp.Select("IDLanguage = " & iIDLanguage)

            If drTranslations.Length > 0 Then ' ho trovato delle traduzioni, modifico la colonna VariableName con la descrizione trovata nelle traduzioni
                ' Cambio i nomi delle colonne, sostituisco quella translation con variablename, quella usata per il caricamento e l'utilizzo
                oDTTemp.Columns("VariableName").ColumnName = "VariableNameOriginal"
                oDTTemp.Columns("Translation").ColumnName = "VariableName"
            End If

            SQLConnClose(oDAL.GetConnObject)
            oDAL = Nothing

            Return oDTTemp

        End Function

        ''' <summary>
        ''' Restituisce un datatable che contiene i valori di tutte le decisioni prese dai player durante il gioco nel singolo periodo
        ''' </summary>
        ''' <param name="IDVariabile"></param>
        ''' <returns></returns>
        Public Function HandleGetValueDecisionPlayer(IDVariabile As Integer, IDPlayer As Integer, IDPeriod As Integer) As DataTable
            Dim sSQL As String

            ' Carico la tabella delle intestazioni da usare
            sSQL = "SELECT * FROM Decisions_Players_Value_List S WHERE S.IDDecisionValue = " & IDVariabile & " AND IDPlayer = " & IDPlayer & " " _
                 & "AND S.IDPeriod = " & IDPeriod _
                 & " ORDER BY S.IDPeriod, S.IDItem, S.IDAge "

            Return g_DAL.ExecuteDataTable(sSQL)
        End Function

        Public Function GetVariableState(NomeVariabileStato As String, IDPlayer As Integer, IDPeriod As Integer, NomeVariabileLista As String, IDGame As Integer, Optional IDAge As Integer = 0, Optional IDItem As Integer = 0) As String
            ' Recupero l'ID della variabile di stato generica
            Dim sSQL As String = "SELECT ID FROM Variables_State WHERE Name = '" & NomeVariabileStato & "' AND IDGame = " & IDGame
            Dim iIDVariabileStato As Integer = Nni(g_DAL.ExecuteScalar(sSQL))
            Dim oDAL As New DBHelper

            ' Con l'Id della variabile appena trovata vado a vedere il valore, se è un "List of" mi sposto nella tabella value_list
            Dim sValore As String
            sSQL = "SELECT Value FROM Variables_State_Value WHERE IDVariable = " & iIDVariabileStato & " AND ISNULL(IDPeriod, " & IDPeriod & ") = " & IDPeriod _
                 & " AND (ISNULL(IDPlayer, " & IDPlayer & ") = " & IDPlayer & " OR IDPLAYER = 0) "
            If IDAge > 0 Then
                sSQL &= " AND ISNULL(IDAge, " & IDAge & ") = " & IDAge
            End If
            If IDItem > 0 Then
                sSQL &= " AND ISNULL(IDItem, " & IDItem & ") = " & IDItem
            End If

            sValore = Nz(oDAL.ExecuteScalar(sSQL))
            If sValore.ToUpper.Contains("LIST OF") Then ' Vado a recuperare il valore nella tabella che contiene variabili di tipo lista
                ' Recupero l'ID della vaiabile di stato value
                sSQL = "SELECT ID FROM Variables_State_Value WHERE IDVariable = " & iIDVariabileStato & " AND ISNULL(IDPeriod, " & IDPeriod & ") = " & IDPeriod
                Dim iIDVariableState As Integer = g_DAL.ExecuteScalar(sSQL)
                sSQL = "SELECT Value FROM Variables_State_Value_List WHERE IDVariableState = " & iIDVariableState _
                     & " AND ISNULL(IDPeriod,  " & IDPeriod & ") = " & IDPeriod & " AND VariableName = '" & NomeVariabileLista & "' "
                sValore = Nz(oDAL.ExecuteScalar(sSQL))

            End If

            If sValore = "" Then
                sValore = "0"
            End If

            SQLConnClose(oDAL.GetConnObject)
            oDAL = Nothing

            Return sValore
        End Function

        Public Function GetVariableBoss(NomeVariabileBoss As String, IDPlayer As Integer, IDPeriod As Integer, NomeVariabileLista As String, IDGame As Integer) As String
            ' Recupero l'ID della variabile di stato generica
            Dim sSQL As String = "SELECT ID FROM Decisions_Boss WHERE Decision = '" & NomeVariabileBoss & "' AND IDGame = " & IDGame
            Dim iIDVariabileBoss As Integer = Nni(g_DAL.ExecuteScalar(sSQL))
            Dim oDAL As New DBHelper

            ' Con l'Id della variabile appena trovata vado a vedere il valore, se è un "List of" mi sposto nella tabella value_list
            Dim sValore As String
            sSQL = "SELECT Value FROM Decisions_Boss_Value WHERE IDDecisionBoss = " & iIDVariabileBoss & " AND ISNULL(IDPeriod, " & IDPeriod & ") = " & IDPeriod
            sValore = Nz(oDAL.ExecuteScalar(sSQL))
            If sValore.ToUpper.Contains("LIST OF") Then ' Vado a recuperare il valore nella tabella che contiene variabili di tipo lista
                ' Recupero l'ID della vaiabile di stato value
                sSQL = "SELECT ID FROM Decisions_Boss_Value WHERE IDDecisionBoss = " & iIDVariabileBoss & " AND ISNULL(IDPeriod, " & IDPeriod & ") = " & IDPeriod
                Dim iIDVariableBossValue As Integer = oDAL.ExecuteScalar(sSQL)
                sSQL = "SELECT Value FROM Decisions_Boss_Value_List WHERE IdDecisionBossValue = " & iIDVariableBossValue _
                 & " AND ISNULL(IDPeriod,  " & IDPeriod & ") = " & IDPeriod & " AND VariableName = '" & NomeVariabileLista & "' "
                sValore = Nz(oDAL.ExecuteScalar(sSQL))
            End If

            If sValore = "" Then
                sValore = "0"
            End If

            SQLConnClose(oDAL.GetConnObject)
            oDAL = Nothing

            Return sValore
        End Function

        Public Function GetDecisionPlayers(NomeVariabile As String, IDPlayer As Integer, IDPeriod As Integer, NomeVariabileLista As String, IDGame As Integer) As String
            ' Recupero l'ID della variabile di stato generica
            Dim sSQL As String = "SELECT ID FROM Decisions_Players WHERE VariableName = '" & NomeVariabile & "' AND IDGame = " & IDGame
            Dim iIDVariabilePlayer As Integer = Nni(g_DAL.ExecuteScalar(sSQL))
            Dim oDAL As New DBHelper

            ' Con l'Id della variabile appena trovata vado a vedere il valore, se è un "List of" mi sposto nella tabella value_list
            Dim sValore As String
            ' Controllo che sia una variabile che contiene anche l'ID del player
            sSQL = "SELECT VariableValue FROM Decisions_Players_Value WHERE IDDecisionPlayer = " & iIDVariabilePlayer & " AND ISNULL(IDPeriod, " & IDPeriod & ") = " & IDPeriod _
                 & " AND IDPlayer = " & IDPlayer
            sValore = Nz(oDAL.ExecuteScalar(sSQL))

            ' Variabile senza player...
            If sValore = "" Then
                sSQL = "SELECT VariableValue FROM Decisions_Players_Value WHERE IDDecisionPlayer = " & iIDVariabilePlayer & " AND ISNULL(IDPeriod, " & IDPeriod & ") = " & IDPeriod
                sValore = Nz(oDAL.ExecuteScalar(sSQL))
            End If

            If sValore.ToUpper.Contains("LIST OF") Then ' Vado a recuperare il valore nella tabella che contiene variabili di tipo lista
                ' Recupero l'ID della vaiabile di stato value
                sSQL = "SELECT ID FROM Decisions_Players_Value WHERE IDDecisionPlayer = " & iIDVariabilePlayer & " AND ISNULL(IDPeriod, " & IDPeriod & ") = " & IDPeriod
                Dim iIDVariableBossValue As Integer = g_DAL.ExecuteScalar(sSQL)
                sSQL = "SELECT Value FROM Decisions_Players_Value_List WHERE IDDecisionValue = " & iIDVariableBossValue _
                 & " AND ISNULL(IDPeriod,  " & IDPeriod & ") = " & IDPeriod & " AND VariableName = '" & NomeVariabileLista & "' "
                sValore = Nz(oDAL.ExecuteScalar(sSQL))
            End If

            If sValore = "" Then
                sValore = "0"
            End If

            SQLConnClose(oDAL.GetConnObject)
            oDAL = Nothing

            Return sValore
        End Function

        Public Function HandleGetMaxPeriodValid(IDGame As Integer) As Integer
            ' Recupero l'ID della variabile di stato generica
            Dim sSQL As String = "SELECT MAX(IDPeriodo) FROM BGOL_Games_Periods WHERE IDGame = " & IDGame

            If g_DAL Is Nothing Then g_DAL = New DBHelper
            Return Nni(g_DAL.ExecuteScalar(sSQL))
        End Function

        Public Function HandleGetMaxPeriodValidPrev(IDGame As Integer, IDPeriodMax As Integer) As Integer
            ' Recupero l'ID della variabile di stato generica

            Dim sSQL As String = "SELECT MAX(IDPeriodo) FROM BGOL_Games_Periods WHERE IDGame = " & IDGame & " AND IDPeriodo < " & IDPeriodMax

            Return Nni(g_DAL.ExecuteScalar(sSQL))
        End Function

        Public Function HandleGetMaxPeriodValidNext(IDGame As Integer, IDPeriodCurrent As Integer) As Integer
            ' Recupero l'ID della variabile di stato generica
            Dim sSQL As String = "SELECT MIN(IDPeriodo) FROM BGOL_Games_Periods WHERE IDGame = " & IDGame & " AND IDPeriodo >= " & IDPeriodCurrent

            Return Nni(g_DAL.ExecuteScalar(sSQL))
        End Function

        Public Function GetLastYearPeriod(IDGame As Integer, CurrentStep As Integer, LunghezzaPeriodo As Integer) As Integer
            Dim sSQL As String

            sSQL = "SELECT Id FROM BGOL_Periods WHERE IDGame = " & IDGame & " AND NumStep = " & CurrentStep - (12 / LunghezzaPeriodo)

            Return Nni(g_DAL.ExecuteScalar(sSQL))
        End Function

        Public Function HandleGetMaxPeriodGame(IDGame As Integer) As Integer
            ' Recupero l'ID della variabile di stato generica
            Dim sSQL As String = "SELECT MAX(ID) FROM BGOL_Periods WHERE IDGame = " & IDGame

            Return Nni(g_DAL.ExecuteScalar(sSQL))
        End Function

        Public Enum eVariablesCateogry
            Undefined = 1
            Sales = 2
            Marketing = 3
            Distribution = 4
            Operations = 5
            Finance_Control = 6
        End Enum

        ''' <summary>
        ''' Controllo la presenza delle variabili nelle tabelle di gestione delle decisioni del player
        ''' </summary>
        ''' <param name="Variables">Lista di variabili da veirificare</param>
        Public Sub CheckDecisionsPlayerExistsDatabase(Variables As List(Of String), IDGame As Integer, IDPeriod As Integer, IDCategory As eVariablesCateogry, IDPlayer As Integer)
            Dim iIDDecision As Integer
            Dim iIDDecisionPlayerValue As Integer
            Dim oDTVariable As DataTable
            Dim sSQL As String

            ' Partendo dalla lista delle variabili ne controllo la presenza 
            ' Ciclo sulla lista delle variabili da ricercare
            For Each sVariabile As String In Variables
                ' Faccio la verifica per ogni player presente in archivio, preparo tutti i dati iniziali
                'For Each oRowPlayer As DataRow In oDTPlayers.Rows

                sSQL = "SELECT ID, IDDataType, VariableName, VariableLabel, ListOfItems, VariableGroupLabel, DefaultValue " _
                     & "FROM Variables " _
                     & "WHERE UPPER(VariableName) = '" & sVariabile.ToUpper & "' "
                oDTVariable = g_DAL.ExecuteDataTable(sSQL)

                For Each oRow As DataRow In oDTVariable.Rows
                    ' Eseguo un update sulla variabile interessata, solo per la categoria, durante l'importazione non sempre sono allineate, uso quella che mi viene
                    ' passata quando richiamo la routine
                    sSQL = "UPDATE Variables Set IDCategory = " & IDCategory & " WHERE ID = " & Nni(oRow("ID"))
                    g_DAL.ExecuteNonQuery(sSQL)

                    ' Controllo l'esistenza della variabile tra le decisioni del player 
                    sSQL = "SELECT DP.ID FROM Decisions_Players DP " _
                        & "INNER JOIN Decisions_Players_Value DPV On DP.ID = DPV.IDDecisionPlayer " _
                        & "WHERE IDGame = " & IDGame & " And VariableName = '" & Nz(oRow("VariableName")) & "' "

                    Dim iIDChekDecision As Integer = Nni(g_DAL.ExecuteScalar(sSQL))

                    If iIDChekDecision = 0 Then
                        ' Procedo con l'inserimento nel database
                        sSQL = "INSERT INTO Decisions_Players (IDGame, VariableName, VariableLabel, VariableLabelGroup, IDDataType, IDVariable)  VALUES (" _
                             & IDGame & ", '" & Nz(oRow("VariableName")) & "', '" & Nz(oRow("VariableLabel")) & "', '" & Nz(oRow("VariableGroupLabel")) & "', " _
                             & Nni(oRow("IDDataType")) & ", " & Nni(oRow("ID")) & ")"
                        g_DAL.ExecuteNonQuery(sSQL)

                        ' Recupero l'ID della decisione inserita
                        sSQL = "SELECT MAX(ID) FROM Decisions_Players "
                        iIDDecision = Nni(g_DAL.ExecuteScalar(sSQL))
                    Else
                        sSQL = "SELECT ID FROM Decisions_Players WHERE IDGame = " & IDGame & " AND VariableName = '" & Nz(oRow("VariableName")) & "' "
                        iIDDecision = Nni(g_DAL.ExecuteScalar(sSQL))
                    End If

                    ' Se la variabile è di tipo lista faccio un certo inserimento altrimenti procedo con l'inserimento classico
                    If Nb(oRow("ListOfItems")) = True Then
                        ' Controllo l'esistenza dei valori nella tabella per il periodo/player selezionato
                        sSQL = "SELECT ID FROM Decisions_Players_Value WHERE IDDecisionPlayer = " & iIDDecision _
                             & " AND ISNULL(IDPeriod, " & IDPeriod & ") = " & IDPeriod & " AND ISNULL(IDPlayer, " & IDPlayer & ") = " & IDPlayer
                        iIDDecisionPlayerValue = Nni(g_DAL.ExecuteScalar(sSQL))

                        If iIDDecisionPlayerValue = 0 Then
                            sSQL = "INSERT INTO Decisions_Players_Value (IDDecisionPlayer, VariableValue) VALUES (" _
                                 & iIDDecision & ", '[List of Items]') "
                            g_DAL.ExecuteNonQuery(sSQL)

                            ' Recupero l'ID della decisione inserita
                            sSQL = "SELECT MAX(ID) FROM Decisions_Players_Value "
                            iIDDecisionPlayerValue = Nni(g_DAL.ExecuteScalar(sSQL))
                        End If

                        ' Recupero item per comporre il nome della variabile da salvare
                        Dim oDTITems As DataTable = HandleGetItemsPeriodVariables(IDGame, "")

                        ' Vado ad inserire i dati per la decisione appena creata, per il player che ha aperto la pagina, nel periodo selezionato
                        For Each oRowItm In oDTITems.Rows
                            ' Controllo la presenza dei dati per ogni item/player/period
                            Dim iIDDecisionPLayerValueList As Integer
                            sSQL = "SELECT ID FROM Decisions_Players_Value_List WHERE IDPlayer = " & IDPlayer & " " _
                                 & "AND IDPeriod = " & IDPeriod & " AND IDDecisionValue = " & iIDDecisionPlayerValue & " " _
                                 & "AND IDItem = " & Nni(oRowItm("IDVariable"))
                            iIDDecisionPLayerValueList = Nni(g_DAL.ExecuteScalar(sSQL))

                            If iIDDecisionPLayerValueList = 0 Then
                                ' Recupero il nome del team
                                Dim sTeamName As String = GetTeamName(IDPlayer)
                                sSQL = "INSERT INTO Decisions_Players_Value_List (IDDecisionValue, IDPlayer, IDPeriod, VariableName, IDItem, Value) VALUES (" _
                                     & iIDDecisionPlayerValue & ", " & IDPlayer & ", " & IDPeriod & ", 'var_" & sTeamName & "_" & Nz(oRow("VariableName")) _
                                     & Nz(oRowItm("VariableName")) & "', " & Nni(oRowItm("IDVariable")) & ", '" & Nz(oRow("DefaultValue")) & "') "
                                g_DAL.ExecuteNonQuery(sSQL)
                            End If
                        Next

                    Else
                        ' Controllo l'esistenza del record player/period nella tabella
                        sSQL = "SELECT ID FROM Decisions_Players_Value WHERE IDDecisionPlayer = " & iIDDecision & " " _
                             & "AND ISNULL(IDPeriod, " & IDPeriod & ") = " & IDPeriod & " AND ISNULL(IDPlayer, " & IDPlayer & ") = " & IDPlayer
                        iIDDecisionPlayerValue = Nni(g_DAL.ExecuteScalar(sSQL))

                        If iIDDecisionPlayerValue = 0 Then
                            sSQL = "INSERT INTO Decisions_Players_Value (IDDecisionPlayer, IDPeriod, IDPlayer, VariableValue) VALUES (" _
                                 & iIDDecision & ", " & IDPeriod & ", " & IDPlayer & ", '" & Nz(oRow("DefaultValue")) & "') "
                            g_DAL.ExecuteNonQuery(sSQL)
                        End If
                    End If
                Next
                'Next
            Next

        End Sub

#Region "Traduzioni"

        Public Function GetVariableTranslation(IDGame As Integer, IDVariable As Integer, CodeLanguages As String) As String
            Dim sRet As String
            Dim iIDLanguage As Integer
            Dim sSQL As String

            ' Recupero l'ID della traduzione 
            sSQL = "SELECT ID FROM Languages WHERE Code = '" & CodeLanguages & "' "
            iIDLanguage = Nni(g_DAL.ExecuteScalar(sSQL))

            ' Con l'id della variabile e del codice lingua da usare recupero la traduzione, se fosse vuota uso quella di default
            sSQL = "SELECT Translation FROM Variables_Translations WHERE IDVariable = " & IDVariable & " AND IDLAnguage = " & iIDLanguage
            sRet = Nz(g_DAL.ExecuteScalar(sSQL))

            If sRet = "" Then ' Recupero la descrizione standard, non esiste la traduzione per la variabile
                sSQL = "SELECT VariableLabel FROM Variables WHERE ID = " & IDVariable
                sRet = Nz(g_DAL.ExecuteScalar(sSQL))
            End If

            Return sRet
        End Function

        Public Function GetVariableTranslation(IDGame As Integer, VariableName As String, CodeLanguages As String) As String
            Dim sRet As String
            Dim iIDLanguage As Integer
            Dim sSQL As String
            Dim iIDVariable As Integer

            ' Recupero l'ID della traduzione 
            sSQL = "SELECT ID FROM Languages WHERE Code = '" & CodeLanguages & "' "
            iIDLanguage = Nni(g_DAL.ExecuteScalar(sSQL))

            sSQL = "SELECT ID FROM Variables WHERE IDGame = " & IDGame & " AND VariableName = '" & VariableName & "' "
            iIDVariable = Nni(g_DAL.ExecuteScalar(sSQL))

            ' Con l'id della variabile e del codice lingua da usare recupero la traduzione, se fosse vuota uso quella di default
            sSQL = "SELECT Translation FROM Variables_Translations WHERE IDVariable = " & iIDVariable & " AND IDLAnguage = " & iIDLanguage
            sRet = Nz(g_DAL.ExecuteScalar(sSQL))

            If sRet = "" Then ' Recupero la descrizione standard, non esiste la traduzione per la variabile
                sSQL = "SELECT VariableLabel FROM Variables WHERE ID = " & iIDVariable
                sRet = Nz(g_DAL.ExecuteScalar(sSQL))
            End If

            Return sRet
        End Function

        Public Function GetItemTranslation(IDItem As Integer, CodeLanguages As String) As String
            Dim sRet As String
            Dim iIDLanguage As Integer
            Dim sSQL As String

            ' Recupero l'ID della traduzione 
            sSQL = "SELECT ID FROM Languages WHERE Code = '" & CodeLanguages & "' "
            iIDLanguage = Nni(g_DAL.ExecuteScalar(sSQL))

            ' Con l'id della variabile e del codice lingua da usare recupero la traduzione, se fosse vuota uso quella di default
            sSQL = "SELECT Translation FROM Items_Translations WHERE IDItem = " & IDItem & " AND IDLanguage = " & iIDLanguage
            sRet = Nz(g_DAL.ExecuteScalar(sSQL))

            If sRet = "" Then ' Recupero la descrizione standard, non esiste la traduzione per la variabile
                sSQL = "SELECT VariableLabel FROM Items_Period_Variables WHERE ID = " & IDItem
                sRet = Nz(g_DAL.ExecuteScalar(sSQL))
            End If

            Return sRet
        End Function

#End Region

#Region "CONFIGURAZIONE"

        Public Function FunctionReturnConfigurationParameter(ParameterName As String) As String
            Dim sSQL As String = "SELECT " & ParameterName & " FROM Configuration "

            Dim sRet As String

            sRet = APPCore.Utility.Nz(g_DAL.ExecuteScalar(sSQL))

            Return sRet
        End Function

        Public Sub SQLConnClose(SQLConnection As SqlClient.SqlConnection)
            If SQLConnection.State = ConnectionState.Open Then
                SQLConnection.Close()
            End If
            SQLConnection.Dispose()

        End Sub

#End Region

#Region "GESTIONE DELLE VARIABILI DI SISTEMA"

        Public Function HandleLoadComboBox_Variables_DataType() As DataTable
            Dim oRet As New DataTable
            Dim sSQL As String = "SELECT ID, TypeDescription, StandardLabel FROM Variables_Type ORDER BY TypeDescription "

            Try
                Return g_DAL.ExecuteDataTable(sSQL, CommandType.Text)
            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            End Try
        End Function

        Public Function ReturnStandardLabel_Decisions_Variables(IDDecisionsVariable As Integer) As String
            Dim sSQL As String = "SELECT ID, TypeDescription, StandardLabel FROM Variables_Type WHERE ID = " & IDDecisionsVariable
            Dim oDT As DataTable = g_DAL.ExecuteDataTable(sSQL)
            Dim oDRDecision As DataRow() = oDT.Select()
            If oDRDecision.Count > 0 Then
                Return APPCore.Utility.Nz(oDRDecision(0)("StandardLabel"))
            Else
                Return ""
            End If
        End Function

        Public Function ReturnVariableDataTypeISCorrect(IDDataType As Integer, ValueVariable As Object) As Boolean
            Dim sSQL As String = "SELECT UPPER(TypeDescription) FROM Variables_Type WHERE ID = " & IDDataType
            Dim sDataType As String = ""

            Dim bRet As Boolean = True

            sDataType = g_DAL.ExecuteScalar(sSQL)

            Select Case sDataType
                Case Is = "STRING"
                    If ValueVariable.GetType Is "System.String" Then
                        bRet = True
                    End If

                Case Is = "BOOLEAN"
                    If ValueVariable.GetType Is "System.Boolean" Then
                        bRet = True
                    End If

                Case Is = "NUMERIC"
                    If IsNumeric(ValueVariable) Then
                        bRet = True
                    End If

                Case Is = "NUMERIC DECIMAL"
                    If IsNumeric(ValueVariable) Then
                        bRet = True
                    End If

                Case Is = "DATE"
                    If ValueVariable.GetType Is "System.Date" Then
                        bRet = True
                    End If

                Case Is = "DATE AND TIME"
                    If ValueVariable.GetType Is "System.DateTime" Then
                        bRet = True
                    End If

                Case Is = "PERCENTAGE"
                    If IsNumeric(ValueVariable) Then
                        bRet = True
                    End If

                Case Is = "MONEY"
                    If IsNumeric(ValueVariable) Then
                        bRet = True
                    End If
            End Select
            Return bRet
        End Function

        Public Function HandleLoadVariablesGroups() As DataTable
            Dim sSQL As String

            sSQL = "SELECT -1 AS ID, '' AS GroupDescription UNION ALL SELECT ID, GroupDescription FROM Variables_Groups ORDER BY ID "
            Return g_DAL.ExecuteDataTable(sSQL)
        End Function

        Public Function HandleLoadVariablesCategories() As DataTable
            Dim sSQL As String

            sSQL = "SELECT -1 AS ID, '' AS Category UNION ALL SELECT ID, Category FROM Variables_Category ORDER BY ID"

            Return g_DAL.ExecuteDataTable(sSQL)
        End Function

        ''' <summary>
        ''' Controlla l'esistenza della variabile nella tabella VARIABLES
        ''' nel caso non ci sia viene creata e ritorna l'ID
        ''' </summary>
        ''' <param name="VariableName"></param>
        ''' <param name="IDGame"></param>
        ''' <returns></returns>
        Public Function VerifiyVariableExists(VariableName As String, IDGame As Integer, IDGroup As Integer,
                                              VariableLabel As String, IDCategory As Integer, IDDefinition As Integer,
                                              DecisionPLayer As Boolean, DecisionBoss As Boolean, DecisionState As Boolean, DefaultValue As String) As Integer
            Dim sSQL As String
            Dim iRet As Integer


            sSQL = "SELECT ID FROM Variables WHERE VariableName = '" & VariableName & "' AND IDGame = " & IDGame
            iRet = Nni(g_DAL.ExecuteScalar(sSQL))

            If iRet = 0 Then
                ' Inserisco i dati necessari
                sSQL = "INSERT INTO Variables (IDGame, IDGroup, VariableName, VariableLabel, IDCategory, IDDefinition, " _
                     & "DecisionPLayer, DecisionBoss, DecisionState) VALUES (" & IDGame & ", " & IDGroup & ", '" & VariableName & "', " _
                     & "'" & VariableLabel & "', " & IDCategory & ", " & IDDefinition & ", " & Boolean_To_Bit(DecisionPLayer) & ", " & Boolean_To_Bit(DecisionBoss) & ", " _
                     & Boolean_To_Bit(DecisionState) & ") "

                ' In aggiornamento per il momento non ci vado, se la trovo la lascio così come è, ho paura che faccia del casino e basta... 

                ' Else ' Vado in aggiornamento, nel caso abbia fatto porcate durante la prima importazione dei dati
                '    sSQL = "UPDATE Variables SET IDGroup = " & IDGroup & ", IDCategory = " & IDCategory & ", IDDefinition = " & IDDefinition & ", DecisionPlayer = " & Boolean_To_Bit(DecisionPLayer) & ", " _
                '         & "DecisionBoss = " & Boolean_To_Bit(DecisionBoss) & ", DecisionState = " & Boolean_To_Bit(DecisionState) & " " _
                '         & "WHERE ID = " & iRet
            End If

            Return g_DAL.ExecuteNonQuery(sSQL)

        End Function

        Public Function GetVariableDefineValue(NomeVariabile As String, IDPlayer As Integer, IDItem As Integer, IDAge As Integer, IDPeriod As Integer, IDGame As Integer) As String
            ' Recupero l'ID Della variabile generica
            Dim sSQL As String = "SELECT ID FROM Variables_Calculate_Define WHERE VariableName = '" & NomeVariabile & "' AND IDGame = " & IDGame
            Dim iIDVariabileMaster As Integer = Nni(g_DAL.ExecuteScalar(sSQL))

            ' Con l'ID appena preso recupero il valore della variabile per uno specifico ITEM
            Dim sValue As String
            sSQL = "SELECT Value FROM Variables_Calculate_Value WHERE IDVariable = " & iIDVariabileMaster & " AND IDPLayer = " & IDPlayer _
                 & " AND IDPeriod = " & IDPeriod & " AND IDItem = " & IDItem & " AND IDAge = " & IDAge
            sValue = Nz(g_DAL.ExecuteScalar(sSQL))

            If sValue = "" Then sValue = "0"

            Return sValue
        End Function

#End Region

        Public Function FindControlRecursive(root As Control, ID As String) As Control
            If root.ID = ID Then
                Return root
            End If
            Dim oCTRL As Control
            For Each oCTRL In root.Controls
                Dim oCTRLFind As Control = FindControlRecursive(oCTRL, ID)
                If Not oCTRLFind Is Nothing Then
                    Return oCTRLFind
                End If
            Next
            Return Nothing
        End Function

        Public Function GetItemName(IDItem As Integer) As String
            Dim sRet As String
            Dim sSQL As String

            sSQL = "SELECT VariableNameDefault FROM vItems WHERE IDVariable = " & IDItem
            sRet = Nz(g_DAL.ExecuteScalar(sSQL))

            Return sRet
        End Function

        Public Function GetAgeName(IDAge As Integer) As String
            Dim sRet As String
            Dim sSQL As String

            sSQL = "SELECT AgeDescription FROM Age WHERE ID = " & IDAge
            sRet = Nz(g_DAL.ExecuteScalar(sSQL))

            Return sRet
        End Function

#Region "INTERPOLAZIONE"

        ' Calcolo dell'interpolazione
        ' Y = Y1 + (X - X1) * ((Y2 - Y1) / (X2 - X1))

        Public Function CalculateInterpolation(NomeTabella As String, Valore As Double) As Double
            Dim sSQL As String

            Dim dRet As Double = 0.0

            Dim dValueX1 As Double
            Dim dValueX2 As Double

            Dim dValueY1 As Double
            Dim dValueY2 As Double

            Dim oDTResultInterpolazione As DataTable

            sSQL = "SELECT TID.ValueX, TID.ValueY " _
                 & "FROM Table_Interpolation TI " _
                 & "INNER JOIN Table_Interpolation_Detail TID ON TI.Id = TID.IDTable " _
                 & "WHERE TI.TableDescription = '" & NomeTabella & "' ORDER BY ValueX "
            oDTResultInterpolazione = g_DAL.ExecuteDataTable(sSQL)

            ' Recupero il valore X1 e Y1
            For Each oRow As DataRow In oDTResultInterpolazione.Rows
                Dim dValoreFind As Double = Nn(oRow("ValueX"))
                If Valore >= dValoreFind Then
                    dValueX1 = dValoreFind
                    dValueY1 = Nn(oRow("ValueY"))
                End If
            Next

            ' Recupero il valore X2 e Y2 per il calcolo dell'interpolazione
            For Each oRow As DataRow In oDTResultInterpolazione.Rows
                Dim dValoreFind As Double = Nn(oRow("ValueX"))
                If Valore <= dValoreFind AndAlso dValoreFind > dValueX1 Then
                    dValueX2 = dValoreFind
                    dValueY2 = Nn(oRow("ValueY"))
                    Exit For ' Trovato il primo valore valido esco, altrimenti mi sballa il risultato
                End If
            Next

            ' Calcolo l'interpolazione
            dRet = dValueY1 + (Valore - dValueX1) * ((dValueY2 - dValueY1) / (dValueX2 - dValueX1))

            Return dRet
        End Function

#End Region

    End Module

End Namespace


