﻿#Region "Using"

Imports System.Text
Imports System.Web.UI
Imports System.Web.UI.WebControls

#End Region

Namespace CustomControls
    ''' <summary>
    ''' Displays a confirm button control on the Web page.
    ''' </summary>
    Public Class ConfirmButton
        Inherits Telerik.Web.UI.RadButton
        Implements IPostBackEventHandler

#Region "Properties"

        ''' <summary>
        ''' The question to ask in the confirm box.
        ''' </summary>
        Public Property Question() As String
            Get
                Return DirectCast(If(ViewState("Question"), String.Empty), String)
            End Get
            Set
                ViewState("Question") = Value
            End Set
        End Property

        ''' <summary>
        ''' The answer recieved from the confirm box.
        ''' </summary>
        Public Property Answer() As Answer
            Get
                Return CType(If(ViewState("Answer"), Answer.None), Answer)
            End Get
            Private Set
                ViewState("Answer") = Value
            End Set
        End Property


#End Region

#Region "Methods"

        Protected Overrides Sub Render(writer As HtmlTextWriter)
            Dim sb As New StringBuilder()
            For Each key As String In Me.Attributes.Keys
                If key.ToLowerInvariant() <> "onclick" Then
                    sb.Append(key & "=""" & Me.Attributes(key) & """ ")
                End If
            Next

            Dim script As String = OnClientClicked & ";var answer='No'; if (confirm('" & Question.Replace("'", "\'") & "')){answer='Yes'}"
            Dim onclick As String = "onclick=""" & script & ";__doPostBack('" & Me.UniqueID & "', answer)" & """"
            Dim output As String = String.Format("<input type=""button"" {0}name=""" & Me.UniqueID & """ id=""{1}"" value=""{2}"" {3} />", sb.ToString(), UniqueID, Text, onclick)
            writer.Write(output)
        End Sub

        ''' <summary>
        ''' Forces the button to print out the
        ''' postback JavaScript
        ''' </summary>
        Protected Overrides Sub OnPreRender(e As EventArgs)
            Me.UseSubmitBehavior = False
            MyBase.OnPreRender(e)
        End Sub

#End Region

#Region "IPostBackEventHandler Members"

        Private Sub IPostBackEventHandler_RaisePostBackEvent(eventArgument As String) Implements IPostBackEventHandler.RaisePostBackEvent
            Me.Answer = CType([Enum].Parse(GetType(Answer), eventArgument), Answer)
            MyBase.OnClick(EventArgs.Empty)
        End Sub

#End Region
    End Class

    ''' <summary>
    ''' The answers from the confirm box.
    ''' </summary>
    Public Enum Answer
        ''' <summary>The default value when the question haven't been answered yet.</summary>
        None
        Yes
        No
    End Enum

End Namespace
