﻿Imports System.Data
Imports APS
Imports APPCore.Utility
Imports DALC4NET
Imports System.Math

Public Class Engine_Arkea

    Private m_DataTablePlayers As DataTable

    Private m_DataTableItems As DataTable

    Private m_DataTableAge As DataTable

    Private m_DataTableVariables As DataTable

    Private m_DataTableVariablesState As DataTable
    Private m_DataTableVariablesStateValue As DataTable
    Private m_DataTableVariablesStateValueList As DataTable

    Private m_DataTableDecisionsPlayers As DataTable
    Private m_DataTableDecisionsPlayersValue As DataTable
    Private m_DataTableDecisionsPlayersValueList As DataTable

    Private m_DataTableVariableCalculateDefine As DataTable
    Private m_DataTableVariableCalculateValue As DataTable

    Private m_DataTableVariablesDecisionsBoss As DataTable
    Private m_DataTableVariablesDecisionsBossValue As DataTable
    Private m_DataTableVariablesDecisionsBossValueList As DataTable

    Private m_DataTableDecisionsDeveloper As DataTable
    Private m_DataTableDecisionsDeveloperValue As DataTable
    Private m_DataTableDecisionsDeveloperValueList As DataTable

    Private m_IDGame As Integer
    Private m_IDPeriod As Integer
    Private m_IDPeriodNext As Integer

    Private m_Simulation As Boolean = False

    Private m_LunghezzaPeriodo As Integer

    Private m_CurrentStep As Integer

#Region "Allineamento tabelle di gestione delle variabili: BOSS, Player, Stato"

    ''' <summary>
    ''' Riallineamento della tabella delle variabili di stato
    ''' </summary>
    Public Function VariableStateTable_Align(IDGame As Integer) As Boolean
        ' Riallineo la tabella poichè dopo un importazione massiva delle variabili posso non avere dei valori, soprattutto nella tabella che contiene liste di valori
        Dim sSQL As String
        Dim oDAL As New DBHelper
        Dim sVariableName As String
        Dim sValoreIniziale As String
        Dim sDescrizione As String
        Dim sDescrizioneDIM2 As String
        Dim iIDVariableState As Integer
        Dim iIDVariableStateValue As Integer
        Dim bRet As Boolean = False
        Dim iIDGruppo As Integer
        Dim iIDVariableExist As Integer

        Try
            sSQL = "SELECT * FROM Variables_Initial_Values VIV " _
                 & "WHERE IDGame = " & IDGame & " AND TIPO ='F' "

            Dim oDTVariablesState As DataTable = oDAL.ExecuteDataTable(sSQL)
            For Each oRowVariableState As DataRow In oDTVariablesState.Rows
                sVariableName = Nz(oRowVariableState("VariableName"))
                sValoreIniziale = Nz(oRowVariableState("ValoreIniziale"))
                sDescrizione = Nz(oRowVariableState("Descrizione")).Replace("'", "''")
                sDescrizioneDIM2 = Nz(oRowVariableState("DIM2DESC"))
                iIDGruppo = Nni(oRowVariableState("IDGruppo"))

                ' Controllo l'esistenza della variabile
                ' se non esiste la creo altrimenti salto il passaggio

                sSQL = "SELECT ID FROM Variables_State WHERE VariableLabel = '" & sDescrizione & "' AND Name = '" & sVariableName & "' AND IDGame = " & IDGame & " AND IDGroup = " & iIDGruppo
                iIDVariableState = Nni(oDAL.ExecuteScalar(sSQL))

                ' Controllo la presenza della variabile all'interno della tabella generale delle variabili
                iIDVariableExist = VerifiyVariableExists(sVariableName, IDGame, iIDGruppo, sDescrizione, 1, 5, 1, 0, 0, sValoreIniziale.Replace(",", "."))

                If iIDVariableState = 0 Then
                    ' Procedo con l'inserimento della variabile in archivio
                    sSQL = "INSERT INTO Variables_State (VariableLabel, Name, IDGame, IDGroup) VALUES ('" & sDescrizione & "', '" & sVariableName & "', " & IDGame & ", " & iIDGruppo & ") "
                    oDAL.ExecuteNonQuery(sSQL)

                    'Recupero l'ID della variabile appena inserita
                    sSQL = "SELECT ID FROM Variables_State WHERE Name = '" & sVariableName & "' AND IDGame = " & IDGame
                    iIDVariableState = Nni(oDAL.ExecuteScalar(sSQL))
                End If

                If Nz(oRowVariableState("DIM1")).ToUpper <> "" Then ' è una variabile con contenuti multidimensionali
                    If Nz(oRowVariableState("DIM1")).ToUpper = "PLAYERS" Then
                        ' Prima di fare l'inserimento controllo la presenza della variabile in archivio, altrimenti la inserisce n volte
                        sSQL = "SELECT ID FROM Variables_State_Value WHERE IDVariable = " & iIDVariableState
                        iIDVariableStateValue = Nni(oDAL.ExecuteScalar(sSQL))

                        If iIDVariableStateValue = 0 Then
                            ' Procedo con l'inserimento della definizione nella tabella Variables_State_Value
                            If Nz(oRowVariableState("DIM2")).ToUpper = "ITEMS" Then
                                sSQL = "INSERT INTO Variables_State_Value (IDVariable, Value) VALUES (" & iIDVariableState & ", '[LIST OF PLAYERS$ITEMS]') "
                            ElseIf Nz(oRowVariableState("DIM2")).ToUpper = "AGE" Then
                                sSQL = "INSERT INTO Variables_State_Value (IDVariable, Value) VALUES (" & iIDVariableState & ", '[LIST OF PLAYERS$AGE]') "
                            End If
                            oDAL.ExecuteNonQuery(sSQL)

                            ' Recupero l'id appena inserito
                            If Nz(oRowVariableState("DIM2")).ToUpper = "ITEMS" Then
                                sSQL = "SELECT MAX(ID) FROM Variables_State_Value WHERE IDVariable = " & iIDVariableState & " AND VALUE = '[LIST OF PLAYERS$ITEMS]' "
                            ElseIf Nz(oRowVariableState("DIM2")).ToUpper = "AGE" Then
                                sSQL = "SELECT MAX(ID) FROM Variables_State_Value WHERE IDVariable = " & iIDVariableState & " AND VALUE = '[LIST OF PLAYERS$AGE]' "
                            End If
                            iIDVariableStateValue = Nni(oDAL.ExecuteScalar(sSQL))
                        End If

                        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                            If Nz(oRowVariableState("DIM2")).ToUpper = "ITEMS" Then
                                ' Con la descrizione recupero anche l'ID del relativo ITEM da salvare
                                sSQL = "SELECT IDVariable FROM vItems WHERE IDGame = " & IDGame & " AND VariableName = '" & sDescrizioneDIM2 & "' "
                                Dim iIDItem As Integer = Nni(oDAL.ExecuteScalar(sSQL))

                                sVariableName = "var_" & Nz(oRowPlayer("TeamName")) & "_" & Nz(oRowVariableState("VariableName")) & "_" & Nz(oRowVariableState("DIM2DESC"))
                                sSQL = "INSERT INTO Variables_State_Value_List (IDVariableState, IDPlayer, IDItem, IDPeriod, VariableName, Value) " _
                                     & "VALUES (" & iIDVariableStateValue & ", " & Nni(oRowPlayer("IDTeam")) & ", " & iIDItem & ", " _
                                     & m_IDPeriod & ", '" & sVariableName & "', '" & sValoreIniziale & "') "
                                oDAL.ExecuteNonQuery(sSQL)

                            ElseIf Nz(oRowVariableState("DIM2")).ToUpper = "AGE" Then
                                ' Con la descrizione recupero anche l'ID del relativo ITEM da salvare
                                sSQL = "SELECT ID FROM Age WHERE IDGame = " & IDGame & " AND AgeDescription = '" & sDescrizioneDIM2 & "' "
                                Dim iIDAge As Integer = Nni(oDAL.ExecuteScalar(sSQL))

                                sSQL = "SELECT AgeDescription FROM Age WHERE ID = " & iIDAge
                                Dim sDescAge As String = Nz(oDAL.ExecuteScalar(sSQL))

                                sVariableName = "var_" & Nz(oRowPlayer("TeamName")) & "_" & Nz(oRowVariableState("VariableName")) & "_" & sDescrizioneDIM2
                                sSQL = "INSERT INTO Variables_State_Value_List (IDVariableState, IDPlayer, IDAge, IDPeriod, VariableName, Value) " _
                                     & "VALUES (" & iIDVariableStateValue & ", " & Nni(oRowPlayer("IDTeam")) & ", " & sDescAge & ", " _
                                     & m_IDPeriod & ", '" & sVariableName & "', '" & sValoreIniziale & "') "
                                oDAL.ExecuteNonQuery(sSQL)

                            Else
                                ' Inserisco per il periodo 1 il valore secco della variabile legata al player
                                sSQL = "INSERT INTO Variables_State_Value (IDVariable, Value, IDPeriod, IDPlayer) VALUES (" _
                                     & iIDVariableState & ", '" & sValoreIniziale & "', 1, " & Nni(oRowPlayer("IDTeam")) & ")"
                                oDAL.ExecuteScalar(sSQL)
                            End If
                        Next
                        sVariableName = ""
                    End If

                Else ' variabile a valore secco
                    ' Procedo con l'inserimento della definizione nella tabella Variables_State_Value
                    sSQL = "INSERT INTO Variables_State_Value (IDVariable, Value, IDPeriod) VALUES (" & iIDVariableState & ", '" & sValoreIniziale & "', 1) "
                    oDAL.ExecuteNonQuery(sSQL)
                    sVariableName = ""
                End If
            Next

            bRet = True

            oDAL.GetConnObject.Close()
            oDAL = Nothing

        Catch ex As Exception
            g_MessaggioErrore &= "VariableStateTable_Align -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try

        Return bRet
    End Function

    ''' <summary>
    ''' Riallineamento delle variabili del deisgner/developer
    ''' </summary>
    ''' <param name="IDGame"></param>
    ''' <returns></returns>
    Public Function VariablePlayersTable_Align(IDGame As Integer, IDPlayer As Integer) As Boolean
        ' Riallineo la tabella poichè dopo un importazione massiva delle variabili posso non avere dei valori, soprattutto nella tabella che contiene liste di valori
        Dim sSQL As String
        Dim bRet As Boolean = False
        Dim sTeamName As String = GetTeamName(IDGame, IDPlayer)

        Try
            ' Decision player value
            sSQL = "INSERT INTO Decisions_Players_Value (DPV.IDDecisionPlayer, DPV.IDPeriod, DPV.VariableValue, DPV.IDPlayer, DPV.Simulation) " _
                 & "SELECT IDDecisionPlayer, IDPeriod, VariableValue, " & IDPlayer & ", Simulation " _
                 & "FROM Decisions_Players D " _
                 & "LEFT JOIN Decisions_Players_Value DPV ON D.Id = DPV.IDDecisionPlayer " _
                 & "WHERE D.IDGame = " & IDGame & " AND DPV.IDPlayer = 2 AND DPV.IDPeriod = 1 "
            g_DAL.ExecuteNonQuery(sSQL)

            ' Decision player value_list
            sSQL = "INSERT INTO Decisions_Players_Value_List (IDDecisionValue, IDPlayer, IDPeriod, VariableName, Value, IDItem, IDAge, Simulation) " _
                 & "SELECT IDDecisionValue, " & IDPlayer & ", DPVL.IDPeriod, REPLACE(DPVL.VariableName, 'M&U', '" & sTeamName & "'), DPVL.Value, DPVL.IDItem, DPVL.IDAge, DPVL.Simulation " _
                 & "FROM Decisions_Players D " _
                 & "LEFT JOIN Decisions_Players_Value DPV ON D.Id = DPV.IDDecisionPlayer " _
                 & "LEFT JOIN Decisions_Players_Value_List DPVL ON DPV.ID = DPVL.IDDecisionValue " _
                 & "WHERE D.IDGame = " & IDGame & " AND DPVL.IDPlayer = 2 AND DPVL.IDPeriod = 1 "
            g_DAL.ExecuteNonQuery(sSQL)

            ' Variabili del boss
            sSQL = "INSERT INTO Decisions_Boss_Value_List (IdDecisionBossValue, IDPeriod, VariableName, Value, Simulation) " _
                 & "SELECT DBVL.IdDecisionBossValue, DBVL.IDPeriod, REPLACE(DBVL.VariableName, 'M&U', '" & sTeamName & "'), DBVL.Value, DBVL.Simulation " _
                 & "FROM Decisions_Boss D " _
                 & "LEFT JOIN Decisions_Boss_Value DBV ON D.Id = DBV.IDDecisionBoss " _
                 & "LEFT JOIN Decisions_Boss_Value_List DBVL ON DBV.id = DBVL.IdDecisionBossValue " _
                 & "WHERE D.IDGame = " & IDGame & " AND DBVL.VariableName LIKE '%M&U%' AND DBVL.IDPeriod = 1 "
            g_DAL.ExecuteNonQuery(sSQL)

            ' Variabili developer
            sSQL = "INSERT INTO Decisions_Developer_Value (IDDecisionDeveloper, IDPeriod, Value, IDPlayer, IDItem, Simulation) " _
                 & "SELECT DBV.IDDecisionDeveloper, DBV.IDPeriod, REPLACE(DBV.Value, 'M&U', '" & sTeamName & "'), " & IDPlayer & ", DBV.IDItem, DBV.Simulation " _
                 & "FROM Decisions_Developer D " _
                 & "LEFT JOIN Decisions_Developer_Value DBV ON D.Id = DBV.IDDecisionDeveloper " _
                 & "WHERE D.IDgame = " & IDGame & " AND DBV.IDPlayer = 2 AND DBV.IDPeriod = 1 "
            g_DAL.ExecuteNonQuery(sSQL)

            ' Variabili di stato 
            sSQL = "INSERT INTO Variables_State_Value (IDVariable, Value, IDPeriod, IDPlayer, IDItem, IDAge, Simulation) " _
                 & "SELECT VSV.IDVariable, VSV.Value, VSV.IDPeriod, " & IDPlayer & ", VSV.IDItem, VSV.IDAge, VSV.Simulation " _
                 & "FROM Variables_State VS " _
                 & "INNER JOIN Variables_State_Value VSV ON VS.ID = VSV.IDVariable " _
                 & "WHERE VS.IDGame = " & IDGame & " AND VSV.IDPlayer = 2 AND VSV.IDPeriod = 1 "
            g_DAL.ExecuteNonQuery(sSQL)

            sSQL = "INSERT INTO Variables_State_Value_List (IDVariableState, IDPlayer, IDItem, IDPeriod, IDAge, VariableName, Value, Simulation) " _
                 & "SELECT VSVL.IDVariableState, " & IDPlayer & ", VSVL.IDItem, VSVL.IDPeriod, VSVL.IDAge, REPLACE(VSVL.VariableName, 'M&U', '" & sTeamName & "'), VSVL.Value, VSVL.Simulation " _
                 & "FROM Variables_State VS " _
                 & "INNER JOIN Variables_State_Value VSV ON VS.ID = VSV.IDVariable " _
                 & "LEFT JOIN Variables_State_Value_List VSVL ON VSV.Id = VSVL.IDVariableState " _
                 & "WHERE VS.IDGame = " & IDGame & " AND VSVL.IDPlayer = 2 AND VSVL.IDPeriod = 1 "
            g_DAL.ExecuteNonQuery(sSQL)

            SQLConnClose(g_DAL.GetConnObject)

            bRet = True
        Catch ex As Exception
            g_MessaggioErrore &= "VariableStateTable_Align -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try

        Return bRet
    End Function

    Public Function VariableDeveloperTable_Align(IDGame As Integer) As Boolean
        ' Riallineo la tabella poichè dopo un importazione massiva delle variabili posso non avere dei valori, soprattutto nella tabella che contiene liste di valori
        Dim sSQL As String
        Dim oDAL As New DBHelper
        Dim sVariableName As String
        Dim sValoreIniziale As String
        Dim sDescrizione As String
        Dim sDescrizioneDIM2 As String
        Dim iIDVariableDeveloper As Integer
        Dim iIDVariableDeveloperValue As Integer
        Dim bRet As Boolean = False
        Dim iIDGruppo As Integer
        Dim iIDVariableExist As Integer

        Try
            sSQL = "Select * FROM Variables_Initial_Values VIV " _
                 & "WHERE IDGame = " & IDGame & " And TIPO ='T' AND Gruppo = 102 "

            Dim oDTVariablesState As DataTable = oDAL.ExecuteDataTable(sSQL)
            For Each oRowVariableState As DataRow In oDTVariablesState.Rows
                sVariableName = Nz(oRowVariableState("VariableName"))
                sValoreIniziale = Nz(oRowVariableState("ValoreIniziale"))
                sDescrizione = Nz(oRowVariableState("Descrizione")).Replace("'", "''")
                sDescrizioneDIM2 = Nz(oRowVariableState("DIM2DESC"))
                iIDGruppo = Nni(oRowVariableState("IDGruppo"))

                ' Controllo l'esistenza della variabile
                ' se non esiste la creo altrimenti salto il passaggio

                sSQL = "SELECT ID FROM Decisions_Developer WHERE VariableLabel = '" & sDescrizione & "' AND VariableName = '" & sVariableName & "' AND IDGame = " & IDGame & " AND IDGroup = " & iIDGruppo
                iIDVariableDeveloper = Nni(oDAL.ExecuteScalar(sSQL))

                ' Controllo la presenza della variabile all'interno della tabella generale delle variabili
                iIDVariableExist = VerifiyVariableExists(sVariableName, IDGame, iIDGruppo, sDescrizione, 1, 5, 1, 0, 0, sValoreIniziale.Replace(",", "."))

                If iIDVariableDeveloper = 0 Then
                    ' Procedo con l'inserimento della variabile in archivio
                    sSQL = "INSERT INTO Decisions_Developer (VariableLabel, VariableName, IDGame, IDGroup) VALUES ('" & sDescrizione & "', '" & sVariableName & "', " & IDGame & ", " & iIDGruppo & ") "
                    oDAL.ExecuteNonQuery(sSQL)

                    'Recupero l'ID della variabile appena inserita
                    sSQL = "SELECT ID FROM Decisions_Developer WHERE VariableName = '" & sVariableName & "' AND IDGame = " & IDGame
                    iIDVariableDeveloper = Nni(oDAL.ExecuteScalar(sSQL))
                End If

                If Nz(oRowVariableState("DIM1")).ToUpper <> "" Then ' è una variabile con contenuti multidimensionali
                    If Nz(oRowVariableState("DIM1")).ToUpper = "PLAYERS" Then
                        ' Prima di fare l'inserimento controllo la presenza della variabile in archivio, altrimenti la inserisce n volte
                        sSQL = "SELECT ID FROM Decisions_Developer_Value WHERE IDDecisionDeveloper = " & iIDVariableDeveloper
                        iIDVariableDeveloperValue = Nni(oDAL.ExecuteScalar(sSQL))

                        If iIDVariableDeveloperValue = 0 Then
                            ' Procedo con l'inserimento della definizione nella tabella Decisions_Developer_Value
                            If Nz(oRowVariableState("DIM2")).ToUpper = "ITEMS" Then
                                sSQL = "INSERT INTO Decisions_Developer_Value (IDDecisionDeveloper, Value) VALUES (" & iIDVariableDeveloper & ", '[LIST OF PLAYERS$ITEMS]') "
                            ElseIf Nz(oRowVariableState("DIM2")).ToUpper = "AGE" Then
                                sSQL = "INSERT INTO Decisions_Developer_Value (IDDecisionDeveloper, Value) VALUES (" & iIDVariableDeveloper & ", '[LIST OF PLAYERS$AGE]') "
                            End If
                            oDAL.ExecuteNonQuery(sSQL)

                            ' Recupero l'id appena inserito
                            If Nz(oRowVariableState("DIM2")).ToUpper = "ITEMS" Then
                                sSQL = "SELECT MAX(ID) FROM Decisions_Developer_Value WHERE IDDecisionDeveloper = " & iIDVariableDeveloper & " AND VALUE = '[LIST OF PLAYERS$ITEMS]' "
                            ElseIf Nz(oRowVariableState("DIM2")).ToUpper = "AGE" Then
                                sSQL = "SELECT MAX(ID) FROM Decisions_Developer_Value WHERE IDDecisionDeveloper = " & iIDVariableDeveloper & " AND VALUE = '[LIST OF PLAYERS$AGE]' "
                            End If
                            iIDVariableDeveloperValue = Nni(oDAL.ExecuteScalar(sSQL))
                        End If

                        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                            If Nz(oRowVariableState("DIM2")).ToUpper = "ITEMS" Then
                                ' Con la descrizione recupero anche l'ID del relativo ITEM da salvare
                                sSQL = "SELECT IDVariable FROM vItems WHERE IDGame = " & IDGame & " AND VariableName = '" & sDescrizioneDIM2 & "' "
                                Dim iIDItem As Integer = Nni(oDAL.ExecuteScalar(sSQL))

                                sVariableName = "var_" & Nz(oRowPlayer("TeamName")) & "_" & Nz(oRowVariableState("VariableName")) & "_" & Nz(oRowVariableState("DIM2DESC"))
                                sSQL = "INSERT INTO Decisions_Developer_Value_List (IDDecisionValue, IDPlayer, IDItem, IDPeriod, VariableName, Value) " _
                                     & "VALUES (" & iIDVariableDeveloperValue & ", " & Nni(oRowPlayer("IDTeam")) & ", " & iIDItem & ", " _
                                     & m_IDPeriod & ", '" & sVariableName & "', '" & sValoreIniziale & "') "
                                oDAL.ExecuteNonQuery(sSQL)

                            ElseIf Nz(oRowVariableState("DIM2")).ToUpper = "AGE" Then
                                ' Con la descrizione recupero anche l'ID del relativo ITEM da salvare
                                sSQL = "SELECT ID FROM Age WHERE IDGame = " & IDGame & " AND AgeDescription = '" & sDescrizioneDIM2 & "' "
                                Dim iIDAge As Integer = Nni(oDAL.ExecuteScalar(sSQL))

                                sVariableName = "var_" & Nz(oRowPlayer("TeamName")) & "_" & Nz(oRowVariableState("VariableName")) & "_" & Nz(oRowVariableState("DIM2DESC"))
                                sSQL = "INSERT INTO Decisions_Developer_Value_List (ID, IDPlayer, IDAge, IDPeriod, VariableName, Value) " _
                                     & "VALUES (" & iIDVariableDeveloperValue & ", " & Nni(oRowPlayer("IDTeam")) & ", " & iIDAge & ", " _
                                     & m_IDPeriod & ", '" & sVariableName & "', '" & sValoreIniziale & "') "
                                oDAL.ExecuteNonQuery(sSQL)

                            Else
                                ' Inserisco per il periodo 1 il valore secco della variabile legata al player
                                sSQL = "INSERT INTO Decisions_Developer_Value (IDDecisionDeveloper, Value, IDPlayer, IDPeriod) VALUES (" _
                                     & iIDVariableDeveloper & ", '" & sValoreIniziale & "', " & Nni(oRowPlayer("IDTeam")) & ", 1)"
                                oDAL.ExecuteScalar(sSQL)
                            End If
                        Next
                        sVariableName = ""

                    ElseIf Nz(oRowVariableState("DIM1")).ToUpper = "ITEMS" Then
                        ' Prima di fare l'inserimento controllo la presenza della variabile in archivio, altrimenti la inserisce n volte
                        sSQL = "SELECT ID FROM Decisions_Developer_Value WHERE IDDecisionDeveloper = " & iIDVariableDeveloper
                        iIDVariableDeveloperValue = Nni(oDAL.ExecuteScalar(sSQL))

                        If iIDVariableDeveloperValue = 0 Then
                            ' Procedo con l'inserimento della definizione nella tabella Decisions_Developer_Value
                            If Nz(oRowVariableState("DIM2")).ToUpper = "PLAYERS" Then
                                sSQL = "INSERT INTO Decisions_Developer_Value (IDDecisionDeveloper, Value) VALUES (" & iIDVariableDeveloper & ", '[LIST OF ITEMS$PLAYERS]') "
                            ElseIf Nz(oRowVariableState("DIM2")).ToUpper = "AGE" Then
                                sSQL = "INSERT INTO Decisions_Developer_Value (IDDecisionDeveloper, Value) VALUES (" & iIDVariableDeveloper & ", '[LIST OF ITEMS$AGE]') "
                            End If
                            oDAL.ExecuteNonQuery(sSQL)

                            ' Recupero l'id appena inserito
                            If Nz(oRowVariableState("DIM2")).ToUpper = "AGE" Then
                                sSQL = "SELECT MAX(ID) FROM Decisions_Developer_Value WHERE IDDecisionDeveloper = " & iIDVariableDeveloper & " AND VALUE = '[LIST OF ITEMS$PLAYERS]' "
                            ElseIf Nz(oRowVariableState("DIM2")).ToUpper = "PLAYERS" Then
                                sSQL = "SELECT MAX(ID) FROM Decisions_Developer_Value WHERE IDDecisionDeveloper = " & iIDVariableDeveloper & " AND VALUE = '[LIST OF ITEMS$AGE]' "
                            End If
                            iIDVariableDeveloperValue = Nni(oDAL.ExecuteScalar(sSQL))
                        End If

                        For Each oRowItem As DataRow In m_DataTableItems.Rows
                            If Nz(oRowVariableState("DIM2")).ToUpper = "PLAYERS" Then
                                ' TODO: Verificheremo in futuro come comportarsi con questa importazione

                            ElseIf Nz(oRowVariableState("DIM2")).ToUpper = "AGE" Then
                                ' TODO: Verificheremo in futuro come comportarsi con questa importazione

                            Else
                                If Nz(oRowItem("VariableName")) = sDescrizioneDIM2 Then
                                    ' Inserisco per il periodo 1 il valore secco della variabile legata all'item
                                    sSQL = "INSERT INTO Decisions_Developer_Value (IDDecisionDeveloper, Value, IDItem, IDPeriod) VALUES (" _
                                         & iIDVariableDeveloper & ", '" & sValoreIniziale & "', " & Nni(oRowItem("IDVariable")) & ", 1)"
                                    oDAL.ExecuteScalar(sSQL)
                                End If

                            End If
                        Next
                        sVariableName = ""

                    ElseIf Nz(oRowVariableState("DIM1")).ToUpper = "AGE" Then
                        ' TODO: Verificheremo in futuro come comportarsi con questa importazione

                    End If

                Else
                    ' variabile a valore secco
                    ' Procedo con l'inserimento della definizione nella tabella Decisions_Developer_Value
                    sSQL = "INSERT INTO Decisions_Developer_Value (IDDecisionDeveloper, Value, IDPeriod) VALUES (" & iIDVariableDeveloper & ", '" & sValoreIniziale & "', 1) "
                    oDAL.ExecuteNonQuery(sSQL)
                    sVariableName = ""
                End If
            Next

            bRet = True

            oDAL.GetConnObject.Close()
            oDAL = Nothing

        Catch ex As Exception
            g_MessaggioErrore &= "VariableDeveloperTable_Align -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try

        Return bRet
    End Function

    Private Function HandleVerifyTitle(Title As String, IDGame As Integer) As Integer
        Dim sSQL As String
        Dim iIDTitle As Integer

        g_DAL = New DBHelper

        ' Ho una variabile valida, procedo con l'aggiornamento
        ' Controllo che esista la descrizione per il titolo altrimenti la creo
        sSQL = "SELECT ID FROM Decisions_Boss_Title WHERE Title = '" & Title.Replace("'", " ").Trim & "' AND IDGame = " & IDGame
        iIDTitle = g_DAL.ExecuteScalar(sSQL)
        If iIDTitle = 0 Then
            sSQL = "INSERT INTO Decisions_Boss_Title (Title, IDGame) VALUES ('" & Title.Replace("'", " ").Trim & "', " & IDGame & ") "
            g_DAL.ExecuteNonQuery(sSQL)
            ' Recupero l'ultimo ID Inserito
            sSQL = "SELECT MAX(ID) FROM Decisions_Boss_Title WHERE Title = '" & Title.Replace("'", " ").Trim & "' AND IDGame = " & IDGame
            iIDTitle = g_DAL.ExecuteScalar(sSQL)
        End If
        Return iIDTitle

    End Function

    Private Function HandleVerifySubTitle(IDTitle As Integer, SubTitle As String) As Integer
        Dim sSQL As String
        Dim iIDSubTitle As Integer

        g_DAL = New DBHelper
        ' Ho una variabile valida, procedo con l'aggiornamento
        ' Controllo che esista la descrizione per il titolo altrimenti la creo
        sSQL = "SELECT ID FROM Decisions_Boss_SubTitle WHERE IDTitle = " & IDTitle & " AND SubTitle = '" & SubTitle.Replace("'", " ").Trim & "' "
        iIDSubTitle = g_DAL.ExecuteScalar(sSQL)
        If iIDSubTitle = 0 Then
            sSQL = "INSERT INTO Decisions_Boss_SubTitle (IDTitle, SubTitle) VALUES (" & IDTitle & ", '" & SubTitle.Replace("'", " ").Trim & "') "
            g_DAL.ExecuteNonQuery(sSQL)

            ' Recupero l'ultimo ID Inserito
            sSQL = "SELECT MAX(ID) FROM Decisions_Boss_SubTitle WHERE IDTitle = " & IDTitle & " "
            iIDSubTitle = g_DAL.ExecuteScalar(sSQL)
        End If
        Return iIDSubTitle

    End Function

    Public Function VariableBossTable_Align(IDGame As Integer) As Boolean
        Dim sSQL As String
        Dim oDAL As New DBHelper
        Dim sVariableName As String
        Dim sValoreIniziale As String
        Dim sDescrizione As String
        Dim sDescrizioneDIM2 As String
        Dim sTitolo As String
        Dim sSottoTitolo As String
        Dim iIDTitolo As Integer
        Dim iIDSottotitolo As Integer
        Dim iIDDecisionBoss As Integer
        Dim bRet As Boolean = False
        Dim iIDVariableExist As Integer
        Dim iGruppo As Integer

        Try
            g_DAL = New DBHelper

            sSQL = "SELECT * FROM Variables_Initial_Values VIV " _
                 & "WHERE IDGame = " & IDGame & " AND VIV.Gruppo = 101 AND VIV.Tipo = 'T' "
            Dim oDTVariablesBoss As DataTable = oDAL.ExecuteDataTable(sSQL)

            For Each oRowVariableBoss As DataRow In oDTVariablesBoss.Rows
                sVariableName = Nz(oRowVariableBoss("VariableName"))
                sValoreIniziale = Nz(oRowVariableBoss("ValoreIniziale"))
                sDescrizione = Nz(oRowVariableBoss("Descrizione"))
                sDescrizioneDIM2 = Nz(oRowVariableBoss("DIM2DESC"))
                sTitolo = Nz(oRowVariableBoss("Title"))
                sSottoTitolo = Nz(oRowVariableBoss("SubTitle"))
                iGruppo = Nni(oRowVariableBoss("IDGruppo"))

                ' Controllo la presenza della variabile all'interno della tabella generale delle variabili
                iIDVariableExist = VerifiyVariableExists(sVariableName, IDGame, iGruppo, sDescrizione, 1, 1, 1, 0, 0, sValoreIniziale.Replace(",", "."))

                ' Verifico l'esistenza del titolo
                ' Anche se vuoto lo inserisco lo stesso, almeno ho un riferimento valido per l'inserimento dei successivi record
                iIDTitolo = HandleVerifyTitle(sTitolo, IDGame)
                iIDSottotitolo = HandleVerifySubTitle(iIDTitolo, sSottoTitolo)

                ' Controllo che non esista già la variabile che sto per inserire
                sSQL = "SELECT ID FROM Decisions_Boss WHERE IDSubTitle = " & iIDSottotitolo & " AND Decision = '" & sVariableName.Trim.Replace("'", " ") & "' " _
                     & " AND IDDataType = 3 AND DecisionLabel = '" & sDescrizione.Trim.Replace("'", " ") & "' AND IDGame = " & IDGame
                iIDDecisionBoss = Nni(oDAL.ExecuteScalar(sSQL))

                If iIDDecisionBoss = 0 Then
                    sSQL = "INSERT INTO Decisions_Boss (IDSubTitle, Decision, IDDataType, DecisionLabel, IDGame) VALUES (" _
                     & iIDSottotitolo & ", '" & sVariableName.Trim.Replace("'", " ") & "', 3, " _
                     & "'" & sDescrizione.Trim.Replace("'", " ") & "', " & IDGame & ") "
                    g_DAL.ExecuteNonQuery(sSQL)

                    ' Recupero l'ultimo ID inserito
                    sSQL = "SELECT MAX(ID) FROM Decisions_Boss WHERE IDGame = " & IDGame
                    iIDDecisionBoss = Nni(g_DAL.ExecuteScalar(sSQL))
                End If

                ' Controllo il tipo di dimensione 1, se ITEM o Player
                If Nz(oRowVariableBoss("DIM1")).ToUpper = "ITEMS" Then
                    Dim iIDDecisionBossValue As Integer
                    Dim iIDDecisionBossValueList As Integer

                    ' Conotrollo l'esistenza della decisione
                    sSQL = "SELECT ID FROM Decisions_Boss_Value WHERE IDDecisionBoss =" & iIDDecisionBoss & " AND Value = '[List of items]' AND IDPeriod = 1 "
                    iIDDecisionBossValue = Nni(oDAL.ExecuteScalar(sSQL))

                    If iIDDecisionBossValue = 0 Then
                        sSQL = "INSERT INTO Decisions_Boss_Value (IDDecisionBoss, Value, IDPeriod) VALUES (" & iIDDecisionBoss & ", '[List of items]', 1) "
                        g_DAL.ExecuteNonQuery(sSQL)

                        sSQL = "SELECT MAX(ID) FROM Decisions_Boss_Value WHERE IDDecisionBoss = " & iIDDecisionBoss
                        iIDDecisionBossValue = Nni(g_DAL.ExecuteScalar(sSQL))
                    End If
                    Dim sNomeItem As String = sVariableName & "_" & sDescrizioneDIM2

                    ' Controllo se esiste la variabile nella lista
                    sSQL = "SELECT ID FROM Decisions_Boss_Value_List WHERE IdDecisionBossValue =" & iIDDecisionBossValue & " AND VariableName = '" & sNomeItem & "' AND IDPeriod = 1 AND Value = '" & sValoreIniziale & "' "
                    iIDDecisionBossValueList = Nni(oDAL.ExecuteScalar(sSQL))

                    If iIDDecisionBossValueList = 0 Then
                        sSQL = "INSERT INTO Decisions_Boss_Value_List (IdDecisionBossValue, VariableName, IDPeriod, Value) VALUES(" _
                     & iIDDecisionBossValue & ", '" & sNomeItem & "', 1, '" & sValoreIniziale & "') "
                        oDAL.ExecuteNonQuery(sSQL)
                    End If

                ElseIf Nz(oRowVariableBoss("DIM1")).ToUpper = "PLAYERS" Then
                    Dim iIDDecisionBossValue As Integer
                    Dim iIDDecisionBossValueList As Integer

                    ' Carico tutti i Players presenti in archivio
                    Dim oDTTeams As DataTable = LoadTeamsGame(IDGame)
                    Dim sNomeTeam As String

                    ' Conotrollo l'esistenza della decisione
                    sSQL = "SELECT ID FROM Decisions_Boss_Value WHERE IDDecisionBoss =" & iIDDecisionBoss & " AND Value = '[List of items]' AND IDPeriod = 1 "
                    iIDDecisionBossValue = Nni(oDAL.ExecuteScalar(sSQL))

                    If iIDDecisionBossValue = 0 Then
                        sSQL = "INSERT INTO Decisions_Boss_Value (IDDecisionBoss, Value, IDPeriod) VALUES (" & iIDDecisionBoss & ", '[List of players]', 1) "
                        g_DAL.ExecuteNonQuery(sSQL)

                        sSQL = "Select MAX(ID) FROM Decisions_Boss_Value WHERE IDDecisionBoss = " & iIDDecisionBoss
                        iIDDecisionBossValue = Nni(g_DAL.ExecuteScalar(sSQL))
                    End If

                    For Each oRowTeams As DataRow In oDTTeams.Rows
                        sNomeTeam = sVariableName & "_" & Nz(oRowTeams("TeamName"))

                        ' Controllo la presenza del valore nella tabella delle liste
                        sSQL = "SELECT ID FROM Decisions_Boss_Value_List WHERE IdDecisionBossValue = " & iIDDecisionBossValueList & " AND VariableName =  '" & sNomeTeam & "' " _
                             & "AND IDPeriod = 1 AND Value = '" & sValoreIniziale & "' "
                        iIDDecisionBossValueList = Nni(oDAL.ExecuteScalar(sSQL))

                        If iIDDecisionBossValueList = 0 Then
                            sSQL = "INSERT INTO Decisions_Boss_Value_List (IdDecisionBossValue, VariableName, IDPeriod, Value) VALUES(" _
                                 & iIDDecisionBossValue & ", '" & sNomeTeam & "', 1, '" & sValoreIniziale & "') "
                            oDAL.ExecuteNonQuery(sSQL)
                        End If

                    Next

                Else ' Valore secco, senza nessun controllo su altri oggetti del database
                    sSQL = "INSERT INTO Decisions_Boss_Value (IDDecisionBoss, Value, IDPeriod) VALUES (" & iIDDecisionBoss & ", '" & sValoreIniziale & "', 1) "
                    g_DAL.ExecuteNonQuery(sSQL)
                End If
            Next

            bRet = True

        Catch ex As Exception
            g_MessaggioErrore &= "VariableBossTable_Align -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try

        Return bRet
    End Function

#End Region

    Private Sub SettingDataTable(IDGame As Integer, IDPeriod As Integer)
        ' Procedo con il caricamento dei datatable necessari
        ' Datatable dei player
        LoadPlayersTeam(IDGame)

        ' Datatable degli item
        ' Recupero gli Item da usare
        m_DataTableItems = HandleGetItemsPeriodVariables(IDGame, "")

        ' Carico la tabella degli AGE con i relativi valori per player
        LoadAge(IDGame)

        ' Recupero le variabili di stato 
        LoadVariablesState(IDGame)
        LoadVariablesStateValue(IDPeriod)
        LoadVariablesStateValueList(IDPeriod)

        ' Decisioni dei players
        LoadDecisionsPlayer(IDGame)
        LoadDecisionsPlayerValue(IDPeriod)
        LoadDecisionsPlayerValueList(IDPeriod)

        ' Variabili calcolate
        LoadVariableCalculateDefine(IDGame)
        LodVariableCalculateValue(IDGame)

        ' Carico la lista completa delle variabili
        LoadVariables(IDGame)

        ' Carico tutte le variabili del boss
        LoadVariablesDecisionsBoss(IDGame)
        LoadVariablesDecisionsBossValue(IDPeriod)
        LoadVariablesDecisionsBossValueList(IDPeriod)

        ' Carico tutte le variabili del developer
        LoadVariablesDecisionsDeveloper(IDGame)
        LoadVariablesDecisionsDeveloperValue(IDPeriod)
        LoadVariablesDecisionsDeveloperValueList(IDPeriod)

        ' Carico le variabili generiche che posso usare durante l'elaborazione
        m_LunghezzaPeriodo = Nz(GetVariableDeveloper(m_IDGame, "PeriodLength", 0, 0), "0")

        ' Recupero il periodo precedente a quello di calcolo attuale, mi serve per recuperare le variabili al periodo precedente
        Dim sSQL As String

        g_DAL = New DBHelper
        sSQL = "SELECT MIN(ID) FROM BGOL_Periods WHERE NumStep > " & m_CurrentStep & " AND IDGame = " & m_IDGame
        m_IDPeriodNext = g_DAL.ExecuteScalar(sSQL)
        SQLConnClose(g_DAL.GetConnObject)

        '*************************************************************************************************************************************
        ' Indicizzo le tabelle

        ' PLAYERS
        m_DataTablePlayers.PrimaryKey = New DataColumn() {m_DataTablePlayers.Columns("IDTeam"), m_DataTablePlayers.Columns("TeamName")}

        ' ITEMS
        m_DataTableItems.PrimaryKey = New DataColumn() {m_DataTableItems.Columns("ID"), m_DataTableItems.Columns("IDVariable"), m_DataTableItems.Columns("VariableName"), m_DataTableItems.Columns("VariableNameDefault")}

        ' AGE
        m_DataTableAge.PrimaryKey = New DataColumn() {m_DataTableAge.Columns("ID"), m_DataTableAge.Columns("AgeDescription")}

        ' VARIABLES STATE
        m_DataTableVariablesState.PrimaryKey = New DataColumn() {m_DataTableVariablesState.Columns("Name"), m_DataTableVariablesState.Columns("IDGame"),
                                                                 m_DataTableVariablesState.Columns("IDAge"), m_DataTableVariablesState.Columns("ID")}

        m_DataTableVariablesStateValue.PrimaryKey = New DataColumn() {m_DataTableVariablesStateValue.Columns("IDVariable"), m_DataTableVariablesStateValue.Columns("ID")}

        m_DataTableVariablesStateValueList.PrimaryKey = New DataColumn() {m_DataTableVariablesStateValueList.Columns("IDPeriod"), m_DataTableVariablesStateValueList.Columns("VariableName"),
                                                                          m_DataTableVariablesStateValueList.Columns("IDPlayer"), m_DataTableVariablesStateValueList.Columns("ID")}

        ' VARIABLE DECISIONS PLAYER
        m_DataTableDecisionsPlayers.PrimaryKey = New DataColumn() {m_DataTableDecisionsPlayers.Columns("ID"), m_DataTableDecisionsPlayers.Columns("VariableName"),
                                                                   m_DataTableDecisionsPlayers.Columns("IDGame")}

        m_DataTableDecisionsPlayersValue.PrimaryKey = New DataColumn() {m_DataTableDecisionsPlayersValue.Columns("IDDecisionPlayer"), m_DataTableDecisionsPlayersValue.Columns("ID"),
                                                                        m_DataTableDecisionsPlayersValue.Columns("variableValue")}

        m_DataTableDecisionsPlayersValueList.PrimaryKey = New DataColumn() {m_DataTableDecisionsPlayersValueList.Columns("ID"), m_DataTableDecisionsPlayersValueList.Columns("IDDecisionValue"),
                                                                            m_DataTableDecisionsPlayersValueList.Columns("IDPeriod"),
                                                                            m_DataTableDecisionsPlayersValueList.Columns("IDPlayer"), m_DataTableDecisionsPlayersValueList.Columns("IDItem")}

        ' VARIABLES DECISIONS BOSS
        m_DataTableVariablesDecisionsBoss.PrimaryKey = New DataColumn() {m_DataTableVariablesDecisionsBoss.Columns("ID"), m_DataTableVariablesDecisionsBoss.Columns("Decision"),
                                                                         m_DataTableVariablesDecisionsBoss.Columns("IDGame")}

        m_DataTableVariablesDecisionsBossValue.PrimaryKey = New DataColumn() {m_DataTableVariablesDecisionsBossValue.Columns("IDDecisionBoss"),
                                                                              m_DataTableVariablesDecisionsBossValue.Columns("IDPeriod"), m_DataTableVariablesDecisionsBossValue.Columns("ID")}

        m_DataTableVariablesDecisionsBossValueList.PrimaryKey = New DataColumn() {m_DataTableVariablesDecisionsBossValueList.Columns("IdDecisionBossValue"),
                                                                                  m_DataTableVariablesDecisionsBossValueList.Columns("VariableName"),
                                                                                  m_DataTableVariablesDecisionsBossValueList.Columns("IDPeriod"),
                                                                                  m_DataTableVariablesDecisionsBossValueList.Columns("Id")}

        ' VARIABLES DECISIONS DEVELOPER
        m_DataTableDecisionsDeveloper.PrimaryKey = New DataColumn() {m_DataTableDecisionsDeveloper.Columns("IDGame"), m_DataTableDecisionsDeveloper.Columns("ID"),
                                                                     m_DataTableDecisionsDeveloper.Columns("VariableName")}

        m_DataTableDecisionsDeveloperValue.PrimaryKey = New DataColumn() {m_DataTableDecisionsDeveloperValue.Columns("ID"), m_DataTableDecisionsDeveloperValue.Columns("IDDecisionDeveloper"),
                                                                          m_DataTableDecisionsDeveloperValue.Columns("IDPeriod"), m_DataTableDecisionsDeveloperValue.Columns("Value")}

        m_DataTableDecisionsDeveloperValueList.PrimaryKey = New DataColumn() {m_DataTableDecisionsDeveloperValueList.Columns("ID"), m_DataTableDecisionsDeveloperValueList.Columns("IDDecisionValue"),
                                                                              m_DataTableDecisionsDeveloperValueList.Columns("IDPeriod"), m_DataTableDecisionsDeveloperValueList.Columns("VariableName")}

        ' VARIBLES CALCULATE 
        m_DataTableVariableCalculateDefine.PrimaryKey = New DataColumn() {m_DataTableVariableCalculateDefine.Columns("ID"), m_DataTableVariableCalculateDefine.Columns("VariableName"),
                                                                          m_DataTableVariableCalculateDefine.Columns("IDGame")}

        m_DataTableVariableCalculateValue.PrimaryKey = New DataColumn() {m_DataTableVariableCalculateValue.Columns("ID"), m_DataTableVariableCalculateValue.Columns("IDVariable"),
                                                                        m_DataTableVariableCalculateValue.Columns("IDPlayer"), m_DataTableVariableCalculateValue.Columns("IDPeriod"),
                                                                        m_DataTableVariableCalculateValue.Columns("IDItem"), m_DataTableVariableCalculateValue.Columns("IDAge")}

    End Sub

    Public Sub New(IDGame As Integer, IDPeriod As Integer, Simulation As Boolean, CurrentStep As Integer)
        m_IDGame = IDGame
        m_IDPeriod = IDPeriod

        m_Simulation = Simulation

        ' Recupero lo step del periodo corremte
        m_CurrentStep = CurrentStep

        SettingDataTable(IDGame, IDPeriod)

    End Sub

#Region "Funzioni di preparazione dati e salvataggi in archivio"
    Private Sub LoadVariables(IDGame As Integer)
        Dim sSQL As String = "SELECT * FROM Variables WHERE IDGame = " & IDGame

        m_DataTableVariables = g_DAL.ExecuteDataTable(sSQL)

    End Sub

    Private Sub LoadVariableCalculateDefine(IDGame As Integer)
        Dim sSQL As String = "SELECT * FROM Variables_Calculate_Define WHERE IDGame = " & IDGame

        m_DataTableVariableCalculateDefine = g_DAL.ExecuteDataTable(sSQL)

        m_DataTableVariableCalculateDefine = g_DAL.ExecuteDataTable(sSQL)
        m_DataTableVariableCalculateDefine.Columns("ID").AutoIncrement = True
        m_DataTableVariableCalculateDefine.Columns("ID").AutoIncrementSeed = 1
        m_DataTableVariableCalculateDefine.Columns("ID").AutoIncrementStep = 1
    End Sub

    Private Sub LodVariableCalculateValue(IDGame As Integer)
        Dim sSQL As String
        sSQL = "SELECT MIN(C.ID) AS ID, C.IDVariable, C.IDPlayer, C.IDPeriod, C.IDItem, C.IDAge, C.Value, C.Simulation " _
             & "FROM Variables_Calculate_Define V " _
             & "INNER JOIN Variables_Calculate_Value C ON V.Id = C.IDVariable " _
             & "WHERE IDGame = " & IDGame _
             & " AND ISNULL(IDPeriod, " & m_IDPeriod & ") = " & m_IDPeriod _
             & " GROUP BY C.IDVariable, C.IDPlayer, C.IDPeriod, C.IDItem, C.IDAge, C.Value, C.Simulation "

        m_DataTableVariableCalculateValue = g_DAL.ExecuteDataTable(sSQL)
        m_DataTableVariableCalculateValue.Columns("ID").AutoIncrement = True
        m_DataTableVariableCalculateValue.Columns("ID").AutoIncrementSeed = 1
        m_DataTableVariableCalculateValue.Columns("ID").AutoIncrementStep = 1
    End Sub

    ''' <summary>
    ''' Caricamento dei players partecipanti al game, soltanto i team, senza nessun dato legato al singolo player che lo compone
    ''' </summary>
    Private Sub LoadPlayersTeam(IDGame As Integer)
        Dim sSQL As String

        ' Carico tutti i player escluso quello legato al team M&U e solo player
        sSQL = "SELECT DISTINCT T.TeamName, T.ID As IDTeam " _
             & "FROM BGOL_PLAYERS P " _
             & "INNER JOIN BGOL_Players_Games PG On P.ID = PG.IDPlayer " _
             & "LEFT JOIN BGOL_Players_Teams_Games PTG On P.ID = PTG.IdPlayer And PG.IDGame = PTG.IdGame " _
             & "LEFT JOIN BGOL_Teams T On PTG.IdTeam = T.Id " _
             & "WHERE PG.IDGame = " & IDGame & " AND PG.IDRole = 'P' "
        ' AND T.TeamName NOT LIKE'%M&U%' 
        m_DataTablePlayers = g_DAL.ExecuteDataTable(sSQL)

    End Sub

    ''' <summary>
    ''' Caricamento delle varibili di stato
    ''' </summary>
    ''' <param name="IDGame"></param>
    Private Sub LoadVariablesState(IDGame As Integer)
        Dim sSQL As String = "SELECT * FROM Variables_State WHERE IDGame = " & IDGame

        m_DataTableVariablesState = g_DAL.ExecuteDataTable(sSQL)
        m_DataTableVariablesState.Columns("ID").AutoIncrement = True
        m_DataTableVariablesState.Columns("ID").AutoIncrementSeed = 1
        m_DataTableVariablesState.Columns("ID").AutoIncrementStep = 1
    End Sub

    ''' <summary>
    ''' Caricamento dei valori delle varibili di stato in un determinato periodo del game
    ''' </summary>
    ''' <param name="IDPeriod"></param>
    Private Sub LoadVariablesStateValue(IDPeriod As Integer)
        Dim sSQL As String = "SELECT ID, IDVariable, Value, IDPeriod, IDPlayer, " _
                           & "IDItem, IDAge, Simulation " _
                           & "FROM Variables_State_Value WHERE ISNULL(IDPeriod, " & IDPeriod & ") = " & IDPeriod

        m_DataTableVariablesStateValue = g_DAL.ExecuteDataTable(sSQL)
        m_DataTableVariablesStateValue.Columns("ID").AutoIncrement = True
        m_DataTableVariablesStateValue.Columns("ID").AutoIncrementSeed = 1
        m_DataTableVariablesStateValue.Columns("ID").AutoIncrementStep = 1
    End Sub

    ''' <summary>
    ''' Caricamento dei valori delle eventuali variabili di stato legate ad items, players o qualunque altro oggetto multiplo
    ''' </summary>
    ''' <param name="IDPeriod"></param>
    Private Sub LoadVariablesStateValueList(IDPeriod As Integer)
        Dim sSQL As String = "SELECT * FROM Variables_State_Value_List WHERE ISNULL(IDPeriod, " & IDPeriod & ") = " & IDPeriod

        m_DataTableVariablesStateValueList = g_DAL.ExecuteDataTable(sSQL)
        m_DataTableVariablesStateValueList.Columns("ID").AutoIncrement = True
        m_DataTableVariablesStateValueList.Columns("ID").AutoIncrementSeed = 1
        m_DataTableVariablesStateValueList.Columns("ID").AutoIncrementStep = 1
    End Sub

    ''' <summary>
    ''' Caricamento delle decisioni dei singoli player
    ''' </summary>
    ''' <param name="IDGame"></param>
    Private Sub LoadDecisionsPlayer(IDGame As Integer)
        Dim sSQL As String = "SELECT Id, IDGame, VariableName, VariableLabelGroup, VariableLabel, IDDataType, IDVariable " _
                           & "FROM Decisions_Players WHERE IDGame = " & m_IDGame

        m_DataTableDecisionsPlayers = g_DAL.ExecuteDataTable(sSQL)
        m_DataTableDecisionsPlayers.Columns("ID").AutoIncrement = True
        m_DataTableDecisionsPlayers.Columns("ID").AutoIncrementSeed = 1
        m_DataTableDecisionsPlayers.Columns("ID").AutoIncrementStep = 1
    End Sub

    ''' <summary>
    ''' Caricamento dei valori delle decisioni dei player
    ''' </summary>
    ''' <param name="IDPeriod"></param>
    Private Sub LoadDecisionsPlayerValue(IDPeriod As Integer)
        Dim sSQL As String = "SELECT Id, IDDecisionPlayer, IDPeriod, VariableValue, IDPlayer, Simulation " _
                           & "FROM Decisions_Players_Value WHERE ISNULL(IDPeriod, " & IDPeriod & ") = " & IDPeriod

        m_DataTableDecisionsPlayersValue = g_DAL.ExecuteDataTable(sSQL)
        m_DataTableDecisionsPlayersValue.Columns("ID").AutoIncrement = True
        m_DataTableDecisionsPlayersValue.Columns("ID").AutoIncrementSeed = 1
        m_DataTableDecisionsPlayersValue.Columns("ID").AutoIncrementStep = 1
    End Sub

    ''' <summary>
    ''' Caricamento dei valori delle decisioni dei player per quei tipi a definizione di tipo LIST OF
    ''' </summary>
    ''' <param name="IDPeriod"></param>
    Private Sub LoadDecisionsPlayerValueList(IDPeriod As Integer)
        Dim sSQL As String = "SELECT Id, IDDecisionValue, ISNULL(IDPlayer, -1) AS IDPlayer, ISNULL(IDPeriod, -1) AS IDPeriod, VariableName, " _
                           & "Value, ISNULL(IDItem, -1) AS IDItem, ISNULL(IDAge, -1) AS IDAge, Simulation " _
                           & "FROM Decisions_Players_Value_List WHERE ISNULL(IDPeriod, " & IDPeriod & ") = " & IDPeriod

        m_DataTableDecisionsPlayersValueList = g_DAL.ExecuteDataTable(sSQL)
        m_DataTableDecisionsPlayersValueList.Columns("ID").AutoIncrement = True
        m_DataTableDecisionsPlayersValueList.Columns("ID").AutoIncrementSeed = 1
        m_DataTableDecisionsPlayersValueList.Columns("ID").AutoIncrementStep = 1
    End Sub

    ''' <summary>
    ''' Controllo l'esistenza della variabile nel database, se non esiste la creo
    ''' </summary>
    ''' 
    Private Function CheckVariableExists(NomeVariabile As String, IDGame As Integer) As Integer
        Dim iIDCategory As Integer
        Dim iID As Integer

        ' Recupero la categoria di appartenenza della variabile 
        Dim drVariables As DataRow() = m_DataTableVariables.Select("VariableName = '" & NomeVariabile & "' ")
        If drVariables.Length > 0 Then
            iIDCategory = Nni(drVariables(0)("IDCategory"))
        Else
            iIDCategory = 0
        End If

        Dim drVariableCalculateDefine As DataRow() = m_DataTableVariableCalculateDefine.Select("VariableName = '" & NomeVariabile & "' AND IDGame = " & IDGame)
        If drVariableCalculateDefine.Length > 0 Then
            iID = drVariableCalculateDefine(0)("ID")
        Else
            iID = 0
        End If

        If iID <= 0 Then ' la variabile non esiste, procedo con la creazione
            '' Recupero l'ID di inserimento appena eseguito
            Dim drNewRow As DataRow = m_DataTableVariableCalculateDefine.NewRow()
            drNewRow("VariableName") = NomeVariabile
            drNewRow("IDGame") = IDGame
            drNewRow("IDCategory") = iIDCategory
            m_DataTableVariableCalculateDefine.Rows.Add(drNewRow)
            drNewRow = Nothing

            m_DataTableVariableCalculateDefine.AcceptChanges()
        End If

        Return iID

    End Function

    ''' <summary>
    ''' Salvo il valore della variabile apena calcolata per il player nel periodo specifico del game
    ''' </summary>
    ''' <param name="IDVariable"></param>
    ''' <param name="IDPlayer"></param>
    ''' <param name="IDItem"></param>
    ''' <param name="Value"></param>
    Private Sub SaveVariableValue(IDVariable As Integer, IDPlayer As Integer, IDItem As Integer, IDAge As Integer, Value As String)
        ' Controllo l'eventuale esistenza della varibile per ricalcoli da fare
        Dim drVariables_Calculate_Value As DataRow() = m_DataTableVariableCalculateValue.Select("IDVariable = " & IDVariable & " AND IDPlayer = " & IDPlayer & " " _
                                                     & "AND IDPeriod = " & m_IDPeriod & " AND IDItem = " & IDItem & " AND IDAge = " & IDAge)
        Dim iID As Integer

        If drVariables_Calculate_Value.Length > 0 Then iID = drVariables_Calculate_Value(0)("ID")

        If iID > 0 Then ' Vado in update
            Dim drUpdate As DataRow() = m_DataTableVariableCalculateValue.Select("ID = " & iID)
            drUpdate(0)("IDVariable") = IDVariable
            drUpdate(0)("IDPlayer") = IDPlayer
            drUpdate(0)("IDPeriod") = m_IDPeriod
            drUpdate(0)("IDItem") = IDItem
            drUpdate(0)("Value") = Value
            drUpdate(0)("IDAge") = IDAge
            drUpdate(0)("Simulation") = Boolean_To_Bit(m_Simulation)
            m_DataTableVariableCalculateValue.AcceptChanges()

        Else ' Procedo con l'inserimento
            Dim drInsert As DataRow = m_DataTableVariableCalculateValue.NewRow
            drInsert("IDVariable") = IDVariable
            drInsert("IDPlayer") = IDPlayer
            drInsert("IDPeriod") = m_IDPeriod
            drInsert("IDItem") = IDItem
            drInsert("Value") = Value
            drInsert("IDAge") = IDAge
            drInsert("Simulation") = Boolean_To_Bit(m_Simulation)
            m_DataTableVariableCalculateValue.Rows.Add(drInsert)
            m_DataTableVariableCalculateValue.AcceptChanges()
        End If

    End Sub

    Private Sub SaveVariableBossValue(NomeVariabile As String, IDPlayer As Integer, IDItem As Integer, IDAge As Integer, Value As String, NomeVariabileLista As String)
        Dim sItemName As String = ""
        Dim sAgeName As String = ""
        Dim sTeamName As String = ""
        Dim sNomeVariabile As String = ""
        Dim iIDDecisionBoss As Integer
        Dim sValore As String
        Dim iIDDecisionBossValue As Integer

        ' Se ho l'ID del team mi recupero il nome
        If IDPlayer > 0 Then sTeamName = GetTeamName(IDPlayer)

        ' Se ho l'ID Item recupero il nome 
        If IDItem > 0 Then sItemName = GetItemName(IDItem)

        ' Se ho l'ID Age ne recupero il nome/descrizione
        If IDAge > 0 Then sAgeName = GetAgeName(IDAge)

        ' Controllo l'esistenza della variabile e ne recupero l'ID
        iIDDecisionBoss = 0
        Dim drDecisions_Boss As DataRow() = m_DataTableVariablesDecisionsBoss.Select("Decision = '" & NomeVariabile & "' AND IDGame = " & m_IDGame)
        If drDecisions_Boss.Length > 0 Then
            iIDDecisionBoss = Nni(drDecisions_Boss(0)("ID"))
        End If

        ' Controllo la presenza di valori nella tabella Decision Boss Value, se ci sono vado in UPDATE, altrimenti controllo nella Decision Boss Value List
        Dim drDecisions_Boss_Value As DataRow() = m_DataTableVariablesDecisionsBossValue.Select("IDDecisionBoss = " & iIDDecisionBoss & " AND ISNULL(IDPeriod, " & m_IDPeriod & ") = " & m_IDPeriod)
        sValore = ""
        If drDecisions_Boss_Value.Length > 0 Then
            sValore = drDecisions_Boss_Value(0)("VALUE").ToString
        End If

        If sValore.ToUpper.Contains("LIST OF") Then
            ' Recupero l'ID e vado nella tabella di dettaglio
            drDecisions_Boss_Value = m_DataTableVariablesDecisionsBossValue.Select("IDDecisionBoss = " & iIDDecisionBoss & " AND ISNULL(IDPeriod, " & m_IDPeriod & ") = " & m_IDPeriod)
            If drDecisions_Boss_Value.Length > 0 Then
                iIDDecisionBossValue = drDecisions_Boss_Value(0)("ID")
            Else
                iIDDecisionBossValue = 0
            End If

            If NomeVariabileLista = "" Then
                ' Recupero i dati della tabella Decision Boss Value List
                If IDPlayer > 0 Then
                    sNomeVariabile = NomeVariabile & "_" & sTeamName
                End If
                If IDItem > 0 Then
                    sNomeVariabile = NomeVariabile & "_" & sItemName
                End If
                If IDAge > 0 Then
                    sNomeVariabile = NomeVariabile & "_" & sAgeName
                End If
            Else
                sNomeVariabile = NomeVariabileLista
            End If

            Dim drDecisions_Boss_Value_List As DataRow() = m_DataTableVariablesDecisionsBossValueList.Select("IDDecisionBossValue = " & iIDDecisionBossValue _
                                                                                                             & " AND ISNULL(IDPeriod, " & m_IDPeriodNext & ") = " & m_IDPeriodNext _
                                                                                                             & " AND VariableName = '" & sNomeVariabile & "' ")
            'Dim oDTValue As DataTable = g_DAL.ExecuteDataTable(sSQL)

            If drDecisions_Boss_Value_List.Length > 0 Then
                For Each oRow As DataRow In drDecisions_Boss_Value_List
                    oRow("Value") = Nn(oRow("Value")) + Nn(Value)
                    oRow("Simulation") = Boolean_To_Bit(m_Simulation)
                    m_DataTableVariablesDecisionsBossValueList.AcceptChanges()
                Next

            Else ' Inserisco nuove righe

                If NomeVariabileLista = "" Then
                    If sTeamName <> "" Then
                        sNomeVariabile = NomeVariabile & "_" & sTeamName
                    End If
                    If sItemName <> "" Then
                        sNomeVariabile = NomeVariabile & "_" & sItemName
                    End If
                    If sAgeName <> "" Then
                        sNomeVariabile = NomeVariabile & "_" & sAgeName
                    End If
                Else
                    sNomeVariabile = NomeVariabileLista
                End If

                Dim drInsert As DataRow = m_DataTableVariablesDecisionsBossValueList.NewRow
                drInsert("IDDecisionBossValue") = iIDDecisionBossValue
                drInsert("IDPeriod") = m_IDPeriodNext
                drInsert("VariableName") = CStrSql(NomeVariabileLista)
                drInsert("Value") = Nn(Value)
                drInsert("Simulation") = Boolean_To_Bit(m_Simulation)

                m_DataTableVariablesDecisionsBossValueList.Rows.Add(drInsert)
                m_DataTableVariablesDecisionsBossValueList.AcceptChanges()

            End If

        Else ' Inserisco o aggiorno i valori nella tabella
            ' Controllo la presenza di valori per il periodo attuale
            drDecisions_Boss_Value = m_DataTableVariablesDecisionsBossValue.Select("IDDecisionBoss = " & iIDDecisionBoss & " AND ISNULL(IDPeriod, " & m_IDPeriodNext & ") = " & m_IDPeriodNext)
            If drDecisions_Boss_Value.Length > 0 Then 'Aggiorno le righe esistenti
                For Each oRow As DataRow In drDecisions_Boss_Value
                    oRow("Value") = Nn(oRow("Value")) + Nn(Value)
                    oRow("Simulation") = Boolean_To_Bit(m_Simulation)
                    m_DataTableVariablesDecisionsBossValue.AcceptChanges()
                Next

            Else ' Inserisco nuove righe
                Dim drInsert As DataRow = m_DataTableVariablesDecisionsBossValue.NewRow
                drInsert("IDDecisionBoss") = iIDDecisionBoss
                drInsert("IDPeriod") = m_IDPeriodNext
                drInsert("Value") = Nn(Value)
                drInsert("Simulation") = Boolean_To_Bit(m_Simulation)
                m_DataTableVariablesDecisionsBossValue.AcceptChanges()

            End If
        End If

    End Sub

    Private Sub SaveVariableStateValue(NomeVariabile As String, IDPlayer As Integer, IDItem As Integer, IDAge As Integer, Value As String)
        Dim sItemName As String = ""
        Dim sAgeName As String = ""
        Dim sTeamName As String = ""
        Dim iIDVariableState As Integer

        ' Se ho l'ID del team mi recupero il nome
        If IDPlayer > 0 Then sTeamName = GetTeamName(IDPlayer)

        ' Se ho l'ID Item recupero il nome 
        If IDItem > 0 Then sItemName = GetItemName(IDItem)

        ' Se ho l'ID Age ne recupero il nome/descrizione
        If IDAge > 0 Then sAgeName = GetAgeName(IDAge)

        ' Controllo l'esistenza della variabile e ne recupero l'ID
        Dim drVariables_State As DataRow() = m_DataTableVariablesState.Select("Name = '" & NomeVariabile & "' AND IDGame = " & m_IDGame)
        iIDVariableState = Nni(drVariables_State(0)("ID"))

        ' Controllo la presenza di valori nella tabella Variables State Value, se ci sono vado in UPDATE, altrimenti controllo nella Variables State Value List
        Dim sValore As String = ""

        Dim drVariables_State_Value As DataRow() = m_DataTableVariablesStateValue.Select("IDVariable = " & iIDVariableState & " AND ISNULL(IDPeriod, " & m_IDPeriodNext & ") = " & m_IDPeriodNext)
        If drVariables_State_Value.Length > 0 Then
            sValore = Nz(drVariables_State_Value(0)("VALUE"))
        End If

        If sValore.ToUpper.Contains("LIST OF") Then
            Dim iIDVariableStateValue As Integer
            ' Recupero l'ID e vado nella tabella di dettaglio
            drVariables_State_Value = m_DataTableVariablesStateValue.Select("IDVariable = " & iIDVariableState & " AND ISNULL(IDPeriod, " & m_IDPeriodNext & ") = " & m_IDPeriodNext)
            If drVariables_State_Value.Length > 0 Then
                iIDVariableStateValue = drVariables_State_Value(0)("ID")
            Else
                iIDVariableStateValue = 0
            End If

            ' Recupero i dati della tabella Variables State Value List
            Dim drVariables_State_Value_List() As DataRow = m_DataTableVariablesStateValueList.Select("IDVariableState = " & iIDVariableStateValue & " AND ISNULL(IDPlayer, " & IDPlayer & ") = " & IDPlayer _
                                                          & "AND ISNULL(IDItem, " & IDItem & ") = " & IDItem & " AND ISNULL(IDAge, " & IDAge & ") = " & IDAge _
                                                          & " AND ISNULL(IDPeriod, " & m_IDPeriodNext & ") = " & m_IDPeriodNext)
            If drVariables_State_Value_List.Length > 0 Then
                For Each oRow As DataRow In drVariables_State_Value_List
                    oRow("Value") = Nn(oRow("Value")) + Nn(Value)
                    oRow("Simulation") = Boolean_To_Bit(m_Simulation)
                    m_DataTableVariablesStateValueList.AcceptChanges()
                Next

            Else ' Inserisco nuove righe
                Dim sNomeVariabile As String = ""

                If sItemName <> "" Then
                    sNomeVariabile = "var_" & sTeamName & "_" & NomeVariabile & "_" & sItemName
                End If
                If sAgeName <> "" Then
                    sNomeVariabile = "var_" & sTeamName & "_" & NomeVariabile & "_" & sAgeName
                End If

                Dim drInsert As DataRow = m_DataTableVariablesStateValueList.NewRow
                drInsert("IDVariableState") = iIDVariableStateValue
                drInsert("IDPlayer") = IDPlayer
                drInsert("IDPeriod") = m_IDPeriodNext
                drInsert("IDItem") = IDItem
                drInsert("IDAge") = IDAge
                drInsert("VariableName") = sNomeVariabile
                drInsert("Value") = Value
                drInsert("Simulation") = Boolean_To_Bit(m_Simulation)

                m_DataTableVariablesStateValueList.Rows.Add(drInsert)
                m_DataTableVariablesStateValueList.AcceptChanges()
            End If


        Else ' Inserisco o aggiorno i valori nella tabella
            Dim oDTValue As New DataTable

            ' Controllo la presenza di valori per il periodo attuale
            drVariables_State_Value = m_DataTableVariablesStateValue.Select("IDVariable = " & iIDVariableState _
                                    & " AND ISNULL(IDPeriod, " & m_IDPeriodNext & ") = " & m_IDPeriodNext _
                                    & " AND ISNULL(IDPlayer, " & IDPlayer & ") = " & IDPlayer _
                                    & " AND ISNULL(IDItem, " & IDItem & ") = " & IDItem & " AND ISNULL(IDAge, " & IDAge & ") = " & IDAge)

            If drVariables_State_Value.Length > 0 Then 'Aggiorno le righe esistenti
                For Each oRow As DataRow In oDTValue.Rows
                    oRow("Value") = Nn(oRow("Value")) + Nn(Value)
                    oRow("Simulation") = Boolean_To_Bit(m_Simulation)
                    m_DataTableVariablesStateValue.AcceptChanges()
                Next

            Else ' Inserisco nuove righe
                Dim drInsert As DataRow = m_DataTableVariablesStateValue.NewRow
                drInsert("IDVariable") = iIDVariableState
                drInsert("IDPlayer") = IDPlayer
                drInsert("IDPeriod") = m_IDPeriodNext
                drInsert("IDItem") = IDItem
                drInsert("IDAge") = IDAge
                drInsert("Value") = Nn(Value)
                drInsert("Simulation") = Boolean_To_Bit(m_Simulation)
                m_DataTableVariablesStateValue.Rows.Add(drInsert)
                m_DataTableVariablesStateValue.AcceptChanges()
            End If

        End If

    End Sub

    ''' <summary>
    ''' Recupero il valore per lo specifico player nel periodo selezionato della varibile calcolata
    ''' </summary>
    ''' <param name="NomeVariabile"></param>
    ''' <param name="IDPlayer"></param>
    ''' <param name="IDItem"></param>
    ''' <returns></returns>
    Private Function GetVariableDefineValue(NomeVariabile As String, IDPlayer As Integer, IDItem As Integer, IDAge As Integer) As String
        ' Recupero l'ID Della variabile generica
        Dim iIDVariabileMaster As Integer

        Dim drVariables_Calculate_Define As DataRow()
        drVariables_Calculate_Define = m_DataTableVariableCalculateDefine.Select("VariableName = '" & NomeVariabile & "' AND IDGame = " & m_IDGame)
        iIDVariabileMaster = Nni(drVariables_Calculate_Define(0)("ID"))

        ' Con l'ID appena preso recupero il valore della variabile per uno specifico ITEM
        Dim sValue As String

        Dim drVariables_Calculate_Define_Value As DataRow() =
                            m_DataTableVariableCalculateValue.Select("IDVariable = " & iIDVariabileMaster & " AND IDPLayer = " & IDPlayer _
                                                                    & " AND ISNULL(IDPeriod, " & m_IDPeriod & ") =  " & m_IDPeriod _
                                                                    & " AND ISNULL(IDItem, " & IDItem & ") = " & IDItem _
                                                                    & " AND ISNULL(IDAge, " & IDAge & ") = " & IDAge)

        If drVariables_Calculate_Define_Value.Length > 0 Then
            sValue = Nz(drVariables_Calculate_Define_Value(0)("Value"))
        Else
            sValue = "0"
        End If

        m_DataTableVariableCalculateDefine.Select("")
        m_DataTableVariableCalculateValue.Select("")

        Return sValue
    End Function

    ''' <summary>
    ''' Recupero la variabile di stato per il player nel periodo specifico
    ''' </summary>
    ''' <param name="NomeVariabileStato"></param>
    ''' <param name="IDPlayer"></param>
    ''' <param name="NomeVariabileLista"></param>
    ''' <returns></returns>
    Private Function GetVariableState(NomeVariabileStato As String, IDPlayer As Integer, NomeVariabileLista As String, IDItem As Integer, IDAge As Integer) As String
        Dim sWhereClause As String
        Dim iIDVariabileStato As Integer
        Dim sValore As String = "0"

        ' Recupero l'ID della variabile di stato generica
        Dim drVariablesState As DataRow
        Dim drVariables_State_Value As DataRow
        Dim drVariables_State_Value_List As DataRow

        Try
            drVariablesState = m_DataTableVariablesState.Select("Name = '" & NomeVariabileStato & "' AND IDGame = " & m_IDGame).FirstOrDefault
            iIDVariabileStato = Nni(drVariablesState("ID"))

            ' Con l'Id della variabile appena trovata vado a vedere il valore, se è un "List of" mi sposto nella tabella value_list
            sWhereClause = "IDVariable = " & iIDVariabileStato & " And ISNULL(IDPeriod,  " & m_IDPeriod & ") = " & m_IDPeriod

            If IDPlayer <> 0 Then
                sWhereClause &= " AND ISNULL(IDPlayer, " & IDPlayer & ") = " & IDPlayer
            End If
            If IDItem <> 0 Then
                sWhereClause &= "AND ISNULL(IDItem, " & IDItem & ") = " & IDItem
            End If
            If IDAge <> 0 Then
                sWhereClause &= " AND ISNULL(IDAge, " & IDAge & ") = " & IDAge
            End If
            drVariables_State_Value = m_DataTableVariablesStateValue.Select(sWhereClause).FirstOrDefault

            sValore = Nz(drVariables_State_Value("Value"))

            If sValore.ToUpper.Contains("LIST OF") Then ' Vado a recuperare il valore nella tabella che contiene variabili di tipo lista
                sWhereClause = "ISNULL(IDPeriod,  " & m_IDPeriod & ") = " & m_IDPeriod & " AND VariableName = '" & NomeVariabileLista & "' "
                If IDPlayer <> 0 Then
                    sWhereClause &= " AND ISNULL(IDPlayer, " & IDPlayer & ") = " & IDPlayer
                End If
                If IDItem <> 0 Then
                    sWhereClause &= " AND ISNULL(IDItem, " & IDItem & ") = " & IDItem
                End If
                If IDAge <> 0 Then
                    sWhereClause &= " AND ISNULL(IDAge, " & IDAge & ") = " & IDAge
                End If

                drVariables_State_Value_List = m_DataTableVariablesStateValueList.Select(sWhereClause).FirstOrDefault

                If Not drVariables_State_Value_List Is Nothing Then
                    sValore = Nz(drVariables_State_Value_List("Value"))
                Else
                    sValore = "0"
                End If

            End If

            If sValore = "" Then sValore = "0"

            Return sValore

        Catch ex As Exception
            g_MessaggioErrore &= "GetVariableState -> " & NomeVariabileStato & " -> " & ex.Message & " < br /> "
            ex.Data.Clear()
            Return sValore
        End Try

    End Function

    Private Sub LoadVariablesDecisionsBoss(IDGame As Integer)
        Dim sSQL As String = "SELECT * FROM Decisions_Boss WHERE IDGame = " & IDGame

        m_DataTableVariablesDecisionsBoss = g_DAL.ExecuteDataTable(sSQL)
        m_DataTableVariablesDecisionsBoss.Columns("ID").AutoIncrement = True
        m_DataTableVariablesDecisionsBoss.Columns("ID").AutoIncrementSeed = 1
        m_DataTableVariablesDecisionsBoss.Columns("ID").AutoIncrementStep = 1
    End Sub

    Private Sub LoadVariablesDecisionsBossValue(IDPeriod As Integer)
        Dim sSQL As String = "SELECT Id, IDDecisionBoss, Value, ISNULL(IDPeriod, " & m_IDPeriod & ") AS IDPeriod, Simulation FROM Decisions_Boss_Value WHERE ISNULL(IDPeriod, " & IDPeriod & ") = " & IDPeriod

        m_DataTableVariablesDecisionsBossValue = g_DAL.ExecuteDataTable(sSQL)
        m_DataTableVariablesDecisionsBossValue.Columns("ID").AutoIncrement = True
        m_DataTableVariablesDecisionsBossValue.Columns("ID").AutoIncrementSeed = 1
        m_DataTableVariablesDecisionsBossValue.Columns("ID").AutoIncrementStep = 1
    End Sub

    Private Sub LoadVariablesDecisionsBossValueList(IDPeriod As Integer)
        Dim sSQL As String = "SELECT * FROM Decisions_Boss_Value_List WHERE ISNULL(IDPeriod, " & IDPeriod & ") = " & IDPeriod

        m_DataTableVariablesDecisionsBossValueList = g_DAL.ExecuteDataTable(sSQL)
        m_DataTableVariablesDecisionsBossValueList.Columns("ID").AutoIncrement = True
        m_DataTableVariablesDecisionsBossValueList.Columns("ID").AutoIncrementSeed = 1
        m_DataTableVariablesDecisionsBossValueList.Columns("ID").AutoIncrementStep = 1
    End Sub

    Private Sub LoadVariablesDecisionsDeveloper(IDGame As Integer)
        Dim sSQL As String = "SELECT * FROM Decisions_Developer WHERE IDGame = " & IDGame

        m_DataTableDecisionsDeveloper = g_DAL.ExecuteDataTable(sSQL)
        m_DataTableDecisionsDeveloper.Columns("ID").AutoIncrement = True
        m_DataTableDecisionsDeveloper.Columns("ID").AutoIncrementSeed = 1
        m_DataTableDecisionsDeveloper.Columns("ID").AutoIncrementStep = 1
    End Sub

    Private Sub LoadVariablesDecisionsDeveloperValue(IDPeriod As Integer)
        Dim sSQL As String = "SELECT Id, IDDecisionDeveloper, Value, ISNULL(IDPeriod, " & m_IDPeriod & ") AS IDPeriod, " _
                           & "Simulation, IDItem, IDPlayer FROM Decisions_Developer_Value WHERE ISNULL(IDPeriod, " & IDPeriod & ") = " & IDPeriod

        m_DataTableDecisionsDeveloperValue = g_DAL.ExecuteDataTable(sSQL)
        m_DataTableDecisionsDeveloperValue.Columns("ID").AutoIncrement = True
        m_DataTableDecisionsDeveloperValue.Columns("ID").AutoIncrementSeed = 1
        m_DataTableDecisionsDeveloperValue.Columns("ID").AutoIncrementStep = 1
    End Sub

    Private Sub LoadVariablesDecisionsDeveloperValueList(IDPeriod As Integer)
        Dim sSQL As String = "SELECT * FROM Decisions_Developer_Value_List WHERE ISNULL(IDPeriod, " & IDPeriod & ") = " & IDPeriod

        m_DataTableDecisionsDeveloperValueList = g_DAL.ExecuteDataTable(sSQL)
        m_DataTableDecisionsDeveloperValueList.Columns("ID").AutoIncrement = True
        m_DataTableDecisionsDeveloperValueList.Columns("ID").AutoIncrementSeed = 1
        m_DataTableDecisionsDeveloperValueList.Columns("ID").AutoIncrementStep = 1
    End Sub

    ''' <summary>
    ''' Recupero tutti i dati di una specifica variabile del boss nel periodo specifico
    ''' </summary>
    ''' <param name="NomeVariabileBoss"></param>
    ''' <param name="NomeVariabileLista"></param>
    ''' <returns></returns>
    Private Function GetVariableBoss(NomeVariabileBoss As String, NomeVariabileLista As String) As String
        Dim sValore As String = "0"
        Dim iIDVariabileBoss As Integer

        Try
            ' Recupero l'ID della variabile di stato generica
            Dim drDecisionsBoss As DataRow = m_DataTableVariablesDecisionsBoss.Select("Decision = '" & NomeVariabileBoss & "' AND IDGame = " & m_IDGame).FirstOrDefault
            iIDVariabileBoss = drDecisionsBoss("ID")

            ' Con l'Id della variabile appena trovata vado a vedere il valore, se è un "List Of" mi sposto nella tabella value_list
            Dim drDecisionsBossValue As DataRow = m_DataTableVariablesDecisionsBossValue.Select("IDDecisionBoss = " & iIDVariabileBoss _
                                                & " AND ISNULL(IDPeriod, " & m_IDPeriod & ") = " & m_IDPeriod).FirstOrDefault
            If Not drDecisionsBossValue Is Nothing Then
                sValore = drDecisionsBossValue("Value")
            Else
                sValore = "0"
            End If

            If sValore.ToUpper.Contains("LIST OF") Then ' Vado a recuperare il valore nella tabella che contiene variabili di tipo lista
                ' Recupero l'ID della variabile della tabella Decsions_Boss_Value
                Dim iIDDecisionBossValue As Integer
                drDecisionsBossValue = m_DataTableVariablesDecisionsBossValue.Select("IDDecisionBoss = " & iIDVariabileBoss & " AND ISNULL(IDPeriod, " & m_IDPeriod & ") = " & m_IDPeriod).FirstOrDefault
                If Not drDecisionsBossValue Is Nothing Then
                    iIDDecisionBossValue = drDecisionsBossValue("ID")
                Else
                    sValore = "0"
                End If

                Dim drDecisionsBossValueList As DataRow = m_DataTableVariablesDecisionsBossValueList.Select("IDDecisionBossValue = " & iIDDecisionBossValue _
                                                          & " And VariableName = '" & NomeVariabileLista & "' AND ISNULL(IDPeriod, " & m_IDPeriod & ") = " & m_IDPeriod).FirstOrDefault
                sValore = "0"
                If Not drDecisionsBossValueList Is Nothing Then
                    If Nz(drDecisionsBossValueList("Value")) <> "" Then
                        sValore = drDecisionsBossValueList("Value")
                    End If
                End If
            End If

            Return sValore
        Catch ex As Exception
            g_MessaggioErrore &= "GetVariableBoss -> " & NomeVariabileBoss & " -> " & ex.Message & "<br/>"
            ex.Data.Clear()
            Return sValore
        End Try

    End Function

    Private Function GetVariableDeveloper(IDGame As Integer, NomeVariabile As String, IDPlayer As Integer, IDItem As Integer) As String
        Dim iIDVariabileDeveloper As Integer
        Dim iIDVariabileDeveloperValue As Integer
        Dim sValore As String = "0"
        Dim sWhereClause As String
        Try
            ' Recupero l'id della variabile da usare

            Dim drVariableDeveloper As DataRow = m_DataTableDecisionsDeveloper.Select("VariableName = '" & NomeVariabile.Trim & "' AND IDGame = " & IDGame).FirstOrDefault
            If Not drVariableDeveloper Is Nothing Then
                iIDVariabileDeveloper = drVariableDeveloper("ID")
            Else
                iIDVariabileDeveloper = 0
            End If

            If iIDVariabileDeveloper <> 0 Then ' Esiste la variabile che cerco
                sWhereClause = "IDDecisionDeveloper = " & iIDVariabileDeveloper & " AND IDPeriod = " & m_IDPeriod
                If IDPlayer <> 0 Then
                    sWhereClause &= " AND ISNULL(IDPlayer, " & IDPlayer & ") = " & IDPlayer
                End If
                If IDItem <> 0 Then
                    sWhereClause &= " AND ISNULL(IDItem, " & IDItem & ") = " & IDItem
                End If

                Dim drDecisions_Developer_Value As DataRow = m_DataTableDecisionsDeveloperValue.Select(sWhereClause).FirstOrDefault
                If Not drDecisions_Developer_Value Is Nothing Then
                    sValore = drDecisions_Developer_Value("Value")
                Else
                    sValore = "0"
                End If

                If sValore.ToUpper.Contains("LIST OF") Then
                    ' Recupero l'id della variabile interessata
                    sWhereClause = "IDDecisionDeveloper = " & iIDVariabileDeveloper & " AND IDPeriod = " & m_IDPeriod
                    If IDPlayer <> 0 Then
                        sWhereClause &= " AND ISNULL(IDPlayer, " & IDPlayer & ") = " & IDPlayer
                    End If
                    If IDItem <> 0 Then
                        sWhereClause &= " AND ISNULL(IDItem, " & IDItem & ") = " & IDItem
                    End If
                    drDecisions_Developer_Value = m_DataTableDecisionsDeveloperValue.Select(sWhereClause).FirstOrDefault
                    iIDVariabileDeveloperValue = Nni(drDecisions_Developer_Value("ID"))

                    ' Recupero il valore necessario nella tabella che contiene eventuali liste
                    sWhereClause = "IDDecsionValue = " & iIDVariabileDeveloperValue & " AND IDPeriod = " & m_IDPeriod
                    If IDPlayer <> 0 Then
                        sWhereClause &= " AND IDPlayer = " & IDPlayer
                    End If
                    If IDItem <> 0 Then
                        sWhereClause &= " AND IDItem = " & IDItem
                    End If
                    Dim drDecisions_Developer_Value_List As DataRow = m_DataTableDecisionsDeveloperValueList.Select(sWhereClause).FirstOrDefault

                    If Not drDecisions_Developer_Value_List Is Nothing Then
                        sValore = Nn(drDecisions_Developer_Value_List("Value"))
                    Else
                        sValore = "0"
                    End If

                End If
            End If

            If sValore = "" Then sValore = "0"
            Return sValore
        Catch ex As Exception
            g_MessaggioErrore &= "GetVariableDeveloper -> " & NomeVariabile & " -> " & ex.Message & "<br/>"
            ex.Data.Clear()
            Return sValore
        End Try

    End Function

    Private Function GetVariableDecisionPlayer(IDGame As Integer, NomeVariabile As String, IDPlayer As Integer, IDItem As Integer) As String
        Dim iIDVariabilePlayer As Integer
        Dim iIDVariabilePlayerValue As Integer
        Dim sValore As String = "0"
        Dim sWhereClause As String

        Try
            ' Recupero l'id della variabile da usare
            sWhereClause = "VariableName = '" & NomeVariabile.Trim & "' AND IDGame = " & IDGame
            Dim drDecisionsPlayer As DataRow = m_DataTableDecisionsPlayers.Select(sWhereClause).FirstOrDefault
            iIDVariabilePlayer = drDecisionsPlayer("ID")

            If iIDVariabilePlayer <> 0 Then ' Esiste la variabile che cerco
                sWhereClause = "IDDecisionPlayer = " & iIDVariabilePlayer & " AND ISNULL(IDPeriod, " & m_IDPeriod & ") = " & m_IDPeriod
                If IDPlayer <> 0 Then
                    sWhereClause &= " AND ISNULL(IDPlayer, " & IDPlayer & ") = " & IDPlayer
                End If

                Dim drDecisionsPlayerValue As DataRow = m_DataTableDecisionsPlayersValue.Select(sWhereClause).FirstOrDefault
                sValore = drDecisionsPlayerValue("VariableValue")

                If sValore.ToUpper.Contains("LIST OF") Then
                    sWhereClause = "IDDecisionPlayer = " & iIDVariabilePlayer & " And ISNULL(IDPeriod, " & m_IDPeriod & ") = " & m_IDPeriod

                    If IDPlayer <> 0 Then
                        sWhereClause &= " AND ISNULL(IDPlayer, " & IDPlayer & ") = " & IDPlayer
                    End If
                    drDecisionsPlayerValue = m_DataTableDecisionsPlayersValue.Select(sWhereClause).FirstOrDefault
                    iIDVariabilePlayerValue = drDecisionsPlayerValue("ID")

                    ' Recupero il valore necessario nella tabella che contiene eventuali liste
                    sWhereClause = "IDDecisionValue = " & iIDVariabilePlayerValue & " And ISNULL(IDPeriod, " & m_IDPeriod & ") = " & m_IDPeriod

                    If IDPlayer <> 0 Then
                        sWhereClause &= " AND IDPlayer = " & IDPlayer
                    End If
                    If IDItem <> 0 Then
                        sWhereClause &= " AND IDItem = " & IDItem
                    End If

                    Dim drDecisionsPlayerValueList As DataRow = m_DataTableDecisionsPlayersValueList.Select(sWhereClause).FirstOrDefault
                    sValore = drDecisionsPlayerValueList("Value")

                End If
            End If

            If sValore = "" Then sValore = "0"
            Return sValore
        Catch ex As Exception
            g_MessaggioErrore &= "GetVariableDecisionPlayer -> " & NomeVariabile & "<br/>" & ex.Message & "<br/>"
            ex.Data.Clear()
            Return sValore
        End Try

    End Function

    ''' <summary>
    ''' Carica un datatable contenente i valori AGE per ogi player
    ''' </summary>
    Private Sub LoadAge(IDGame As Integer)
        Dim sSQL As String

        sSQL = "Select A.ID, A.IDGame, A.AgeDescription, A.AgeLabel " _
             & "FROM Age A " _
             & "WHERE A.IDGame = " & IDGame
        m_DataTableAge = g_DAL.ExecuteDataTable(sSQL)
    End Sub

    Private Sub HandleLoadNumberPeriod(IDGame As Integer)
        Dim sLunghezzaPeriodo As String = Nz(GetVariableDeveloper(IDGame, "PeriodLength", 0, 0))

        If sLunghezzaPeriodo <> "" Then
            m_LunghezzaPeriodo = Nni(sLunghezzaPeriodo)
        Else
            m_LunghezzaPeriodo = 0
        End If

    End Sub

#End Region

#Region "ARKEA 2016 The game Of games"

    Public Function Sim1() As String
        ' 1- 20 
        g_MessaggioErrore = ""

        TotalActivMachi_001()
        PortfDelivNIC_002()
        GlobaActuaLease_003()
        AveraTechnYear_004()
        TableAveraTechn_005()
        NewLeaseCost_006()
        IncreRawEurop_007()
        IncreRawNIC_008()
        VehicScrapAllow_009()
        PerifScrapAllow_010()
        OperaVehicUsed_011()
        VehicScrapAge_012()
        TotalProduCosts_013()
        IncreProduCost_014()
        DischPreviCosts_015()
        AveraTrainYear_016()
        TableAveraTrain_017()
        DischPreviPrice_018()
        NewCostStaff_019()
        NewCostPoint_020()

        Return g_MessaggioErrore

    End Function

    Public Function Sim2() As String
        ' 21 - 40
        g_MessaggioErrore = ""

        RentPerioPerif_101()
        RentPerioCentr_102()
        SalesPriceAllow_103()
        AveraAdverYear_104()
        TableAveraAdver_105()
        AveraGreenYear_106()
        TableAveraGreen_107()
        DischMarkeShare_108()
        LoansAllow_109()
        PurchEuropFinan_110()
        PurchNICFinan_111()
        ProduCashFinan_112()
        PromoCashFinan_113()
        ExtraProduFinan_114()
        PlantCashFinan_115()
        LeaseCashFinan_016()
        VehicCashFinan_117()
        CentrStoreFinan_118()
        PerifCashFinan_119()
        PersoStaffFinan_120()

        If g_MessaggioErrore <> "" Then
            Return g_MessaggioErrore
        End If
    End Function

    Public Function Sim3() As String
        ' 41 - 60
        g_MessaggioErrore = ""

        PersoPointFinan_201()
        CashNeedFirst_202()
        TotalCashNeede_203()
        DischMaximLoans_204()
        NetVehicles_205()
        LoansEndPerio_206()
        PaymeToCover_207()
        DisinAllowPerio_208()
        DecreDebts_209()
        DecreCredits_210()
        BeginRawValue_211()
        ValueStockBegin_212()
        DischCostsFixed_213()
        SinkiFundVehic_214()
        DebtsWrittOff_215()
        DischRavenTotal_216()
        DischDeltaRaw_217()
        DischDeltaStock_218()
        DischCostPurch_219()
        DischDirecCosts_220()

        If g_MessaggioErrore <> "" Then
            Return g_MessaggioErrore
        End If
    End Function

    Public Function Sim4() As String
        ' 61 - 80
        g_MessaggioErrore = ""
        DischExtraProdu_301()
        DischSinkiPlant_302()
        DischLeasiCosts_303()
        DischWarehCosts_304()
        DischTransCosts_305()
        DischSinkiVehic_306()
        DischFixedCosts_307()
        DischPersoCosts_308()
        DischOutleCosts_309()
        DischExpenCosts_310()
        DischDebtsOff_311()
        DischPositInter_312()
        DischNegatInter_313()
        DischBeforTax_314()
        PlantSoldAllow_315()
        OperaPlantScrap_316()
        NextRawEurop_317()
        NextRawNIC_318()
        CentrAlienAllow_319()
        DeltaSalesPrice_320()

        If g_MessaggioErrore <> "" Then
            Return g_MessaggioErrore
        End If
    End Function

    Public Function Sim5() As String
        ' 81 - 120
        g_MessaggioErrore = ""

        TaxPayment_401()
        NegatTaxPayement_402()
        WriteOffLost_403()
        CumulSinkiVehic_404()
        CumulDebtsOff_405()
        TotalPlant_406()
        TotalCashAvail_407()
        PerceFirstRequi_408()
        NetPlants_409()
        ExtraMoneyAllow_410()
        CumulExtraProdu_411()
        PerceExpenCash_412()
        PurchAllowEurop_413()
        PurchAllowNIC_414()
        ProduAllow_415()
        ExtraAllow_416()
        PlantAllow_417()
        LeaseAllow_418()
        TechnAutomAllow_419()
        PortfPurchNIC_420()

        If g_MessaggioErrore <> "" Then
            Return g_MessaggioErrore
        End If
    End Function

    Public Function Sim6() As String
        ' 121 - 140
        g_MessaggioErrore = ""
        IncreAgePlant_501()
        IncreAgeLease_502()
        ProduPossiRaw_503()
        TotalHoursRequi_504()
        IncreAveraTechn_505()
        TechnAutomYear_506()
        TableTechnAllow_507()
        IncreTechnCumul_508()
        MarkeAveraTechn_509()
        IncreAbsolAutom_510()
        FulvioTeknoRap_511()
        DecreHoursItem_512()
        NextHoursItem_513()
        VehicAllow_514()
        CentrStoreAllow_515()
        PerifStoreAllow_516()
        NewStaffAllow_517()
        NewPointAllow_518()
        AddesTotalAllow_519()
        TrainServiAllow_520()

        If g_MessaggioErrore <> "" Then
            Return g_MessaggioErrore
        End If
    End Function

    Public Function Sim7() As String
        ' 141 - 160
        g_MessaggioErrore = ""
        MarkeExpenAllow_601()
        IncreAgeVehic_602()
        AveraAgeVehic_603()
        TableAveraTrans_604()
        TotalCapacTrans_605()
        NextCapacTrans_606()
        CurreOutleCentr_607()
        CurreOutleSubur_608()
        MaximSpaceAvail_609()
        CurreSuperStaff_610()
        CurreSalesPeopl_611()
        IncreStaffLevel_612()
        IncrePointLevel_613()
        IncreAveraTrain_614()
        TrainServiYear_615()
        TableTrainPermit_616()
        IncreTrainCumul_617()
        MarkeAveraTrain_618()
        IncreAbsolTrain_619()
        AveraTrainLevel_620()

        If g_MessaggioErrore <> "" Then
            Return g_MessaggioErrore
        End If

    End Function

    Public Function Sim8() As String
        ' 161 - 180
        g_MessaggioErrore = ""
        TotalExpenTrain_701()
        AdverAllow_702()
        AdverYearAllow_703()
        TableAdverPermit_704()
        IncreAveraAdver_705()
        IncreAdverCumul_706()
        MarkeAveraAdver_707()
        IncreAbsolAdver_708()
        AveraAdverLevel_709()
        TotalExpenAdver_710()
        GreenAllow_711()
        InterMarkeCosts_712()
        CostPurchRaw_713()
        CostPurchEurop_714()
        CostPurchNIC_715()
        CostPurchTotal_716()
        TechnCostsPerio_717()
        TotalNegatInter_718()
        TotalCentrCosts_719()
        PerifStoreCosts_720()

        If g_MessaggioErrore <> "" Then
            Return g_MessaggioErrore
        End If

    End Function

    Public Function Sim9() As String
        ' 161 - 180
        g_MessaggioErrore = ""
        TotalCostsOutle_801()
        CostsPersoStaff_802()
        CostsPersoPoint_803()
        TotalCostsPerso_804()
        PromoInvesPerio_805()
        AdditCostsPerio_806()
        CumulCostPurch_807()
        CumulOutleCosts_808()
        CumulPersoCosts_809()
        CumulExpenCosts_810()
        CumulNegatInter_811()
        TotalLease_812()
        ActuaLeaseMachi_813()
        TotalMachiLease_814()
        PlantTousePerio_815()
        TotalCapacProdu_816()
        NextCapacProdu_817()
        ProduReque_818()
        PerceProduUsed_819()
        AveraTechnLevel_820()

        If g_MessaggioErrore <> "" Then
            Return g_MessaggioErrore
        End If
    End Function

    Public Function Sim10() As String
        ' 201- 220
        g_MessaggioErrore = ""
        VariaTechnQuali_901()
        ProduQualiProce_902()
        TotalMarkeAllow_903()
        VariaTrainQuali_904()
        TrainQualiProce_905()
        QualiTrainProce_906()
        VariaAdverQuali_907()
        AdverQualiProce_908()
        IncreAveraGreen_909()
        GreenYearAllow_910()
        TableGreenPermi_911()
        IncreGreenCumul_912()
        MarkeAveraGreen_913()
        IncreAbsolGreen_914()
        AveraGreenLevel_915()
        MaximGreenPeace_916()
        DirecProduCosts_917()
        DirecTotalCosts_918()
        ProduTotalCost_919()
        FixedProduCosts_920()

        If g_MessaggioErrore <> "" Then
            Return g_MessaggioErrore
        End If

    End Function

    Public Function Sim11() As String
        ' 221 - 240
        g_MessaggioErrore = ""

        ProduFixedCosts_1001()
        SinkiFundPlant_1002()
        LeasiTotalCost_1003()
        CumulDirecCosts_1004()
        CumulSinkiPlant_1005()
        CumulLeasiCosts_1006()
        CumulFixedCosts_1007()
        ProduCostsItem_1008()
        IncreQuantStock_1009()
        StockRawNext_1010()
        IncreValueRaw_1011()
        IncreQualiStock_1012()
        RawQualiUsed_1013()
        ProduTotalQuali_1014()
        InterPriceAllow_1015()
        VariaGreenQuali_1016()
        GreenQualiProce_1017()
        InterSalesAllow_1018()
        VolumInterOffer_1019()
        TotalPriceOffer_1020()

        If g_MessaggioErrore <> "" Then
            Return g_MessaggioErrore
        End If
    End Function

    Public Function Sim12() As String
        ' 241 - 260
        g_MessaggioErrore = ""
        TotalQualiOffer_1101()
        AveraQualiOffer_1102()
        QualiGoodsOffer_1103()
        AttractionIndex_1104()
        DecreBankCash_1105()
        EndRawValue_1106()
        DeltaRawValue_1107()
        CumulDeltaRaw_1108()
        DischPreviPrInt_1109()
        AveraPriceOffer_1110()
        IndexGenerOffer_1111()
        MinimIndexGener_1112()
        SalesInterMarke_1113()
        GlobaInterSales_1114()
        IndexOfAttraction_1115()
        RawEndPerio_1116()
        ProduRevenInter_1117()
        TotalRevenInter_1118()
        TotalTransRequi_1119()
        ExtraTransNeede_1120()

        If g_MessaggioErrore <> "" Then
            Return g_MessaggioErrore
        End If

    End Function

    Public Function Sim13() As String
        g_MessaggioErrore = ""

        TotalOfferCusto_1201()
        AveraAttraIndex_1202()
        ThresAttraOffer_1203()
        InterTransCosts_1204()
        ExterTransCosts_1205()
        TotalTransCosts_1206()
        CumulTransCosts_1207()
        TotalVolumSpace_1208()
        OttimGoodsStaff_1209()
        OttimPointPerso_1210()
        RateGoodsStaff_1211()
        RateGarriPoint_1212()
        TableEffecStaff_1213()
        TableEffecPoint_1214()
        MaximSellaGoods_1215()
        RatioMaximAvail_1216()
        CompoMarkePrice_1217()
        CompoMarkeQuali_1218()
        DiscoPriceOffer_1219()
        CompoDiscoPrice_1220()

        Return g_MessaggioErrore
    End Function

    Public Function Sim14() As String
        g_MessaggioErrore = ""

        ThresQualiOffer_1301()
        ThresPriceOffer_1302()
        ThresPriceOffer_1302()
        MarketDemand_1303()
        IndexMarkeOffer_1304()
        IndexOfCompetition_1305()
        FirstOrderShare_1306()
        FirstOrderAlloc_1307()
        TotalOrderAlloc_1308()
        ResidMarkeDeman_1309()
        ResidDemanAlloc_1310()
        TotalProduSold_1311()
        RatioSalesMaxim_1312()
        GlobaMarkeSales_1313()
        MarketShare_1314()
        MarketPriceShare_1315()
        RatioCompoShare_1316()
        ProduRevenMarke_1317()
        TotalRevenMarke_1318()
        RatioPromoReven_1319()
        RatioProduReven_1320()

        Return g_MessaggioErrore
    End Function

    Public Function Sim15() As String
        g_MessaggioErrore = ""

        RatioFixedReven_1401()
        RatioOutleReven_1402()
        RatioPersoReven_1403()
        IncreGoodsStock_1404()
        FinisGoodsStock_1405()
        IncreValueStock_1406()
        IncreQualiFinis_1407()
        IncreDebts_1408()
        ValueStockEnd_1409()
        DeltaStockValue_1410()
        TotalWarehCosts_1411()
        TotalCostsPerio_1412()
        CumulDeltaStock_1413()
        CumulWarehCosts_1414()
        MarginPerProduct_1415()
        RatioWarehReven_1416()
        DebtsEndPerio_1417()
        StockEndPerio_1718()
        IncreCredits_1419()
        TotalPositInter_1420()
        FinisValueStock_1421()
        FinisQualiStock_1422()

        Return g_MessaggioErrore
    End Function

    Public Function Sim16() As String
        g_MessaggioErrore = ""

        TotalRevenPerio_1501()
        ProfiBeforTax_1502()
        CumulRevenTotal_1503()
        CumulPositInter_1504()
        CumulBeforTax_1505()
        ReturOnSales_1506()
        IncreMaximLoans_1507()
        CrediEndPerio_1508()
        IncreBankCash_1509()
        TaxOfPerio_1510()
        ProfiAfterTax_1511()
        ReturOnEquit_1512()
        CashEndPerio_1513()
        TotalAssets_1514()
        UtilsEndPerio_1515()
        TaxPerioFunds_1516()
        TotalLiabilities_1517()
        ShareMarkeValue_1518()
        ReturOnInves_1519()
        DischCostsTechn_1520()
        CumulCostsTechn_1521()

        Return g_MessaggioErrore
    End Function

    ''' <summary>
    ''' Avvia il calcolo usando la modalità della simulazione, non avanza il periodo
    ''' </summary>
    ''' <returns></returns>
    Public Function Calcola() As String
        Dim sSQL As String
        Dim oDAL As New DBHelper

        g_MessaggioErrore = ""

        ' Cancello le eventuali simulazioni effettuate fino adesso

        If g_MessaggioErrore = "" Then
            sSQL = "DELETE FROM Variables_Calculate_Value " _
                 & "WHERE ID In (Select C.ID FROM Variables_Calculate_Value C " _
                 & "INNER JOIN Variables_Calculate_Define V On C.IDVariable = V.Id " _
                 & "WHERE C.Simulation = 1 And V.IDGame = " & m_IDGame & ") "
            oDAL.ExecuteNonQuery(sSQL)

            sSQL = "DELETE FROM Variables_Calculate_Value " _
                 & "WHERE ID In (Select C.ID FROM Variables_Calculate_Value C " _
                 & "INNER JOIN Variables_Calculate_Define V On C.IDVariable = V.Id " _
                 & "WHERE C.Simulation = 0 And V.IDGame = " & m_IDGame & " And C.IDPeriod = " & m_IDPeriod & ") "
            oDAL.ExecuteNonQuery(sSQL)

            ' Periodi
            sSQL = "DELETE FROM BGOL_Games_Periods WHERE IDGame = " & m_IDGame & " AND ISNULL(Simulation, 0) = 1 "
            oDAL.ExecuteNonQuery(sSQL)

            ' BOSS
            sSQL = "DELETE FROM Decisions_Boss_Value_List WHERE ID In(Select DISTINCT DBVL.Id " _
                 & "FROM Decisions_Boss D " _
                 & "LEFT JOIN Decisions_Boss_Value DBV On D.Id = DBV.IDDecisionBoss " _
                 & "LEFT JOIN Decisions_Boss_Value_List DBVL On DBV.id = DBVL.IdDecisionBossValue " _
                 & "WHERE D.IDGame = " & m_IDGame & " AND ISNULL(DBVL.ID, 0) <> 0 AND ISNULL(DBVL.Simulation, 0) = 1) "
            oDAL.ExecuteNonQuery(sSQL)

            sSQL = "DELETE FROM Decisions_Boss_Value WHERE ID In(Select DISTINCT DBV.Id " _
                 & "FROM Decisions_Boss D " _
                 & "LEFT JOIN Decisions_Boss_Value DBV On D.Id = DBV.IDDecisionBoss " _
                 & "LEFT JOIN Decisions_Boss_Value_List DBVL On DBV.id = DBVL.IdDecisionBossValue " _
                 & "WHERE D.IDGame = " & m_IDGame & " AND ISNULL(DBV.Simulation, 0) = 1) "
            oDAL.ExecuteNonQuery(sSQL)

            ' DEVELOPER
            sSQL = "DELETE FROM Decisions_Developer_Value_List WHERE ID In(Select DISTINCT DBVL.Id " _
                 & "FROM Decisions_Developer D " _
                 & "LEFT JOIN Decisions_Developer_Value DBV On D.Id = DBV.IDDecisionDeveloper " _
                 & "LEFT JOIN Decisions_Developer_Value_List DBVL On DBV.id = DBVL.IDDecisionValue " _
                 & "WHERE D.IDGame = " & m_IDGame & " AND ISNULL(DBVL.ID, 0) <> 0 AND ISNULL(DBVL.Simulation, 0) = 1) "
            oDAL.ExecuteNonQuery(sSQL)

            sSQL = "DELETE FROM Decisions_Boss_Value WHERE ID In(Select DISTINCT DBV.Id " _
                 & "FROM Decisions_Developer D " _
                 & "LEFT JOIN Decisions_Developer_Value DBV On D.Id = DBV.IDDecisionDeveloper " _
                 & "LEFT JOIN Decisions_Developer_Value_List DBVL On DBV.id = DBVL.IDDecisionValue " _
                 & "WHERE D.IDGame = " & m_IDGame & " AND ISNULL(DBV.Simulation, 0) = 1) "
            oDAL.ExecuteNonQuery(sSQL)

            ' STATE
            sSQL = "DELETE FROM Variables_State_Value_List WHERE ID In(Select DISTINCT VSVL.Id " _
                 & "FROM Variables_State VS " _
                 & "INNER JOIN Variables_State_Value VSV On VS.ID = VSV.IDVariable " _
                 & "LEFT JOIN Variables_State_Value_List VSVL On VSV.Id = VSVL.IDVariableState " _
                 & "WHERE VS.IDGame = " & m_IDGame & " AND ISNULL(VSVL.ID, 0) <> 0 AND ISNULL(VSVL.Simulation, 0) = 1) "
            oDAL.ExecuteNonQuery(sSQL)

            sSQL = "DELETE FROM Variables_State_Value WHERE ID In(Select DISTINCT VSV.Id " _
                 & "FROM Variables_State VS " _
                 & "INNER JOIN Variables_State_Value VSV On VS.ID = VSV.IDVariable " _
                 & "LEFT JOIN Variables_State_Value_List VSVL On VSV.Id = VSVL.IDVariableState " _
                 & "WHERE VS.IDGame = " & m_IDGame & "AND ISNULL(VSV.Simulation, 0) = 1) "
            oDAL.ExecuteNonQuery(sSQL)

            oDAL.GetConnObject.Close()
            oDAL = Nothing

            g_MessaggioErrore = ""

            Sim1()
            If g_MessaggioErrore = "" Then
                Sim2()
            End If

            If g_MessaggioErrore = "" Then
                Sim3()
            End If

            If g_MessaggioErrore = "" Then
                Sim4()
            End If

            If g_MessaggioErrore = "" Then
                Sim5()
            End If

            If g_MessaggioErrore = "" Then
                Sim6()
            End If

            If g_MessaggioErrore = "" Then
                Sim7()
            End If

            If g_MessaggioErrore = "" Then
                Sim8()
            End If

            If g_MessaggioErrore = "" Then
                Sim9()
            End If

            If g_MessaggioErrore = "" Then
                Sim10()
            End If

            If g_MessaggioErrore = "" Then
                Sim11()
            End If

            If g_MessaggioErrore = "" Then
                Sim12()
            End If

            If g_MessaggioErrore = "" Then
                Sim13()
            End If

            If g_MessaggioErrore = "" Then
                Sim14()
            End If

            If g_MessaggioErrore = "" Then
                Sim15()
            End If

            If g_MessaggioErrore = "" Then
                Sim16()
            End If

            If g_MessaggioErrore = "" Then
                CalcolaPUV()
            End If

            If g_MessaggioErrore = "" Then
                CalcolaDashBoard()
            End If

            If g_MessaggioErrore = "" Then
                CalcolaEVA()
            End If

            If g_MessaggioErrore = "" Then
                CalcolaContiEconomiciPeL()
            End If

            If g_MessaggioErrore = "" Then
                CalculateVariabiliFlusso()
            End If

            If g_MessaggioErrore = "" Then
                BilancioRiclassificato()
            End If

            ' Fisso i valori nel database
            If g_MessaggioErrore = "" Then
                FixDataIntoDatabase()
            End If

            If g_MessaggioErrore = "" Then
                g_MessaggioErrore = "Simulation complete!"
            End If

        End If

        Return g_MessaggioErrore
    End Function

    Private Sub CleanDataGame()
        Try
            ' Procedo alla pulitaura di eventuali dati sporchi derivanti da elaborazioni errate, o probabilmente in fase di test
            Dim oprmID As New DBParameter("@IDGame", m_IDGame, DbType.Int16)
            Dim oprmCollection As New DBParameterCollection()
            oprmCollection.Add(oprmID)
            g_DAL.ExecuteNonQuery("sp_CleanDataGame", oprmCollection, CommandType.StoredProcedure)
        Catch ex As Exception
            g_MessaggioErrore = "CleanDataGame -> " & ex.Message
        End Try
    End Sub

    Private Sub FixDataIntoDatabase()
        Dim oDTTemp As DataTable

        ' VARIABLES_CALCULATE_VALUE
        If m_DataTableVariableCalculateValue.Rows.Count > 0 Then
            Dim consString As String = ConfigurationManager.ConnectionStrings("ConnectString").ConnectionString
            Using con As New SqlClient.SqlConnection(consString)
                Using sqlBulkCopy As New SqlClient.SqlBulkCopy(con)
                    oDTTemp = New DataTable
                    'Set the database table name
                    oDTTemp = m_DataTableVariableCalculateValue.Select("IDPeriod = " & m_IDPeriod).CopyToDataTable
                    sqlBulkCopy.DestinationTableName = "dbo.Variables_Calculate_Value"
                    con.Open()
                    sqlBulkCopy.WriteToServer(oDTTemp)
                    con.Close()
                End Using
            End Using
        End If

        ' DECISIONS_BOSS_VALUE
        If m_DataTableVariablesDecisionsBossValue.Rows.Count > 0 Then
            Dim consString As String = ConfigurationManager.ConnectionStrings("ConnectString").ConnectionString
            Using con As New SqlClient.SqlConnection(consString)
                If Not IsNothing(m_DataTableVariablesDecisionsBossValue.Select("IDPeriod = " & m_IDPeriodNext).FirstOrDefault) Then
                    Using sqlBulkCopy As New SqlClient.SqlBulkCopy(con)
                        oDTTemp = New DataTable
                        'Set the database table name
                        oDTTemp = m_DataTableVariablesDecisionsBossValue.Select("IDPeriod = " & m_IDPeriodNext).CopyToDataTable
                        If oDTTemp.Rows.Count > 0 Then
                            sqlBulkCopy.DestinationTableName = "dbo.Decisions_Boss_Value"
                            con.Open()
                            sqlBulkCopy.WriteToServer(oDTTemp)
                            con.Close()
                        End If
                    End Using
                End If
            End Using
        End If

        ' DECISIONS_BOSS_VALUE_LIST
        If m_DataTableVariablesDecisionsBossValueList.Rows.Count > 0 Then
            Dim consString As String = ConfigurationManager.ConnectionStrings("ConnectString").ConnectionString
            Using con As New SqlClient.SqlConnection(consString)
                If Not IsNothing(m_DataTableVariablesDecisionsBossValueList.Select("IDPeriod = " & m_IDPeriodNext).FirstOrDefault) Then
                    Using sqlBulkCopy As New SqlClient.SqlBulkCopy(con)
                        oDTTemp = New DataTable
                        'Set the database table name
                        oDTTemp = m_DataTableVariablesDecisionsBossValueList.Select("IDPeriod = " & m_IDPeriodNext).CopyToDataTable
                        If oDTTemp.Rows.Count > 0 Then
                            sqlBulkCopy.DestinationTableName = "dbo.Decisions_Boss_Value_List"
                            con.Open()
                            sqlBulkCopy.WriteToServer(oDTTemp)
                            con.Close()
                        End If
                    End Using
                End If
            End Using
        End If

        ' VARIABLES_STATE_VALUE
        If m_DataTableVariablesStateValue.Rows.Count > 0 Then
            Dim consString As String = ConfigurationManager.ConnectionStrings("ConnectString").ConnectionString
            Using con As New SqlClient.SqlConnection(consString)
                If Not IsNothing(m_DataTableVariablesStateValue.Select("IDPeriod = " & m_IDPeriodNext).FirstOrDefault) Then
                    Using sqlBulkCopy As New SqlClient.SqlBulkCopy(con)
                        oDTTemp = New DataTable
                        'Set the database table name
                        oDTTemp = m_DataTableVariablesStateValue.Select("IDPeriod = " & m_IDPeriodNext).CopyToDataTable
                        If oDTTemp.Rows.Count > 0 Then
                            sqlBulkCopy.DestinationTableName = "dbo.Variables_State_Value"
                            con.Open()
                            sqlBulkCopy.WriteToServer(oDTTemp)
                            con.Close()
                        End If
                    End Using
                End If
            End Using
        End If

        ' VARIABLES_STATE_VALUE_LIST
        If m_DataTableVariablesStateValueList.Rows.Count > 0 Then
            Dim consString As String = ConfigurationManager.ConnectionStrings("ConnectString").ConnectionString
            Using con As New SqlClient.SqlConnection(consString)
                If Not IsNothing(m_DataTableVariablesStateValueList.Select("IDPeriod = " & m_IDPeriodNext).FirstOrDefault) Then
                    Using sqlBulkCopy As New SqlClient.SqlBulkCopy(con)
                        oDTTemp = New DataTable
                        'Set the database table name
                        oDTTemp = m_DataTableVariablesStateValueList.Select("IDPeriod = " & m_IDPeriodNext).CopyToDataTable
                        If oDTTemp.Rows.Count > 0 Then
                            sqlBulkCopy.DestinationTableName = "dbo.Variables_State_Value_List"
                            con.Open()
                            sqlBulkCopy.WriteToServer(oDTTemp)
                            con.Close()
                        End If
                    End Using
                End If
            End Using
        End If

    End Sub

#Region "1 - 20 FUNZIONI DI CALCOLO"

    ''' <summary>
    ''' Totale dei macchinari attivi
    ''' </summary>
    Private Sub TotalActivMachi_001()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileCalc As String = "TotalActivMachi"
        Dim iTotalActivMachi As Integer = 0
        Dim iIDPlayer As Integer
        Dim sNomeVariabileEntrata As String = "PlantPerAge"

        Try
            ' Ciclo sui players presenti nel game
            If m_DataTablePlayers.Rows.Count > 0 Then
                ' Controllo la presenza della variabile da calcolare e mi faccio ritornare l'id 
                sNomeVariabileCalc = "TotalActivMachi"
                iIDVariabile = CheckVariableExists(sNomeVariabileCalc, m_IDGame)

                For Each oROWPlayer As DataRow In m_DataTablePlayers.Rows
                    iTotalActivMachi = 0
                    iIDPlayer = Nni(oROWPlayer("IDTeam"))

                    ' Recupero il valore di TotalActivMachi per il player e lo aggiorno in archivio
                    For Each oRowAge As DataRow In m_DataTableAge.Rows
                        ' Prendo la riga secca che mi interessa 
                        Dim sNomeVariabileLista As String = "var_" & Nz(oROWPlayer("TeamName")) & "_" & sNomeVariabileEntrata & "_" & Nz(oRowAge("AgeDescription"))
                        iTotalActivMachi += GetVariableState(sNomeVariabileEntrata, iIDPlayer, sNomeVariabileLista, 0, Nni(oRowAge("ID")))
                    Next
                    ' Salvo la variabile di stato/AGE in archivio
                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, iTotalActivMachi)
                Next
            End If

        Catch ex As Exception
            g_MessaggioErrore &= "TotalActivMachi -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Importazioni pendenti ricevute
    ''' </summary>
    Private Sub PortfDelivNIC_002()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileCalc As String = "PortfDelivNIC"
        Dim dValueCalc As Double
        Dim iIDPlayer As Integer
        Dim sNomeVariabileEntrata As String = "PurchDelivNIC"

        Try
            ' Ciclo sui players presenti nel game
            If m_DataTablePlayers.Rows.Count > 0 Then
                ' Controllo la presenza della variabile da calcolare e mi faccio ritornare l'id 
                iIDVariabile = CheckVariableExists(sNomeVariabileCalc, m_IDGame)

                For Each oROWPlayer As DataRow In m_DataTablePlayers.Rows
                    dValueCalc = 0
                    iIDPlayer = Nni(oROWPlayer("IDTeam"))

                    ' Recupero il valore di TotalActivMachi per il player e lo aggiorno in archivio
                    For Each oRowItem As DataRow In m_DataTableItems.Rows

                        ' Prendo la riga secca che mi interessa 
                        Dim sNomeVariabileLista As String = "var_" & Nz(oROWPlayer("TeamName")) & "_" & sNomeVariabileEntrata & "_" & Nz(oRowItem("VariableName"))
                        dValueCalc = GetVariableState(sNomeVariabileEntrata, iIDPlayer, sNomeVariabileLista, Nni(oRowItem("IDVariable")), 0)

                        SaveVariableValue(iIDVariabile, iIDPlayer, Nni(oRowItem("IDVariable")), 0, dValueCalc)
                    Next
                Next
            End If
        Catch ex As Exception
            g_MessaggioErrore &= "PortfDelivNIC -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Totale dei leasing
    ''' </summary>
    Private Sub GlobaActuaLease_003()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileCalc As String = "GlobaActuaLease"
        Dim sValueCalc As String
        Dim sNomeVariabileEntrata As String = "LeasePerAge"
        Dim iIDPlayer As Integer

        Try
            ' Ciclo sui players presenti nel game
            If m_DataTablePlayers.Rows.Count > 0 Then
                ' Controllo la presenza della variabile da calcolare e mi faccio ritornare l'id 
                iIDVariabile = CheckVariableExists(sNomeVariabileCalc, m_IDGame)

                For Each oROWPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oROWPlayer("IDTeam"))

                    ' Prendo la riga secca che mi interessa 
                    Dim sNomeVariabileLista As String = "var_" & Nz(oROWPlayer("TeamName")) & "_" & sNomeVariabileEntrata & "_1"
                    sValueCalc = GetVariableState(sNomeVariabileEntrata, iIDPlayer, sNomeVariabileLista, 0, 0)
                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValueCalc)
                Next
            End If
        Catch ex As Exception
            g_MessaggioErrore &= "GlobaActuaLease_003 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Media degli investimenti tecnologici annui
    ''' </summary>
    Private Sub AveraTechnYear_004()
        Dim iIDVariabile As Integer
        Dim sValueCalc As String = ""
        Dim sNomeVariabileCalc As String = "AveraTechnYear"
        Dim sNomeVariabileEntrata As String = "AveraTechnInves"
        Dim iIDPlayer As Integer = 0

        Try
            ' Ciclo sui players presenti nel game
            If m_DataTablePlayers.Rows.Count > 0 Then
                ' Controllo la presenza della variabile da calcolare e mi faccio ritornare l'id 
                iIDVariabile = CheckVariableExists(sNomeVariabileCalc, m_IDGame)
                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sValueCalc = GetVariableState(sNomeVariabileEntrata, iIDPlayer, Nni(oRowPlayer("IDTeam")), 0, 0)
                    sValueCalc = Nn(sValueCalc) * (m_LunghezzaPeriodo / 12)
                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValueCalc)
                Next
            End If
        Catch ex As Exception
            g_MessaggioErrore &= "AveraTechnYear_004 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Tabella sulla media dell'inv.tecnologico
    ''' </summary>
    Private Sub TableAveraTechn_005()
        Dim iIDVariabile As Integer
        Dim dValueCalc As String = ""
        Dim sNomeVariabileCalc As String = "TableAveraTechn"
        Dim sNomeVariabileEntrata As String = "AveraTechnYear"
        Dim iIDPlayer As Integer = 0

        Try
            ' Ciclo sui players presenti nel game
            If m_DataTablePlayers.Rows.Count > 0 Then
                ' Controllo la presenza della variabile da calcolare e mi faccio ritornare l'id 
                iIDVariabile = CheckVariableExists(sNomeVariabileCalc, m_IDGame)
                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))

                    dValueCalc = GetVariableDefineValue(sNomeVariabileEntrata, iIDPlayer, 0, 0)
                    dValueCalc = CalculateInterpolation(sNomeVariabileCalc, dValueCalc)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, dValueCalc)
                Next
            End If

        Catch ex As Exception
            g_MessaggioErrore &= "TableAveraTechn_005 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Costo di un leasing per periodo
    ''' </summary>
    Private Sub NewLeaseCost_006()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileUscita As String = "NewLeaseCost"
        Dim sValore As String
        Dim sNomeVariabileEntrata As String = "LeasePlantCost"

        Try
            iIDVariabile = CheckVariableExists(sNomeVariabileUscita, m_IDGame)

            sValore = Nn(GetVariableBoss(sNomeVariabileEntrata, "")) * (m_LunghezzaPeriodo / 12)
            SaveVariableValue(iIDVariabile, 0, 0, 0, sValore)
        Catch ex As Exception
            g_MessaggioErrore &= "NewLeaseCost_006 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Incremento costo mat.1e locali
    ''' </summary>
    Private Sub IncreRawEurop_007()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileUscita As String = "IncreRawEurop"
        Dim sValore As String
        Dim RawEuropCost As String
        Dim InflaRateProdu As String
        Dim sNomeVariabileEntrata As String = "InflaRateProdu"

        Try

            If m_DataTableItems.Rows.Count > 0 Then
                ' Controllo la presenza della variabile da calcolare e mi faccio ritornare l'id 
                iIDVariabile = CheckVariableExists(sNomeVariabileUscita, m_IDGame)

                For Each oRowItem As DataRow In m_DataTableItems.Rows
                    ' Recupero i valori delle varibili che mi servono per il calcolo
                    RawEuropCost = GetVariableBoss("RawEuropCost", "RawEuropCost_" & oRowItem("VariableName"))
                    InflaRateProdu = GetVariableBoss(sNomeVariabileEntrata, "")
                    sValore = Math.Truncate(Nn(RawEuropCost) * (Nn(InflaRateProdu) / ((12 / m_LunghezzaPeriodo)) / 100))
                    SaveVariableValue(iIDVariabile, 0, Nni(oRowItem("IDVariable")), 0, sValore)
                Next
            End If
        Catch ex As Exception
            g_MessaggioErrore &= "IncreRawEurop_007 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Incremento costo mat.1e import
    ''' </summary>
    Private Sub IncreRawNIC_008()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileUscita As String = "IncreRawNIC"
        Dim sValore As String
        Dim sRawEuropCostValue As String
        Dim sInflaRateProduValue As String
        Dim sNomeVariabileEntrata As String = "RawNICCost"

        Try
            If m_DataTableItems.Rows.Count > 0 Then
                ' Controllo la presenza della variabile da calcolare e mi faccio ritornare l'id 
                iIDVariabile = CheckVariableExists(sNomeVariabileUscita, m_IDGame)

                For Each oRowItem As DataRow In m_DataTableItems.Rows
                    ' Recupero i valori delle varibili che mi servono per il calcolo
                    sRawEuropCostValue = GetVariableBoss("RawEuropCost", "RawEuropCost_" & oRowItem("VariableName"))
                    sInflaRateProduValue = GetVariableBoss("InflaRateProdu", "")
                    sValore = Math.Truncate(Nn(sRawEuropCostValue) * (Nn(sInflaRateProduValue) / ((12 / m_LunghezzaPeriodo)) / 100))
                    SaveVariableValue(iIDVariabile, 0, Nni(oRowItem("iDVariable")), 0, sValore)
                Next
            End If
        Catch ex As Exception
            g_MessaggioErrore &= "IncreRawNIC_008 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Veicoli ceduti concessi
    ''' </summary>
    Private Sub VehicScrapAllow_009()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "VehicScrapAllow"
        Dim sValore As String
        Dim iIDPlayer As Integer

        Try
            ' Ciclo sui players presenti nel game
            If m_DataTablePlayers.Rows.Count > 0 Then
                ' Controllo la presenza della variabile da calcolare e mi faccio ritornare l'id 
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oROWPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = oROWPlayer("IDTeam")
                    sValore = 0
                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next
            End If

        Catch ex As Exception
            g_MessaggioErrore &= "VehicScrapAllow_009 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Affitti non rinnovati - negozi perifer.
    ''' </summary>
    Private Sub PerifScrapAllow_010()
        Dim iIDPlayer As Integer
        Dim iIDVariabile As Integer
        Dim sNomeVariabile As String
        Dim sValore As String
        Dim sPerifStoreRequiValue As String = "0"
        Dim sTotalPerifStoreValue As String = "0"
        Dim sTotalCentrStoreValue As String = "0"

        Try
            If m_DataTableItems.Rows.Count > 0 Then
                sNomeVariabile = "PerifScrapAllow"
                iIDVariabile = CheckVariableExists(sNomeVariabile, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))

                    sPerifStoreRequiValue = Nz(GetVariableDecisionPlayer(m_IDGame, "PerifStoreRequi", iIDPlayer, 0), "0")
                    sTotalCentrStoreValue = Nz(GetVariableState("TotalCentrStore", iIDPlayer, "", 0, 0), "0")
                    sTotalPerifStoreValue = Nz(GetVariableState("TotalPerifStore", iIDPlayer, "", 0, 0), "0")

                    If Nn(sPerifStoreRequiValue) < 0 AndAlso Nn(sTotalPerifStoreValue) + Nn(sTotalCentrStoreValue) > 1 Then
                        sValore = Math.Min(-(Nn(sPerifStoreRequiValue)), Nn(sTotalPerifStoreValue))
                    Else
                        sValore = "0"
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next
            End If
        Catch ex As Exception
            g_MessaggioErrore &= "PerifScrapAllow_010 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Totali veicoli operativi
    ''' </summary>
    Private Sub OperaVehicUsed_011()
        Dim iIDVariabile As Integer
        Dim sNomeVariabile As String
        Dim sValore As String
        Dim sTotalVehicOwnedValue As String = "0"
        Dim iIDPlayer As Integer
        Dim iIDAge As Integer
        Dim sNomeVariabileLista As String
        Dim sNomeTeam As String

        Try
            If m_DataTableItems.Rows.Count > 0 Then
                sNomeVariabile = "OperaVehicUsed"
                iIDVariabile = CheckVariableExists(sNomeVariabile, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sNomeTeam = Nz(oRowPlayer("TeamName"))
                    sValore = "0"
                    For Each oRowAge As DataRow In m_DataTableAge.Rows
                        iIDAge = Nni(oRowAge("ID"))
                        sNomeVariabileLista = "var_" & sNomeTeam & "_TotalVehicOwned_" & iIDAge
                        sValore = Nn(sValore) + Nn(GetVariableState("TotalVehicOwned", iIDPlayer, sNomeVariabileLista, 0, iIDAge))
                    Next
                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next
            End If
        Catch ex As Exception
            g_MessaggioErrore &= "OperaVehicUsed_011 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Veicoli venduti
    ''' </summary>
    Private Sub VehicScrapAge_012()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "VehicScrapAge"
        Dim sValore As String
        Dim iIDPlayer As Integer
        Dim sVehicScrapAllowValue As String
        Dim iIDAge As Integer
        Dim dCars As Double
        Dim sVehicScrapAgeValue As String
        Dim sTotalVehicOwnedValue As String

        Try
            ' Ciclo sui players presenti nel game
            If m_DataTablePlayers.Rows.Count > 0 Then
                ' Controllo la presenza della variabile da calcolare e mi faccio ritornare l'id 
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oROWPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = oROWPlayer("IDTeam")
                    For Each oRowAge As DataRow In m_DataTableAge.Rows
                        iIDAge = Nni(oRowAge("ID"))
                        sValore = "0"
                        SaveVariableValue(iIDVariabile, iIDPlayer, 0, iIDAge, sValore)
                    Next
                Next

                For Each oROWPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = oROWPlayer("IDTeam")

                    sVehicScrapAllowValue = Nz(GetVariableDefineValue("VehicScrapAllow", iIDPlayer, 0, 0))

                    If Nn(sVehicScrapAllowValue) > 0 Then
                        dCars = sVehicScrapAllowValue
                        For iIndexAge = 9 To 0 Step -1
                            sTotalVehicOwnedValue = Nz(GetVariableDefineValue("TotalVehicOwned", iIDPlayer, 0, iIndexAge), "0")
                            sVehicScrapAgeValue = Math.Min(Nn(sTotalVehicOwnedValue), dCars)
                            SaveVariableValue(iIDVariabile, iIDPlayer, 0, iIndexAge, sVehicScrapAgeValue)
                            dCars -= Nn(sTotalVehicOwnedValue)
                        Next
                    End If
                Next
            End If

        Catch ex As Exception
            g_MessaggioErrore &= "VehicScrapAge_012 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Costi di Produzione (/unitari)
    ''' </summary>
    Private Sub TotalProduCosts_013()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TotalProduCosts"
        Dim sValore As String
        Dim sProduUnitCostValue As String
        Dim iIDItem As Integer
        Dim iIDPlayer As Integer
        Dim sNomeVariabileLista As String

        Try
            If m_DataTableItems.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sNomeVariabileLista = "ProduUnitCost_" & oRowItem("VariableName")
                        sProduUnitCostValue = Nz(GetVariableBoss("ProduUnitCost", sNomeVariabileLista), "0")
                        sValore = sProduUnitCostValue
                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next
                Next
            End If
        Catch ex As Exception
            g_MessaggioErrore &= "TotalProduCosts_013 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Incremento del costo di produzione
    ''' </summary>
    Private Sub IncreProduCost_014()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "IncreProduCost"
        Dim sValore As String
        Dim sNomeVariabileLista As String
        Dim sProduUnitCostValue As String
        Dim sInflaRateProduValue As String
        Dim iIDItem As Integer

        Try
            If m_DataTableItems.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowItem As DataRow In m_DataTableItems.Rows
                    iIDItem = Nni(oRowItem("IDVariable"))
                    sNomeVariabileLista = "ProduUnitCost_" & oRowItem("VariableName")
                    sProduUnitCostValue = GetVariableBoss("ProduUnitCost", sNomeVariabileLista)
                    sInflaRateProduValue = GetVariableBoss("InflaRateProdu", "")
                    sValore = Nn(sProduUnitCostValue) * (Nn(sInflaRateProduValue) / 100)
                    SaveVariableValue(iIDVariabile, 0, iIDItem, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "IncreProduCost_014 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Scarica il costo di produzione precedente
    ''' </summary>
    Private Sub DischPreviCosts_015()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DischPreviCosts"
        Dim sValore As String
        Dim iIDPlayer As Integer
        Dim sNomeVariabileLista As String
        Dim iIDItem As Integer

        Try
            If m_DataTableItems.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sNomeVariabileLista = "var_" & Nz(oRowPlayer("TeamName")) & "_PreviProduCosts_" & Nz(oRowItem("VariableName"))
                        sValore = Nz(GetVariableState("PreviProduCosts", iIDPlayer, sNomeVariabileLista, iIDItem, 0), "0")

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "DischPreviCosts_015 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Investimento medio annual in customer service
    ''' </summary>
    Private Sub AveraTrainYear_016()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "AveraTrainYear"
        Dim sValore As String
        Dim iIDPlayer As Integer
        Dim sAveraTrainInvesValue As String

        Try
            If m_DataTableItems.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sAveraTrainInvesValue = Nz(GetVariableState("AveraTrainInves", iIDPlayer, "", 0, 0), "0")
                    sValore = Nn(sAveraTrainInvesValue) * (m_LunghezzaPeriodo / 12)
                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "AveraTrainYear_016 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Tabella sull'inv. di servizio al cliente
    ''' </summary>
    Private Sub TableAveraTrain_017()
        Dim iIDVariabile As Integer
        Dim dValueCalc As String = ""
        Dim sNomeVariabileCalc As String = "TableAveraTrain"
        Dim sNomeVariabileEntrata As String = "AveraTrainYear"
        Dim iIDPlayer As Integer = 0

        Try
            ' Ciclo sui players presenti nel game
            If m_DataTablePlayers.Rows.Count > 0 Then
                ' Controllo la presenza della variabile da calcolare e mi faccio ritornare l'id 
                iIDVariabile = CheckVariableExists(sNomeVariabileCalc, m_IDGame)
                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))

                    dValueCalc = GetVariableDefineValue(sNomeVariabileEntrata, iIDPlayer, 0, 0)
                    dValueCalc = CalculateInterpolation(sNomeVariabileCalc, dValueCalc)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, dValueCalc)
                Next
            End If

        Catch ex As Exception
            g_MessaggioErrore &= "TableAveraTrain_016 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Scarica il prezzo di vendita precedente
    ''' </summary>
    Private Sub DischPreviPrice_018()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DischPreviPrice"
        Dim sValore As String
        Dim iIDPlayer As Integer
        Dim iIDItem As Integer
        Dim sSalesPriceRequiValue As String
        Dim sNomeVariabileLista As String
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))

                        ' Recupero il valore della variabile decisionale del player SalesPriceRequi
                        sNomeVariabileLista = "var_" & sTeamName & "_SalesPriceRequi_" & Nz(oRowItem("VariableName"))
                        sSalesPriceRequiValue = GetVariableDecisionPlayer(m_IDGame, "SalesPriceRequi", iIDPlayer, iIDItem)

                        If Nn(sSalesPriceRequiValue) = 0 Then
                            sValore = 0
                        Else
                            sNomeVariabileLista = "var_" & Nz(oRowPlayer("TeamName")) & "_PreviSalesPrice_" & Nz(oRowItem("VariableName"))
                            sValore = Nz(GetVariableState("PreviSalesPrice", iIDPlayer, sNomeVariabileLista, iIDItem, 0), "0")
                        End If
                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "DischPreviPrice_018 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Costo di un resp. di staff per periodo
    ''' </summary>
    Private Sub NewCostStaff_019()
        Dim sPersoCostStaffValue As String
        Dim sValore As String
        Dim sNomeVariabileEntrata As String = "NewCostStaff"
        Dim iIDVariabile As Integer

        Try
            iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

            sPersoCostStaffValue = GetVariableBoss("PersoCostStaff", "")

            sValore = Nn(sPersoCostStaffValue) * (m_LunghezzaPeriodo / 12)

            SaveVariableValue(iIDVariabile, 0, 0, 0, sValore)
        Catch ex As Exception
            g_MessaggioErrore &= "NewCostStaff_019 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Costo di un venditore per periodo
    ''' </summary>
    Private Sub NewCostPoint_020()
        Dim sPersoCostPointValue As String
        Dim sValore As String
        Dim sNomeVariabileEntrata As String = "NewCostPoint"
        Dim iIDVariabile As Integer

        Try
            iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)
            sPersoCostPointValue = Nz(GetVariableBoss("PersoCostPoint", ""), "0")
            sValore = Nn(sPersoCostPointValue) * (m_LunghezzaPeriodo / 12)
            SaveVariableValue(iIDVariabile, 0, 0, 0, sValore)
        Catch ex As Exception
            g_MessaggioErrore &= "NewCostPoint_020 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

#End Region

#Region "21 - 40 FUNZIONI DI CALCOLO"

    ''' <summary>
    ''' Affitto di un negozio periferico
    ''' </summary>
    Private Sub RentPerioPerif_101()
        Dim sPersoCostPointValue As String
        Dim sValore As String
        Dim sNomeVariabileEntrata As String = "RentPerioPerif"
        Dim iIDVariabile As Integer

        Try
            iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)
            sPersoCostPointValue = Nz(GetVariableBoss("NewStoreCost", ""), "0")
            sValore = Nn(sPersoCostPointValue) * (m_LunghezzaPeriodo / 12)
            SaveVariableValue(iIDVariabile, 0, 0, 0, sValore)
        Catch ex As Exception
            g_MessaggioErrore &= "RentPerioPerif_101 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Affitto di un negozio centrale
    ''' </summary>
    Private Sub RentPerioCentr_102()
        Dim sPersoCostPointValue As String
        Dim sValore As String
        Dim sNomeVariabileEntrata As String = "RentPerioCentr"
        Dim iIDVariabile As Integer

        Try
            iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)
            sPersoCostPointValue = Nz(GetVariableBoss("NewCentrCost", ""), "0")
            sValore = Nn(sPersoCostPointValue) * (m_LunghezzaPeriodo / 12)
            SaveVariableValue(iIDVariabile, 0, 0, 0, sValore)
        Catch ex As Exception
            g_MessaggioErrore &= "RentPerioCentr_102 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Prezzo di vendita concesso
    ''' </summary>
    Private Sub SalesPriceAllow_103()
        Dim iIDVariabile As Integer
        Dim sValore As String = "SalesPriceAllow"
        Dim sVariabileEntrata As String = "SalesPriceAllow"
        Dim PreviProduCosts As String = "0"
        Dim PreviSalesPrice As String = "0"
        Dim SalesPriceRequi As String = "0"
        Dim iIDPlayer As Integer = 0
        Dim iIDItem As Integer = 0
        Dim sTeamName As String
        Dim sNomeVariabileLista As String

        Try
            ' Ciclo sui players presenti nel game
            If m_DataTablePlayers.Rows.Count > 0 Then
                ' Controllo la presenza della variabile da calcolare e mi faccio ritornare l'id 
                iIDVariabile = CheckVariableExists(sVariabileEntrata, m_IDGame)
                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))

                        ' Recupero il valore della variabile decisionale BOSS PreviProduCosts
                        sNomeVariabileLista = "var_" & sTeamName & "_PreviProduCosts_" & Nz(oRowItem("VariableName"))
                        PreviProduCosts = Nz(GetVariableState("PreviProduCosts", iIDPlayer, sNomeVariabileLista, iIDItem, 0), "0")

                        ' Recupero il valore della variabile decisionale BOSS PreviSalesPrice
                        sNomeVariabileLista = "var_" & sTeamName & "_PreviSalesPrice_" & Nz(oRowItem("VariableName"))
                        PreviSalesPrice = Nz(GetVariableState("PreviSalesPrice", iIDPlayer, sNomeVariabileLista, iIDItem, 0), "0")

                        ' Recupero il valore della variabile decisionale del player SalesPriceRequi
                        sNomeVariabileLista = "var_" & sTeamName & "_SalesPriceRequi_" & Nz(oRowItem("VariableName"))
                        SalesPriceRequi = GetVariableDecisionPlayer(m_IDGame, "SalesPriceRequi", iIDPlayer, iIDItem)

                        ' Calcolo il valore effettivo
                        sValore = Max(Nn(PreviProduCosts, 0), Min(Nn(PreviSalesPrice) * 2.5, Nn(SalesPriceRequi)))
                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next
                Next
            End If

        Catch ex As Exception
            g_MessaggioErrore &= "SalesPriceAllow_103 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Investimento medio annuo in pubblictà
    ''' </summary>
    Private Sub AveraAdverYear_104()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "AveraAdverYear"
        Dim sValore As String
        Dim iIDPlayer As Integer

        Dim sAveraAdverInvesValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))

                    sAveraAdverInvesValue = Nz(GetVariableState("AveraAdverInves", iIDPlayer, "", 0, 0), "0")
                    sValore = Nn(sAveraAdverInvesValue) * (m_LunghezzaPeriodo / 12)
                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)

                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "AveraAdverYear_104 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Tabella sull'inv.medio in pubblicità
    ''' </summary>
    Private Sub TableAveraAdver_105()
        Dim iIDVariabile As Integer
        Dim dValueCalc As String = ""
        Dim sNomeVariabileCalc As String = "TableAveraAdver"
        Dim sNomeVariabileEntrata As String = "AveraAdverYear"
        Dim iIDPlayer As Integer = 0

        Try
            ' Ciclo sui players presenti nel game
            If m_DataTablePlayers.Rows.Count > 0 Then
                ' Controllo la presenza della variabile da calcolare e mi faccio ritornare l'id 
                iIDVariabile = CheckVariableExists(sNomeVariabileCalc, m_IDGame)
                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))

                    dValueCalc = GetVariableDefineValue(sNomeVariabileEntrata, iIDPlayer, 0, 0)
                    dValueCalc = CalculateInterpolation(sNomeVariabileCalc, dValueCalc)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, dValueCalc)
                Next
            End If

        Catch ex As Exception
            g_MessaggioErrore &= "TableAveraAdver_105 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Investimenti ecologici annui medi
    ''' </summary>
    Private Sub AveraGreenYear_106()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "AveraGreenYear"
        Dim sValore As String
        Dim iIDPlayer As Integer
        Dim sAveraGreenInvesValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))

                    sAveraGreenInvesValue = Nz(GetVariableState("AveraGreenInves", iIDPlayer, "", 0, 0), "0")
                    sValore = Nn(sAveraGreenInvesValue) * (m_LunghezzaPeriodo / 12)
                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "AveraGreenYear_106 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Tabella sugli inv.ecologici medi
    ''' </summary>
    Private Sub TableAveraGreen_107()
        Dim iIDVariabile As Integer
        Dim dValueCalc As String = ""
        Dim sNomeVariabileCalc As String = "TableAveraGreen"
        Dim sNomeVariabileEntrata As String = "AveraGreenYear"
        Dim iIDPlayer As Integer = 0

        Try
            ' Ciclo sui players presenti nel game
            If m_DataTablePlayers.Rows.Count > 0 Then
                ' Controllo la presenza della variabile da calcolare e mi faccio ritornare l'id 
                iIDVariabile = CheckVariableExists(sNomeVariabileCalc, m_IDGame)
                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))

                    dValueCalc = GetVariableDefineValue(sNomeVariabileEntrata, iIDPlayer, 0, 0)
                    dValueCalc = CalculateInterpolation(sNomeVariabileCalc, dValueCalc)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, dValueCalc)
                Next
            End If

        Catch ex As Exception
            g_MessaggioErrore &= "TableAveraGreen_107 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Scarica la quota di mercato
    ''' </summary>
    Private Sub DischMarkeShare_108()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DischMarkeShare"
        Dim sValore As String
        Dim iIDPlayer As Integer
        Dim sNomeTeam As String
        Dim iIDItem As Integer
        Dim sPreviMarkeShareValue As String
        Dim sNomeVariabileLista As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sNomeTeam = Nz(oRowPlayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sNomeVariabileLista = "var_" & sNomeTeam & "_PreviMarkeShare_" & Nz(oRowItem("VariableName"))
                        sPreviMarkeShareValue = Nz(GetVariableState("PreviMarkeShare", iIDPlayer, sNomeVariabileLista, iIDItem, 0), "0")
                        sValore = sPreviMarkeShareValue
                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "DischMarkeShare_108 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Prestiti concessi
    ''' </summary>
    Private Sub LoansAllow_109()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "LoansAllow"

        Dim sValore As String
        Dim iIDPlayer As Integer

        Dim sLoansRequiValue As String
        Dim sActuaMaximLoansValue As String
        Dim sLoansLevelValue As String
        Dim sBankLevelValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))

                    sLoansRequiValue = Nz(GetVariableDecisionPlayer(m_IDGame, "LoansRequi", iIDPlayer, 0), "0")
                    sActuaMaximLoansValue = Nz(GetVariableState("ActuaMaximLoans", iIDPlayer, "", 0, 0), "0")
                    sLoansLevelValue = Nz(GetVariableState("LoansLevel", iIDPlayer, "", 0, 0), "0")

                    If Nn(sLoansRequiValue) >= 0 Then
                        sValore = Max(0, Min(Nn(sLoansRequiValue), Nn(sActuaMaximLoansValue) - Nn(sLoansLevelValue)))
                    Else
                        sBankLevelValue = Nz(GetVariableState("BankLevel", iIDPlayer, "", 0, 0), "0")
                        sValore = Max(Max(-Nn(sBankLevelValue), Nn(sLoansRequiValue)), -Nn(sLoansLevelValue))
                    End If
                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)

                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "LoansAllow_109 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Materie prime locali da finanziare
    ''' </summary>
    Private Sub PurchEuropFinan_110()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "PurchEuropFinan"

        Dim sValore As String
        Dim iIDPlayer As Integer

        Dim iIDItem As Integer
        Dim sTeamName As String

        Dim sRawEuropCostValue As String = "0"
        Dim sPurchEuropRequiValue As String = "0"
        Dim sRicarTermiMeseValue As String = "0"
        Dim sDiritRitarPagamValue As String = "0"
        Dim sLagPaymeEuropValue As String = "0"
        Dim sDelayPaymeEuropValue As String = "0"

        Dim sNomeVariabileLista As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = "0"

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))

                        sNomeVariabileLista = "RawEuropCost_" & Nz(oRowItem("VariableName"))
                        sRawEuropCostValue = GetVariableBoss("RawEuropCost", sNomeVariabileLista)

                        sNomeVariabileLista = "var_" & sTeamName & "_PurchEuropRequi_" & Nz(oRowItem("VariableName"))
                        sPurchEuropRequiValue = GetVariableDecisionPlayer(m_IDGame, "PurchEuropRequi", iIDPlayer, iIDItem)

                        sNomeVariabileLista = "RicarTermiMese_" & sTeamName
                        sRicarTermiMeseValue = GetVariableBoss("RicarTermiMese", sNomeVariabileLista)

                        sDiritRitarPagamValue = GetVariableBoss("DiritRitarPagam", "")

                        sLagPaymeEuropValue = GetVariableBoss("LagPaymeEurop", "")

                        sNomeVariabileLista = "var_" & sTeamName & "DelayPaymeEurop" & Nz(oRowItem("VariableName"))
                        sDelayPaymeEuropValue = GetVariableDecisionPlayer(m_IDGame, "DelayPaymeEurop", iIDPlayer, iIDItem)

                        sValore = Nn(sValore) + (Nn(sRawEuropCostValue) * Nn(sPurchEuropRequiValue))

                    Next

                    sValore = Nn(sValore) * (1 + Nn(sRicarTermiMeseValue) / 100 * Nn(sDiritRitarPagamValue) * Min(m_LunghezzaPeriodo - Nn(sLagPaymeEuropValue), Nn(sDelayPaymeEuropValue)))
                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "PurchEuropFinan_110 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Materie prime import da finanziare
    ''' </summary>
    Private Sub PurchNICFinan_111()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "PurchNICFinan"

        Dim PurchNICFinan As String
        Dim iIDPlayer As Integer

        Dim iIDItem As Integer
        Dim sItemName As String
        Dim sTeamName As String

        Dim PurchNICRequi As String
        Dim RawNICCost As String
        Dim LagDelivNIC As String

        Dim sNomeVariabileLista As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    PurchNICFinan = "0"

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))
                        PurchNICRequi = GetVariableDecisionPlayer(m_IDGame, "PurchNICRequi", iIDPlayer, iIDItem)

                        sNomeVariabileLista = "RawNICCost_" & sItemName
                        RawNICCost = GetVariableBoss("RawNICCost", sNomeVariabileLista)

                        LagDelivNIC = GetVariableBoss("LagDelivNIC", "")

                        PurchNICFinan = Nn(PurchNICFinan) + (Nn(PurchNICRequi) * Nn(RawNICCost)) * ((m_LunghezzaPeriodo - Nn(LagDelivNIC)) / m_LunghezzaPeriodo)
                        SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, PurchNICFinan)
                    Next

                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "PurchNICFinan_111 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try

    End Sub

    ''' <summary>
    ''' Produzione da finanziare
    ''' </summary>
    Private Sub ProduCashFinan_112()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "ProduCashFinan"

        Dim sValore As String
        Dim iIDPlayer As Integer

        Dim iIDItem As Integer
        Dim sTeamName As String

        Dim sProduUnitCostValue As String = "0"
        Dim sProduBasicRequiValue As String = "0"
        Dim sLagPaymeProduValue As String = "0"
        Dim sDiritRitarPagamValue As String = "0"
        Dim sDelayPaymeProduValue As String = "0"
        Dim sRicarTermiMeseValue As String = "0"

        Dim sNomeVariabileLista As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = "0"

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))

                        sNomeVariabileLista = "ProduUnitCost_" & Nz(oRowItem("VariableName"))
                        sProduUnitCostValue = GetVariableBoss("ProduUnitCost", sNomeVariabileLista)

                        sProduBasicRequiValue = GetVariableDecisionPlayer(m_IDGame, "ProduBasicRequi", iIDPlayer, iIDItem)

                        sLagPaymeProduValue = GetVariableBoss("LagPaymeProdu", "")

                        sDiritRitarPagamValue = GetVariableBoss("DiritRitarPagam", "")

                        sDelayPaymeProduValue = GetVariableDecisionPlayer(m_IDGame, "DelayPaymeProdu", iIDPlayer, iIDItem)

                        sNomeVariabileLista = "RicarTermiMese_" & Nz(oRowItem("VariableName"))
                        sRicarTermiMeseValue = GetVariableBoss("RicarTermiMese", sNomeVariabileLista)

                        sValore = Nn(sValore) + Nn(sProduUnitCostValue) * Nn(sProduBasicRequiValue) * (((m_LunghezzaPeriodo - Nn(sLagPaymeProduValue)) / m_LunghezzaPeriodo))
                    Next

                    sValore = Nn(sValore) * (1 + Nn(sRicarTermiMeseValue) / 100 * Nn(sDiritRitarPagamValue) * Min(m_LunghezzaPeriodo - Nn(sLagPaymeProduValue), Nn(sDelayPaymeProduValue)))
                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "ProduCashFinan_112 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Promozioni da finanziare
    ''' </summary>
    Private Sub PromoCashFinan_113()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "PromoCashFinan"

        Dim sValore As String
        Dim iIDPlayer As Integer

        Dim iIDItem As Integer
        Dim sTeamName As String

        Dim sMarkeExpenRequiValue As String
        Dim sLagPaymePromoValue As String
        Dim sDiritRitarPagamValue As String
        Dim sDelayPaymePromoValue As String
        Dim sRicarTermiMeseValue As String

        Dim sNomeVariabileLista As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = "0"

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))

                        sMarkeExpenRequiValue = GetVariableDecisionPlayer(m_IDGame, "MarkeExpenRequi", iIDPlayer, iIDItem)

                        sLagPaymePromoValue = GetVariableBoss("LagPaymePromo", "")

                        sDiritRitarPagamValue = GetVariableBoss("DiritRitarPagam", "")

                        sDelayPaymePromoValue = GetVariableDecisionPlayer(m_IDGame, "DelayPaymePromo", iIDPlayer, iIDItem)

                        sNomeVariabileLista = "RicarTermiMese_" & Nz(oRowItem("VariableName"))
                        sRicarTermiMeseValue = GetVariableBoss("RicarTermiMese", sNomeVariabileLista)

                        sValore = Nn(sValore) + (Nn(sMarkeExpenRequiValue) * (((m_LunghezzaPeriodo - Nn(sLagPaymePromoValue)) / m_LunghezzaPeriodo)))

                        sValore = Nn(sValore) * (1 + Nn(sRicarTermiMeseValue) / 100 * Nn(sDiritRitarPagamValue) * Min(m_LunghezzaPeriodo - Nn(sLagPaymePromoValue), Nn(sDelayPaymePromoValue)))

                        SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                    Next
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "ProduCashFinan_112 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Acquisti da finanziare
    ''' </summary>
    Private Sub ExtraProduFinan_114()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "ExtraProduFinan"

        Dim sValore As String = "0"

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sNomeItem As String

        Dim ForwaPaymeExtra As Double
        Dim DiscoTermiMese As Double
        Dim DiritScontPagam As Double
        Dim LagPaymeExtra As Double
        Dim ExtraRequi As Double
        Dim ThresDiscoExtra As Double
        Dim ExtraProduCost As Double
        Dim DiscoLevelExtra As Double

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                DiscoTermiMese = Nn(GetVariableBoss("DiscoTermiMese", ""))
                DiritScontPagam = Nn(GetVariableBoss("DiritScontPagam", ""))
                LagPaymeExtra = Nn(GetVariableBoss("LagPaymeExtra", ""))
                DiscoLevelExtra = Nn(GetVariableBoss("DiscoLevelExtra", ""))

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows ' CICLO SUI PLAYERS
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = "0"

                    ForwaPaymeExtra = Nn(GetVariableDecisionPlayer(m_IDGame, "ForwaPaymeExtra", iIDPlayer, 0))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows ' CICLO SUGLI ITEMS
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sNomeItem = Nz(oRowItem("VariableName"))

                        ExtraRequi = Nn(GetVariableDecisionPlayer(m_IDGame, "ExtraRequi", iIDPlayer, iIDItem))
                        ThresDiscoExtra = Nn(GetVariableBoss("ThresDiscoExtra", "ThresDiscoExtra_" & sNomeItem))
                        ExtraProduCost = Nn(GetVariableBoss("ExtraProduCost", "ExtraProduCost_" & sNomeItem))

                        sValore = Nn(sValore) + ((100 - ForwaPaymeExtra * DiscoTermiMese * DiritScontPagam) / 100) *
                                   ((m_LunghezzaPeriodo - LagPaymeExtra + ForwaPaymeExtra) / m_LunghezzaPeriodo) *
                                   (Math.Min(ExtraRequi, ThresDiscoExtra) * ExtraProduCost +
                                   (Math.Max(0, ExtraRequi - ThresDiscoExtra) * ExtraProduCost * (100 - DiscoLevelExtra) / 100))
                    Next

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next


            End If
        Catch ex As Exception
            g_MessaggioErrore &= "ExtraProduFinan_114 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub


    'Private Sub ExtraProduFinan_114()
    '    Dim iIDVariabile As Integer
    '    Dim sNomeVariabileEntrata As String = "ExtraProduFinan"

    '    Dim sValore As String

    '    Dim iIDPlayer As Integer
    '    Dim sTeamName As String

    '    Dim iIDItem As Integer
    '    Dim sNomeItem As String

    '    Dim sExtraRequiValue As String
    '    Dim sThresDiscoExtraValue As String
    '    Dim sExtraProduCostValue As String
    '    Dim sDiscoLevelExtraValue As String
    '    Dim sDiscoTermiMeseValue As String = "0"
    '    Dim sDiritScontPagamValue As String = "0"
    '    Dim sLagPaymeExtraValue As String = "0"
    '    Dim sForwaPaymeExtraValue As String = "0"

    '    Dim sNomeVariabileLista As String

    '    Dim dWithin As Double
    '    Dim dBeyond As Double

    '    Try
    '        If m_DataTablePlayers.Rows.Count > 0 Then
    '            iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

    '            For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows ' CICLO SUI PLAYERS
    '                iIDPlayer = Nni(oRowPlayer("IDTeam"))
    '                sTeamName = Nz(oRowPlayer("TeamName"))

    '                dWithin = 0
    '                dBeyond = 0

    '                For Each oRowItem As DataRow In m_DataTableItems.Rows ' CICLO SUGLI ITEMS
    '                    iIDItem = Nni(oRowItem("IDVariable"))
    '                    sNomeItem = Nz(oRowItem("VariableName"))

    '                    sNomeVariabileLista = "var_" & sTeamName & "_ExtraRequi_" & sNomeItem
    '                    sExtraRequiValue = GetVariableDecisionPlayer(m_IDGame, "ExtraRequi", iIDPlayer, iIDItem)

    '                    sNomeVariabileLista = "ThresDiscoExtra_" & sNomeItem
    '                    sThresDiscoExtraValue = GetVariableBoss("ThresDiscoExtra", sNomeVariabileLista)

    '                    sNomeVariabileLista = "ExtraProduCost_" & sNomeItem
    '                    sExtraProduCostValue = GetVariableBoss("ExtraProduCost", sNomeVariabileLista)

    '                    sNomeVariabileLista = "DiscoLevelExtra_" & sNomeItem
    '                    sDiscoLevelExtraValue = GetVariableBoss("DiscoLevelExtra", sNomeVariabileLista)

    '                    sDiscoTermiMeseValue = GetVariableBoss("DiscoTermiMese", "")

    '                    sDiritScontPagamValue = GetVariableBoss("DiritScontPagam", "")

    '                    sNomeVariabileLista = "LagPaymeExtra_" & sNomeItem
    '                    sLagPaymeExtraValue = GetVariableBoss("LagPaymeExtra", sNomeVariabileLista)

    '                    sForwaPaymeExtraValue = GetVariableDecisionPlayer(m_IDGame, "ForwaPaymeExtra", iIDPlayer, iIDItem)

    '                    ' Calcolo WITHIN
    '                    dWithin += Max(0, Min(Nn(sExtraRequiValue), Nn(sThresDiscoExtraValue)) * Nn(sExtraProduCostValue))

    '                    ' Calcolo BEYOND
    '                    dBeyond += Max(0, Nn(sExtraRequiValue) - Nn(sThresDiscoExtraValue)) * Nn(sExtraProduCostValue) * (1 - (Nn(sDiscoLevelExtraValue) / 100))
    '                Next

    '                sValore = dBeyond + dWithin
    '                sValore = Nn(sValore) * (1 - Nn(sDiscoTermiMeseValue) / 100 * Nn(sDiritScontPagamValue) * Min(m_LunghezzaPeriodo, Max(m_LunghezzaPeriodo - Nn(sLagPaymeExtraValue), Nn(sForwaPaymeExtraValue))))

    '                SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
    '            Next

    '        End If
    '    Catch ex As Exception
    '        g_MessaggioErrore &= "ExtraProduFinan_114 -> " & ex.Message & "<br/>"
    '        ex.Data.Clear()
    '    End Try
    'End Sub

    ''' <summary>
    ''' Macchinari da finanziare
    ''' </summary>
    Private Sub PlantCashFinan_115()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "PlantCashFinan"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sPlantRequiValue As String
        Dim sNewPlantCostValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sPlantRequiValue = GetVariableDecisionPlayer(m_IDGame, "PlantRequi", iIDPlayer, 0)

                    sNewPlantCostValue = GetVariableBoss("NewPlantCost", "")

                    If Nn(sPlantRequiValue) > 0 Then
                        sValore = Nn(sNewPlantCostValue) * Nn(sPlantRequiValue)
                    Else
                        sValore = 0
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "PlantCashFinan_115 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Leasing da finanziare
    ''' </summary>
    Private Sub LeaseCashFinan_016()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "LeaseCashFinan"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sLeaseRequiValue As String
        Dim sNewLeaseCostValue As String
        Dim sRicarTermiMeseValue As String
        Dim sDiritRitarPagamValue As String
        Dim sLagPaymeLeaseValue As String
        Dim sDelayPaymeLeaseValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sLeaseRequiValue = GetVariableDecisionPlayer(m_IDGame, "LeaseRequi", iIDPlayer, 0)

                    sNewLeaseCostValue = GetVariableDefineValue("NewLeaseCost", 0, 0, 0)

                    sRicarTermiMeseValue = GetVariableBoss("RicarTermiMese", "")

                    sDiritRitarPagamValue = GetVariableBoss("DiritRitarPagam", "")

                    sLagPaymeLeaseValue = GetVariableDecisionPlayer(m_IDGame, "DelayPaymeLease", iIDPlayer, 0)

                    If Nn(sLeaseRequiValue) < 0 Then
                        sValore = 0
                    Else
                        sValore = Nn(sNewLeaseCostValue) * Nn(sLeaseRequiValue)
                    End If
                    sValore = Nn(sValore) * (1 + Nn(sRicarTermiMeseValue) / 100 * Nn(sDiritRitarPagamValue) * Min(m_LunghezzaPeriodo - Nn(sLagPaymeLeaseValue), Nn(sDelayPaymeLeaseValue)))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "LeaseCashFinan_016 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Veicoli da finanziare
    ''' </summary>
    Private Sub VehicCashFinan_117()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "VehicCashFinan"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sVehicRequiValue As String
        Dim sNewVehicCostValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sVehicRequiValue = GetVariableDecisionPlayer(m_IDGame, "VehicRequi", iIDPlayer, 0)

                    sNewVehicCostValue = GetVariableBoss("NewVehicCost", "")

                    If Nn(sVehicRequiValue) > 0 Then
                        sValore = Nn(sNewVehicCostValue) * Nn(sVehicRequiValue)
                    Else
                        sValore = 0
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "VehicCashFinan_117 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Negozi centrali da finanziare
    ''' </summary>
    Private Sub CentrStoreFinan_118()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CentrStoreFinan"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sCentrStoreRequiValue As String
        Dim sRentPerioCentrValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sCentrStoreRequiValue = GetVariableDecisionPlayer(m_IDGame, "CentrStoreRequi", iIDPlayer, 0)

                    sRentPerioCentrValue = GetVariableDefineValue("RentPerioCentr", 0, 0, 0)

                    If Nn(sCentrStoreRequiValue) > 0 Then
                        sValore = Nn(sRentPerioCentrValue) * Nn(sCentrStoreRequiValue)
                    Else
                        sValore = 0
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "CentrStoreFinan_118 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Negozi periferici da finanziare
    ''' </summary>
    Private Sub PerifCashFinan_119()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "PerifCashFinan"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sPerifStoreRequiValue As String
        Dim sRentPerioPerifValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sPerifStoreRequiValue = GetVariableDecisionPlayer(m_IDGame, "PerifStoreRequi", iIDPlayer, 0)

                    sRentPerioPerifValue = GetVariableDefineValue("RentPerioPerif", 0, 0, 0)

                    If Nn(sPerifStoreRequiValue) > 0 Then
                        sValore = Nn(sRentPerioPerifValue) * Nn(sPerifStoreRequiValue)
                    Else
                        sValore = 0
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "PerifCashFinan_119 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Staff da finanziare
    ''' </summary>
    Private Sub PersoStaffFinan_120()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "PersoStaffFinan"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sNewPersoStaffValue As String
        Dim sNewCostStaffValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sNewPersoStaffValue = GetVariableDecisionPlayer(m_IDGame, "NewPersoStaff", iIDPlayer, 0)

                    sNewCostStaffValue = GetVariableDefineValue("NewCostStaff", 0, 0, 0)

                    If Nn(sNewPersoStaffValue) > 0 Then
                        sValore = Nn(sNewCostStaffValue) * Nn(sNewPersoStaffValue)
                    Else
                        sValore = 0
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "PersoStaffFinan_120 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

#End Region

#Region "41 - 60 FUNZIONI DI CALCOLO"

    ''' <summary>
    ''' Venditori da finanziare
    ''' </summary>
    Private Sub PersoPointFinan_201()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "PersoPointFinan"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sNewPersoPointValue As String
        Dim sNewCostPointValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sNewPersoPointValue = GetVariableDecisionPlayer(m_IDGame, "NewPersoPoint", iIDPlayer, 0)

                    sNewCostPointValue = GetVariableDefineValue("NewCostPoint", 0, 0, 0)

                    If Nn(sNewPersoPointValue) > 0 Then
                        sValore = Nn(sNewCostPointValue) * Nn(sNewPersoPointValue)
                    Else
                        sValore = 0
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "PersoPointFinan_201 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Fabbisogno finanziario
    ''' </summary>
    Private Sub CashNeedFirst_202()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CashNeedFirst"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sPurchEuropFinanValue As String
        Dim sLagPaymeEuropValue As String
        Dim sDiritRitarPagamValue As String
        Dim sDelayPaymeEuropValue As String

        Dim sProduCashFinanValue As String
        Dim sLagPaymeProduValue As String
        Dim sDelayPaymeProduValue As String

        Dim sLeaseCashFinanValue As String
        Dim sLagPaymeLeaseValue As String
        Dim sDelayPaymeLeaseValue As String

        Dim sPromoCashFinanValue As String
        Dim sLagPaymePromoValue As String
        Dim sDelayPaymePromoValue As String

        Dim sExtraProduFinanValue As String
        Dim sDiritScontPagamValue As String
        Dim sLagPaymeExtraValue As String
        Dim sForwaPaymeExtraValue As String

        Dim sPurchNICFinanValue As String

        Dim sCentrStoreFinanValue As String
        Dim sPerifCashFinanValue As String
        Dim sPersoStaffFinanValue As String
        Dim sPersoPointFinanValue As String

        Dim sPlantCashFinanValue As String
        Dim sVehicCashFinanValue As String

        Dim sTechnAutomRequiValue As String
        Dim sLagPaymeAutomValue As String
        Dim sForwaPaymeAutomValue As String

        Dim sAddesTotalPersoValue As String

        Dim sTrainServiRequiValue As String
        Dim sGreenRequiValue As String
        Dim sAdverRequiValue As String
        Dim sLagPaymeMktgValue As String
        Dim sForwaPaymeMktgValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    sValore = "0"
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    ' ***********************************************************************************************************************************
                    sPurchEuropFinanValue = GetVariableDefineValue("PurchEuropFinan", iIDPlayer, 0, 0)
                    sLagPaymeEuropValue = GetVariableBoss("LagPaymeEurop", "")
                    sDiritRitarPagamValue = GetVariableBoss("DiritRitarPagam", "")
                    sDelayPaymeEuropValue = GetVariableDecisionPlayer(m_IDGame, "DelayPaymeEurop", iIDPlayer, 0)

                    sValore = Nn(sValore) + Nn(sPurchEuropFinanValue) * (m_LunghezzaPeriodo - (Nn(sLagPaymeEuropValue) + Nn(sDiritRitarPagamValue) * Min(m_LunghezzaPeriodo - Nn(sLagPaymeEuropValue), Nn(sDelayPaymeEuropValue)))) / m_LunghezzaPeriodo
                    ' ***********************************************************************************************************************************

                    ' ***********************************************************************************************************************************
                    sProduCashFinanValue = GetVariableDefineValue("ProduCashFinan", iIDPlayer, 0, 0)
                    sLagPaymeProduValue = GetVariableBoss("LagPaymeProdu", "")
                    sDelayPaymeProduValue = GetVariableDecisionPlayer(m_IDGame, "DelayPaymeProdu", iIDPlayer, 0)

                    sValore = Nn(sValore) + Nn(sProduCashFinanValue) * (m_LunghezzaPeriodo - (Nn(sLagPaymeProduValue) + Nn(sDiritRitarPagamValue) _
                              * Min(m_LunghezzaPeriodo - Nn(sLagPaymeProduValue), Nn(sDelayPaymeProduValue)))) / m_LunghezzaPeriodo
                    ' ***********************************************************************************************************************************

                    ' ***********************************************************************************************************************************
                    sLeaseCashFinanValue = GetVariableDefineValue("LeaseCashFinan", iIDPlayer, 0, 0)
                    sLagPaymeLeaseValue = GetVariableBoss("LagPaymeLease", "")
                    sDelayPaymeLeaseValue = GetVariableDecisionPlayer(m_IDGame, "DelayPaymeLease", iIDPlayer, 0)

                    sValore = Nn(sValore) + Nn(sLeaseCashFinanValue) * (m_LunghezzaPeriodo - (Nn(sLagPaymeLeaseValue) + Nn(sDiritRitarPagamValue) _
                              * Min(m_LunghezzaPeriodo - Nn(sLagPaymeLeaseValue), Nn(sDelayPaymeLeaseValue)))) / m_LunghezzaPeriodo
                    ' ***********************************************************************************************************************************

                    ' ***********************************************************************************************************************************
                    sPromoCashFinanValue = GetVariableDefineValue("PromoCashFinan", iIDPlayer, 0, 0)
                    sLagPaymePromoValue = GetVariableBoss("LagPaymePromo", "")
                    sDelayPaymePromoValue = GetVariableDecisionPlayer(m_IDGame, "DelayPaymePromo", iIDPlayer, 0)

                    sValore = Nn(sValore) + Nn(sPromoCashFinanValue) * (m_LunghezzaPeriodo - (Nn(sLagPaymePromoValue) + Nn(sDiritRitarPagamValue) _
                              * Min(m_LunghezzaPeriodo - Nn(sLagPaymePromoValue), Nn(sDelayPaymePromoValue)))) / m_LunghezzaPeriodo
                    ' ***********************************************************************************************************************************

                    ' ***********************************************************************************************************************************
                    sExtraProduFinanValue = GetVariableDefineValue("ExtraProduFinan", iIDPlayer, 0, 0)
                    sDiritScontPagamValue = GetVariableBoss("DiritScontPagam", "")
                    sLagPaymeExtraValue = GetVariableBoss("LagPaymeExtra", "")
                    sForwaPaymeExtraValue = GetVariableDecisionPlayer(m_IDGame, "ForwaPaymeExtra", iIDPlayer, 0)

                    sValore = Nn(sValore) + Nn(sExtraProduFinanValue) * (Nn(sDiritScontPagamValue) *
                              Min(m_LunghezzaPeriodo, Max(m_LunghezzaPeriodo - Nn(sLagPaymeExtraValue) + Nn(sForwaPaymeExtraValue), 0)) / m_LunghezzaPeriodo)
                    ' ***********************************************************************************************************************************

                    sPurchNICFinanValue = GetVariableDefineValue("PurchNICFinan", iIDPlayer, 0, 0)
                    sValore = Nn(sValore) + Nn(sPurchNICFinanValue)

                    sCentrStoreFinanValue = GetVariableDefineValue("CentrStoreFinan", iIDPlayer, 0, 0)
                    sValore = Nn(sValore) + Nn(sCentrStoreFinanValue)

                    sPerifCashFinanValue = GetVariableDefineValue("PerifCashFinan", iIDPlayer, 0, 0)
                    sValore = Nn(sValore) + Nn(sPerifCashFinanValue)

                    sPersoStaffFinanValue = GetVariableDefineValue("PersoStaffFinan", iIDPlayer, 0, 0)
                    sValore = Nn(sValore) + Nn(sPersoStaffFinanValue)

                    sPersoPointFinanValue = GetVariableDefineValue("PersoPointFinan", iIDPlayer, 0, 0)
                    sValore = Nn(sValore) + Nn(sPersoPointFinanValue)

                    sPlantCashFinanValue = GetVariableDefineValue("PlantCashFinan", iIDPlayer, 0, 0)
                    sValore = Nn(sValore) + Nn(sPlantCashFinanValue)

                    sVehicCashFinanValue = GetVariableDefineValue("VehicCashFinan", iIDPlayer, 0, 0)
                    sValore = Nn(sValore) + Nn(sVehicCashFinanValue)

                    ' ***********************************************************************************************************************************
                    sTechnAutomRequiValue = GetVariableDecisionPlayer(m_IDGame, "TechnAutomRequi", iIDPlayer, 0)
                    sLagPaymeAutomValue = GetVariableBoss("LagPaymeAutom", "")
                    sForwaPaymeAutomValue = GetVariableDecisionPlayer(m_IDGame, "ForwaPaymeAutom", iIDPlayer, 0)

                    sValore = Nn(sValore) + Nn(sTechnAutomRequiValue) * (Nn(sDiritScontPagamValue) *
                              Min(m_LunghezzaPeriodo, Max(m_LunghezzaPeriodo - Nn(sLagPaymeAutomValue) + Nn(sForwaPaymeAutomValue), 0)) / m_LunghezzaPeriodo)
                    ' ***********************************************************************************************************************************

                    sAddesTotalPersoValue = GetVariableDecisionPlayer(m_IDGame, "AddesTotalPerso", iIDPlayer, 0)
                    sValore = Nn(sValore) + Nn(sAddesTotalPersoValue)

                    ' ***********************************************************************************************************************************
                    sTrainServiRequiValue = GetVariableDecisionPlayer(m_IDGame, "TrainServiRequi", iIDPlayer, 0)
                    sGreenRequiValue = GetVariableDecisionPlayer(m_IDGame, "GreenRequi", iIDPlayer, 0)
                    sAdverRequiValue = GetVariableDecisionPlayer(m_IDGame, "AdverRequi", iIDPlayer, 0)
                    sLagPaymeMktgValue = GetVariableBoss("LagPaymeMktg", "")
                    sForwaPaymeMktgValue = GetVariableDecisionPlayer(m_IDGame, "ForwaPaymeMktg", iIDPlayer, 0)

                    sValore = Nn(sValore) + (Nn(sTrainServiRequiValue) + Nn(sGreenRequiValue) + Nn(sAdverRequiValue)) *
                             (Nn(sDiritScontPagamValue) * Min(m_LunghezzaPeriodo, Max(m_LunghezzaPeriodo - Nn(sLagPaymeMktgValue) + Nn(sForwaPaymeMktgValue), 0)) / m_LunghezzaPeriodo)
                    ' ***********************************************************************************************************************************

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "CashNeedFirst_202 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Liquidità necesaria per le decisioni
    ''' </summary>
    Private Sub TotalCashNeede_203()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TotalCashNeede"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sCashNeedFirstValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sCashNeedFirstValue = GetVariableDefineValue("CashNeedFirst", iIDPlayer, 0, 0)

                    sValore = Nn(sCashNeedFirstValue)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "TotalCashNeede_203 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Scarica il prestito massimo
    ''' </summary>
    Private Sub DischMaximLoans_204()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DischMaximLoans"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sCashNeedFirstValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sCashNeedFirstValue = GetVariableState("ActuaMaximLoans", iIDPlayer, "", 0, 0)

                    sValore = Nn(sCashNeedFirstValue)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "DischMaximLoans_204 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Valore netto dei veicoli
    ''' </summary>
    Private Sub NetVehicles_205()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "NetVehicles"
        Dim iNumeroVeicoli As Integer
        Dim iNumMaxVeicoli As Integer

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sValoreVeicolo As String
        Dim sNewVehicCostValue As String
        Dim sNomeVariabileLista As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                iNumMaxVeicoli = m_DataTableAge.Rows.Count
                sNewVehicCostValue = GetVariableBoss("NewVehicCost", "")
                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iNumeroVeicoli = m_DataTableAge.Rows.Count - 1
                    sValore = "0"
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    ' Con un indice eseguo il ciclo su tutti i veicoli, escludendio l'ultimo
                    For iIndexAge As Integer = 1 To m_DataTableAge.Rows.Count - 1
                        sNomeVariabileLista = "var_" & sTeamName & "_TotalVehicOwned_" & iIndexAge
                        sValoreVeicolo = GetVariableState("TotalVehicOwned", iIDPlayer, sNomeVariabileLista, 0, iIndexAge)
                        sValore = Nn(sValore) + Nn(sValoreVeicolo) * Nn(sNewVehicCostValue) * iNumeroVeicoli / iNumMaxVeicoli
                        iNumeroVeicoli -= 1
                    Next

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "DischMaximLoans_204 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Debiti a medio/lungo
    ''' </summary>
    Private Sub LoansEndPerio_206()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "LoansEndPerio"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sLoansLevelValue As String
        Dim sLoansAllowValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sLoansLevelValue = GetVariableState("LoansLevel", iIDPlayer, "", 0, 0)
                    sLoansAllowValue = GetVariableDefineValue("LoansAllow", iIDPlayer, 0, 0)

                    sValore = Nn(sLoansLevelValue) + Nn(sLoansAllowValue)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "DischMaximLoans_204 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Pagamenti del periodo
    ''' </summary>
    Private Sub PaymeToCover_207()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "PaymeToCover"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sTotalCentrStoreValue As String
        Dim sRentPerioCentrValue As String
        Dim sTotalPerifStoreValue As String
        Dim sRentPerioPerifValue As String
        Dim sTotalPointPersoValue As String
        Dim sNewCostPointValue As String
        Dim sTotalStaffPersoValue As String
        Dim sNewCostStaffValue As String
        Dim sGlobaActuaLeaseValue As String
        Dim sNewLeaseCostValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sTotalCentrStoreValue = Nz(GetVariableState("TotalCentrStore", iIDPlayer, "", 0, 0), "0")
                    sRentPerioCentrValue = GetVariableDefineValue("RentPerioCentr", 0, 0, 0)

                    sTotalPerifStoreValue = Nz(GetVariableState("TotalPerifStore", iIDPlayer, "", 0, 0), "0")
                    sRentPerioPerifValue = GetVariableDefineValue("RentPerioPerif", 0, 0, 0)

                    sTotalPointPersoValue = Nz(GetVariableState("TotalPointPerso", iIDPlayer, "", 0, 0), "0")
                    sNewCostPointValue = GetVariableDefineValue("NewCostPoint", 0, 0, 0)

                    sTotalStaffPersoValue = Nz(GetVariableState("TotalStaffPerso", iIDPlayer, "", 0, 0), "0")
                    sNewCostStaffValue = GetVariableDefineValue("NewCostStaff", 0, 0, 0)

                    sGlobaActuaLeaseValue = GetVariableDefineValue("GlobaActuaLease", iIDPlayer, 0, 0)
                    sNewLeaseCostValue = GetVariableDefineValue("NewLeaseCost", 0, 0, 0)

                    sValore = Nn(sTotalCentrStoreValue) * Nn(sRentPerioCentrValue) +
                              Nn(sTotalPerifStoreValue) * Nn(sRentPerioPerifValue) +
                              Nn(sTotalPointPersoValue) * Nn(sNewCostPointValue) +
                              Nn(sTotalStaffPersoValue) * Nn(sNewCostStaffValue) +
                              Nn(sGlobaActuaLeaseValue) * Nn(sNewLeaseCostValue)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "PaymeToCover_207 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Disinvestimenti del periodo
    ''' </summary>
    Private Sub DisinAllowPerio_208()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DisinAllowPerio"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sMaximDismiPersoValue As String
        Dim sTotalPointPersoValue As String
        Dim sNewPersoPointValue As String
        Dim sNewCostPointValue As String
        Dim sTotalStaffPersoValue As String
        Dim sNewPersoStaffValue As String
        Dim sNewCostStaffValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sMaximDismiPersoValue = GetVariableBoss("MaximDismiPerso", "")

                    sTotalPointPersoValue = GetVariableState("TotalPointPerso", 0, "", 0, 0)

                    sNewPersoPointValue = GetVariableDecisionPlayer(m_IDGame, "NewPersoPoint", iIDPlayer, 0)

                    sNewCostPointValue = GetVariableDefineValue("NewCostPoint", 0, 0, 0)
                    sTotalStaffPersoValue = GetVariableState("TotalStaffPerso", 0, "", 0, 0)

                    sNewPersoStaffValue = GetVariableDecisionPlayer(m_IDGame, "NewPersoStaff", iIDPlayer, 0)

                    sNewCostStaffValue = GetVariableDefineValue("NewCostStaff", 0, 0, 0)

                    sValore = -Min(0, Max(-(Nn(sMaximDismiPersoValue) / 100) * (m_LunghezzaPeriodo / 12) * Nn(sTotalPointPersoValue), Nn(sNewPersoPointValue)) *
                              Nn(sNewCostPointValue) * 2) - Min(0, Max(-(Nn(sMaximDismiPersoValue) / 100) *
                              (m_LunghezzaPeriodo / 12) * Nn(sTotalStaffPersoValue), Nn(sNewPersoStaffValue)) * Nn(sNewCostStaffValue) * 2)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "DisinAllowPerio_208 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Decrermento dei crediti
    ''' </summary>
    Private Sub DecreDebts_209()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DecreDebts"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sDebtorsValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sDebtorsValue = GetVariableState("Debtors", iIDPlayer, "", 0, 0)

                    sValore = Nn(sDebtorsValue)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "DecreDebts_209 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Decremento dei debiti
    ''' </summary>
    Private Sub DecreCredits_210()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DecreCredits"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sDebtorsValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sDebtorsValue = GetVariableState("Creditors", iIDPlayer, "", 0, 0)

                    sValore = Nn(sDebtorsValue)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "DecreCredits_210 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Valore iniziale delle materie prime
    ''' </summary>
    Private Sub BeginRawValue_211()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "BeginRawValue"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim sNomeVariabileLista As String

        Dim sStockRawMaterValue As String
        Dim sValueRawMaterValue As String
        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        sNomeVariabileLista = "var_" & sTeamName & "_StockRawMater_" & sItemName
                        sStockRawMaterValue = GetVariableState("StockRawMater", iIDPlayer, sNomeVariabileLista, iIDItem, 0)

                        sNomeVariabileLista = "var_" & sTeamName & "_ValueRawMater_" & sItemName
                        sValueRawMaterValue = GetVariableState("ValueRawMater", iIDPlayer, sNomeVariabileLista, iIDItem, 0)

                        sValore = Nn(sStockRawMaterValue) * Nn(sValueRawMaterValue)

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "BeginRawValue_211 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Valore iniziale dei prodotti finiti
    ''' </summary>
    Private Sub ValueStockBegin_212()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "ValueStockBegin"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim sNomeVariabileLista As String

        Dim sLastFinisValueValue As String
        Dim sStockFinisGoodsValue As String
        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        sNomeVariabileLista = "var_" & sTeamName & "_LastFinisValue_" & sItemName
                        sLastFinisValueValue = GetVariableState("LastFinisValue", iIDPlayer, sNomeVariabileLista, iIDItem, 0)

                        sNomeVariabileLista = "var_" & sTeamName & "_StockFinisGoods_" & sItemName
                        sStockFinisGoodsValue = GetVariableState("StockFinisGoods", iIDPlayer, sNomeVariabileLista, iIDItem, 0)

                        sValore = Nn(sLastFinisValueValue) * Nn(sStockFinisGoodsValue)

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "BeginRawValue_211 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Scarica i costi fissi
    ''' </summary>
    Private Sub DischCostsFixed_213()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DischCostsFixed"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sPreviFixedCostsValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sPreviFixedCostsValue = GetVariableState("PreviFixedCosts", iIDPlayer, "", 0, 0)

                    sValore = Nn(sPreviFixedCostsValue)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "DischCostsFixed_213 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Ammortamento dei veicoli
    ''' </summary>
    Private Sub SinkiFundVehic_214()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "SinkiFundVehic"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sOperaVehicUsedValue As String
        Dim sNewVehicCostValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sOperaVehicUsedValue = GetVariableDefineValue("OperaVehicUsed", iIDPlayer, 0, 0)
                    sNewVehicCostValue = GetVariableBoss("NewVehicCost", "")

                    sValore = Nn(sOperaVehicUsedValue) * Nn(sNewVehicCostValue) / m_DataTableAge.Rows.Count

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "SinkiFundVehic_214 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Crediti inesigibili
    ''' </summary>
    Private Sub DebtsWrittOff_215()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DebtsWrittOff"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sDebtorsValue As String
        Dim sDebtWrittRateValue As String

        Dim sNomeVariabileLista As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sDebtorsValue = GetVariableState("Debtors", iIDPlayer, "", 0, 0)
                    sNomeVariabileLista = "DebtWrittRate_" & sTeamName
                    sDebtWrittRateValue = GetVariableBoss("DebtWrittRate", sNomeVariabileLista)

                    sValore = Nn(sDebtorsValue) * Nn(sDebtWrittRateValue) / 100

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "DebtsWrittOff_215 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Totale revenues
    ''' </summary>
    Private Sub DischRavenTotal_216()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DischRevenTotal"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sVariaRevenTotalValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sVariaRevenTotalValue = GetVariableState("VariaRevenTotal", iIDPlayer, "", 0, 0)

                    sValore = 0

                    If m_CurrentStep <= 2 Then
                        sValore = 0
                    Else

                        If m_LunghezzaPeriodo < 4 AndAlso m_LunghezzaPeriodo > 2 Then
                            If m_CurrentStep = 5 OrElse m_CurrentStep = 9 OrElse m_CurrentStep = 13 OrElse m_CurrentStep = 17 Then
                                sValore = sVariaRevenTotalValue
                            Else
                                sValore = 0
                            End If

                        ElseIf m_LunghezzaPeriodo < 5 AndAlso m_LunghezzaPeriodo > 3 Then
                            If m_CurrentStep = 4 OrElse m_CurrentStep = 7 OrElse m_CurrentStep = 10 OrElse m_CurrentStep = 13 Then
                                sValore = sVariaRevenTotalValue
                            Else
                                sValore = 0
                            End If

                        ElseIf m_LunghezzaPeriodo < 7 AndAlso m_LunghezzaPeriodo > 5 Then
                            If m_CurrentStep = 3 OrElse m_CurrentStep = 5 OrElse m_CurrentStep = 7 OrElse m_CurrentStep = 9 OrElse m_CurrentStep = 11 OrElse m_CurrentStep = 13 Then
                                sValore = sVariaRevenTotalValue
                            Else
                                sValore = 0
                            End If

                        Else
                            sValore = sVariaRevenTotalValue

                        End If

                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "DischRavenTotal_216 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Delta raw materials
    ''' </summary>
    Private Sub DischDeltaRaw_217()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DischDeltaRaw"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sDischRevenTotalValue As String
        Dim sVariaDeltaRawValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sDischRevenTotalValue = GetVariableDefineValue("DischRevenTotal", iIDPlayer, 0, 0)
                    sVariaDeltaRawValue = GetVariableState("VariaDeltaRaw", iIDPlayer, "", 0, 0)

                    If Nn(sDischRevenTotalValue) = 0 Then
                        sValore = 0
                    Else
                        sValore = sVariaDeltaRawValue
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "DischDeltaRaw_217 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Delta stock of finished goods
    ''' </summary>
    Private Sub DischDeltaStock_218()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DischDeltaStock"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sDischRevenTotalValue As String
        Dim sVariaDeltaStockValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sDischRevenTotalValue = GetVariableDefineValue("DischRevenTotal", iIDPlayer, 0, 0)
                    sVariaDeltaStockValue = GetVariableState("VariaDeltaStock", iIDPlayer, "", 0, 0)

                    If Nn(sDischRevenTotalValue) = 0 Then
                        sValore = 0
                    Else
                        sValore = sVariaDeltaStockValue
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "DischDeltaStock_218 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Cost of purchase
    ''' </summary>
    Private Sub DischCostPurch_219()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DischCostPurch"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sDischRevenTotalValue As String
        Dim sVariaCostPurchValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sDischRevenTotalValue = GetVariableDefineValue("DischRevenTotal", iIDPlayer, 0, 0)
                    sVariaCostPurchValue = GetVariableState("VariaCostPurch", iIDPlayer, "", 0, 0)

                    If Nn(sDischRevenTotalValue) = 0 Then
                        sValore = 0
                    Else
                        sValore = sVariaCostPurchValue
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "DischCostPurch_219 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Direct costs
    ''' </summary>
    Private Sub DischDirecCosts_220()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DischDirecCosts"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sDischRevenTotalValue As String
        Dim sVariaDirecCostsValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sDischRevenTotalValue = GetVariableDefineValue("DischRevenTotal", iIDPlayer, 0, 0)
                    sVariaDirecCostsValue = GetVariableState("VariaDirecCosts", iIDPlayer, "", 0, 0)

                    If Nn(sDischRevenTotalValue) = 0 Then
                        sValore = 0
                    Else
                        sValore = sVariaDirecCostsValue
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "DischDirecCosts_220 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

#End Region

#Region "61 - 80 FUNZIONI DI CALCOLO"

    ''' <summary>
    ''' Extra products
    ''' </summary>
    Private Sub DischExtraProdu_301()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DischExtraProdu"

        Dim DischExtraProdu As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sDischRevenTotalValue As String
        Dim sVariaExtraProduValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sDischRevenTotalValue = GetVariableDefineValue("DischRevenTotal", iIDPlayer, 0, 0)
                    sVariaExtraProduValue = GetVariableState("VariaExtraProdu", iIDPlayer, "", 0, 0)

                    If Nn(sDischRevenTotalValue) = 0 Then
                        DischExtraProdu = 0
                    Else
                        DischExtraProdu = sVariaExtraProduValue
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, DischExtraProdu)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "DischExtraProdu_301 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Plant depreciation
    ''' </summary>
    Private Sub DischSinkiPlant_302()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DischSinkiPlant"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sDischRevenTotalValue As String
        Dim sVariaSinkiPlantValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sDischRevenTotalValue = GetVariableDefineValue("DischRevenTotal", iIDPlayer, 0, 0)
                    sVariaSinkiPlantValue = GetVariableState("VariaSinkiPlant", iIDPlayer, "", 0, 0)

                    If Nn(sDischRevenTotalValue) = 0 Then
                        sValore = 0
                    Else
                        sValore = sVariaSinkiPlantValue
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "DischExtraProdu_301 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Leasing
    ''' </summary>
    Private Sub DischLeasiCosts_303()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DischLeasiCosts"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sDischRevenTotalValue As String
        Dim sVariaLeasiCostsValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sDischRevenTotalValue = GetVariableDefineValue("DischRevenTotal", iIDPlayer, 0, 0)
                    sVariaLeasiCostsValue = GetVariableState("VariaLeasiCosts", iIDPlayer, "", 0, 0)

                    If Nn(sDischRevenTotalValue) = 0 Then
                        sValore = 0
                    Else
                        sValore = sVariaLeasiCostsValue
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "DischLeasiCosts_303 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Warehouse
    ''' </summary>
    Private Sub DischWarehCosts_304()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DischWarehCosts"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sDischRevenTotalValue As String
        Dim sVariaWarehCostsValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sDischRevenTotalValue = GetVariableDefineValue("DischRevenTotal", iIDPlayer, 0, 0)
                    sVariaWarehCostsValue = GetVariableState("VariaWarehCosts", iIDPlayer, "", 0, 0)

                    If Nn(sDischRevenTotalValue) = 0 Then
                        sValore = 0
                    Else
                        sValore = sVariaWarehCostsValue
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "DischWarehCosts_304 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Transport
    ''' </summary>
    Private Sub DischTransCosts_305()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DischTransCosts"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sDischRevenTotalValue As String
        Dim sVariaTransCostsValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sDischRevenTotalValue = GetVariableDefineValue("DischRevenTotal", iIDPlayer, 0, 0)
                    sVariaTransCostsValue = GetVariableState("VariaTransCosts", iIDPlayer, "", 0, 0)

                    If Nn(sDischRevenTotalValue) = 0 Then
                        sValore = 0
                    Else
                        sValore = sVariaTransCostsValue
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "DischTransCosts_305 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Vehicles depreciation
    ''' </summary>
    Private Sub DischSinkiVehic_306()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DischSinkiVehic"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sDischRevenTotalValue As String
        Dim sVariaSinkiVehicValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sDischRevenTotalValue = GetVariableDefineValue("DischRevenTotal", iIDPlayer, 0, 0)
                    sVariaSinkiVehicValue = GetVariableState("VariaSinkiVehic", iIDPlayer, "", 0, 0)

                    If Nn(sDischRevenTotalValue) = 0 Then
                        sValore = 0
                    Else
                        sValore = sVariaSinkiVehicValue
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "DischSinkiVehic_306 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Fixed costs
    ''' </summary>
    Private Sub DischFixedCosts_307()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DischFixedCosts"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sDischRevenTotalValue As String
        Dim sVariaFixedCostsValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sDischRevenTotalValue = GetVariableDefineValue("DischRevenTotal", iIDPlayer, 0, 0)
                    sVariaFixedCostsValue = GetVariableState("VariaFixedCosts", iIDPlayer, "", 0, 0)

                    If Nn(sDischRevenTotalValue) = 0 Then
                        sValore = 0
                    Else
                        sValore = sVariaFixedCostsValue
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "DischFixedCosts_307 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Human resources
    ''' </summary>
    Private Sub DischPersoCosts_308()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DischPersoCosts"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sDischRevenTotalValue As String
        Dim sVariaPersoCostsValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sDischRevenTotalValue = GetVariableDefineValue("DischRevenTotal", iIDPlayer, 0, 0)
                    sVariaPersoCostsValue = GetVariableState("VariaPersoCosts", iIDPlayer, "", 0, 0)

                    If Nn(sDischRevenTotalValue) = 0 Then
                        sValore = 0
                    Else
                        sValore = sVariaPersoCostsValue
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "DischPersoCosts_308 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Outlets
    ''' </summary>
    Private Sub DischOutleCosts_309()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DischOutleCosts"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sDischRevenTotalValue As String
        Dim sVariaOutleCostsValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sDischRevenTotalValue = GetVariableDefineValue("DischRevenTotal", iIDPlayer, 0, 0)
                    sVariaOutleCostsValue = GetVariableState("VariaOutleCosts", iIDPlayer, "", 0, 0)

                    If Nn(sDischRevenTotalValue) = 0 Then
                        sValore = 0
                    Else
                        sValore = sVariaOutleCostsValue
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "DischOutleCosts_309 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Total expenses
    ''' </summary>
    Private Sub DischExpenCosts_310()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DischExpenCosts"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sDischRevenTotalValue As String
        Dim sVariaExpenCostsValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sDischRevenTotalValue = GetVariableDefineValue("DischRevenTotal", iIDPlayer, 0, 0)
                    sVariaExpenCostsValue = GetVariableState("VariaExpenCosts", iIDPlayer, "", 0, 0)

                    If Nn(sDischRevenTotalValue) = 0 Then
                        sValore = 0
                    Else
                        sValore = sVariaExpenCostsValue
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "DischExpenCosts_310 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Debts written off / loss on sale
    ''' </summary>
    Private Sub DischDebtsOff_311()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DischDebtsOff"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sDischRevenTotalValue As String
        Dim sVariaDebtsOffValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sDischRevenTotalValue = GetVariableDefineValue("DischRevenTotal", iIDPlayer, 0, 0)
                    sVariaDebtsOffValue = GetVariableState("VariaDebtsOff", iIDPlayer, "", 0, 0)

                    If Nn(sDischRevenTotalValue) = 0 Then
                        sValore = 0
                    Else
                        sValore = sVariaDebtsOffValue
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "DischDebtsOff_311 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Interests (+)
    ''' </summary>
    Private Sub DischPositInter_312()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DischPositInter"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sDischRevenTotalValue As String
        Dim sVariaPositInterValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sDischRevenTotalValue = GetVariableDefineValue("DischRevenTotal", iIDPlayer, 0, 0)
                    sVariaPositInterValue = GetVariableState("VariaPositInter", iIDPlayer, "", 0, 0)

                    If Nn(sDischRevenTotalValue) = 0 Then
                        sValore = 0
                    Else
                        sValore = sVariaPositInterValue
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "DischPositInter_312 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Interests (-)
    ''' </summary>
    Private Sub DischNegatInter_313()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DischNegatInter"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sDischRevenTotalValue As String
        Dim sVariaNegatInterValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sDischRevenTotalValue = GetVariableDefineValue("DischRevenTotal", iIDPlayer, 0, 0)
                    sVariaNegatInterValue = GetVariableState("VariaNegatInter", iIDPlayer, "", 0, 0)

                    If Nn(sDischRevenTotalValue) = 0 Then
                        sValore = 0
                    Else
                        sValore = sVariaNegatInterValue
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "DischNegatInter_313 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Profitto prima delle tasse 
    ''' </summary>
    Private Sub DischBeforTax_314()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DischBeforTax"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sDischRevenTotalValue As String
        Dim sVariaBeforTaxValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sDischRevenTotalValue = GetVariableDefineValue("DischRevenTotal", iIDPlayer, 0, 0)
                    sVariaBeforTaxValue = GetVariableState("VariaBeforTax", iIDPlayer, "", 0, 0)

                    If Nn(sDischRevenTotalValue) = 0 Then
                        sValore = 0
                    Else
                        sValore = sVariaBeforTaxValue
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "DischBeforTax_314 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Macchinari venduti concessi
    ''' </summary>
    Private Sub PlantSoldAllow_315()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "PlantSoldAllow"

        Dim PlantSoldAllow As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim PlantRequi As String
        Dim TotalActivMachi As String
        Dim PlantPerAge As String
        Dim sNomeVariabileLista As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    sNomeVariabileLista = "var_" & sTeamName & "_PlantPerAge_12"

                    PlantSoldAllow = "0"

                    PlantRequi = GetVariableDecisionPlayer(m_IDGame, "PlantRequi", iIDPlayer, 0)
                    TotalActivMachi = GetVariableDefineValue("TotalActivMachi", iIDPlayer, 0, 0)
                    PlantPerAge = GetVariableState("PlantPerAge", iIDPlayer, sNomeVariabileLista, 0, 12)

                    If Nn(PlantRequi) < 0 Then
                        PlantSoldAllow = Min(-Nn(PlantRequi), Nn(TotalActivMachi) - Nn(PlantPerAge))
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, PlantSoldAllow)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "PlantSoldAllow_315 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Numero dei macchinari ceduti
    ''' </summary>
    Private Sub OperaPlantScrap_316()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "OperaPlantScrap"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim dPlanta As Double

        Dim sPlantRequiValue As String
        Dim sPlantSoldAllowValue As String
        Dim sPlantPerAgeValue As String
        Dim sNomeVariabileLista As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sPlantRequiValue = GetVariableDecisionPlayer(m_IDGame, "PlantRequi", iIDPlayer, 0)
                    sPlantSoldAllowValue = GetVariableDefineValue("PlantSoldAllow", iIDPlayer, 0, 0)

                    sValore = "0"

                    dPlanta = 0
                    For iAge As Integer = m_DataTableAge.Rows.Count - 1 To 1 Step -1
                        SaveVariableValue(iIDVariabile, iIDPlayer, 0, iAge, sValore)
                    Next

                    If Nn(sPlantRequiValue) < 0 Then
                        dPlanta = Nn(sPlantSoldAllowValue)

                        For iAge As Integer = m_DataTableAge.Rows.Count - 1 To 1 Step -1
                            sNomeVariabileLista = "var_" & sTeamName & "_PlantPerAge_" & iAge
                            sPlantPerAgeValue = GetVariableState("PlantPerAge", iIDPlayer, sNomeVariabileLista, 0, iAge)

                            sValore = Min(Nn(sPlantPerAgeValue), dPlanta)

                            SaveVariableValue(iIDVariabile, iIDPlayer, 0, iAge, sValore)

                            dPlanta = dPlanta - Nn(sValore)
                        Next

                    End If

                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "OperaPlantScrap_316 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Costo materie 1e (Prossimo)
    ''' </summary>
    Private Sub NextRawEurop_317()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "NextRawEurop"

        Dim sValore As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim sRawEuropCostValue As String
        Dim sIncreRawEuropValue As String
        Dim sNomeVariabileLista As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowItem As DataRow In m_DataTableItems.Rows
                    iIDItem = Nni(oRowItem("IDVariable"))
                    sItemName = Nz(oRowItem("VariableName"))

                    sNomeVariabileLista = "RawEuropCost_" & sItemName
                    sRawEuropCostValue = GetVariableBoss("RawEuropCost", sNomeVariabileLista)
                    sIncreRawEuropValue = GetVariableDefineValue("IncreRawEurop", 0, iIDItem, 0)

                    sValore = Nn(sRawEuropCostValue) + Nn(sIncreRawEuropValue)

                    SaveVariableValue(iIDVariabile, 0, iIDItem, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "NextRawEurop_317 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Costo materie 1e importo (Prossimo)
    ''' </summary>
    Private Sub NextRawNIC_318()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "NextRawNIC"

        Dim sValore As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim sRawNICCostValue As String
        Dim sIncreRawNICValue As String
        Dim sNomeVariabileLista As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowItem As DataRow In m_DataTableItems.Rows
                    iIDItem = Nni(oRowItem("IDVariable"))
                    sItemName = Nz(oRowItem("VariableName"))

                    sNomeVariabileLista = "RawNICCost_" & sItemName
                    sRawNICCostValue = GetVariableBoss("RawNICCost", sNomeVariabileLista)
                    sIncreRawNICValue = GetVariableDefineValue("IncreRawNIC", 0, iIDItem, 0)

                    sValore = Nn(sRawNICCostValue) + Nn(sIncreRawNICValue)

                    SaveVariableValue(iIDVariabile, 0, iIDItem, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "NextRawNIC_318 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Affitti non rinnovati - negozi centrali
    ''' </summary>
    Private Sub CentrAlienAllow_319()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CentrAlienAllow"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sCentrStoreRequiValue As String
        Dim sTotalPerifStoreValue As String
        Dim sTotalCentrStoreValue As String
        Dim sPerifScrapAllowValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sCentrStoreRequiValue = GetVariableDecisionPlayer(m_IDGame, "CentrStoreRequi", iIDPlayer, 0)
                    sTotalPerifStoreValue = GetVariableState("TotalPerifStore", iIDPlayer, "", 0, 0)
                    sTotalCentrStoreValue = GetVariableState("TotalCentrStore", iIDPlayer, "", 0, 0)
                    sPerifScrapAllowValue = GetVariableDefineValue("PerifScrapAllow", iIDPlayer, 0, 0)

                    If Nn(sCentrStoreRequiValue) < 0 AndAlso (Nn(sTotalPerifStoreValue) + Nn(sTotalCentrStoreValue) - Nn(sPerifScrapAllowValue)) > 1 Then
                        sValore = Min(-Nn(sCentrStoreRequiValue), Nn(sTotalCentrStoreValue))
                    Else
                        sValore = 0
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "CentrAlienAllow_319 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Variazione sul prezzo di vendita
    ''' </summary>
    Private Sub DeltaSalesPrice_320()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DeltaSalesPrice"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim sSalesPriceAllowValue As String
        Dim sPreviSalesPriceValue As String
        Dim sNomeVariabileLista As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = "0"

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        sSalesPriceAllowValue = GetVariableDefineValue("SalesPriceAllow", iIDPlayer, iIDItem, 0)
                        sNomeVariabileLista = "var_" & sTeamName & "_PreviSalesPrice_" & sItemName
                        sPreviSalesPriceValue = GetVariableState("PreviSalesPrice", iIDPlayer, sNomeVariabileLista, iIDItem, 0)

                        sValore = Abs(Nn(sSalesPriceAllowValue) - Nn(sPreviSalesPriceValue))

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next

                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "DeltaSalesPrice_320 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

#End Region

#Region "81 - 100 FUNZIONI DI CALCOLO"

    ''' <summary>
    ''' Pagamento delle tasse
    ''' </summary>
    Private Sub TaxPayment_401()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TaxPayment"

        Dim TaxPayment As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim DischRevenTotal As String
        Dim TaxFunds As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    DischRevenTotal = GetVariableDefineValue("DischRevenTotal", iIDPlayer, 0, 0)
                    TaxFunds = GetVariableState("TaxFunds", iIDPlayer, "", 0, 0)

                    If Nn(DischRevenTotal) > 0 Then
                        TaxPayment = Max(0, Nn(TaxFunds))
                    Else
                        TaxPayment = "0"
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, TaxPayment)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "TaxPayement_401 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Pagamento delle tasse
    ''' </summary>
    Private Sub NegatTaxPayement_402()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "NegatTaxPayme"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sDischRevenTotalValue As String
        Dim sTaxFundsValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sDischRevenTotalValue = GetVariableDefineValue("DischRevenTotal", iIDPlayer, 0, 0)
                    sTaxFundsValue = GetVariableState("TaxFunds", iIDPlayer, "", 0, 0)

                    sValore = "0"
                    If Nn(sDischRevenTotalValue) > 0 Then
                        sValore = Min(0, Nn(sTaxFundsValue))
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "NegatTaxPayement_402 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Minusvalenze da alienazione macchinari
    ''' </summary>
    Private Sub WriteOffLost_403()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "WriteOffLost"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sNewPlantCostValue As String
        Dim sOperaPlantScrapValue As String
        Dim sPerceLossDismiValue As String

        Dim iNumeroVeicoli As Integer
        Dim iNumMaxVeicoli As Integer

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                iNumMaxVeicoli = m_DataTableAge.Rows.Count

                ' Variabili del boss a valore fisso
                sNewPlantCostValue = GetVariableBoss("NewPlantCost", "")
                sPerceLossDismiValue = GetVariableBoss("PerceLossDismi", "")

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iNumeroVeicoli = m_DataTableAge.Rows.Count - 1
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    sValore = "0"

                    ' Con un indice eseguo il ciclo su tutti i veicoli, escludendio l'ultimo
                    For iIndexAge As Integer = 1 To m_DataTableAge.Rows.Count - 1
                        sOperaPlantScrapValue = GetVariableDefineValue("OperaPlantScrap", iIDPlayer, 0, iIndexAge)

                        sValore = Nn(sNewPlantCostValue) * Nn(sOperaPlantScrapValue) * iNumeroVeicoli / iNumMaxVeicoli * Nn(sPerceLossDismiValue) / 100 + Nn(sValore)
                        iNumeroVeicoli -= 1
                    Next

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "WriteOffLost_403 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Ammortamento dei veicoli
    ''' </summary>
    Private Sub CumulSinkiVehic_404()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CumulSinkiVehic"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sSinkiFundVehicValue As String
        Dim sVariaSinkiVehicValue As String
        Dim sDischSinkiVehicValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sSinkiFundVehicValue = GetVariableDefineValue("SinkiFundVehic", iIDPlayer, 0, 0)
                    sDischSinkiVehicValue = GetVariableDefineValue("DischSinkiVehic", iIDPlayer, 0, 0)
                    sVariaSinkiVehicValue = GetVariableState("VariaSinkiVehic", iIDPlayer, "", 0, 0)

                    sValore = Nn(sVariaSinkiVehicValue) + Nn(sSinkiFundVehicValue) - Nn(sDischSinkiVehicValue)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "CumulSinkiVehic_404 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Minusvalenze da alienazione macchinari
    ''' </summary>
    Private Sub CumulDebtsOff_405()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CumulDebtsOff"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sVariaDebtsOffValue As String
        Dim sWriteOffLostValue As String
        Dim sDebtsWrittOffValue As String
        Dim sDischDebtsOffValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sVariaDebtsOffValue = GetVariableState("VariaDebtsOff", iIDPlayer, "", 0, 0)
                    sWriteOffLostValue = GetVariableDefineValue("WriteOffLost", iIDPlayer, 0, 0)
                    sDebtsWrittOffValue = GetVariableDefineValue("DebtsWrittOff", iIDPlayer, 0, 0)
                    sDischDebtsOffValue = GetVariableDefineValue("DischDebtsOff", iIDPlayer, 0, 0)

                    sValore = Nn(sVariaDebtsOffValue) + Nn(sWriteOffLostValue) + Nn(sDebtsWrittOffValue) - Nn(sDischDebtsOffValue)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "CumulSinkiVehic_404 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Macchinari propri per età (/attuale)
    ''' </summary>
    Private Sub TotalPlant_406()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TotalPlant"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String
        Dim iIDAge As Integer

        Dim sPlantPerAgeValue As String
        Dim sOperaPlantScrapValue As String
        Dim sNomeVariabileLista As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    For Each oRowAge As DataRow In m_DataTableAge.Rows
                        iIDAge = Nni(oRowAge("ID"))
                        sNomeVariabileLista = "var_" & sTeamName & "_PlantPerAge_" & Nz(oRowAge("AgeDescription"))
                        sPlantPerAgeValue = GetVariableState("PlantPerAge", iIDPlayer, sNomeVariabileLista, 0, Nz(oRowAge("AgeDescription")))
                        sOperaPlantScrapValue = GetVariableDefineValue("OperaPlantScrap", iIDPlayer, 0, Nz(oRowAge("AgeDescription")))

                        sValore = Nn(sPlantPerAgeValue) - Nn(sOperaPlantScrapValue)
                        SaveVariableValue(iIDVariabile, iIDPlayer, 0, iIDAge, sValore)
                    Next

                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "TotalPlant_406 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Liquidità disponibile per le decisioni
    ''' </summary>
    Private Sub TotalCashAvail_407()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TotalCashAvail"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sBankLevelValue As String

        Dim sTaxPaymentValue As String
        Dim sLoansAllowValue As String
        Dim sPaymeToCoverValue As String
        Dim sDisinAllowPerioValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sBankLevelValue = GetVariableState("BankLevel", iIDPlayer, "", 0, 0)

                    sTaxPaymentValue = GetVariableDefineValue("TaxPayment", iIDPlayer, 0, 0)
                    sLoansAllowValue = GetVariableDefineValue("LoansAllow", iIDPlayer, 0, 0)
                    sPaymeToCoverValue = GetVariableDefineValue("PaymeToCover", iIDPlayer, 0, 0)
                    sDisinAllowPerioValue = GetVariableDefineValue("DisinAllowPerio", iIDPlayer, 0, 0)

                    sValore = Nn(sBankLevelValue) + Nn(sLoansAllowValue) - Nn(sPaymeToCoverValue) + Nn(sDisinAllowPerioValue) - Nn(sTaxPaymentValue)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "TotalCashAvail_407 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Percentuale delle decisioni soddisfatte
    ''' </summary>
    Private Sub PerceFirstRequi_408()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "PerceFirstRequi"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sTotalCashAvailValue As String
        Dim sCashNeedFirstValue As String
        Dim sPaymeToCoverValue As String
        Dim sSuperExtraFidoValue As String
        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sTotalCashAvailValue = GetVariableDefineValue("TotalCashAvail", iIDPlayer, 0, 0)
                    sCashNeedFirstValue = GetVariableDefineValue("CashNeedFirst", iIDPlayer, 0, 0)
                    sPaymeToCoverValue = GetVariableDefineValue("PaymeToCover", iIDPlayer, 0, 0)
                    sSuperExtraFidoValue = GetVariableDecisionPlayer(m_IDGame, "SuperExtraFido", iIDPlayer, 0)

                    If (Nn(sTotalCashAvailValue) >= Nn(sCashNeedFirstValue)) Then
                        sValore = "100"
                    Else
                        If (Nn(sCashNeedFirstValue) = 0 OrElse Nn(sTotalCashAvailValue) + Nn(sSuperExtraFidoValue) < 0) Then
                            sValore = "0"
                        Else
                            sValore = Max(0, Min((Nn(sTotalCashAvailValue) + Nn(sSuperExtraFidoValue)) / Nn(sCashNeedFirstValue), 1) * 100)
                        End If
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "PerceFirstRequi_408 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Valore netto dei macchinari
    ''' </summary>
    Private Sub NetPlants_409()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "NetPlants"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sNewPlantCostValue As String
        Dim sTotalPlantValue As String

        Dim iNumeroVeicoli As Integer
        Dim iNumMaxVeicoli As Integer

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                iNumMaxVeicoli = m_DataTableAge.Rows.Count

                ' Variabili del boss a valore fisso
                sNewPlantCostValue = GetVariableBoss("NewPlantCost", "")

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iNumeroVeicoli = m_DataTableAge.Rows.Count - 1
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    sValore = "0"

                    ' Con un indice eseguo il ciclo su tutti i veicoli, escludendio l'ultimo
                    For iIndexAge As Integer = 1 To m_DataTableAge.Rows.Count - 1
                        sTotalPlantValue = GetVariableDefineValue("TotalPlant", iIDPlayer, 0, iIndexAge)

                        sValore = Nn(sValore) + Nn(sNewPlantCostValue) * Nn(sTotalPlantValue) * iNumeroVeicoli / iNumMaxVeicoli
                        iNumeroVeicoli -= 1
                    Next

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "NetPlants_409 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Acquisti di prodotto finito 
    ''' </summary>
    Private Sub ExtraMoneyAllow_410()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "ExtraMoneyAllow"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String
        Dim iIDItem As Integer
        Dim sItemName As String

        Dim ExtraProduFinan As Double
        Dim PerceFirstRequi As Double
        Dim ExtraRequi As Double
        Dim ThresDiscoExtra As Double
        Dim ExtraProduCost As Double
        Dim DiscoLevelExtra As Double

        Dim sNomeVariabileLista As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = 0

                    ExtraProduFinan = Nn(GetVariableDefineValue("ExtraProduFinan", iIDPlayer, 0, 0))
                    PerceFirstRequi = Nn(GetVariableDefineValue("PerceFirstRequi", iIDPlayer, 0, 0))
                    DiscoLevelExtra = Nn(GetVariableBoss("DiscoLevelExtra", "DiscoLevelExtra"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        sNomeVariabileLista = "var_" & sTeamName & "_ExtraRequi_" & sItemName
                        ExtraRequi = Nn(GetVariableDecisionPlayer(m_IDGame, "ExtraRequi", iIDPlayer, iIDItem))

                        ThresDiscoExtra = Nn(GetVariableBoss("ThresDiscoExtra", "ThresDiscoExtra_" & sItemName))
                        ExtraProduCost = Nn(GetVariableBoss("ExtraProduCost", "ExtraProduCost_" & sItemName))

                        sValore += ((Min(ExtraRequi * PerceFirstRequi / 100, ThresDiscoExtra) * ExtraProduCost +
                                   (Max(0, ExtraRequi * PerceFirstRequi / 100 - ThresDiscoExtra) * ExtraProduCost * (100 - DiscoLevelExtra) / 100)))
                    Next

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

                'sValore = Nn(ExtraProduFinan) * Nn(PerceFirstRequi) / 100
            End If
        Catch ex As Exception
            g_MessaggioErrore &= "ExtraMoneyAllow_410 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Acquisti di prodotti finiti
    ''' </summary>
    Private Sub CumulExtraProdu_411()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CumulExtraProdu"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sVariaExtraProduValue As String
        Dim sExtraMoneyAllowValue As String
        Dim sDischExtraProduValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sVariaExtraProduValue = GetVariableState("VariaExtraProdu", iIDPlayer, "", 0, 0)

                    sExtraMoneyAllowValue = GetVariableDefineValue("ExtraMoneyAllow", iIDPlayer, 0, 0)
                    sDischExtraProduValue = GetVariableDefineValue("DischExtraProdu", iIDPlayer, 0, 0)

                    sValore = Nn(sVariaExtraProduValue) + Nn(sExtraMoneyAllowValue) - Nn(sDischExtraProduValue)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "CumulExtraProdu_411 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Ratio spese/Liquidità
    ''' </summary>
    Private Sub PerceExpenCash_412()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "PerceExpenCash"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim BankLevel As Double
        Dim TotalCashNeede As Double
        Dim PerceFirstRequi As Double

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = 0

                    BankLevel = Nn(GetVariableState("BankLevel", iIDPlayer, "", 0, 0))

                    TotalCashNeede = Nn(GetVariableDefineValue("TotalCashNeede", iIDPlayer, 0, 0))
                    PerceFirstRequi = Nn(GetVariableDefineValue("PerceFirstRequi", iIDPlayer, 0, 0))

                    If BankLevel <> 0 Then
                        sValore = (TotalCashNeede * PerceFirstRequi * 100) / BankLevel * 100
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "PerceExpenCash_412 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Materie prime locali concesse
    ''' </summary>
    Private Sub PurchAllowEurop_413()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "PurchAllowEurop"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim sPurchEuropRequiValue As String
        Dim sPerceFirstRequiValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    sValore = 0

                    sPerceFirstRequiValue = GetVariableDefineValue("PerceFirstRequi", iIDPlayer, 0, 0)

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        sPurchEuropRequiValue = GetVariableDecisionPlayer(m_IDGame, "PurchEuropRequi", iIDPlayer, iIDItem)

                        sValore = Max(0, Truncate(Nn(sPurchEuropRequiValue) * Nn(sPerceFirstRequiValue) / 100 / 100) * 100)

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "PurchAllowEurop_413 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Materie prime importate concesse
    ''' </summary>
    Private Sub PurchAllowNIC_414()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "PurchAllowNIC"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim sPurchNICRequiValue As String
        Dim sPerceFirstRequiValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    sValore = 0

                    sPerceFirstRequiValue = GetVariableDefineValue("PerceFirstRequi", iIDPlayer, 0, 0)

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        sPurchNICRequiValue = GetVariableDecisionPlayer(m_IDGame, "PurchNICRequi", iIDPlayer, iIDItem)

                        sValore = Max(0, Truncate(Nn(sPurchNICRequiValue) * Nn(sPerceFirstRequiValue) / 100 / 100) * 100)

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "PurchAllowNIC_414 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Produzione concessa (finanziariamente)
    ''' </summary>
    Private Sub ProduAllow_415()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "ProduAllow"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim sProduBasicRequiValue As String
        Dim sPerceFirstRequiValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    sValore = 0

                    sPerceFirstRequiValue = GetVariableDefineValue("PerceFirstRequi", iIDPlayer, 0, 0)

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        sProduBasicRequiValue = GetVariableDecisionPlayer(m_IDGame, "ProduBasicRequi", iIDPlayer, iIDItem)

                        sValore = Max(0, Truncate(Nn(sProduBasicRequiValue) * Nn(sPerceFirstRequiValue) / 100 / 100) * 100)

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "ProduAllow_415 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Prodotti finiti concessi
    ''' </summary>
    Private Sub ExtraAllow_416()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "ExtraAllow"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim dAVAILABILITY As Double = 0
        Dim dWITHIN As Double = 0
        Dim dBEYOND As Double = 0
        Dim dPERCE As Double = 0

        Dim sNomeVariabileLista As String

        Dim sExtraRequiValue As String

        Dim sExtraProduFinanValue As String
        Dim sPerceFirstRequiValue As String

        Dim sExtraProduCostValue As String
        Dim sThresDiscoExtraValue As String
        Dim sDiscoLevelExtraValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = "0"

                    dAVAILABILITY = 0
                    dWITHIN = 0
                    dBEYOND = 0
                    dPERCE = 0

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        sExtraRequiValue = GetVariableDecisionPlayer(m_IDGame, "ExtraRequi", iIDPlayer, iIDItem)

                        If Nn(sExtraRequiValue) = 0 Then
                            sValore = "0"
                        Else
                            sExtraProduFinanValue = GetVariableDefineValue("ExtraProduFinan", iIDPlayer, 0, 0)
                            sPerceFirstRequiValue = GetVariableDefineValue("PerceFirstRequi", iIDPlayer, 0, 0)
                            dAVAILABILITY = Truncate(Nn(sExtraProduFinanValue) * Nn(sPerceFirstRequiValue) / 100)

                            sNomeVariabileLista = "ExtraProduCost_" & sItemName
                            sExtraProduCostValue = GetVariableBoss("ExtraProduCost", sNomeVariabileLista)
                            sDiscoLevelExtraValue = GetVariableBoss("DiscoLevelExtra", "")
                            sThresDiscoExtraValue = GetVariableBoss("ThresDiscoExtra", "ThresDiscoExtra_" & sItemName)
                            sExtraProduFinanValue = GetVariableDefineValue("ExtraProduFinan", iIDPlayer, 0, 0)
                            dWITHIN = Min(Nn(sExtraRequiValue) * Nn(sExtraProduCostValue), Nn(sThresDiscoExtraValue) * Nn(sExtraProduCostValue))
                            dBEYOND = Max(0, (Nn(sExtraRequiValue) - Nn(sThresDiscoExtraValue))) * (Nn(sExtraProduCostValue) * (1 - (Nn(sDiscoLevelExtraValue) / 100)))

                            If dWITHIN + dBEYOND = 0 Then
                                dPERCE = 0
                            Else
                                dPERCE = dWITHIN / (dWITHIN + dBEYOND)
                            End If

                            If Nn(sExtraProduFinanValue) = 0 Then
                                sValore = "0"
                            ElseIf dAVAILABILITY = Nn(sExtraProduFinanValue) Then
                                sValore = Nn(sExtraRequiValue)
                            Else
                                sValore = Min(Nn(sExtraRequiValue), (dPERCE + (1 - dPERCE) * (1 + Nn(sDiscoLevelExtraValue) / 100)) * (dAVAILABILITY / Nn(sExtraProduFinanValue)) * Nn(sExtraRequiValue))
                            End If
                        End If

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "ProduAllow_415 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Macchinari acquistati concessi
    ''' </summary>
    Private Sub PlantAllow_417()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "PlantAllow"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sPlantRequiValue As String
        Dim sPerceFirstRequiValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    sValore = "0"

                    sPlantRequiValue = GetVariableDecisionPlayer(m_IDGame, "PlantRequi", iIDPlayer, 0)
                    sPerceFirstRequiValue = GetVariableDefineValue("PerceFirstRequi", iIDPlayer, 0, 0)

                    sValore = Max(0, Truncate(Nn(sPlantRequiValue) * Nn(sPerceFirstRequiValue) / 100))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "PlantAllow_417 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Leasing dei macchinari concessi
    ''' </summary>
    Private Sub LeaseAllow_418()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "LeaseAllow"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sLeaseRequiValue As String
        Dim sPerceFirstRequiValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    sValore = "0"

                    sLeaseRequiValue = GetVariableDecisionPlayer(m_IDGame, "LeaseRequi", iIDPlayer, 0)
                    sPerceFirstRequiValue = GetVariableDefineValue("PerceFirstRequi", iIDPlayer, 0, 0)

                    If Nn(sLeaseRequiValue) > 0 Then
                        sValore = Round(Nn(sLeaseRequiValue) * Nn(sPerceFirstRequiValue) / 100)
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "LeaseAllow_418 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Automation Technology concesso
    ''' </summary>
    Private Sub TechnAutomAllow_419()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TechnAutomAllow"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sTechnAutomRequiValue As String
        Dim sPerceFirstRequiValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    sValore = "0"

                    sTechnAutomRequiValue = GetVariableDecisionPlayer(m_IDGame, "TechnAutomRequi", iIDPlayer, 0)
                    sPerceFirstRequiValue = GetVariableDefineValue("PerceFirstRequi", iIDPlayer, 0, 0)

                    sValore = Max(0, Nn(sTechnAutomRequiValue) * Nn(sPerceFirstRequiValue) / 100)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "TechnAutomAllow_419 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Importazioni pendenti da ricevere 
    ''' </summary>
    Private Sub PortfPurchNIC_420()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "PortfPurchNIC"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim sLagDelivNICValue As String
        Dim sPurchAllowNICValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sLagDelivNICValue = GetVariableBoss("LagDelivNIC", "")

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    sValore = "0"

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        sPurchAllowNICValue = GetVariableDefineValue("PurchAllowNIC", iIDPlayer, iIDItem, 0)

                        sValore = Nn(sPurchAllowNICValue) - Round((Nn(sPurchAllowNICValue) * ((m_LunghezzaPeriodo - Nn(sLagDelivNICValue)) / m_LunghezzaPeriodo)) * 100) / 100

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next

                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "PortfPurchNIC_420 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

#End Region

#Region "101 - 120 FUNZIONI DI CALCOLO"

    ''' <summary>
    ''' Incremento dell'età dei macchinari prop.
    ''' </summary>
    Private Sub IncreAgePlant_501()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "IncreAgePlant"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sNomeVariabileLista As String
        Dim sNomeVariabileListaMinor As String

        Dim sPlantPerAgeValue As String
        Dim sPlantPerAgeValueMinor As String
        Dim sOperaPlantScrapValue As String
        Dim sPlantAllowValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    ' Con un indice eseguo il ciclo su tutti i veicoli, escludendio l'ultimo
                    For iIndexAge As Integer = 1 To m_DataTableAge.Rows.Count
                        sValore = "0"

                        sNomeVariabileLista = "var_" & sTeamName & "_PlantPerAge_" & iIndexAge
                        sNomeVariabileListaMinor = "var_" & sTeamName & "_PlantPerAge_" & iIndexAge - 1
                        sPlantPerAgeValue = GetVariableState("PlantPerAge", iIDPlayer, sNomeVariabileLista, 0, iIndexAge)
                        sPlantAllowValue = GetVariableDefineValue("PlantAllow", iIDPlayer, 0, 0)
                        sPlantPerAgeValueMinor = GetVariableState("PlantPerAge", iIDPlayer, sNomeVariabileListaMinor, 0, iIndexAge - 1)
                        sOperaPlantScrapValue = GetVariableDefineValue("OperaPlantScrap", iIDPlayer, 0, iIndexAge - 1)

                        If iIndexAge > 1 Then
                            sValore = -Nn(sPlantPerAgeValue) + Nn(sPlantPerAgeValueMinor) - Nn(sOperaPlantScrapValue)
                        End If

                        If iIndexAge = 1 Then
                            sValore = -Nn(sPlantPerAgeValue) + Max(0, Nn(sPlantAllowValue))
                        End If

                        SaveVariableValue(iIDVariabile, iIDPlayer, 0, iIndexAge, sValore)
                    Next
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "IncreAgePlant_501 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Incremento dell'età dei leasing
    ''' </summary>
    Private Sub IncreAgeLease_502()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "IncreAgeLease"

        Dim IncreAgeLease As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sNomeVariabileLista As String
        Dim sNomeVariabileListaMinor As String

        Dim sLeasePerAgeValue As String
        Dim sLeasePerAgeValueMinor As String

        Dim sLeaseAllowValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    For iIndexAge As Integer = 1 To m_DataTableAge.Rows.Count
                        IncreAgeLease = "0"

                        sNomeVariabileLista = "var_" & sTeamName & "_LeasePerAge_" & iIndexAge
                        sNomeVariabileListaMinor = "var_" & sTeamName & "_LeasePerAge_" & iIndexAge - 1

                        sLeasePerAgeValue = GetVariableState("LeasePerAge", iIDPlayer, sNomeVariabileLista, 0, iIndexAge)
                        sLeasePerAgeValueMinor = GetVariableState("LeasePerAge", iIDPlayer, sNomeVariabileListaMinor, 0, iIndexAge - 1)

                        sLeaseAllowValue = GetVariableDefineValue("LeaseAllow", iIDPlayer, 0, 0)

                        If iIndexAge = (12 / m_LunghezzaPeriodo) AndAlso iIndexAge <> 1 Or iIndexAge = 3 Then
                            IncreAgeLease = -Nn(sLeasePerAgeValue)
                        End If

                        If iIndexAge < 12 / m_LunghezzaPeriodo Or iIndexAge = 4 Then
                            IncreAgeLease = -Nn(sLeasePerAgeValue) + Nn(sLeasePerAgeValueMinor)
                        End If

                        If iIndexAge = 1 Then
                            IncreAgeLease = -Nn(sLeasePerAgeValue) + Max(0, Nn(sLeaseAllowValue))
                        End If

                        SaveVariableValue(iIDVariabile, iIDPlayer, 0, iIndexAge, IncreAgeLease)
                    Next

                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "IncreAgeLease_502 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Produzione possibile (materie prime)
    ''' </summary>
    Private Sub ProduPossiRaw_503()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "ProduPossiRaw"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim sNomeVariabileLista As String

        Dim sProduAllowValue As String = "0"
        Dim sPurchAllowEuropValue As String = "0"
        Dim sStockRawMaterValue As String = "0"
        Dim sPurchDelivNICValue As String = "0"
        Dim sPurchAllowNICValue As String = "0"
        Dim sPortfPurchNICValue As String = "0"

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    sValore = "0"

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        sProduAllowValue = GetVariableDefineValue("ProduAllow", iIDPlayer, iIDItem, 0)
                        sPurchAllowEuropValue = GetVariableDefineValue("PurchAllowEurop", iIDPlayer, iIDItem, 0)

                        sNomeVariabileLista = "var_" & sTeamName & "_StockRawMater_" & sItemName
                        sStockRawMaterValue = GetVariableState("StockRawMater", iIDPlayer, sNomeVariabileLista, iIDItem, 0)

                        sNomeVariabileLista = "var_" & sTeamName & "_PurchDelivNIC_" & sItemName
                        sPurchDelivNICValue = GetVariableState("PurchDelivNIC", iIDPlayer, sNomeVariabileLista, iIDItem, 0)

                        sPurchAllowNICValue = GetVariableDefineValue("PurchAllowNIC", iIDPlayer, iIDItem, 0)
                        sPortfPurchNICValue = GetVariableDefineValue("PortfPurchNIC", iIDPlayer, iIDItem, 0)

                        sValore = Min(Nn(sProduAllowValue), Nn(sPurchAllowEuropValue) + Nn(sStockRawMaterValue) + Nn(sPurchDelivNICValue) + Nn(sPurchAllowNICValue) - Nn(sPortfPurchNICValue))

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "ProduPossiRaw_503 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Ore totali richieste per la produzione
    ''' </summary>
    Private Sub TotalHoursRequi_504()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TotalHoursRequi"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim sNomeVariabileLista As String

        Dim sHoursPerItemValue As String = "0"
        Dim sProduPossiRawValue As String = "0"

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = "0"

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableNameDefault"))

                        sNomeVariabileLista = "var_" & sTeamName & "_HoursPerItem_" & sItemName
                        sHoursPerItemValue = GetVariableBoss("HoursPerItem", sNomeVariabileLista)

                        sProduPossiRawValue = GetVariableDefineValue("ProduPossiRaw", iIDPlayer, iIDItem, 0)

                        sValore = Nn(sValore) + (Nn(sHoursPerItemValue) * Nn(sProduPossiRawValue))

                    Next

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "TotalHoursRequi_504 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Incremento della media annua tecnologia
    ''' </summary>
    Private Sub IncreAveraTechn_505()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "IncreAveraTechn"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sAveraTechnInvesValue As String = "0"
        Dim sTechnAutomAllowValue As String = "0"

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    sValore = "0"

                    sTechnAutomAllowValue = GetVariableDefineValue("TechnAutomAllow", iIDPlayer, 0, 0)
                    sAveraTechnInvesValue = GetVariableState("AveraTechnInves", iIDPlayer, "", 0, 0)

                    sValore = (Nn(sTechnAutomAllowValue) - Nn(sAveraTechnInvesValue)) / m_CurrentStep
                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "IncreAveraTechn_505 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Investimento annuale in tecnologia
    ''' </summary>
    Private Sub TechnAutomYear_506()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TechnAutomYear"

        Dim TechnAutomYear As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sTechnAutomAllowValue As String = "0"

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    TechnAutomYear = "0"

                    sTechnAutomAllowValue = GetVariableDefineValue("TechnAutomAllow", iIDPlayer, 0, 0)

                    TechnAutomYear = Nn(sTechnAutomAllowValue) * (m_LunghezzaPeriodo / 12)
                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, TechnAutomYear)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "IncreAveraTechn_506 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Tabella sulle spese tecnologiche
    ''' </summary>
    Private Sub TableTechnAllow_507()
        Dim iIDVariabile As Integer
        Dim dValueCalc As String = ""
        Dim sNomeVariabileCalc As String = "TableTechnAllow"
        Dim sNomeVariabileEntrata As String = "TechnAutomYear"
        Dim iIDPlayer As Integer = 0

        Try
            ' Ciclo sui players presenti nel game
            If m_DataTablePlayers.Rows.Count > 0 Then
                ' Controllo la presenza della variabile da calcolare e mi faccio ritornare l'id 
                iIDVariabile = CheckVariableExists(sNomeVariabileCalc, m_IDGame)
                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))

                    dValueCalc = GetVariableDefineValue(sNomeVariabileEntrata, iIDPlayer, 0, 0)
                    dValueCalc = CalculateInterpolation(sNomeVariabileCalc, dValueCalc)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, dValueCalc)
                Next
            End If

        Catch ex As Exception
            g_MessaggioErrore &= "TechnAutomYear_507 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Incremento cumulato della tecnologia
    ''' </summary>
    Private Sub IncreTechnCumul_508()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "IncreTechnCumul"

        Dim IncreTechnCumul As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim TechnAutomAllow As String = "0"
        Dim TableTechnAllow As String = "0"
        Dim BossAutomAllow As String = "0"
        Dim BossAutomAvera As String = "0"
        Dim TableAveraTechn As String = "0"

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    IncreTechnCumul = "0"

                    TechnAutomAllow = GetVariableDefineValue("TechnAutomAllow", iIDPlayer, 0, 0)

                    If Nn(TechnAutomAllow) > 0 Then
                        TableTechnAllow = GetVariableDefineValue("TableTechnAllow", iIDPlayer, 0, 0)
                        BossAutomAllow = GetVariableDeveloper(m_IDGame, "BossAutomAllow", 0, 0)
                        BossAutomAvera = GetVariableDeveloper(m_IDGame, "BossAutomAvera", 0, 0)
                        TableAveraTechn = GetVariableDefineValue("TableAveraTechn", iIDPlayer, 0, 0)
                        IncreTechnCumul = (Nn(TableTechnAllow) * (1 + Nn(BossAutomAllow) / 10) + Nn(TableAveraTechn) * (1 + Nn(BossAutomAvera) / 15))
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, IncreTechnCumul)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "IncreTechnCumul_508 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Incremento medio di mercato della tecnologia
    ''' </summary>
    Private Sub MarkeAveraTechn_509()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "MarkeAveraTechn"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sIncreTechnCumulValue As String = "0"

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"
                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sIncreTechnCumulValue = GetVariableDefineValue("IncreTechnCumul", iIDPlayer, 0, 0)

                    sValore = Nn(sValore) + Nn(sIncreTechnCumulValue)

                Next
                sValore = Nn(sValore) / Max(1, m_DataTablePlayers.Rows.Count)

                SaveVariableValue(iIDVariabile, 0, 0, 0, sValore)

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "MarkeAveraTechn_509 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Incremento della media investimenti tecnologici
    ''' </summary>
    Private Sub IncreAbsolAutom_510()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "IncreAbsolAutom"

        Dim IncreAbsolAutom As String

        Dim MarkeAveraTechn As String = "0"
        Dim SpeedWorldChang As String = "0"
        Dim AbsolQualiProce As String = "0"

        Try
            iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

            IncreAbsolAutom = "0"

            MarkeAveraTechn = GetVariableDefineValue("MarkeAveraTechn", 0, 0, 0)
            SpeedWorldChang = GetVariableDeveloper(m_IDGame, "SpeedWorldChang", 0, 0)
            AbsolQualiProce = GetVariableState("AbsolQualiProce", 0, "", 0, 0)

            IncreAbsolAutom = Min(20, Nn(MarkeAveraTechn) - Nn(SpeedWorldChang))
            If (MarkeAveraTechn > SpeedWorldChang) Then
                IncreAbsolAutom = Min(100 - Nn(AbsolQualiProce), Nn(IncreAbsolAutom) * (100 - Nn(AbsolQualiProce)) / 100)
            Else
                IncreAbsolAutom = Min(Nn(AbsolQualiProce) - 45, Nn(IncreAbsolAutom) * (Nn(AbsolQualiProce) - 45) / 100)
            End If
            SaveVariableValue(iIDVariabile, 0, 0, 0, IncreAbsolAutom)

        Catch ex As Exception
            g_MessaggioErrore &= "IncreAbsolAutom_510 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Spese tecnologiche medie
    ''' </summary>
    Private Sub FulvioTeknoRap_511()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "FulvioTeknoRap"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sTechnAutomAllowValue As String = "0"

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"
                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sTechnAutomAllowValue = GetVariableDefineValue("TechnAutomAllow", iIDPlayer, 0, 0)

                    sValore = Nn(sValore) + Nn(sTechnAutomAllowValue)
                Next

                SaveVariableValue(iIDVariabile, 0, 0, 0, sValore)
            End If
        Catch ex As Exception
            g_MessaggioErrore &= "FulvioTeknoRap_511 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try

    End Sub

    ''' <summary>
    ''' Riduzione del tempo di produzione
    ''' </summary>
    Private Sub DecreHoursItem_512()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DecreHoursItem"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer

        Dim sTableAveraTechnValue As String = "0"
        Dim sTableTechnAllowValue As String = "0"
        Dim sHoursPerItemValue As String = "0"
        Dim sNomeVariabileLista As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"
                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sTableAveraTechnValue = GetVariableDefineValue("TableAveraTechn", iIDPlayer, 0, 0)
                    sTableTechnAllowValue = GetVariableDefineValue("TableTechnAllow", iIDPlayer, 0, 0)

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sNomeVariabileLista = "var_" & sTeamName & "_HoursPerItem_" & Nz(oRowItem("VariableName"))
                        sHoursPerItemValue = GetVariableBoss("HoursPerItem", sNomeVariabileLista)

                        sValore = (Min(5.0, 1.1 + Nn(sTableAveraTechnValue)) + Min(4.0, 0.5 + Nn(sTableTechnAllowValue))) / 100 * Nn(sHoursPerItemValue)

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next
                Next
            End If
        Catch ex As Exception
            g_MessaggioErrore &= "DecreHoursItem_512 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Tempo unitario di produzione (Prossimo)
    ''' </summary>
    Private Sub NextHoursItem_513()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "NextHoursItem"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer

        Dim sDecreHoursItemValue As String = "0"
        Dim sHoursPerItemValue As String = "0"
        Dim sNomeVariabileLista As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"
                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sNomeVariabileLista = "var_" & sTeamName & "_HoursPerItem_" & Nz(oRowItem("VariableName"))
                        sHoursPerItemValue = GetVariableBoss("HoursPerItem", sNomeVariabileLista)

                        sDecreHoursItemValue = GetVariableDefineValue("DecreHoursItem", iIDPlayer, iIDItem, 0)

                        sValore = Nn(sHoursPerItemValue) - Nn(sDecreHoursItemValue)

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next
                Next
            End If
        Catch ex As Exception
            g_MessaggioErrore &= "NextHoursItem_513 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Veicoli conccessi
    ''' </summary>
    Private Sub VehicAllow_514()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "VehicAllow"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sVehicRequiValue As String = "0"
        Dim sPerceFirstRequiValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)


                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    sValore = "0"

                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sVehicRequiValue = GetVariableDecisionPlayer(m_IDGame, "VehicRequi", iIDPlayer, 0)
                    sPerceFirstRequiValue = GetVariableDefineValue("PerceFirstRequi", iIDPlayer, 0, 0)

                    If Nn(sVehicRequiValue) > 0 Then
                        sValore = Max(0, Truncate(Nn(sVehicRequiValue) * Nn(sPerceFirstRequiValue) / 100))
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)

                Next
            End If
        Catch ex As Exception
            g_MessaggioErrore &= "VehicAllow_514 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Nuovi negozi centrali in affitto
    ''' </summary>
    Private Sub CentrStoreAllow_515()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CentrStoreAllow"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sCentrStoreRequiValue As String = "0"
        Dim sPerceFirstRequiValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)


                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    sValore = "0"

                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sCentrStoreRequiValue = GetVariableDecisionPlayer(m_IDGame, "CentrStoreRequi", iIDPlayer, 0)
                    sPerceFirstRequiValue = GetVariableDefineValue("PerceFirstRequi", iIDPlayer, 0, 0)

                    If Nn(sCentrStoreRequiValue) > 0 Then
                        sValore = Max(0, Truncate(Nn(sCentrStoreRequiValue) * Nn(sPerceFirstRequiValue) / 100))
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)

                Next
            End If
        Catch ex As Exception
            g_MessaggioErrore &= "CentrStoreAllow_515 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Nuovi negozi periferci in affitto
    ''' </summary>
    Private Sub PerifStoreAllow_516()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "PerifStoreAllow"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sPerifStoreRequiValue As String = "0"
        Dim sPerceFirstRequiValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)


                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    sValore = "0"

                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sPerifStoreRequiValue = GetVariableDecisionPlayer(m_IDGame, "PerifStoreRequi", iIDPlayer, 0)
                    sPerceFirstRequiValue = GetVariableDefineValue("PerceFirstRequi", iIDPlayer, 0, 0)

                    sValore = Max(0, Round(Nn(sPerifStoreRequiValue) * Nn(sPerceFirstRequiValue) / 100))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)

                Next
            End If
        Catch ex As Exception
            g_MessaggioErrore &= "PerifStoreAllow_516 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Staff concesso
    ''' </summary>
    Private Sub NewStaffAllow_517()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "NewStaffAllow"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sPerceFirstRequiValue As String
        Dim sNewPersoStaffValue As String
        Dim sTotalStaffPersoValue As String
        Dim sMaximDismiPersoValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)


                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    sValore = "0"

                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sPerceFirstRequiValue = GetVariableDefineValue("PerceFirstRequi", iIDPlayer, 0, 0)
                    sNewPersoStaffValue = GetVariableDecisionPlayer(m_IDGame, "NewPersoStaff", iIDPlayer, 0)
                    sTotalStaffPersoValue = GetVariableState("TotalStaffPerso", iIDPlayer, "", 0, 0)
                    sMaximDismiPersoValue = GetVariableBoss("MaximDismiPerso", "")

                    If sNewPersoStaffValue < 0 Then
                        sValore = Truncate(Max(Nn(sNewPersoStaffValue), -Nn(sTotalStaffPersoValue) * (Nn(sMaximDismiPersoValue) / 100) * (m_LunghezzaPeriodo / 12)))
                    Else
                        sValore = Truncate(sNewPersoStaffValue * Nn(sPerceFirstRequiValue) / 100)
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)

                Next
            End If
        Catch ex As Exception
            g_MessaggioErrore &= "NewStaffAllow_517 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Venditori concessi
    ''' </summary>
    Private Sub NewPointAllow_518()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "NewPointAllow"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sPerceFirstRequiValue As String
        Dim sNewPersoPointValue As String
        Dim sTotalPointPersoValue As String
        Dim sMaximDismiPersoValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)


                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    sValore = "0"

                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sPerceFirstRequiValue = GetVariableDefineValue("PerceFirstRequi", iIDPlayer, 0, 0)
                    sNewPersoPointValue = GetVariableDecisionPlayer(m_IDGame, "NewPersoPoint", iIDPlayer, 0)
                    sTotalPointPersoValue = GetVariableState("TotalPointPerso", iIDPlayer, "", 0, 0)
                    sMaximDismiPersoValue = GetVariableBoss("MaximDismiPerso", "")

                    If Nn(sNewPersoPointValue) < 0 Then
                        sValore = Truncate(Max(Nn(sNewPersoPointValue), -Nn(sTotalPointPersoValue) * (Nn(sMaximDismiPersoValue) / 100) * (m_LunghezzaPeriodo / 12)))
                    Else
                        sValore = Truncate(Nn(sNewPersoPointValue) * Nn(sPerceFirstRequiValue) / 100)
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)

                Next
            End If
        Catch ex As Exception
            g_MessaggioErrore &= "NewPointAllow_518 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Formazione concessa
    ''' </summary>
    Private Sub AddesTotalAllow_519()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "AddesTotalAllow"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sAddesTotalPersoValue As String
        Dim sPerceFirstRequiValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    sValore = "0"

                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sPerceFirstRequiValue = GetVariableDefineValue("PerceFirstRequi", iIDPlayer, 0, 0)
                    sAddesTotalPersoValue = GetVariableDecisionPlayer(m_IDGame, "AddesTotalPerso", iIDPlayer, 0)

                    sValore = Nn(sAddesTotalPersoValue) * Nn(sPerceFirstRequiValue) / 100

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)

                Next
            End If
        Catch ex As Exception
            g_MessaggioErrore &= "AddesTotalAllow_519 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Customer service concesso
    ''' </summary>
    Private Sub TrainServiAllow_520()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TrainServiAllow"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sTrainServiRequiValue As String
        Dim sPerceFirstRequiValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    sValore = "0"

                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sPerceFirstRequiValue = GetVariableDefineValue("PerceFirstRequi", iIDPlayer, 0, 0)
                    sTrainServiRequiValue = GetVariableDecisionPlayer(m_IDGame, "TrainServiRequi", iIDPlayer, 0)

                    sValore = Max(0, Nn(sTrainServiRequiValue) * Nn(sPerceFirstRequiValue) / 100)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)

                Next
            End If
        Catch ex As Exception
            g_MessaggioErrore &= "TrainServiAllow_520 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

#End Region

#Region "121 - 140 FUNZIONI DI CALCOLO "

    ''' <summary>
    ''' Promozioni concesse
    ''' </summary>
    Private Sub MarkeExpenAllow_601()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "MarkeExpenAllow"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer

        Dim sMarkeExpenRequiValue As String = "0"
        Dim sPerceFirstRequiValue As String = "0"

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"
                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))

                        sMarkeExpenRequiValue = GetVariableDecisionPlayer(m_IDGame, "MarkeExpenRequi", iIDPlayer, iIDItem)
                        sPerceFirstRequiValue = GetVariableDefineValue("PerceFirstRequi", iIDPlayer, 0, 0)

                        sValore = Max(0, Nn(sMarkeExpenRequiValue) * Nn(sPerceFirstRequiValue) / 100)

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next
                Next
            End If
        Catch ex As Exception
            g_MessaggioErrore &= "MarkeExpenAllow_601 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Incremento dell'età dei  veicoli
    ''' </summary>
    Private Sub IncreAgeVehic_602()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "IncreAgeVehic"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sNomeVariabileLista As String
        Dim sNomeVariabileListaMinor As String

        Dim sTotalVehicOwnedValue As String
        Dim sTotalVehicOwnedValueMinor As String
        Dim sVehicScrapAgeValue As String
        Dim sVehicAllowValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    ' Con un indice eseguo il ciclo su tutti i veicoli, escludendio l'ultimo
                    For iIndexAge As Integer = 1 To m_DataTableAge.Rows.Count
                        sValore = "0"

                        sNomeVariabileLista = "var_" & sTeamName & "_TotalVehicOwned_" & iIndexAge
                        sTotalVehicOwnedValue = GetVariableState("TotalVehicOwned", iIDPlayer, sNomeVariabileLista, 0, iIndexAge)

                        sNomeVariabileListaMinor = "var_" & sTeamName & "_TotalVehicOwned_" & iIndexAge - 1
                        sTotalVehicOwnedValueMinor = GetVariableState("TotalVehicOwned", iIDPlayer, sNomeVariabileListaMinor, 0, iIndexAge - 1)

                        sVehicScrapAgeValue = GetVariableDefineValue("VehicScrapAge", iIDPlayer, 0, iIndexAge - 1)

                        sVehicAllowValue = GetVariableDefineValue("VehicAllow", iIDPlayer, 0, 0)

                        If iIndexAge > 1 Then
                            sValore = -Nn(sTotalVehicOwnedValue) + Nn(sTotalVehicOwnedValueMinor) - Nn(sVehicScrapAgeValue)
                        End If

                        If iIndexAge = 1 Then
                            sValore = -Nn(sTotalVehicOwnedValue) + Max(0, Nn(sVehicAllowValue))
                        End If

                        SaveVariableValue(iIDVariabile, iIDPlayer, 0, iIndexAge, sValore)
                    Next
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "IncreAgeVehic_602 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Età media dei veicoli
    ''' </summary>
    Private Sub AveraAgeVehic_603()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "AveraAgeVehic"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iNumMaxVeicoli As Integer = m_DataTableAge.Rows.Count

        Dim sNomeVariabileLista As String

        Dim dCars As Double
        Dim sVehicAllowValue As String = "0"
        Dim sTotalVehicOwnedValue As String = "0"

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    sValore = 0
                    dCars = 0
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sVehicAllowValue = GetVariableDefineValue("VehicAllow", iIDPlayer, 0, 0)

                    ' Con un indice eseguo il ciclo su tutti i veicoli, escludendio l'ultimo
                    For iIndexAge As Integer = 1 To m_DataTableAge.Rows.Count
                        sNomeVariabileLista = "var_" & sTeamName & "_TotalVehicOwned_" & iIndexAge
                        dCars += Nn(GetVariableState("TotalVehicOwned", iIDPlayer, sNomeVariabileLista, 0, iIndexAge))
                    Next

                    If dCars + Nn(sVehicAllowValue) = 0 Then
                        sValore = 0

                    Else
                        For iIndexAge As Integer = 1 To m_DataTableAge.Rows.Count
                            sNomeVariabileLista = "var_" & sTeamName & "_TotalVehicOwned_" & iIndexAge
                            sTotalVehicOwnedValue = GetVariableState("TotalVehicOwned", iIDPlayer, sNomeVariabileLista, 0, iIndexAge)
                            sValore = Nn(sValore) + Nn(sTotalVehicOwnedValue * iIndexAge)
                        Next
                        sValore = Nn(sValore) / dCars

                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "AveraAgeVehic_603 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Unità trasportate medie
    ''' </summary>
    Private Sub TableAveraTrans_604()
        Dim iIDVariabile As Integer
        Dim dValueCalc As String = ""
        Dim sNomeVariabileCalc As String = "TableAveraTrans"
        Dim sNomeVariabileEntrata As String = "AveraAgeVehic"
        Dim iIDPlayer As Integer = 0

        Try
            ' Ciclo sui players presenti nel game
            If m_DataTablePlayers.Rows.Count > 0 Then
                ' Controllo la presenza della variabile da calcolare e mi faccio ritornare l'id 
                iIDVariabile = CheckVariableExists(sNomeVariabileCalc, m_IDGame)
                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))

                    dValueCalc = GetVariableDefineValue(sNomeVariabileEntrata, iIDPlayer, 0, 0)
                    dValueCalc = CalculateInterpolation(sNomeVariabileCalc, dValueCalc)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, dValueCalc)
                Next
            End If

        Catch ex As Exception
            g_MessaggioErrore &= "TableAveraTrans_604 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Capacità di trasporto attuale
    ''' </summary>
    Private Sub TotalCapacTrans_605()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TotalCapacTrans"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sTableAveraTransValue As String = "0"
        Dim sOperaVehicUsedValue As String = "0"

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"
                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sTableAveraTransValue = GetVariableDefineValue("TableAveraTrans", iIDPlayer, 0, 0)
                    sOperaVehicUsedValue = GetVariableDefineValue("OperaVehicUsed", iIDPlayer, 0, 0)

                    sValore = Nn(sTableAveraTransValue) * Nn(sOperaVehicUsedValue) * (m_LunghezzaPeriodo / 12)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next
            End If
        Catch ex As Exception
            g_MessaggioErrore &= "TotalCapacTrans_605 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Capacità di trasporto prossima
    ''' </summary>
    Private Sub NextCapacTrans_606()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "NextCapacTrans"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sTotalCapacTransValue As String = "0"
        Dim sOperaVehicUsedValue As String = "0"
        Dim sVehicAllowValue As String = "0"

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"
                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sTotalCapacTransValue = GetVariableDefineValue("TotalCapacTrans", iIDPlayer, 0, 0)
                    sOperaVehicUsedValue = GetVariableDefineValue("OperaVehicUsed", iIDPlayer, 0, 0)
                    sVehicAllowValue = GetVariableDefineValue("VehicAllow", iIDPlayer, 0, 0)

                    sValore = Nn(sTotalCapacTransValue) + Nn(sVehicAllowValue) * 30000 - Nn(sOperaVehicUsedValue) * 10000

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next
            End If
        Catch ex As Exception
            g_MessaggioErrore &= "TotalCapacTrans_605 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Negozi centrali attivi
    ''' </summary>
    Private Sub CurreOutleCentr_607()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CurreOutleCentr"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sTotalCentrStoreValue As String
        Dim sCentrStoreAllowValue As String
        Dim sCentrAlienAllowValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"
                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sTotalCentrStoreValue = GetVariableState("TotalCentrStore", iIDPlayer, "", 0, 0)
                    sCentrStoreAllowValue = GetVariableDefineValue("CentrStoreAllow", iIDPlayer, 0, 0)
                    sCentrAlienAllowValue = GetVariableDefineValue("CentrAlienAllow", iIDPlayer, 0, 0)

                    sValore = Nn(sTotalCentrStoreValue) + Nn(sCentrStoreAllowValue) - Nn(sCentrAlienAllowValue)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next
            End If
        Catch ex As Exception
            g_MessaggioErrore &= "CurreOutleCentr_607 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Negozi periferici attivi
    ''' </summary>
    Private Sub CurreOutleSubur_608()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CurreOutleSubur"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sTotalPerifStoreValue As String
        Dim sPerifStoreAllowValue As String
        Dim sPerifScrapAllowValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"
                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sTotalPerifStoreValue = GetVariableState("TotalPerifStore", iIDPlayer, "", 0, 0)
                    sPerifStoreAllowValue = GetVariableDefineValue("PerifStoreAllow", iIDPlayer, 0, 0)
                    sPerifScrapAllowValue = GetVariableDefineValue("PerifScrapAllow", iIDPlayer, 0, 0)

                    sValore = Nn(sTotalPerifStoreValue) + Nn(sPerifStoreAllowValue) - Nn(sPerifScrapAllowValue)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next
            End If
        Catch ex As Exception
            g_MessaggioErrore &= "CurreOutleSubur_608 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Capacità di rotazione totale dei negozi
    ''' </summary>
    Private Sub MaximSpaceAvail_609()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "MaximSpaceAvail"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sCurreOutleCentrValue As String
        Dim sGoodsSellaCentrValue As String
        Dim sGoodsSellaPerifValue As String
        Dim sCurreOutleSuburValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"
                sGoodsSellaCentrValue = GetVariableBoss("GoodsSellaCentr", "")
                sGoodsSellaPerifValue = GetVariableBoss("GoodsSellaPerif", "")

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sCurreOutleCentrValue = GetVariableDefineValue("CurreOutleCentr", iIDPlayer, 0, 0)
                    sCurreOutleSuburValue = GetVariableDefineValue("CurreOutleSubur", iIDPlayer, 0, 0)

                    sValore = Nn(sCurreOutleCentrValue) * Nn(sGoodsSellaCentrValue) + Nn(sCurreOutleSuburValue) * Nn(sGoodsSellaPerifValue)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next
            End If
        Catch ex As Exception
            g_MessaggioErrore &= "MaximSpaceAvail_609 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Staff
    ''' </summary>
    Private Sub CurreSuperStaff_610()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CurreSuperStaff"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sTotalStaffPersoValue As String
        Dim sNewStaffAllowValule As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sTotalStaffPersoValue = GetVariableState("TotalStaffPerso", iIDPlayer, "", 0, 0)
                    sNewStaffAllowValule = GetVariableDefineValue("NewStaffAllow", iIDPlayer, 0, 0)

                    sValore = Nn(sTotalStaffPersoValue) + Nn(sNewStaffAllowValule)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next
            End If
        Catch ex As Exception
            g_MessaggioErrore &= "CurreSuperStaff_610 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Venditori
    ''' </summary>
    Private Sub CurreSalesPeopl_611()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CurreSalesPeopl"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sTotalPointPersoValue As String
        Dim sNewPointAllowValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sTotalPointPersoValue = GetVariableState("TotalPointPerso", iIDPlayer, "", 0, 0)
                    sNewPointAllowValue = GetVariableDefineValue("NewPointAllow", iIDPlayer, 0, 0)

                    sValore = Nn(sTotalPointPersoValue) + Nn(sNewPointAllowValue)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next
            End If
        Catch ex As Exception
            g_MessaggioErrore &= "CurreSalesPeopl_611 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Variazione del livello formazione staff
    ''' </summary>
    Private Sub IncreStaffLevel_612()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "IncreStaffLevel"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sTotalStaffPersoValue As String
        Dim sNewStaffAllowValue As String

        Dim sAddesTotalAllowValue As String
        Dim sTotalPointPersoValue As String
        Dim sNewPointAllowValue As String

        Dim sDecreTrainLevelValue As String

        Dim sStaffTrainLevelValue As String

        Dim dIdealInvest As Double = 0
        Dim dBudgetDispo As Double = 0

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sTotalStaffPersoValue = GetVariableState("TotalStaffPerso", iIDPlayer, "", 0, 0)
                    sNewStaffAllowValue = GetVariableDefineValue("NewStaffAllow", iIDPlayer, 0, 0)
                    dIdealInvest = (2500 / 5) * (Nn(sTotalStaffPersoValue) - Max(0, -Nn(sNewStaffAllowValue))) + 2500 * Max(0, Nn(sNewStaffAllowValue))

                    sAddesTotalAllowValue = GetVariableDefineValue("AddesTotalAllow", iIDPlayer, 0, 0)
                    sTotalPointPersoValue = GetVariableState("TotalPointPerso", iIDPlayer, "", 0, 0)
                    sNewPointAllowValue = GetVariableDefineValue("NewPointAllow", iIDPlayer, 0, 0)
                    dBudgetDispo = Nn(sAddesTotalAllowValue) * (Nn(sTotalStaffPersoValue) + Nn(sNewStaffAllowValue)) / (Nn(sTotalStaffPersoValue) + Nn(sNewStaffAllowValue) + Nn(sTotalPointPersoValue) + Nn(sNewPointAllowValue))

                    sDecreTrainLevelValue = GetVariableBoss("DecreTrainLevel", "")

                    sValore = +20 * Min(dBudgetDispo, dIdealInvest) / dIdealInvest - Nn(sDecreTrainLevelValue)

                    sStaffTrainLevelValue = GetVariableState("StaffTrainLevel", iIDPlayer, "", 0, 0)
                    sValore = Min(Nn(sValore), 100 - Nn(sStaffTrainLevelValue))

                    sValore = Max(Nn(sValore), 30 - Nn(sStaffTrainLevelValue))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next
            End If
        Catch ex As Exception
            g_MessaggioErrore &= "IncreStaffLevel_612 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Variazione del livello addestr.commessi
    ''' </summary>
    Private Sub IncrePointLevel_613()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "IncrePointLevel"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sTotalPointPersoValue As String
        Dim sNewPointAllowValue As String

        Dim sAddesTotalAllowValue As String
        Dim sTotalStaffPersoValue As String
        Dim sNewStaffAllowValue As String

        Dim sDecreTrainLevelValue As String

        Dim sPointTrainLevelValue As String

        Dim dIdealInvest As Double = 0
        Dim dBudgetDispo As Double = 0

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    sValore = "0"
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sTotalPointPersoValue = GetVariableState("TotalPointPerso", iIDPlayer, "", 0, 0)
                    sNewPointAllowValue = GetVariableDefineValue("NewPointAllow", iIDPlayer, 0, 0)
                    dIdealInvest = (2500 / 5) * (Nn(sTotalPointPersoValue) - Max(0, -Nn(sNewPointAllowValue))) + 2500 * Max(0, Nn(sNewPointAllowValue))

                    sAddesTotalAllowValue = GetVariableDefineValue("AddesTotalAllow", iIDPlayer, 0, 0)
                    sTotalStaffPersoValue = GetVariableState("TotalStaffPerso", iIDPlayer, "", 0, 0)
                    sNewStaffAllowValue = GetVariableDefineValue("NewStaffAllow", iIDPlayer, 0, 0)
                    dBudgetDispo = Nn(sAddesTotalAllowValue) * (Nn(sTotalPointPersoValue) + Nn(sNewPointAllowValue)) / (Nn(sTotalStaffPersoValue) + Nn(sNewStaffAllowValue) + Nn(sTotalPointPersoValue) + Nn(sNewPointAllowValue))

                    sDecreTrainLevelValue = GetVariableBoss("DecreTrainLevel", "")
                    sValore = +20 * Min(dBudgetDispo, dIdealInvest) / dIdealInvest - Nn(sDecreTrainLevelValue)

                    sPointTrainLevelValue = GetVariableState("PointTrainLevel", iIDPlayer, "", 0, 0)
                    sValore = Min(Nn(sValore), 100 - Nn(sPointTrainLevelValue))
                    sValore = Max(Nn(sValore), 30 - Nn(sPointTrainLevelValue))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next
            End If
        Catch ex As Exception
            g_MessaggioErrore &= "IncrePointLevel_613 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Incremento medio del servizio al cliente
    ''' </summary>
    Private Sub IncreAveraTrain_614()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "IncreAveraTrain"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sTrainServiAllowValue As String
        Dim sAveraTrainInvesValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sTrainServiAllowValue = GetVariableDefineValue("TrainServiAllow", iIDPlayer, 0, 0)
                    sAveraTrainInvesValue = GetVariableState("AveraTrainInves", 0, "", 0, 0)

                    sValore = (Nn(sTrainServiAllowValue) - Nn(sAveraTrainInvesValue)) / m_CurrentStep

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next
            End If
        Catch ex As Exception
            g_MessaggioErrore &= "IncreAveraTrain_614 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Investimento annuale in servizio al cli.
    ''' </summary>
    Private Sub TrainServiYear_615()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TrainServiYear"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sTrainServiAllowValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sTrainServiAllowValue = GetVariableDefineValue("TrainServiAllow", iIDPlayer, 0, 0)

                    sValore = Nn(sTrainServiAllowValue) * (m_LunghezzaPeriodo / 12)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next
            End If
        Catch ex As Exception
            g_MessaggioErrore &= "TrainServiYear_615 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Tabella sulla media del servizio al cliente
    ''' </summary>
    Private Sub TableTrainPermit_616()
        Dim iIDVariabile As Integer
        Dim dValueCalc As String = ""
        Dim sNomeVariabileCalc As String = "TableTrainPermit"
        Dim sNomeVariabileEntrata As String = "TrainServiYear"
        Dim iIDPlayer As Integer = 0

        Try
            ' Ciclo sui players presenti nel game
            If m_DataTablePlayers.Rows.Count > 0 Then
                ' Controllo la presenza della variabile da calcolare e mi faccio ritornare l'id 
                iIDVariabile = CheckVariableExists(sNomeVariabileCalc, m_IDGame)
                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))

                    dValueCalc = GetVariableDefineValue(sNomeVariabileEntrata, iIDPlayer, 0, 0)
                    dValueCalc = CalculateInterpolation(sNomeVariabileCalc, dValueCalc)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, dValueCalc)
                Next
            End If

        Catch ex As Exception
            g_MessaggioErrore &= "TableTrainPermit_616 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Incremento del livello di servizio al cliente
    ''' </summary>
    Private Sub IncreTrainCumul_617()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "IncreTrainCumul"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sTrainServiAllowValue As String
        Dim sTableTrainPermitValue As String
        Dim sBossTrainAllowValue As String
        Dim sTableAveraTrainValue As String
        Dim sBossTrainAveraValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    sValore = "0"
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sTrainServiAllowValue = GetVariableDefineValue("TrainServiAllow", iIDPlayer, 0, 0)
                    sTableTrainPermitValue = GetVariableDefineValue("TableTrainPermit", iIDPlayer, 0, 0)
                    sBossTrainAllowValue = GetVariableDeveloper(m_IDGame, "BossTrainAllow", iIDPlayer, 0)
                    sTableAveraTrainValue = GetVariableDefineValue("TableAveraTrain", iIDPlayer, 0, 0)
                    sBossTrainAveraValue = GetVariableDeveloper(m_IDGame, "BossTrainAvera", iIDPlayer, 0)

                    If sTrainServiAllowValue > 0 Then
                        sValore = (Nn(sTableTrainPermitValue) * (1 + Nn(sBossTrainAllowValue) / 10) + Nn(sTableAveraTrainValue) * (1 + Nn(sBossTrainAveraValue) / 15))
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next
            End If
        Catch ex As Exception
            g_MessaggioErrore &= "IncreTrainCumul_617 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try

    End Sub

    ''' <summary>
    ''' Incremento medio dell'inv.in serv.al cl.
    ''' </summary>
    Private Sub MarkeAveraTrain_618()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "MarkeAveraTrain"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sIncreTrainCumulValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sIncreTrainCumulValue = GetVariableDefineValue("IncreTrainCumul", iIDPlayer, 0, 0)
                    sValore = Nn(sValore) + Nn(sIncreTrainCumulValue)

                Next
                sValore = Nn(sValore) / Max(1, m_DataTablePlayers.Rows.Count)

                SaveVariableValue(iIDVariabile, 0, 0, 0, sValore)
            End If
        Catch ex As Exception
            g_MessaggioErrore &= "MarkeAveraTrain_618 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Incremento medio mkt.servizio al cliente
    ''' </summary>
    Private Sub IncreAbsolTrain_619()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "IncreAbsolTrain"

        Dim IncreAbsolTrain As String

        Dim MarkeAveraTrain As String
        Dim SpeedWorldChang As String
        Dim AbsolQualiTrain As String

        Try

            iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

            IncreAbsolTrain = "0"

            MarkeAveraTrain = GetVariableDefineValue("MarkeAveraTrain", 0, 0, 0)
            SpeedWorldChang = GetVariableDeveloper(m_IDGame, "SpeedWorldChang", 0, 0)
            AbsolQualiTrain = GetVariableState("AbsolQualiTrain", 0, "", 0, 0)

            IncreAbsolTrain = Min(20, Nn(MarkeAveraTrain) - Nn(SpeedWorldChang))

            If (Nn(MarkeAveraTrain) > Nn(SpeedWorldChang)) Then
                IncreAbsolTrain = Min(100 - Nn(AbsolQualiTrain), Nn(IncreAbsolTrain) * (100 - Nn(AbsolQualiTrain)) / 100)
            Else
                IncreAbsolTrain = Min(Nn(AbsolQualiTrain) - 45, Nn(IncreAbsolTrain) * (Nn(AbsolQualiTrain) - 45) / 100)
            End If

            SaveVariableValue(iIDVariabile, 0, 0, 0, IncreAbsolTrain)

        Catch ex As Exception
            g_MessaggioErrore &= "IncreAbsolTrain_619 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Media sul livello di servizio al cli.
    ''' </summary>
    Private Sub AveraTrainLevel_620()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "AveraTrainLevel"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sCumulTrainLevelValue As String
        Dim sIncreTrainCumulValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sCumulTrainLevelValue = GetVariableState("CumulTrainLevel", iIDPlayer, "", 0, 0)
                    sIncreTrainCumulValue = GetVariableDefineValue("IncreTrainCumul", iIDPlayer, 0, 0)

                    sValore = Nn(sValore) + Nn(sCumulTrainLevelValue) + Nn(sIncreTrainCumulValue)
                Next

                SaveVariableValue(iIDVariabile, 0, 0, 0, sValore)
            End If
        Catch ex As Exception
            g_MessaggioErrore &= "AveraTrainLevel_620 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

#End Region

#Region "141 - 160 FUNZIONI DI CALCOLO"

    ''' <summary>
    ''' Investimento medio mkt. servizio cliente
    ''' </summary>
    Private Sub TotalExpenTrain_701()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TotalExpenTrain"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sTrainServiAllowValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sTrainServiAllowValue = GetVariableDefineValue("TrainServiAllow", iIDPlayer, 0, 0)

                    sValore = Nn(sValore) + Nn(sTrainServiAllowValue)
                Next

                SaveVariableValue(iIDVariabile, 0, 0, 0, sValore)
            End If
        Catch ex As Exception
            g_MessaggioErrore &= "TotalExpenTrain_701 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Pubblicità concessa
    ''' </summary>
    Private Sub AdverAllow_702()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "AdverAllow"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sAdverRequiValue As String
        Dim sPerceFirstRequiValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sAdverRequiValue = GetVariableDecisionPlayer(m_IDGame, "AdverRequi", iIDPlayer, 0)
                    sPerceFirstRequiValue = GetVariableDefineValue("PerceFirstRequi", iIDPlayer, 0, 0)

                    sValore = Max(0, Nn(sAdverRequiValue) * Nn(sPerceFirstRequiValue) / 100)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "AdverAllow_702 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Investimento annuale in pubblicità
    ''' </summary>
    Private Sub AdverYearAllow_703()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "AdverYearAllow"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sAdverAllowValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sAdverAllowValue = GetVariableDefineValue("AdverAllow", iIDPlayer, 0, 0)

                    sValore = Nn(sAdverAllowValue) * (m_LunghezzaPeriodo / 12)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "AdverYearAllow_703 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Tabella sulle spese in pubblicità
    ''' </summary>
    Private Sub TableAdverPermit_704()
        Dim iIDVariabile As Integer
        Dim dValueCalc As String = ""
        Dim sNomeVariabileCalc As String = "TableAdverPermit"
        Dim sNomeVariabileEntrata As String = "AdverYearAllow"
        Dim iIDPlayer As Integer = 0

        Try
            ' Ciclo sui players presenti nel game
            If m_DataTablePlayers.Rows.Count > 0 Then
                ' Controllo la presenza della variabile da calcolare e mi faccio ritornare l'id 
                iIDVariabile = CheckVariableExists(sNomeVariabileCalc, m_IDGame)
                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))

                    dValueCalc = GetVariableDefineValue(sNomeVariabileEntrata, iIDPlayer, 0, 0)
                    dValueCalc = CalculateInterpolation(sNomeVariabileCalc, dValueCalc)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, dValueCalc)
                Next
            End If

        Catch ex As Exception
            g_MessaggioErrore &= "TableAdverPermit_704 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Incremento spese medie in pubblicità
    ''' </summary>
    Private Sub IncreAveraAdver_705()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "IncreAveraAdver"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sAdverAllowValue As String
        Dim sAveraAdverInvesValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sAdverAllowValue = GetVariableDefineValue("AdverAllow", iIDPlayer, 0, 0)
                    sAveraAdverInvesValue = GetVariableState("AveraAdverInves", iIDPlayer, "", 0, 0)

                    sValore = (Nn(sAdverAllowValue) - Nn(sAveraAdverInvesValue)) / m_CurrentStep

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "IncreAveraAdver_705 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Incremento livello medio pubblicitario
    ''' </summary>
    Private Sub IncreAdverCumul_706()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "IncreAdverCumul"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sTableAdverPermitValue As String
        Dim sBossAdverPermitValue As String
        Dim sTableAveraAdverValue As String
        Dim sBossAdverAveraValue As String

        Dim sAdverAllowValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)



                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = "0"

                    sAdverAllowValue = GetVariableDefineValue("AdverAllow", iIDPlayer, 0, 0)

                    sTableAdverPermitValue = GetVariableDefineValue("TableAdverPermit", iIDPlayer, 0, 0)
                    sBossAdverPermitValue = GetVariableDeveloper(m_IDGame, "BossAdverPermit", iIDPlayer, 0)
                    sTableAveraAdverValue = GetVariableDefineValue("TableAveraAdver", iIDPlayer, 0, 0)
                    sBossAdverAveraValue = GetVariableDeveloper(m_IDGame, "BossAdverAvera", iIDPlayer, 0)

                    If Nn(sAdverAllowValue) > 0 Then
                        sValore = (Nn(sTableAdverPermitValue) * (1 + Nn(sBossAdverPermitValue) / 10) + Nn(sTableAveraAdverValue) * (1 + Nn(sBossAdverAveraValue) / 15))
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "IncreAdverCumul_706 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try

    End Sub

    ''' <summary>
    ''' Incremento delle spese medie mkt publicitarie
    ''' </summary>
    Private Sub MarkeAveraAdver_707()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "MarkeAveraAdver"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = 0

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = Nn(sValore) + Nn(GetVariableDefineValue("IncreAdverCumul", iIDPlayer, 0, 0))
                Next

                sValore = Nn(sValore) / Max(1, m_DataTablePlayers.Rows.Count)

                SaveVariableValue(iIDVariabile, 0, 0, 0, sValore)
            End If
        Catch ex As Exception
            g_MessaggioErrore &= "MarkeAveraAdver_707 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Incremento inv.medio di mkt in pubblicitario
    ''' </summary>
    Private Sub IncreAbsolAdver_708()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "IncreAbsolAdver"

        Dim sValore As String

        Dim sMarkeAveraAdverValue As String
        Dim sVisibMessaAdverValue As String
        Dim sAbsolAdverMarkeValue As String

        Try
            iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

            sMarkeAveraAdverValue = GetVariableDefineValue("MarkeAveraAdver", 0, 0, 0)
            sVisibMessaAdverValue = GetVariableDeveloper(m_IDGame, "VisibMessaAdver", 0, 0)
            sAbsolAdverMarkeValue = GetVariableState("AbsolAdverMarke", 0, "", 0, 0)

            sValore = Min(20, Nn(sMarkeAveraAdverValue) - Nn(sVisibMessaAdverValue))
            If Nn(sMarkeAveraAdverValue) > Nn(sVisibMessaAdverValue) Then
                sValore = Min(100 - Nn(sAbsolAdverMarkeValue), Nn(sValore) * (100 - Nn(sAbsolAdverMarkeValue)) / 100)
            Else
                sValore = Min(Nn(sAbsolAdverMarkeValue) - 45, Nn(sValore) * (Nn(sAbsolAdverMarkeValue) - 45) / 100)
            End If

            SaveVariableValue(iIDVariabile, 0, 0, 0, sValore)

        Catch ex As Exception
            g_MessaggioErrore &= "IncreAbsolAdver_708 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Varianza sul livello pubblicitario
    ''' </summary>
    Private Sub AveraAdverLevel_709()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "AveraAdverLevel"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sCumulAdverLevelValue As String
        Dim sIncreAdverCumulValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = 0

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sCumulAdverLevelValue = GetVariableState("CumulAdverLevel", iIDPlayer, "", 0, 0)
                    sIncreAdverCumulValue = GetVariableDefineValue("IncreAdverCumul", iIDPlayer, 0, 0)

                    sValore = Nn(sValore) + Nn(sCumulAdverLevelValue) + Nn(sIncreAdverCumulValue)
                Next

                SaveVariableValue(iIDVariabile, 0, 0, 0, sValore)
            End If
        Catch ex As Exception
            g_MessaggioErrore &= "AveraAdverLevel_709 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Spese totali in pubblicità
    ''' </summary>
    Private Sub TotalExpenAdver_710()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TotalExpenAdver"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sAdverAllowValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = 0

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sAdverAllowValue = GetVariableDefineValue("AdverAllow", iIDPlayer, 0, 0)

                    sValore = Nn(sValore) + Nn(sAdverAllowValue)
                Next

                SaveVariableValue(iIDVariabile, 0, 0, 0, sValore)
            End If
        Catch ex As Exception
            g_MessaggioErrore &= "TotalExpenAdver_710 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Investimenti ecologici concessi
    ''' </summary>
    Private Sub GreenAllow_711()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "GreenAllow"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sGreenRequiValue As String
        Dim sPerceFirstRequiValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = 0

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sGreenRequiValue = GetVariableDecisionPlayer(m_IDGame, "GreenRequi", iIDPlayer, 0)
                    sPerceFirstRequiValue = GetVariableDefineValue("PerceFirstRequi", iIDPlayer, 0, 0)

                    sValore = Max(0, Nn(sGreenRequiValue) * Nn(sPerceFirstRequiValue) / 100)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "GreenAllow_711 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Costi di marketing
    ''' </summary>
    Private Sub InterMarkeCosts_712()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "InterMarkeCosts"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sNomeItem As String

        Dim sNomeVariabileLista As String

        Dim sExtraAllowValue As String
        Dim sExtraProduCostValue As String
        Dim sDiscoTermiMeseValue As String
        Dim sDiritScontPagamValue As String
        Dim sLagPaymeExtraValue As String
        Dim sForwaPaymeExtraValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = 0

                sDiscoTermiMeseValue = GetVariableBoss("DiscoTermiMese", "")
                sDiritScontPagamValue = GetVariableBoss("DiritScontPagam", "")
                sLagPaymeExtraValue = GetVariableBoss("LagPaymeExtra", "")

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = 0

                    sForwaPaymeExtraValue = GetVariableDecisionPlayer(m_IDGame, "ForwaPaymeExtra", iIDPlayer, 0)
                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sNomeItem = Nz(oRowItem("VariableName"))

                        sNomeVariabileLista = "ExtraProduCost_" & sNomeItem
                        sExtraProduCostValue = GetVariableBoss("ExtraProduCost", sNomeVariabileLista)

                        sExtraAllowValue = GetVariableDefineValue("ExtraAllow", iIDPlayer, iIDItem, 0)

                        sValore = (Nn(sExtraAllowValue) * Nn(sExtraProduCostValue)) * (1 - Nn(sDiscoTermiMeseValue) / 100 * Nn(sDiritScontPagamValue) * Min(m_LunghezzaPeriodo, Max(m_LunghezzaPeriodo - Nn(sLagPaymeExtraValue), Nn(sForwaPaymeExtraValue))))
                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next

                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "InterMarkeCosts_712 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    Private Sub CostPurchRaw_713()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CostPurchRaw"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sNomeItem As String

        Dim sNomeVariabileLista As String

        Dim sPurchAllowEuropValue As String
        Dim sRawEuropCostValue As String
        Dim sRicarTermiMeseValue As String
        Dim sDiritRitarPagamValue As String
        Dim sLagPaymeEuropValue As String
        Dim sDelayPaymeEuropValue As String
        Dim sPurchAllowNICValue As String
        Dim sRawNICCostValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = 0

                sRicarTermiMeseValue = GetVariableBoss("RicarTermiMese", "")
                sDiritRitarPagamValue = GetVariableBoss("DiritRitarPagam", "")
                sLagPaymeEuropValue = GetVariableBoss("LagPaymeEurop", "")

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = 0

                    sDelayPaymeEuropValue = GetVariableDecisionPlayer(m_IDGame, "DelayPaymeEurop", iIDPlayer, 0)

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sNomeItem = Nz(oRowItem("VariableName"))

                        sPurchAllowEuropValue = GetVariableDefineValue("PurchAllowEurop", iIDPlayer, iIDItem, 0)
                        sNomeVariabileLista = "RawEuropCost_" & sNomeItem
                        sRawEuropCostValue = GetVariableBoss("RawEuropCost", sNomeVariabileLista)
                        sPurchAllowNICValue = GetVariableDefineValue("PurchAllowNIC", iIDPlayer, iIDItem, 0)
                        sNomeVariabileLista = "RawNICCost_" & sNomeItem
                        sRawNICCostValue = GetVariableBoss("RawNICCost", sNomeVariabileLista)
                        sValore = Nn(sPurchAllowEuropValue) * Nn(sRawEuropCostValue) * (1 + Nn(sRicarTermiMeseValue) / 100 * Nn(sDiritRitarPagamValue) * Min(m_LunghezzaPeriodo - Nn(sLagPaymeEuropValue), Nn(sDelayPaymeEuropValue))) + Nn(sPurchAllowNICValue) * Nn(sRawNICCostValue)
                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next

                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "CostPurchRaw_713 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Costo delle materie prime - Locali
    ''' </summary>
    Private Sub CostPurchEurop_714()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CostPurchEurop"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sNomeItem As String

        Dim sNomeVariabileLista As String

        Dim sPurchAllowEuropValue As String
        Dim sRawEuropCostValue As String
        Dim sRicarTermiMeseValue As String
        Dim sDiritRitarPagamValue As String
        Dim sLagPaymeEuropValue As String
        Dim sDelayPaymeEuropValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = 0

                sRicarTermiMeseValue = GetVariableBoss("RicarTermiMese", "")
                sDiritRitarPagamValue = GetVariableBoss("DiritRitarPagam", "")
                sLagPaymeEuropValue = GetVariableBoss("LagPaymeEurop", "")

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = 0

                    sDelayPaymeEuropValue = GetVariableDecisionPlayer(m_IDGame, "DelayPaymeEurop", iIDPlayer, 0)

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sNomeItem = Nz(oRowItem("VariableName"))

                        sPurchAllowEuropValue = GetVariableDefineValue("PurchAllowEurop", iIDPlayer, iIDItem, 0)
                        sNomeVariabileLista = "RawEuropCost_" & sNomeItem
                        sRawEuropCostValue = GetVariableBoss("RawEuropCost", sNomeVariabileLista)
                        sValore = Nn(sValore) + (Nn(sPurchAllowEuropValue) * Nn(sRawEuropCostValue) * (1 + Nn(sRicarTermiMeseValue) / 100 * Nn(sDiritRitarPagamValue) * Min(m_LunghezzaPeriodo - Nn(sLagPaymeEuropValue), Nn(sDelayPaymeEuropValue))))
                    Next

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "CostPurchEurop_714 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Costo delle materie prime - NIC
    ''' </summary>
    Private Sub CostPurchNIC_715()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CostPurchNIC"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sNomeItem As String

        Dim sNomeVariabileLista As String
        Dim sPurchAllowNICValue As String
        Dim sRawNICCostValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = 0

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = 0

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sNomeItem = Nz(oRowItem("VariableName"))

                        sPurchAllowNICValue = GetVariableDefineValue("PurchAllowNIC", iIDPlayer, iIDItem, 0)
                        sNomeVariabileLista = "RawNICCost_" & sNomeItem
                        sRawNICCostValue = GetVariableBoss("RawNICCost", sNomeVariabileLista)
                        sValore = Nn(sValore) + Nn(sPurchAllowNICValue) * Nn(sRawNICCostValue)
                    Next

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "CostPurchNIC_715 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Costo delle materie prime - Totale
    ''' </summary>
    Private Sub CostPurchTotal_716()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CostPurchTotal"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sNomeItem As String

        Dim sCostPurchRawValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = 0

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = 0

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sNomeItem = Nz(oRowItem("VariableName"))

                        sCostPurchRawValue = GetVariableDefineValue("CostPurchRaw", iIDPlayer, iIDItem, 0)
                        sValore = Nn(sValore) + Nn(sCostPurchRawValue)
                    Next

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "CostPurchTotal_716 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Costi di tecnologia
    ''' </summary>
    Private Sub TechnCostsPerio_717()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TechnCostsPerio"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sTechnAutomAllowValue As String
        Dim sDiscoTermiMeseValue As String
        Dim sDiritScontPagamValue As String
        Dim sLagPaymeAutomValue As String
        Dim sForwaPaymeAutomValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = 0

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = 0

                    sTechnAutomAllowValue = GetVariableDefineValue("TechnAutomAllow", iIDPlayer, 0, 0)
                    sDiscoTermiMeseValue = GetVariableBoss("DiscoTermiMese", "")
                    sDiritScontPagamValue = GetVariableBoss("DiritScontPagam", "")
                    sLagPaymeAutomValue = GetVariableBoss("LagPaymeAutom", "")
                    sForwaPaymeAutomValue = GetVariableDecisionPlayer(m_IDGame, "ForwaPaymeAutom", iIDPlayer, 0)
                    sValore = Nn(sTechnAutomAllowValue) * (1 - Nn(sDiscoTermiMeseValue) / 100 * Nn(sDiritScontPagamValue) * Min(m_LunghezzaPeriodo, Max(m_LunghezzaPeriodo - Nn(sLagPaymeAutomValue), Nn(sForwaPaymeAutomValue))))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "TechnCostsPerio_717 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Interessi sui prestiti
    ''' </summary>
    Private Sub TotalNegatInter_718()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TotalNegatInter"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sLoansLevelValue As String
        Dim sTaxRateCrediValue As String
        Dim sGreenAllowValue As String
        Dim sSuperExtraFidoValue As String
        Dim sTaxCrediExtraValue As String
        Dim sNomeVariabileLista As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = 0

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = 0

                    sLoansLevelValue = GetVariableState("LoansLevel", iIDPlayer, "", 0, 0)
                    sNomeVariabileLista = "TaxRateCredi_" & sTeamName
                    sTaxRateCrediValue = GetVariableBoss("TaxRateCredi", sNomeVariabileLista)
                    sGreenAllowValue = GetVariableDefineValue("GreenAllow", iIDPlayer, 0, 0)
                    sSuperExtraFidoValue = GetVariableDecisionPlayer(m_IDGame, "SuperExtraFido", iIDPlayer, 0)
                    sTaxCrediExtraValue = GetVariableBoss("TaxCrediExtra", "")

                    sValore = (Nn(sLoansLevelValue) * Max(1.0, Nn(sTaxRateCrediValue) - Min(0.5 * Nn(sTaxRateCrediValue), Nn(sGreenAllowValue) * 0.000002)) + Nn(sSuperExtraFidoValue) * Nn(sTaxRateCrediValue) * (Nn(sTaxCrediExtraValue) / 10)) / 100
                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "TotalNegatInter_718 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Affitti
    ''' </summary>
    Private Sub TotalCentrCosts_719()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TotalCentrCosts"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sRentPerioCentrValue As String
        Dim sCentrStoreAllowValue As String
        Dim sTotalCentrStoreValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = 0

                sRentPerioCentrValue = GetVariableDefineValue("RentPerioCentr", 0, 0, 0)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = 0
                    sCentrStoreAllowValue = GetVariableDefineValue("CentrStoreAllow", iIDPlayer, 0, 0)
                    sTotalCentrStoreValue = GetVariableState("TotalCentrStore", iIDPlayer, "", 0, 0)

                    sValore = Nn(sRentPerioCentrValue) * (Nn(sCentrStoreAllowValue) + Nn(sTotalCentrStoreValue))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "TotalCentrCosts_719 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Affitti dei negozi periferici
    ''' </summary>
    Private Sub PerifStoreCosts_720()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "PerifStoreCosts"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sRentPerioPerifValue As String
        Dim sPerifStoreAllowValue As String
        Dim sTotalPerifStoreValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = 0

                sRentPerioPerifValue = GetVariableDefineValue("RentPerioPerif", 0, 0, 0)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = 0
                    sPerifStoreAllowValue = GetVariableDefineValue("PerifStoreAllow", iIDPlayer, 0, 0)
                    sTotalPerifStoreValue = GetVariableState("TotalPerifStore", iIDPlayer, "", 0, 0)

                    sValore = Nn(sRentPerioPerifValue) * (Nn(sPerifStoreAllowValue) + Nn(sTotalPerifStoreValue))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "PerifStoreCosts_720 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

#End Region

#Region "161 - 180 FUNZIONI DI CALCOLO"

    ''' <summary>
    ''' Affitti totali
    ''' </summary>
    Private Sub TotalCostsOutle_801()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TotalCostsOutle"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sTotalCentrCostsValue As String
        Dim sPerifStoreCostsValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    sValore = "0"

                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sTotalCentrCostsValue = GetVariableDefineValue("TotalCentrCosts", iIDPlayer, 0, 0)
                    sPerifStoreCostsValue = GetVariableDefineValue("PerifStoreCosts", iIDPlayer, 0, 0)

                    sValore = Nn(sTotalCentrCostsValue) + Nn(sPerifStoreCostsValue)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "TotalCostsOutle_801 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Costo dello staff
    ''' </summary>
    Private Sub CostsPersoStaff_802()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CostsPersoStaff"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sTotalStaffPersoValue As String
        Dim sNewStaffAllowValue As String
        Dim sNewCostStaffValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sNewCostStaffValue = GetVariableDefineValue("NewCostStaff", 0, 0, 0)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    sValore = "0"

                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sTotalStaffPersoValue = GetVariableState("TotalStaffPerso", iIDPlayer, "", 0, 0)
                    sNewStaffAllowValue = GetVariableDefineValue("NewStaffAllow", iIDPlayer, 0, 0)

                    sValore = (Nn(sTotalStaffPersoValue) + Nn(sNewStaffAllowValue)) * Nn(sNewCostStaffValue)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "CostsPersoStaff_802 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Costo del personale di vendita
    ''' </summary>
    Private Sub CostsPersoPoint_803()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CostsPersoPoint"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sTotalPointPersoValue As String
        Dim sNewPointAllowValue As String
        Dim sNewCostPointValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sNewCostPointValue = GetVariableDefineValue("NewCostPoint", 0, 0, 0)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    sValore = "0"

                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sTotalPointPersoValue = GetVariableState("TotalPointPerso", iIDPlayer, "", 0, 0)
                    sNewPointAllowValue = GetVariableDefineValue("NewPointAllow", iIDPlayer, 0, 0)

                    sValore = (Nn(sTotalPointPersoValue) + Nn(sNewPointAllowValue)) * Nn(sNewCostPointValue)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "CostsPersoPoint_803 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Salari
    ''' </summary>
    Private Sub TotalCostsPerso_804()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TotalCostsPerso"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sCostsPersoPointValue As String
        Dim sCostsPersoStaffValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    sValore = "0"

                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sCostsPersoPointValue = GetVariableDefineValue("CostsPersoPoint", iIDPlayer, 0, 0)
                    sCostsPersoStaffValue = GetVariableDefineValue("CostsPersoStaff", iIDPlayer, 0, 0)

                    sValore = Nn(sCostsPersoStaffValue) + Nn(sCostsPersoPointValue)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "TotalCostsPerso_804 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Spese promozionali
    ''' </summary>
    Private Sub PromoInvesPerio_805()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "PromoInvesPerio"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As String
        Dim sItemName As String

        Dim sMarkeExpenAllowValue As String
        Dim sRicarTermiMeseValue As String
        Dim sDiritRitarPagamValue As String
        Dim sLagPaymePromoValue As String
        Dim sDelayPaymePromoValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sRicarTermiMeseValue = GetVariableBoss("RicarTermiMese", "")
                sDiritRitarPagamValue = GetVariableBoss("DiritRitarPagam", "")
                sLagPaymePromoValue = GetVariableBoss("LagPaymePromo", "")

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    sValore = "0"

                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sDelayPaymePromoValue = GetVariableDecisionPlayer(m_IDGame, "DelayPaymePromo", iIDPlayer, 0)
                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        sMarkeExpenAllowValue = GetVariableDefineValue("MarkeExpenAllow", iIDPlayer, iIDItem, 0)
                        sValore = Nn(sValore) + Nn(sMarkeExpenAllowValue) * (1 + Nn(sRicarTermiMeseValue) / 100 * Nn(sDiritRitarPagamValue) * Min(m_LunghezzaPeriodo - Nn(sLagPaymePromoValue), Nn(sDelayPaymePromoValue)))
                    Next

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "PromoInvesPerio_805 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Costi addizionali (advert,cus.service)
    ''' </summary>
    Private Sub AdditCostsPerio_806()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "AdditCostsPerio"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sTrainServiAllowValue As String
        Dim sAdverAllowValue As String
        Dim sDiscoTermiMeseValue As String
        Dim sDiritScontPagamValue As String
        Dim sLagPaymeMktgValue As String
        Dim sForwaPaymeMktgValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sDiscoTermiMeseValue = GetVariableBoss("DiscoTermiMese", "")
                sDiritScontPagamValue = GetVariableBoss("DiritScontPagam", "")
                sLagPaymeMktgValue = GetVariableBoss("LagPaymeMktg", "")

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    sValore = "0"

                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sTrainServiAllowValue = GetVariableDefineValue("TrainServiAllow", iIDPlayer, 0, 0)
                    sAdverAllowValue = GetVariableDefineValue("AdverAllow", iIDPlayer, 0, 0)

                    sForwaPaymeMktgValue = GetVariableDecisionPlayer(m_IDGame, "ForwaPaymeMktg", iIDPlayer, 0)

                    sValore = (Nn(sTrainServiAllowValue) + Nn(sAdverAllowValue))
                    ' Parte commentata come da richiesta di Fulvio con mail del 10/10/2017
                    '* (1 - Nn(sDiscoTermiMeseValue) / 100 * Nn(sDiritScontPagamValue) * Min(m_LunghezzaPeriodo, Max(m_LunghezzaPeriodo - Nn(sLagPaymeMktgValue), Nn(sForwaPaymeMktgValue))))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "AdditCostsPerio_806 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Costi di acquisto
    ''' </summary>
    Private Sub CumulCostPurch_807()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CumulCostPurch"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sVariaCostPurchValue As String
        Dim sCostPurchTotalValue As String
        Dim sDischCostPurchValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    sValore = "0"

                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sVariaCostPurchValue = GetVariableState("VariaCostPurch", iIDPlayer, "", 0, 0)
                    sCostPurchTotalValue = GetVariableDefineValue("CostPurchTotal", iIDPlayer, 0, 0)
                    sDischCostPurchValue = GetVariableDefineValue("DischCostPurch", iIDPlayer, 0, 0)

                    sValore = Nn(sVariaCostPurchValue) + Nn(sCostPurchTotalValue) - Nn(sDischCostPurchValue)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "CumulCostPurch_807 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Affitti
    ''' </summary>
    Private Sub CumulOutleCosts_808()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CumulOutleCosts"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sVariaOutleCostsValue As String
        Dim sTotalCostsOutleValue As String
        Dim sDischOutleCostsValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    sValore = "0"

                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sVariaOutleCostsValue = GetVariableState("VariaOutleCosts", iIDPlayer, "", 0, 0)
                    sTotalCostsOutleValue = GetVariableDefineValue("TotalCostsOutle", iIDPlayer, 0, 0)
                    sDischOutleCostsValue = GetVariableDefineValue("DischOutleCosts", iIDPlayer, 0, 0)

                    sValore = Nn(sVariaOutleCostsValue) + Nn(sTotalCostsOutleValue) - Nn(sDischOutleCostsValue)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "CumulOutleCosts_808 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Salari
    ''' </summary>
    Private Sub CumulPersoCosts_809()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CumulPersoCosts"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sVariaPersoCostsValue As String
        Dim sTotalCostsPersoValue As String
        Dim sDischPersoCostsValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    sValore = "0"

                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sVariaPersoCostsValue = GetVariableState("VariaPersoCosts", iIDPlayer, "", 0, 0)
                    sTotalCostsPersoValue = GetVariableDefineValue("TotalCostsPerso", iIDPlayer, 0, 0)
                    sDischPersoCostsValue = GetVariableDefineValue("DischPersoCosts", iIDPlayer, 0, 0)

                    sValore = Nn(sVariaPersoCostsValue) + Nn(sTotalCostsPersoValue) - Nn(sDischPersoCostsValue)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "CumulPersoCosts_809 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Altre spese
    ''' </summary>
    Private Sub CumulExpenCosts_810()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CumulExpenCosts"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sVariaExpenCostsValue As String
        Dim sAdditCostsPerioValue As String
        'Dim sTechnCostsPerioValue As String
        Dim sGreenAllowValue As String
        Dim sAddesTotalAllowValue As String
        Dim sPromoInvesPerioValue As String
        Dim sDischExpenCostsValule As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    sValore = "0"

                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sVariaExpenCostsValue = GetVariableState("VariaExpenCosts", iIDPlayer, "", 0, 0)
                    sAdditCostsPerioValue = GetVariableDefineValue("AdditCostsPerio", iIDPlayer, 0, 0)
                    'sTechnCostsPerioValue = GetVariableDefineValue("TechnCostsPerio", iIDPlayer, 0, 0)
                    sGreenAllowValue = GetVariableDefineValue("GreenAllow", iIDPlayer, 0, 0)
                    sAddesTotalAllowValue = GetVariableDefineValue("AddesTotalAllow", iIDPlayer, 0, 0)
                    sPromoInvesPerioValue = GetVariableDefineValue("PromoInvesPerio", iIDPlayer, 0, 0)
                    sDischExpenCostsValule = GetVariableDefineValue("DischExpenCosts", iIDPlayer, 0, 0)

                    sValore = Nn(sVariaExpenCostsValue) + Nn(sAdditCostsPerioValue) + Nn(sGreenAllowValue) _
                            + Nn(sAddesTotalAllowValue) + Nn(sPromoInvesPerioValue) - Nn(sDischExpenCostsValule)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "CumulExpenCosts_810 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Interessi sui prestiti
    ''' </summary>
    Private Sub CumulNegatInter_811()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CumulNegatInter"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sVariaNegatInterValue As String
        Dim sTotalNegatInterValue As String
        Dim sDischNegatInterValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    sValore = "0"

                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sVariaNegatInterValue = GetVariableState("VariaNegatInter", iIDPlayer, "", 0, 0)
                    sTotalNegatInterValue = GetVariableDefineValue("TotalNegatInter", iIDPlayer, 0, 0)
                    sDischNegatInterValue = GetVariableDefineValue("DischNegatInter", iIDPlayer, 0, 0)

                    sValore = Nn(sVariaNegatInterValue) + Nn(sTotalNegatInterValue) - Nn(sDischNegatInterValue)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "CumulNegatInter_811 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Macchinari in Leasing (/attuale)
    ''' </summary>
    Private Sub TotalLease_812()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TotalLease"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sLeasePerAgeValue As String
        Dim sIncreAgeLeaseValue As String

        Dim sNomeVariabileLista As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    sValore = "0"

                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    ' Con un indice eseguo il ciclo su tutti i veicoli, escludendio l'ultimo
                    For iIndexAge As Integer = 1 To m_DataTableAge.Rows.Count
                        sNomeVariabileLista = "var_" & sTeamName & "_LeasePerAge_" & iIndexAge
                        sLeasePerAgeValue = GetVariableState("LeasePerAge", iIDPlayer, sNomeVariabileLista, 0, iIndexAge)
                        sIncreAgeLeaseValue = GetVariableDefineValue("IncreAgeLease", iIDPlayer, 0, iIndexAge)

                        sValore = Nn(sLeasePerAgeValue) + Nn(sIncreAgeLeaseValue)

                        SaveVariableValue(iIDVariabile, iIDPlayer, 0, iIndexAge, sValore)
                    Next
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "TotalLease_812 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Leasing totali (attuali)
    ''' </summary>
    Private Sub ActuaLeaseMachi_813()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "ActuaLeaseMachi"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sTotalLeaseValue As String
        Dim sTotalLeaseValue1 As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    sValore = "0"

                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sTotalLeaseValue = GetVariableDefineValue("TotalLease", iIDPlayer, 0, 1)
                    sTotalLeaseValue1 = GetVariableDefineValue("TotalLease", iIDPlayer, 0, 2)

                    sValore = Nn(sTotalLeaseValue) + Nn(sTotalLeaseValue1)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "ActuaLeaseMachi_813 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Totale delle macchine in leasing
    ''' </summary>
    Private Sub TotalMachiLease_814()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TotalMachiLease"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sTotalLeaseValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    sValore = "0"

                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    For iIndexAge As Integer = 1 To m_DataTableAge.Rows.Count

                        sTotalLeaseValue = GetVariableDefineValue("TotalLease", iIDPlayer, 0, iIndexAge)

                        sValore = Nn(sValore) + Nn(sTotalLeaseValue)
                    Next

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "TotalMachiLease_814 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Macchinari propri totali (/attuale)
    ''' </summary>
    Private Sub PlantTousePerio_815()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "PlantTousePerio"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sTotalPlantValue As String
        Dim sTotalLeaseValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    sValore = "0"

                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    ' Con un indice eseguo il ciclo su tutti i veicoli, escludendio l'ultimo
                    For iIndexAge As Integer = 1 To m_DataTableAge.Rows.Count
                        sTotalPlantValue = GetVariableDefineValue("TotalPlant", iIDPlayer, 0, iIndexAge)
                        sTotalLeaseValue = GetVariableDefineValue("TotalLease", iIDPlayer, 0, iIndexAge)

                        sValore = Nn(sValore) + Nn(sTotalPlantValue) + Nn(sTotalLeaseValue)
                    Next

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "PlantTousePerio_815 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Capacità produttiva (/attuale)
    ''' </summary>
    Private Sub TotalCapacProdu_816()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TotalCapacProdu"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sTotalPlantValue As String
        Dim sTotalLeaseValue As String
        Dim dValueMoltiplicatore As Integer
        Dim iContaMezzi As Integer = 0

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    sValore = "0"

                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    iContaMezzi = m_DataTableAge.Rows.Count
                    ' Con un indice eseguo il ciclo su tutti i veicoli, escludendio l'ultimo
                    For iIndexAge As Integer = 1 To m_DataTableAge.Rows.Count

                        dValueMoltiplicatore = iContaMezzi / 2 * 1000

                        sTotalPlantValue = GetVariableDefineValue("TotalPlant", iIDPlayer, 0, iIndexAge)
                        sTotalLeaseValue = GetVariableDefineValue("TotalLease", iIDPlayer, 0, iIndexAge)

                        sValore = Nn(sValore) + (Nn(sTotalLeaseValue) + Nn(sTotalPlantValue)) * dValueMoltiplicatore * (m_LunghezzaPeriodo / 12)

                        iContaMezzi -= 1
                    Next

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "TotalCapacProdu_816 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Capacità produttiva massima
    ''' </summary>
    Private Sub NextCapacProdu_817()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "NextCapacProdu"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sPlantAllowValue As String
        Dim sLeaseAllowValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sPlantAllowValue = GetVariableDefineValue("PlantAllow", iIDPlayer, 0, 0)
                    sLeaseAllowValue = GetVariableDefineValue("LeaseAllow", iIDPlayer, 0, 0)

                    ' Con un indice eseguo il ciclo su tutti i veicoli, escludendio l'ultimo

                    sValore = "0"

                    sValore = Nn(sPlantAllowValue) * 12000 * m_LunghezzaPeriodo / 12 +
   (Nn(sLeaseAllowValue) + Nn(GetVariableDefineValue("TotalPlant", iIDPlayer, 0, 1))) * 11000 * (m_LunghezzaPeriodo _
/ 12) +
   (Nn(GetVariableDefineValue("TotalLEase", iIDPlayer, 0, 3)) + Nn(GetVariableDefineValue("TotalPlant", iIDPlayer, 0, 2))) * 10000 * (m_LunghezzaPeriodo _
/ 12) +
   (Nn(GetVariableDefineValue("TotalLEase", iIDPlayer, 0, 4)) + Nn(GetVariableDefineValue("TotalPlant", iIDPlayer, 0, 3))) * 9000 * (m_LunghezzaPeriodo _
/ 12) +
   (Nn(GetVariableDefineValue("TotalLEase", iIDPlayer, 0, 5)) + Nn(GetVariableDefineValue("TotalPlant", iIDPlayer, 0, 4))) * 8000 * (m_LunghezzaPeriodo _
/ 12) +
   (Nn(GetVariableDefineValue("TotalLEase", iIDPlayer, 0, 6)) + Nn(GetVariableDefineValue("TotalPlant", iIDPlayer, 0, 5))) * 7000 * (m_LunghezzaPeriodo _
/ 12) +
   (Nn(GetVariableDefineValue("TotalLEase", iIDPlayer, 0, 7)) + Nn(GetVariableDefineValue("TotalPlant", iIDPlayer, 0, 6))) * 6000 * (m_LunghezzaPeriodo _
/ 12) +
   (Nn(GetVariableDefineValue("TotalLEase", iIDPlayer, 0, 8)) + Nn(GetVariableDefineValue("TotalPlant", iIDPlayer, 0, 7))) * 5000 * (m_LunghezzaPeriodo _
/ 12) +
   (Nn(GetVariableDefineValue("TotalLEase", iIDPlayer, 0, 9)) + Nn(GetVariableDefineValue("TotalPlant", iIDPlayer, 0, 8))) * 4000 * (m_LunghezzaPeriodo _
/ 12) +
   (Nn(GetVariableDefineValue("TotalLEase", iIDPlayer, 0, 10)) + Nn(GetVariableDefineValue("TotalPlant", iIDPlayer, 0, 9))) * 3000 * (m_LunghezzaPeriodo _
/ 12) +
   (Nn(GetVariableDefineValue("TotalPlant", iIDPlayer, 0, 11))) * 2000 * (m_LunghezzaPeriodo / 12) +
   (Nn(GetVariableDefineValue("TotalPlant", iIDPlayer, 0, 12))) * 1000 * (m_LunghezzaPeriodo / 12)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "NextCapacProdu_817 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Produzione del periodo
    ''' </summary>
    Private Sub ProduReque_818()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "ProduReque"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim sTotalHoursRequiValue As String
        Dim sTotalCapacProduValue As String
        Dim sProduPossiRawValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    sValore = "0"

                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sTotalCapacProduValue = GetVariableDefineValue("TotalCapacProdu", iIDPlayer, 0, 0)

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))

                        sProduPossiRawValue = GetVariableDefineValue("ProduPossiRaw", iIDPlayer, iIDItem, 0)
                        sTotalHoursRequiValue = GetVariableDefineValue("TotalHoursRequi", iIDPlayer, 0, 0)

                        If (Nn(sTotalHoursRequiValue) = 0) Then
                            sValore = 0
                        Else
                            If (Nn(sTotalCapacProduValue) >= Nn(sTotalHoursRequiValue)) Then
                                sValore = Nn(sProduPossiRawValue)
                            Else
                                sValore = Truncate(Nn(sProduPossiRawValue) * Nn(sTotalCapacProduValue) / Nn(sTotalHoursRequiValue))
                            End If
                        End If
                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next

                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "ProduReque_818 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' % di capacità produttiva usata
    ''' </summary>
    Private Sub PerceProduUsed_819()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "PerceProduUsed"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sTotalCapacProduValue As String
        Dim sTotalHoursRequiValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    sValore = "0"

                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sTotalCapacProduValue = GetVariableDefineValue("TotalCapacProdu", iIDPlayer, 0, 0)
                    sTotalHoursRequiValue = GetVariableDefineValue("TotalHoursRequi", iIDPlayer, 0, 0)

                    If Nn(sTotalCapacProduValue) <> 0 Then
                        sValore = Min(1, Nn(sTotalHoursRequiValue) / Nn(sTotalCapacProduValue)) * 100
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "PerceProduUsed_819 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Media Automation Technology level
    ''' </summary>
    Private Sub AveraTechnLevel_820()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "AveraTechnLevel"

        Dim AveraTechnLevel As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sCumulTechnLevelValue As String
        Dim sIncreTechnCumulValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)
                AveraTechnLevel = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sCumulTechnLevelValue = GetVariableState("CumulTechnLevel", iIDPlayer, "", 0, 0)
                    sIncreTechnCumulValue = GetVariableDefineValue("IncreTechnCumul", iIDPlayer, 0, 0)

                    AveraTechnLevel = Nn(AveraTechnLevel) + Nn(sCumulTechnLevelValue) + Nn(sIncreTechnCumulValue)

                Next

                SaveVariableValue(iIDVariabile, 0, 0, 0, AveraTechnLevel)

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "AveraTechnLevel_820 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

#End Region

#Region "181 - 200 FUNZIONI DI CALCOLO"

    ''' <summary>
    ''' Varianza sul livello tecnologico
    ''' </summary>
    Private Sub VariaTechnQuali_901()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "VariaTechnQuali"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sCumulTechnLevelValue As String
        Dim sIncreTechnCumulValue As String
        Dim sAveraTechnLevelValue As String
        Dim iNUMPlayers As Integer = m_DataTablePlayers.Rows.Count

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sAveraTechnLevelValue = GetVariableDefineValue("AveraTechnLevel", 0, 0, 0)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sCumulTechnLevelValue = GetVariableState("CumulTechnLevel", iIDPlayer, "", 0, 0)
                    sIncreTechnCumulValue = GetVariableDefineValue("IncreTechnCumul", iIDPlayer, 0, 0)

                    sValore = Nn(sValore) + Pow((Nn(sCumulTechnLevelValue) + Nn(sIncreTechnCumulValue) - Nn(sAveraTechnLevelValue) / iNUMPlayers), 2) / iNUMPlayers

                Next
                sValore = Pow(Nn(sValore), 0.5) * 3

                SaveVariableValue(iIDVariabile, 0, 0, 0, sValore)
            End If

        Catch ex As Exception
            g_MessaggioErrore &= "VariaTechnQuali_201 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Qualità della produzione
    ''' </summary>
    Private Sub ProduQualiProce_902()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "ProduQualiProce"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sAbsolQualiProceValue As String = GetVariableState("AbsolQualiProce", 0, "", 0, 0)
        Dim dTeamMax As Double
        Dim dTeamMIN As Double
        Dim dCUMUL As Double
        Dim dUpper As Double = Nn(sAbsolQualiProceValue)
        Dim dLower As Double = Nn(sAbsolQualiProceValue)

        Dim sMarkeAveraTechnValue As String
        Dim sVariaTechnQualiValue As String

        Dim sIncreTechnCumulValue As String
        Dim sCumulTechnLevelValue As String

        Dim iNUMPlayers As Integer = m_DataTablePlayers.Rows.Count

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                sMarkeAveraTechnValue = GetVariableDefineValue("MarkeAveraTechn", 0, 0, 0)
                sVariaTechnQualiValue = GetVariableDefineValue("VariaTechnQuali", 0, 0, 0)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    dCUMUL += Nn(GetVariableState("CumulTechnLevel", iIDPlayer, "", 0, 0))
                Next

                dUpper = Min(100, Nn(sAbsolQualiProceValue) + Min(0.3, Nn(sVariaTechnQualiValue) / (Nn(sMarkeAveraTechnValue) + dCUMUL / iNUMPlayers)) * Nn(sAbsolQualiProceValue))
                dLower = Max(1, Nn(sAbsolQualiProceValue) - Min(0.3, Nn(sVariaTechnQualiValue) / (Nn(sMarkeAveraTechnValue) + dCUMUL / iNUMPlayers)) * Nn(sAbsolQualiProceValue))

                dTeamMax = 0
                dTeamMIN = 99999
                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sIncreTechnCumulValue = GetVariableDefineValue("IncreTechnCumul", iIDPlayer, 0, 0)
                    sCumulTechnLevelValue = GetVariableState("CumulTechnLevel", iIDPlayer, "", 0, 0)
                    If Nn(sIncreTechnCumulValue) + Nn(sCumulTechnLevelValue) > 0 AndAlso Nn(sIncreTechnCumulValue) + Nn(sCumulTechnLevelValue) > dTeamMax Then
                        dTeamMax = Nn(sIncreTechnCumulValue) + Nn(sCumulTechnLevelValue)
                    End If
                Next

                dTeamMIN = 99999
                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sIncreTechnCumulValue = GetVariableDefineValue("IncreTechnCumul", iIDPlayer, 0, 0)
                    sCumulTechnLevelValue = GetVariableState("CumulTechnLevel", iIDPlayer, "", 0, 0)

                    If Nn(sIncreTechnCumulValue) + Nn(sCumulTechnLevelValue) < 99999 AndAlso Nn(sIncreTechnCumulValue) + Nn(sCumulTechnLevelValue) < dTeamMIN Then
                        dTeamMIN = Nn(sIncreTechnCumulValue) + Nn(sCumulTechnLevelValue)
                    End If
                Next

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    If dTeamMax - dTeamMIN = 0 Then
                        sValore = Nn(sAbsolQualiProceValue)
                    Else
                        sIncreTechnCumulValue = GetVariableDefineValue("IncreTechnCumul", iIDPlayer, 0, 0)
                        sCumulTechnLevelValue = GetVariableState("CumulTechnLevel", iIDPlayer, "", 0, 0)

                        sValore = (dUpper - dLower) * (Nn(sIncreTechnCumulValue) + Nn(sCumulTechnLevelValue) - dTeamMIN) / (dTeamMax - dTeamMIN) + dLower
                    End If


                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next
            End If

        Catch ex As Exception
            g_MessaggioErrore &= "ProduQualiProce_902 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Spese totali in marketing e promozioni
    ''' </summary>
    Private Sub TotalMarkeAllow_903()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TotalMarkeAllow"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String
        Dim iIDItem As Integer

        Dim sMarkeExpenAllowValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    sValore = "0"

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sMarkeExpenAllowValue = GetVariableDefineValue("MarkeExpenAllow", iIDPlayer, iIDItem, 0)

                        sValore = Nn(sValore) + Nn(sMarkeExpenAllowValue)
                    Next

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next
            End If

        Catch ex As Exception
            g_MessaggioErrore &= "TotalMarkeAllow_903 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Varianza sul livello di servizio al cliente
    ''' </summary>
    Private Sub VariaTrainQuali_904()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "VariaTrainQuali"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sCumulTrainLevelValue As String
        Dim sIncreTrainCumulValue As String
        Dim sAveraTrainLevelValue As String

        Dim iNUMPlayers As Integer = m_DataTablePlayers.Rows.Count

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"
                sAveraTrainLevelValue = GetVariableDefineValue("AveraTrainLevel", 0, 0, 0)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sCumulTrainLevelValue = GetVariableState("CumulTrainLevel", iIDPlayer, "", 0, 0)
                    sIncreTrainCumulValue = GetVariableDefineValue("IncreTrainCumul", iIDPlayer, 0, 0)

                    sValore = Nn(sValore) + Pow((Nn(sCumulTrainLevelValue) + Nn(sIncreTrainCumulValue) - Nn(sAveraTrainLevelValue) / iNUMPlayers), 2) / iNUMPlayers

                Next
                sValore = Pow(Nn(sValore), 0.5) * 3

                SaveVariableValue(iIDVariabile, 0, 0, 0, sValore)
            End If

        Catch ex As Exception
            g_MessaggioErrore &= "VariaTrainQuali_904 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Qualità del servizio cliente
    ''' </summary>
    Private Sub TrainQualiProce_905()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TrainQualiProce"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sAbsolQualiProceValue As String = GetVariableState("AbsolQualiProce", 0, "", 0, 0)
        Dim dTeamMax As Double
        Dim dTeamMIN As Double
        Dim dCUMUL As Double
        Dim dUpper As Double = Nn(sAbsolQualiProceValue)
        Dim dLower As Double = Nn(sAbsolQualiProceValue)

        Dim sMarkeAveraTrainValue As String
        Dim sVariaTrainQualiValue As String

        Dim sIncreTrainCumulValue As String
        Dim sCumulTrainLevelValue As String

        Dim iNUMPlayers As Integer = m_DataTablePlayers.Rows.Count

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                sMarkeAveraTrainValue = GetVariableDefineValue("MarkeAveraTrain", 0, 0, 0)
                sVariaTrainQualiValue = GetVariableDefineValue("VariaTrainQuali", 0, 0, 0)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    dCUMUL += Nn(GetVariableState("CumulTrainLevel", iIDPlayer, "", 0, 0))
                Next

                dUpper = Min(100, Nn(sAbsolQualiProceValue) + Min(0.3, Nn(sVariaTrainQualiValue) / (Nn(sMarkeAveraTrainValue) + dCUMUL / iNUMPlayers)) * Nn(sAbsolQualiProceValue))
                dLower = Max(1, Nn(sAbsolQualiProceValue) - Min(0.3, Nn(sVariaTrainQualiValue) / (Nn(sMarkeAveraTrainValue) + dCUMUL / iNUMPlayers)) * Nn(sAbsolQualiProceValue))

                dTeamMax = 0
                dTeamMIN = 99999
                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sIncreTrainCumulValue = GetVariableDefineValue("IncreTrainCumul", iIDPlayer, 0, 0)
                    sCumulTrainLevelValue = GetVariableState("CumulTrainLevel", iIDPlayer, "", 0, 0)
                    If Nn(sIncreTrainCumulValue) + Nn(sCumulTrainLevelValue) > 0 AndAlso Nn(sIncreTrainCumulValue) + Nn(sCumulTrainLevelValue) > dTeamMax Then
                        dTeamMax = Nn(sIncreTrainCumulValue) + Nn(sCumulTrainLevelValue)
                    End If
                Next

                dTeamMIN = 99999
                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sIncreTrainCumulValue = GetVariableDefineValue("IncreTrainCumul", iIDPlayer, 0, 0)
                    sCumulTrainLevelValue = GetVariableState("CumulTrainLevel", iIDPlayer, "", 0, 0)

                    If Nn(sIncreTrainCumulValue) + Nn(sCumulTrainLevelValue) < 99999 AndAlso Nn(sIncreTrainCumulValue) + Nn(sCumulTrainLevelValue) < dTeamMIN Then
                        dTeamMIN = Nn(sIncreTrainCumulValue) + Nn(sCumulTrainLevelValue)
                    End If
                Next

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    If dTeamMax - dTeamMIN = 0 Then
                        sValore = Nn(sAbsolQualiProceValue)
                    Else
                        sIncreTrainCumulValue = GetVariableDefineValue("IncreTrainCumul", iIDPlayer, 0, 0)
                        sCumulTrainLevelValue = GetVariableState("CumulTrainLevel", iIDPlayer, "", 0, 0)

                        sValore = (dUpper - dLower) * (Nn(sIncreTrainCumulValue) + Nn(sCumulTrainLevelValue) - dTeamMIN) / (dTeamMax - dTeamMIN) + dLower
                    End If


                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next
            End If

        Catch ex As Exception
            g_MessaggioErrore &= "TrainQualiProce_905 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Qualità del servizio al cliente
    ''' </summary>
    Private Sub QualiTrainProce_906()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "QualiTrainProce"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sAbsolQualiTrainValue As String = GetVariableState("AbsolQualiTrain", 0, "", 0, 0)
        Dim dTeamMax As Double
        Dim dTeamMIN As Double
        Dim dCUMUL As Double
        Dim dUpper As Double = Nn(sAbsolQualiTrainValue)
        Dim dLower As Double = Nn(sAbsolQualiTrainValue)

        Dim sMarkeAveraTrainValue As String
        Dim sVariaTrainQualiValue As String

        Dim sIncreTrainCumulValue As String
        Dim sCumulTrainLevelValue As String

        Dim iNUMPlayers As Integer = m_DataTablePlayers.Rows.Count

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                sMarkeAveraTrainValue = GetVariableDefineValue("MarkeAveraTrain", 0, 0, 0)
                sVariaTrainQualiValue = GetVariableDefineValue("VariaTrainQuali", 0, 0, 0)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    dCUMUL += Nn(GetVariableState("CumulTrainLevel", iIDPlayer, "", 0, 0))
                Next

                dUpper = Nn(sAbsolQualiTrainValue) + Nn(sVariaTrainQualiValue) / (Nn(sMarkeAveraTrainValue) + dCUMUL / iNUMPlayers) * Nn(sAbsolQualiTrainValue)
                dLower = Nn(sAbsolQualiTrainValue) - Nn(sVariaTrainQualiValue) / (Nn(sMarkeAveraTrainValue) + dCUMUL / iNUMPlayers) * Nn(sAbsolQualiTrainValue)

                dTeamMax = 0
                dTeamMIN = 99999

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sIncreTrainCumulValue = GetVariableDefineValue("IncreTrainCumul", iIDPlayer, 0, 0)
                    sCumulTrainLevelValue = GetVariableState("CumulTrainLevel", iIDPlayer, "", 0, 0)
                    If Nn(sIncreTrainCumulValue) + Nn(sCumulTrainLevelValue) > 0 AndAlso Nn(sIncreTrainCumulValue) + Nn(sCumulTrainLevelValue) > dTeamMax Then
                        dTeamMax = Nn(sIncreTrainCumulValue) + Nn(sCumulTrainLevelValue)
                    End If
                Next

                dTeamMIN = 99999
                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sIncreTrainCumulValue = GetVariableDefineValue("IncreTrainCumul", iIDPlayer, 0, 0)
                    sCumulTrainLevelValue = GetVariableState("CumulTrainLevel", iIDPlayer, "", 0, 0)

                    If Nn(sIncreTrainCumulValue) + Nn(sCumulTrainLevelValue) < 99999 AndAlso Nn(sIncreTrainCumulValue) + Nn(sCumulTrainLevelValue) < dTeamMIN Then
                        dTeamMIN = Nn(sIncreTrainCumulValue) + Nn(sCumulTrainLevelValue)
                    End If
                Next

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    If dTeamMax - dTeamMIN = 0 Then
                        sValore = Nn(sAbsolQualiTrainValue)
                    Else
                        sIncreTrainCumulValue = GetVariableDefineValue("IncreTrainCumul", iIDPlayer, 0, 0)
                        sCumulTrainLevelValue = GetVariableState("CumulTrainLevel", iIDPlayer, "", 0, 0)

                        sValore = (dUpper - dLower) * (Nn(sIncreTrainCumulValue) + Nn(sCumulTrainLevelValue) - dTeamMIN) / (dTeamMax - dTeamMIN) + dLower
                    End If


                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next
            End If

        Catch ex As Exception
            g_MessaggioErrore &= "QualiTrainProce_906 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Varianza sul livello pubblicitario
    ''' </summary>
    Private Sub VariaAdverQuali_907()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "VariaAdverQuali"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sCumulAdverLevelValue As String
        Dim sIncreAdverCumulValue As String
        Dim sAveraAdverLevelValue As String

        Dim iNUMPlayers As Integer = m_DataTablePlayers.Rows.Count

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                sAveraAdverLevelValue = GetVariableDefineValue("AveraAdverLevel", 0, 0, 0)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sCumulAdverLevelValue = GetVariableState("CumulAdverLevel", iIDPlayer, "", 0, 0)
                    sIncreAdverCumulValue = GetVariableDefineValue("IncreAdverCumul", iIDPlayer, 0, 0)
                    sValore = Nn(sValore) + Pow((Nn(sCumulAdverLevelValue) + Nn(sIncreAdverCumulValue) - Nn(sAveraAdverLevelValue) / iNUMPlayers), 2.0) / iNUMPlayers
                Next
                sValore = Pow(Nn(sValore), 0.5) * 3

                SaveVariableValue(iIDVariabile, 0, 0, 0, sValore)
            End If

        Catch ex As Exception
            g_MessaggioErrore &= "VariaAdverQuali_907 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Il ricordo pubblicitario
    ''' </summary>
    Private Sub AdverQualiProce_908()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "AdverQualiProce"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sAbsolAdverMarkeValue As String = GetVariableState("AbsolAdverMarke", 0, "", 0, 0)
        Dim dTeamMax As Double
        Dim dTeamMIN As Double
        Dim dCUMUL As Double
        Dim dUpper As Double = Nn(sAbsolAdverMarkeValue)
        Dim dLower As Double = Nn(sAbsolAdverMarkeValue)

        Dim sMarkeAveraAdverValue As String
        Dim sVariaAdverQualiValue As String

        Dim sIncreAdverCumulValue As String
        Dim sCumulAdverLevelValue As String

        Dim iNUMPlayers As Integer = m_DataTablePlayers.Rows.Count

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                sMarkeAveraAdverValue = GetVariableDefineValue("MarkeAveraAdver", 0, 0, 0)
                sVariaAdverQualiValue = GetVariableDefineValue("VariaAdverQuali", 0, 0, 0)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    dCUMUL += Nn(GetVariableState("CumulAdverLevel", iIDPlayer, "", 0, 0))
                Next

                dUpper = Min(100, Nn(sAbsolAdverMarkeValue) + Min(0.3, Nn(sVariaAdverQualiValue) / (Nn(sMarkeAveraAdverValue) + dCUMUL / iNUMPlayers)) * Nn(sAbsolAdverMarkeValue))
                dLower = Max(0, Nn(sAbsolAdverMarkeValue) - Min(0.3, Nn(sVariaAdverQualiValue) / (Nn(sMarkeAveraAdverValue) + dCUMUL / iNUMPlayers)) * Nn(sAbsolAdverMarkeValue))

                dTeamMax = 0
                dTeamMIN = 99999
                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sIncreAdverCumulValue = GetVariableDefineValue("IncreAdverCumul", iIDPlayer, 0, 0)
                    sCumulAdverLevelValue = GetVariableState("CumulAdverLevel", iIDPlayer, "", 0, 0)
                    If Nn(sIncreAdverCumulValue) + Nn(sCumulAdverLevelValue) > 0 AndAlso Nn(sIncreAdverCumulValue) + Nn(sCumulAdverLevelValue) > dTeamMax Then
                        dTeamMax = Nn(sIncreAdverCumulValue) + Nn(sCumulAdverLevelValue)
                    End If
                Next

                dTeamMIN = 99999
                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sIncreAdverCumulValue = GetVariableDefineValue("IncreAdverCumul", iIDPlayer, 0, 0)
                    sCumulAdverLevelValue = GetVariableState("CumulAdverLevel", iIDPlayer, "", 0, 0)

                    If Nn(sIncreAdverCumulValue) + Nn(sCumulAdverLevelValue) < 99999 AndAlso Nn(sIncreAdverCumulValue) + Nn(sCumulAdverLevelValue) < dTeamMIN Then
                        dTeamMIN = Nn(sIncreAdverCumulValue) + Nn(sCumulAdverLevelValue)
                    End If
                Next

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    If dTeamMax - dTeamMIN = 0 Then
                        sValore = Nn(sAbsolAdverMarkeValue)
                    Else
                        sIncreAdverCumulValue = GetVariableDefineValue("IncreAdverCumul", iIDPlayer, 0, 0)
                        sCumulAdverLevelValue = GetVariableState("CumulAdverLevel", iIDPlayer, "", 0, 0)

                        sValore = (dUpper - dLower) * (Nn(sIncreAdverCumulValue) + Nn(sCumulAdverLevelValue) - dTeamMIN) / (dTeamMax - dTeamMIN) + dLower
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next
            End If

        Catch ex As Exception
            g_MessaggioErrore &= "AdverQualiProce_908 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Incremento delle spese tecnologiche medie
    ''' </summary>
    Private Sub IncreAveraGreen_909()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "IncreAveraGreen"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sGreenAllowValue As String
        Dim sAveraGreenInvesValue As String

        Dim iNUMPlayers As Integer = m_DataTablePlayers.Rows.Count

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    sGreenAllowValue = GetVariableDefineValue("GreenAllow", iIDPlayer, 0, 0)
                    sAveraGreenInvesValue = GetVariableState("AveraGreenInves", iIDPlayer, "", 0, 0)

                    sValore = (Nn(sGreenAllowValue) - Nn(sAveraGreenInvesValue)) / m_CurrentStep
                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "IncreAveraGreen_909 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Investimenti ecologici annui
    ''' </summary>
    Private Sub GreenYearAllow_910()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "GreenYearAllow"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sGreenAllowValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    sValore = "0"
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sGreenAllowValue = GetVariableDefineValue("GreenAllow", iIDPlayer, 0, 0)

                    sValore = Nn(sGreenAllowValue) * (m_LunghezzaPeriodo / 12)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "GreenYearAllow_910 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Tabella sugli investimenti tecnologici annui
    ''' </summary>
    Private Sub TableGreenPermi_911()
        Dim iIDVariabile As Integer
        Dim dValueCalc As String = ""
        Dim sNomeVariabileCalc As String = "TableGreenPermi"
        Dim sNomeVariabileEntrata As String = "GreenYearAllow"
        Dim iIDPlayer As Integer = 0

        Try
            ' Ciclo sui players presenti nel game
            If m_DataTablePlayers.Rows.Count > 0 Then
                ' Controllo la presenza della variabile da calcolare e mi faccio ritornare l'id 
                iIDVariabile = CheckVariableExists(sNomeVariabileCalc, m_IDGame)
                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))

                    dValueCalc = GetVariableDefineValue(sNomeVariabileEntrata, iIDPlayer, 0, 0)
                    dValueCalc = CalculateInterpolation(sNomeVariabileCalc, dValueCalc)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, dValueCalc)
                Next
            End If

        Catch ex As Exception
            g_MessaggioErrore &= "TableGreenPermi_911 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Incremento del livello ecologico medio
    ''' </summary>
    Private Sub IncreGreenCumul_912()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "IncreGreenCumul"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sTableGreenPermiValue As String
        Dim sBossCurreGreenValue As String
        Dim sTableAveraGreenValue As String
        Dim sBossPastGreenValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    sValore = "0"
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sTableGreenPermiValue = GetVariableDefineValue("TableGreenPermi", iIDPlayer, 0, 0)
                    sBossCurreGreenValue = GetVariableDeveloper(m_IDGame, "BossCurreGreen", iIDPlayer, 0)
                    sTableAveraGreenValue = GetVariableDefineValue("TableAveraGreen", iIDPlayer, 0, 0)
                    sBossPastGreenValue = GetVariableDeveloper(m_IDGame, "BossPastGreen", iIDPlayer, 0)

                    sValore = (Nn(sTableGreenPermiValue) * Min(1, Nn(sBossCurreGreenValue)) + Nn(sTableAveraGreenValue) * Min(1, Nn(sBossPastGreenValue))) / (Min(1, Nn(sBossCurreGreenValue)) + Min(1, Nn(sBossPastGreenValue)))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "IncreGreenCumul_912 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    Private Sub MarkeAveraGreen_913()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "MarkeAveraGreen"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sIncreGreenCumulValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sIncreGreenCumulValue = GetVariableDefineValue("IncreGreenCumul", iIDPlayer, 0, 0)

                    sValore = Nn(sValore) + Nn(sIncreGreenCumulValue)

                Next
                sValore = Nn(sValore) / Max(1, m_DataTablePlayers.Rows.Count)

                SaveVariableValue(iIDVariabile, 0, 0, 0, sValore)

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "MarkeAveraGreen_913 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Incremento delle spese medie ecol.di mkt
    ''' </summary>
    Private Sub IncreAbsolGreen_914()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "IncreAbsolGreen"

        Dim IncreAbsolGreen As String

        Dim MarkeAveraGreen As String
        Dim SpeedWorldChang As String
        Dim AbsolQualiGreen As String

        Try
            iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

            IncreAbsolGreen = "0"

            MarkeAveraGreen = GetVariableDefineValue("MarkeAveraGreen", 0, 0, 0)
            SpeedWorldChang = GetVariableDeveloper(m_IDGame, "SpeedWorldChang", 0, 0)
            AbsolQualiGreen = GetVariableState("AbsolQualiGreen", 0, "", 0, 0)

            IncreAbsolGreen = Min(20, Nn(MarkeAveraGreen) - Nn(SpeedWorldChang))

            If (Nn(MarkeAveraGreen) > Nn(SpeedWorldChang)) Then
                IncreAbsolGreen = Min(100 - Nn(AbsolQualiGreen), Nn(IncreAbsolGreen) * (100 - Nn(AbsolQualiGreen)) / 100)
            Else
                IncreAbsolGreen = Min(Nn(AbsolQualiGreen) - 45, Nn(IncreAbsolGreen) * (Nn(AbsolQualiGreen) - 45) / 100)
            End If

            SaveVariableValue(iIDVariabile, 0, 0, 0, IncreAbsolGreen)

        Catch ex As Exception
            g_MessaggioErrore &= "IncreAbsolGreen_914 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Media green level
    ''' </summary>
    Private Sub AveraGreenLevel_915()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "AveraGreenLevel"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sIncreGreenCumulValue As String
        Dim sCumulGreenLevelValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sIncreGreenCumulValue = GetVariableDefineValue("IncreGreenCumul", iIDPlayer, 0, 0)
                    sCumulGreenLevelValue = GetVariableState("CumulGreenLevel", iIDPlayer, "", 0, 0)
                    sValore = Nn(sValore) + Nn(sIncreGreenCumulValue) + Nn(sCumulGreenLevelValue)

                Next

                SaveVariableValue(iIDVariabile, 0, 0, 0, sValore)

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "AveraGreenLevel_915 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Media degli investimenti ecologici
    ''' </summary>
    Private Sub MaximGreenPeace_916()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "MaximGreenPeace"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sGreenAllowValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sGreenAllowValue = GetVariableDefineValue("GreenAllow", iIDPlayer, 0, 0)

                    sValore = Nn(sValore) + Nn(sGreenAllowValue)
                Next

                SaveVariableValue(iIDVariabile, 0, 0, 0, sValore)

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "MaximGreenPeace_916 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Costi diretti - Prodotto
    ''' </summary>
    Private Sub DirecProduCosts_917()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DirecProduCosts"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim sProduRequeValue As String
        Dim sNomeVariabileLista As String
        Dim sProduUnitCostValue As String
        Dim sRicarTermiMeseValue As String
        Dim sDiritRitarPagamValue As String
        Dim sLagPaymeProduValue As String
        Dim sDelayPaymeProduValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        sProduRequeValue = GetVariableDefineValue("ProduReque", iIDPlayer, iIDItem, 0)
                        sNomeVariabileLista = "ProduUnitCost_" & sItemName
                        sProduUnitCostValue = GetVariableBoss("ProduUnitCost", sNomeVariabileLista)
                        sRicarTermiMeseValue = GetVariableBoss("RicarTermiMese", "")
                        sDiritRitarPagamValue = GetVariableBoss("DiritRitarPagam", "")
                        sLagPaymeProduValue = GetVariableBoss("LagPaymeProdu", "")
                        sDelayPaymeProduValue = GetVariableDecisionPlayer(m_IDGame, "DelayPaymeProdu", iIDPlayer, 0)

                        sValore = Nn(sProduRequeValue) * Nn(sProduUnitCostValue) * (1 + Nn(sRicarTermiMeseValue) / 100 * Nn(sDiritRitarPagamValue) * Min(m_LunghezzaPeriodo - Nn(sLagPaymeProduValue), Nn(sDelayPaymeProduValue)))

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "DirecProduCosts_917 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Costi diretti - totali
    ''' </summary>
    Private Sub DirecTotalCosts_918()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DirecTotalCosts"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim sDirecProduCostsValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = "0"

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        sDirecProduCostsValue = GetVariableDefineValue("DirecProduCosts", iIDPlayer, iIDItem, 0)

                        sValore = Nn(sValore) + Nn(sDirecProduCostsValue)
                    Next

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "DirecTotalCosts_918 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' = Costo totale di produzione
    ''' </summary>
    Private Sub ProduTotalCost_919()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "ProduTotalCost"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sDirecTotalCostsValue As String
        Dim sCostPurchTotalvalue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sDirecTotalCostsValue = GetVariableDefineValue("DirecTotalCosts", iIDPlayer, 0, 0)
                    sCostPurchTotalvalue = GetVariableDefineValue("CostPurchTotal", iIDPlayer, 0, 0)

                    sValore = Nn(sDirecTotalCostsValue) + Nn(sCostPurchTotalvalue)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "ProduTotalCost_919 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Costi fissi totali
    ''' </summary>
    Private Sub FixedProduCosts_920()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "FixedProduCosts"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sPlantTousePerioValue As String
        Dim sFixedCostPlantValue As String
        Dim sTaxCompanyValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                sFixedCostPlantValue = GetVariableBoss("FixedCostPlant", "")
                sTaxCompanyValue = GetVariableBoss("TaxCompany", 0)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sPlantTousePerioValue = GetVariableDefineValue("PlantTousePerio", iIDPlayer, 0, 0)

                    sValore = Nn(sPlantTousePerioValue) * Nn(sFixedCostPlantValue) + Nn(sTaxCompanyValue)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "FixedProduCosts_920 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

#End Region

#Region "201 - 220 FUNZIONI DI CALCOLO"

    ''' <summary>
    ''' (Costi fissi totali (duplicato))  
    ''' </summary>
    Private Sub ProduFixedCosts_1001()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "ProduFixedCosts"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sFixedProduCostsValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sFixedProduCostsValue = GetVariableDefineValue("FixedProduCosts", iIDPlayer, 0, 0)

                    sValore = Nn(sFixedProduCostsValue)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "ProduFixedCosts_1001 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Ammortamento dei macchinari
    ''' </summary>
    Private Sub SinkiFundPlant_1002()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "SinkiFundPlant"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sPlantTousePerioValue As String
        Dim sPlantSoldAllowValue As String
        Dim sTotalMachiLeaseValue As String
        Dim sNewPlantCostValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                sNewPlantCostValue = GetVariableBoss("NewPlantCost", "")
                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sPlantTousePerioValue = GetVariableDefineValue("PlantTousePerio", iIDPlayer, 0, 0)
                    sPlantSoldAllowValue = GetVariableDefineValue("PlantSoldAllow", iIDPlayer, 0, 0)
                    sTotalMachiLeaseValue = GetVariableDefineValue("TotalMachiLease", iIDPlayer, 0, 0)

                    sValore = (Nn(sPlantTousePerioValue) + Nn(sPlantSoldAllowValue) - Nn(sTotalMachiLeaseValue)) * Nn(sNewPlantCostValue) / 12

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "SinkiFundPlant_1002 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Leasing
    ''' </summary>
    Private Sub LeasiTotalCost_1003()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "LeasiTotalCost"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sTotalLeaseValue1 As String
        Dim sTotalLeaseValue2 As String
        Dim sTotalLeaseValue3 As String
        Dim sTotalLeaseValue4 As String
        Dim sNewLeaseCostValue As String
        Dim sRicarTermiMeseValue As String
        Dim sDiritRitarPagamValue As String
        Dim sLagPaymeLeaseValue As String
        Dim sDelayPaymeLeaseValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                sNewLeaseCostValue = GetVariableDefineValue("NewLeaseCost", 0, 0, 0)
                sRicarTermiMeseValue = GetVariableBoss("RicarTermiMese", "")
                sDiritRitarPagamValue = GetVariableBoss("DiritRitarPagam", "")
                sLagPaymeLeaseValue = GetVariableBoss("LagPaymeLease", "")

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sTotalLeaseValue1 = GetVariableDefineValue("TotalLease", iIDPlayer, 0, 1)
                    sTotalLeaseValue2 = GetVariableDefineValue("TotalLease", iIDPlayer, 0, 2)
                    sTotalLeaseValue3 = GetVariableDefineValue("TotalLease", iIDPlayer, 0, 3)
                    sTotalLeaseValue4 = GetVariableDefineValue("TotalLease", iIDPlayer, 0, 4)

                    sDelayPaymeLeaseValue = GetVariableDecisionPlayer(m_IDGame, "DelayPaymeLease", iIDPlayer, 0)

                    sValore = (Nn(sTotalLeaseValue1) * Nn(sNewLeaseCostValue) * (1 + Nn(sRicarTermiMeseValue) / 100 * Nn(sDiritRitarPagamValue) * Min(m_LunghezzaPeriodo - Nn(sLagPaymeLeaseValue), Nn(sDelayPaymeLeaseValue)))) + (Nn(sTotalLeaseValue2) + Nn(sTotalLeaseValue3) + Nn(sTotalLeaseValue4)) * Nn(sNewLeaseCostValue)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "LeasiTotalCost_1003 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Costi diretti
    ''' </summary>
    Private Sub CumulDirecCosts_1004()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CumulDirecCosts"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sVariaDirecCostsValue As String
        Dim sDirecTotalCostsValue As String
        Dim sDischDirecCostsValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sVariaDirecCostsValue = GetVariableState("VariaDirecCosts", iIDPlayer, "", 0, 0)
                    sDirecTotalCostsValue = GetVariableDefineValue("DirecTotalCosts", iIDPlayer, 0, 0)
                    sDischDirecCostsValue = GetVariableDefineValue("DischDirecCosts", iIDPlayer, 0, 0)

                    sValore = Nn(sVariaDirecCostsValue) + Nn(sDirecTotalCostsValue) - Nn(sDischDirecCostsValue)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "CumulDirecCosts_1004 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Ammortamento dei macchinari
    ''' </summary>
    Private Sub CumulSinkiPlant_1005()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CumulSinkiPlant"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sVariaSinkiPlantValue As String
        Dim sSinkiFundPlantValue As String
        Dim sDischSinkiPlantValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sVariaSinkiPlantValue = GetVariableState("VariaSinkiPlant", iIDPlayer, "", 0, 0)
                    sSinkiFundPlantValue = GetVariableDefineValue("SinkiFundPlant", iIDPlayer, 0, 0)
                    sDischSinkiPlantValue = GetVariableDefineValue("DischSinkiPlant", iIDPlayer, 0, 0)

                    sValore = Nn(sVariaSinkiPlantValue) + Nn(sSinkiFundPlantValue) - Nn(sDischSinkiPlantValue)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "CumulSinkiPlant_1005 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Leasing
    ''' </summary>
    Private Sub CumulLeasiCosts_1006()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CumulLeasiCosts"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sVariaLeasiCostsValue As String
        Dim sLeasiTotalCostValue As String
        Dim sDischLeasiCostsValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sVariaLeasiCostsValue = GetVariableState("VariaLeasiCosts", iIDPlayer, "", 0, 0)
                    sLeasiTotalCostValue = GetVariableDefineValue("LeasiTotalCost", iIDPlayer, 0, 0)
                    sDischLeasiCostsValue = GetVariableDefineValue("DischLeasiCosts", iIDPlayer, 0, 0)

                    sValore = Nn(sVariaLeasiCostsValue) + Nn(sLeasiTotalCostValue) - Nn(sDischLeasiCostsValue)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "CumulLeasiCosts_1006 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Costi fissi
    ''' </summary>
    Private Sub CumulFixedCosts_1007()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CumulFixedCosts"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sVariaFixedCostsValue As String
        Dim sProduFixedCostsValue As String
        Dim sDischFixedCostsValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sVariaFixedCostsValue = GetVariableState("VariaFixedCosts", iIDPlayer, "", 0, 0)
                    sProduFixedCostsValue = GetVariableDefineValue("ProduFixedCosts", iIDPlayer, 0, 0)
                    sDischFixedCostsValue = GetVariableDefineValue("DischFixedCosts", iIDPlayer, 0, 0)

                    sValore = Nn(sVariaFixedCostsValue) + Nn(sProduFixedCostsValue) - Nn(sDischFixedCostsValue)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "CumulFixedCosts_1007 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Costi indir(amm.) imputabili ai prodotti
    ''' </summary>
    Private Sub ProduCostsItem_1008()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "ProduCostsItem"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String
        Dim iIDItem As Integer
        Dim sItemName As String

        Dim dOre As Double
        Dim sNomeVariabileLista As String
        Dim sProduRequeValue As String
        Dim sHoursPerItemValue As String
        Dim sSinkiFundPlantValue As String
        Dim sLeasiTotalCostValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    sValore = "0"

                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        sProduRequeValue = GetVariableDefineValue("ProduReque", iIDPlayer, iIDItem, 0)
                        sNomeVariabileLista = "var_" & sTeamName & "_HoursPerItem_" & sItemName
                        sHoursPerItemValue = GetVariableBoss("HoursPerItem", sNomeVariabileLista)
                        dOre += Nn(sProduRequeValue) * Nn(sHoursPerItemValue)
                    Next

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        sProduRequeValue = GetVariableDefineValue("ProduReque", iIDPlayer, iIDItem, 0)

                        If dOre = 0 OrElse Nn(sProduRequeValue) = 0 Then
                            sValore = 0
                        Else
                            sNomeVariabileLista = "var_" & sTeamName & "_HoursPerItem_" & sItemName
                            sHoursPerItemValue = GetVariableBoss("HoursPerItem", sNomeVariabileLista)
                            sSinkiFundPlantValue = GetVariableDefineValue("SinkiFundPlant", iIDPlayer, 0, 0)
                            sLeasiTotalCostValue = GetVariableDefineValue("LeasiTotalCost", iIDPlayer, 0, 0)

                            sValore = (Nn(sProduRequeValue) * Nn(sHoursPerItemValue)) / dOre * (Nn(sSinkiFundPlantValue) + Nn(sLeasiTotalCostValue)) / Nn(sProduRequeValue)
                        End If

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "ProduCostsItem_1008 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Incremento dello stock di materie prime
    ''' </summary>
    Private Sub IncreQuantStock_1009()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "IncreQuantStock"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String
        Dim iIDItem As Integer
        Dim sItemName As String

        Dim sPurchAllowEuropValue As String
        Dim sPurchAllowNICValue As String
        Dim sProduRequeValue As String
        Dim sPortfPurchNICValue As String
        Dim sPortfDelivNICValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        sPurchAllowEuropValue = GetVariableDefineValue("PurchAllowEurop", iIDPlayer, iIDItem, 0)
                        sPurchAllowNICValue = GetVariableDefineValue("PurchAllowNIC", iIDPlayer, iIDItem, 0)
                        sProduRequeValue = GetVariableDefineValue("ProduReque", iIDPlayer, iIDItem, 0)
                        sPortfPurchNICValue = GetVariableDefineValue("PortfPurchNIC", iIDPlayer, iIDItem, 0)
                        sPortfDelivNICValue = GetVariableDefineValue("PortfDelivNIC", iIDPlayer, iIDItem, 0)

                        sValore = Nn(sPurchAllowEuropValue) + Nn(sPurchAllowNICValue) - Nn(sProduRequeValue) - Nn(sPortfPurchNICValue) + Nn(sPortfDelivNICValue)

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "IncreQuantStock_1009 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Quantità in stock: materie 1e (/fine)
    ''' </summary>
    Private Sub StockRawNext_1010()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "StockRawNext"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String
        Dim iIDItem As Integer
        Dim sItemName As String

        Dim sNomeVariabileLista As String
        Dim sStockRawMaterValue As String
        Dim sIncreQuantStockValule As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        sNomeVariabileLista = "var_" & sTeamName & "_StockRawMater_" & sItemName
                        sStockRawMaterValue = GetVariableState("StockRawMater", iIDPlayer, sNomeVariabileLista, iIDItem, 0)
                        sIncreQuantStockValule = GetVariableDefineValue("IncreQuantStock", iIDPlayer, iIDItem, 0)

                        sValore = Nn(sStockRawMaterValue) + Nn(sIncreQuantStockValule)

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "StockRawNext_1010 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Incremento del valore delle mat.1e stock
    ''' </summary>
    Private Sub IncreValueRaw_1011()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "IncreValueRaw"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String
        Dim iIDItem As Integer
        Dim sItemName As String
        Dim dCompounded As Double

        Dim sNomeVariabileLista As String

        Dim sPurchAllowNICValue As String
        Dim sPortfDelivNICValue As String
        Dim sPurchAllowEuropValue As String
        Dim sRawEuropCostValue As String
        Dim sPortfPurchNICValue As String
        Dim sRawNICCostValue As String
        Dim sStockRawMaterValue As String
        Dim sIncreQuantStockValue As String
        Dim sValueRawMaterValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        sPurchAllowNICValue = GetVariableDefineValue("PurchAllowNIC", iIDPlayer, iIDItem, 0)
                        sPortfDelivNICValue = GetVariableDefineValue("PortfDelivNIC", iIDPlayer, iIDItem, 0)
                        sPurchAllowEuropValue = GetVariableDefineValue("PurchAllowEurop", iIDPlayer, iIDItem, 0)

                        sNomeVariabileLista = "RawEuropCost_" & sItemName
                        sRawEuropCostValue = GetVariableBoss("RawEuropCost", sNomeVariabileLista)

                        sPortfPurchNICValue = GetVariableDefineValue("PortfPurchNIC", iIDPlayer, iIDItem, 0)

                        sNomeVariabileLista = "RawNICCost_" & sItemName
                        sRawNICCostValue = GetVariableBoss("RawNICCost", sNomeVariabileLista)

                        sNomeVariabileLista = "var_" & sTeamName & "_StockRawMater_" & sItemName
                        sStockRawMaterValue = GetVariableState("StockRawMater", iIDPlayer, sNomeVariabileLista, iIDItem, 0)

                        sIncreQuantStockValue = GetVariableDefineValue("IncreQuantStock", iIDPlayer, iIDItem, 0)

                        sNomeVariabileLista = "var_" & sTeamName & "_ValueRawMater_" & sItemName
                        sValueRawMaterValue = GetVariableState("ValueRawMater", iIDPlayer, sNomeVariabileLista, iIDItem, 0)

                        If Nn(sPurchAllowNICValue) + Nn(sPortfDelivNICValue) - Nn(sPortfPurchNICValue) + Nn(sPurchAllowEuropValue) = 0 Then
                            dCompounded = 0
                        Else
                            dCompounded = ((Nn(sPurchAllowEuropValue) * Nn(sRawEuropCostValue)) + ((Nn(sPurchAllowNICValue) + Nn(sPortfDelivNICValue) - Nn(sPortfPurchNICValue)) * Nn(sRawNICCostValue))) / (Nn(sPurchAllowNICValue) + Nn(sPortfDelivNICValue) - Nn(sPortfPurchNICValue) + Nn(sPurchAllowEuropValue))
                        End If

                        If Nn(sStockRawMaterValue) + Nn(sIncreQuantStockValue) = 0 Then
                            sValore = 0
                        ElseIf Nn(sIncreQuantStockValue) <= 0 Then
                            sValore = 0
                        Else
                            sValore = Nn(sIncreQuantStockValue) * (dCompounded - Nn(sValueRawMaterValue)) / (Nn(sStockRawMaterValue) + Nn(sIncreQuantStockValue))
                        End If


                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)

                    Next

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "IncreValueRaw_1011 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Incremento della qualità materie prime
    ''' </summary>
    Private Sub IncreQualiStock_1012()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "IncreQualiStock"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String
        Dim iIDItem As Integer
        Dim sItemName As String
        Dim dCompounded As Double

        Dim sNomeVariabileLista As String
        Dim sPurchAllowNICValue As String
        Dim sPortfDelivNICValue As String
        Dim sPortfPurchNICValue As String
        Dim sPurchAllowEuropValue As String
        Dim sRawEuropQualiValue As String
        Dim sRawNICQualiValue As String
        Dim sStockRawMaterValue As String
        Dim sIncreQuantStockValue As String
        Dim sRawStockQualiValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        sPurchAllowNICValue = GetVariableDefineValue("PurchAllowNIC", iIDPlayer, iIDItem, 0)
                        sPortfDelivNICValue = GetVariableDefineValue("PortfDelivNIC", iIDPlayer, iIDItem, 0)
                        sPortfPurchNICValue = GetVariableDefineValue("PortfPurchNIC", iIDPlayer, iIDItem, 0)
                        sPurchAllowEuropValue = GetVariableDefineValue("PurchAllowEurop", iIDPlayer, iIDItem, 0)

                        sNomeVariabileLista = "RawEuropQuali_" & sItemName
                        sRawEuropQualiValue = GetVariableBoss("RawEuropQuali", sNomeVariabileLista)

                        sNomeVariabileLista = "RawNICQuali_" & sItemName
                        sRawNICQualiValue = GetVariableBoss("RawNICQuali", sNomeVariabileLista)

                        sNomeVariabileLista = "var_" & sTeamName & "_StockRawMater_" & sItemName
                        sStockRawMaterValue = GetVariableState("StockRawMater", iIDPlayer, sNomeVariabileLista, iIDItem, 0)

                        sIncreQuantStockValue = GetVariableDefineValue("IncreQuantStock", iIDPlayer, iIDItem, 0)

                        sNomeVariabileLista = "var_" & sTeamName & "_RawStockQuali_" & sItemName
                        sRawStockQualiValue = GetVariableState("RawStockQuali", iIDPlayer, sNomeVariabileLista, iIDItem, 0)

                        If Nn(sPurchAllowNICValue) + Nn(sPortfDelivNICValue) - Nn(sPortfPurchNICValue) + Nn(sPurchAllowEuropValue) = 0 Then
                            dCompounded = 0
                        Else
                            dCompounded = ((Nn(sPurchAllowEuropValue) * Nn(sRawEuropQualiValue)) + ((Nn(sPurchAllowNICValue) + Nn(sPortfDelivNICValue) - Nn(sPortfPurchNICValue)) * Nn(sRawNICQualiValue))) / (Nn(sPurchAllowNICValue) + Nn(sPortfDelivNICValue) - Nn(sPortfPurchNICValue) + Nn(sPurchAllowEuropValue))
                        End If

                        If Nn(sStockRawMaterValue) + Nn(sIncreQuantStockValue) = 0 Then
                            sValore = -(Nn(sRawStockQualiValue))
                        ElseIf Nn(sIncreQuantStockValue) <= 0 Then
                            sValore = 0
                        Else
                            sValore = Nn(sIncreQuantStockValue) * (dCompounded - Nn(sRawStockQualiValue)) / (Nn(sStockRawMaterValue) + Nn(sIncreQuantStockValue))
                        End If

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)

                    Next

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "IncreQualiStock_1012 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Qualità delle materie usate in produzion
    ''' </summary>
    Private Sub RawQualiUsed_1013()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "RawQualiUsed"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String
        Dim iIDItem As Integer
        Dim sItemName As String

        Dim sNomeVariabileLista As String
        Dim sProduRequeValue As String
        Dim sRawEuropQualiValue As String
        Dim sRawNICQualiValue As String
        Dim sStockRawMaterValue As String
        Dim sIncreQuantStockValue As String
        Dim sRawStockQualiValue As String
        Dim sPurchAllowEuropValue As String
        Dim sPurchDelivNICValue As String
        Dim sPurchAllowNICValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        sProduRequeValue = GetVariableDefineValue("ProduReque", iIDPlayer, iIDItem, 0)
                        sNomeVariabileLista = "var_" & sTeamName & "_PurchDelivNIC_" & sItemName
                        sPurchDelivNICValue = GetVariableState("PurchDelivNIC", iIDPlayer, sNomeVariabileLista, iIDItem, 0)
                        sPurchAllowEuropValue = GetVariableDefineValue("PurchAllowEurop", iIDPlayer, iIDItem, 0)
                        sPurchAllowNICValue = GetVariableDefineValue("PurchAllowNIC", iIDPlayer, iIDItem, 0)

                        sNomeVariabileLista = "RawEuropQuali_" & sItemName
                        sRawEuropQualiValue = GetVariableBoss("RawEuropQuali", sNomeVariabileLista)
                        sNomeVariabileLista = "RawNICQuali_" & sItemName
                        sRawNICQualiValue = GetVariableBoss("RawNICQuali", sNomeVariabileLista)
                        sNomeVariabileLista = "StockRawMater_" & sItemName
                        sStockRawMaterValue = GetVariableState("StockRawMater", iIDPlayer, sNomeVariabileLista, iIDItem, 0)
                        sIncreQuantStockValue = GetVariableDefineValue("IncreQuantStock", iIDPlayer, iIDItem, 0)
                        sNomeVariabileLista = "RawStockQuali_" & sItemName
                        sRawStockQualiValue = GetVariableState("RawStockQuali", iIDPlayer, sNomeVariabileLista, iIDItem, 0)

                        If Nn(sProduRequeValue) = 0 Then
                            sValore = 0
                        ElseIf Nn(sPurchAllowEuropValue) + Nn(sPurchDelivNICValue) + Nn(sPurchAllowNICValue) = 0 Then
                            sValore = Nn(sRawStockQualiValue)
                        Else
                            sValore = (Min(Nn(sProduRequeValue), Nn(sStockRawMaterValue)) * Nn(sRawStockQualiValue) + Max(0, Nn(sProduRequeValue) - Nn(sStockRawMaterValue)) * ((Nn(sRawEuropQualiValue) * Nn(sPurchAllowEuropValue) + Nn(sRawNICQualiValue) * Nn(sPurchDelivNICValue) + Nn(sRawNICQualiValue) * Nn(sPurchAllowNICValue)) / (Nn(sPurchAllowEuropValue) + Nn(sPurchDelivNICValue) + Nn(sPurchAllowNICValue)))) / Nn(sProduRequeValue)
                        End If

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)

                    Next

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "RawQualiUsed_1013 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Qualità dei prodotti manufatti
    ''' </summary>
    Private Sub ProduTotalQuali_1014()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "ProduTotalQuali"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String
        Dim iIDItem As Integer
        Dim sItemName As String

        Dim sProduRequeValue As String
        Dim sProduQualiProceValue As String
        Dim sBossQualiProceValue As String
        Dim sRawQualiUsedValue As String
        Dim sBossQualiRawValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                sBossQualiProceValue = GetVariableBoss("BossQualiProce", "")
                sBossQualiRawValue = GetVariableBoss("BossQualiRaw", "")

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sProduQualiProceValue = GetVariableDefineValue("ProduQualiProce", iIDPlayer, 0, 0)

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        sProduRequeValue = GetVariableDefineValue("ProduReque", iIDPlayer, iIDItem, 0)
                        sRawQualiUsedValue = GetVariableDefineValue("RawQualiUsed", iIDPlayer, iIDItem, 0)

                        If Nn(sProduRequeValue) = 0 Then
                            sValore = 0
                        Else
                            sValore = (Nn(sProduQualiProceValue) * Min(1, Nn(sBossQualiProceValue)) + Nn(sRawQualiUsedValue) * Min(1, Nn(sBossQualiRawValue))) _
                                    / (Min(1, Nn(sBossQualiProceValue)) + Min(1, Nn(sBossQualiRawValue)))
                        End If

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "ProduTotalQuali_1014 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Prezzo offerto ai grossisti
    ''' </summary>
    Private Sub InterPriceAllow_1015()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "InterPriceAllow"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String
        Dim iIDItem As Integer
        Dim sItemName As String

        Dim sNomeVariabileLista As String
        Dim sLowesQualiAccepValue As String
        Dim sProduTotalQualiValue As String
        Dim sHighePriceAccepValue As String
        Dim sPriceMarkeRequiValue As String
        Dim sPreviProduCostsValue As String
        Dim sPreviInterPriceValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        sNomeVariabileLista = "LowesQualiAccep_" & sItemName
                        sLowesQualiAccepValue = GetVariableBoss("LowesQualiAccep", sNomeVariabileLista)

                        sProduTotalQualiValue = GetVariableDefineValue("ProduTotalQuali", iIDPlayer, iIDItem, 0)

                        sNomeVariabileLista = "HighePriceAccep_" & sItemName
                        sHighePriceAccepValue = GetVariableBoss("HighePriceAccep", sNomeVariabileLista)

                        sPriceMarkeRequiValue = GetVariableDecisionPlayer(m_IDGame, "PriceMarkeRequi", iIDPlayer, iIDItem)

                        sNomeVariabileLista = "var_" & sTeamName & "_PreviProduCosts_" & sItemName
                        sPreviProduCostsValue = GetVariableState("PreviProduCosts", iIDPlayer, sNomeVariabileLista, iIDItem, 0)

                        sNomeVariabileLista = "var_" & sTeamName & "_PreviInterPrice_" & sItemName
                        sPreviInterPriceValue = GetVariableState("PreviInterPrice", iIDPlayer, sNomeVariabileLista, iIDItem, 0)

                        If Nn(sLowesQualiAccepValue) > Nn(sProduTotalQualiValue) OrElse Nn(sHighePriceAccepValue) < Nn(sPriceMarkeRequiValue) OrElse Nn(sPriceMarkeRequiValue) = 0 Then
                            sValore = 0
                        Else
                            sValore = Max(Nn(sPreviProduCostsValue), Min(Nn(sPreviInterPriceValue) * 2.0, Max(Nn(sPreviInterPriceValue) * 0.5, Nn(sPriceMarkeRequiValue))))
                        End If

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "InterPriceAllow_1015 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Varianza sugli investimenti ecologici
    ''' </summary>
    Private Sub VariaGreenQuali_1016()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "VariaGreenQuali"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sCumulGreenLevelValue As String
        Dim sIncreGreenCumulValue As String
        Dim sAveraGreenLevelValue As String
        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"
                sAveraGreenLevelValue = GetVariableDefineValue("AveraGreenLevel", 0, 0, 0)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sCumulGreenLevelValue = GetVariableState("CumulGreenLevel", iIDPlayer, "", 0, 0)
                    sIncreGreenCumulValue = GetVariableDefineValue("IncreGreenCumul", iIDPlayer, 0, 0)

                    sValore = Nn(sValore) + Pow((Nn(sCumulGreenLevelValue) + Nn(sIncreGreenCumulValue) - Nn(sAveraGreenLevelValue) / m_DataTablePlayers.Rows.Count), 2) / m_DataTablePlayers.Rows.Count

                Next
                sValore = Pow(Nn(sValore), 0.5) * 3

                SaveVariableValue(iIDVariabile, 0, 0, 0, sValore)
            End If

        Catch ex As Exception
            g_MessaggioErrore &= "VariaGreenQuali_1016 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Ecoimmagine
    ''' </summary>
    Private Sub GreenQualiProce_1017()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "GreenQualiProce"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sAbsolQualiGreenValue As String = GetVariableState("AbsolQualiGreen", 0, "", 0, 0)
        Dim dTEAMMAX As Double
        Dim dTEAMMIN As Double
        Dim dCUMUL As Double
        Dim dUPPER As Double = Nn(sAbsolQualiGreenValue)
        Dim dLOWER As Double = Nn(sAbsolQualiGreenValue)
        Dim sIncreGreenCumulValue As String
        Dim sVariaGreenQualiValue As String
        Dim iNUMPlayers As String = m_DataTablePlayers.Rows.Count
        Dim sMarkeAveraGreenValule As String
        Dim sCumulGreenLevelValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                sVariaGreenQualiValue = GetVariableDefineValue("VariaGreenQuali", 0, 0, 0)
                sMarkeAveraGreenValule = GetVariableDefineValue("MarkeAveraGreen", 0, 0, 0)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    dCUMUL += Nn(GetVariableState("CumulGreenLevel", iIDPlayer, "", 0, 0))
                Next

                dUPPER = Min(100, Nn(sAbsolQualiGreenValue) + Min(0.3, Nn(sVariaGreenQualiValue) / (Nn(sMarkeAveraGreenValule) + dCUMUL / iNUMPlayers)) * Nn(sAbsolQualiGreenValue))
                dLOWER = Max(1, Nn(sAbsolQualiGreenValue) - Min(0.3, Nn(sVariaGreenQualiValue) / (Nn(sMarkeAveraGreenValule) + dCUMUL / iNUMPlayers)) * Nn(sAbsolQualiGreenValue))

                dTEAMMAX = 0
                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sIncreGreenCumulValue = GetVariableDefineValue("IncreGreenCumul", iIDPlayer, 0, 0)
                    sCumulGreenLevelValue = GetVariableState("CumulGreenLevel", iIDPlayer, "", 0, 0)

                    If Nn(sIncreGreenCumulValue) + Nn(sCumulGreenLevelValue) > 0 AndAlso Nn(sIncreGreenCumulValue) + Nn(sCumulGreenLevelValue) > dTEAMMAX Then
                        dTEAMMAX = Nn(sIncreGreenCumulValue) + Nn(sCumulGreenLevelValue)
                    End If
                Next

                dTEAMMIN = 99999
                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sIncreGreenCumulValue = GetVariableDefineValue("IncreGreenCumul", iIDPlayer, 0, 0)
                    sCumulGreenLevelValue = GetVariableState("CumulGreenLevel", iIDPlayer, "", 0, 0)

                    If Nn(sIncreGreenCumulValue) + Nn(sCumulGreenLevelValue) < 99999 AndAlso Nn(sIncreGreenCumulValue) + Nn(sCumulGreenLevelValue) < dTEAMMIN Then
                        dTEAMMIN = Nn(sIncreGreenCumulValue) + Nn(sCumulGreenLevelValue)
                    End If

                Next

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    If dTEAMMAX - dTEAMMIN = 0 Then
                        sValore = Nn(sAbsolQualiGreenValue)
                    Else
                        sIncreGreenCumulValue = GetVariableDefineValue("IncreGreenCumul", iIDPlayer, 0, 0)
                        sCumulGreenLevelValue = GetVariableState("CumulGreenLevel", iIDPlayer, "", 0, 0)

                        sValore = (dUPPER - dLOWER) * (Nn(sIncreGreenCumulValue) + Nn(sCumulGreenLevelValue) - dTEAMMIN) / (dTEAMMAX - dTEAMMIN) + dLOWER
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next
            End If

        Catch ex As Exception
            g_MessaggioErrore &= "GreenQualiProce_1017 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Quantità offerta ai grossisti
    ''' </summary>
    Private Sub InterSalesAllow_1018()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "InterSalesAllow"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String
        Dim iIDItem As Integer
        Dim sItemName As String

        Dim sNomeVariabileLista As String
        Dim sLowesQualiAccepValue As String
        Dim sProduTotalQualiValue As String
        Dim sHighePriceAccepValue As String
        Dim sPriceMarkeRequiValue As String
        Dim sInterPriceAllowValue As String
        Dim sQuantRequiInterValue As String
        Dim sInterMarkeRequiValue As String
        Dim sProduRequeValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        sNomeVariabileLista = "LowesQualiAccep_" & sItemName
                        sLowesQualiAccepValue = GetVariableBoss("LowesQualiAccep", sNomeVariabileLista)

                        sProduTotalQualiValue = GetVariableDefineValue("ProduTotalQuali", iIDPlayer, iIDItem, 0)

                        sNomeVariabileLista = "HighePriceAccep_" & sItemName
                        sHighePriceAccepValue = GetVariableBoss("HighePriceAccep", sNomeVariabileLista)

                        sNomeVariabileLista = "var_" & sTeamName & "_PriceMarkeRequi_" & sItemName
                        sPriceMarkeRequiValue = GetVariableDecisionPlayer(m_IDGame, "PriceMarkeRequi", iIDPlayer, iIDItem)

                        sInterPriceAllowValue = GetVariableDefineValue("InterPriceAllow", iIDPlayer, iIDItem, 0)

                        sNomeVariabileLista = "QuantRequiInter_" & sItemName
                        sQuantRequiInterValue = GetVariableBoss("QuantRequiInter", sNomeVariabileLista)

                        sInterMarkeRequiValue = GetVariableDecisionPlayer(m_IDGame, "InterMarkeRequi", iIDPlayer, iIDItem)
                        sProduRequeValue = GetVariableDefineValue("ProduReque", iIDPlayer, iIDItem, 0)

                        If Nn(sLowesQualiAccepValue) > Nn(sProduTotalQualiValue) OrElse Nn(sHighePriceAccepValue) < Nn(sPriceMarkeRequiValue) OrElse Nn(sInterPriceAllowValue) = 0 Then
                            sValore = 0
                        Else
                            sValore = Min(Min(Nn(sQuantRequiInterValue), Nn(sInterMarkeRequiValue)), Nn(sProduRequeValue))
                        End If

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "InterSalesAllow_1018 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Volumi totali offerti ai grossisti
    ''' </summary>
    Private Sub VolumInterOffer_1019()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "VolumInterOffer"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String
        Dim iIDItem As Integer

        Dim sInterSalesAllowValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowItem As DataRow In m_DataTableItems.Rows
                    iIDItem = Nni(oRowItem("IDVariable"))

                    sValore = "0"

                    For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                        iIDPlayer = Nni(oRowPlayer("IDTeam"))
                        sTeamName = Nz(oRowPlayer("TeamName"))
                        sInterSalesAllowValue = GetVariableDefineValue("InterSalesAllow", iIDPlayer, iIDItem, 0)
                        sValore = Nn(sValore) + Nn(sInterSalesAllowValue)
                    Next

                    SaveVariableValue(iIDVariabile, 0, iIDItem, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "VolumInterOffer_1019 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' PRezzo totale offerto-mkt ai grossisti
    ''' </summary>
    Private Sub TotalPriceOffer_1020()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TotalPriceOffer"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String
        Dim iIDItem As Integer

        Dim sInterSalesAllowValue As String
        Dim sInterPriceAllowValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowItem As DataRow In m_DataTableItems.Rows
                    iIDItem = Nni(oRowItem("IDVariable"))

                    sValore = "0"

                    For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                        iIDPlayer = Nni(oRowPlayer("IDTeam"))
                        sTeamName = Nz(oRowPlayer("TeamName"))
                        sInterSalesAllowValue = GetVariableDefineValue("InterSalesAllow", iIDPlayer, iIDItem, 0)
                        sInterPriceAllowValue = GetVariableDefineValue("InterPriceAllow", iIDPlayer, iIDItem, 0)
                        sValore = Nn(sValore) + Nn(sInterSalesAllowValue) * Nn(sInterPriceAllowValue)
                    Next

                    SaveVariableValue(iIDVariabile, 0, iIDItem, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "TotalPriceOffer_1020 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

#End Region

#Region "221 - 240 FUNZIONI DI CALCOLO"

    ''' <summary>
    ''' Qualità totale offerto-mkt ai grossisti
    ''' </summary>
    Private Sub TotalQualiOffer_1101()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TotalQualiOffer"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String
        Dim iIDItem As Integer

        Dim sInterSalesAllowValue As String
        Dim sProduTotalQualiValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowItem As DataRow In m_DataTableItems.Rows
                    iIDItem = Nni(oRowItem("IDVariable"))

                    sValore = 0

                    For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                        iIDPlayer = Nni(oRowPlayer("IDTeam"))
                        sTeamName = Nz(oRowPlayer("TeamName"))

                        sInterSalesAllowValue = GetVariableDefineValue("InterSalesAllow", iIDPlayer, iIDItem, 0)
                        sProduTotalQualiValue = GetVariableDefineValue("ProduTotalQuali", iIDPlayer, iIDItem, 0)

                        sValore = Nn(sValore) + (Nn(sInterSalesAllowValue) * Nn(sProduTotalQualiValue))

                    Next
                    SaveVariableValue(iIDVariabile, 0, iIDItem, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "TotalQualiOffer_1101 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Qualità media offerta ai grossisti
    ''' </summary>
    Private Sub AveraQualiOffer_1102()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "AveraQualiOffer"

        Dim sValore As String

        Dim iIDItem As Integer

        Dim sVolumInterOfferValue As String
        Dim sTotalQualiOfferValue As String

        Try
            iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

            For Each oRowItem As DataRow In m_DataTableItems.Rows
                iIDItem = Nni(oRowItem("IDVariable"))

                sVolumInterOfferValue = GetVariableDefineValue("VolumInterOffer", 0, iIDItem, 0)
                sTotalQualiOfferValue = GetVariableDefineValue("TotalQualiOffer", 0, iIDItem, 0)

                If Nn(sVolumInterOfferValue) = 0 Then
                    sValore = "0"
                Else
                    sValore = Nn(sTotalQualiOfferValue) / Nn(sVolumInterOfferValue)
                End If

                SaveVariableValue(iIDVariabile, 0, iIDItem, 0, sValore)
            Next

        Catch ex As Exception
            g_MessaggioErrore &= "AveraQualiOffer_1102 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Qualità della merce offerta
    ''' </summary>
    Private Sub QualiGoodsOffer_1103()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "QualiGoodsOffer"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String
        Dim iIDItem As Integer
        Dim sItemName As String

        Dim sNomeVariabileLista As String

        Dim sProduRequeValue As String
        Dim sStockFinisGoodsValue As String
        Dim sExtraAllowValue As String
        Dim sProduTotalQualiValue As String
        Dim sQualiStockFinisValue As String
        Dim sExtraProduQualiValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        sProduRequeValue = GetVariableDefineValue("ProduReque", iIDPlayer, iIDItem, 0)

                        sNomeVariabileLista = "var_" & sTeamName & "_StockFinisGoods_" & sItemName
                        sStockFinisGoodsValue = GetVariableState("StockFinisGoods", iIDPlayer, sNomeVariabileLista, iIDItem, 0)

                        sExtraAllowValue = GetVariableDefineValue("ExtraAllow", iIDPlayer, iIDItem, 0)
                        sProduTotalQualiValue = GetVariableDefineValue("ProduTotalQuali", iIDPlayer, iIDItem, 0)

                        sNomeVariabileLista = "var_" & sTeamName & "_QualiStockFinis_" & sItemName
                        sQualiStockFinisValue = GetVariableState("QualiStockFinis", iIDPlayer, sNomeVariabileLista, iIDItem, 0)

                        sNomeVariabileLista = "var_" & sTeamName & "_StockFinisGoods_" & sItemName
                        sStockFinisGoodsValue = GetVariableState("StockFinisGoods", iIDPlayer, sNomeVariabileLista, iIDItem, 0)

                        sNomeVariabileLista = "ExtraProduQuali_" & sItemName
                        sExtraProduQualiValue = GetVariableBoss("ExtraProduQuali", sNomeVariabileLista)

                        If Nn(sProduRequeValue) + Nn(sStockFinisGoodsValue) + Nn(sExtraAllowValue) = 0 Then
                            sValore = 0
                        Else
                            sValore = (Nn(sProduTotalQualiValue) * Nn(sProduRequeValue) + Nn(sQualiStockFinisValue) _
                                    * Nn(sStockFinisGoodsValue) + Nn(sExtraAllowValue) * Nn(sExtraProduQualiValue)) _
                                    / (Nn(sProduRequeValue) + Nn(sStockFinisGoodsValue) + Nn(sExtraAllowValue))
                        End If

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "QualiGoodsOffer_1103 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Indice di attrazione
    ''' </summary>
    Private Sub AttractionIndex_1104()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "AttractionIndex"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim dShare As Double = 0

        Dim sNomeVariabile As String
        Dim sPreviMarkeShareValue As String
        Dim sImporAdverQualiValue As String
        Dim sAdverQualiProceValue As String
        Dim sImporTrainQualiValue As String
        Dim sTrainQualiProceValue As String
        Dim sImporPreviShareValue As String
        Dim sGreenQualiProceValue As String
        Dim sImporNumbeStoreValue As String
        Dim sTotalCentrStoreValue As String
        Dim sCentrAlienAllowValue As String
        Dim sTotalPerifStoreValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                sPreviMarkeShareValue = "0"

                sImporAdverQualiValue = GetVariableBoss("ImporAdverQuali", "")
                sImporTrainQualiValue = GetVariableBoss("ImporTrainQuali", "")
                sImporPreviShareValue = GetVariableBoss("ImporPreviShare", "")
                sImporNumbeStoreValue = GetVariableBoss("ImporNumbeStore", "")

                For Each oRowItem As DataRow In m_DataTableItems.Rows
                    iIDItem = Nni(oRowItem("IDVariable"))
                    sItemName = Nz(oRowItem("VariableName"))

                    dShare = 0

                    For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                        iIDPlayer = Nni(oRowPlayer("IDTeam"))
                        sTeamName = Nz(oRowPlayer("TeamName"))

                        sNomeVariabile = "var_" & sTeamName & "_PreviMarkeShare_" & sItemName
                        dShare += Nn(GetVariableState("PreviMarkeShare", iIDPlayer, sNomeVariabile, iIDItem, 0))
                    Next

                    For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                        iIDPlayer = Nni(oRowPlayer("IDTeam"))
                        sTeamName = Nz(oRowPlayer("TeamName"))

                        sAdverQualiProceValue = GetVariableDefineValue("AdverQualiProce", iIDPlayer, 0, 0)
                        sTrainQualiProceValue = GetVariableDefineValue("TrainQualiProce", iIDPlayer, 0, 0)
                        sGreenQualiProceValue = GetVariableDefineValue("GreenQualiProce", iIDPlayer, 0, 0)
                        sTotalCentrStoreValue = GetVariableState("TotalCentrStore", iIDPlayer, "", 0, 0)
                        sCentrAlienAllowValue = GetVariableDefineValue("CentrAlienAllow", iIDPlayer, 0, 0)
                        sTotalPerifStoreValue = GetVariableState("TotalPerifStore", iIDPlayer, "", 0, 0)

                        sValore = (Nn(sImporAdverQualiValue) * Nn(sAdverQualiProceValue) _
                                  + Nn(sImporTrainQualiValue) * Nn(sTrainQualiProceValue) _
                                  + Nn(sImporPreviShareValue) * Nn(sGreenQualiProceValue) _
                                  + Nn(sImporPreviShareValue) * dShare / m_DataTableItems.Rows.Count * m_DataTablePlayers.Rows.Count _
                                  + (Nn(sImporNumbeStoreValue) _
                                  * Min(200, Max(30, (Nn(sTotalCentrStoreValue) - 5 - Nn(sCentrAlienAllowValue) + Nn(sTotalPerifStoreValue) / 3.0) * 7)))) _
                                  / (Nn(sImporAdverQualiValue) + Nn(sImporTrainQualiValue) + Nn(sImporPreviShareValue) * 2.0 + Nn(sImporNumbeStoreValue))

                        SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                    Next
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "AttractionIndex_1104 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Decremento della liquidità
    ''' </summary>
    Private Sub DecreBankCash_1105()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DecreBankCash"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sCostPurchEuropValue As String
        Dim sLagPaymeEuropValue As String = GetVariableBoss("LagPaymeEurop", "")
        Dim sDelayPaymeEuropValue As String
        Dim sDiritRitarPagamValue As String = GetVariableBoss("DiritRitarPagam", "")
        Dim sDirecTotalCostsValue As String
        Dim sLagPaymeProduValue As String = GetVariableBoss("LagPaymeProdu", "")
        Dim sDelayPaymeProduValue As String
        Dim sLeasiTotalCostValue As String
        Dim sLagPaymeLeaseValue As String = GetVariableBoss("LagPaymeLease", "")
        Dim sDelayPaymeLeaseValue As String
        Dim sPromoInvesPerioValue As String
        Dim sLagPaymePromoValue As String = GetVariableBoss("LagPaymePromo", "")
        Dim sDelayPaymePromoValue As String
        Dim sExtraMoneyAllowValue As String
        Dim sLagPaymeExtraValue As String = GetVariableBoss("LagPaymeExtra", "")
        Dim sForwaPaymeExtraValue As String
        Dim sDiritScontPagamValue As String = GetVariableBoss("DiritScontPagam", "")
        Dim sTechnCostsPerioValule As String
        Dim sLagPaymeAutomVaue As String = GetVariableBoss("LagPaymeAutom", "")
        Dim sForwaPaymeAutomValue As String
        Dim sAdditCostsPerioValue As String
        Dim sLagPaymeMktgValue As String = GetVariableBoss("LagPaymeMktg", "")
        Dim sForwaPaymeMktgValue As String
        Dim sCostPurchNICValue As String
        Dim sPlantPerAgeValue As String
        Dim sNewPlantCostValue As String = GetVariableBoss("NewPlantCost", "")
        Dim sTotalVehicOwnedValue As String
        Dim sNewVehicCostValue As String = GetVariableBoss("NewVehicCost", "")
        Dim sTotalCostsPersoValue As String
        Dim sTotalCostsOutleValue As String
        Dim sCreditorsValue As String
        Dim sTotalNegatInterValue As String
        Dim sLoansAllowValue As String
        Dim sTaxPaymentValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sCostPurchEuropValue = GetVariableDefineValue("CostPurchEurop", iIDPlayer, 0, 0)
                    sDelayPaymeEuropValue = GetVariableDecisionPlayer(m_IDGame, "DelayPaymeEurop", iIDPlayer, 0)
                    sDirecTotalCostsValue = GetVariableDefineValue("DirecTotalCosts", iIDPlayer, 0, 0)
                    sDelayPaymeProduValue = GetVariableDecisionPlayer(m_IDGame, "DelayPaymeProdu", iIDPlayer, 0)
                    sLeasiTotalCostValue = GetVariableDefineValue("LeasiTotalCost", iIDPlayer, 0, 0)
                    sDelayPaymeLeaseValue = GetVariableDecisionPlayer(m_IDGame, "DelayPaymeLease", iIDPlayer, 0)
                    sPromoInvesPerioValue = GetVariableDefineValue("PromoInvesPerio", iIDPlayer, 0, 0)
                    sDelayPaymePromoValue = GetVariableDecisionPlayer(m_IDGame, "DelayPaymePromo", iIDPlayer, 0)
                    sExtraMoneyAllowValue = GetVariableDefineValue("ExtraMoneyAllow", iIDPlayer, 0, 0)
                    sForwaPaymeExtraValue = GetVariableDecisionPlayer(m_IDGame, "ForwaPaymeExtra", iIDPlayer, 0)
                    sTechnCostsPerioValule = GetVariableDefineValue("TechnCostsPerio", iIDPlayer, 0, 0)
                    sForwaPaymeAutomValue = GetVariableDecisionPlayer(m_IDGame, "ForwaPaymeAutom", iIDPlayer, 0)
                    sAdditCostsPerioValue = GetVariableDefineValue("AdditCostsPerio", iIDPlayer, 0, 0)
                    sForwaPaymeMktgValue = GetVariableDecisionPlayer(m_IDGame, "ForwaPaymeMktg", iIDPlayer, 0)
                    sCostPurchNICValue = GetVariableDefineValue("CostPurchNIC", iIDPlayer, 0, 0)
                    sPlantPerAgeValue = GetVariableState("PlantPerAge", iIDPlayer, "var_" & sTeamName & "_PlantPerAge_1", 0, 1)
                    sTotalVehicOwnedValue = GetVariableState("TotalVehicOwned", iIDPlayer, "var_" & sTeamName & "_TotalVehicOwned_1", 0, 1)
                    sTotalCostsPersoValue = GetVariableDefineValue("TotalCostsPerso", iIDPlayer, 0, 0)
                    sTotalCostsOutleValue = GetVariableDefineValue("TotalCostsOutle", iIDPlayer, 0, 0)
                    sCreditorsValue = GetVariableState("Creditors", iIDPlayer, "", 0, 0)
                    sTotalNegatInterValue = GetVariableDefineValue("TotalNegatInter", iIDPlayer, 0, 0)
                    sLoansAllowValue = GetVariableDefineValue("LoansAllow", iIDPlayer, 0, 0)
                    sTaxPaymentValue = GetVariableDefineValue("TaxPayment", iIDPlayer, 0, 0)

                    sValore = Nn(sCostPurchEuropValue) * ((Max(m_LunghezzaPeriodo - Nn(sLagPaymeEuropValue) - Nn(sDelayPaymeEuropValue), 0) / m_LunghezzaPeriodo) * Nn(sDiritRitarPagamValue)) _
                            + Nn(sDirecTotalCostsValue) _
                            * ((Max(m_LunghezzaPeriodo - Nn(sLagPaymeProduValue) - Nn(sDelayPaymeProduValue), 0) / m_LunghezzaPeriodo) * Nn(sDiritRitarPagamValue)) _
                            + Nn(sLeasiTotalCostValue) _
                            * ((Max(m_LunghezzaPeriodo - Nn(sLagPaymeLeaseValue) - Nn(sDelayPaymeLeaseValue), 0) / m_LunghezzaPeriodo) * Nn(sDiritRitarPagamValue)) _
                            + Nn(sPromoInvesPerioValue) * ((Max(m_LunghezzaPeriodo - Nn(sLagPaymePromoValue) - Nn(sDelayPaymePromoValue), 0) / m_LunghezzaPeriodo) * Nn(sDiritRitarPagamValue)) _
                            + Nn(sExtraMoneyAllowValue) * ((Min(m_LunghezzaPeriodo, Max(m_LunghezzaPeriodo - Nn(sLagPaymeExtraValue), Nn(sForwaPaymeExtraValue))) / m_LunghezzaPeriodo) * Nn(sDiritScontPagamValue)) _
                            + Nn(sTechnCostsPerioValule) * ((Min(m_LunghezzaPeriodo, Max(m_LunghezzaPeriodo - Nn(sLagPaymeAutomVaue), Nn(sForwaPaymeAutomValue))) / m_LunghezzaPeriodo) * Nn(sDiritScontPagamValue)) _
                            + Nn(sAdditCostsPerioValue) * ((Min(m_LunghezzaPeriodo, Max(m_LunghezzaPeriodo - Nn(sLagPaymeMktgValue), Nn(sForwaPaymeMktgValue))) / m_LunghezzaPeriodo) * Nn(sDiritScontPagamValue)) _
                            + Nn(sCostPurchNICValue) + Nn(sPlantPerAgeValue) * Nn(sNewPlantCostValue) _
                            + Nn(sTotalVehicOwnedValue) * Nn(sNewVehicCostValue) + Nn(sTotalCostsPersoValue) + Nn(sTotalCostsOutleValue) _
                            + Nn(sCreditorsValue) + Nn(sTotalNegatInterValue) + Max(0, -Nn(sLoansAllowValue)) + Max(0, Nn(sTaxPaymentValue))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "DecreBankCash_1105 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Valore finale del prodotto finito
    ''' </summary>
    Private Sub EndRawValue_1106()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "EndRawValue"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String
        Dim iIDItem As Integer
        Dim sItemName As String

        Dim sStockRawMaterValue As String
        Dim sIncreQuantStockValue As String
        Dim sValueRawMaterValue As String
        Dim sIncreValueRawValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        sStockRawMaterValue = GetVariableState("StockRawMater", iIDPlayer, "var_" & sTeamName & "_StockRawMater_" & sItemName, iIDItem, 0)
                        sIncreQuantStockValue = GetVariableDefineValue("IncreQuantStock", iIDPlayer, iIDItem, 0)
                        sValueRawMaterValue = GetVariableState("ValueRawMater", iIDPlayer, "var_" & sTeamName & "_ValueRawMater_" & sItemName, iIDItem, 0)
                        sIncreValueRawValue = GetVariableDefineValue("IncreValueRaw", iIDPlayer, iIDItem, 0)

                        sValore = (Nn(sStockRawMaterValue) + Nn(sIncreQuantStockValue)) * (Nn(sValueRawMaterValue) + Nn(sIncreValueRawValue))

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "EndRawValue_1106 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Variazione dello stock delle materie prime
    ''' </summary>
    Private Sub DeltaRawValue_1107()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DeltaRawValue"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String
        Dim iIDItem As Integer
        Dim sItemName As String

        Dim sBeginRawValueValue As String
        Dim sEndRawValueValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    sValore = "0"

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        sBeginRawValueValue = GetVariableDefineValue("BeginRawValue", iIDPlayer, iIDItem, 0)
                        sEndRawValueValue = GetVariableDefineValue("EndRawValue", iIDPlayer, iIDItem, 0)

                        sValore = Nn(sValore) + Nn(sEndRawValueValue) - Nn(sBeginRawValueValue)

                    Next
                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "DeltaRawValue_1107 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Variazione del magazzino materie 1e
    ''' </summary>
    Private Sub CumulDeltaRaw_1108()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CumulDeltaRaw"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sVariaDeltaRawValue As String
        Dim sDeltaRawValueValue As String
        Dim sDischDeltaRawValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sVariaDeltaRawValue = GetVariableState("VariaDeltaRaw", iIDPlayer, "", 0, 0)
                    sDeltaRawValueValue = GetVariableDefineValue("DeltaRawValue", iIDPlayer, 0, 0)
                    sDischDeltaRawValue = GetVariableDefineValue("DischDeltaRaw", iIDPlayer, 0, 0)

                    sValore = Nn(sVariaDeltaRawValue) + Nn(sDeltaRawValueValue) - Nn(sDischDeltaRawValue)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "CumulDeltaRaw_1108 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Scarica il prezzo precedente ai grossis.
    ''' </summary>
    Private Sub DischPreviPrInt_1109()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DischPreviPrint"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String
        Dim iIDItem As Integer
        Dim sItemName As String

        Dim sPreviInterPriceValue As String
        Dim sInterPriceAllowValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    sValore = "0"

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        sInterPriceAllowValue = GetVariableDefineValue("InterPriceAllow", iIDPlayer, iIDItem, 0)

                        sPreviInterPriceValue = GetVariableState("PreviInterPrice", iIDPlayer, "var_" & sTeamName & "_PreviInterPrice_" & sItemName, iIDItem, 0)
                        If Nn(sInterPriceAllowValue) = 0 Then
                            sValore = 0
                        Else
                            sValore = Nn(sPreviInterPriceValue)
                        End If
                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)

                    Next

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "DischPreviPrInt_1109 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Prezzo medio offerto ai grossisti
    ''' </summary>
    Private Sub AveraPriceOffer_1110()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "AveraPriceOffer"

        Dim sValore As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim sVolumInterOfferValue As String
        Dim sTotalPriceOfferValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowItem As DataRow In m_DataTableItems.Rows
                    iIDItem = Nni(oRowItem("IDVariable"))
                    sItemName = Nz(oRowItem("VariableName"))

                    sVolumInterOfferValue = GetVariableDefineValue("VolumInterOffer", 0, iIDItem, 0)
                    sTotalPriceOfferValue = GetVariableDefineValue("TotalPriceOffer", 0, iIDItem, 0)

                    If Nn(sVolumInterOfferValue) = 0 Then
                        sValore = 0
                    Else
                        sValore = Nn(sTotalPriceOfferValue) / Nn(sVolumInterOfferValue)
                    End If

                    SaveVariableValue(iIDVariabile, 0, iIDItem, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "AveraPriceOffer_1110 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Qualità dell'offerta ai grossisti
    ''' </summary>
    Private Sub IndexGenerOffer_1111()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "IndexGenerOffer"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String
        Dim iIDItem As Integer
        Dim sItemName As String

        Dim sAveraQualiOfferValue As String
        Dim sInterPriceAllowValue As String
        Dim sInterSalesAllowValue As String
        Dim sProduTotalQualiValue As String
        Dim sAveraPriceOfferValue As String
        Dim sTermsPaymeWholeValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    sValore = "0"

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        sAveraQualiOfferValue = GetVariableDefineValue("AveraQualiOffer", 0, iIDItem, 0)
                        sInterPriceAllowValue = GetVariableDefineValue("InterPriceAllow", iIDPlayer, iIDItem, 0)
                        sInterSalesAllowValue = GetVariableDefineValue("InterSalesAllow", iIDPlayer, iIDItem, 0)
                        sProduTotalQualiValue = GetVariableDefineValue("ProduTotalQuali", iIDPlayer, iIDItem, 0)
                        sAveraPriceOfferValue = GetVariableDefineValue("AveraPriceOffer", 0, iIDItem, 0)
                        sTermsPaymeWholeValue = GetVariableDecisionPlayer(m_IDGame, "TermsPaymeWhole", iIDPlayer, 0)

                        If Nn(sAveraQualiOfferValue) = 0 OrElse Nn(sInterPriceAllowValue) = 0 OrElse Nn(sInterSalesAllowValue) = 0 Then
                            sValore = "0"
                        Else
                            sValore = (1 / (Nn(sProduTotalQualiValue) / Nn(sAveraQualiOfferValue) * Nn(sAveraPriceOfferValue) / Nn(sInterPriceAllowValue))) - Max(0, Min(0.09, (Min(3, Nn(sTermsPaymeWholeValue)) * 3) / 100))
                        End If

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)

                    Next

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "IndexGenerOffer_1111 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Offerta migliore (più bassa) a grossisti
    ''' </summary>
    Private Sub MinimIndexGener_1112()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "MinimIndexGener"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String
        Dim iIDItem As Integer
        Dim sItemName As String

        Dim sIndexGenerOfferValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowItem As DataRow In m_DataTableItems.Rows
                    iIDItem = Nni(oRowItem("IDVariable"))
                    sItemName = Nz(oRowItem("VariableName"))

                    sValore = "9999999"

                    For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                        iIDPlayer = Nni(oRowPlayer("IDTeam"))
                        sTeamName = Nz(oRowPlayer("TeamName"))

                        sIndexGenerOfferValue = GetVariableDefineValue("IndexGenerOffer", iIDPlayer, iIDItem, 0)
                        If Nn(sIndexGenerOfferValue) < Nn(sValore) AndAlso Nn(sIndexGenerOfferValue) <> 0 Then
                            sValore = Nn(sIndexGenerOfferValue)
                        End If
                    Next
                    SaveVariableValue(iIDVariabile, 0, iIDItem, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "MinimIndexGener_1112 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Vendite proprie ai grossisti
    ''' </summary>
    Private Sub SalesInterMarke_1113()
        Dim sNomeVariabileEntrata As String = "SalesInterMarke"
        Dim iIDVariabile As Integer

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String
        Dim iIDItem As Integer
        Dim sItemName As String

        Dim dSomma1 As Double
        Dim dSomma2 As Double
        Dim dResto As Double

        Dim sMinimIndexGenerValue As String
        Dim sIndexGenerOfferValue As String
        Dim sInterSalesAllowValue As String
        Dim sInterPriceAllowValue As String
        Dim sQuantRequiInterValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowItem As DataRow In m_DataTableItems.Rows
                    iIDItem = Nni(oRowItem("IDVariable"))
                    sItemName = Nz(oRowItem("VariableName"))

                    dSomma1 = 0
                    dSomma2 = 0
                    dResto = 0

                    sQuantRequiInterValue = GetVariableBoss("QuantRequiInter", "QuantRequiInter_" & sItemName)

                    For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                        iIDPlayer = Nni(oRowPlayer("IDTeam"))
                        sTeamName = Nz(oRowPlayer("TeamName"))

                        sMinimIndexGenerValue = GetVariableDefineValue("MinimIndexGener", iIDPlayer, iIDItem, 0)
                        sIndexGenerOfferValue = GetVariableDefineValue("IndexGenerOffer", iIDPlayer, iIDItem, 0)
                        sInterSalesAllowValue = GetVariableDefineValue("InterSalesAllow", iIDPlayer, iIDItem, 0)
                        sInterPriceAllowValue = GetVariableDefineValue("InterPriceAllow", iIDPlayer, iIDItem, 0)

                        If Nn(sMinimIndexGenerValue) * 1.05203 > Nn(sIndexGenerOfferValue) AndAlso Nn(sInterSalesAllowValue) > 0 AndAlso Nn(sInterPriceAllowValue) > 0 Then
                            dSomma1 += Nn(sInterSalesAllowValue)
                        Else
                            dSomma2 += Nn(sInterSalesAllowValue)
                        End If

                    Next

                    dResto = Max(0, Nn(sQuantRequiInterValue) - dSomma1)

                    For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                        iIDPlayer = Nni(oRowPlayer("IDTeam"))
                        sTeamName = Nz(oRowPlayer("TeamName"))

                        sMinimIndexGenerValue = GetVariableDefineValue("MinimIndexGener", iIDPlayer, iIDItem, 0)
                        sIndexGenerOfferValue = GetVariableDefineValue("IndexGenerOffer", iIDPlayer, iIDItem, 0)
                        sInterSalesAllowValue = GetVariableDefineValue("InterSalesAllow", iIDPlayer, iIDItem, 0)
                        sInterPriceAllowValue = GetVariableDefineValue("InterPriceAllow", iIDPlayer, iIDItem, 0)
                        sQuantRequiInterValue = GetVariableBoss("QuantRequiInter", "QuantRequiInter_" & sItemName)

                        If Nn(sMinimIndexGenerValue) * 1.05203 > Nn(sIndexGenerOfferValue) AndAlso Nn(sInterSalesAllowValue) > 0 AndAlso Nn(sInterPriceAllowValue) > 0 Then
                            If dSomma1 <= Nn(sQuantRequiInterValue) Then
                                sValore = Nn(sInterSalesAllowValue)
                            ElseIf dSomma1 = 0 Then
                                sValore = 0
                            Else
                                sValore = Nn(sInterSalesAllowValue) * Nn(sQuantRequiInterValue) / dSomma1
                            End If

                        ElseIf dSomma2 <= dResto Then
                            sValore = Nn(sInterSalesAllowValue)
                        ElseIf dSomma2 = 0 Then
                            sValore = 0
                        Else
                            sValore = Nn(sInterSalesAllowValue) * dResto / dSomma2
                        End If

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "SalesInterMarke_1113 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Vendite totali al mercato dei grossisti
    ''' </summary>
    Private Sub GlobaInterSales_1114()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "GlobaInterSales"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String
        Dim iIDItem As Integer
        Dim sItemName As String

        Dim sSalesInterMarkeValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowItem As DataRow In m_DataTableItems.Rows
                    iIDItem = Nni(oRowItem("IDVariable"))
                    sItemName = Nz(oRowItem("VariableName"))

                    sValore = "0"

                    For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                        iIDPlayer = Nni(oRowPlayer("IDTeam"))
                        sTeamName = Nz(oRowPlayer("TeamName"))

                        sSalesInterMarkeValue = GetVariableDefineValue("SalesInterMarke", iIDPlayer, iIDItem, 0)

                        sValore = Nn(sValore) + Nn(sSalesInterMarkeValue)

                        SaveVariableValue(iIDVariabile, 0, iIDItem, 0, sValore)
                    Next
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "GlobaInterSales_1114 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Indice di attrattività
    ''' </summary>
    Private Sub IndexOfAttraction_1115()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "IndexOfAttraction"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String
        Dim dSum As Double

        Dim sAttractionIndexValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    dSum += Nn(GetVariableDefineValue("AttractionIndex", iIDPlayer, 0, 0))
                Next

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    If dSum = 0 Then
                        sValore = "0"
                    Else
                        sAttractionIndexValue = GetVariableDefineValue("AttractionIndex", iIDPlayer, 0, 0)
                        sValore = Nn(sAttractionIndexValue) / (dSum / m_DataTablePlayers.Rows.Count)
                    End If
                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "IndexOfAttraction_1115 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Valore dello stock materie prime
    ''' </summary>
    Private Sub RawEndPerio_1116()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "RawEndPerio"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    sValore = "0"

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        sValore = Nn(sValore) + Nn(GetVariableDefineValue("EndRawValue", iIDPlayer, Nni(oRowItem("IDVariable")), 0))
                    Next

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "RawEndPerio_1116 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    Private Sub ProduRevenInter_1117()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "ProduRevenInter"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    sValore = "0"

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        sValore = Nn(GetVariableDefineValue("SalesInterMarke", iIDPlayer, Nni(oRowItem("IDVariable")), 0)) *
                                  Nn(GetVariableDefineValue("InterPriceAllow", iIDPlayer, Nni(oRowItem("IDVariable")), 0))

                        SaveVariableValue(iIDVariabile, iIDPlayer, Nni(oRowItem("IDVariable")), 0, sValore)
                    Next
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "ProduRevenInter_1117 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Fatturato dai grossisti - Totale
    ''' </summary>
    Private Sub TotalRevenInter_1118()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TotalRevenInter"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sProduRevenInterValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    sValore = "0"
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        Dim iIDItem As Integer = Nni(oRowItem("IDVariable"))

                        sProduRevenInterValue = GetVariableDefineValue("ProduRevenInter", iIDPlayer, iIDItem, 0)
                        sValore = Nn(sValore) + Nn(sProduRevenInterValue)

                    Next
                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "TotalRevenInter_1118 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Capacità di trasporto necessaria
    ''' </summary>
    Private Sub TotalTransRequi_1119()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TotalTransRequi"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String
        Dim iIDItem As Integer

        Dim sProduRequeValue As String
        Dim sSalesInterMarkeValue As String
        Dim sCapacTransProduValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = "0"

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))

                        sProduRequeValue = GetVariableDefineValue("ProduReque", iIDPlayer, iIDItem, 0)
                        sSalesInterMarkeValue = GetVariableDefineValue("SalesInterMarke", iIDPlayer, iIDItem, 0)
                        sCapacTransProduValue = GetVariableBoss("CapacTransProdu", "CapacTransProdu_" & Nz(oRowItem("VariableName")))

                        sValore = Nn(sValore) + (Nn(sProduRequeValue) - Nn(sSalesInterMarkeValue)) * Nn(sCapacTransProduValue)
                    Next

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "TotalTransRequi_1119 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Trasporti esterni utilizzati
    ''' </summary>
    Private Sub ExtraTransNeede_1120()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "ExtraTransNeede"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sTotalCapacTransValue As String
        Dim sTotalTransRequiValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sTotalCapacTransValue = GetVariableDefineValue("TotalCapacTrans", iIDPlayer, 0, 0)
                    sTotalTransRequiValue = GetVariableDefineValue("TotalTransRequi", iIDPlayer, 0, 0)

                    If Nn(sTotalCapacTransValue) > Nn(sTotalTransRequiValue) Then
                        sValore = 0
                    Else
                        sValore = Nn(sTotalTransRequiValue) - Nn(sTotalCapacTransValue)
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "ExtraTransNeede_1120 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

#End Region

#Region "241 - 260 FUNZIONI DI CALCOLO"

    ''' <summary>
    '''  Quantità deisponibile per vendite dettaglio
    ''' </summary>
    Private Sub TotalOfferCusto_1201()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TotalOfferCusto"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim sExtraAllowValue As String
        Dim sProduRequeValue As String
        Dim sSalesInterMarkeValue As String
        Dim sStockFinisGoodsValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        sExtraAllowValue = GetVariableDefineValue("ExtraAllow", iIDPlayer, iIDItem, 0)
                        sProduRequeValue = GetVariableDefineValue("ProduReque", iIDPlayer, iIDItem, 0)
                        sSalesInterMarkeValue = GetVariableDefineValue("SalesInterMarke", iIDPlayer, iIDItem, 0)
                        sStockFinisGoodsValue = GetVariableState("StockFinisGoods", iIDPlayer, "var_" & sTeamName & "_StockFinisGoods_" & sItemName, iIDItem, 0)

                        sValore = Nn(sExtraAllowValue) + Nn(sProduRequeValue) - Nn(sSalesInterMarkeValue) + Nn(sStockFinisGoodsValue)

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "TotalOfferCusto_1201 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Indice medio di attrazione
    ''' </summary>
    Private Sub AveraAttraIndex_1202()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "AveraAttraIndex"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String
        Dim dSomma As Double

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    dSomma += Nn(GetVariableDefineValue("IndexOfAttraction", iIDPlayer, 0, 0))
                Next

                sValore = dSomma / m_DataTablePlayers.Rows.Count

                SaveVariableValue(iIDVariabile, 0, 0, 0, sValore)
            End If

        Catch ex As Exception
            g_MessaggioErrore &= "AveraAttraIndex_1202 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try

    End Sub

    ''' <summary>
    ''' Penalità sull'attrattività
    ''' </summary>
    Private Sub ThresAttraOffer_1203()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "ThresAttraOffer"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sIndexOfAttractionValue As String
        Dim sAveraAttraIndexValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sIndexOfAttractionValue = GetVariableDefineValue("IndexOfAttraction", iIDPlayer, 0, 0)
                    sAveraAttraIndexValue = GetVariableDefineValue("AveraAttraIndex", 0, 0, 0)

                    If Nn(sIndexOfAttractionValue) >= Nn(sAveraAttraIndexValue) Then
                        sValore = "0"
                    Else
                        If Nn(sIndexOfAttractionValue) >= Nn(sAveraAttraIndexValue) * 0.9 Then
                            sValore = GetVariableBoss("PenalThresOne", "")
                        Else
                            sValore = GetVariableBoss("PenalThresTwo", "")
                        End If
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "ThresAttraOffer_1203 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Costo dei trasportri interni
    ''' </summary>
    Private Sub InterTransCosts_1204()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "InterTransCosts"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sTotalCapacTransValue As String
        Dim sTotalTransRequiValue As String
        Dim sTransUnitCostValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sTotalCapacTransValue = GetVariableDefineValue("TotalCapacTrans", iIDPlayer, 0, 0)
                    sTotalTransRequiValue = GetVariableDefineValue("TotalTransRequi", iIDPlayer, 0, 0)
                    sTransUnitCostValue = GetVariableBoss("TransUnitCost", "TransUnitCost_" & sTeamName)

                    sValore = Min(Nn(sTotalCapacTransValue), Nn(sTotalTransRequiValue)) * Nn(sTransUnitCostValue)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "InterTransCosts_1204 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Costo dei trasporti esterni
    ''' </summary>
    Private Sub ExterTransCosts_1205()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "ExterTransCosts"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sExtraTransNeedeValue As String
        Dim sTransUnitCostValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sExtraTransNeedeValue = GetVariableDefineValue("ExtraTransNeede", iIDPlayer, 0, 0)
                    sTransUnitCostValue = GetVariableBoss("TransUnitCost", "TransUnitCost_" & sTeamName)

                    sValore = Nn(sExtraTransNeedeValue) * Nn(sTransUnitCostValue) * 2.5

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

                ' 2.5 E'LA MAGGIORAZIONE SUI COSTI DI TRASPORTO UNITARI INTERNI  
                ' IN MODO CHE FINO A META' CARICO CONVENGA USARE VETTORI ESTERNI 

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "ExterTransCosts_1205 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Costi di trasporto totali
    ''' </summary>
    Private Sub TotalTransCosts_1206()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TotalTransCosts"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = Nn(GetVariableDefineValue("InterTransCosts", iIDPlayer, 0, 0)) + Nn(GetVariableDefineValue("ExterTransCosts", iIDPlayer, 0, 0))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "TotalTransCosts_1206 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Trasporti
    ''' </summary>
    Private Sub CumulTransCosts_1207()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CumulTransCosts"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sVariaTransCostsValue As String
        Dim sTotalTransCostsValue As String
        Dim sDischTransCostsValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"
                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sVariaTransCostsValue = GetVariableState("VariaTransCosts", iIDPlayer, "", 0, 0)
                    sTotalTransCostsValue = GetVariableDefineValue("TotalTransCosts", iIDPlayer, 0, 0)
                    sDischTransCostsValue = GetVariableDefineValue("DischTransCosts", iIDPlayer, 0, 0)

                    sValore = Nn(sVariaTransCostsValue) + Nn(sTotalTransCostsValue) - Nn(sDischTransCostsValue)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "CumulTransCosts_1207 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Rotazione richiesta dalla merce disponibile
    ''' </summary>
    Private Sub TotalVolumSpace_1208()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TotalVolumSpace"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim sTotalOfferCustoValue As String
        Dim sCapacTransProduValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    sValore = "0"

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        sTotalOfferCustoValue = GetVariableDefineValue("TotalOfferCusto", iIDPlayer, iIDItem, 0)
                        sCapacTransProduValue = GetVariableBoss("CapacTransProdu", "CapacTransProdu_" & sItemName)

                        sValore = Nn(sValore) + Nn(sTotalOfferCustoValue) * Nn(sCapacTransProduValue)
                    Next

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "TotalVolumSpace_1208 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Staff necessario
    ''' </summary>
    Private Sub OttimGoodsStaff_1209()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "OttimGoodsStaff"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim TotalVolumSpace As String
        Dim StaffNecesCentr As String = GetVariableBoss("StaffNecesCentr", "")
        Dim TotalCentrStore As String
        Dim CentrStoreAllow As String
        Dim StaffNecesPerif As String = GetVariableBoss("StaffNecesPerif", "")
        Dim TotalPerifStore As String
        Dim PerifStoreAllow As String
        Dim MinimTotalStaff As String = GetVariableBoss("MinimTotalStaff", "")

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    TotalVolumSpace = GetVariableDefineValue("TotalVolumSpace", iIDPlayer, 0, 0)
                    TotalCentrStore = GetVariableState("TotalCentrStore", iIDPlayer, "", 0, 0)
                    CentrStoreAllow = GetVariableDefineValue("CentrStoreAllow", iIDPlayer, 0, 0)
                    TotalPerifStore = GetVariableState("TotalPerifStore", iIDPlayer, "", 0, 0)
                    PerifStoreAllow = GetVariableDefineValue("PerifStoreAllow", iIDPlayer, 0, 0)
                    sValore = Truncate(Max(Nn(MinimTotalStaff), 0.00002 * Nn(TotalVolumSpace) + Nn(StaffNecesCentr) * Nn(TotalCentrStore) + Nn(CentrStoreAllow) _
                            + Nn(StaffNecesPerif) * Nn(TotalPerifStore) + Nn(PerifStoreAllow)))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "OttimGoodsStafF -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Venditori necessari
    ''' </summary>
    Private Sub OttimPointPerso_1210()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "OttimPointPerso"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim TotalVolumSpace As String
        Dim PersoNecesCentr As String = GetVariableBoss("PersoNecesCentr", "")
        Dim TotalCentrStore As String
        Dim CentrStoreAllow As String
        Dim CentrAlienAllow As String
        Dim PersoNecesPerif As String = GetVariableBoss("PersoNecesPerif", "")
        Dim TotalPerifStore As String
        Dim PerifStoreAllow As String
        Dim PerifScrapAllow As String
        Dim RateSalesVolum As String = GetVariableBoss("RateSalesVolum", "")

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    TotalVolumSpace = GetVariableDefineValue("TotalVolumSpace", iIDPlayer, 0, 0)
                    TotalCentrStore = GetVariableState("TotalCentrStore", iIDPlayer, "", 0, 0)
                    CentrStoreAllow = GetVariableDefineValue("CentrStoreAllow", iIDPlayer, 0, 0)
                    CentrAlienAllow = GetVariableDefineValue("CentrAlienAllow", iIDPlayer, 0, 0)
                    TotalPerifStore = GetVariableState("TotalPerifStore", iIDPlayer, "", 0, 0)
                    PerifStoreAllow = GetVariableDefineValue("PerifStoreAllow", iIDPlayer, 0, 0)
                    PerifScrapAllow = GetVariableDefineValue("PerifScrapAllow", iIDPlayer, 0, 0)

                    sValore = Truncate(Nn(TotalVolumSpace) * Nn(RateSalesVolum) + Nn(PersoNecesCentr) * (Nn(TotalCentrStore) + Nn(CentrStoreAllow) - Nn(CentrAlienAllow)) _
                            + Nn(PersoNecesPerif) * (Nn(TotalPerifStore) + Nn(PerifStoreAllow) - Nn(PerifScrapAllow)))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "OttimPointPerso_1210 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Tasso di supervisori in staff
    ''' </summary>
    Private Sub RateGoodsStaff_1211()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "RateGoodsStaff"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sOttimGoodsStaffValue As String
        Dim sTotalStaffPersoValue As String
        Dim sNewStaffAllowValue As String
        Dim sStaffTrainLevelValue As String
        Dim sIncreStaffLevelValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sOttimGoodsStaffValue = GetVariableDefineValue("OttimGoodsStaff", iIDPlayer, 0, 0)
                    sTotalStaffPersoValue = GetVariableState("TotalStaffPerso", iIDPlayer, "", 0, 0)
                    sNewStaffAllowValue = GetVariableDefineValue("NewStaffAllow", iIDPlayer, 0, 0)
                    sStaffTrainLevelValue = GetVariableState("StaffTrainLevel", iIDPlayer, "", 0, 0)
                    sIncreStaffLevelValue = GetVariableDefineValue("IncreStaffLevel", iIDPlayer, 0, 0)

                    If Nn(sOttimGoodsStaffValue) = 0 Then
                        sValore = "0"
                    Else
                        sValore = ((Nn(sTotalStaffPersoValue) + Nn(sNewStaffAllowValue)) * (Nn(sStaffTrainLevelValue) + Nn(sIncreStaffLevelValue)) / 100) / Nn(sOttimGoodsStaffValue)
                    End If

                    ' Forzo il valore a 2.00 come da richiesta di Fulvio con mail del 27/10/2017, modifica valida solo per il mese di novembre richiesta da Karen
                    If Now.Month = 11 AndAlso Now.Year = 2017 Then
                        SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, "2")
                    Else
                        SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                    End If

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "RateGoodsStaff_1211 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Tasso dei commessi
    ''' </summary>
    Private Sub RateGarriPoint_1212()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "RateGarriPoint"

        Dim RateGarriPoint As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim OttimPointPerso As Double
        Dim TotalPointPerso As Double
        Dim NewPointAllow As Double
        Dim PointTrainLevel As Double
        Dim IncrePointLevel As Double

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    OttimPointPerso = Nn(GetVariableDefineValue("OttimPointPerso", iIDPlayer, 0, 0))
                    TotalPointPerso = Nn(GetVariableState("TotalPointPerso", iIDPlayer, "", 0, 0))
                    NewPointAllow = Nn(GetVariableDefineValue("NewPointAllow", iIDPlayer, 0, 0))
                    PointTrainLevel = Nn(GetVariableState("PointTrainLevel", iIDPlayer, "", 0, 0))
                    IncrePointLevel = Nn(GetVariableDefineValue("IncrePointLevel", iIDPlayer, 0, 0))

                    If Nn(OttimPointPerso) = 0 Then
                        RateGarriPoint = 0
                    Else
                        RateGarriPoint = ((TotalPointPerso + NewPointAllow) * (PointTrainLevel + IncrePointLevel) / 100) / OttimPointPerso
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, RateGarriPoint)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "RateGarriPoint_1212 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Efficiacia dello staff
    ''' </summary>
    Private Sub TableEffecStaff_1213()
        Dim iIDVariabile As Integer
        Dim dValueCalc As String = ""
        Dim sNomeVariabileCalc As String = "TableEffecStaff"
        Dim sNomeVariabileEntrata As String = "RateGoodsStaff"
        Dim iIDPlayer As Integer = 0

        Try
            ' Ciclo sui players presenti nel game
            If m_DataTablePlayers.Rows.Count > 0 Then
                ' Controllo la presenza della variabile da calcolare e mi faccio ritornare l'id 
                iIDVariabile = CheckVariableExists(sNomeVariabileCalc, m_IDGame)
                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))

                    dValueCalc = GetVariableDefineValue(sNomeVariabileEntrata, iIDPlayer, 0, 0)
                    dValueCalc = CalculateInterpolation(sNomeVariabileCalc, dValueCalc)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, Nz(dValueCalc))
                Next
            End If

        Catch ex As Exception
            g_MessaggioErrore &= "TableEffecStaff_1213 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Efficacia dei venditori
    ''' </summary>
    Private Sub TableEffecPoint_1214()
        Dim iIDVariabile As Integer
        Dim dValueCalc As String = ""
        Dim sNomeVariabileCalc As String = "TableEffecPoint"
        Dim sNomeVariabileEntrata As String = "RateGarriPoint"
        Dim iIDPlayer As Integer = 0

        Try
            ' Ciclo sui players presenti nel game
            If m_DataTablePlayers.Rows.Count > 0 Then
                ' Controllo la presenza della variabile da calcolare e mi faccio ritornare l'id 
                iIDVariabile = CheckVariableExists(sNomeVariabileCalc, m_IDGame)
                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))

                    dValueCalc = GetVariableDefineValue(sNomeVariabileEntrata, iIDPlayer, 0, 0)
                    dValueCalc = CalculateInterpolation(sNomeVariabileCalc, dValueCalc)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, dValueCalc)
                Next
            End If

        Catch ex As Exception
            g_MessaggioErrore &= "TableEffecPoint_1214 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Quantità massima vendibile al dettaglio
    ''' </summary>
    Private Sub MaximSellaGoods_1215()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "MaximSellaGoods"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim TotalVolumSpace As String
        Dim MaximSpaceAvail As String
        Dim TableEffecStaff As String
        Dim TableEffecPoint As String
        Dim TotalOfferCusto As String
        Dim LimitMaximSales As String = GetVariableBoss("LimitMaximSales", "")

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    TotalVolumSpace = GetVariableDefineValue("TotalVolumSpace", iIDPlayer, 0, 0)
                    MaximSpaceAvail = GetVariableDefineValue("MaximSpaceAvail", iIDPlayer, 0, 0)
                    TableEffecStaff = GetVariableDefineValue("TableEffecStaff", iIDPlayer, 0, 0)
                    TableEffecPoint = GetVariableDefineValue("TableEffecPoint", iIDPlayer, 0, 0)

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        TotalOfferCusto = GetVariableDefineValue("TotalOfferCusto", iIDPlayer, iIDItem, 0)

                        If Nn(TotalVolumSpace) <= Nn(MaximSpaceAvail) * Nn(TableEffecStaff) / 100 * Nn(TableEffecPoint) / 100 Then
                            sValore = Truncate(Min(Nn(TotalOfferCusto) * Nn(LimitMaximSales), Nn(TotalOfferCusto) * Min(Nn(TableEffecStaff), Nn(TableEffecPoint)) / 100))
                        Else
                            sValore = Truncate(Min(Nn(TotalOfferCusto) * Nn(LimitMaximSales), Nn(TotalOfferCusto) * Min(Nn(TableEffecStaff), Nn(TableEffecPoint)) / 100 * (Nn(MaximSpaceAvail) / Nn(TotalVolumSpace))))
                        End If

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "MaximSellaGoods_1215 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Ratio Vendite massime / quantità disponibile
    ''' </summary>
    Private Sub RatioMaximAvail_1216()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "RatioMaximAvail"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim sTotalOfferCustoValue As String
        Dim sMaximSellaGoodsValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        sTotalOfferCustoValue = GetVariableDefineValue("TotalOfferCusto", iIDPlayer, iIDItem, 0)
                        sMaximSellaGoodsValue = GetVariableDefineValue("MaximSellaGoods", iIDPlayer, iIDItem, 0)

                        If Nn(sTotalOfferCustoValue) = 0 Then
                            sValore = 0
                        Else
                            sValore = (Nn(sMaximSellaGoodsValue) / Nn(sTotalOfferCustoValue)) * 100
                        End If

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "RatioMaximAvail_1216 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Prezzo medio di mercato
    ''' </summary>
    Private Sub CompoMarkePrice_1217()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CompoMarkePrice"

        Dim CompoMarkePrice As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim dOffers As Double

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                CompoMarkePrice = "0"
                For Each oRowItem As DataRow In m_DataTableItems.Rows
                    iIDItem = Nni(oRowItem("IDVariable"))
                    sItemName = Nz(oRowItem("VariableName"))

                    dOffers = 0
                    For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                        iIDPlayer = Nni(oRowPlayer("IDTeam"))
                        sTeamName = Nz(oRowPlayer("TeamName"))
                        dOffers += GetVariableDefineValue("MaximSellaGoods", iIDPlayer, iIDItem, 0)
                    Next

                    CompoMarkePrice = "0"
                    For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                        iIDPlayer = Nni(oRowPlayer("IDTeam"))
                        CompoMarkePrice = Nn(CompoMarkePrice) + (Nn(GetVariableDefineValue("MaximSellaGoods", iIDPlayer, iIDItem, 0)) *
                                          Nn(GetVariableDefineValue("SalesPriceAllow", iIDPlayer, iIDItem, 0)))
                    Next
                    If dOffers = 0 Then
                        CompoMarkePrice = "0"
                    Else
                        CompoMarkePrice = Nn(CompoMarkePrice) / dOffers
                    End If

                    SaveVariableValue(iIDVariabile, 0, iIDItem, 0, CompoMarkePrice)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "CompoMarkePrice_1217 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Qualità media di mercato
    ''' </summary>
    Private Sub CompoMarkeQuali_1218()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CompoMarkeQuali"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim dOffer As Double

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowItem As DataRow In m_DataTableItems.Rows
                    iIDItem = Nni(oRowItem("IDVariable"))
                    sItemName = Nz(oRowItem("VariableName"))

                    dOffer = 0

                    For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                        iIDPlayer = Nni(oRowPlayer("IDTeam"))
                        sTeamName = Nz(oRowPlayer("TeamName"))
                        dOffer += GetVariableDefineValue("MaximSellaGoods", iIDPlayer, iIDItem, 0)
                    Next

                    sValore = "0"
                    For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                        iIDPlayer = Nni(oRowPlayer("IDTeam"))
                        sValore = Nn(sValore) + (Nn(GetVariableDefineValue("QualiGoodsOffer", iIDPlayer, iIDItem, 0)) *
                                  Nn(GetVariableDefineValue("MaximSellaGoods", iIDPlayer, iIDItem, 0)))
                    Next
                    If dOffer = 0 Then
                        sValore = "0"
                    Else
                        sValore = Nn(sValore) / dOffer
                    End If

                    SaveVariableValue(iIDVariabile, 0, iIDItem, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "CompoMarkeQuali_1218 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Prezzo scontato offerto
    ''' </summary>
    Private Sub DiscoPriceOffer_1219()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DiscoPriceOffer"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String
        Dim sMaximSellaGoodsValue As String
        Dim sSalesPriceAllowValue As String
        Dim sNomeVariabileLista As String
        Dim sPreviSalesPriceValue As String
        Dim sMarkeExpenAllowValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        sMaximSellaGoodsValue = GetVariableDefineValue("MaximSellaGoods", iIDPlayer, Nni(oRowItem("IDVariable")), 0)
                        sSalesPriceAllowValue = GetVariableDefineValue("SalesPriceAllow", iIDPlayer, Nni(oRowItem("IDVariable")), 0)

                        sNomeVariabileLista = "var_" & sTeamName & "_PreviSalesPrice_" & Nz(oRowItem("VariableName"))
                        sPreviSalesPriceValue = GetVariableState("PreviSalesPrice", iIDPlayer, sNomeVariabileLista, Nni(oRowItem("IDVariable")), 0)

                        sMarkeExpenAllowValue = GetVariableDefineValue("MarkeExpenAllow", iIDPlayer, Nni(oRowItem("IDVariable")), 0)

                        If Nn(sMaximSellaGoodsValue) = 0 OrElse Nn(sSalesPriceAllowValue) = 0 Then
                            sValore = "0"
                        Else
                            sValore = Max(Nn(sPreviSalesPriceValue) * 0.5, Max(Nn(sSalesPriceAllowValue) * 0.75, (Nn(sMaximSellaGoodsValue) * Nn(sSalesPriceAllowValue) - Nn(sMarkeExpenAllowValue)) / Nn(sMaximSellaGoodsValue)))
                        End If

                        SaveVariableValue(iIDVariabile, iIDPlayer, Nni(oRowItem("IDVariable")), 0, sValore)
                    Next

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "DiscoPriceOffer_1219 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Prezo scontato dalle promozioni
    ''' </summary>
    Private Sub CompoDiscoPrice_1220()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CompoDiscoPrice"

        Dim sValore As String

        Dim iIDPlayer As Integer

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim dOffer As Double

        Dim sMaximSellaGoodsValue As String
        Dim sSalesPriceAllowValue As String
        Dim sMarkeExpenAllowValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowItem As DataRow In m_DataTableItems.Rows
                    iIDItem = Nni(oRowItem("IDVariable"))
                    sItemName = Nz(oRowItem("VariableName"))

                    dOffer = 0

                    For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                        iIDPlayer = Nni(oRowPlayer("IDTeam"))
                        sMaximSellaGoodsValue = GetVariableDefineValue("MaximSellaGoods", iIDPlayer, iIDItem, 0)
                        dOffer += Nn(sMaximSellaGoodsValue)
                    Next

                    sValore = "0"
                    For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                        iIDPlayer = Nni(oRowPlayer("IDTeam"))
                        sMaximSellaGoodsValue = GetVariableDefineValue("MaximSellaGoods", iIDPlayer, iIDItem, 0)
                        sSalesPriceAllowValue = GetVariableDefineValue("SalesPriceAllow", iIDPlayer, Nni(oRowItem("IDVariable")), 0)
                        sMarkeExpenAllowValue = GetVariableDefineValue("MarkeExpenAllow", iIDPlayer, Nni(oRowItem("IDVariable")), 0)

                        sValore = Nn(sValore) + Nn(sMaximSellaGoodsValue) * Nn(sSalesPriceAllowValue) - Nn(sMarkeExpenAllowValue)
                    Next
                    If dOffer = 0 Then
                        sValore = "0"
                    Else
                        sValore = Nn(sValore) / dOffer
                    End If

                    SaveVariableValue(iIDVariabile, 0, iIDItem, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "CompoDiscoPrice_1220 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

#End Region

#Region "261 - 280 FUNZIONI DI CALCOLO"

    ''' <summary>
    ''' Penalità sulla qualità
    ''' </summary>
    Private Sub ThresQualiOffer_1301()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "ThresQualiOffer"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sProduTotalQualiValue As String
        Dim sCompoMarkeQualiValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        sProduTotalQualiValue = GetVariableDefineValue("ProduTotalQuali", iIDPlayer, Nni(oRowItem("IDVariable")), 0)
                        sCompoMarkeQualiValue = GetVariableDefineValue("CompoMarkeQuali", 0, Nni(oRowItem("IDVariable")), 0)

                        If Nn(sProduTotalQualiValue) >= Nn(sCompoMarkeQualiValue) Then
                            sValore = 0
                        Else
                            If Nn(sProduTotalQualiValue) >= Nn(sCompoMarkeQualiValue) * 0.9 Then
                                sValore = GetVariableBoss("PenalThresOne", "")
                            Else
                                sValore = GetVariableBoss("PenalThresTwo", "")
                            End If
                        End If

                        SaveVariableValue(iIDVariabile, iIDPlayer, Nni(oRowItem("IDVariable")), 0, sValore)
                    Next

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "ThresQualiOffer_1301 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Penalità sul prezzo
    ''' </summary>
    Private Sub ThresPriceOffer_1302()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "ThresPriceOffer"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sDiscoPriceOfferValue As String
        Dim sCompoDiscoPriceValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        sDiscoPriceOfferValue = GetVariableDefineValue("DiscoPriceOffer", iIDPlayer, Nni(oRowItem("IDVariable")), 0)
                        sCompoDiscoPriceValue = GetVariableDefineValue("CompoDiscoPrice", 0, Nni(oRowItem("IDVariable")), 0)

                        If Nn(sDiscoPriceOfferValue) <= Nn(sCompoDiscoPriceValue) Then
                            sValore = 0
                        Else
                            If Nn(sDiscoPriceOfferValue) <= Nn(sCompoDiscoPriceValue) * 1.1 Then
                                sValore = GetVariableBoss("PenalThresOne", "")
                            Else
                                sValore = GetVariableBoss("PenalThresTwo", "")
                            End If
                        End If
                        SaveVariableValue(iIDVariabile, iIDPlayer, Nni(oRowItem("IDVariable")), 0, sValore)
                    Next

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "ThresPriceOffer_1302 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Domanda di mercato
    ''' </summary>
    Private Sub MarketDemand_1303()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "MarketDemand"

        Dim sValore As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim CompoMarkeQuali As String
        Dim AveraOfferBegin As String
        Dim ImporCompoQuali As String = GetVariableBoss("ImporCompoQuali", "")
        Dim MarkeDemanElast As String
        Dim CompoMarkePrice As String
        Dim AveraPriceBegin As String
        Dim EconoTrendProdu As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowItem As DataRow In m_DataTableItems.Rows
                    iIDItem = Nni(oRowItem("IDVariable"))
                    sItemName = Nz(oRowItem("VariableName"))

                    CompoMarkeQuali = GetVariableDefineValue("CompoMarkeQuali", 0, iIDItem, 0)
                    AveraOfferBegin = GetVariableDeveloper(m_IDGame, "AveraOfferBegin", 0, iIDItem)
                    MarkeDemanElast = GetVariableBoss("MarkeDemanElast", "MarkeDemanElast_" & sItemName)
                    CompoMarkePrice = GetVariableDefineValue("CompoMarkePrice", 0, iIDItem, 0)
                    AveraPriceBegin = GetVariableDeveloper(m_IDGame, "AveraPriceBegin", 0, iIDItem)
                    EconoTrendProdu = GetVariableBoss("EconoTrendProdu", "EconoTrendProdu_" & sItemName)

                    sValore = ((Nn(CompoMarkeQuali) - 60.0) * Nn(AveraOfferBegin) * Nn(ImporCompoQuali) / 100 _
                            + Nn(AveraOfferBegin) * m_DataTablePlayers.Rows.Count - Nn(MarkeDemanElast) _
                            * (Nn(CompoMarkePrice) - Nn(AveraPriceBegin)) / Nn(AveraPriceBegin) _
                            * Nn(AveraOfferBegin)) * Nn(EconoTrendProdu)

                    SaveVariableValue(iIDVariabile, 0, iIDItem, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "MarketDemand_1303 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Indice di competitività
    ''' </summary>
    Private Sub IndexMarkeOffer_1304()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "IndexMarkeOffer"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String
        Dim iIDItem As Integer

        Dim sCompoMarkeQualiValue As String
        Dim sDiscoPriceOfferValue As String
        Dim sImporCompoQualiValue As String = GetVariableBoss("ImporCompoQuali", "")
        Dim sImporCompoPriceValue As String = GetVariableBoss("ImporCompoPrice", "")
        Dim sImporCompoAttraValue As String = GetVariableBoss("ImporCompoAttra", "")
        Dim sAveraAttraIndexValue As String
        Dim sCompoDiscoPriceValue As String
        Dim sIndexOfAttractionValue As String
        Dim sCumulDeltaSalesValue As String
        Dim sQualiGoodsOfferValue As String
        Dim sThresQualiOfferValue As String
        Dim sThresPriceOfferValue As String
        Dim sThresAttraOfferValule As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"
                sAveraAttraIndexValue = GetVariableDefineValue("AveraAttraIndex", 0, 0, 0)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))

                        sCompoMarkeQualiValue = GetVariableDefineValue("CompoMarkeQuali", 0, iIDItem, 0)
                        sDiscoPriceOfferValue = GetVariableDefineValue("DiscoPriceOffer", iIDPlayer, iIDItem, 0)
                        sCompoDiscoPriceValue = GetVariableDefineValue("CompoDiscoPrice", 0, iIDItem, 0)
                        sIndexOfAttractionValue = GetVariableDefineValue("IndexOfAttraction", iIDPlayer, 0, 0)
                        sCumulDeltaSalesValue = GetVariableState("CumulDeltaSales", iIDPlayer, "var_" & sTeamName & "_CumulDeltaSales_" & Nz(oRowItem("VariableName")), iIDItem, 0)
                        sQualiGoodsOfferValue = GetVariableDefineValue("QualiGoodsOffer", iIDPlayer, iIDItem, 0)
                        sThresQualiOfferValue = GetVariableDefineValue("ThresQualiOffer", iIDPlayer, iIDItem, 0)
                        sThresPriceOfferValue = GetVariableDefineValue("ThresPriceOffer", iIDPlayer, iIDItem, 0)
                        sThresAttraOfferValule = GetVariableDefineValue("ThresAttraOffer", iIDPlayer, 0, 0)

                        If Nn(sCompoMarkeQualiValue) = 0 OrElse Nn(sDiscoPriceOfferValue) = 0 OrElse (Nn(sImporCompoQualiValue) + Nn(sImporCompoPriceValue) + Nn(sImporCompoAttraValue)) = 0 OrElse Nn(sAveraAttraIndexValue) = 0 Then
                            ' AVOID DIVISIONS BY ZERO 
                            ' DIVISION BY ZERO 
                            sValore = 0

                        ElseIf Nn(sDiscoPriceOfferValue) > Nn(sCompoDiscoPriceValue) * 1.9 Then
                            ' CASE 2: PRICE IS OVER 190% AVERAGE PRICE 
                            sValore = Min(100, (+Pow(Nn(sQualiGoodsOfferValue) / Nn(sCompoMarkeQualiValue), 2.0) * Nn(sImporCompoQualiValue) + (+Pow(Nn(sCompoDiscoPriceValue) / Max(0.1, Nn(sDiscoPriceOfferValue)), 2.0) - Nn(sCumulDeltaSalesValue) / m_CurrentStep / Nn(sCompoDiscoPriceValue)) * Nn(sImporCompoPriceValue) + Pow(Nn(sIndexOfAttractionValue) / Max(0.1, Nn(sAveraAttraIndexValue)), 2.0) * Nn(sImporCompoAttraValue)) / (Nn(sImporCompoQualiValue) + Nn(sImporCompoPriceValue) + Nn(sImporCompoAttraValue)) * 0.05)

                        ElseIf Nn(sDiscoPriceOfferValue) >= Nn(sCompoDiscoPriceValue) * 1.4 AndAlso Nn(sDiscoPriceOfferValue) <= Nn(sCompoDiscoPriceValue) * 1.9 Then
                            ' CASE 3: PRICE IS BETWEEN 140% AND 190% 
                            sValore = Min(100, (+Pow(Nn(sQualiGoodsOfferValue) / Nn(sCompoMarkeQualiValue), 3.0) * Nn(sImporCompoQualiValue) + (+Pow(Nn(sCompoDiscoPriceValue) / Max(0.1, Nn(sDiscoPriceOfferValue)), 6.0) - Nn(sCumulDeltaSalesValue) / m_CurrentStep / Nn(sCompoDiscoPriceValue)) * Nn(sImporCompoPriceValue) + Pow(Nn(sIndexOfAttractionValue) / Max(0.1, Nn(sAveraAttraIndexValue)), 3.0) * Nn(sImporCompoAttraValue)) / (Nn(sImporCompoQualiValue) + Nn(sImporCompoPriceValue) + Nn(sImporCompoAttraValue)) * 0.4)

                        Else
                            ' CASE 4: PRICE IS UNDER 140% 
                            sValore = Min(100, (+Pow(Nn(sQualiGoodsOfferValue) / Nn(sCompoMarkeQualiValue), 3.0) * Nn(sImporCompoQualiValue) + (+Pow(Nn(sCompoDiscoPriceValue) / Max(0.1, Nn(sDiscoPriceOfferValue)), 4.0) - Nn(sCumulDeltaSalesValue) / m_CurrentStep / Nn(sCompoDiscoPriceValue)) * Nn(sImporCompoPriceValue) + Pow(Nn(sIndexOfAttractionValue) / Max(0.1, Nn(sAveraAttraIndexValue)), 3.0) * Nn(sImporCompoAttraValue)) / (Nn(sImporCompoQualiValue) + Nn(sImporCompoPriceValue) + Nn(sImporCompoAttraValue)) * 1.0)

                        End If

                        sValore = Max(0, Nn(sValore) - Nn(sThresQualiOfferValue) - Nn(sThresPriceOfferValue) - Nn(sThresAttraOfferValule))

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "IndexMarkeOffer_1304 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Indice di competitività
    ''' </summary>
    Private Sub IndexOfCompetition_1305()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "IndexOfCompetition"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String
        Dim dSum As Double
        Dim iIDItem As Integer
        Dim sCoeffSuperBossValue As String
        Dim sIndexMarkeOfferValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowItem As DataRow In m_DataTableItems.Rows
                    dSum = 0
                    iIDItem = Nni(oRowItem("IDVariable"))

                    For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                        iIDPlayer = Nni(oRowPlayer("IDTeam"))
                        sCoeffSuperBossValue = GetVariableDecisionPlayer(m_IDGame, "CoeffSuperBoss", iIDPlayer, iIDItem)

                        dSum += Nn(GetVariableDefineValue("IndexMarkeOffer", iIDPlayer, iIDItem, 0)) * Nn(sCoeffSuperBossValue)
                    Next

                    For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                        iIDPlayer = Nni(oRowPlayer("IDTeam"))
                        sTeamName = Nz(oRowPlayer("TeamName"))

                        If dSum = 0 Then
                            sValore = 0
                        Else
                            sIndexMarkeOfferValue = GetVariableDefineValue("IndexMarkeOffer", iIDPlayer, iIDItem, 0)

                            sValore = Nn(sIndexMarkeOfferValue) / (dSum / m_DataTablePlayers.Rows.Count)
                        End If

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "IndexOfCompetition_1305 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Quote ipotetiche di mercato prima scelta
    ''' </summary>
    Private Sub FirstOrderShare_1306()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "FirstOrderShare"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String
        Dim dSum As Double
        Dim iIDItem As Integer

        Dim sIndexOfCompetitionValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowItem As DataRow In m_DataTableItems.Rows
                    dSum = 0
                    iIDItem = Nni(oRowItem("IDVariable"))

                    For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                        iIDPlayer = Nni(oRowPlayer("IDTeam"))

                        dSum += Nn(GetVariableDefineValue("IndexOfCompetition", iIDPlayer, iIDItem, 0))
                    Next

                    For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                        iIDPlayer = Nni(oRowPlayer("IDTeam"))
                        sTeamName = Nz(oRowPlayer("TeamName"))

                        If dSum = 0 Then
                            sValore = 0
                        Else
                            sIndexOfCompetitionValue = GetVariableDefineValue("IndexOfCompetition", iIDPlayer, iIDItem, 0)

                            sValore = Nn(sIndexOfCompetitionValue) / dSum
                        End If

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "FirstOrderShare_1306 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Vendite di prima scelta
    ''' </summary>
    Private Sub FirstOrderAlloc_1307()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "FirstOrderAlloc"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sMaximSellaGoodsValue As String
        Dim sFirstOrderShareValue As String
        Dim sMarketDemandValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        sMaximSellaGoodsValue = GetVariableDefineValue("MaximSellaGoods", iIDPlayer, Nni(oRowItem("IDVariable")), 0)
                        sFirstOrderShareValue = GetVariableDefineValue("FirstOrderShare", iIDPlayer, Nni(oRowItem("IDVariable")), 0)
                        sMarketDemandValue = GetVariableDefineValue("MarketDemand", 0, Nni(oRowItem("IDVariable")), 0)

                        sValore = Truncate(Min(Nn(sMaximSellaGoodsValue), Nn(sFirstOrderShareValue) * Nn(sMarketDemandValue)))

                        SaveVariableValue(iIDVariabile, iIDPlayer, Nni(oRowItem("IDVariable")), 0, sValore)
                    Next

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "FirstOrderAlloc_1307 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Prime scelte totali
    ''' </summary>
    Private Sub TotalOrderAlloc_1308()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TotalOrderAlloc"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowItem As DataRow In m_DataTableItems.Rows
                    sValore = "0"

                    For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                        iIDPlayer = Nni(oRowPlayer("IDTeam"))
                        sTeamName = Nz(oRowPlayer("TeamName"))
                        sValore = Nn(sValore) + Nn(GetVariableDefineValue("FirstOrderAlloc", iIDPlayer, Nni(oRowItem("IDVariable")), 0))

                        SaveVariableValue(iIDVariabile, 0, Nni(oRowItem("IDVariable")), 0, sValore)
                    Next
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "TotalOrderAlloc_1308 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Domanda di mercato insoddisfatta
    ''' </summary>
    Private Sub ResidMarkeDeman_1309()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "ResidMarkeDeman"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sMarketDemandValue As String
        Dim sTotalOrderAllocValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"
                For Each oRowItem As DataRow In m_DataTableItems.Rows
                    iIDItem = Nni(oRowItem("IDVariable"))

                    For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                        iIDPlayer = Nni(oRowPlayer("IDTeam"))
                        sTeamName = Nz(oRowPlayer("TeamName"))

                        sMarketDemandValue = GetVariableDefineValue("MarketDemand", 0, iIDItem, 0)
                        sTotalOrderAllocValue = GetVariableDefineValue("TotalOrderAlloc", 0, iIDItem, 0)

                        sValore = Max(0, Nn(sMarketDemandValue) - Nn(sTotalOrderAllocValue))

                    Next

                    SaveVariableValue(iIDVariabile, 0, iIDItem, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "ResidMarkeDeman_1309 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Allocazione delle seconde scelte
    ''' </summary>
    Private Sub ResidDemanAlloc_1310()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "ResidDemanAlloc"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer

        Dim dSoom As Double

        Dim sMaximSellaGoodsValue As String
        Dim sFirstOrderAllocValue As String
        Dim sIndexOfCompetitionValule As String
        Dim sDemanAllocThresValue As String = GetVariableBoss("DemanAllocThres", "")
        Dim sResidMarkeDemanValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"
                For Each oRowItem As DataRow In m_DataTableItems.Rows
                    iIDItem = Nni(oRowItem("IDVariable"))
                    dSoom = 0


                    For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                        iIDPlayer = Nni(oRowPlayer("IDTeam"))
                        sTeamName = Nz(oRowPlayer("TeamName"))

                        sMaximSellaGoodsValue = GetVariableDefineValue("MaximSellaGoods", iIDPlayer, iIDItem, 0)
                        sFirstOrderAllocValue = GetVariableDefineValue("FirstOrderAlloc", iIDPlayer, iIDItem, 0)
                        sIndexOfCompetitionValule = GetVariableDefineValue("IndexOfCompetition", iIDPlayer, iIDItem, 0)

                        If Nn(sMaximSellaGoodsValue) > Nn(sFirstOrderAllocValue) AndAlso Nn(sIndexOfCompetitionValule) >= Nn(sDemanAllocThresValue) Then
                            dSoom += Nn(sMaximSellaGoodsValue) - Nn(sFirstOrderAllocValue)
                        End If

                    Next

                    For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                        iIDPlayer = Nni(oRowPlayer("IDTeam"))
                        sMaximSellaGoodsValue = GetVariableDefineValue("MaximSellaGoods", iIDPlayer, iIDItem, 0)
                        sFirstOrderAllocValue = GetVariableDefineValue("FirstOrderAlloc", iIDPlayer, iIDItem, 0)
                        sIndexOfCompetitionValule = GetVariableDefineValue("IndexOfCompetition", iIDPlayer, iIDItem, 0)
                        sResidMarkeDemanValue = GetVariableDefineValue("ResidMarkeDeman", 0, iIDItem, 0)

                        If Nn(sMaximSellaGoodsValue) > Nn(sFirstOrderAllocValue) AndAlso Nn(sIndexOfCompetitionValule) >= Nn(sDemanAllocThresValue) AndAlso dSoom > 0 Then
                            sValore = Truncate((Nn(sMaximSellaGoodsValue) - Nn(sFirstOrderAllocValue)) * Min(1, (Nn(sResidMarkeDemanValue) / dSoom)))
                        Else
                            sValore = 0
                        End If

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "ResidDemanAlloc_1310 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Vendite al dettaglio
    ''' </summary>
    Private Sub TotalProduSold_1311()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TotalProduSold"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String
        Dim iIDItem As Integer

        Dim sFirstOrderAllocValue As String
        Dim sResidDemanAllocValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))

                        sFirstOrderAllocValue = GetVariableDefineValue("FirstOrderAlloc", iIDPlayer, iIDItem, 0)
                        sResidDemanAllocValue = GetVariableDefineValue("ResidDemanAlloc", iIDPlayer, iIDItem, 0)
                        sValore = Nn(sFirstOrderAllocValue) + Nn(sResidDemanAllocValue)

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "TotalProduSold_1311 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Ratio Vendite al dettaglio / ven.massime
    ''' </summary>
    Private Sub RatioSalesMaxim_1312()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "RatioSalesMaxim"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String
        Dim iIDItem As Integer

        Dim sMaximSellaGoodsValue As String
        Dim sTotalProduSoldValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))

                        sMaximSellaGoodsValue = GetVariableDefineValue("MaximSellaGoods", iIDPlayer, iIDItem, 0)
                        sTotalProduSoldValue = GetVariableDefineValue("TotalProduSold", iIDPlayer, iIDItem, 0)

                        If Nn(sMaximSellaGoodsValue) = 0 Then
                            sValore = 0
                        Else
                            sValore = (Nn(sTotalProduSoldValue) / Nn(sMaximSellaGoodsValue)) * 100
                        End If

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "RatioSalesMaxim_1312 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Vendite totali al merecato
    ''' </summary>
    Private Sub GlobaMarkeSales_1313()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "GlobaMarkeSales"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer

        Dim sTotalProduSoldValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)


                For Each oRowItem As DataRow In m_DataTableItems.Rows
                    iIDItem = Nni(oRowItem("IDVariable"))

                    sValore = "0"
                    For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                        iIDPlayer = Nni(oRowPlayer("IDTeam"))
                        sTeamName = Nz(oRowPlayer("TeamName"))

                        sTotalProduSoldValue = GetVariableDefineValue("TotalProduSold", iIDPlayer, iIDItem, 0)

                        sValore = Nn(sValore) + Nn(sTotalProduSoldValue)

                    Next
                    SaveVariableValue(iIDVariabile, 0, iIDItem, 0, sValore)

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "GlobaMarkeSales_1313 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Quota di mercato in quantità
    ''' </summary>
    Private Sub MarketShare_1314()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "MarketShare"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim dSale As Double

        Dim sTotalProduSoldValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"
                For Each oRowItem As DataRow In m_DataTableItems.Rows
                    iIDItem = Nni(oRowItem("IDVariable"))

                    dSale = 0
                    For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                        iIDPlayer = Nni(oRowPlayer("IDTeam"))
                        sTeamName = Nz(oRowPlayer("TeamName"))

                        sTotalProduSoldValue = GetVariableDefineValue("TotalProduSold", iIDPlayer, iIDItem, 0)
                        dSale += Nn(sTotalProduSoldValue)

                    Next

                    For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                        iIDPlayer = Nni(oRowPlayer("IDTeam"))
                        sTeamName = Nz(oRowPlayer("TeamName"))
                        If dSale = 0 Then
                            sValore = 0
                        Else
                            sTotalProduSoldValue = GetVariableDefineValue("TotalProduSold", iIDPlayer, iIDItem, 0)
                            sValore = Nn(sTotalProduSoldValue) / dSale * 100
                        End If

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "MarketShare_1314 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Quota di mercato in valore
    ''' </summary>
    Private Sub MarketPriceShare_1315()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "MarketPriceShare"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim dSoom As Double

        Dim sTotalProduSoldValue As String
        Dim sSalesPriceAllowValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"
                For Each oRowItem As DataRow In m_DataTableItems.Rows
                    iIDItem = Nni(oRowItem("IDVariable"))

                    dSoom = 0
                    For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                        iIDPlayer = Nni(oRowPlayer("IDTeam"))
                        sTeamName = Nz(oRowPlayer("TeamName"))

                        sTotalProduSoldValue = GetVariableDefineValue("TotalProduSold", iIDPlayer, iIDItem, 0)
                        sSalesPriceAllowValue = GetVariableDefineValue("SalesPriceAllow", iIDPlayer, iIDItem, 0)

                        dSoom += Nn(sTotalProduSoldValue) * Nn(sSalesPriceAllowValue)
                    Next

                    For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                        iIDPlayer = Nni(oRowPlayer("IDTeam"))
                        sTeamName = Nz(oRowPlayer("TeamName"))
                        If dSoom = 0 Then
                            sValore = 0
                        Else
                            sTotalProduSoldValue = GetVariableDefineValue("TotalProduSold", iIDPlayer, iIDItem, 0)
                            sSalesPriceAllowValue = GetVariableDefineValue("SalesPriceAllow", iIDPlayer, iIDItem, 0)

                            sValore = ((Nn(sTotalProduSoldValue) * Nn(sSalesPriceAllowValue)) / dSoom) * 100
                        End If

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "MarketPriceShare_1315 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    Private Sub RatioCompoShare_1316()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "RatioCompoShare"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String
        Dim iIDItem As String

        Dim sMarketShareValue As String
        Dim sFirstOrderShareValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sMarketShareValue = GetVariableDefineValue("MarketShare", iIDPlayer, iIDItem, 0)
                        sFirstOrderShareValue = GetVariableDefineValue("FirstOrderShare", iIDPlayer, iIDItem, 0)
                        If Nn(sMarketShareValue) = 0 Then
                            sValore = 0
                        Else
                            sValore = ((Nn(sFirstOrderShareValue) * 100) / Nn(sMarketShareValue)) * 100
                        End If
                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "RatioCompoShare_1316 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Fatturato al dettaglio - Prodotto
    ''' </summary>
    Private Sub ProduRevenMarke_1317()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "ProduRevenMarke"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String
        Dim iIDItem As Integer

        Dim sTotalProduSoldValue As String
        Dim sSalesPriceAllowValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))

                        sTotalProduSoldValue = GetVariableDefineValue("TotalProduSold", iIDPlayer, iIDItem, 0)
                        sSalesPriceAllowValue = GetVariableDefineValue("SalesPriceAllow", iIDPlayer, iIDItem, 0)

                        sValore = Nn(sTotalProduSoldValue) * Nn(sSalesPriceAllowValue)
                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "ProduRevenMarke_1317 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Fatturato al dettaglio - Totale
    ''' </summary>
    Private Sub TotalRevenMarke_1318()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TotalRevenMarke"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String
        Dim iIDItem As Integer

        Dim sProduRevenMarkeValue As String


        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    sValore = "0"

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))

                        sProduRevenMarkeValue = GetVariableDefineValue("ProduRevenMarke", iIDPlayer, iIDItem, 0)

                        sValore = Nn(sValore) + Nn(sProduRevenMarkeValue)

                    Next
                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "TotalRevenMarke_1318 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Ratio Promozioni/ Fatturato al dettaglio
    ''' </summary>
    Private Sub RatioPromoReven_1319()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "RatioPromoReven"

        Dim RatioPromoReven As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String
        Dim iIDItem As Integer

        Dim ProduRevenMarke As String
        Dim MarkeExpenAllow As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    RatioPromoReven = "0"

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))

                        ProduRevenMarke = GetVariableDefineValue("ProduRevenMarke", iIDPlayer, iIDItem, 0)
                        MarkeExpenAllow = GetVariableDefineValue("MarkeExpenAllow", iIDPlayer, iIDItem, 0)

                        If Nn(ProduRevenMarke) = 0 Then
                            RatioPromoReven = "0"
                        Else
                            RatioPromoReven = Nn(MarkeExpenAllow) / Nn(ProduRevenMarke) * 100
                        End If
                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, RatioPromoReven)
                    Next

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "RatioPromoReven_1319 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Ratio Costi di produzione / Fatturato dettaglio
    ''' </summary>
    Private Sub RatioProduReven_1320()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "RatioProduReven"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sTotalRevenMarkeValue As String
        Dim sProduTotalCostValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sTotalRevenMarkeValue = GetVariableDefineValue("TotalRevenMarke", iIDPlayer, 0, 0)
                    sProduTotalCostValue = GetVariableDefineValue("ProduTotalCost", iIDPlayer, 0, 0)

                    If Nn(sTotalRevenMarkeValue) = 0 Then
                        sValore = "0"
                    Else
                        sValore = Nn(sProduTotalCostValue) / Nn(sTotalRevenMarkeValue) * 100
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "RatioProduReven_1320 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

#End Region

#Region "281 - 300 FUNZIONI DI CALCOLO"

    ''' <summary>
    ''' Ratio Costi fissi / Fatturato al dett.
    ''' </summary>
    Private Sub RatioFixedReven_1401()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "RatioFixedReven"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sTotalRevenMarkeValue As String
        Dim sFixedProduCostsValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sTotalRevenMarkeValue = GetVariableDefineValue("TotalRevenMarke", iIDPlayer, 0, 0)
                    sFixedProduCostsValue = GetVariableDefineValue("FixedProduCosts", iIDPlayer, 0, 0)

                    If Nn(sTotalRevenMarkeValue) Then
                        sValore = "0"
                    Else
                        sValore = Nn(sFixedProduCostsValue) / Nn(sTotalRevenMarkeValue) * 100
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "RatioFixedReven_1401 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Ratio affitti/fatturato
    ''' </summary>
    Private Sub RatioOutleReven_1402()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "RatioOutleReven"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sTotalRevenMarkeValue As String
        Dim sTotalCostsOutleValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sTotalRevenMarkeValue = GetVariableDefineValue("TotalRevenMarke", iIDPlayer, 0, 0)
                    sTotalCostsOutleValue = GetVariableDefineValue("TotalCostsOutle", iIDPlayer, 0, 0)

                    If Nn(sTotalRevenMarkeValue) Then
                        sValore = "0"
                    Else
                        sValore = Nn(sTotalCostsOutleValue) / Nn(sTotalRevenMarkeValue) * 100
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "RatioOutleReven_1402 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    Private Sub RatioPersoReven_1403()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "RatioPersoReven"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sTotalRevenMarkeValue As String
        Dim sTotalCostsPersoValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sTotalRevenMarkeValue = GetVariableDefineValue("TotalRevenMarke", iIDPlayer, 0, 0)
                    sTotalCostsPersoValue = GetVariableDefineValue("TotalCostsPerso", iIDPlayer, 0, 0)

                    If Nn(sTotalRevenMarkeValue) = 0 Then
                        sValore = "0"
                    Else
                        sValore = Nn(sTotalCostsPersoValue) / Nn(sTotalRevenMarkeValue) * 100
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "RatioPersoReven_1403 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Incremento dello stock di prodotto finito
    ''' </summary>
    Private Sub IncreGoodsStock_1404()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "IncreGoodsStock"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer

        Dim sProduRequeValue As String
        Dim sExtraAllowValue As String
        Dim sSalesInterMarkeValue As String
        Dim sTotalProduSoldValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))

                        sProduRequeValue = GetVariableDefineValue("ProduReque", iIDPlayer, iIDItem, 0)
                        sExtraAllowValue = GetVariableDefineValue("ExtraAllow", iIDPlayer, iIDItem, 0)
                        sSalesInterMarkeValue = GetVariableDefineValue("SalesInterMarke", iIDPlayer, iIDItem, 0)
                        sTotalProduSoldValue = GetVariableDefineValue("TotalProduSold", iIDPlayer, iIDItem, 0)

                        sValore = Nn(sProduRequeValue) + Nn(sExtraAllowValue) - Nn(sSalesInterMarkeValue) - Nn(sTotalProduSoldValue)

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "IncreGoodsStock_1404 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Stock di prodotto finito/fine
    ''' </summary>
    Private Sub FinisGoodsStock_1405()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "FinisGoodsStock"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim sIncreGoodsStockValue As String
        Dim sStockFinisGoodsValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        sIncreGoodsStockValue = "0"
                        sStockFinisGoodsValue = "0"
                        sValore = "0"

                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        sIncreGoodsStockValue = GetVariableDefineValue("IncreGoodsStock", iIDPlayer, iIDItem, 0)
                        sStockFinisGoodsValue = GetVariableState("StockFinisGoods", iIDPlayer, "var_" & sTeamName & "_StockFinisGoods_" & sItemName, iIDItem, 0)

                        sValore = Nn(sIncreGoodsStockValue) + Nn(sStockFinisGoodsValue)

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "FinisGoodsStock_1405 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Incremento del valore unitario prodotto finito
    ''' </summary>
    Private Sub IncreValueStock_1406()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "IncreValueStock"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim sIncreGoodsStockValue As String
        Dim sStockFinisGoodsValue As String
        Dim sProduRequeValue As String
        Dim sExtraAllowValue As String
        Dim sStockRawMaterValue As String
        Dim sPurchAllowEuropValue As String
        Dim sPortfDelivNICValue As String
        Dim sTotalProduCostsValue As String
        Dim sValueRawMaterValue As String
        Dim sRawEuropCostValue As String
        Dim sRawNICCostValue As String
        Dim sExtraProduCostValue As String
        Dim sLastFinisValueValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        sValore = "0"

                        sIncreGoodsStockValue = GetVariableDefineValue("IncreGoodsStock", iIDPlayer, iIDItem, 0)
                        sStockFinisGoodsValue = GetVariableState("StockFinisGoods", iIDPlayer, "var_" & sTeamName & "_StockFinisGoods_" & sItemName, iIDItem, 0)
                        sProduRequeValue = GetVariableDefineValue("ProduReque", iIDPlayer, iIDItem, 0)
                        sExtraAllowValue = GetVariableDefineValue("ExtraAllow", iIDPlayer, iIDItem, 0)
                        sStockRawMaterValue = GetVariableState("StockRawMater", iIDPlayer, "var_" & sTeamName & "_StockRawMater_" & sItemName, iIDItem, 0)
                        sPurchAllowEuropValue = GetVariableDefineValue("PurchAllowEurop", iIDPlayer, iIDItem, 0)
                        sPortfDelivNICValue = GetVariableDefineValue("PortfDelivNIC", iIDPlayer, iIDItem, 0)
                        sTotalProduCostsValue = GetVariableDefineValue("TotalProduCosts", iIDPlayer, iIDItem, 0)
                        sValueRawMaterValue = GetVariableState("ValueRawMater", iIDPlayer, "var_" & sTeamName & "_ValueRawMater_" & sItemName, iIDItem, 0)
                        sRawEuropCostValue = GetVariableBoss("RawEuropCost", "RawEuropCost_" & sItemName)
                        sRawNICCostValue = GetVariableBoss("RawNICCost", "RawNICCost_" & sItemName)
                        sExtraProduCostValue = GetVariableBoss("ExtraProduCost", "ExtraProduCost_" & sItemName)
                        sLastFinisValueValue = GetVariableState("LastFinisValue", iIDPlayer, "var_" & sTeamName & "_LastFinisValue_" & sItemName, iIDItem, 0)

                        If Nn(sIncreGoodsStockValue) <= 0 Then
                            sValore = 0
                        Else

                            If Nn(sIncreGoodsStockValue) + Nn(sStockFinisGoodsValue) = 0 OrElse Nn(sProduRequeValue) + Nn(sExtraAllowValue) = 0 _
                                OrElse Nn(sStockRawMaterValue) + Nn(sPurchAllowEuropValue) + Nn(sPortfDelivNICValue) = 0 Then
                                sValore = 0
                            Else
                                sValore = (((Nn(sIncreGoodsStockValue) * ((Nn(sProduRequeValue) * (Nn(sTotalProduCostsValue) + ((Nn(sStockRawMaterValue) * Nn(sValueRawMaterValue) _
                                        + Nn(sPurchAllowEuropValue) * Nn(sRawEuropCostValue) + Nn(sPortfDelivNICValue) * Nn(sRawNICCostValue)) / (Nn(sStockRawMaterValue) _
                                        + Nn(sPurchAllowEuropValue) + Nn(sPortfDelivNICValue)))) + Nn(sExtraAllowValue) * Nn(sExtraProduCostValue)) _
                                        / (Nn(sProduRequeValue) + Nn(sExtraAllowValue))) + (Nn(sStockFinisGoodsValue) * Nn(sLastFinisValueValue)))) _
                                        / (Nn(sIncreGoodsStockValue) + Nn(sStockFinisGoodsValue))) - Nn(sLastFinisValueValue)
                            End If
                        End If
                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "IncreValueStock_1406 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Incremento della qualità della merce
    ''' </summary>
    Private Sub IncreQualiFinis_1407()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "IncreQualiFinis"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim sIncreGoodsStockValue As String
        Dim sStockFinisGoodsValue As String
        Dim sProduRequeValue As String
        Dim sExtraAllowValue As String
        Dim sProduTotalQualiValue As String
        Dim sExtraProduQualiValue As String
        Dim sQualiStockFinisValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))
                        sIncreGoodsStockValue = GetVariableDefineValue("IncreGoodsStock", iIDPlayer, iIDItem, 0)
                        sStockFinisGoodsValue = GetVariableState("StockFinisGoods", iIDPlayer, "var_" & sTeamName & "_StockFinisGoods_" & sItemName, iIDItem, 0)
                        sProduRequeValue = GetVariableDefineValue("ProduReque", iIDPlayer, iIDItem, 0)
                        sExtraAllowValue = GetVariableDefineValue("ExtraAllow", iIDPlayer, iIDItem, 0)
                        sProduTotalQualiValue = GetVariableDefineValue("ProduTotalQuali", iIDPlayer, iIDItem, 0)
                        sExtraProduQualiValue = GetVariableBoss("ExtraProduQuali", "ExtraProduQuali_" & sItemName)
                        sQualiStockFinisValue = GetVariableState("QualiStockFinis", iIDPlayer, "var_" & sTeamName & "_QualiStockFinis_" & sItemName, iIDItem, 0)

                        If Nn(sIncreGoodsStockValue) <= 0 Then
                            sValore = 0
                        Else

                            If Nn(sIncreGoodsStockValue) + Nn(sStockFinisGoodsValue) = 0 OrElse Nn(sProduRequeValue) + Nn(sExtraAllowValue) = 0 Then
                                sValore = 0
                            Else
                                sValore = (((Nn(sIncreGoodsStockValue) * ((Nn(sProduRequeValue) * Nn(sProduTotalQualiValue) + Nn(sExtraAllowValue) *
                                          Nn(sExtraProduQualiValue)) / (Nn(sProduRequeValue) + Nn(sExtraAllowValue))) + (Nn(sStockFinisGoodsValue) * Nn(sQualiStockFinisValue)))) _
                                          / (Nn(sIncreGoodsStockValue) + Nn(sStockFinisGoodsValue))) - Nn(sQualiStockFinisValue)
                            End If
                        End If

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "IncreQualiFinis_1407 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Incremento dei crediti
    ''' </summary>
    Private Sub IncreDebts_1408()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "IncreDebts"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sTotalRevenInterValue As String
        Dim sTermsPaymeWholeValue As String
        Dim sTotalRevenMarkeValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sTotalRevenInterValue = GetVariableDefineValue("TotalRevenInter", iIDPlayer, 0, 0)
                    sTermsPaymeWholeValue = GetVariableDecisionPlayer(m_IDGame, "TermsPaymeWhole", iIDPlayer, 0)
                    sTotalRevenMarkeValue = GetVariableDefineValue("TotalRevenMarke", iIDPlayer, 0, 0)

                    sValore = Nn(sTotalRevenInterValue) * (Max(0, Min(3, Nn(sTermsPaymeWholeValue))) / m_LunghezzaPeriodo) + Nn(sTotalRevenMarkeValue) / 3.0

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "IncreDebts_1408 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' ValoreFinale dei prodotti finiti
    ''' </summary>
    Private Sub ValueStockEnd_1409()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "ValueStockEnd"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim sIncreValueStockValue As String
        Dim sLastFinisValueValue As String
        Dim sIncreGoodsStockValue As String
        Dim sStockFinisGoodsValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)



                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))
                        sValore = "0"

                        sIncreValueStockValue = GetVariableDefineValue("IncreValueStock", iIDPlayer, iIDItem, 0)
                        sLastFinisValueValue = GetVariableState("LastFinisValue", iIDPlayer, "var_" & sTeamName & "_LastFinisValue_" & sItemName, iIDItem, 0)
                        sIncreGoodsStockValue = GetVariableDefineValue("IncreGoodsStock", iIDPlayer, iIDItem, 0)
                        sStockFinisGoodsValue = GetVariableState("StockFinisGoods", iIDPlayer, "var_" & sTeamName & "_StockFinisGoods_" & sItemName, iIDItem, 0)

                        sValore = (Nn(sIncreValueStockValue) + Nn(sLastFinisValueValue)) * (Nn(sIncreGoodsStockValue) + Nn(sStockFinisGoodsValue))

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "ValueStockEnd_1409 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Variazione dello stock prodotto finito
    ''' </summary>
    Private Sub DeltaStockValue_1410()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DeltaStockValue"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim sValueStockEndValue As String
        Dim sValueStockBeginValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = "0"

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        sValueStockEndValue = GetVariableDefineValue("ValueStockEnd", iIDPlayer, iIDItem, 0)
                        sValueStockBeginValue = GetVariableDefineValue("ValueStockBegin", iIDPlayer, iIDItem, 0)
                        sValore = Nn(sValore) + Nn(sValueStockEndValue) - Nn(sValueStockBeginValue)
                    Next
                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "DeltaStockValue_1410 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Costi di immagazinaggio
    ''' </summary>
    Private Sub TotalWarehCosts_1411()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TotalWarehCosts"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim StockFinisGoods As Double
        Dim IncreGoodsStock As Double
        Dim CapacTransProdu As Double
        Dim CostWarehUnit As Double = Nn(GetVariableBoss("CostWarehUnit", ""), 0)
        Dim CostBaseLogis As Double = Nn(GetVariableBoss("CostBaseLogis", ""), 0)

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = "0"

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        StockFinisGoods = GetVariableState("StockFinisGoods", iIDPlayer, "var_" & sTeamName & "_StockFinisGoods_" & sItemName, iIDItem, 0)
                        IncreGoodsStock = GetVariableDefineValue("IncreGoodsStock", iIDPlayer, iIDItem, 0)
                        CapacTransProdu = GetVariableBoss("CapacTransProdu", "CapacTransProdu_" & sItemName)

                        sValore = Nn(sValore) + ((StockFinisGoods + IncreGoodsStock) * CapacTransProdu * CostWarehUnit)

                    Next

                    sValore = Nn(sValore) + CostBaseLogis

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "TotalWarehCosts_1411 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Costi totali
    ''' </summary>
    Private Sub TotalCostsPerio_1412()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TotalCostsPerio"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    sValore = Nn(GetVariableDefineValue("CostPurchTotal", iIDPlayer, 0, 0)) + Nn(GetVariableDefineValue("FixedProduCosts", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("DirecTotalCosts", iIDPlayer, 0, 0)) + Nn(GetVariableDefineValue("SinkiFundPlant", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("LeasiTotalCost", iIDPlayer, 0, 0)) + Nn(GetVariableDefineValue("TotalTransCosts", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("TotalWarehCosts", iIDPlayer, 0, 0)) + Nn(GetVariableDefineValue("DebtsWrittOff", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("TotalNegatInter", iIDPlayer, 0, 0)) + Nn(GetVariableDefineValue("TotalCostsOutle", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("TotalCostsPerso", iIDPlayer, 0, 0)) + Nn(GetVariableDefineValue("ExtraMoneyAllow", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("PromoInvesPerio", iIDPlayer, 0, 0)) + Nn(GetVariableDefineValue("GreenAllow", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("AdditCostsPerio", iIDPlayer, 0, 0)) + Nn(GetVariableDefineValue("TechnCostsPerio", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("SinkiFundVehic", iIDPlayer, 0, 0)) + Nn(GetVariableDefineValue("WriteOffLost", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("AddesTotalAllow", iIDPlayer, 0, 0))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "TotalCostsPerio_1412 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Variazione del magazzino prodotti finiti
    ''' </summary>
    Private Sub CumulDeltaStock_1413()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CumulDeltaStock"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = Nn(GetVariableState("VariaDeltaStock", iIDPlayer, "", 0, 0)) _
                            + Nn(GetVariableDefineValue("DeltaStockValue", iIDPlayer, 0, 0)) _
                            - Nn(GetVariableDefineValue("DischDeltaStock", iIDPlayer, 0, 0))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "CumulDeltaStock_1413 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Magazzino
    ''' </summary>
    Private Sub CumulWarehCosts_1414()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CumulWarehCosts"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = Nn(GetVariableState("VariaWarehCosts", iIDPlayer, "", 0, 0)) _
                            + Nn(GetVariableDefineValue("TotalWarehCosts", iIDPlayer, 0, 0)) _
                            - Nn(GetVariableDefineValue("DischWarehCosts", iIDPlayer, 0, 0))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "CumulWarehCosts_1414 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Margine per prodotto
    ''' </summary>
    Private Sub MarginPerProduct_1415()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "MarginPerProduct"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim sProduRevenMarkeValue As String
        Dim sProduRevenInterValue As String
        Dim sDirecProduCostsValue As String
        Dim sCostPurchRawValue As String
        Dim sRawNICCostValue As String
        Dim sPortfPurchNICValue As String
        Dim sPortfDelivNICValue As String
        Dim sExtraAllowValue As String
        Dim sExtraProduCostValue As String
        Dim sBeginRawValueValue As String
        Dim sEndRawValueValue As String
        Dim sValueStockBeginValue As String
        Dim sValueStockEndValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        sProduRevenMarkeValue = GetVariableDefineValue("ProduRevenMarke", iIDPlayer, iIDItem, 0)
                        sProduRevenInterValue = GetVariableDefineValue("ProduRevenInter", iIDPlayer, iIDItem, 0)
                        sDirecProduCostsValue = GetVariableDefineValue("DirecProduCosts", iIDPlayer, iIDItem, 0)
                        sCostPurchRawValue = GetVariableDefineValue("CostPurchRaw", iIDPlayer, iIDItem, 0)
                        sRawNICCostValue = GetVariableBoss("RawNICCost", "RawNICCost_" & sItemName)
                        sPortfPurchNICValue = GetVariableDefineValue("PortfPurchNIC", iIDPlayer, iIDItem, 0)
                        sPortfDelivNICValue = GetVariableDefineValue("PortfDelivNIC", iIDPlayer, iIDItem, 0)
                        sExtraAllowValue = GetVariableDefineValue("ExtraAllow", iIDPlayer, iIDItem, 0)
                        sExtraProduCostValue = GetVariableBoss("ExtraProduCost", "ExtraProduCost_" & sItemName)
                        sBeginRawValueValue = GetVariableDefineValue("BeginRawValue", iIDPlayer, iIDItem, 0)
                        sEndRawValueValue = GetVariableDefineValue("EndRawValue", iIDPlayer, iIDItem, 0)
                        sValueStockBeginValue = GetVariableDefineValue("ValueStockBegin", iIDPlayer, iIDItem, 0)
                        sValueStockEndValue = GetVariableDefineValue("ValueStockEnd", iIDPlayer, iIDItem, 0)

                        If Nn(sProduRevenMarkeValue) + Nn(sProduRevenInterValue) = 0 Then
                            sValore = "0"
                        Else
                            sValore = 100 - ((Nn(sDirecProduCostsValue) + Nn(sCostPurchRawValue) + Nn(sRawNICCostValue) _
                                    * (Nn(sPortfPurchNICValue) - Nn(sPortfDelivNICValue)) + Nn(sExtraAllowValue) * Nn(sExtraProduCostValue) _
                                    + Nn(sBeginRawValueValue) - Nn(sEndRawValueValue) + Nn(sValueStockBeginValue) - Nn(sValueStockEndValue)) _
                                    / (Nn(sProduRevenMarkeValue) + Nn(sProduRevenInterValue))) * 100
                        End If

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "MarginPerProduct_1415 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    Private Sub RatioWarehReven_1416()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "RatioWarehReven"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sTotalRevenMarkeValue As String
        Dim sTotalWarehCostsValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sTotalRevenMarkeValue = GetVariableDefineValue("TotalRevenMarke", iIDPlayer, 0, 0)
                    sTotalWarehCostsValue = GetVariableDefineValue("TotalWarehCosts", iIDPlayer, 0, 0)

                    If (Nn(sTotalRevenMarkeValue) = 0) Then
                        sValore = 0
                    Else
                        sValore = Nn(sTotalWarehCostsValue) / Nn(sTotalRevenMarkeValue) * 100
                    End If
                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "RatioWarehReven_1416 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Creditori
    ''' </summary>
    Private Sub DebtsEndPerio_1417()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DebtsEndPerio"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = Nn(GetVariableState("Debtors", iIDPlayer, "", 0, 0)) _
                            + Nn(GetVariableDefineValue("IncreDebts", iIDPlayer, 0, 0)) _
                            - Nn(GetVariableDefineValue("DecreDebts", iIDPlayer, 0, 0))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "DebtsEndPerio_1417 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Valore dello stock prodotto finito
    ''' </summary>
    Private Sub StockEndPerio_1718()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "StockEndPerio"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = "0"
                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        sValore = Nn(sValore) + Nn(GetVariableDefineValue("ValueStockEnd", iIDPlayer, iIDItem, 0))

                    Next

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "StockEndPerio_1718 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Incremento dei debiti
    ''' </summary>
    Private Sub IncreCredits_1419()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "IncreCredits"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sDiritRitarPagamValue As String = GetVariableBoss("DiritRitarPagam", "")
        Dim sDiritScontPagamValue As String = GetVariableBoss("DiritScontPagam", "")

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = Nn(GetVariableDefineValue("CostPurchEurop", iIDPlayer, 0, 0)) * (m_LunghezzaPeriodo _
                            - (Max(m_LunghezzaPeriodo - Nn(GetVariableBoss("LagPaymeEurop", "")) _
                            - Nn(GetVariableDecisionPlayer(m_IDGame, "DelayPaymeEurop", iIDPlayer, 0)) * Nn(sDiritRitarPagamValue), 0))) _
                            / m_LunghezzaPeriodo + Nn(GetVariableDefineValue("DirecTotalCosts", iIDPlayer, 0, 0)) _
                            * (m_LunghezzaPeriodo - (Max(m_LunghezzaPeriodo - Nn(GetVariableBoss("LagPaymeProdu", "")) _
                            - Nn(GetVariableDecisionPlayer(m_IDGame, "DelayPaymeProdu", iIDPlayer, 0)) * Nn(sDiritRitarPagamValue), 0))) _
                            / m_LunghezzaPeriodo + Nn(GetVariableDefineValue("LeasiTotalCost", iIDPlayer, 0, 0)) _
                            * (m_LunghezzaPeriodo - (Max(m_LunghezzaPeriodo - Nn(GetVariableBoss("LagPaymeLease", "")) _
                            - Nn(GetVariableDecisionPlayer(m_IDGame, "DelayPaymeLease", iIDPlayer, 0)) * Nn(sDiritRitarPagamValue), 0))) _
                            / m_LunghezzaPeriodo + Nn(GetVariableDefineValue("PromoInvesPerio", iIDPlayer, 0, 0)) _
                            * (m_LunghezzaPeriodo - (Max(m_LunghezzaPeriodo - Nn(GetVariableBoss("LagPaymePromo", "")) _
                            - Nn(GetVariableDecisionPlayer(m_IDGame, "DelayPaymePromo", iIDPlayer, 0)) * Nn(sDiritRitarPagamValue), 0))) _
                            / m_LunghezzaPeriodo + Nn(GetVariableDefineValue("ExtraMoneyAllow", iIDPlayer, 0, 0)) _
                            * (m_LunghezzaPeriodo - (m_LunghezzaPeriodo - Nn(GetVariableBoss("LagPaymeExtra", "")) _
                            + Min(m_LunghezzaPeriodo, Nn(GetVariableDecisionPlayer(m_IDGame, "ForwaPaymeExtra", iIDPlayer, 0)) * Nn(sDiritScontPagamValue)))) _
                            / m_LunghezzaPeriodo + Nn(GetVariableDefineValue("TechnCostsPerio", iIDPlayer, 0, 0)) _
                            * (m_LunghezzaPeriodo - (m_LunghezzaPeriodo - Nn(GetVariableBoss("LagPaymeAutom", "")) _
                            + Min(m_LunghezzaPeriodo, Nn(GetVariableDecisionPlayer(m_IDGame, "ForwaPaymeAutom", iIDPlayer, 0)) * Nn(sDiritScontPagamValue)))) _
                            / m_LunghezzaPeriodo + Nn(GetVariableDefineValue("AdditCostsPerio", iIDPlayer, 0, 0)) _
                            * (m_LunghezzaPeriodo - (m_LunghezzaPeriodo - Nn(GetVariableBoss("LagPaymeMktg", "")) _
                            + Min(m_LunghezzaPeriodo, Nn(GetVariableDecisionPlayer(m_IDGame, "ForwaPaymeMktg", iIDPlayer, 0)) * Nn(sDiritScontPagamValue)))) _
                            / m_LunghezzaPeriodo + Nn(0)
                    '+ Nn(GetVariableState("PlantPerAge", iIDPlayer, "var_" & sTeamName & "_PlantPerAge_1", 0, 1)) _
                    '* Nn(GetVariableBoss("NewPlantCost", "")) _
                    sValore = Nn(sValore) _
                            + Nn(GetVariableDefineValue("GreenAllow", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("AddesTotalAllow", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("FixedProduCosts", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("TotalWarehCosts", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("TotalTransCosts", iIDPlayer, 0, 0))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "IncreCredits_1419 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Interessi attivi
    ''' </summary>
    Private Sub TotalPositInter_1420()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TotalPositInter"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sBankLevelValue As String
        Dim sTotalCostsPerioValue As String
        Dim sSinkiFundPlantValue As String
        Dim sSinkiFundVehicValue As String
        Dim sTaxRateCrediValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sBankLevelValue = GetVariableState("BankLevel", iIDPlayer, "", 0, 0)
                    sTotalCostsPerioValue = GetVariableDefineValue("TotalCostsPerio", iIDPlayer, 0, 0)
                    sSinkiFundPlantValue = GetVariableDefineValue("SinkiFundPlant", iIDPlayer, 0, 0)
                    sSinkiFundVehicValue = GetVariableDefineValue("SinkiFundVehic", iIDPlayer, 0, 0)
                    sTaxRateCrediValue = GetVariableBoss("TaxRateCredi", "TaxRateCredi_" & sTeamName)

                    sValore = "0"
                    If Nn(sBankLevelValue) > Nn(sTotalCostsPerioValue) - Nn(sSinkiFundPlantValue) - Nn(sSinkiFundVehicValue) Then
                        sValore = (Nn(sBankLevelValue) - Nn(sTotalCostsPerioValue) + Nn(sSinkiFundPlantValue) + Nn(sSinkiFundVehicValue)) * Nn(sTaxRateCrediValue) / 100 / 12
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "TotalPositInter_1420 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Valore finale della merce 
    ''' </summary>
    Private Sub FinisValueStock_1421()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "FinisValueStock"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        sValore = Nn(GetVariableDefineValue("IncreValueStock", iIDPlayer, iIDItem, 0)) _
                                + Nn(GetVariableState("LastFinisValue", iIDPlayer, "var_" & sTeamName & "_LastFinisValue_" & sItemName, iIDItem, 0))

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "FinisValueStock_1421 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Qualità finale della merce
    ''' </summary>
    Private Sub FinisQualiStock_1422()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "FinisQualiStock"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        sValore = Nn(GetVariableDefineValue("IncreQualiFinis", iIDPlayer, iIDItem, 0)) _
                                + Nn(GetVariableState("QualiStockFinis", iIDPlayer, "var_" & sTeamName & "_QualiStockFinis_" & sItemName, iIDItem, 0))

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "FinisQualiStock_1422 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

#End Region

#Region "301 - 320 FUNZIONI DI CALCOLO"

    ''' <summary>
    ''' Ricavi totali
    ''' </summary>
    Private Sub TotalRevenPerio_1501()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TotalRevenPerio"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    sValore = Nn(GetVariableDefineValue("TotalRevenMarke", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("TotalRevenInter", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("TotalPositInter", iIDPlayer, 0, 0))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "TotalRevenPerio_1501 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Profitto prima delle tasse
    ''' </summary>
    Private Sub ProfiBeforTax_1502()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "ProfiBeforTax"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    sValore = Nn(GetVariableDefineValue("TotalRevenPerio", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("DeltaStockValue", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("DeltaRawValue", iIDPlayer, 0, 0)) _
                            - Nn(GetVariableDefineValue("TotalCostsPerio", iIDPlayer, 0, 0))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "ProfiBeforTax_1502 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Fatturato totale
    ''' </summary>
    Private Sub CumulRevenTotal_1503()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CumulRevenTotal"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    sValore = Nn(GetVariableState("VariaRevenTotal", iIDPlayer, "", 0, 0)) _
                            + Nn(GetVariableDefineValue("TotalRevenPerio", iIDPlayer, 0, 0)) _
                            - Nn(GetVariableDefineValue("DischRevenTotal", iIDPlayer, 0, 0))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "CumulRevenTotal_1503 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Interessi attivi
    ''' </summary>
    Private Sub CumulPositInter_1504()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CumulPositInter"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    sValore = Nn(GetVariableState("VariaPositInter", iIDPlayer, "", 0, 0)) _
                            + Nn(GetVariableDefineValue("TotalPositInter", iIDPlayer, 0, 0)) _
                            - Nn(GetVariableDefineValue("DischPositInter", iIDPlayer, 0, 0))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "CumulPositInter_1504 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Profitto prima delle tasse
    ''' </summary>
    Private Sub CumulBeforTax_1505()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CumulBeforTax"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    sValore = Nn(GetVariableState("VariaBeforTax", iIDPlayer, "", 0, 0)) _
                            + Nn(GetVariableDefineValue("ProfiBeforTax", iIDPlayer, 0, 0)) _
                            - Nn(GetVariableDefineValue("DischBeforTax", iIDPlayer, 0, 0))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "CumulBeforTax_1505 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    Private Sub ReturOnSales_1506()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "ReturOnSales"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sTotalRevenPerioValue As String
        Dim sProfiBeforTaxValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sTotalRevenPerioValue = GetVariableDefineValue("TotalRevenPerio", iIDPlayer, 0, 0)
                    sProfiBeforTaxValue = GetVariableDefineValue("ProfiBeforTax", iIDPlayer, 0, 0)

                    If Nn(sTotalRevenPerioValue) = 0 Then
                        sValore = 0
                    Else
                        sValore = Nn(sProfiBeforTaxValue) / Nn(sTotalRevenPerioValue) * 100
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "ReturOnSales_1506 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Prestiti massimi - Prossimo periodo
    ''' </summary>
    Private Sub IncreMaximLoans_1507()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "IncreMaximLoans"

        Dim IncreMaximLoans As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim TotalRevenPerio As Double
        Dim ProduTotalCost As Double
        Dim StockEndPerio As Double
        Dim TaxFunds As Double
        Dim NetPlants As Double
        Dim BankLevel As Double
        Dim SelliPersoCost As Double

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                IncreMaximLoans = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    TotalRevenPerio = GetVariableDefineValue("TotalRevenPerio", iIDPlayer, 0, 0)
                    ProduTotalCost = GetVariableDefineValue("ProduTotalCost", iIDPlayer, 0, 0)
                    StockEndPerio = GetVariableDefineValue("StockEndPerio", iIDPlayer, 0, 0)
                    TaxFunds = GetVariableState("TaxFunds", iIDPlayer, "", 0, 0)
                    NetPlants = GetVariableDefineValue("NetPlants", iIDPlayer, 0, 0)
                    BankLevel = GetVariableState("BankLevel", iIDPlayer, "", 0, 0)
                    SelliPersoCost = GetVariableDeveloper(m_IDGame, "SelliPersoCost", iIDPlayer, 0)

                    IncreMaximLoans = (Max(0, TotalRevenPerio - ProduTotalCost - 0.5 * StockEndPerio - TaxFunds + 0.5 * NetPlants) + BankLevel / 6) * (SelliPersoCost / 10000)
                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, IncreMaximLoans)
                Next

            End If
            '/* 
            '   0.5 E' IL VALORE DI REALIZZO DEI PLANTS NEL PROSSIMO PERIODO,
            '   SE NON NE COMPRO DI NUOVI;   BANK LEVEL E' UNO STABILIZZATORE CHE TIENE CONTO DELLA CASSA
            '   DI FINE PERIODO PRECEDENTE 
            '*/
        Catch ex As Exception
            g_MessaggioErrore &= "IncreMaximLoans_1507 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Deiti verso terzi
    ''' </summary>
    Private Sub CrediEndPerio_1508()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CrediEndPerio"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    sValore = Nn(GetVariableState("Creditors", iIDPlayer, "", 0, 0)) _
                            + Nn(GetVariableDefineValue("IncreCredits", iIDPlayer, 0, 0)) _
                            - Nn(GetVariableDefineValue("DecreCredits", iIDPlayer, 0, 0))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "CrediEndPerio_1508 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Incremento della liquidità
    ''' </summary>
    Private Sub IncreBankCash_1509()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "IncreBankCash"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = Nn(GetVariableDefineValue("TotalRevenMarke", iIDPlayer, 0, 0)) * 2.0 / 3.0 _
                            + Nn(GetVariableDefineValue("TotalPositInter", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableState("Debtors", iIDPlayer, "", 0, 0)) _
                            + Nn(GetVariableDefineValue("WriteOffLost", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("TotalRevenInter", iIDPlayer, 0, 0)) _
                            * ((m_LunghezzaPeriodo - Max(0, Min(3, Nn(GetVariableDecisionPlayer(m_IDGame, "TermsPaymeWhole", iIDPlayer, 0))))) / m_LunghezzaPeriodo) _
                            + Max(0, Nn(GetVariableDefineValue("LoansAllow", iIDPlayer, 0, 0)))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "IncreBankCash_1509 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Tasse del periodo
    ''' </summary>
    Private Sub TaxOfPerio_1510()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TaxOfPerio"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = "0"

                    If m_CurrentStep = 4 OrElse m_CurrentStep = 8 OrElse m_CurrentStep = 12 OrElse m_CurrentStep = 16 OrElse m_CurrentStep = 20 Then
                        sValore = Max(0, Nn(GetVariableDefineValue("CumulBeforTax", iIDPlayer, 0, 0)) * Nn(GetVariableBoss("TaxRatePerce", "")) / 100)
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "TaxOfPerio_1510 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Profitto dopo le tasse
    ''' </summary>
    Private Sub ProfiAfterTax_1511()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "ProfiAfterTax"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = "0"

                    sValore = Nn(GetVariableDefineValue("ProfiBeforTax", iIDPlayer, 0, 0)) _
                            - Nn(GetVariableDefineValue("TaxOfPerio", iIDPlayer, 0, 0))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "ProfiAfterTax_1511 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    Private Sub ReturOnEquit_1512()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "ReturOnEquit"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sNetCapitalValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = "0"
                    sNetCapitalValue = GetVariableState("NetCapital", iIDPlayer, "", 0, 0)

                    If (Nn(sNetCapitalValue) <> 0) Then
                        sValore = Nn(GetVariableDefineValue("ProfiAfterTax", iIDPlayer, 0, 0)) / Nn(sNetCapitalValue) * 100
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "ReturOnEquit_1512 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Liquidità
    ''' </summary>
    Private Sub CashEndPerio_1513()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CashEndPerio"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = "0"

                    sValore = Nn(GetVariableState("BankLevel", iIDPlayer, "", 0, 0)) _
                            + Nn(GetVariableDefineValue("IncreBankCash", iIDPlayer, 0, 0)) _
                            - Nn(GetVariableDefineValue("DecreBankCash", iIDPlayer, 0, 0))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "CashEndPerio_1513 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Capitale investito
    ''' </summary>
    Private Sub TotalAssets_1514()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TotalAssets"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = "0"

                    sValore = Nn(GetVariableDefineValue("CashEndPerio", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("DebtsEndPerio", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("StockEndPerio", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("NetPlants", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("NetVehicles", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("RawEndPerio", iIDPlayer, 0, 0))
                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "TotalAssets_1514 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    Private Sub UtilsEndPerio_1515()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "UtilsEndPerio"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = "0"

                    sValore = Nn(GetVariableState("CumulProfi", iIDPlayer, "", 0, 0)) _
                            + Nn(GetVariableDefineValue("ProfiAfterTax", iIDPlayer, 0, 0))
                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "UtilsEndPerio_1515 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    Private Sub TaxPerioFunds_1516()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TaxPerioFunds"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = "0"

                    sValore = Nn(GetVariableState("TaxFunds", iIDPlayer, "", 0, 0)) _
                            + Nn(GetVariableDefineValue("TaxOfPerio", iIDPlayer, 0, 0)) _
                            - Nn(GetVariableDefineValue("TaxPayment", iIDPlayer, 0, 0))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "TaxPerioFunds_1516 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    Private Sub TotalLiabilities_1517()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TotalLiabilities"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = "0"

                    sValore = Nn(GetVariableDefineValue("LoansEndPerio", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("CrediEndPerio", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("UtilsEndPerio", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableState("NetCapital", iIDPlayer, "", 0, 0)) _
                            + Nn(GetVariableDefineValue("TaxPerioFunds", iIDPlayer, 0, 0))
                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "TotalLiabilities_1517 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    Private Sub ShareMarkeValue_1518()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "ShareMarkeValue"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sNetCapitalValue As String
        Dim sUtilsEndPerioValue As String
        Dim sCashEndPerioValue As String
        Dim sDebtsEndPerioValue As String
        Dim sRawEndPerioValue As String
        Dim sStockEndPerioValue As String
        Dim sNetPlantsValue As String
        Dim sLoansEndPerioValue As String
        Dim sCrediEndPerioValue As String
        Dim sTaxPerioFundsValue As String
        Dim sProfiBeforTaxValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = "0"

                    sNetCapitalValue = Nn(GetVariableState("NetCapital", iIDPlayer, "", 0, 0))
                    sUtilsEndPerioValue = Nn(GetVariableDefineValue("UtilsEndPerio", iIDPlayer, 0, 0))
                    sCashEndPerioValue = Nn(GetVariableDefineValue("CashEndPerio", iIDPlayer, 0, 0))
                    sDebtsEndPerioValue = Nn(GetVariableDefineValue("DebtsEndPerio", iIDPlayer, 0, 0))
                    sRawEndPerioValue = Nn(GetVariableDefineValue("RawEndPerio", iIDPlayer, 0, 0))
                    sStockEndPerioValue = Nn(GetVariableDefineValue("StockEndPerio", iIDPlayer, 0, 0))
                    sNetPlantsValue = Nn(GetVariableDefineValue("NetPlants", iIDPlayer, 0, 0))
                    sLoansEndPerioValue = Nn(GetVariableDefineValue("LoansEndPerio", iIDPlayer, 0, 0))
                    sCrediEndPerioValue = Nn(GetVariableDefineValue("CrediEndPerio", iIDPlayer, 0, 0))
                    sTaxPerioFundsValue = Nn(GetVariableDefineValue("TaxPerioFunds", iIDPlayer, 0, 0))
                    sProfiBeforTaxValue = Nn(GetVariableDefineValue("ProfiBeforTax", iIDPlayer, 0, 0))

                    If Nn(sNetCapitalValue) + Nn(sUtilsEndPerioValue) <= 0 Then
                        sValore = 0
                    Else
                        sValore = Max(0, (0.8 * Nn(sCashEndPerioValue) + Nn(sDebtsEndPerioValue) + 0.5 * Nn(sRawEndPerioValue) _
                                + 0.5 * Nn(sStockEndPerioValue) + 0.5 * Nn(sNetPlantsValue) - Nn(sLoansEndPerioValue) _
                                - Nn(sCrediEndPerioValue) - Nn(sTaxPerioFundsValue) + 0.25 * Nn(sProfiBeforTaxValue)) / 1000000)
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "ShareMarkeValue_1518 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' ROI - Return on investments
    ''' </summary>
    Private Sub ReturOnInves_1519()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "ReturOnInves"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sTotalAssetsValue As String
        Dim sTotalRevenPerioValue As String
        Dim sTotalCostsPerioValue As String
        Dim sSinkiFundPlantValue As String
        Dim sSinkiFundVehicValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = "0"

                    sTotalAssetsValue = GetVariableDefineValue("TotalAssets", iIDPlayer, 0, 0)
                    sTotalRevenPerioValue = GetVariableDefineValue("TotalRevenPerio", iIDPlayer, 0, 0)
                    sTotalCostsPerioValue = GetVariableDefineValue("TotalCostsPerio", iIDPlayer, 0, 0)
                    sSinkiFundPlantValue = GetVariableDefineValue("SinkiFundPlant", iIDPlayer, 0, 0)
                    sSinkiFundVehicValue = GetVariableDefineValue("SinkiFundVehic", iIDPlayer, 0, 0)

                    If Nn(sTotalAssetsValue) <> 0 Then
                        sValore = (Nn(sTotalRevenPerioValue) - Nn(sTotalCostsPerioValue) - Nn(sSinkiFundPlantValue) - Nn(sSinkiFundVehicValue)) / Nn(sTotalAssetsValue) * 100
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "ReturOnInves_1519 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    Private Sub DischCostsTechn_1520()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DischCostsTechn"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim sDischRevenTotalValue As String
        Dim sVariaAutomTechnValue As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sDischRevenTotalValue = GetVariableDefineValue("DischRevenTotal", iIDPlayer, 0, 0)
                    sVariaAutomTechnValue = GetVariableState("VariaAutomTechn", iIDPlayer, "", 0, 0)

                    If Nn(sDischRevenTotalValue) = 0 Then
                        sValore = 0
                    Else
                        sValore = sVariaAutomTechnValue
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "DischCostsTechn_1520 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    Private Sub CumulCostsTechn_1521()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CumulCostsTechn"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    sValore = Nn(GetVariableState("VariaAutomTechn", iIDPlayer, "", 0, 0)) _
                            + Nn(GetVariableDefineValue("TechnCostsPerio", iIDPlayer, 0, 0)) _
                            - Nn(GetVariableDefineValue("DischCostsTechn", iIDPlayer, 0, 0))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "CumulCostsTechn_1521 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

#End Region

#Region "CALCOLO PUV"
    Private Sub CalcolaPUV()
        CompoAttraIndex_00()
        PercePriceRetail_01()
        PerceUseValue_02()
        DeltaPerceValue_03()
        DeltaPercePrice_04()
        CompoMarkePUV_05()
        MappaPosizPUV_06()
    End Sub

    ''' <summary>
    ''' Indice medio di attrattività di base
    ''' </summary>
    Private Sub CompoAttraIndex_00()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CompoAttraIndex"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim AttractionIndex As Double
        Dim Attra As Double

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPLayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPLayer("IDTeam"))
                    sTeamName = Nz(oRowPLayer("TeamName"))
                    AttractionIndex = Nn(GetVariableDefineValue("AttractionIndex", iIDPlayer, 0, 0))
                    Attra += AttractionIndex
                Next
                sValore = Attra / m_DataTablePlayers.Rows.Count

                SaveVariableValue(iIDVariabile, 0, 0, 0, sValore)

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "CompoAttraIndex_00 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Prezzo percepito al dettaglio
    ''' </summary>
    Private Sub PercePriceRetail_01()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "PercePriceRetail"

        Dim PercePriceRetail As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim DiscoPriceOffer As Double
        Dim CompoDiscoPrice As Double

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPLayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPLayer("IDTeam"))
                    sTeamName = Nz(oRowPLayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        DiscoPriceOffer = Nn(GetVariableDefineValue("DiscoPriceOffer", iIDPlayer, iIDItem, 0))
                        CompoDiscoPrice = Nn(GetVariableDefineValue("CompoDiscoPrice", 0, iIDItem, 0))

                        If CompoDiscoPrice <> 0 Then
                            PercePriceRetail = (1 / (DiscoPriceOffer / CompoDiscoPrice))
                        Else
                            PercePriceRetail = "0"
                        End If


                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, PercePriceRetail)
                    Next
                Next


            End If
        Catch ex As Exception
            g_MessaggioErrore &= "PercePriceRetail_01 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Valore d'usopercepito
    ''' </summary>
    Private Sub PerceUseValue_02()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "PerceUseValue"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim QualiGoodsOffer As Double
        Dim CompoMarkeQuali As Double
        Dim ImporCompoQuali As Double
        Dim IndexOfAttraction As Double
        Dim ImporCompoAttra As Double

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                ImporCompoQuali = Nn(GetVariableBoss("ImporCompoQuali", ""))
                ImporCompoAttra = Nn(GetVariableBoss("ImporCompoAttra", ""))

                For Each oRowPLayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPLayer("IDTeam"))
                    sTeamName = Nz(oRowPLayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        QualiGoodsOffer = Nn(GetVariableDefineValue("QualiGoodsOffer", iIDPlayer, iIDItem, 0))
                        CompoMarkeQuali = Nn(GetVariableDefineValue("CompoMarkeQuali", 0, iIDItem, 0))
                        IndexOfAttraction = Nn(GetVariableDefineValue("IndexOfAttraction", iIDPlayer, 0, 0))

                        sValore = (((QualiGoodsOffer / CompoMarkeQuali) * ImporCompoQuali) + (IndexOfAttraction * ImporCompoAttra)) / (ImporCompoQuali + ImporCompoAttra)

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next
                Next


            End If
        Catch ex As Exception
            g_MessaggioErrore &= "PerceUseValue_02 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Delta valore  d'uso percepito
    ''' </summary>
    Private Sub DeltaPerceValue_03()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DeltaPerceValue"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim QualiGoodsOffer As Double
        Dim CompoMarkeQuali As Double
        Dim ImporCompoQuali As Double
        Dim IndexOfAttraction As Double
        Dim ImporCompoAttra As Double

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                ImporCompoQuali = Nn(GetVariableBoss("ImporCompoQuali", ""))
                ImporCompoAttra = Nn(GetVariableBoss("ImporCompoAttra", ""))

                For Each oRowPLayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPLayer("IDTeam"))
                    sTeamName = Nz(oRowPLayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        QualiGoodsOffer = Nn(GetVariableDefineValue("QualiGoodsOffer", iIDPlayer, iIDItem, 0))
                        CompoMarkeQuali = Nn(GetVariableDefineValue("CompoMarkeQuali", 0, iIDItem, 0))
                        IndexOfAttraction = Nn(GetVariableDefineValue("IndexOfAttraction", iIDPlayer, 0, 0))

                        sValore = ((((QualiGoodsOffer / CompoMarkeQuali) * ImporCompoQuali) + (IndexOfAttraction * ImporCompoAttra)) / (ImporCompoQuali + ImporCompoAttra)) - 1.0

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "DeltaPerceValue_03 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Delta prezzo percepito
    ''' </summary>
    Private Sub DeltaPercePrice_04()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DeltaPercePrice"

        Dim DeltaPercePrice As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim DiscoPriceOffer As Double
        Dim CompoDiscoPrice As Double

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPLayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPLayer("IDTeam"))
                    sTeamName = Nz(oRowPLayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        DiscoPriceOffer = Nn(GetVariableDefineValue("DiscoPriceOffer", iIDPlayer, iIDItem, 0))
                        CompoDiscoPrice = Nn(GetVariableDefineValue("CompoDiscoPrice", 0, iIDItem, 0))

                        DeltaPercePrice = "0"
                        If CompoDiscoPrice <> 0 Then
                            DeltaPercePrice = (1 / (DiscoPriceOffer / CompoDiscoPrice)) - 1.0
                        End If
                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, DeltaPercePrice)
                    Next
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "DeltaPercePrice_04 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Compunded market average PUV
    ''' </summary>
    Private Sub CompoMarkePUV_05()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CompoMarkePUV"

        Dim sValore As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim CompoMarkeQuali As Double
        Dim CompoAttraIndex As Double

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowItem As DataRow In m_DataTableItems.Rows
                    iIDItem = Nni(oRowItem("IDVariable"))
                    sItemName = Nz(oRowItem("VariableName"))

                    CompoAttraIndex = Nn(GetVariableDefineValue("CompoAttraIndex", 0, 0, 0))
                    CompoMarkeQuali = Nn(GetVariableDefineValue("CompoMarkeQuali", 0, iIDItem, 0))

                    sValore = (3 * CompoMarkeQuali + CompoAttraIndex) / 4

                    SaveVariableValue(iIDVariabile, 0, iIDItem, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "CompoMarkePUV_05 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Mappa di posizionamento sul PUV percepita
    ''' </summary>
    Private Sub MappaPosizPUV_06()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "MappaPosizPUV"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim DeltaPerceValue As Double
        Dim CompoMarkePUV As Double

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPLayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPLayer("IDTeam"))
                    sTeamName = Nz(oRowPLayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        DeltaPerceValue = Nn(GetVariableDefineValue("DeltaPerceValue", iIDPlayer, iIDItem, 0))
                        CompoMarkePUV = Nn(GetVariableDefineValue("CompoMarkePUV", 0, iIDItem, 0))

                        sValore = CompoMarkePUV * (100 + DeltaPerceValue * 100) / 100

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "MappaPosizPUV_06 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

#End Region

#Region "DASHBOARD"

    Private Sub CalcolaDashBoard()
        IndexROE_00()
        IndexROCE_01()
        OperatingProfit_02()
        EBITDAperce_03()
        CurrentRatio_04()
        AcidTest_05()
        InterestCover_06()
        DebtX_07()
        RevenPerShop_08()
        RevenPerPerso_09()
        SaturNegoz_10()
        WholesaleServed_11()
        StockRotation_12()
        ClientServed_13()
        PerceValueItem_14()
        PerceQuality_15()
    End Sub

    ''' <summary>
    ''' Index ROE
    ''' </summary>
    Private Sub IndexROE_00()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "IndexROE"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim CumulBeforTax As Double
        Dim UtilsEndPerio As Double
        Dim NetCapital As Double

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPLayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPLayer("IDTeam"))
                    sTeamName = Nz(oRowPLayer("TeamName"))

                    CumulBeforTax = Nn(GetVariableDefineValue("CumulBeforTax", iIDPlayer, 0, 0))
                    UtilsEndPerio = Nn(GetVariableDefineValue("UtilsEndPerio", iIDPlayer, 0, 0))
                    NetCapital = Nn(GetVariableState("NetCapital", iIDPlayer, "", 0, 0))

                    sValore = ((CumulBeforTax / 2) / (UtilsEndPerio + NetCapital)) * 100

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "IndexROE_00 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Index ROCE
    ''' </summary>
    Private Sub IndexROCE_01()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "IndexROCE"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim CumulBeforTax As Double
        Dim UtilsEndPerio As Double
        Dim NetCapital As Double
        Dim CumulNegatInter As Double
        Dim LoansEndPerio As Double

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPLayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPLayer("IDTeam"))
                    sTeamName = Nz(oRowPLayer("TeamName"))

                    CumulBeforTax = Nn(GetVariableDefineValue("CumulBeforTax", iIDPlayer, 0, 0))
                    UtilsEndPerio = Nn(GetVariableDefineValue("UtilsEndPerio", iIDPlayer, 0, 0))
                    NetCapital = Nn(GetVariableState("NetCapital", iIDPlayer, "", 0, 0))
                    CumulNegatInter = Nn(GetVariableDefineValue("CumulNegatInter", iIDPlayer, 0, 0))
                    LoansEndPerio = Nn(GetVariableDefineValue("LoansEndPerio", iIDPlayer, 0, 0))

                    sValore = ((CumulBeforTax + CumulNegatInter) / (UtilsEndPerio + NetCapital + LoansEndPerio)) * 100

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "IndexROCE_01 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Operating profit
    ''' </summary>
    Private Sub OperatingProfit_02()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "OperatingProfit"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim CumulBeforTax As Double
        Dim CumulDebtsOff As Double
        Dim CumulRevenTotal As Double
        Dim CumulNegatInter As Double

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPLayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPLayer("IDTeam"))
                    sTeamName = Nz(oRowPLayer("TeamName"))

                    CumulBeforTax = Nn(GetVariableDefineValue("CumulBeforTax", iIDPlayer, 0, 0))
                    CumulDebtsOff = Nn(GetVariableDefineValue("CumulDebtsOff", iIDPlayer, 0, 0))
                    CumulRevenTotal = Nn(GetVariableDefineValue("CumulRevenTotal", iIDPlayer, 0, 0))
                    CumulNegatInter = Nn(GetVariableDefineValue("CumulNegatInter", iIDPlayer, 0, 0))

                    sValore = ((CumulBeforTax + CumulNegatInter + CumulDebtsOff) / (CumulRevenTotal)) * 100

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "OperatingProfit_03 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' EBITDA percentuale
    ''' </summary>
    ''' 
    Private Sub EBITDAperce_03()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "EBITDAperce"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim CumulBeforTax As Double
        Dim CumulDebtsOff As Double
        Dim CumulRevenTotal As Double
        Dim CumulNegatInter As Double
        Dim CumulSinkiPlant As Double
        Dim CumulSinkiVehic As Double
        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPLayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPLayer("IDTeam"))
                    sTeamName = Nz(oRowPLayer("TeamName"))

                    CumulBeforTax = Nn(GetVariableDefineValue("CumulBeforTax", iIDPlayer, 0, 0))
                    CumulDebtsOff = Nn(GetVariableDefineValue("CumulDebtsOff", iIDPlayer, 0, 0))
                    CumulRevenTotal = Nn(GetVariableDefineValue("CumulRevenTotal", iIDPlayer, 0, 0))
                    CumulNegatInter = Nn(GetVariableDefineValue("CumulNegatInter", iIDPlayer, 0, 0))
                    CumulSinkiPlant = Nn(GetVariableDefineValue("CumulSinkiPlant", iIDPlayer, 0, 0))
                    CumulSinkiVehic = Nn(GetVariableDefineValue("CumulSinkiVehic", iIDPlayer, 0, 0))

                    sValore = ((CumulBeforTax + CumulNegatInter + CumulDebtsOff + CumulSinkiPlant + CumulSinkiVehic) / (CumulRevenTotal)) * 100

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "EBITDAperce_03 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    Private Sub CurrentRatio_04()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CurrentRatio"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim CashEndPerio As Double
        Dim DebtsEndPerio As Double
        Dim RawEndPerio As Double
        Dim StockEndPerio As Double
        Dim CrediEndPerio As Double
        Dim TaxPerioFunds As Double

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPLayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPLayer("IDTeam"))
                    sTeamName = Nz(oRowPLayer("TeamName"))

                    CashEndPerio = Nn(GetVariableDefineValue("CashEndPerio", iIDPlayer, 0, 0))
                    DebtsEndPerio = Nn(GetVariableDefineValue("DebtsEndPerio", iIDPlayer, 0, 0))
                    RawEndPerio = Nn(GetVariableDefineValue("RawEndPerio", iIDPlayer, 0, 0))
                    StockEndPerio = Nn(GetVariableDefineValue("StockEndPerio", iIDPlayer, 0, 0))
                    CrediEndPerio = Nn(GetVariableDefineValue("CrediEndPerio", iIDPlayer, 0, 0))
                    TaxPerioFunds = Nn(GetVariableDefineValue("TaxPerioFunds", iIDPlayer, 0, 0))

                    sValore = ((CashEndPerio + DebtsEndPerio + RawEndPerio + StockEndPerio) / (CrediEndPerio + TaxPerioFunds)) * 1

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "CurrentRatio_04 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Aceid test
    ''' </summary>
    Private Sub AcidTest_05()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "AcidTest"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim CashEndPerio As Double
        Dim DebtsEndPerio As Double
        Dim CrediEndPerio As Double
        Dim TaxPerioFunds As Double

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPLayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPLayer("IDTeam"))
                    sTeamName = Nz(oRowPLayer("TeamName"))

                    CashEndPerio = Nn(GetVariableDefineValue("CashEndPerio", iIDPlayer, 0, 0))
                    DebtsEndPerio = Nn(GetVariableDefineValue("DebtsEndPerio", iIDPlayer, 0, 0))
                    CrediEndPerio = Nn(GetVariableDefineValue("CrediEndPerio", iIDPlayer, 0, 0))
                    TaxPerioFunds = Nn(GetVariableDefineValue("TaxPerioFunds", iIDPlayer, 0, 0))

                    sValore = ((CashEndPerio + DebtsEndPerio) / (CrediEndPerio + TaxPerioFunds)) * 1

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "AcidTest_05 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Interest cover
    ''' </summary>
    Private Sub InterestCover_06()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "InterestCover"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim CumulBeforTax As Double
        Dim CumulNegatInter As Double

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPLayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPLayer("IDTeam"))
                    sTeamName = Nz(oRowPLayer("TeamName"))

                    CumulBeforTax = Nn(GetVariableDefineValue("CumulBeforTax", iIDPlayer, 0, 0))
                    CumulNegatInter = Nn(GetVariableDefineValue("CumulNegatInter", iIDPlayer, 0, 0))

                    sValore = ((CumulBeforTax + CumulNegatInter) / (CumulNegatInter))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "InterestCover_06 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Debts
    ''' </summary>
    Private Sub DebtX_07()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "DebtX"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim DebtsEndPerio As Double
        Dim UtilsEndPerio As Double
        Dim LoansEndPerio As Double
        Dim NetCapital As Double

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPLayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPLayer("IDTeam"))
                    sTeamName = Nz(oRowPLayer("TeamName"))

                    DebtsEndPerio = Nn(GetVariableDefineValue("DebtsEndPerio", iIDPlayer, 0, 0))
                    UtilsEndPerio = Nn(GetVariableDefineValue("UtilsEndPerio", iIDPlayer, 0, 0))
                    LoansEndPerio = Nn(GetVariableDefineValue("LoansEndPerio", iIDPlayer, 0, 0))
                    NetCapital = Nn(GetVariableState("NetCapital", iIDPlayer, "", 0, 0))

                    sValore = ((DebtsEndPerio) / (UtilsEndPerio + LoansEndPerio + NetCapital))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "DebtX_07 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Revenues per shop
    ''' </summary>
    Private Sub RevenPerShop_08()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "RevenPerShop"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim TotalRevenMarke As Double
        Dim CurreOutleCentr As Double
        Dim CurreOutleSubur As Double

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPLayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPLayer("IDTeam"))
                    sTeamName = Nz(oRowPLayer("TeamName"))

                    TotalRevenMarke = Nn(GetVariableDefineValue("TotalRevenMarke", iIDPlayer, 0, 0))
                    CurreOutleCentr = Nn(GetVariableDefineValue("CurreOutleCentr", iIDPlayer, 0, 0))
                    CurreOutleSubur = Nn(GetVariableDefineValue("CurreOutleSubur", iIDPlayer, 0, 0))

                    sValore = ((TotalRevenMarke) / (CurreOutleCentr + CurreOutleSubur))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "RevenPerShop_08 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Revenues per person
    ''' </summary>
    Private Sub RevenPerPerso_09()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "RevenPerPerso"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim TotalRevenMarke As Double
        Dim CurreSuperStaff As Double
        Dim CurreSalesPeopl As Double

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPLayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPLayer("IDTeam"))
                    sTeamName = Nz(oRowPLayer("TeamName"))

                    TotalRevenMarke = Nn(GetVariableDefineValue("TotalRevenMarke", iIDPlayer, 0, 0))
                    CurreSuperStaff = Nn(GetVariableDefineValue("CurreSuperStaff", iIDPlayer, 0, 0))
                    CurreSalesPeopl = Nn(GetVariableDefineValue("CurreSalesPeopl", iIDPlayer, 0, 0))

                    sValore = ((TotalRevenMarke) / (CurreSuperStaff + CurreSalesPeopl))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "RevenPerPerso_09 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Saturazione dei negozi
    ''' </summary>
    Private Sub SaturNegoz_10()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "SaturNegoz"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim TotalVolumSpace As Double
        Dim MaximSpaceAvail As Double

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPLayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPLayer("IDTeam"))
                    sTeamName = Nz(oRowPLayer("TeamName"))

                    TotalVolumSpace = Nn(GetVariableDefineValue("TotalVolumSpace", iIDPlayer, 0, 0))
                    MaximSpaceAvail = Nn(GetVariableDefineValue("MaximSpaceAvail", iIDPlayer, 0, 0))

                    sValore = TotalVolumSpace / MaximSpaceAvail * 100

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "SaturNegoz_10 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Wholesalers serviti
    ''' </summary>
    Private Sub WholesaleServed_11()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "WholesaleServed"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim InterMarkeRequi As Double
        Dim SalesInterMarke As Double

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPLayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPLayer("IDTeam"))
                    sTeamName = Nz(oRowPLayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        InterMarkeRequi = GetVariableDecisionPlayer(m_IDGame, "InterMarkeRequi", iIDPlayer, iIDItem)
                        SalesInterMarke = Nn(GetVariableDefineValue("SalesInterMarke", iIDPlayer, iIDItem, 0))
                        If InterMarkeRequi = 0 Then
                            sValore = 0.0
                        Else
                            sValore = SalesInterMarke / InterMarkeRequi * 100
                        End If
                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next
                Next


            End If
        Catch ex As Exception
            g_MessaggioErrore &= "PercePriceRetail_01 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Stock rotation
    ''' </summary>
    Private Sub StockRotation_12()

        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "StockRotation"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim CumulCostPurch As Double
        Dim CumulDirecCosts As Double
        Dim CumulExtraProdu As Double
        Dim CumulDeltaRaw As Double
        Dim CumulDeltaStock As Double
        Dim StockEndPerio As Double

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPLayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPLayer("IDTeam"))
                    sTeamName = Nz(oRowPLayer("TeamName"))

                    CumulCostPurch = Nn(GetVariableDefineValue("CumulCostPurch", iIDPlayer, 0, 0))
                    CumulDirecCosts = Nn(GetVariableDefineValue("CumulDirecCosts", iIDPlayer, 0, 0))
                    CumulExtraProdu = Nn(GetVariableDefineValue("CumulExtraProdu", iIDPlayer, 0, 0))
                    CumulDeltaRaw = Nn(GetVariableDefineValue("CumulDeltaRaw", iIDPlayer, 0, 0))
                    CumulDeltaStock = Nn(GetVariableDefineValue("CumulDeltaStock", iIDPlayer, 0, 0))
                    StockEndPerio = Nn(GetVariableDefineValue("StockEndPerio", iIDPlayer, 0, 0))

                    If CumulCostPurch + CumulDirecCosts + CumulExtraProdu - CumulDeltaRaw - CumulDeltaStock = 0 Then
                        sValore = 0
                    Else
                        sValore = StockEndPerio / ((CumulCostPurch + CumulDirecCosts + CumulExtraProdu - CumulDeltaRaw - CumulDeltaStock) / (12 / m_LunghezzaPeriodo)) * 365
                    End If

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next


            End If
        Catch ex As Exception
            g_MessaggioErrore &= "StockRotation_12 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    Private Sub ClientServed_13()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "ClientServed"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim MarketDemand As Double
        Dim FirstOrderShare As Double
        Dim TotalProduSold As Double

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPLayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPLayer("IDTeam"))
                    sTeamName = Nz(oRowPLayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        MarketDemand = Nn(GetVariableDefineValue("MarketDemand", 0, iIDItem, 0))
                        FirstOrderShare = Nn(GetVariableDefineValue("FirstOrderShare", iIDPlayer, iIDItem, 0))
                        TotalProduSold = Nn(GetVariableDefineValue("TotalProduSold", iIDPlayer, iIDItem, 0))

                        If MarketDemand * FirstOrderShare = 0 Then
                            sValore = 0.0
                        Else
                            sValore = TotalProduSold / (MarketDemand * FirstOrderShare) * 100
                        End If

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next
                Next


            End If
        Catch ex As Exception
            g_MessaggioErrore &= "ClientServed_13 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Perceived Value per Item
    ''' </summary>
    Private Sub PerceValueItem_14()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "PerceValueItem"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim CompoMarkeQuali As Double
        Dim IndexOfAttraction As Double
        Dim ImporCompoAttra As Double = GetVariableBoss("ImporCompoAttra", "")
        Dim ImporCompoQuali As Double = GetVariableBoss("ImporCompoQuali", "")
        Dim QualiGoodsOffer As Double

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPLayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPLayer("IDTeam"))
                    sTeamName = Nz(oRowPLayer("TeamName"))

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        CompoMarkeQuali = Nn(GetVariableDefineValue("CompoMarkeQuali", 0, iIDItem, 0))
                        IndexOfAttraction = Nn(GetVariableDefineValue("IndexOfAttraction", iIDPlayer, 0, 0))
                        QualiGoodsOffer = Nn(GetVariableDefineValue("QualiGoodsOffer", iIDPlayer, iIDItem, 0))

                        If CompoMarkeQuali = 0 OrElse IndexOfAttraction * ImporCompoAttra = 0 Then
                            sValore = 0.0
                        Else
                            sValore = ((((QualiGoodsOffer / CompoMarkeQuali) * ImporCompoQuali + (IndexOfAttraction * ImporCompoAttra)) / (ImporCompoQuali + ImporCompoAttra)) - 1.0)
                        End If

                        SaveVariableValue(iIDVariabile, iIDPlayer, iIDItem, 0, sValore)
                    Next
                Next


            End If
        Catch ex As Exception
            g_MessaggioErrore &= "PerceValueItem_14 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    Private Sub PerceQuality_15()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "PerceQuality"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim AbsolQualiProce As Double = GetVariableState("AbsolQualiProce", 0, "", 0, 0)
        Dim ProduQualiProce As Double

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPLayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPLayer("IDTeam"))
                    sTeamName = Nz(oRowPLayer("TeamName"))


                    ProduQualiProce = Nn(GetVariableDefineValue("ProduQualiProce", iIDPlayer, 0, 0))
                    sValore = ((ProduQualiProce / AbsolQualiProce)) * -1.0
                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)

                Next

            End If
        Catch ex As Exception
            g_MessaggioErrore &= "PerceQuality_15 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

#End Region

#Region "EVA"
    Private Sub CalcolaEVA()
        Equity_00()
        LTCapital_02()
        TassePerioCumul_03()
        OperaProfiAfterTax_04()
        ReturOnCapital_05()
        CostOfEquit_06()
        InterRateYear_07()
        InterNetRate_08()
        ActualCostOfDebt_09()
        WeighCostCapit_10()
        EVA1_11()
        ReturnToInves_12()
        CostOfCapital_13()
    End Sub

    ''' <summary>
    ''' Equity
    ''' </summary>
    Private Sub Equity_00()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "Equity"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    sValore = Nn(GetVariableState("NetCapital", iIDPlayer, "", 0, 0)) _
                            + Nn(GetVariableDefineValue("UtilsEndPerio", iIDPlayer, 0, 0))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "Equity_00 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Long term capital
    ''' </summary>
    Private Sub LTCapital_02()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "LTCapital"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    sValore = Nn(GetVariableState("NetCapital", iIDPlayer, "", 0, 0)) _
                            + Nn(GetVariableDefineValue("UtilsEndPerio", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("LoansEndPerio", iIDPlayer, 0, 0))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "LTCapital_02 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Tax period cumulato
    ''' </summary>
    Private Sub TassePerioCumul_03()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TassePerioCumul"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    sValore = (Nn(GetVariableDefineValue("CumulBeforTax", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("CumulNegatInter", iIDPlayer, 0, 0))) _
                            / 2

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "TassePerioCumul_03 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Operative profit after tax
    ''' </summary>
    Private Sub OperaProfiAfterTax_04()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "OperaProfiAfterTax"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim CumulBeforTax As Double
        Dim CumulNegatInter As Double

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    CumulBeforTax = Nn(GetVariableDefineValue("CumulBeforTax", iIDPlayer, 0, 0))
                    CumulNegatInter = Nn(GetVariableDefineValue("CumulNegatInter", iIDPlayer, 0, 0))

                    sValore = CumulBeforTax + CumulNegatInter - (CumulBeforTax + CumulNegatInter) / 2

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "OperaProfiAfterTax_04 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Return on capital
    ''' </summary>
    Private Sub ReturOnCapital_05()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "ReturOnCapital"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim OperaProfiAfterTax As Double
        Dim LTCapital As Double

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    OperaProfiAfterTax = Nn(GetVariableDefineValue("OperaProfiAfterTax", iIDPlayer, 0, 0))
                    LTCapital = Nn(GetVariableDefineValue("LTCapital", iIDPlayer, 0, 0))

                    sValore = OperaProfiAfterTax / LTCapital * 100

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "ReturOnCapital_05 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Cost of Equity
    ''' </summary>
    Private Sub CostOfEquit_06()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CostOfEquit"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = "10"

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "CostOfEquit_06 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Interest annual rate
    ''' </summary>
    Private Sub InterRateYear_07()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "InterRateYear"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim TaxRateCredi As Double

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    TaxRateCredi = GetVariableBoss("TaxRateCredi", "TaxRateCredi_" & sTeamName)
                    sValore = TaxRateCredi * (12 / m_LunghezzaPeriodo)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "InterRateYear_07 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Interest Net rate
    ''' </summary>
    Private Sub InterNetRate_08()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "InterNetRate"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim InterRateYear As Double

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    InterRateYear = GetVariableDefineValue("InterRateYear", iIDPlayer, 0, 0)
                    sValore = InterRateYear / 2

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "InterNetRate_08 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Actual cost of debt
    ''' </summary>
    Private Sub ActualCostOfDebt_09()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "ActualCostOfDebt"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim InterRateYear As Double
        Dim InterNetRate As Double

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    InterRateYear = GetVariableDefineValue("InterRateYear", iIDPlayer, 0, 0)
                    InterNetRate = GetVariableDefineValue("InterNetRate", iIDPlayer, 0, 0)

                    sValore = InterRateYear - InterNetRate

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "ActualCostOfDebt_09 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Weighted cost of capital
    ''' </summary>
    Private Sub WeighCostCapit_10()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "WeighCostCapit"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim InterRateYear As Double
        Dim InterNetRate As Double
        Dim LoansEndPerio As Double
        Dim NetCapital As Double
        Dim CostOfEquit As Double
        Dim UtilsEndPerio As Double
        Dim LTCapital As Double

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    InterRateYear = GetVariableDefineValue("InterRateYear", iIDPlayer, 0, 0)
                    InterNetRate = GetVariableDefineValue("InterNetRate", iIDPlayer, 0, 0)
                    LoansEndPerio = GetVariableDefineValue("LoansEndPerio", iIDPlayer, 0, 0)
                    NetCapital = GetVariableState("NetCapital", iIDPlayer, "", 0, 0)
                    CostOfEquit = GetVariableDefineValue("CostOfEquit", iIDPlayer, 0, 0)
                    UtilsEndPerio = GetVariableDefineValue("UtilsEndPerio", iIDPlayer, 0, 0)
                    LTCapital = GetVariableDefineValue("LTCapital", iIDPlayer, 0, 0)

                    sValore = ((InterRateYear - InterNetRate) * (LoansEndPerio / NetCapital)) + (CostOfEquit * (NetCapital + UtilsEndPerio) / LTCapital)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "WeighCostCapit_10 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' EVA
    ''' </summary>
    Private Sub EVA1_11()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "EVA1"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim WeighCostCapit As Double
        Dim ReturOnCapital As Double
        Dim LTCapital As Double

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    WeighCostCapit = GetVariableDefineValue("WeighCostCapit", iIDPlayer, 0, 0)
                    ReturOnCapital = GetVariableDefineValue("ReturOnCapital", iIDPlayer, 0, 0)
                    LTCapital = GetVariableDefineValue("LTCapital", iIDPlayer, 0, 0)

                    sValore = LTCapital * (ReturOnCapital - WeighCostCapit) / 100

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "EVA1_11 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Return on investment
    ''' </summary>
    Private Sub ReturnToInves_12()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "ReturnToInves"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim CumulBeforTax As Double
        Dim CumulNegatInter As Double

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    CumulBeforTax = GetVariableDefineValue("CumulBeforTax", iIDPlayer, 0, 0)
                    CumulNegatInter = GetVariableDefineValue("CumulNegatInter", iIDPlayer, 0, 0)

                    sValore = CumulBeforTax + CumulNegatInter - (CumulBeforTax + CumulNegatInter) / 2

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "ReturnToInves_12 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    Private Sub CostOfCapital_13()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CostOfCapital"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim LTCapital As Double
        Dim WeighCostCapit As Double

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    LTCapital = GetVariableDefineValue("LTCapital", iIDPlayer, 0, 0)
                    WeighCostCapit = GetVariableDefineValue("WeighCostCapit", iIDPlayer, 0, 0)

                    sValore = LTCapital * (WeighCostCapit / 100)

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "CostOfCapital -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

#End Region

#Region "SOMME E CONTI ECONOMICI PeL"

    Private Sub CalcolaContiEconomiciPeL()
        TotalLiabilities_00()
        FattuTotalPerio_01()
        SalariSomma_02()
        AltreSpese_03()
        CostTotProduz_04()
        CostoVenduto_05()
        GrossProfit_06()
        CostoLogistica_07()
        CostoCommerciale_08()
        OperativeMargin_09()
        CostoFinanza_10()
        FattuTotalCumul_11()
        CostoTotalPrCumul_12()
        CostoVenduCumul_13()
        CostoLogisCumul_14()
        CostoCommeCumul_15()
        CostoFinanCumul_16()
        CumulGrossProfit_17()
        OperaMargiCumul_18()

    End Sub

    ''' <summary>
    ''' Fonti di finanziamento
    ''' </summary>
    Private Sub TotalLiabilities_00()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "TotalLiabilities"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    sValore = Nn(GetVariableState("NetCapital", iIDPlayer, "", 0, 0)) _
                            + Nn(GetVariableDefineValue("UtilsEndPerio", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("LoansEndPerio", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("CrediEndPerio", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("TaxPerioFunds", iIDPlayer, 0, 0))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "TotalLiabilities_00 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Fatturato totale del periodo
    ''' </summary>
    Private Sub FattuTotalPerio_01()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "FattuTotalPerio"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Dim iIDItem As Integer
        Dim sItemName As String

        Dim ProduRevenInter As Double
        Dim ProduRevenMarke As Double

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)


                For Each oRowPLayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPLayer("IDTeam"))
                    sTeamName = Nz(oRowPLayer("TeamName"))

                    sValore = "0"

                    For Each oRowItem As DataRow In m_DataTableItems.Rows
                        iIDItem = Nni(oRowItem("IDVariable"))
                        sItemName = Nz(oRowItem("VariableName"))

                        ProduRevenInter = Nn(GetVariableDefineValue("ProduRevenInter", iIDPlayer, iIDItem, 0))
                        ProduRevenMarke = Nn(GetVariableDefineValue("ProduRevenMarke", iIDPlayer, iIDItem, 0))

                        sValore = Nn(sValore) + ProduRevenInter + ProduRevenMarke
                    Next

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "FattuTotalPerio_01 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Salari totali del periodo
    ''' </summary>
    Private Sub SalariSomma_02()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "SalariSomma"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    sValore = Nn(GetVariableDefineValue("CostsPersoPoint", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("CostsPersoStaff", iIDPlayer, 0, 0))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "SalariSomma_02 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Altre spese del periodo
    ''' </summary>
    Private Sub AltreSpese_03()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "AltreSpese"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    sValore = "0"
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = Nn(GetVariableDefineValue("AddesTotalAllow", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("TrainServiAllow", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("TotalMarkeAllow", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("AdverAllow", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("GreenAllow", iIDPlayer, 0, 0))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "AltreSpese_03 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Costo totale di produzione del periodo
    ''' </summary>
    Private Sub CostTotProduz_04()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CostTotProduz"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    sValore = Nn(GetVariableDefineValue("FixedProduCosts", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("SinkiFundPlant", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("LeasiTotalCost", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("TechnCostsPerio", iIDPlayer, 0, 0))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "CostTotProduz_04 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Costo del venduto del periodo
    ''' </summary>
    Private Sub CostoVenduto_05()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CostoVenduto"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    sValore = Nn(GetVariableDefineValue("CostPurchTotal", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("DirecTotalCosts", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("ExtraMoneyAllow", iIDPlayer, 0, 0)) _
                            - Nn(GetVariableDefineValue("DeltaRawValue", iIDPlayer, 0, 0)) _
                            - Nn(GetVariableDefineValue("DeltaStockValue", iIDPlayer, 0, 0))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "CostoVenduto_05 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Gross profit del Periodo
    ''' </summary>
    Private Sub GrossProfit_06()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "GrossProfit"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    sValore = Nn(GetVariableDefineValue("FattuTotalPerio", iIDPlayer, 0, 0)) _
                            - Nn(GetVariableDefineValue("CostoVenduto", iIDPlayer, 0, 0))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "GrossProfit_06 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Costo di logistica del periodo
    ''' </summary>
    Private Sub CostoLogistica_07()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CostoLogistica"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    sValore = Nn(GetVariableDefineValue("TotalTransCosts", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("SinkiFundVehic", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("TotalWarehCosts", iIDPlayer, 0, 0))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "CostoLogistica_07 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Costi commerciali del periodo
    ''' </summary>
    Private Sub CostoCommerciale_08()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CostoCommerciale"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    sValore = "0"

                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = Nn(GetVariableDefineValue("TotalCostsOutle", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("SalariSomma", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("AltreSpese", iIDPlayer, 0, 0))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "CostoCommerciale_08 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Margine operativo del periodo
    ''' </summary>
    Private Sub OperativeMargin_09()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "OperativeMargin"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = Nn(GetVariableDefineValue("GrossProfit", iIDPlayer, 0, 0)) _
                            - Nn(GetVariableDefineValue("CostTotProduz", iIDPlayer, 0, 0)) _
                            - Nn(GetVariableDefineValue("CostoLogistica", iIDPlayer, 0, 0)) _
                            - Nn(GetVariableDefineValue("CostoCommerciale", iIDPlayer, 0, 0))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "OperativeMargin_09 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Costi della finanza del periodo
    ''' </summary>
    Private Sub CostoFinanza_10()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CostoFinanza"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    sValore = Nn(GetVariableDefineValue("TotalNegatInter", iIDPlayer, 0, 0)) _
                            - Nn(GetVariableDefineValue("TotalPositInter", iIDPlayer, 0, 0))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "CostoFinanza_10 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Fatturato totale cumulativo
    ''' </summary>
    Private Sub FattuTotalCumul_11()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "FattuTotalCumul"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    sValore = Nn(GetVariableDefineValue("CumulRevenTotal", iIDPlayer, 0, 0)) _
                            - Nn(GetVariableDefineValue("CumulPositInter", iIDPlayer, 0, 0))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "FattuTotalCumul_11 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Costo del venduto cumulato
    ''' </summary>
    Private Sub CostoTotalPrCumul_12()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CostoTotalPrCumul"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))
                    sValore = Nn(GetVariableDefineValue("CumulFixedCosts", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("CumulSinkiPlant", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("CumulLeasiCosts", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("CumulCostsTechn", iIDPlayer, 0, 0))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "CostoTotalPrCumul_12 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Costo del venduto - cumulato
    ''' </summary>
    Private Sub CostoVenduCumul_13()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CostoVenduCumul"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = Nn(GetVariableDefineValue("CumulCostPurch", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("CumulDirecCosts", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("CumulExtraProdu", iIDPlayer, 0, 0)) _
                            - Nn(GetVariableDefineValue("CumulDeltaRaw", iIDPlayer, 0, 0)) _
                            - Nn(GetVariableDefineValue("CumulDeltaStock", iIDPlayer, 0, 0))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "CostoVenduCumul_13 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Costo logistica cumulato
    ''' </summary>
    Private Sub CostoLogisCumul_14()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CostoLogisCumul"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = Nn(GetVariableDefineValue("CumulTransCosts", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("CumulSinkiVehic", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("CumulWarehCosts", iIDPlayer, 0, 0))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "CostoLogisCumul_14 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Costo commerciale cumulato
    ''' </summary>
    Private Sub CostoCommeCumul_15()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CostoCommeCumul"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = Nn(GetVariableDefineValue("CumulOutleCosts", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("CumulExpenCosts", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("CumulPersoCosts", iIDPlayer, 0, 0))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "CostoCommeCumul_15 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Costo finanza cumulato
    ''' </summary>
    Private Sub CostoFinanCumul_16()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CostoFinanCumul"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = Nn(GetVariableDefineValue("CumulNegatInter", iIDPlayer, 0, 0)) _
                            - Nn(GetVariableDefineValue("CumulPositInter", iIDPlayer, 0, 0))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "CostoFinanCumul_16 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Gross profit - cumulato
    ''' </summary>
    Private Sub CumulGrossProfit_17()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "CumulGrossProfit"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = Nn(GetVariableDefineValue("FattuTotalCumul", iIDPlayer, 0, 0)) _
                            - Nn(GetVariableDefineValue("CostoVenduCumul", iIDPlayer, 0, 0))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "CumulGrossProfit_17 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

    Private Sub OperaMargiCumul_18()
        Dim iIDVariabile As Integer
        Dim sNomeVariabileEntrata As String = "OperaMargiCumul"

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                iIDVariabile = CheckVariableExists(sNomeVariabileEntrata, m_IDGame)

                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    sValore = Nn(GetVariableDefineValue("CumulGrossProfit", iIDPlayer, 0, 0)) _
                            - Nn(GetVariableDefineValue("CostoTotalPrCumul", iIDPlayer, 0, 0)) _
                            - Nn(GetVariableDefineValue("CostoLogisCumul", iIDPlayer, 0, 0)) _
                            - Nn(GetVariableDefineValue("CostoCommeCumul", iIDPlayer, 0, 0))

                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)
                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "OperaMargiCumul_18 -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

#End Region

#Region "CALCOLO DELLE VARIABILI DI FLUSSO"

    Private Sub CalculateVariabiliFlusso()
        Dim sValore As String
        Dim sNomeVariabile As String = ""
        Dim sSQL As String

        Try
            For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows

                sValore = Nn(GetVariableState("VariaExpenCosts", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        + Nn(GetVariableDefineValue("AddesTotalAllow", Nni(oRowPlayer("IDTeam")), 0, 0)) _
                        + Nn(GetVariableDefineValue("AdditCostsPerio", Nni(oRowPlayer("IDTeam")), 0, 0)) _
                        - Nn(GetVariableDefineValue("DischExpenCosts", Nni(oRowPlayer("IDTeam")), 0, 0)) _
                        + Nn(GetVariableDefineValue("GreenAllow", Nni(oRowPlayer("IDTeam")), 0, 0)) _
                        + Nn(GetVariableDefineValue("PromoInvesPerio", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("VariaExpenCosts", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                sValore = Nn(GetVariableState("VariaAutomTechn", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        + Nn(GetVariableDefineValue("TechnCostsPerio", Nni(oRowPlayer("IDTeam")), 0, 0)) _
                        - Nn(GetVariableDefineValue("DischCostsTechn", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("VariaAutomTechn", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                sValore = Nn(GetVariableState("TotalCentrStore", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        - Nn(GetVariableDefineValue("CentrAlienAllow", Nni(oRowPlayer("IDTeam")), 0, 0)) _
                        + Nn(GetVariableDefineValue("CentrStoreAllow", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("TotalCentrStore", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                sValore = Nn(GetVariableState("VariaDebtsOff", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        + Nn(GetVariableDefineValue("DebtsWrittOff", Nni(oRowPlayer("IDTeam")), 0, 0)) _
                        + Nn(GetVariableDefineValue("WriteOffLost", Nni(oRowPlayer("IDTeam")), 0, 0)) _
                        - Nn(GetVariableDefineValue("DischDebtsOff", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("VariaDebtsOff", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                sValore = Nn(GetVariableState("BankLevel", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        + Nn(GetVariableDefineValue("IncreBankCash", Nni(oRowPlayer("IDTeam")), 0, 0)) _
                        - Nn(GetVariableDefineValue("DecreBankCash", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("BankLevel", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                sValore = Nn(GetVariableState("Debtors", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        + Nn(GetVariableDefineValue("IncreDebts", Nni(oRowPlayer("IDTeam")), 0, 0)) _
                        - Nn(GetVariableDefineValue("DecreDebts", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("Debtors", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                For Each oRowItem As DataRow In m_DataTableItems.Rows
                    sNomeVariabile = "var_" & Nz(oRowPlayer("TeamName")) & "_HoursPerItem_" & Nz(oRowItem("VariableName"))
                    sValore = Nn(GetVariableBoss("HoursPerItem", sNomeVariabile)) _
                            - Nn(GetVariableDefineValue("DecreHoursItem", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0))
                    SaveVariableBossValue("HoursPerItem", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0, sValore, sNomeVariabile)
                Next

                sValore = Nn(GetVariableState("VariaDeltaRaw", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        + Nn(GetVariableDefineValue("DeltaRawValue", Nni(oRowPlayer("IDTeam")), 0, 0)) _
                        - Nn(GetVariableDefineValue("DischDeltaRaw", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("VariaDeltaRaw", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                For Each oRowItem As DataRow In m_DataTableItems.Rows
                    sNomeVariabile = "var_" & Nz(oRowPlayer("TeamName")) & "_CumulDeltaSales_" & Nz(oRowItem("VariableName"))
                    sValore = Nn(GetVariableState("CumulDeltaSales", Nni(oRowPlayer("IDTeam")), sNomeVariabile, Nni(oRowItem("IDVariable")), 0)) _
                            + Nn(GetVariableDefineValue("DeltaSalesPrice", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0))
                    SaveVariableStateValue("CumulDeltaSales", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0, sValore)
                Next

                sValore = Nn(GetVariableState("VariaDeltaStock", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        + Nn(GetVariableDefineValue("DeltaStockValue", Nni(oRowPlayer("IDTeam")), 0, 0)) _
                        - Nn(GetVariableDefineValue("DischDeltaStock", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("VariaDeltaStock", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                sValore = Nn(GetVariableState("VariaCostPurch", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        + Nn(GetVariableDefineValue("CostPurchTotal", Nni(oRowPlayer("IDTeam")), 0, 0)) _
                        - Nn(GetVariableDefineValue("DischCostPurch", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("VariaCostPurch", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                sValore = Nn(GetVariableState("PreviFixedCosts", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        - Nn(GetVariableDefineValue("DischCostsFixed", Nni(oRowPlayer("IDTeam")), 0, 0)) _
                        + Nn(GetVariableDefineValue("FixedProduCosts", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("PreviFixedCosts", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                sValore = Nn(GetVariableState("VariaDirecCosts", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        + Nn(GetVariableDefineValue("DirecTotalCosts", Nni(oRowPlayer("IDTeam")), 0, 0)) _
                        - Nn(GetVariableDefineValue("DischDirecCosts", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("VariaDirecCosts", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                sValore = Nn(GetVariableState("VariaFixedCosts", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        + Nn(GetVariableDefineValue("ProduFixedCosts", Nni(oRowPlayer("IDTeam")), 0, 0)) _
                        - Nn(GetVariableDefineValue("DischFixedCosts", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("VariaFixedCosts", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                sValore = Nn(GetVariableState("VariaLeasiCosts", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        - Nn(GetVariableDefineValue("DischLeasiCosts", Nni(oRowPlayer("IDTeam")), 0, 0)) _
                        + Nn(GetVariableDefineValue("LeasiTotalCost", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("VariaLeasiCosts", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                sValore = Nn(GetVariableState("ActuaMaximLoans", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        + Nn(GetVariableDefineValue("IncreMaximLoans", Nni(oRowPlayer("IDTeam")), 0, 0)) _
                        - Nn(GetVariableDefineValue("DischMaximLoans", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("ActuaMaximLoans", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                sValore = Nn(GetVariableState("VariaPersoCosts", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        + Nn(GetVariableDefineValue("TotalCostsPerso", Nni(oRowPlayer("IDTeam")), 0, 0)) _
                        - Nn(GetVariableDefineValue("DischPersoCosts", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("VariaPersoCosts", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                sValore = Nn(GetVariableState("VariaPositInter", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        + Nn(GetVariableDefineValue("TotalPositInter", Nni(oRowPlayer("IDTeam")), 0, 0)) _
                        - Nn(GetVariableDefineValue("DischPositInter", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("VariaPositInter", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                For Each oRowItem As DataRow In m_DataTableItems.Rows
                    sNomeVariabile = "var_" & Nz(oRowPlayer("TeamName")) & "_PreviProduCosts_" & Nz(oRowItem("VariableName"))
                    sValore = Nn(GetVariableState("PreviProduCosts", Nni(oRowPlayer("IDTeam")), sNomeVariabile, Nni(oRowItem("IDVariable")), 0)) _
                            - Nn(GetVariableDefineValue("DischPreviCosts", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0)) _
                            + Nn(GetVariableDefineValue("TotalProduCosts", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0))
                    SaveVariableStateValue("PreviProduCosts", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0, sValore)
                Next

                For Each oRowItem As DataRow In m_DataTableItems.Rows
                    sNomeVariabile = "var_" & Nz(oRowPlayer("TeamName")) & "_PreviInterPrice_" & Nz(oRowItem("VariableName"))
                    sValore = Nn(GetVariableState("PreviInterPrice", Nni(oRowPlayer("IDTeam")), sNomeVariabile, Nni(oRowItem("IDVariable")), 0)) _
                            + Nn(GetVariableDefineValue("InterPriceAllow", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0)) _
                            - Nn(GetVariableDefineValue("DischPreviPrint", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0))
                    SaveVariableStateValue("PreviInterPrice", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0, sValore)
                Next

                For Each oRowItem As DataRow In m_DataTableItems.Rows
                    sNomeVariabile = "var_" & Nz(oRowPlayer("TeamName")) & "_PreviSalesPrice_" & Nz(oRowItem("VariableName"))
                    sValore = Nn(GetVariableState("PreviSalesPrice", Nni(oRowPlayer("IDTeam")), sNomeVariabile, Nni(oRowItem("IDVariable")), 0)) _
                            - Nn(GetVariableDefineValue("DischPreviPrice", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0)) _
                            + Nn(GetVariableDefineValue("SalesPriceAllow", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0))
                    SaveVariableStateValue("PreviSalesPrice", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0, sValore)
                Next

                sValore = Nn(GetVariableState("VariaRevenTotal", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        - Nn(GetVariableDefineValue("DischRevenTotal", Nni(oRowPlayer("IDTeam")), 0, 0)) _
                        + Nn(GetVariableDefineValue("TotalRevenPerio", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("VariaRevenTotal", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                sValore = Nn(GetVariableState("VariaSinkiPlant", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        + Nn(GetVariableDefineValue("SinkiFundPlant", Nni(oRowPlayer("IDTeam")), 0, 0)) _
                        - Nn(GetVariableDefineValue("DischSinkiPlant", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("VariaSinkiPlant", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                sValore = Nn(GetVariableState("VariaTransCosts", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        - Nn(GetVariableDefineValue("DischTransCosts", Nni(oRowPlayer("IDTeam")), 0, 0)) _
                        + Nn(GetVariableDefineValue("TotalTransCosts", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("VariaTransCosts", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                sValore = Nn(GetVariableState("VariaExtraProdu", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        + Nn(GetVariableDefineValue("ExtraMoneyAllow", Nni(oRowPlayer("IDTeam")), 0, 0)) _
                        - Nn(GetVariableDefineValue("DischExtraProdu", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("VariaExtraProdu", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                sValore = Nn(GetVariableState("CumulAdverLevel", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        + Nn(GetVariableDefineValue("IncreAdverCumul", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("CumulAdverLevel", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                For iIndexAge As Integer = 1 To m_DataTableAge.Rows.Count
                    sNomeVariabile = "var_" & Nz(oRowPlayer("TeamName")) & "_LeasePerAge_" & iIndexAge
                    sValore = Nn(GetVariableState("LeasePerAge", Nni(oRowPlayer("IDTeam")), sNomeVariabile, 0, iIndexAge)) _
                            + Nn(GetVariableDefineValue("IncreAgeLease", Nni(oRowPlayer("IDTeam")), 0, iIndexAge))
                    SaveVariableStateValue("LeasePerAge", Nni(oRowPlayer("IDTeam")), 0, iIndexAge, sValore)
                Next

                For iIndexAge As Integer = 1 To m_DataTableAge.Rows.Count
                    sNomeVariabile = "var_" & Nz(oRowPlayer("TeamName")) & "_PlantPerAge_" & iIndexAge
                    sValore = Nn(GetVariableState("PlantPerAge", Nni(oRowPlayer("IDTeam")), sNomeVariabile, 0, iIndexAge)) _
                            + Nn(GetVariableDefineValue("IncreAgePlant", Nni(oRowPlayer("IDTeam")), 0, iIndexAge))
                    SaveVariableStateValue("PlantPerAge", Nni(oRowPlayer("IDTeam")), 0, iIndexAge, sValore)
                Next

                For iIndexAge As Integer = 1 To m_DataTableAge.Rows.Count
                    sNomeVariabile = "var_" & Nz(oRowPlayer("TeamName")) & "_TotalVehicOwned_" & iIndexAge
                    sValore = Nn(GetVariableState("TotalVehicOwned", Nni(oRowPlayer("IDTeam")), sNomeVariabile, 0, iIndexAge)) _
                            + Nn(GetVariableDefineValue("IncreAgeVehic", Nni(oRowPlayer("IDTeam")), 0, iIndexAge))
                    SaveVariableStateValue("TotalVehicOwned", Nni(oRowPlayer("IDTeam")), 0, iIndexAge, sValore)
                Next

                sValore = Nn(GetVariableState("AveraAdverInves", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        + Nn(GetVariableDefineValue("IncreAveraAdver", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("AveraAdverInves", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                sValore = Nn(GetVariableState("AveraGreenInves", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        + Nn(GetVariableDefineValue("IncreAveraGreen", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("AveraGreenInves", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                sValore = Nn(GetVariableState("AveraTechnInves", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        + Nn(GetVariableDefineValue("IncreAveraTechn", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("AveraTechnInves", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                sValore = Nn(GetVariableState("AveraTrainInves", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        + Nn(GetVariableDefineValue("IncreAveraTrain", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("AveraTrainInves", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                sValore = Nn(GetVariableState("Creditors", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        + Nn(GetVariableDefineValue("IncreCredits", Nni(oRowPlayer("IDTeam")), 0, 0)) _
                        - Nn(GetVariableDefineValue("DecreCredits", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("Creditors", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                For Each oRowItem As DataRow In m_DataTableItems.Rows
                    sNomeVariabile = "var_" & Nz(oRowPlayer("TeamName")) & "_StockFinisGoods_" & Nz(oRowItem("VariableName"))
                    sValore = Nn(GetVariableState("StockFinisGoods", Nni(oRowPlayer("IDTeam")), sNomeVariabile, Nni(oRowItem("IDVariable")), 0)) _
                            + Nn(GetVariableDefineValue("IncreGoodsStock", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0))
                    SaveVariableStateValue("StockFinisGoods", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0, sValore)
                Next

                sValore = Nn(GetVariableState("CumulGreenLevel", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        + Nn(GetVariableDefineValue("IncreGreenCumul", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("CumulGreenLevel", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                sValore = Nn(GetVariableState("PointTrainLevel", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        + Nn(GetVariableDefineValue("IncrePointLevel", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("PointTrainLevel", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                For Each oRowItem As DataRow In m_DataTableItems.Rows
                    sNomeVariabile = "var_" & Nz(oRowPlayer("TeamName")) & "_QualiStockFinis_" & Nz(oRowItem("VariableName"))
                    sValore = Nn(GetVariableState("QualiStockFinis", Nni(oRowPlayer("IDTeam")), sNomeVariabile, Nni(oRowItem("IDVariable")), 0)) _
                            + Nn(GetVariableDefineValue("IncreQualiFinis", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0))
                    SaveVariableStateValue("QualiStockFinis", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0, sValore)
                Next

                For Each oRowItem As DataRow In m_DataTableItems.Rows
                    sNomeVariabile = "var_" & Nz(oRowPlayer("TeamName")) & "_RawStockQuali_" & Nz(oRowItem("VariableName"))
                    sValore = Nn(GetVariableState("RawStockQuali", Nni(oRowPlayer("IDTeam")), sNomeVariabile, Nni(oRowItem("IDVariable")), 0)) _
                            + Nn(GetVariableDefineValue("IncreQualiStock", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0))
                    SaveVariableStateValue("RawStockQuali", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0, sValore)
                Next

                For Each oRowItem As DataRow In m_DataTableItems.Rows
                    sNomeVariabile = "var_" & Nz(oRowPlayer("TeamName")) & "_StockRawMater_" & Nz(oRowItem("VariableName"))
                    sValore = Nn(GetVariableState("StockRawMater", Nni(oRowPlayer("IDTeam")), sNomeVariabile, Nni(oRowItem("IDVariable")), 0)) _
                            + Nn(GetVariableDefineValue("IncreQuantStock", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0))
                    SaveVariableStateValue("StockRawMater", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0, sValore)
                Next

                sValore = Nn(GetVariableState("StaffTrainLevel", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        + Nn(GetVariableDefineValue("IncreStaffLevel", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("StaffTrainLevel", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                sValore = Nn(GetVariableState("CumulTechnLevel", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        + Nn(GetVariableDefineValue("IncreTechnCumul", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("CumulTechnLevel", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                sValore = Nn(GetVariableState("CumulTrainLevel", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        + Nn(GetVariableDefineValue("IncreTrainCumul", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("CumulTrainLevel", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                For Each oRowItem As DataRow In m_DataTableItems.Rows
                    sNomeVariabile = "var_" & Nz(oRowPlayer("TeamName")) & "_ValueRawMater_" & Nz(oRowItem("VariableName"))
                    sValore = Nn(GetVariableState("ValueRawMater", Nni(oRowPlayer("IDTeam")), sNomeVariabile, Nni(oRowItem("IDVariable")), 0)) _
                            + Nn(GetVariableDefineValue("IncreValueRaw", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0))
                    SaveVariableStateValue("ValueRawMater", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0, sValore)
                Next

                For Each oRowItem As DataRow In m_DataTableItems.Rows
                    sNomeVariabile = "var_" & Nz(oRowPlayer("TeamName")) & "_LastFinisValue_" & Nz(oRowItem("VariableName"))
                    sValore = Nn(GetVariableState("LastFinisValue", Nni(oRowPlayer("IDTeam")), sNomeVariabile, Nni(oRowItem("IDVariable")), 0)) + Nn(GetVariableDefineValue("IncreValueStock", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0))
                    SaveVariableStateValue("LastFinisValue", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0, sValore)
                Next

                For Each oRowItem As DataRow In m_DataTableItems.Rows
                    sNomeVariabile = "var_" & Nz(oRowPlayer("TeamName")) & "_PreviInterPrice_" & Nz(oRowItem("VariableName"))
                    sValore = Nn(GetVariableState("PreviInterPrice", Nni(oRowPlayer("IDTeam")), sNomeVariabile, Nni(oRowItem("IDVariable")), 0)) _
                            - Nn(GetVariableDefineValue("DischPreviPrint", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0)) _
                            + Nn(GetVariableDefineValue("InterPriceAllow", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0))
                    SaveVariableStateValue("PreviInterPrice", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0, sValore)
                Next

                sValore = Nn(GetVariableState("LoansLevel", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        + Nn(GetVariableDefineValue("LoansAllow", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("LoansLevel", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                For Each oRowItem As DataRow In m_DataTableItems.Rows
                    sNomeVariabile = "var_" & Nz(oRowPlayer("TeamName")) & "_PreviMarkeShare_" & Nz(oRowItem("VariableName"))
                    sValore = Nn(GetVariableState("PreviMarkeShare", Nni(oRowPlayer("IDTeam")), sNomeVariabile, Nni(oRowItem("IDVariable")), 0)) _
                            - Nn(GetVariableDefineValue("DischMarkeShare", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0)) _
                            + Nn(GetVariableDefineValue("MarketShare", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0))
                    SaveVariableStateValue("PreviMarkeShare", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0, sValore)
                Next

                sValore = Nn(GetVariableState("TaxFunds", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        + Nn(GetVariableDefineValue("TaxOfPerio", Nni(oRowPlayer("IDTeam")), 0, 0)) _
                        - Nn(GetVariableDefineValue("TaxPayment", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("TaxFunds", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                sValore = Nn(GetVariableState("CumulProfi", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        + Nn(GetVariableDefineValue("NegatTaxPayme", Nni(oRowPlayer("IDTeam")), 0, 0)) _
                        + Nn(GetVariableDefineValue("ProfiAfterTax", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("CumulProfi", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                sValore = Nn(GetVariableState("TotalPointPerso", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        + Nn(GetVariableDefineValue("NewPointAllow", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("TotalPointPerso", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                sValore = Nn(GetVariableState("TotalStaffPerso", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        + Nn(GetVariableDefineValue("NewStaffAllow", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("TotalStaffPerso", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                sValore = Nn(GetVariableState("TotalPerifStore", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        - Nn(GetVariableDefineValue("PerifScrapAllow", Nni(oRowPlayer("IDTeam")), 0, 0)) _
                        + Nn(GetVariableDefineValue("PerifStoreAllow", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("TotalPerifStore", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                For Each oRowItem As DataRow In m_DataTableItems.Rows
                    sNomeVariabile = "var_" & Nz(oRowPlayer("TeamName")) & "_PurchDelivNIC_" & Nz(oRowItem("VariableName"))
                    sValore = Nn(GetVariableState("PurchDelivNIC", Nni(oRowPlayer("IDTeam")), sNomeVariabile, Nni(oRowItem("IDVariable")), 0)) _
                            - Nn(GetVariableDefineValue("PortfDelivNIC", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0)) _
                            + Nn(GetVariableDefineValue("PortfPurchNIC", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0))
                    SaveVariableStateValue("PurchDelivNIC", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0, sValore)
                Next

                sValore = Nn(GetVariableState("VariaBeforTax", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        + Nn(GetVariableDefineValue("ProfiBeforTax", Nni(oRowPlayer("IDTeam")), 0, 0)) _
                        - Nn(GetVariableDefineValue("DischBeforTax", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("VariaBeforTax", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                sValore = Nn(GetVariableState("VariaSinkiVehic", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        + Nn(GetVariableDefineValue("SinkiFundVehic", Nni(oRowPlayer("IDTeam")), 0, 0)) _
                        - Nn(GetVariableDefineValue("DischSinkiVehic", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("VariaSinkiVehic", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                sValore = Nn(GetVariableState("VariaOutleCosts", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        + Nn(GetVariableDefineValue("TotalCostsOutle", Nni(oRowPlayer("IDTeam")), 0, 0)) _
                        - Nn(GetVariableDefineValue("DischOutleCosts", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("VariaOutleCosts", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                sValore = Nn(GetVariableState("VariaNegatInter", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        + Nn(GetVariableDefineValue("TotalNegatInter", Nni(oRowPlayer("IDTeam")), 0, 0)) _
                        - Nn(GetVariableDefineValue("DischNegatInter", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("VariaNegatInter", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                sValore = Nn(GetVariableState("VariaWarehCosts", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
                        + Nn(GetVariableDefineValue("TotalWarehCosts", Nni(oRowPlayer("IDTeam")), 0, 0)) _
                        - Nn(GetVariableDefineValue("DischWarehCosts", Nni(oRowPlayer("IDTeam")), 0, 0))
                SaveVariableStateValue("VariaWarehCosts", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

                sValore = GetVariableState("NetCapital", Nni(oRowPlayer("IDTeam")), "", 0, 0)
                SaveVariableStateValue("NetCapital", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)

            Next

            sValore = Nn(GetVariableState("AbsolAdverMarke", 0, "", 0, 0)) + Nn(GetVariableDefineValue("IncreAbsolAdver", 0, 0, 0))
            SaveVariableStateValue("AbsolAdverMarke", 0, 0, 0, sValore)

            sValore = Nn(GetVariableState("AbsolQualiProce", 0, "", 0, 0)) + Nn(GetVariableDefineValue("IncreAbsolAutom", 0, 0, 0))
            SaveVariableStateValue("AbsolQualiProce", 0, 0, 0, sValore)

            sValore = Nn(GetVariableState("AbsolQualiGreen", 0, "", 0, 0)) + Nn(GetVariableDefineValue("IncreAbsolGreen", 0, 0, 0))
            SaveVariableStateValue("AbsolQualiGreen", 0, 0, 0, sValore)

            sValore = Nn(GetVariableState("AbsolQualiTrain", 0, "", 0, 0)) + Nn(GetVariableDefineValue("IncreAbsolTrain", 0, 0, 0))
            SaveVariableStateValue("AbsolQualiTrain", 0, 0, 0, sValore)

            For Each oRowItem As DataRow In m_DataTableItems.Rows
                sValore = Nn(GetVariableBoss("RawEuropCost", "RawEuropCost_" & Nz(oRowItem("VariableName")))) _
                        + Nn(GetVariableDefineValue("IncreRawEurop", 0, Nni(oRowItem("IDVariable")), 0))
                SaveVariableBossValue("RawEuropCost", 0, Nni(oRowItem("IDVariable")), 0, sValore, "RawEuropCost_" & Nz(oRowItem("VariableName")))
            Next

            For Each oRowItem As DataRow In m_DataTableItems.Rows
                sValore = Nn(GetVariableBoss("RawNICCost", "RawNICCost_" & Nz(oRowItem("VariableName")))) _
                        + Nn(GetVariableDefineValue("IncreRawNIC", 0, Nni(oRowItem("IDVariable")), 0))
                SaveVariableBossValue("RawNICCost", 0, Nni(oRowItem("IDVariable")), 0, sValore, "RawNICCost_" & Nz(oRowItem("VariableName")))
            Next

            Dim iContaItem As Integer = 1
            Dim IIDDecisionBossValue As Integer

            For Each oRowItem As DataRow In m_DataTableItems.Rows
                If iContaItem = 1 Then
                    sSQL = "SELECT DISTINCT DBV.Id FROM Decisions_Boss D " _
                         & "LEFT JOIN Decisions_Boss_Value DBV On D.Id = DBV.IDDecisionBoss " _
                         & "LEFT JOIN Decisions_Boss_Value_List DBVL On DBV.id = DBVL.IdDecisionBossValue " _
                         & "WHERE D.Decision = 'ProduUnitCost' AND ISNULL(DBV.IDPeriod, " & m_IDPeriodNext & ") = " & m_IDPeriodNext & " AND D.IDGame = " & m_IDGame
                    IIDDecisionBossValue = g_DAL.ExecuteScalar(sSQL)
                End If

                sNomeVariabile = "ProduUnitCost_" & Nz(oRowItem("VariableName"))
                sValore = Nn(GetVariableBoss("ProduUnitCost", sNomeVariabile)) _
                        + Nn(GetVariableDefineValue("IncreProduCost", 0, Nni(oRowItem("IDVariable")), 0))

                sSQL = "INSERT INTO Decisions_Boss_Value_List (IDDecisionBossValue, IDPeriod, VariableName, Value, Simulation) VALUES (" _
                     & IIDDecisionBossValue & ", " & m_IDPeriodNext & ", '" & sNomeVariabile & "', '" & sValore & "', " & Boolean_To_Bit(m_Simulation) & " ) "
                g_DAL.ExecuteNonQuery(sSQL)

                iContaItem += 1
            Next

        Catch ex As Exception
            g_MessaggioErrore &= "CalculateVariabiliFlusso -> " & sNomeVariabile & " -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try

    End Sub

    'Private Sub CalculateVariableFlow()
    '    Dim sValore As String
    '    Dim sNomeVariabile As String
    '    Dim sSQL As String

    '    Try
    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("VariaExpenCosts", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
    '                    + Nn(GetVariableDefineValue("AddesTotalAllow", Nni(oRowPlayer("IDTeam")), 0, 0)) _
    '                    + Nn(GetVariableDefineValue("AdditCostsPerio", Nni(oRowPlayer("IDTeam")), 0, 0)) _
    '                    - Nn(GetVariableDefineValue("DischExpenCosts", Nni(oRowPlayer("IDTeam")), 0, 0)) _
    '                    + Nn(GetVariableDefineValue("GreenAllow", Nni(oRowPlayer("IDTeam")), 0, 0)) _
    '                    + Nn(GetVariableDefineValue("PromoInvesPerio", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("VariaExpenCosts", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("VariaAutomTechn", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
    '                    + Nn(GetVariableDefineValue("TechnCostsPerio", Nni(oRowPlayer("IDTeam")), 0, 0)) _
    '                    - Nn(GetVariableDefineValue("DischCostsTechn", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("VariaAutomTechn", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("TotalCentrStore", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
    '                    - Nn(GetVariableDefineValue("CentrAlienAllow", Nni(oRowPlayer("IDTeam")), 0, 0)) _
    '                    + Nn(GetVariableDefineValue("CentrStoreAllow", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("TotalCentrStore", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("VariaDebtsOff", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
    '                    + Nn(GetVariableDefineValue("DebtsWrittOff", Nni(oRowPlayer("IDTeam")), 0, 0)) _
    '                    + Nn(GetVariableDefineValue("WriteOffLost", Nni(oRowPlayer("IDTeam")), 0, 0)) _
    '                    - Nn(GetVariableDefineValue("DischDebtsOff", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("VariaDebtsOff", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("BankLevel", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
    '                    + Nn(GetVariableDefineValue("IncreBankCash", Nni(oRowPlayer("IDTeam")), 0, 0)) _
    '                    - Nn(GetVariableDefineValue("DecreBankCash", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("BankLevel", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("Debtors", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
    '                    + Nn(GetVariableDefineValue("IncreDebts", Nni(oRowPlayer("IDTeam")), 0, 0)) _
    '                    - Nn(GetVariableDefineValue("DecreDebts", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("Debtors", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            For Each oRowItem As DataRow In m_DataTableItems.Rows
    '                sNomeVariabile = "var_" & Nz(oRowPlayer("TeamName")) & "_HoursPerItem_" & Nz(oRowItem("VariableName"))
    '                sValore = Nn(GetVariableBoss("HoursPerItem", sNomeVariabile)) _
    '                        - Nn(GetVariableDefineValue("DecreHoursItem", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0))
    '                SaveVariableBossValue("HoursPerItem", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0, sValore, sNomeVariabile)
    '            Next
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("VariaDeltaRaw", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
    '                    + Nn(GetVariableDefineValue("DeltaRawValue", Nni(oRowPlayer("IDTeam")), 0, 0)) _
    '                    - Nn(GetVariableDefineValue("DischDeltaRaw", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("VariaDeltaRaw", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            For Each oRowItem As DataRow In m_DataTableItems.Rows
    '                sNomeVariabile = "var_" & Nz(oRowPlayer("TeamName")) & "_CumulDeltaSales_" & Nz(oRowItem("VariableName"))
    '                sValore = Nn(GetVariableState("CumulDeltaSales", Nni(oRowPlayer("IDTeam")), sNomeVariabile, Nni(oRowItem("IDVariable")), 0)) _
    '                        + Nn(GetVariableDefineValue("DeltaSalesPrice", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0))
    '                SaveVariableStateValue("CumulDeltaSales", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0, sValore)
    '            Next
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("VariaDeltaStock", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
    '                    + Nn(GetVariableDefineValue("DeltaStockValue", Nni(oRowPlayer("IDTeam")), 0, 0)) _
    '                    - Nn(GetVariableDefineValue("DischDeltaStock", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("VariaDeltaStock", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("VariaCostPurch", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
    '                    + Nn(GetVariableDefineValue("CostPurchTotal", Nni(oRowPlayer("IDTeam")), 0, 0)) _
    '                    - Nn(GetVariableDefineValue("DischCostPurch", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("VariaCostPurch", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("PreviFixedCosts", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
    '                    - Nn(GetVariableDefineValue("DischCostsFixed", Nni(oRowPlayer("IDTeam")), 0, 0)) _
    '                    + Nn(GetVariableDefineValue("FixedProduCosts", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("PreviFixedCosts", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("VariaDirecCosts", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
    '                    + Nn(GetVariableDefineValue("DirecTotalCosts", Nni(oRowPlayer("IDTeam")), 0, 0)) _
    '                    - Nn(GetVariableDefineValue("DischDirecCosts", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("VariaDirecCosts", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("VariaFixedCosts", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
    '                    + Nn(GetVariableDefineValue("ProduFixedCosts", Nni(oRowPlayer("IDTeam")), 0, 0)) _
    '                    - Nn(GetVariableDefineValue("DischFixedCosts", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("VariaFixedCosts", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("VariaLeasiCosts", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
    '                    - Nn(GetVariableDefineValue("DischLeasiCosts", Nni(oRowPlayer("IDTeam")), 0, 0)) _
    '                    + Nn(GetVariableDefineValue("LeasiTotalCost", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("VariaLeasiCosts", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("ActuaMaximLoans", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
    '                    + Nn(GetVariableDefineValue("IncreMaximLoans", Nni(oRowPlayer("IDTeam")), 0, 0)) _
    '                    - Nn(GetVariableDefineValue("DischMaximLoans", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("ActuaMaximLoans", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("VariaPersoCosts", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
    '                    + Nn(GetVariableDefineValue("TotalCostsPerso", Nni(oRowPlayer("IDTeam")), 0, 0)) _
    '                    - Nn(GetVariableDefineValue("DischPersoCosts", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("VariaPersoCosts", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("VariaPositInter", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
    '                    + Nn(GetVariableDefineValue("TotalPositInter", Nni(oRowPlayer("IDTeam")), 0, 0)) _
    '                    - Nn(GetVariableDefineValue("DischPositInter", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("VariaPositInter", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            For Each oRowItem As DataRow In m_DataTableItems.Rows
    '                sNomeVariabile = "var_" & Nz(oRowPlayer("TeamName")) & "_PreviProduCosts_" & Nz(oRowItem("VariableName"))
    '                sValore = Nn(GetVariableState("PreviProduCosts", Nni(oRowPlayer("IDTeam")), sNomeVariabile, Nni(oRowItem("IDVariable")), 0)) _
    '                        - Nn(GetVariableDefineValue("DischPreviCosts", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0)) _
    '                        + Nn(GetVariableDefineValue("TotalProduCosts", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0))
    '                SaveVariableStateValue("PreviProduCosts", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0, sValore)
    '            Next
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            For Each oRowItem As DataRow In m_DataTableItems.Rows
    '                sNomeVariabile = "var_" & Nz(oRowPlayer("TeamName")) & "_PreviInterPrice_" & Nz(oRowItem("VariableName"))
    '                sValore = Nn(GetVariableState("PreviInterPrice", Nni(oRowPlayer("IDTeam")), sNomeVariabile, Nni(oRowItem("IDVariable")), 0)) _
    '                        + Nn(Nn(GetVariableDefineValue("InterPriceAllow", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0))) _
    '                        - Nn(GetVariableDefineValue("DischPreviPrint", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0))
    '                SaveVariableStateValue("PreviInterPrice", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0, sValore)
    '            Next
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            For Each oRowItem As DataRow In m_DataTableItems.Rows
    '                sNomeVariabile = "var_" & Nz(oRowPlayer("TeamName")) & "_PreviSalesPrice_" & Nz(oRowItem("VariableName"))
    '                sValore = Nn(GetVariableState("PreviSalesPrice", Nni(oRowPlayer("IDTeam")), sNomeVariabile, Nni(oRowItem("IDVariable")), 0)) _
    '                        - Nn(GetVariableDefineValue("DischPreviPrice", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0)) _
    '                        + Nn(GetVariableDefineValue("SalesPriceAllow", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0))
    '                SaveVariableStateValue("PreviSalesPrice", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0, sValore)
    '            Next
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("VariaRevenTotal", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
    '                    - Nn(GetVariableDefineValue("DischRevenTotal", Nni(oRowPlayer("IDTeam")), 0, 0)) _
    '                    + Nn(GetVariableDefineValue("TotalRevenPerio", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("VariaRevenTotal", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("VariaSinkiPlant", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
    '                    + Nn(GetVariableDefineValue("SinkiFundPlant", Nni(oRowPlayer("IDTeam")), 0, 0)) _
    '                    - Nn(GetVariableDefineValue("DischSinkiPlant", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("VariaSinkiPlant", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("VariaTransCosts", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
    '                    - Nn(GetVariableDefineValue("DischTransCosts", Nni(oRowPlayer("IDTeam")), 0, 0)) _
    '                    + Nn(GetVariableDefineValue("TotalTransCosts", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("VariaTransCosts", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("VariaExtraProdu", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
    '                    + Nn(GetVariableDefineValue("ExtraMoneyAllow", Nni(oRowPlayer("IDTeam")), 0, 0)) _
    '                    - Nn(GetVariableDefineValue("DischExtraProdu", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("VariaExtraProdu", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        sValore = Nn(GetVariableState("AbsolAdverMarke", 0, "", 0, 0)) + Nn(GetVariableDefineValue("IncreAbsolAdver", 0, 0, 0))
    '        SaveVariableStateValue("AbsolAdverMarke", 0, 0, 0, sValore)

    '        sValore = Nn(GetVariableState("AbsolQualiProce", 0, "", 0, 0)) + Nn(GetVariableDefineValue("IncreAbsolAutom", 0, 0, 0))
    '        SaveVariableStateValue("AbsolQualiProce", 0, 0, 0, sValore)

    '        sValore = Nn(GetVariableState("AbsolQualiGreen", 0, "", 0, 0)) + Nn(GetVariableDefineValue("IncreAbsolGreen", 0, 0, 0))
    '        SaveVariableStateValue("AbsolQualiGreen", 0, 0, 0, sValore)

    '        sValore = Nn(GetVariableState("AbsolQualiTrain", 0, "", 0, 0)) + Nn(GetVariableDefineValue("IncreAbsolTrain", 0, 0, 0))
    '        SaveVariableStateValue("AbsolQualiTrain", 0, 0, 0, sValore)

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("CumulAdverLevel", Nni(oRowPlayer("IDTeam")), "", 0, 0)) + Nn(GetVariableDefineValue("IncreAdverCumul", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("CumulAdverLevel", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            For iIndexAge As Integer = 1 To m_DataTableAge.Rows.Count
    '                sNomeVariabile = "var_" & Nz(oRowPlayer("TeamName")) & "_LeasePerAge_" & iIndexAge
    '                sValore = Nn(GetVariableState("LeasePerAge", Nni(oRowPlayer("IDTeam")), sNomeVariabile, 0, iIndexAge)) _
    '                        + Nn(GetVariableDefineValue("IncreAgeLease", Nni(oRowPlayer("IDTeam")), 0, iIndexAge))
    '                SaveVariableStateValue("LeasePerAge", Nni(oRowPlayer("IDTeam")), 0, iIndexAge, sValore)
    '            Next
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            For iIndexAge As Integer = 1 To m_DataTableAge.Rows.Count
    '                sNomeVariabile = "var_" & Nz(oRowPlayer("TeamName")) & "_PlantPerAge_" & iIndexAge
    '                sValore = Nn(GetVariableState("PlantPerAge", Nni(oRowPlayer("IDTeam")), sNomeVariabile, 0, iIndexAge)) + Nn(GetVariableDefineValue("IncreAgePlant", Nni(oRowPlayer("IDTeam")), 0, iIndexAge))
    '                SaveVariableStateValue("PlantPerAge", Nni(oRowPlayer("IDTeam")), 0, iIndexAge, sValore)
    '            Next
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            For iIndexAge As Integer = 1 To m_DataTableAge.Rows.Count
    '                sNomeVariabile = "var_" & Nz(oRowPlayer("TeamName")) & "_TotalVehicOwned_" & iIndexAge
    '                sValore = Nn(GetVariableState("TotalVehicOwned", Nni(oRowPlayer("IDTeam")), sNomeVariabile, 0, iIndexAge)) + Nn(GetVariableDefineValue("IncreAgeVehic", Nni(oRowPlayer("IDTeam")), 0, iIndexAge))
    '                SaveVariableStateValue("TotalVehicOwned", Nni(oRowPlayer("IDTeam")), 0, iIndexAge, sValore)
    '            Next
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("AveraAdverInves", Nni(oRowPlayer("IDTeam")), "", 0, 0)) + Nn(GetVariableDefineValue("IncreAveraAdver", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("AveraAdverInves", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("AveraGreenInves", Nni(oRowPlayer("IDTeam")), "", 0, 0)) + Nn(GetVariableDefineValue("IncreAveraGreen", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("AveraGreenInves", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("AveraTechnInves", Nni(oRowPlayer("IDTeam")), "", 0, 0)) + Nn(GetVariableDefineValue("IncreAveraTechn", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("AveraTechnInves", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("AveraTrainInves", Nni(oRowPlayer("IDTeam")), "", 0, 0)) + Nn(GetVariableDefineValue("IncreAveraTrain", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("AveraTrainInves", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("Creditors", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
    '                    + Nn(GetVariableDefineValue("IncreCredits", Nni(oRowPlayer("IDTeam")), 0, 0)) _
    '                    - Nn(GetVariableDefineValue("DecreCredits", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("Creditors", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            For Each oRowItem As DataRow In m_DataTableItems.Rows
    '                sNomeVariabile = "var_" & Nz(oRowPlayer("TeamName")) & "_StockFinisGoods_" & Nz(oRowItem("VariableName"))
    '                sValore = Nn(GetVariableState("StockFinisGoods", Nni(oRowPlayer("IDTeam")), sNomeVariabile, Nni(oRowItem("IDVariable")), 0)) _
    '                        + Nn(GetVariableDefineValue("IncreGoodsStock", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0))
    '                SaveVariableStateValue("StockFinisGoods", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0, sValore)
    '            Next
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("CumulGreenLevel", Nni(oRowPlayer("IDTeam")), "", 0, 0)) + Nn(GetVariableDefineValue("IncreGreenCumul", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("CumulGreenLevel", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("PointTrainLevel", Nni(oRowPlayer("IDTeam")), "", 0, 0)) + Nn(GetVariableDefineValue("IncrePointLevel", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("PointTrainLevel", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            For Each oRowItem As DataRow In m_DataTableItems.Rows
    '                sNomeVariabile = "var_" & Nz(oRowPlayer("TeamName")) & "_QualiStockFinis_" & Nz(oRowItem("VariableName"))
    '                sValore = Nn(GetVariableState("QualiStockFinis", Nni(oRowPlayer("IDTeam")), sNomeVariabile, Nni(oRowItem("IDVariable")), 0)) + Nn(GetVariableDefineValue("IncreQualiFinis", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0))
    '                SaveVariableStateValue("QualiStockFinis", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0, sValore)
    '            Next
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            For Each oRowItem As DataRow In m_DataTableItems.Rows
    '                sNomeVariabile = "var_" & Nz(oRowPlayer("TeamName")) & "_RawStockQuali_" & Nz(oRowItem("VariableName"))
    '                sValore = Nn(GetVariableState("RawStockQuali", Nni(oRowPlayer("IDTeam")), sNomeVariabile, Nni(oRowItem("IDVariable")), 0)) + Nn(GetVariableDefineValue("IncreQualiStock", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0))
    '                SaveVariableStateValue("RawStockQuali", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0, sValore)
    '            Next
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            For Each oRowItem As DataRow In m_DataTableItems.Rows
    '                sNomeVariabile = "var_" & Nz(oRowPlayer("TeamName")) & "_StockRawMater_" & Nz(oRowItem("VariableName"))
    '                sValore = Nn(GetVariableState("StockRawMater", Nni(oRowPlayer("IDTeam")), sNomeVariabile, Nni(oRowItem("IDVariable")), 0)) + Nn(GetVariableDefineValue("IncreQuantStock", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0))
    '                SaveVariableStateValue("StockRawMater", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0, sValore)
    '            Next
    '        Next

    '        For Each oRowItem As DataRow In m_DataTableItems.Rows
    '            sValore = Nn(GetVariableBoss("RawEuropCost", "RawEuropCost_" & Nz(oRowItem("VariableName")))) _
    '                    + Nn(GetVariableDefineValue("IncreRawEurop", 0, Nni(oRowItem("IDVariable")), 0))
    '            SaveVariableBossValue("RawEuropCost", 0, Nni(oRowItem("IDVariable")), 0, sValore, "RawEuropCost_" & Nz(oRowItem("VariableName")))
    '        Next

    '        For Each oRowItem As DataRow In m_DataTableItems.Rows
    '            sValore = Nn(GetVariableBoss("RawNICCost", "RawNICCost_" & Nz(oRowItem("VariableName")))) _
    '                    + Nn(GetVariableDefineValue("IncreRawNIC", 0, Nni(oRowItem("IDVariable")), 0))
    '            SaveVariableBossValue("RawNICCost", 0, Nni(oRowItem("IDVariable")), 0, sValore, "RawNICCost_" & Nz(oRowItem("VariableName")))
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("StaffTrainLevel", Nni(oRowPlayer("IDTeam")), "", 0, 0)) + Nn(GetVariableDefineValue("IncreStaffLevel", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("StaffTrainLevel", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("CumulTechnLevel", Nni(oRowPlayer("IDTeam")), "", 0, 0)) + Nn(GetVariableDefineValue("IncreTechnCumul", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("CumulTechnLevel", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("CumulTrainLevel", Nni(oRowPlayer("IDTeam")), "", 0, 0)) + Nn(GetVariableDefineValue("IncreTrainCumul", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("CumulTrainLevel", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            For Each oRowItem As DataRow In m_DataTableItems.Rows
    '                sNomeVariabile = "var_" & Nz(oRowPlayer("TeamName")) & "_ValueRawMater_" & Nz(oRowItem("VariableName"))
    '                sValore = Nn(GetVariableState("ValueRawMater", Nni(oRowPlayer("IDTeam")), sNomeVariabile, Nni(oRowItem("IDVariable")), 0)) + Nn(GetVariableDefineValue("IncreValueRaw", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0))
    '                SaveVariableStateValue("ValueRawMater", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0, sValore)
    '            Next
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            For Each oRowItem As DataRow In m_DataTableItems.Rows
    '                sNomeVariabile = "var_" & Nz(oRowPlayer("TeamName")) & "_LastFinisValue_" & Nz(oRowItem("VariableName"))
    '                sValore = Nn(GetVariableState("LastFinisValue", Nni(oRowPlayer("IDTeam")), sNomeVariabile, Nni(oRowItem("IDVariable")), 0)) + Nn(GetVariableDefineValue("IncreValueStock", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0))
    '                SaveVariableStateValue("LastFinisValue", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0, sValore)
    '            Next
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            For Each oRowItem As DataRow In m_DataTableItems.Rows
    '                sNomeVariabile = "var_" & Nz(oRowPlayer("TeamName")) & "_PreviInterPrice_" & Nz(oRowItem("VariableName"))
    '                sValore = Nn(GetVariableState("PreviInterPrice", Nni(oRowPlayer("IDTeam")), sNomeVariabile, Nni(oRowItem("IDVariable")), 0)) _
    '                        - Nn(GetVariableDefineValue("DischPreviPrint", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0)) _
    '                        + Nn(GetVariableDefineValue("InterPriceAllow", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0))
    '                SaveVariableStateValue("PreviInterPrice", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0, sValore)
    '            Next
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("LoansLevel", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
    '                    + Nn(GetVariableDefineValue("LoansAllow", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("LoansLevel", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            For Each oRowItem As DataRow In m_DataTableItems.Rows
    '                sNomeVariabile = "var_" & Nz(oRowPlayer("TeamName")) & "_PreviMarkeShare_" & Nz(oRowItem("VariableName"))
    '                sValore = Nn(GetVariableState("PreviMarkeShare", Nni(oRowPlayer("IDTeam")), sNomeVariabile, Nni(oRowItem("IDVariable")), 0)) _
    '                        - Nn(GetVariableDefineValue("DischMarkeShare", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0)) _
    '                        + Nn(GetVariableDefineValue("MarketShare", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0))
    '                SaveVariableStateValue("PreviMarkeShare", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0, sValore)
    '            Next
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("TaxFunds", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
    '                    + Nn(GetVariableDefineValue("TaxOfPerio", Nni(oRowPlayer("IDTeam")), 0, 0)) _
    '                    - Nn(GetVariableDefineValue("TaxPayment", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("TaxFunds", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("CumulProfi", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
    '                    + Nn(GetVariableDefineValue("NegatTaxPayme", Nni(oRowPlayer("IDTeam")), 0, 0)) _
    '                    + Nn(GetVariableDefineValue("ProfiAfterTax", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("CumulProfi", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("TotalPointPerso", Nni(oRowPlayer("IDTeam")), "", 0, 0)) + Nn(GetVariableDefineValue("NewPointAllow", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("TotalPointPerso", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("TotalStaffPerso", Nni(oRowPlayer("IDTeam")), "", 0, 0)) + Nn(GetVariableDefineValue("NewStaffAllow", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("TotalStaffPerso", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("TotalPerifStore", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
    '                    - Nn(GetVariableDefineValue("PerifScrapAllow", Nni(oRowPlayer("IDTeam")), 0, 0)) _
    '                    + Nn(GetVariableDefineValue("PerifStoreAllow", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("TotalPerifStore", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            For Each oRowItem As DataRow In m_DataTableItems.Rows
    '                sNomeVariabile = "var_" & Nz(oRowPlayer("TeamName")) & "_PurchDelivNIC_" & Nz(oRowItem("VariableName"))
    '                sValore = Nn(GetVariableState("PurchDelivNIC", Nni(oRowPlayer("IDTeam")), sNomeVariabile, Nni(oRowItem("IDVariable")), 0)) _
    '                        - Nn(GetVariableDefineValue("PortfDelivNIC", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0)) _
    '                        + Nn(GetVariableDefineValue("PortfPurchNIC", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0))
    '                SaveVariableStateValue("PurchDelivNIC", Nni(oRowPlayer("IDTeam")), Nni(oRowItem("IDVariable")), 0, sValore)
    '            Next
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("VariaBeforTax", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
    '                    + Nn(GetVariableDefineValue("ProfiBeforTax", Nni(oRowPlayer("IDTeam")), 0, 0)) _
    '                    - Nn(GetVariableDefineValue("DischBeforTax", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("VariaBeforTax", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("VariaSinkiVehic", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
    '                    + Nn(GetVariableDefineValue("SinkiFundVehic", Nni(oRowPlayer("IDTeam")), 0, 0)) _
    '                    - Nn(GetVariableDefineValue("DischSinkiVehic", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("VariaSinkiVehic", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("VariaOutleCosts", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
    '                    + Nn(GetVariableDefineValue("TotalCostsOutle", Nni(oRowPlayer("IDTeam")), 0, 0)) _
    '                    - Nn(GetVariableDefineValue("DischOutleCosts", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("VariaOutleCosts", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("VariaNegatInter", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
    '                    + Nn(GetVariableDefineValue("TotalNegatInter", Nni(oRowPlayer("IDTeam")), 0, 0)) _
    '                    - Nn(GetVariableDefineValue("DischNegatInter", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("VariaNegatInter", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = Nn(GetVariableState("VariaWarehCosts", Nni(oRowPlayer("IDTeam")), "", 0, 0)) _
    '                    + Nn(GetVariableDefineValue("TotalWarehCosts", Nni(oRowPlayer("IDTeam")), 0, 0)) _
    '                    - Nn(GetVariableDefineValue("DischWarehCosts", Nni(oRowPlayer("IDTeam")), 0, 0))
    '            SaveVariableStateValue("VariaWarehCosts", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        ' Riporto il NETCapital, non viene movimentato da flussi, ma me lo riporto lo stesso
    '        For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
    '            sValore = GetVariableState("NetCapital", Nni(oRowPlayer("IDTeam")), "", 0, 0)
    '            SaveVariableStateValue("NetCapital", Nni(oRowPlayer("IDTeam")), 0, 0, sValore)
    '        Next

    '        g_DAL = New DBHelper
    '        Dim iContaItem As Integer = 1
    '        Dim IIDDecisionBossValue As Integer

    '        For Each oRowItem As DataRow In m_DataTableItems.Rows
    '            If iContaItem = 1 Then
    '                sSQL = "SELECT DISTINCT DBV.Id FROM Decisions_Boss D " _
    '                     & "LEFT JOIN Decisions_Boss_Value DBV On D.Id = DBV.IDDecisionBoss " _
    '                     & "LEFT JOIN Decisions_Boss_Value_List DBVL On DBV.id = DBVL.IdDecisionBossValue " _
    '                     & "WHERE D.Decision = 'ProduUnitCost' AND ISNULL(DBV.IDPeriod, " & m_IDPeriodNext & ") = " & m_IDPeriodNext & " AND D.IDGame = " & m_IDGame
    '                IIDDecisionBossValue = g_DAL.ExecuteScalar(sSQL)
    '            End If

    '            sNomeVariabile = "ProduUnitCost_" & Nz(oRowItem("VariableName"))
    '            sValore = Nn(GetVariableBoss("ProduUnitCost", sNomeVariabile)) _
    '                    + Nn(GetVariableDefineValue("IncreProduCost", 0, Nni(oRowItem("IDVariable")), 0))

    '            sSQL = "INSERT INTO Decisions_Boss_Value_List (IDDecisionBossValue, IDPeriod, VariableName, Value, Simulation) VALUES (" _
    '                 & IIDDecisionBossValue & ", " & m_IDPeriodNext & ", '" & sNomeVariabile & "', '" & sValore & "', " & Boolean_To_Bit(m_Simulation) & " ) "
    '            g_DAL.ExecuteNonQuery(sSQL)

    '            iContaItem += 1
    '        Next

    '    Catch ex As Exception
    '        g_MessaggioErrore &= "CalculateVariableFlow -> " & ex.Message & "<br/>"
    '        ex.Data.Clear()
    '    End Try

    'End Sub

#End Region

#Region "CALCOLO BILANCIO RICLASSIFICATO"

    Private Sub BilancioRiclassificato()
        Dim iIDVariabile As Integer

        Dim sValore As String

        Dim iIDPlayer As Integer
        Dim sTeamName As String

        Try
            If m_DataTablePlayers.Rows.Count > 0 Then
                sValore = "0"

                For Each oRowPlayer As DataRow In m_DataTablePlayers.Rows
                    iIDPlayer = Nni(oRowPlayer("IDTeam"))
                    sTeamName = Nz(oRowPlayer("TeamName"))

                    ' ImmobNette
                    iIDVariabile = CheckVariableExists("ImmobNette", m_IDGame)
                    sValore = Nn(GetVariableDefineValue("NetPlants", iIDPlayer, 0, 0)) + Nn(GetVariableDefineValue("NetVehicles", iIDPlayer, 0, 0))
                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)

                    ' AttivABreve
                    iIDVariabile = CheckVariableExists("AttivABreve", m_IDGame)
                    sValore = Nn(GetVariableDefineValue("CashEndPerio", iIDPlayer, 0, 0)) + Nn(GetVariableDefineValue("DebtsEndPerio", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("RawEndPerio", iIDPlayer, 0, 0)) + Nn(GetVariableDefineValue("StockEndPerio", iIDPlayer, 0, 0)) _
                            - Nn(GetVariableDefineValue("TotalCashNeede", iIDPlayer, 0, 0))
                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)

                    ' DebitABreve
                    iIDVariabile = CheckVariableExists("DebitABreve", m_IDGame)
                    sValore = Nn(GetVariableDefineValue("CashEndPerio", iIDPlayer, 0, 0)) + Nn(GetVariableDefineValue("TaxPerioFunds", iIDPlayer, 0, 0))
                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)

                    ' DebitAMedioLungo
                    iIDVariabile = CheckVariableExists("DebitAMedioLungo", m_IDGame)
                    sValore = Nn(GetVariableDefineValue("LoansEndPerio", iIDPlayer, 0, 0)) - Nn(GetVariableDefineValue("TotalCashNeede", iIDPlayer, 0, 0))
                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)

                    ' AttivFisseNette
                    iIDVariabile = CheckVariableExists("AttivFisseNette", m_IDGame)
                    sValore = Nn(GetVariableDefineValue("NetPlants", iIDPlayer, 0, 0)) + Nn(GetVariableDefineValue("NetVehicles", iIDPlayer, 0, 0))
                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)

                    ' CapitCircoNetto
                    iIDVariabile = CheckVariableExists("CapitCircoNetto", m_IDGame)
                    sValore = Nn(GetVariableDefineValue("DebtsEndPerio", iIDPlayer, 0, 0)) + Nn(GetVariableDefineValue("RawEndPerio", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("StockEndPerio", iIDPlayer, 0, 0)) + Nn(GetVariableDefineValue("CrediEndPerio", iIDPlayer, 0, 0))
                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)

                    ' DebitFinanNetti
                    iIDVariabile = CheckVariableExists("DebitFinanNetti", m_IDGame)
                    sValore = Nn(GetVariableDefineValue("LoansEndPerio", iIDPlayer, 0, 0)) + Nn(GetVariableDefineValue("TaxPerioFunds", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("CashEndPerio", iIDPlayer, 0, 0))
                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)

                    ' RisulMOL
                    iIDVariabile = CheckVariableExists("RisulMOL", m_IDGame)
                    sValore = Nn(GetVariableDefineValue("OperativeMargin", iIDPlayer, 0, 0)) + Nn(GetVariableDefineValue("CumulSinkiPlant", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("CumulSinkiVehic", iIDPlayer, 0, 0)) + Nn(GetVariableDefineValue("CumulDebtsOff", iIDPlayer, 0, 0)) _
                            + Nn(GetVariableDefineValue("CumulNegatInter", iIDPlayer, 0, 0)) - Nn(GetVariableDefineValue("CumulPositInter", iIDPlayer, 0, 0))
                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)

                    ' IndexROS
                    iIDVariabile = CheckVariableExists("IndexROS", m_IDGame)
                    sValore = Nn(GetVariableDefineValue("RisulMOL", iIDPlayer, 0, 0)) / Nn(GetVariableDefineValue("CumulRevenTotal", iIDPlayer, 0, 0))
                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)

                    ' IndicDispo
                    iIDVariabile = CheckVariableExists("IndicDispo", m_IDGame)
                    sValore = Nn(GetVariableDefineValue("AttivABreve", iIDPlayer, 0, 0)) / Nn(GetVariableDefineValue("DebitABreve", iIDPlayer, 0, 0))
                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)

                    '  PatrimonioNetto
                    iIDVariabile = CheckVariableExists("PatrimonioNetto", m_IDGame)
                    sValore = Nn(GetVariableState("NetCapital", iIDPlayer, "", 0, 0)) + Nn(GetVariableDefineValue("UtilsEndPerio", iIDPlayer, 0, 0))
                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)

                    ' CapitInvesNetto
                    iIDVariabile = CheckVariableExists("CapitInvesNetto", m_IDGame)
                    sValore = Nn(GetVariableDefineValue("AttivFisseNette", iIDPlayer, 0, 0)) + Nn(GetVariableDefineValue("CapitCircoNetto", iIDPlayer, 0, 0))
                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)

                    ' RappoIndeb
                    iIDVariabile = CheckVariableExists("RappoIndeb", m_IDGame)
                    sValore = Nn(GetVariableDefineValue("DebitFinanNetti", iIDPlayer, 0, 0)) /
                             (Nn(GetVariableState("NetCapital", iIDPlayer, "", 0, 0)) + Nn(GetVariableDefineValue("UtilsEndPerio", iIDPlayer, 0, 0))) ' il capitale investito lo devo passare così perchè non disponibile nel datatable
                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)

                    ' IndexRONA
                    iIDVariabile = CheckVariableExists("IndexRONA", m_IDGame)
                    sValore = Nn(GetVariableDefineValue("OperativeMargin", iIDPlayer, 0, 0)) /
                             (Nn(GetVariableDefineValue("AttivFisseNette", iIDPlayer, 0, 0)) + Nn(GetVariableDefineValue("CapitCircoNetto", iIDPlayer, 0, 0))) ' il capitale netto lo devo passare così perchè non disponibile nel datatable
                    SaveVariableValue(iIDVariabile, iIDPlayer, 0, 0, sValore)

                Next

            End If

        Catch ex As Exception
            g_MessaggioErrore &= "BilancioRiclassificato -> " & ex.Message & "<br/>"
            ex.Data.Clear()
        End Try
    End Sub

#End Region

#End Region

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

End Class
