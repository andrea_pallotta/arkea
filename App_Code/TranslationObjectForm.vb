﻿Imports System.Data
Imports DALC4NET
Imports Microsoft.VisualBasic
Imports APPCore.Utility
Imports APS.APSUtility

Public Class TranslationObjectForm

    Private m_WebPage As Page
    Private m_LanguageID As String
    Private m_PageName As String
    Private m_DataTableWebForms As DataTable
    Private m_IDGame As Integer
    Private m_DAL As New DBHelper

    Public Sub New(ByRef ObjectPage As Page, DAL As DBHelper, LanguageID As String, Optional IDGame As Integer = 0)
        m_WebPage = ObjectPage
        m_LanguageID = LanguageID
        m_IDGame = IDGame

        LoadObject()
    End Sub

    Private Sub LoadDataTableWebForms()
        Dim sSQL As String

        Try
            sSQL = "SELECT *, 0 AS ObjectNew FROM WebForms WHERE FormName = '" & m_PageName & "' "
            ' Escludo alcuni oggetti, come ad esempio il titolo della pagina
            sSQL &= "AND  ObjectID NOT LIKE '%lblGameTitle%' "
            m_DataTableWebForms = m_DAL.ExecuteDataTable(sSQL)
        Catch ex As Exception

        End Try
    End Sub

    ''' <summary>
    ''' Caricamento di tutti gli oggetti presenti nella pagina
    ''' </summary>
    Private Sub LoadObject()
        Dim lstControlPhisical As New List(Of Control)

        m_PageName = m_WebPage.ToString().Substring(4, m_WebPage.ToString().Substring(4).Length - 5) + ".aspx"

        LoadDataTableWebForms()

        GetControlsPage(m_WebPage, lstControlPhisical)

        ' Ho tutti i controlli della pagina
        ' Devo porcedere alla differenziazione, recupero per il momento, etichette, griglie/tabelle HTML
        CheckObjectIntoDatabase(lstControlPhisical)

        Dim lstControlTranslation As New List(Of KeyValuePair(Of String, String))
        lstControlTranslation = GetListTranslations()

        SetTranslationControl(lstControlTranslation, lstControlPhisical)
    End Sub

    ''' <summary>
    ''' Ritorna una lista di tutti gli oggetti presenti nella pagina 
    ''' </summary>
    ''' <param name="Page"></param>
    ''' <param name="allControls"></param>
    Sub GetControlsPage(ByRef Page As Control, ByRef allControls As List(Of Control))
        If Page IsNot Nothing Then
            allControls.Add(Page)
            If Page.Controls.Count > 0 Then
                For Each ctrl As Control In Page.Controls
                    If Not ctrl.ID Is Nothing Then
                        If Not ctrl.ID.ToString.Contains("lblGameTitle") Then
                            GetControlsPage(ctrl, allControls)
                        End If
                    Else
                        GetControlsPage(ctrl, allControls)
                    End If
                Next
            End If
        End If
    End Sub

    ''' <summary>
    ''' Controllo la presenza degli oggetti all'interno della tabella WEBForms
    ''' </summary>
    ''' <param name="ObjectControls"></param>
    Private Sub CheckObjectIntoDatabase(ObjectControls As List(Of Control))
        Dim drWebFormsControl As DataRow
        Dim sNomeFile As String = ""
        Dim sControls As String = ""

        For Each oControl As Control In ObjectControls
            ' Controllo gli oggetti:
            ' Label
            ' RadLabel
            ' Literal -> nel caso dei messaggi di errore, per ora ometto, poi vedremo

            drWebFormsControl = m_DataTableWebForms.Select("ObjectID = '" & oControl.ClientID & "'").FirstOrDefault
            If IsNothing(drWebFormsControl) Then ' Non esiste l'oggetto lo creo nella tabella temporanea
                Dim drNewRow As DataRow = m_DataTableWebForms.NewRow
                drNewRow("FormName") = m_PageName
                drNewRow("ObjectID") = oControl.ClientID

                drNewRow("ObjectNew") = 0
                If TypeOf oControl Is Label Then
                    drNewRow("ObjectName") = CType(oControl, Label).ID
                    drNewRow("ObjectDescription") = HttpContext.Current.Server.HtmlEncode(CType(oControl, Label).Text)
                    drNewRow("ObjectNew") = 1

                ElseIf TypeOf oControl Is Telerik.Web.UI.RadLabel Then
                    drNewRow("ObjectName") = CType(oControl, Telerik.Web.UI.RadLabel).ID
                    drNewRow("ObjectDescription") = HttpContext.Current.Server.HtmlEncode(CType(oControl, Telerik.Web.UI.RadLabel).Text)
                    drNewRow("ObjectNew") = 1

                ElseIf TypeOf oControl Is Literal Then
                    drNewRow("ObjectName") = CType(oControl, Literal).ID
                    drNewRow("ObjectDescription") = HttpContext.Current.Server.HtmlEncode(CType(oControl, Literal).Text)
                    drNewRow("ObjectNew") = 1

                ElseIf TypeOf oControl Is Telerik.Web.UI.RadHtmlChart Then
                    drNewRow("ObjectName") = CType(oControl, Telerik.Web.UI.RadHtmlChart).ID
                    drNewRow("ObjectDescription") = HttpContext.Current.Server.HtmlEncode(CType(oControl, Telerik.Web.UI.RadHtmlChart).ChartTitle.Text)
                    drNewRow("ObjectNew") = 1

                ElseIf TypeOf oControl Is Telerik.Web.UI.RadButton Then
                    drNewRow("ObjectName") = CType(oControl, Telerik.Web.UI.RadButton).ID
                    drNewRow("ObjectDescription") = HttpContext.Current.Server.HtmlEncode(CType(oControl, Telerik.Web.UI.RadButton).Text)
                    drNewRow("ObjectNew") = 1

                ElseIf TypeOf oControl Is Table Then


                End If
                m_DataTableWebForms.Rows.Add(drNewRow)
                m_DataTableWebForms.AcceptChanges()
            End If

            If HttpContext.Current.IsDebuggingEnabled Then
                If sControls.Contains([GetType].ToString) = False Then
                    sControls &= oControl.GetType.ToString & ControlChars.NewLine
                End If
            End If

        Next

        If HttpContext.Current.IsDebuggingEnabled Then
            sNomeFile = WriteToTempFile(sControls)
        End If

        ' Eseguo l'aggiornamento degli eventuali oggetti nuovi creati 
        If m_DataTableWebForms.Select("ObjectNew = 1").Length > 0 Then
            Dim oDTTempUpdate As New DataTable
            oDTTempUpdate = m_DataTableWebForms.Select("ObjectNew = 1").CopyToDataTable
            oDTTempUpdate.Columns.Remove("ObjectNew")
            If oDTTempUpdate.Rows.Count > 0 Then
                Dim consString As String = ConfigurationManager.ConnectionStrings("ConnectString").ConnectionString
                Using con As New SqlClient.SqlConnection(consString)
                    Using sqlBulkCopy As New SqlClient.SqlBulkCopy(con)
                        sqlBulkCopy.DestinationTableName = "dbo.WebForms"
                        con.Open()
                        sqlBulkCopy.WriteToServer(oDTTempUpdate)
                        con.Close()
                    End Using
                End Using
            End If
        End If
    End Sub

    Private Function GetListTranslations() As List(Of KeyValuePair(Of String, String))
        Dim sSQL As String
        Dim lstReturn As New List(Of KeyValuePair(Of String, String))

        sSQL = "SELECT WF.ID, WFT.IDWebForm, WFT.IDLanguage, WF.ObjectName, WF.ObjectDescription, WFT.ObejctTranslation, WFT.ID AS IDObjectWebForm, WFT.IDGame " _
             & "FROM WebForms WF " _
             & "LEFT JOIN WebForms_Translation WFT ON WF.Id = WFT.IDWebForm " _
             & "WHERE FormName = '" & m_PageName & "' AND ISNULL(WFT.IDGame, " & m_IDGame & ") = " & m_IDGame
        Dim oDTTranslations As DataTable = m_DAL.ExecuteDataTable(sSQL)

        ' Recupero l'ID della lingua 
        Dim iIDLanguage As Integer
        sSQL = "SELECT ID FROM Languages WHERE Code = '" & m_LanguageID & "' "
        iIDLanguage = Nni(m_DAL.ExecuteScalar(sSQL))

        Dim drTranslations() As DataRow = oDTTranslations.Select("IDLanguage = " & iIDLanguage)

        If drTranslations.Length > 0 Then ' Ho delle traduzioni associate, altrimenti uso quelle di Default
            For Each drTranslation As DataRow In drTranslations
                If Nz(drTranslation("ObejctTranslation")) <> "" Then
                    ' Provo a recuperare solo la traduzione legata al singolo gioco
                    ' mi serve perchè posso avere traduzioni globali, legate ai vecchi giochi che non hanno associato il game
                    Dim drTraduzione As DataRow = oDTTranslations.Select("IDLanguage = " & iIDLanguage & " AND ObjectName = '" & Nz(drTranslation("ObjectName")) & "' AND IDGame = " & m_IDGame).FirstOrDefault
                    If Not drTraduzione Is Nothing Then
                        If Not lstReturn.Exists(Function(x) x.Key = Nz(drTraduzione("ObjectName")) And x.Value = Nz(drTraduzione("ObejctTranslation"))) Then
                            lstReturn.Add(New KeyValuePair(Of String, String)(Nz(drTraduzione("ObjectName")), Nz(drTraduzione("ObejctTranslation"))))
                        End If

                    Else
                        If Not lstReturn.Exists(Function(x) x.Key = Nz(drTranslation("ObjectName")) And x.Value = Nz(drTranslation("ObejctTranslation"))) Then
                            lstReturn.Add(New KeyValuePair(Of String, String)(Nz(drTranslation("ObjectName")), Nz(drTranslation("ObejctTranslation"))))
                        End If

                    End If

                Else
                    If Not lstReturn.Exists(Function(x) x.Key = Nz(drTranslation("ObjectName")) And x.Value = Nz(drTranslation("ObjectDescription"))) Then
                        lstReturn.Add(New KeyValuePair(Of String, String)(Nz(drTranslation("ObjectName")), Nz(drTranslation("ObjectDescription"))))
                    End If
                End If
            Next

        Else

            For Each drTranslation As DataRow In oDTTranslations.Rows
                If Not lstReturn.Exists(Function(x) x.Key = Nz(drTranslation("ObjectName")) And x.Value = Nz(drTranslation("ObjectDescription"))) Then
                    lstReturn.Add(New KeyValuePair(Of String, String)(Nz(drTranslation("ObjectName")), Nz(drTranslation("ObjectDescription"))))
                End If
            Next

        End If

        Return lstReturn
    End Function

    Private Sub SetTranslationControl(ListControl As List(Of KeyValuePair(Of String, String)), Controls As List(Of Control))
        For Each oPair As KeyValuePair(Of String, String) In ListControl
            Dim oControl As Control = FindControlRecursive(m_WebPage, oPair.Key)

            If Not IsNothing(oControl) Then
                If TypeOf oControl Is Label Then
                    CType(oControl, Label).Text = HttpContext.Current.Server.HtmlDecode(oPair.Value)

                ElseIf TypeOf oControl Is Telerik.Web.UI.RadLabel Then
                    CType(oControl, Telerik.Web.UI.RadLabel).Text = HttpContext.Current.Server.HtmlDecode(oPair.Value)

                ElseIf TypeOf oControl Is Literal Then
                    CType(oControl, Literal).Text = HttpContext.Current.Server.HtmlDecode(oPair.Value)

                ElseIf TypeOf oControl Is Telerik.Web.UI.RadHtmlChart Then
                    CType(oControl, Telerik.Web.UI.RadHtmlChart).ChartTitle.Text = HttpContext.Current.Server.HtmlDecode(oPair.Value)

                ElseIf TypeOf oControl Is Telerik.Web.UI.RadButton Then
                    CType(oControl, Telerik.Web.UI.RadButton).Text = HttpContext.Current.Server.HtmlDecode(oPair.Value)

                End If
            End If
        Next
    End Sub

    Private Function WriteToTempFile(ByVal Data As String) As String
        ' Writes text to a temporary file and returns path
        Dim strFilename As String = System.IO.Path.GetTempFileName()
        Dim objFS As New System.IO.FileStream(strFilename, System.IO.FileMode.Append, System.IO.FileAccess.Write)

        ' Opens stream and begins writing
        Dim Writer As New System.IO.StreamWriter(objFS)
        Writer.BaseStream.Seek(0, System.IO.SeekOrigin.End)
        Writer.WriteLine(Data)
        Writer.Flush()

        ' Closes and returns temp path
        Writer.Close()
        Return strFilename
    End Function

End Class
