﻿Imports System.Data
Imports APS.APSUtility
Imports APPCore.Utility
Imports DALC4NET
Imports Telerik.Web.UI

Partial Class CheckOnLine_Player_WhatIf
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadData()
        Message.Visible = False

        UpdatePanelMain.Update()
    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadData()

        Message.Visible = False
        UpdatePanelMain.Update()
    End Sub

    Private Sub CheckOnLine_Player_WhatIf_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        m_SessionData = Session("SessionData")

        m_SiteMaster = Me.Master

        m_SiteMaster.LoadPeriodsValid = Session("IDRole") = "P"

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oCBOPeriod As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOPeriodValid As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriodValid"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler m_SiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler m_SiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(Session("IDGame")) ' Ultimo periodo
        oCBOPeriod.SelectedValue = Session("IDPeriodValid")
        m_SiteMaster.PeriodChange = Session("IDPeriodValid")

        Session("NameTeam") = GetTeamName(Session("IDTeam"))
        oCBOTeam.SelectedValue = Session("IDTeam")
        m_SiteMaster.TeamChange = Session("IDTeam")

    End Sub

    Private Sub CheckOnLine_Player_WhatIf_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        If Not Page.IsPostBack Then
            If Session("IDRole") = "P" Then
                m_SiteMaster.HandleReloadPeriods(HandleGetMaxPeriodValid(Session("IDGame")))
            End If
            LoadData()

            btnTranslate.Visible = Session("IDRole").Contains("D")

        End If

        ' Controllo tutti gli oggetti presenti
        Dim oTranslation As New TranslationObjectForm(Me, g_DAL, Nz(Session("LanguageActive")))

    End Sub

    Private Sub LoadData()
        tblProductionCapacity.Rows.Clear()
        LoadDataProduction()
    End Sub

    Private Sub LoadDataProduction()
        Dim LoansRequi, ActuaMaximLoans, LoansLevel, BankLevel, LoansAllowVar, PaymeToCover, DisinAllowPerio, CA1, CA2, CA3 As Double

        '/* CALCOLO PRESTITI DEL PERIODO */
        LoansRequi = GetDecisionPlayers("LoansRequi", Session("IDTeam"), Session("IDPeriod"), "", Session("IDGame"))
        ActuaMaximLoans = GetVariableState("ActuaMaximLoans", Session("IDTeam"), Session("IDPeriod"), "", Session("IDGame"))
        LoansLevel = GetVariableState("LoansLevel", Session("IDTeam"), Session("IDPeriod"), "", Session("IDGame"))
        BankLevel = GetVariableState("BankLevel", Session("IDTeam"), Session("IDPeriod"), "", Session("IDGame"))
        If (LoansRequi >= 0) Then
            LoansAllowVar = Math.Max(0, Math.Min(LoansRequi, ActuaMaximLoans - LoansLevel))
        Else
            LoansAllowVar = Math.Max(Math.Max(-BankLevel, LoansRequi), -LoansLevel)
        End If


        '/* CALCOLO DISINVESTIMENTI DEL PERIODO  */
        Dim TotalPointPerso As Double = GetVariableState("TotalPointPerso", Session("IDTeam"), Session("IDPeriod"), "", Session("IDGame"))
        Dim NewPersoPoint As Double = GetDecisionPlayers("NewPersoPoint", Session("IDTeam"), Session("IDPeriod"), "", Session("IDGame"))
        Dim NewCostPoint As Double = GetVariableDefineValue("NewCostPoint", Session("IDTeam"), 0, 0, Session("IDPeriod"), Session("IDGame"))
        Dim TotalStaffPerso As Double = GetVariableState("TotalStaffPerso", Session("IDTeam"), Session("IDPeriod"), "", Session("IDGame"))
        Dim NewPersoStaff As Double = GetDecisionPlayers("NewPersoStaff", Session("IDTeam"), Session("IDPeriod"), "", Session("IDGame"))
        Dim NewCostStaff As Double = GetVariableDefineValue("NewCostStaff", Session("IDTeam"), 0, 0, Session("IDPeriod"), Session("IDGame"))

        DisinAllowPerio = -Math.Min(0, Math.Max(-0.2 * TotalPointPerso, NewPersoPoint) * NewCostPoint * 2) _
                        - Math.Min(0, Math.Max(-0.2 * TotalStaffPerso, NewPersoStaff) * NewCostStaff * 2)

        '/* CALCOLO PAGAMENTI ANTICIPATI DA COPRIRE  */
        Dim TotalCentrStore As Double = GetVariableState("TotalCentrStore", Session("IDTeam"), Session("IDPeriod"), "", Session("IDGame"))
        Dim RentPerioCentr As Double = GetVariableDefineValue("RentPerioCentr", Session("IDTeam"), 0, 0, Session("IDPeriod"), Session("IDGame"))
        Dim TotalPerifStore As Double = GetVariableState("TotalPerifStore", Session("IDTeam"), Session("IDPeriod"), "", Session("IDGame"))
        Dim RentPerioPerif As Double = GetVariableDefineValue("RentPerioPerif", Session("IDTeam"), 0, 0, Session("IDPeriod"), Session("IDGame"))
        Dim GlobaActuaLease As Double = GetVariableDefineValue("GlobaActuaLease", Session("IDTeam"), 0, 0, Session("IDPeriod"), Session("IDGame"))
        Dim NewLeaseCost As Double = GetVariableDefineValue("NewLeaseCost", Session("IDTeam"), 0, 0, Session("IDPeriod"), Session("IDGame"))

        Dim TaxPayment As Double = GetVariableDefineValue("TaxPayment", Session("IDTeam"), 0, 0, Session("IDPeriod"), Session("IDGame"))

        PaymeToCover = TotalCentrStore * RentPerioCentr _
                     + TotalPerifStore * RentPerioPerif _
                     + TotalPointPerso * NewCostPoint _
                     + TotalStaffPerso * NewCostStaff _
                     + GlobaActuaLease * NewLeaseCost

        CA1 = BankLevel + LoansAllowVar + DisinAllowPerio - PaymeToCover - TaxPayment

        Dim LeaseCashFinan, PerifCashFinan, CentrStoreFinan, VehicCashFinan, PersoStaffFinan, PersoPointFinan, PlantCashFinan, AutomFinan, MktgFinan As Double

        Dim LeaseRequi As Double = GetDecisionPlayers("LeaseRequi", Session("IDTeam"), Session("IDPeriod"), "", Session("IDGame"))
        Dim DelayPaymeLease As Double = GetDecisionPlayers("DelayPaymeLease", Session("IDTeam"), Session("IDPeriod"), "", Session("IDGame"))
        Dim RicarTermiMese As Double = GetVariableBoss("RicarTermiMese", Session("IDTeam"), Session("IDPeriod"), "", Session("IDGame"))
        Dim DiritRitarPagam As Double = GetVariableBoss("DiritRitarPagam", Session("IDTeam"), Session("IDPeriod"), "", Session("IDGame"))
        Dim TechnAutomRequi As Double = GetDecisionPlayers("TechnAutomRequi", Session("IDTeam"), Session("IDPeriod"), "", Session("IDGame"))
        Dim ForwaPaymeAutom As Double = GetDecisionPlayers("ForwaPaymeAutom", Session("IDTeam"), Session("IDPeriod"), "", Session("IDGame"))
        Dim DiscoTermiMese As Double = GetVariableBoss("DiscoTermiMese", Session("IDTeam"), Session("IDPeriod"), "", Session("IDGame"))
        Dim DiritScontPagam As Double = GetVariableBoss("DiritScontPagam", Session("IDTeam"), Session("IDPeriod"), "", Session("IDGame"))
        Dim TrainServiRequi As Double = GetDecisionPlayers("TrainServiRequi", Session("IDTeam"), Session("IDPeriod"), "", Session("IDGame"))
        Dim AdverRequi As Double = GetDecisionPlayers("AdverRequi", Session("IDTeam"), Session("IDPeriod"), "", Session("IDGame"))
        Dim ForwaPaymeMktg As Double = GetDecisionPlayers("ForwaPaymeMktg", Session("IDTeam"), Session("IDPeriod"), "", Session("IDGame"))
        Dim PlantRequi As Double = GetDecisionPlayers("PlantRequi", Session("IDTeam"), Session("IDPeriod"), "", Session("IDGame"))
        Dim VehicRequi As Double = GetDecisionPlayers("VehicRequi", Session("IDTeam"), Session("IDPeriod"), "", Session("IDGame"))
        Dim NewVehicCost As Double = GetVariableBoss("NewVehicCost", Session("IDTeam"), Session("IDPeriod"), "", Session("IDGame"))
        Dim PerifStoreRequi As Double = GetVariableBoss("PerifStoreRequi", Session("IDTeam"), Session("IDPeriod"), "", Session("IDGame"))
        Dim CentrStoreRequi As Double = GetDecisionPlayers("CentrStoreRequi", Session("IDTeam"), Session("IDPeriod"), "", Session("IDGame"))

        LeaseCashFinan = LeaseCashFinan + LeaseRequi * NewLeaseCost * ((3 - DelayPaymeLease) / 3) * ((100 + DelayPaymeLease * RicarTermiMese * DiritRitarPagam) / 100)

        AutomFinan = TechnAutomRequi * ((100 - ForwaPaymeAutom * DiscoTermiMese * DiritScontPagam) / 100) * ((0 + ForwaPaymeAutom) / 3)

        MktgFinan = (TrainServiRequi + AdverRequi) * ((100 - ForwaPaymeMktg * DiscoTermiMese * DiritScontPagam) / 100) * ((0 + ForwaPaymeMktg) / 3)

        PlantCashFinan = PlantRequi

        VehicCashFinan = VehicRequi * NewVehicCost

        If PerifStoreRequi > 0 Then
            PerifCashFinan += PerifStoreRequi * RentPerioPerif
        End If

        If CentrStoreRequi > 0 Then
            CentrStoreFinan += CentrStoreRequi * RentPerioCentr
        End If

        If NewPersoStaff > 0 Then
            PersoStaffFinan += NewPersoStaff * NewCostStaff
        End If

        If NewPersoPoint > 0 Then
            PersoPointFinan += NewPersoPoint * NewCostPoint
        End If

        Dim ProduCashFinan, PurchNicFinan, PurchEuropFinan, PromoToFinan, ExtraProduFinan As Double

        ProduCashFinan = 0
        PurchNicFinan = 0
        PurchEuropFinan = 0
        PromoToFinan = 0
        ExtraProduFinan = 0

        ' Ciclo sui prodotti per calcolare i dati corretti
        Dim oDTItems As DataTable = LoadItemsGame(Session("IDGame"), "")
        Dim oDTPlayers As DataTable = LoadTeamsGame(Session("IDGame"))
        Dim ProduBasicRequi As Double
        Dim ProduUnitCost As Double
        Dim DelayPaymeProdu As Double
        Dim PurchNICRequi As Double
        Dim RawNICCost As Double
        Dim PurchEuropRequi As Double
        Dim RawEuropCost As Double
        Dim DelayPaymeEurop As Double
        Dim MarkeExpenRequi As Double
        Dim DelayPaymePromo As Double
        Dim ForwaPaymeExtra As Double
        Dim ExtraRequi As Double
        Dim ThresDiscoExtra As Double
        Dim ExtraProduCost As Double
        Dim DiscoLevelExtra As Double

        For Each oRowItm As DataRow In oDTItems.Rows
            ProduBasicRequi = GetDecisionPlayers("ProduBasicRequi", Session("IDTeam"), Session("IDPeriod"), "var_" & GetTeamName(Session("IDTeam")) & "_ProduBasicRequi_" & oRowItm("VariableName"), Session("IDGame"))
            ProduUnitCost = GetVariableBoss("ProduUnitCost", Session("IDTeam"), Session("IDPeriod"), "ProduUnitCost_" & Nz(oRowItm("VariableName")), Session("IDGame"))
            DelayPaymeProdu = GetDecisionPlayers("DelayPaymeProdu", Session("IDTeam"), Session("IDPeriod"), "", Session("IDGame"))

            PurchNICRequi = GetDecisionPlayers("PurchNICRequi", Session("IDTeam"), Session("IDPeriod"), "var_" & GetTeamName(Session("IDTeam")) & "_PurchNICRequi_" & oRowItm("VariableName"), Session("IDGame"))
            RawNICCost = GetVariableBoss("RawNICCost", Session("IDTeam"), Session("IDPeriod"), "RawNICCost_" & Nz(oRowItm("VariableName")), Session("IDGame"))

            PurchEuropRequi = GetDecisionPlayers("PurchEuropRequi", Session("IDTeam"), Session("IDPeriod"), "var_" & GetTeamName(Session("IDTeam")) & "_PurchEuropRequi_" & oRowItm("VariableName"), Session("IDGame"))
            RawEuropCost = GetVariableBoss("RawEuropCost", Session("IDTeam"), Session("IDPeriod"), "RawEuropCost_" & Nz(oRowItm("VariableName")), Session("IDGame"))
            DelayPaymeEurop = GetDecisionPlayers("DelayPaymeEurop", Session("IDTeam"), Session("IDPeriod"), "", Session("IDGame"))

            MarkeExpenRequi = GetDecisionPlayers("MarkeExpenRequi", Session("IDTeam"), Session("IDPeriod"), "var_" & GetTeamName(Session("IDTeam")) & "_MarkeExpenRequi_" & oRowItm("VariableName"), Session("IDGame"))
            DelayPaymePromo = GetDecisionPlayers("DelayPaymePromo", Session("IDTeam"), Session("IDPeriod"), "", Session("IDGame"))
            ForwaPaymeExtra = GetDecisionPlayers("ForwaPaymeExtra", Session("IDTeam"), Session("IDPeriod"), "", Session("IDGame"))
            ExtraRequi = GetDecisionPlayers("ExtraRequi", Session("IDTeam"), Session("IDPeriod"), "", Session("IDGame"))
            ThresDiscoExtra = GetVariableBoss("ThresDiscoExtra", Session("IDTeam"), Session("IDPeriod"), "ThresDiscoExtra_" & Nz(oRowItm("VariableName")), Session("IDGame"))
            ExtraProduCost = GetVariableBoss("ExtraProduCost", Session("IDTeam"), Session("IDPeriod"), "ExtraProduCost_" & Nz(oRowItm("VariableName")), Session("IDGame"))
            DiscoLevelExtra = GetVariableBoss("DiscoLevelExtra", Session("IDTeam"), Session("IDPeriod"), "", Session("IDGame"))

            ProduCashFinan += ProduBasicRequi * ProduUnitCost * ((3 - DelayPaymeProdu) / 3) * ((100 + DelayPaymeProdu * RicarTermiMese * DiritRitarPagam) / 100)
            PurchNicFinan += (PurchNICRequi * RawNICCost) * (2 / 3)
            PurchEuropFinan += (PurchEuropRequi * RawEuropCost) * ((2 - DelayPaymeEurop) / 3) * ((100 + DelayPaymeEurop * RicarTermiMese * DiritRitarPagam) / 100)
            PromoToFinan += MarkeExpenRequi * ((3 - DelayPaymePromo) / 3) * ((100 + DelayPaymePromo * RicarTermiMese * DiritRitarPagam) / 100)
            ExtraProduFinan += ((100 - ForwaPaymeExtra * DiscoTermiMese * DiritScontPagam) / 100) * ((0 + ForwaPaymeExtra) / 3) _
                             * (Math.Min(ExtraRequi, ThresDiscoExtra) * ExtraProduCost + (Math.Max(0, ExtraRequi - ThresDiscoExtra) *
                             ExtraProduCost * (100 - DiscoLevelExtra) / 100))
        Next

        CA2 = ProduCashFinan + LeaseCashFinan + PerifCashFinan + PurchNicFinan + PurchEuropFinan + CentrStoreFinan _
            + VehicCashFinan + PersoStaffFinan + PersoPointFinan + PromoToFinan + MktgFinan + AutomFinan + ExtraProduFinan

        If (CA1 >= CA2 Or CA2 = 0) Then
            CA3 = 100
        Else
            CA3 = Math.Max(0, Math.Min(CA1 / CA2 * 100, 100))
        End If
        Session("CA3") = (Math.Floor(CA3 * 10) / 10).ToString("n1") + "%"

        Dim PR1, PR2, PR3, CP1, CP2, CP3, CP4 As Double

        ' Capacità produttiva da nuovi leasing (decisione)
        CP1 = Math.Round(LeaseRequi * CA3 / 100, 0) * 6000 * (3 / 12)

        Dim sNomeTeam As String = GetTeamName(Session("IDTeam"))
        ' Capacità produttiva da macchine di proprietà acquistate
        CP2 = GetVariableState("PlantPerAge", Session("IDTeam"), Session("IDPeriod"), "var_" & sNomeTeam & "_PlantPerAge_1", Session("IDGame")) * 6000 * (3 / 12) _
            + GetVariableState("LeasePerAge", Session("IDTeam"), Session("IDPeriod"), "var_" & sNomeTeam & "_LeasePerAge_2", Session("IDGame"), 2) * 5500 * (3 / 12) _
            + GetVariableState("LeasePerAge", Session("IDTeam"), Session("IDPeriod"), "var_" & sNomeTeam & "_LeasePerAge_3", Session("IDGame"), 3) * 5000 * (3 / 12) _
            + GetVariableState("LeasePerAge", Session("IDTeam"), Session("IDPeriod"), "var_" & sNomeTeam & "_LeasePerAge_4", Session("IDGame"), 4) * 4500 * (3 / 12) _
            + GetVariableState("LeasePerAge", Session("IDTeam"), Session("IDPeriod"), "var_" & sNomeTeam & "_LeasePerAge_5", Session("IDGame"), 5) * 4000 * (3 / 12) _
            + GetVariableState("LeasePerAge", Session("IDTeam"), Session("IDPeriod"), "var_" & sNomeTeam & "_LeasePerAge_6", Session("IDGame"), 6) * 3500 * (3 / 12) _
            + GetVariableState("LeasePerAge", Session("IDTeam"), Session("IDPeriod"), "var_" & sNomeTeam & "_LeasePerAge_7", Session("IDGame"), 7) * 3000 * (3 / 12) _
            + GetVariableState("LeasePerAge", Session("IDTeam"), Session("IDPeriod"), "var_" & sNomeTeam & "_LeasePerAge_8", Session("IDGame"), 8) * 2500 * (3 / 12) _
            + GetVariableState("LeasePerAge", Session("IDTeam"), Session("IDPeriod"), "var_" & sNomeTeam & "_LeasePerAge_9", Session("IDGame"), 9) * 2000 * (3 / 12) _
            + GetVariableState("LeasePerAge", Session("IDTeam"), Session("IDPeriod"), "var_" & sNomeTeam & "_LeasePerAge_10", Session("IDGame"), 10) * 1500 * (3 / 12) _
            + GetVariableState("LeasePerAge", Session("IDTeam"), Session("IDPeriod"), "var_" & sNomeTeam & "_LeasePerAge_11", Session("IDGame"), 11) * 1000 * (3 / 12) _
            + GetVariableState("LeasePerAge", Session("IDTeam"), Session("IDPeriod"), "var_" & sNomeTeam & "_LeasePerAge_12", Session("IDGame"), 12) * 500 * (3 / 12)

        'Capacità produttiva da leasing attivi 
        CP3 = GetVariableState("LeasePerAge", Session("IDTeam"), Session("IDPeriod"), "var_" & sNomeTeam & "_LeasePerAge_1", Session("IDGame"), 1) * 5500 * (3 / 12) _
            + GetVariableState("LeasePerAge", Session("IDTeam"), Session("IDPeriod"), "var_" & sNomeTeam & "_LeasePerAge_2", Session("IDGame"), 2) * 5000 * (3 / 12) _
            + GetVariableState("LeasePerAge", Session("IDTeam"), Session("IDPeriod"), "var_" & sNomeTeam & "_LeasePerAge_3", Session("IDGame"), 3) * 4500 * (3 / 12)

        'Capacità produttiva da macchine di proprietà dismesse
        Dim Rimanenti, Macchinari, Dismessi As Double

        Rimanenti = -Math.Min(0, Nn(GetDecisionPlayers("PlantRequi", Session("IDTeam"), Session("IDPeriod"), "", Session("IDGame"))))

        CP4 = 0
        Dim iIndex As Integer
        For iIndex = 11 To 1 Step -1
            Macchinari = GetVariableDefineValue("PlantRequi", Session("IDTeam"), 0, iIndex, Session("IDPeriod"), Session("IDGame"))
            Dismessi = Math.Min(Macchinari, Rimanenti)
            Rimanenti -= Dismessi
            CP4 -= Dismessi * (1500 - (iIndex) * 125)
        Next iIndex

        PR1 = CP1 + CP2 + CP3 + CP4
        Session("CP1") = CP1.ToString("n0")
        Session("CP2") = CP2.ToString("n0")
        Session("CP3") = CP3.ToString("n0")
        Session("CP4") = CP4.ToString("n0")
        Session("PR1") = PR1.ToString("n0")

        For Each oRowItm As DataRow In oDTItems.Rows
            PR2 = PR2 + Nn(GetVariableBoss("HoursPerItem", Session("IDTeam"), Session("IDPeriod"), "var_" & m_SiteMaster.TeamNameGet & "_HoursPerItem_" & oRowItm("VariableName"), Session("IDGame"))) _
                * Math.Min(Nn(GetDecisionPlayers("ProduBasicRequi", Session("IDTeam"), Session("IDPeriod"), "var_" & m_SiteMaster.TeamNameGet & "_ProduBasicRequi_" & oRowItm("VariableName"), Session("IDGame"))) * CA3 / 100,
                          Nn(GetDecisionPlayers("PurchEuropRequi", Session("IDTeam"), Session("IDPeriod"), "var_" & m_SiteMaster.TeamNameGet & "_PurchEuropRequi_" & oRowItm("VariableName"), Session("IDGame"))) * CA3 / 100 _
                + Nn(GetDecisionPlayers("PurchNICRequi", Session("IDTeam"), Session("IDPeriod"), "var_" & m_SiteMaster.TeamNameGet & "_PurchNICRequi_" & oRowItm("VariableName"), Session("IDGame"))) * (2 / 3) * CA3 / 100 _
                + Nn(GetVariableState("StockRawMater", Session("IDTeam"), Session("IDPeriod"), "var_" & m_SiteMaster.TeamNameGet & "_StockRawMater_" & oRowItm("VariableName"), Session("IDGame")))) _
                + Nn(GetVariableState("PurchDelivNIC", Session("IDTeam"), Session("IDPeriod"), "var_" & m_SiteMaster.TeamNameGet & "_PurchDelivNIC_" & oRowItm("VariableName"), Session("IDGame")))
        Next

        Session("PR2") = PR2.ToString("n0")

        If (PR1 = 0) Then
            PR3 = 100
        Else
            PR3 = Math.Min(100, PR2 / PR1 * 100)
        End If

        ' MODIFICA: ARROTONDA PER DIFETTO, PER EVITARE CHE UN VALORE >99,5 SIA VIUSALIZZATO COME 100
        Session("PR3") = (Math.Floor(PR3 * 10) / 10).ToString("n1")

        ' Inizio la costruzione della tabella
        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        ' Costruisco la riga Header
        tblHeaderRow = New TableHeaderRow
        tblHeaderCell = New TableHeaderCell

        ' Prima cella della prima riga dell'Header vuota
        Dim oLBLTitleGrid As New RadLabel
        oLBLTitleGrid.ID = "lblGridTitle"
        oLBLTitleGrid.Text = "Production"
        oLBLTitleGrid.ForeColor = Drawing.Color.White
        tblHeaderCell.Controls.Add(oLBLTitleGrid)
        tblHeaderCell.BorderStyle = BorderStyle.None
        tblHeaderCell.ColumnSpan = 2
        tblHeaderCell.Style.Add("background", "#525252")

        tblHeaderRow.Cells.Add(tblHeaderCell)

        tblProductionCapacity.Rows.Add(tblHeaderRow)

        ' Aggiungo le righe di dettaglio
        ' Prima riga
        tblRow = New TableRow
        tblCell = New TableCell

        Dim oLblProductionCapacityNew As New RadLabel
        oLblProductionCapacityNew.ID = "lblProductionCapacityNew"
        oLblProductionCapacityNew.Text = "Production capacity of the new leased machines"
        oLblProductionCapacityNew.ForeColor = Drawing.Color.DarkRed
        tblCell.Controls.Add(oLblProductionCapacityNew)
        tblCell.Style.Add("width", "70%")
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim oLblProductionCapacityNewValue As New RadLabel
        oLblProductionCapacityNewValue.ID = "lblProductionCapacityValueNew"
        oLblProductionCapacityNewValue.Text = CP1.ToString("N0") & " h"
        tblCell.Controls.Add(oLblProductionCapacityNewValue)
        tblCell.Style.Add("width", "30%")
        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblProductionCapacity.Rows.Add(tblRow)

        ' Seconda riga
        tblRow = New TableRow
        tblCell = New TableCell

        Dim oLblProductionCapacityExist As New RadLabel
        oLblProductionCapacityExist.ID = "oLblProductionCapacityExist"
        oLblProductionCapacityExist.Text = "Production capacity of the existing owned machines"
        oLblProductionCapacityExist.ForeColor = Drawing.Color.DarkRed
        tblCell.Controls.Add(oLblProductionCapacityExist)
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim oLblProductionCapacityExistValue As New RadLabel
        oLblProductionCapacityExistValue.ID = "lblProductionCapacityExist"
        oLblProductionCapacityExistValue.Text = CP2.ToString("N0") & " h"
        tblCell.Controls.Add(oLblProductionCapacityExistValue)
        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblProductionCapacity.Rows.Add(tblRow)

        ' Terza riga
        tblRow = New TableRow
        tblCell = New TableCell

        Dim oLblProductionCapacityExistOwned As New RadLabel
        oLblProductionCapacityExistOwned.ID = "oLblProductionCapacityExistOwned"
        oLblProductionCapacityExistOwned.Text = "Production capacity of the existing leased  machines"
        oLblProductionCapacityExistOwned.ForeColor = Drawing.Color.DarkRed
        tblCell.Controls.Add(oLblProductionCapacityExistOwned)
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim oLblProductionCapacityExistOwnedValue As New RadLabel
        oLblProductionCapacityExistOwnedValue.ID = "lblProductionCapacityExistOwnedValue"
        oLblProductionCapacityExistOwnedValue.Text = CP3.ToString("N0") & " h"
        tblCell.Controls.Add(oLblProductionCapacityExistOwnedValue)
        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblProductionCapacity.Rows.Add(tblRow)

        ' Quarta riga
        tblRow = New TableRow
        tblCell = New TableCell

        Dim oLblCapacityProductionLost As New RadLabel
        oLblCapacityProductionLost.ID = "oLblCapacityProductionLost"
        oLblCapacityProductionLost.Text = "(-) Production capacity lost for owned machines dismissed (decision)"
        oLblCapacityProductionLost.ForeColor = Drawing.Color.DarkBlue
        tblCell.Controls.Add(oLblCapacityProductionLost)
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim oLblCapacityProductionLostValue As New RadLabel
        oLblCapacityProductionLostValue.ID = "lblCapacityProductionLostValue"
        oLblCapacityProductionLostValue.Text = CP4.ToString("N0") & " h"
        tblCell.Controls.Add(oLblCapacityProductionLostValue)
        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblProductionCapacity.Rows.Add(tblRow)

        ' Quinta riga
        tblRow = New TableRow
        tblCell = New TableCell

        Dim oLblProductionTotal As New RadLabel
        oLblProductionTotal.ID = "oLblProductionTotal"
        oLblProductionTotal.Text = "Total production capacity for the period"
        oLblProductionTotal.ForeColor = Drawing.Color.DarkBlue
        tblCell.Controls.Add(oLblProductionTotal)
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim oLblProductionTotalValue As New RadLabel
        oLblProductionTotalValue.ID = "lblProductionTotalValue"
        oLblProductionTotalValue.Text = PR1.ToString("N0") & " h"
        tblCell.Controls.Add(oLblProductionTotalValue)
        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblProductionCapacity.Rows.Add(tblRow)

        ' Sesta riga
        tblRow = New TableRow
        tblCell = New TableCell

        Dim oLblProdctionPlanned As New RadLabel
        oLblProdctionPlanned.ID = "oLblProdctionPlanned"
        oLblProdctionPlanned.Text = "Production level planned for the current period"
        oLblProdctionPlanned.ForeColor = Drawing.Color.DarkBlue
        tblCell.Controls.Add(oLblProdctionPlanned)
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim oLblProdctionPlannedValue As New RadLabel
        oLblProdctionPlannedValue.ID = "lblProdctionPlannedValue"
        oLblProdctionPlannedValue.Text = PR2.ToString("N0") & " h"
        tblCell.Controls.Add(oLblProdctionPlannedValue)
        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblProductionCapacity.Rows.Add(tblRow)

        ' Settima riga
        tblRow = New TableRow
        tblCell = New TableCell

        Dim oLblProductionPercentage As New RadLabel
        oLblProductionPercentage.ID = "oLblProductionPercentage"
        oLblProductionPercentage.Text = "Expenditure team"
        oLblProductionPercentage.ForeColor = Drawing.Color.DarkBlue
        tblCell.Controls.Add(oLblProductionPercentage)
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim oLblProductionPercentageValue As New RadLabel
        oLblProductionPercentageValue.ID = "lblProductionPercentageValue"
        oLblProductionPercentageValue.Text = PR3.ToString("N1") & " %"
        tblCell.Controls.Add(oLblProductionPercentageValue)
        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblProductionCapacity.Rows.Add(tblRow)

        ' Ottava riga
        tblRow = New TableRow
        tblCell = New TableCell

        Dim oLblDescription As New RadLabel
        oLblDescription.ID = "oLblDescription"
        oLblDescription.Text = "The indicator shows a 100% of planned production capacity the current capacity has been saturated! " _
                             & " In this situation it is possible that part of the planne production will not be prodeuced for a lack of machines !!!"
        oLblDescription.ForeColor = Drawing.Color.DarkRed
        tblCell.Controls.Add(oLblDescription)
        tblCell.BorderStyle = BorderStyle.None
        tblCell.ColumnSpan = 2
        tblRow.Cells.Add(tblCell)

        tblProductionCapacity.Rows.Add(tblRow)

    End Sub

    Private Sub btnTranslate_Click(sender As Object, e As EventArgs) Handles btnTranslate.Click
        Session("PageOrigin") = Me.ToString().Substring(4, Me.ToString().Substring(4).Length - 5) + ".aspx"
        Response.Redirect("PageTranslation.aspx")
        Response.End()
    End Sub

End Class
