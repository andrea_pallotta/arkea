﻿Imports System.Data
Imports APS.APSUtility
Imports APPCore.Utility
Imports Telerik.Web.UI

Partial Class CheckOnLine_Player_WhatIf_Cash
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadData()
        Message.Visible = False

        UpdatePanelMain.Update()
    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadData()

        Message.Visible = False
        UpdatePanelMain.Update()
    End Sub

    Private Sub CheckOnLine_Player_WhatIf_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        m_SessionData = Session("SessionData")

        m_SiteMaster = Me.Master

        m_SiteMaster.LoadPeriodsValid = Session("IDRole") = "P"

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oCBOPeriod As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOPeriodValid As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriodValid"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler m_SiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler m_SiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(Session("IDGame")) ' Ultimo periodo
        oCBOPeriod.SelectedValue = Session("IDPeriodValid")
        m_SiteMaster.PeriodChange = Session("IDPeriodValid")

        Session("NameTeam") = GetTeamName(Session("IDTeam"))
        oCBOTeam.SelectedValue = Session("IDTeam")
        m_SiteMaster.TeamChange = Session("IDTeam")

        SQLConnClose(g_DAL.GetConnObject)
    End Sub

    Private Sub CheckOnLine_Player_WhatIf_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        If Not Page.IsPostBack Then
            If Session("IDRole") = "P" Then
                m_SiteMaster.HandleReloadPeriods(HandleGetMaxPeriodValid(Session("IDGame")))
            End If
            LoadData()

            btnTranslate.Visible = Session("IDRole").Contains("D")

        End If

        ' Controllo tutti gli oggetti presenti
        Dim oTranslation As New TranslationObjectForm(Me, g_DAL, Nz(Session("LanguageActive")))

    End Sub

    Private Sub LoadData()
        tblCash.Rows.Clear()
        LoadDataCash()
    End Sub

    Private Sub LoadDataCash()
        Message.Visible = True

        Try
            Dim iIDPeriodo As Integer = Session("IDPeriod")
            Dim iIDTeam As Integer = Session("IDTeam")
            Dim iIDGame As Integer = Session("IDGame")

            Dim LoansRequi, ActuaMaximLoans, LoansLevel, BankLevel, LoansAllow, PaymeToCover, DisinAllowPerio, CA1, CA2, CA3 As Double

            '/* CALCOLO PRESTITI DEL PERIODO */
            LoansRequi = GetDecisionPlayers("LoansRequi", iIDTeam, iIDPeriodo, "", iIDGame)
            ActuaMaximLoans = GetVariableState("ActuaMaximLoans", iIDTeam, iIDPeriodo, "", iIDGame)
            LoansLevel = GetVariableState("LoansLevel", iIDTeam, iIDPeriodo, "", iIDGame)
            BankLevel = GetVariableState("BankLevel", iIDTeam, iIDPeriodo, "", iIDGame)
            If (LoansRequi >= 0) Then
                LoansAllow = Math.Max(0, Math.Min(LoansRequi, ActuaMaximLoans - LoansLevel))
            Else
                LoansAllow = Math.Max(Math.Max(-BankLevel, LoansRequi), -LoansLevel)
            End If

            '/ CALCOLO DISINVESTIMENTI DEL PERIODO  /
            Dim TotalPointPerso As Double = GetVariableState("TotalPointPerso", iIDTeam, iIDPeriodo, "", iIDGame)
            Dim NewPersoPoint As Double = GetDecisionPlayers("NewPersoPoint", iIDTeam, iIDPeriodo, "", iIDGame)
            Dim PersoCostPoint As Double = GetVariableBoss("PersoCostPoint", 0, iIDPeriodo, "", iIDGame) / (12 / Nni(Session("LunghezzaPeriodo")))
            Dim TotalStaffPerso As Double = GetVariableState("TotalStaffPerso", iIDTeam, iIDPeriodo, "", iIDGame)
            Dim NewPersoStaff As Double = GetDecisionPlayers("NewPersoStaff", iIDTeam, iIDPeriodo, "", iIDGame)
            Dim PersoCostStaff As Double = GetVariableBoss("PersoCostStaff", 0, iIDPeriodo, "", iIDGame) / (12 / Nni(Session("LunghezzaPeriodo")))

            DisinAllowPerio = -Math.Min(0, Math.Max(-0.2 * TotalPointPerso, NewPersoPoint) * PersoCostPoint * 2) _
                              - Math.Min(0, Math.Max(-0.2 * TotalStaffPerso, NewPersoStaff) * PersoCostStaff * 2)

            '/* CALCOLO PAGAMENTI ANTICIPATI DA COPRIRE  */
            Dim TotalCentrStore As Double = GetVariableState("TotalCentrStore", iIDTeam, iIDPeriodo, "", iIDGame)
            Dim NewCentrCost As Double = GetVariableBoss("NewCentrCost", 0, iIDPeriodo, "", iIDGame) / (12 / Nni(Session("LunghezzaPeriodo")))
            Dim TotalPerifStore As Double = GetVariableState("TotalPerifStore", iIDTeam, iIDPeriodo, "", iIDGame)
            Dim NewStoreCost As Double = GetVariableBoss("NewStoreCost", 0, iIDPeriodo, "", iIDGame) / (12 / Nni(Session("LunghezzaPeriodo")))
            Dim GlobaActuaLease As Double = GetVariableDefineValue("GlobaActuaLease", iIDTeam, 0, 0, iIDPeriodo, iIDGame)
            Dim LeasePlantCost As Double = GetVariableBoss("LeasePlantCost", 0, iIDPeriodo, "", iIDGame) / (12 / Nni(Session("LunghezzaPeriodo")))

            Dim TaxFunds As Double = GetVariableState("TaxFunds", iIDTeam, iIDPeriodo, "", iIDGame)

            Dim SuperExtraFido As Double = GetDecisionPlayers("SuperExtraFido", iIDTeam, iIDPeriodo, "", iIDGame)

            PaymeToCover = TotalCentrStore * NewCentrCost _
                         + TotalPerifStore * NewStoreCost _
                         + TotalPointPerso * PersoCostPoint _
                         + TotalStaffPerso * PersoCostStaff _
                         + GlobaActuaLease * LeasePlantCost

            CA1 = BankLevel + LoansAllow + SuperExtraFido + DisinAllowPerio - PaymeToCover - TaxFunds

            Dim LeaseCashFinan, PerifCashFinan, CentrStoreFinan, VehicCashFinan, PersoStaffFinan, PersoPointFinan, PlantCashFinan, AutomFinan, MktgFinan As Double

            Dim LeaseRequi As Double = GetDecisionPlayers("LeaseRequi", iIDTeam, iIDPeriodo, "", iIDGame)
            Dim DelayPaymeLease As Double = GetDecisionPlayers("DelayPaymeLease", iIDTeam, iIDPeriodo, "", iIDGame)
            Dim RicarTermiMese As Double = GetVariableBoss("RicarTermiMese", iIDTeam, iIDPeriodo, "", iIDGame)
            Dim DiritRitarPagam As Double = GetVariableBoss("DiritRitarPagam", iIDTeam, iIDPeriodo, "", iIDGame)
            Dim TechnAutomRequi As Double = GetDecisionPlayers("TechnAutomRequi", iIDTeam, iIDPeriodo, "", iIDGame)
            Dim ForwaPaymeAutom As Double = GetDecisionPlayers("ForwaPaymeAutom", iIDTeam, iIDPeriodo, "", iIDGame)
            Dim DiscoTermiMese As Double = GetVariableBoss("DiscoTermiMese", 0, iIDPeriodo, "", iIDGame)
            Dim DiritScontPagam As Double = GetVariableBoss("DiritScontPagam", iIDTeam, iIDPeriodo, "", iIDGame)
            Dim TrainServiRequi As Double = GetDecisionPlayers("TrainServiRequi", iIDTeam, iIDPeriodo, "", iIDGame)
            Dim AdverRequi As Double = GetDecisionPlayers("AdverRequi", iIDTeam, iIDPeriodo, "", iIDGame)
            Dim ForwaPaymeMktg As Double = GetDecisionPlayers("ForwaPaymeMktg", iIDTeam, iIDPeriodo, "", iIDGame)
            Dim PlantRequi As Double = GetDecisionPlayers("PlantRequi", iIDTeam, iIDPeriodo, "", iIDGame)
            Dim VehicRequi As Double = GetDecisionPlayers("VehicRequi", iIDTeam, iIDPeriodo, "", iIDGame)
            Dim NewVehicCost As Double = GetVariableBoss("NewVehicCost", iIDTeam, iIDPeriodo, "", iIDGame)
            Dim PerifStoreRequi As Double = GetDecisionPlayers("PerifStoreRequi", iIDTeam, iIDPeriodo, "", iIDGame)
            Dim CentrStoreRequi As Double = GetDecisionPlayers("CentrStoreRequi", iIDTeam, iIDPeriodo, "", iIDGame)
            Dim LagPaymeMktg As Double = GetVariableBoss("LagPaymeMktg", 0, iIDPeriodo, "", iIDGame)
            Dim LagPaymeExtra As Double = GetVariableBoss("LagPaymeExtra", 0, iIDPeriodo, "", iIDGame)
            Dim Advertising As Double = 0.0

            ' (AdverRequi + TrainServiRequi) * (( PeriodLength - LagPaymeMktg ) / PeriodLength)
            Advertising = (AdverRequi + TrainServiRequi) * ((Nni(Session("LunghezzaPeriodo")) - LagPaymeMktg) / Nni(Session("LunghezzaPeriodo")))

            ' LeaseCashFinan = LeaseCashFinan + LeaseRequi * LeasePlantCost * ((3 - DelayPaymeLease) / 3) * ((100 + DelayPaymeLease * RicarTermiMese * DiritRitarPagam) / 100)
            LeaseCashFinan = LeaseCashFinan + LeaseRequi * LeasePlantCost * ((Nni(Session("LunghezzaPeriodo")) - DelayPaymeLease) / Nni(Session("LunghezzaPeriodo"))) *
                             ((100 + DelayPaymeLease * RicarTermiMese * DiritRitarPagam) / 100)

            ' AutomFinan = TechnAutomRequi * ((100 - ForwaPaymeAutom * DiscoTermiMese * DiritScontPagam) / 100) * ((0 + ForwaPaymeAutom) / 3)
            AutomFinan = TechnAutomRequi * ((100 - ForwaPaymeAutom * DiscoTermiMese * DiritScontPagam) / 100) * ((0 + ForwaPaymeAutom) / Nni(Session("LunghezzaPeriodo")))

            ' MktgFinan = (TrainServiRequi + AdverRequi) * ((100 - ForwaPaymeMktg * DiscoTermiMese * DiritScontPagam) / 100) * ((0 + ForwaPaymeMktg) / 3)
            MktgFinan = (TrainServiRequi + AdverRequi) * ((100 - ForwaPaymeMktg * DiscoTermiMese * DiritScontPagam) / 100) *
                        ((Nni(Session("LunghezzaPeriodo")) - LagPaymeMktg + ForwaPaymeMktg) / Nni(Session("LunghezzaPeriodo")))
            MktgFinan = MktgFinan * (Math.Min(Nni(Session("LunghezzaPeriodo")), Nni(Session("LunghezzaPeriodo")) - LagPaymeMktg) / Nni(Session("LunghezzaPeriodo")))

            PlantCashFinan = PlantRequi

            VehicCashFinan = VehicRequi * NewVehicCost

            If PerifStoreRequi > 0 Then
                PerifCashFinan += PerifStoreRequi * NewStoreCost
            End If

            If CentrStoreRequi > 0 Then
                CentrStoreFinan = CentrStoreRequi * NewCentrCost
            End If

            If NewPersoStaff > 0 Then
                PersoStaffFinan = NewPersoStaff * PersoCostStaff
            End If

            If NewPersoPoint > 0 Then
                PersoPointFinan = NewPersoPoint * PersoCostPoint
            End If

            Dim ProduCashFinan, PurchNicFinan, PurchEuropFinan, PromoToFinan, ExtraProduFinan As Double

            ProduCashFinan = 0
            PurchNicFinan = 0
            PurchEuropFinan = 0
            PromoToFinan = 0
            ExtraProduFinan = 0

            ' Ciclo sui prodotti per calcolare i dati corretti
            Dim oDTItems As DataTable = LoadItemsGame(iIDGame, "")
            Dim ProduBasicRequi As Double
            Dim ProduUnitCost As Double
            Dim DelayPaymeProdu As Double
            Dim PurchNICRequi As Double
            Dim RawNICCost As Double
            Dim PurchEuropRequi As Double
            Dim RawEuropCost As Double
            Dim DelayPaymeEurop As Double
            Dim MarkeExpenRequi As Double
            Dim DelayPaymePromo As Double
            Dim ForwaPaymeExtra As Double
            Dim ExtraRequi As Double
            Dim ThresDiscoExtra As Double
            Dim ExtraProduCost As Double
            Dim DiscoLevelExtra As Double

            Dim iLunghezzaPeriodo As Integer = Nni(Session("LunghezzaPeriodo"))

            For Each oRowItm As DataRow In oDTItems.Rows
                ProduBasicRequi = GetDecisionPlayers("ProduBasicRequi", iIDTeam, iIDPeriodo, "var_" & GetTeamName(iIDTeam) & "_ProduBasicRequi_" & oRowItm("VariableName"), iIDGame)
                ProduUnitCost = GetVariableBoss("ProduUnitCost", iIDTeam, iIDPeriodo, "ProduUnitCost_" & Nz(oRowItm("VariableName")), iIDGame)
                DelayPaymeProdu = GetDecisionPlayers("DelayPaymeProdu", iIDTeam, iIDPeriodo, "", iIDGame)

                PurchNICRequi = GetDecisionPlayers("PurchNICRequi", iIDTeam, iIDPeriodo, "var_" & GetTeamName(iIDTeam) & "_PurchNICRequi_" & oRowItm("VariableName"), iIDGame)
                RawNICCost = GetVariableBoss("RawNICCost", iIDTeam, iIDPeriodo, "RawNICCost_" & Nz(oRowItm("VariableName")), iIDGame)

                PurchEuropRequi = GetDecisionPlayers("PurchEuropRequi", iIDTeam, iIDPeriodo, "var_" & GetTeamName(iIDTeam) & "_PurchEuropRequi_" & oRowItm("VariableName"), iIDGame)
                RawEuropCost = GetVariableBoss("RawEuropCost", iIDTeam, iIDPeriodo, "RawEuropCost_" & Nz(oRowItm("VariableName")), iIDGame)
                DelayPaymeEurop = GetDecisionPlayers("DelayPaymeEurop", iIDTeam, iIDPeriodo, "", iIDGame)

                MarkeExpenRequi = GetDecisionPlayers("MarkeExpenRequi", iIDTeam, iIDPeriodo, "var_" & GetTeamName(iIDTeam) & "_MarkeExpenRequi_" & oRowItm("VariableName"), iIDGame)
                DelayPaymePromo = GetDecisionPlayers("DelayPaymePromo", iIDTeam, iIDPeriodo, "", iIDGame)
                ForwaPaymeExtra = GetDecisionPlayers("ForwaPaymeExtra", iIDTeam, iIDPeriodo, "", iIDGame)
                ExtraRequi = GetDecisionPlayers("ExtraRequi", iIDTeam, iIDPeriodo, "var_" & GetTeamName(iIDTeam) & "_ExtraRequi_" & oRowItm("VariableName"), iIDGame)
                ThresDiscoExtra = GetVariableBoss("ThresDiscoExtra", iIDTeam, iIDPeriodo, "ThresDiscoExtra_" & Nz(oRowItm("VariableName")), iIDGame)
                ExtraProduCost = GetVariableBoss("ExtraProduCost", iIDTeam, iIDPeriodo, "ExtraProduCost_" & Nz(oRowItm("VariableName")), iIDGame)
                DiscoLevelExtra = GetVariableBoss("DiscoLevelExtra", iIDTeam, iIDPeriodo, "", iIDGame)

                ProduCashFinan += ProduBasicRequi * ProduUnitCost * ((iLunghezzaPeriodo - DelayPaymeProdu) / iLunghezzaPeriodo) _
                                * ((100 + DelayPaymeProdu * RicarTermiMese * DiritRitarPagam) / 100)

                PurchNicFinan += (PurchNICRequi * RawNICCost) * (2 / iLunghezzaPeriodo)

                PurchEuropFinan += (PurchEuropRequi * RawEuropCost) * ((2 - DelayPaymeEurop) / iLunghezzaPeriodo) * ((100 + DelayPaymeEurop * RicarTermiMese * DiritRitarPagam) / 100)

                PromoToFinan += MarkeExpenRequi * ((iLunghezzaPeriodo - DelayPaymePromo) / iLunghezzaPeriodo) *
                                ((100 + DelayPaymePromo * RicarTermiMese * DiritRitarPagam) / 100)

                ExtraProduFinan += ((100 - ForwaPaymeExtra * DiscoTermiMese * DiritScontPagam) / 100) *
                                ((iLunghezzaPeriodo - LagPaymeExtra + ForwaPaymeExtra) / iLunghezzaPeriodo) *
                                (Math.Min(ExtraRequi, ThresDiscoExtra) * ExtraProduCost +
                                (Math.Max(0, ExtraRequi - ThresDiscoExtra) * ExtraProduCost * (100 - DiscoLevelExtra) / 100))

            Next

            Message.Visible = True
            CA2 = ProduCashFinan + LeaseCashFinan + PerifCashFinan + PurchNicFinan + PurchEuropFinan + CentrStoreFinan _
                + VehicCashFinan + PersoStaffFinan + PersoPointFinan + PromoToFinan + Advertising + AutomFinan + ExtraProduFinan


            If (CA1 >= CA2 Or CA2 = 0) Then
                CA3 = 100
            Else
                CA3 = Math.Max(0, Math.Min(CA1 / CA2 * 100, 100))
            End If
            Session("CA3") = (Math.Floor(CA3 * 10) / 10).ToString("n1") + "%"

            ' Inizio la costruzione della tabella
            Dim tblHeaderRow As TableHeaderRow
            Dim tblHeaderCell As New TableHeaderCell

            Dim tblRow As TableRow
            Dim tblCell As TableCell

            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header vuota
            Dim oLBLTitleGrid As New RadLabel
            oLBLTitleGrid.ID = "lblGridTitle"
            oLBLTitleGrid.Text = "Cash available"
            oLBLTitleGrid.ForeColor = Drawing.Color.White
            tblHeaderCell.Controls.Add(oLBLTitleGrid)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.ColumnSpan = 2
            tblHeaderCell.Style.Add("background", "#525252")

            tblHeaderRow.Cells.Add(tblHeaderCell)

            tblCash.Rows.Add(tblHeaderRow)

            ' Aggiungo le righe di dettaglio
            ' Prima riga
            tblRow = New TableRow
            tblCell = New TableCell

            Dim oLblCashFlowFrom As New RadLabel
            oLblCashFlowFrom.ID = "lblCashFlowFrom"
            oLblCashFlowFrom.Text = "Cash flow from B.S."
            oLblCashFlowFrom.ForeColor = Drawing.Color.DarkRed
            tblCell.Controls.Add(oLblCashFlowFrom)
            tblCell.Style.Add("width", "70%")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLblCashFlowFromValue As New RadLabel
            oLblCashFlowFromValue.ID = "lblCashFlowFromValue"
            oLblCashFlowFromValue.Text = BankLevel.ToString("N0")
            tblCell.Controls.Add(oLblCashFlowFromValue)
            tblCell.Style.Add("width", "30%")
            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCash.Rows.Add(tblRow)

            ' Seconda riga
            tblRow = New TableRow
            tblCell = New TableCell

            Dim oLblNewLoans As New RadLabel
            oLblNewLoans.ID = "lblNewLoans"
            oLblNewLoans.Text = "New loans (variation of the period)"
            oLblNewLoans.ForeColor = Drawing.Color.DarkRed
            tblCell.Controls.Add(oLblNewLoans)
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLblNewLoansValue As New RadLabel
            oLblNewLoansValue.ID = "lblNewLoansValue"
            oLblNewLoansValue.Text = LoansAllow.ToString("N0")
            tblCell.Controls.Add(oLblNewLoansValue)
            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCash.Rows.Add(tblRow)

            ' Terza riga
            tblRow = New TableRow
            tblCell = New TableCell

            Dim oLblExtraLoans As New RadLabel
            oLblExtraLoans.ID = "oLblProductionCapacityExistOwned"
            oLblExtraLoans.Text = "Extra Loans - SuperFido "
            oLblExtraLoans.ForeColor = Drawing.Color.DarkRed
            tblCell.Controls.Add(oLblExtraLoans)
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLblExtraLoansValue As New RadLabel
            oLblExtraLoansValue.ID = "lblExtraLoansValue"
            oLblExtraLoansValue.Text = (SuperExtraFido).ToString("n0")
            tblCell.Controls.Add(oLblExtraLoansValue)
            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCash.Rows.Add(tblRow)

            ' Quarta riga
            tblRow = New TableRow
            tblCell = New TableCell

            Dim oLblDivestment As New RadLabel
            oLblDivestment.ID = "LblDivestment"
            oLblDivestment.Text = "Disinvestment"
            oLblDivestment.ForeColor = Drawing.Color.DarkRed
            tblCell.Controls.Add(oLblDivestment)
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLblDivestmentValue As New RadLabel
            oLblDivestmentValue.ID = "lblDivestment"
            oLblDivestmentValue.Text = DisinAllowPerio.ToString("N0")
            tblCell.Controls.Add(oLblDivestmentValue)
            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCash.Rows.Add(tblRow)

            ' Quinta riga
            tblRow = New TableRow
            tblCell = New TableCell

            Dim oLblPrepayments As New RadLabel
            oLblPrepayments.ID = "oLblPrepayments"
            oLblPrepayments.Text = "Committments (pagamenti anticipati)"
            oLblPrepayments.ForeColor = Drawing.Color.DarkRed
            tblCell.Controls.Add(oLblPrepayments)
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLblPrepaymentsValue As New RadLabel
            oLblPrepaymentsValue.ID = "lblPrepaymentsValue"
            oLblPrepaymentsValue.Text = PaymeToCover.ToString("N0")
            tblCell.Controls.Add(oLblPrepaymentsValue)
            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCash.Rows.Add(tblRow)

            ' Sesta riga
            tblRow = New TableRow
            tblCell = New TableCell

            Dim oLblTaxPayment As New RadLabel
            oLblTaxPayment.ID = "oLblTaxPayment "
            oLblTaxPayment.Text = "Tax payment"
            oLblTaxPayment.ForeColor = Drawing.Color.DarkRed
            tblCell.Controls.Add(oLblTaxPayment)
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLblTaxPaymentValue As New RadLabel
            oLblTaxPaymentValue.ID = "lblTaxPaymentValue"
            oLblTaxPaymentValue.Text = TaxFunds.ToString("N0")
            tblCell.Controls.Add(oLblTaxPaymentValue)
            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCash.Rows.Add(tblRow)

            ' Settima riga
            tblRow = New TableRow
            tblCell = New TableCell

            Dim oLBlCashFlow As New RadLabel
            oLBlCashFlow.ID = "oLblProductionPercentage"
            oLBlCashFlow.Text = "Cash flow available for the period "
            oLBlCashFlow.ForeColor = Drawing.Color.Black
            tblCell.Controls.Add(oLBlCashFlow)
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLblCashFlowValue As New RadLabel
            oLblCashFlowValue.ID = "lblCashFlowValue"
            oLblCashFlowValue.Text = CA1.ToString("N0")
            tblCell.Controls.Add(oLblCashFlowValue)
            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCash.Rows.Add(tblRow)

            ' INSERISCO UNA RIGA VUOTA
            tblRow = New TableRow
            tblCell = New TableCell

            Dim oLblEmpty As New RadLabel
            oLblEmpty.ID = ""
            oLblEmpty.Text = ""
            oLblEmpty.ForeColor = Drawing.Color.Black
            tblCell.Controls.Add(oLblEmpty)
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("min-height", "15px")
            tblRow.Cells.Add(tblCell)

            tblCash.Rows.Add(tblRow)

            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header vuota
            Dim oLBLSubTitleGrid As New RadLabel
            oLBLSubTitleGrid.ID = "lblGridSubTitle"
            oLBLSubTitleGrid.Text = "Cash needed"
            oLBLSubTitleGrid.ForeColor = Drawing.Color.White
            tblHeaderCell.Controls.Add(oLBLSubTitleGrid)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.ColumnSpan = 2
            tblHeaderCell.Style.Add("background", "#525252")

            tblHeaderRow.Cells.Add(tblHeaderCell)

            tblCash.Rows.Add(tblHeaderRow)

            ' Local raw material
            tblRow = New TableRow
            tblCell = New TableCell

            Dim oLblLocalRawMaterial As New RadLabel
            oLblLocalRawMaterial.ID = "lblLocalRawMaterial"
            oLblLocalRawMaterial.Text = "Local Raw Materials (Payment can be delayed from the 1 month std. to " & Nni(Session("LunghezzaPeriodo")) & " months max) "
            oLblLocalRawMaterial.ForeColor = Drawing.Color.DarkRed
            tblCell.Controls.Add(oLblLocalRawMaterial)
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLblLocalRawMaterialValue As New RadLabel
            oLblLocalRawMaterialValue.ID = "lblLocalRawMaterialValue"
            oLblLocalRawMaterialValue.Text = PurchEuropFinan.ToString("N0")
            tblCell.Controls.Add(oLblLocalRawMaterialValue)
            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCash.Rows.Add(tblRow)

            ' Imported raw material
            tblRow = New TableRow
            tblCell = New TableCell

            Dim oLblImportedRawMaterial As New RadLabel
            oLblImportedRawMaterial.ID = "lblImportedRawMaterial"
            oLblImportedRawMaterial.Text = "Imported Raw Materials (to be paid at sight at delivery)  "
            oLblImportedRawMaterial.ForeColor = Drawing.Color.DarkRed
            tblCell.Controls.Add(oLblImportedRawMaterial)
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLblImportedRawMaterialValue As New RadLabel
            oLblImportedRawMaterialValue.ID = "lblImportedRawMaterialValue"
            oLblImportedRawMaterialValue.Text = PurchNicFinan.ToString("N0")
            tblCell.Controls.Add(oLblImportedRawMaterialValue)
            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCash.Rows.Add(tblRow)

            ' Production
            tblRow = New TableRow
            tblCell = New TableCell

            Dim oLblProduction As New RadLabel
            oLblProduction.ID = "LblProduction"
            oLblProduction.Text = "Production(Payment can be delayed from std. cash to " & Nni(Session("LunghezzaPeriodo")) & " months max) "
            oLblProduction.ForeColor = Drawing.Color.DarkRed
            tblCell.Controls.Add(oLblProduction)
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLblProductionValue As New RadLabel
            oLblProductionValue.ID = "lbloLblProductionValue"
            oLblProductionValue.Text = ProduCashFinan.ToString("N0")
            tblCell.Controls.Add(oLblProductionValue)
            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCash.Rows.Add(tblRow)

            ' New lease machines
            tblRow = New TableRow
            tblCell = New TableCell

            Dim oLblNewLeaseMachine As New RadLabel
            oLblNewLeaseMachine.ID = "lblNewLeaseMachine"
            oLblNewLeaseMachine.Text = "New leased machines (the first payment only can be delayed from std. cash to " & Nni(Session("LunghezzaPeriodo")) & " months max)"
            oLblNewLeaseMachine.ForeColor = Drawing.Color.DarkRed
            tblCell.Controls.Add(oLblNewLeaseMachine)
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLblNewLeaseMachineValue As New RadLabel
            oLblNewLeaseMachineValue.ID = "lblNewLeaseMachineValue"
            oLblNewLeaseMachineValue.Text = LeaseCashFinan.ToString("N0")
            tblCell.Controls.Add(oLblNewLeaseMachineValue)
            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCash.Rows.Add(tblRow)

            ' Vehicles
            tblRow = New TableRow
            tblCell = New TableCell

            Dim oLblVehicles As New RadLabel
            oLblVehicles.ID = "lblVehicles"
            oLblVehicles.Text = "Vehicles (cash)"
            oLblVehicles.ForeColor = Drawing.Color.DarkRed
            tblCell.Controls.Add(oLblVehicles)
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLblVehiclesValue As New RadLabel
            oLblVehiclesValue.ID = "lblVehiclesValue"
            oLblVehiclesValue.Text = VehicCashFinan.ToString("N0")
            tblCell.Controls.Add(oLblVehiclesValue)
            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCash.Rows.Add(tblRow)

            ' New central outlets
            tblRow = New TableRow
            tblCell = New TableCell

            Dim oLblNewCentralOUtlets As New RadLabel
            oLblNewCentralOUtlets.ID = "lblNewCentralOUtlets"
            oLblNewCentralOUtlets.Text = "New central outlets (cash)"
            oLblNewCentralOUtlets.ForeColor = Drawing.Color.DarkRed
            tblCell.Controls.Add(oLblNewCentralOUtlets)
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLblNewCentralOUtletsValue As New RadLabel
            oLblNewCentralOUtletsValue.ID = "lblNewCentralOUtletsValue"
            oLblNewCentralOUtletsValue.Text = CentrStoreFinan.ToString("N0")
            tblCell.Controls.Add(oLblNewCentralOUtletsValue)
            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCash.Rows.Add(tblRow)

            ' New perif outlets
            tblRow = New TableRow
            tblCell = New TableCell

            Dim oLblNewPerifOUtlets As New RadLabel
            oLblNewPerifOUtlets.ID = "lblNewPerifOUtlets"
            oLblNewPerifOUtlets.Text = "New out-of-town outlets (cash) "
            oLblNewPerifOUtlets.ForeColor = Drawing.Color.DarkRed
            tblCell.Controls.Add(oLblNewPerifOUtlets)
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLblNewPerifOUtletsValue As New RadLabel
            oLblNewPerifOUtletsValue.ID = "lblNewPerifOUtletsValue"
            oLblNewPerifOUtletsValue.Text = PerifCashFinan.ToString("N0")
            tblCell.Controls.Add(oLblNewPerifOUtletsValue)
            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCash.Rows.Add(tblRow)

            ' New staff cash
            tblRow = New TableRow
            tblCell = New TableCell

            Dim oLblNewCashStaff As New RadLabel
            oLblNewCashStaff.ID = "lblNewCashStaff"
            oLblNewCashStaff.Text = "New staff (cash)"
            oLblNewCashStaff.ForeColor = Drawing.Color.DarkRed
            tblCell.Controls.Add(oLblNewCashStaff)
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLblNewCashStaffValue As New RadLabel
            oLblNewCashStaffValue.ID = "lblNewCashStaffValue"
            oLblNewCashStaffValue.Text = PersoStaffFinan.ToString("N0")
            tblCell.Controls.Add(oLblNewCashStaffValue)
            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCash.Rows.Add(tblRow)

            ' New peson cash
            tblRow = New TableRow
            tblCell = New TableCell

            Dim oLblNewCashPerson As New RadLabel
            oLblNewCashPerson.ID = "lblNewCashPerson"
            oLblNewCashPerson.Text = "New sales persons (cash) "
            oLblNewCashPerson.ForeColor = Drawing.Color.DarkRed
            tblCell.Controls.Add(oLblNewCashPerson)
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLblNewCashPersonValue As New RadLabel
            oLblNewCashPersonValue.ID = "lblNewCashPersonValue"
            oLblNewCashPersonValue.Text = PersoPointFinan.ToString("N0")
            tblCell.Controls.Add(oLblNewCashPersonValue)
            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCash.Rows.Add(tblRow)

            ' Promotions
            tblRow = New TableRow
            tblCell = New TableCell

            Dim oLblPromotions As New RadLabel
            oLblPromotions.ID = "lblPromotions"
            oLblPromotions.Text = "Promotions (Payment can be delayed from std. cash to " & Nni(Session("LunghezzaPeriodo")) & " months max) "
            oLblPromotions.ForeColor = Drawing.Color.DarkRed
            tblCell.Controls.Add(oLblPromotions)
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLblPromotionsValue As New RadLabel
            oLblPromotionsValue.ID = "lblPromotionsValue"
            oLblPromotionsValue.Text = PromoToFinan.ToString("N0")
            tblCell.Controls.Add(oLblPromotionsValue)
            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCash.Rows.Add(tblRow)

            ' Promotions
            tblRow = New TableRow
            tblCell = New TableCell

            Dim oLblAutomationTechnology As New RadLabel
            oLblAutomationTechnology.ID = "lblAutomationTechnology"
            oLblAutomationTechnology.Text = "Automation technology (Advanced payment from std " & Nni(Session("LunghezzaPeriodo")) & " months to cash) "
            oLblAutomationTechnology.ForeColor = Drawing.Color.DarkRed
            tblCell.Controls.Add(oLblAutomationTechnology)
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLblAutomationTechnologyValue As New RadLabel
            oLblAutomationTechnologyValue.ID = "lblAutomationTechnologyValue"
            oLblAutomationTechnologyValue.Text = AutomFinan.ToString("N0")
            tblCell.Controls.Add(oLblAutomationTechnologyValue)
            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCash.Rows.Add(tblRow)

            ' Bought-in products
            tblRow = New TableRow
            tblCell = New TableCell

            Dim oLblExtraDecision As New RadLabel
            oLblExtraDecision.ID = "lblExtraProduFinan"
            oLblExtraDecision.Text = "Bought-in products (Advanced payment from std " & Nni(Session("LunghezzaPeriodo")) & " months to cash) "
            oLblExtraDecision.ForeColor = Drawing.Color.DarkRed
            tblCell.Controls.Add(oLblExtraDecision)
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLblExtraProduFinanValue As New RadLabel
            oLblExtraProduFinanValue.ID = "lblExtraProduFinanValue"
            oLblExtraProduFinanValue.Text = ExtraProduFinan.ToString("N0")
            tblCell.Controls.Add(oLblExtraProduFinanValue)
            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCash.Rows.Add(tblRow)

            ' Advertising
            tblRow = New TableRow
            tblCell = New TableCell

            Dim oLblAdevertising As New RadLabel
            oLblAdevertising.ID = "lblAdevertising"
            oLblAdevertising.Text = "Advertising and Customer service (Advanced payment from std " & Nni(Session("LunghezzaPeriodo")) & " months to cash) "
            oLblAdevertising.ForeColor = Drawing.Color.DarkRed
            tblCell.Controls.Add(oLblAdevertising)
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLblAdvertiosingValue As New RadLabel
            oLblAdvertiosingValue.ID = "lblAdevertisingValue"
            oLblAdvertiosingValue.Text = Advertising.ToString("N0")
            tblCell.Controls.Add(oLblAdvertiosingValue)
            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCash.Rows.Add(tblRow)

            ' Short term cash needed
            tblRow = New TableRow
            tblCell = New TableCell

            Dim oLblShortTermCashNeeded As New RadLabel
            oLblShortTermCashNeeded.ID = "lblShortTermCashNeeded"
            oLblShortTermCashNeeded.Text = "Cash flow available for the period "
            oLblShortTermCashNeeded.ForeColor = Drawing.Color.Black
            tblCell.Controls.Add(oLblShortTermCashNeeded)
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLblShortTermCashNeededValue As New RadLabel
            oLblShortTermCashNeededValue.ID = "lblShortTermCashNeededValue"
            oLblShortTermCashNeededValue.Text = CA2.ToString("N0")
            tblCell.Controls.Add(oLblShortTermCashNeededValue)
            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCash.Rows.Add(tblRow)

            ' Percentage of allowed decisions
            tblRow = New TableRow
            tblCell = New TableCell

            Dim oLblPercentage As New RadLabel
            oLblPercentage.ID = "lblPercentage"
            oLblPercentage.Text = "Percentage of allowed decisions " & "<br/><br/>" _
                                & "In case the allowed decisions level does not reach the 100% all the investments, purchases, " _
                                & "expenditures will be automatically reduced by the percentage shown unless you apply " _
                                & "for an extra loan in the financial decisions area. The extra loans is an exceptional opportunity that you may access to by an higher interest rate."
            oLblPercentage.ForeColor = Drawing.Color.Black
            tblCell.Controls.Add(oLblPercentage)
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLblPercentageValue As New RadLabel
            oLblPercentageValue.ID = "lblPercentageValue"
            oLblPercentageValue.Text = CA3.ToString("N0") & " %"
            tblCell.Controls.Add(oLblPercentageValue)
            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCash.Rows.Add(tblRow)
        Catch ex As Exception
            MessageText.Text = "LoadDataCash -> " & ex.Message
            Message.Visible = True
        End Try

    End Sub

    Private Sub btnTranslate_Click(sender As Object, e As EventArgs) Handles btnTranslate.Click
        Session("PageOrigin") = Me.ToString().Substring(4, Me.ToString().Substring(4).Length - 5) + ".aspx"
        Response.Redirect("PageTranslation.aspx")
        Response.End()
    End Sub

End Class
