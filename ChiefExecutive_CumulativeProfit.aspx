﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="ChiefExecutive_CumulativeProfit.aspx.vb" Inherits="ChiefExecutive_CumulativeProfit" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="cntMain" ContentPlaceHolderID="MainContent" runat="Server" ClientIDMode="Static">

    <asp:UpdatePanel ID="pnlMain" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <h2>
                <asp:Label ID="lblWelcome" runat="server" Text="Chief executive"></asp:Label>
            </h2>

            <div class="clr"></div>

            <h3>
                <asp:Label ID="lblTitolo" runat="server" Text="Cumulative profit"></asp:Label>
            </h3>

            <div class="row">

                <telerik:RadHtmlChart ID="grfCumulativeProfit" runat="server" Width="100%">
                    <ChartTitle Text=""></ChartTitle>

                    <PlotArea>
                        <Series>

                            <telerik:ColumnSeries Name="Cumulative profit" DataFieldY="UtilsEndPerio">
                                <TooltipsAppearance DataFormatString="{0:2P}" BackgroundColor="White" Color="#3e6c86" />
                                <LabelsAppearance Visible="true" Position="InsideEnd" DataField="Period" RotationAngle="-90" Color="#FCE198" />
                                <Appearance FillStyle-BackgroundColor="#25536F" Overlay-Gradient="RoundedBevel" />
                            </telerik:ColumnSeries>

                        </Series>

                        <XAxis>
                        </XAxis>

                        <YAxis>
                            <LabelsAppearance DataFormatString="{0}" />
                        </YAxis>

                    </PlotArea>

                    <Legend>
                        <Appearance Visible="true" Position="Top" />
                    </Legend>
                </telerik:RadHtmlChart>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel ID="pnlMessage" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row">
                <telerik:RadLabel runat="server" ID="lblMessage" Visible="false"></telerik:RadLabel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="clr"></div>

    <div class="row">
        <div class="divTableCell" style="text-align: right;">
            <telerik:RadButton ID="btnTranslate" runat="server" Text="Translate" EnableAjaxSkinRendering="true" ToolTip=""></telerik:RadButton>
        </div>
    </div>

</asp:Content>
