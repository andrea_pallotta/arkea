﻿Imports System.Data
Imports APS.APSUtility
Imports APPCore.Utility
Imports DALC4NET
Imports Telerik.Web.UI

Partial Class ChiefExecutive_Dashboard
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadDataDashboard()
        Message.Visible = False

        UpdatePanelMain.Update()
    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadDataDashboard()
        Message.Visible = False

        UpdatePanelMain.Update()
    End Sub

    Private Sub LoadDataDashboard()
        Dim oDTItems As DataTable = HandleGetItemsPeriodVariables(Session("IDGame"), Nz(Session("LanguageActive")))

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Try
            tblDashboard.Controls.Clear()
            tblDashboard.Rows.Clear()

            ' Controllo la presenza delle variabili nella tabella delle variabili del gioco
            VerifiyVariableExists("IndexROE", Session("IDGame"), 0, "", 0, 0, False, False, False, 0)
            VerifiyVariableExists("IndexROCE", Session("IDGame"), 0, "", 0, 0, False, False, False, 0)
            VerifiyVariableExists("OperatingProfit", Session("IDGame"), 0, "", 0, 0, False, False, False, 0)
            VerifiyVariableExists("EBITDAperce", Session("IDGame"), 0, "", 0, 0, False, False, False, 0)
            VerifiyVariableExists("ReturOnSales", Session("IDGame"), 0, "", 0, 0, False, False, False, 0)
            VerifiyVariableExists("CurrentRatio", Session("IDGame"), 0, "", 0, 0, False, False, False, 0)
            VerifiyVariableExists("InterestCover", Session("IDGame"), 0, "", 0, 0, False, False, False, 0)
            VerifiyVariableExists("DebtX", Session("IDGame"), 0, "", 0, 0, False, False, False, 0)
            VerifiyVariableExists("RevenPerShop", Session("IDGame"), 0, "", 0, 0, False, False, False, 0)
            VerifiyVariableExists("RevenPerPerso", Session("IDGame"), 0, "", 0, 0, False, False, False, 0)
            VerifiyVariableExists("SaturNegoz", Session("IDGame"), 0, "", 0, 0, False, False, False, 0)
            VerifiyVariableExists("WholesaleServed", Session("IDGame"), 0, "", 0, 0, False, False, False, 0)
            VerifiyVariableExists("StockRotation", Session("IDGame"), 0, "", 0, 0, False, False, False, 0)
            VerifiyVariableExists("TotalCapacProdu", Session("IDGame"), 0, "", 0, 0, False, False, False, 0)
            VerifiyVariableExists("PerceProduUsed", Session("IDGame"), 0, "", 0, 0, False, False, False, 0)
            VerifiyVariableExists("PlantTousePerio", Session("IDGame"), 0, "", 0, 0, False, False, False, 0)
            VerifiyVariableExists("ProduFixedCosts", Session("IDGame"), 0, "", 0, 0, False, False, False, 0)
            VerifiyVariableExists("SinkiFundPlant", Session("IDGame"), 0, "", 0, 0, False, False, False, 0)
            VerifiyVariableExists("LeasiTotalCost", Session("IDGame"), 0, "", 0, 0, False, False, False, 0)
            VerifiyVariableExists("ClientServed", Session("IDGame"), 0, "", 0, 0, False, False, False, 0)
            VerifiyVariableExists("MarketShare", Session("IDGame"), 0, "", 0, 0, False, False, False, 0)
            VerifiyVariableExists("MarketPriceShare", Session("IDGame"), 0, "", 0, 0, False, False, False, 0)
            VerifiyVariableExists("PerceValueItem", Session("IDGame"), 0, "", 0, 0, False, False, False, 0)
            VerifiyVariableExists("PerceQuality", Session("IDGame"), 0, "", 0, 0, False, False, False, 0)
            VerifiyVariableExists("StaffTrainLevel", Session("IDGame"), 0, "", 0, 0, False, False, False, 0)
            VerifiyVariableExists("PointTrainLevel", Session("IDGame"), 0, "", 0, 0, False, False, False, 0)

            ' FINANCE
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblFinance As New RadLabel
            lblFinance.ID = "lblFinance"
            lblFinance.Text = "FINANCE"
            lblFinance.Style.Add("font-size", "8pt")
            lblFinance.Style.Add("text-align", "left")
            lblFinance.Style.Add("font-weight", "bold")
            lblFinance.Style.Add("color", "#ffffff")
            tblCell.Controls.Add(lblFinance)
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("padding-left", "20px")
            tblCell.Style.Add("background", "#3e6c86")
            tblCell.ColumnSpan = 2
            tblRow.Cells.Add(tblCell)

            tblDashboard.Rows.Add(tblRow)

            ' Profitability ratios (Current year)
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblProfitability As New RadLabel
            lblProfitability.ID = "lblProfitability"
            lblProfitability.Text = "Profitability ratios (Current year)"
            lblProfitability.Style.Add("font-size", "8pt")
            lblProfitability.Style.Add("text-align", "left")
            lblProfitability.Style.Add("font-weight", "bold")
            tblCell.Controls.Add(lblProfitability)
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("padding-left", "5px")
            tblCell.Style.Add("background", "#e7e7e7")
            tblCell.ColumnSpan = 2
            tblRow.Cells.Add(tblCell)

            tblDashboard.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblROE As New RadLabel
            lblROE.ID = "lblROE"
            lblROE.Text = "ROE"
            lblROE.Style.Add("font-size", "8pt")
            lblROE.Style.Add("text-align", "left")
            lblROE.Style.Add("font-weight", "normal")
            tblCell.Controls.Add(lblROE)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("padding-left", "5px")
            tblCell.Style.Add("width", "75%")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLinkRawMaterialsBoughtValue As New HyperLink
            oLinkRawMaterialsBoughtValue.Text = Nn(GetVariableDefineValue("IndexROE", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
            oLinkRawMaterialsBoughtValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=IndexROE&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            oLinkRawMaterialsBoughtValue.Style.Add("text-decoration", "none")
            oLinkRawMaterialsBoughtValue.Style.Add("cursor", "pointer")
            oLinkRawMaterialsBoughtValue.Style.Add("font-size", "8pt")
            oLinkRawMaterialsBoughtValue.Style.Add("text-align", "right")
            oLinkRawMaterialsBoughtValue.BackColor = Drawing.Color.Transparent

            tblCell.Controls.Add(oLinkRawMaterialsBoughtValue)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblDashboard.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' ROCE
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblROCE As New RadLabel
            lblROCE.ID = "lblROCE"
            lblROCE.Text = "ROCE"
            lblROCE.Style.Add("font-size", "8pt")
            lblROCE.Style.Add("text-align", "left")
            lblROCE.Style.Add("font-weight", "normal")
            tblCell.Controls.Add(lblROCE)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("padding-left", "5px")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLinkROCEValue As New HyperLink
            oLinkROCEValue.Text = Nn(GetVariableDefineValue("IndexROCE", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
            oLinkROCEValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=IndexROCE&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            oLinkROCEValue.Style.Add("text-decoration", "none")
            oLinkROCEValue.Style.Add("cursor", "pointer")
            oLinkROCEValue.Style.Add("font-size", "8pt")
            oLinkROCEValue.Style.Add("text-align", "right")
            oLinkROCEValue.BackColor = Drawing.Color.Transparent

            tblCell.Controls.Add(oLinkROCEValue)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblDashboard.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' OPERATING PROFIT
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblOperatingProfit As New RadLabel
            lblOperatingProfit.ID = "lblOperatingProfit"
            lblOperatingProfit.Text = "Operating profit"
            lblOperatingProfit.Style.Add("font-size", "8pt")
            lblOperatingProfit.Style.Add("text-align", "left")
            lblOperatingProfit.Style.Add("font-weight", "normal")
            tblCell.Controls.Add(lblOperatingProfit)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("padding-left", "5px")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLinkOperatingProfitValue As New HyperLink
            oLinkOperatingProfitValue.Text = Nn(GetVariableDefineValue("OperatingProfit", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
            oLinkOperatingProfitValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=OperatingProfit&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            oLinkOperatingProfitValue.Style.Add("text-decoration", "none")
            oLinkOperatingProfitValue.Style.Add("cursor", "pointer")
            oLinkOperatingProfitValue.Style.Add("font-size", "8pt")
            oLinkOperatingProfitValue.Style.Add("text-align", "right")
            oLinkOperatingProfitValue.BackColor = Drawing.Color.Transparent

            tblCell.Controls.Add(oLinkOperatingProfitValue)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblDashboard.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' EBITDA 
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblEBITDAperce As New RadLabel
            lblEBITDAperce.ID = "lblEBITDAperce"
            lblEBITDAperce.Text = "EBITDAperce"
            lblEBITDAperce.Style.Add("font-size", "8pt")
            lblEBITDAperce.Style.Add("text-align", "left")
            lblEBITDAperce.Style.Add("font-weight", "normal")
            tblCell.Controls.Add(lblEBITDAperce)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("padding-left", "5px")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLinkEBITDAperceValue As New HyperLink
            oLinkEBITDAperceValue.Text = Nn(GetVariableDefineValue("EBITDAperce", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
            oLinkEBITDAperceValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=EBITDAperce&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            oLinkEBITDAperceValue.Style.Add("text-decoration", "none")
            oLinkEBITDAperceValue.Style.Add("cursor", "pointer")
            oLinkEBITDAperceValue.Style.Add("font-size", "8pt")
            oLinkEBITDAperceValue.Style.Add("text-align", "right")
            oLinkEBITDAperceValue.BackColor = Drawing.Color.Transparent

            tblCell.Controls.Add(oLinkEBITDAperceValue)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblDashboard.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' ROS 
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblROS As New RadLabel
            lblROS.ID = "lblROS"
            lblROS.Text = "Return on sales for the period"
            lblROS.Style.Add("font-size", "8pt")
            lblROS.Style.Add("text-align", "left")
            lblROS.Style.Add("font-weight", "normal")
            tblCell.Controls.Add(lblROS)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("padding-left", "5px")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLinkROSValue As New HyperLink
            oLinkROSValue.Text = Nn(GetVariableDefineValue("ReturOnSales", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
            oLinkROSValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=ReturOnSales&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            oLinkROSValue.Style.Add("text-decoration", "none")
            oLinkROSValue.Style.Add("cursor", "pointer")
            oLinkROSValue.Style.Add("font-size", "8pt")
            oLinkROSValue.Style.Add("text-align", "right")
            oLinkROSValue.BackColor = Drawing.Color.Transparent

            tblCell.Controls.Add(oLinkROSValue)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblDashboard.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' Liquidity ratios
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblLiquidityRatios As New RadLabel
            lblLiquidityRatios.ID = "lblLiquidityRatios"
            lblLiquidityRatios.Text = "Liquidity Ratios (current base)"
            lblLiquidityRatios.Style.Add("font-size", "8pt")
            lblLiquidityRatios.Style.Add("text-align", "left")
            lblLiquidityRatios.Style.Add("font-weight", "bold")
            tblCell.Controls.Add(lblLiquidityRatios)
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("padding-left", "5px")
            tblCell.Style.Add("background", "#e7e7e7")
            tblCell.ColumnSpan = 2
            tblRow.Cells.Add(tblCell)

            tblDashboard.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' CURRENT RATIO 
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLBLCurrentRatio As New RadLabel
            oLBLCurrentRatio.ID = "lblCurrentRatio"
            oLBLCurrentRatio.Text = "Current ratio"
            oLBLCurrentRatio.Style.Add("font-size", "8pt")
            oLBLCurrentRatio.Style.Add("text-align", "left")
            oLBLCurrentRatio.Style.Add("font-weight", "normal")
            tblCell.Controls.Add(oLBLCurrentRatio)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("padding-left", "5px")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLinkCurrentRatioValue As New HyperLink
            oLinkCurrentRatioValue.Text = Nn(GetVariableDefineValue("CurrentRatio", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
            oLinkCurrentRatioValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=CurrentRatio&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            oLinkCurrentRatioValue.Style.Add("text-decoration", "none")
            oLinkCurrentRatioValue.Style.Add("cursor", "pointer")
            oLinkCurrentRatioValue.Style.Add("font-size", "8pt")
            oLinkCurrentRatioValue.Style.Add("text-align", "right")
            oLinkCurrentRatioValue.BackColor = Drawing.Color.Transparent

            tblCell.Controls.Add(oLinkCurrentRatioValue)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblDashboard.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' ACID TEST RATIO
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblAcidTestRatio As New RadLabel
            lblAcidTestRatio.ID = "lblAcidTestRatio"
            lblAcidTestRatio.Text = "Acid test ratio"
            lblAcidTestRatio.Style.Add("font-size", "8pt")
            lblAcidTestRatio.Style.Add("text-align", "left")
            lblAcidTestRatio.Style.Add("font-weight", "normal")
            tblCell.Controls.Add(lblAcidTestRatio)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("padding-left", "5px")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLBLAcidTestRatioValue As New HyperLink
            oLBLAcidTestRatioValue.Text = Nn(GetVariableDefineValue("AcidTest", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
            oLBLAcidTestRatioValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=AcidTest&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            oLBLAcidTestRatioValue.Style.Add("text-decoration", "none")
            oLBLAcidTestRatioValue.Style.Add("cursor", "pointer")
            oLBLAcidTestRatioValue.Style.Add("font-size", "8pt")
            oLBLAcidTestRatioValue.Style.Add("text-align", "right")
            oLBLAcidTestRatioValue.BackColor = Drawing.Color.Transparent

            tblCell.Controls.Add(oLBLAcidTestRatioValue)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblDashboard.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' INTEREST COVER
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblInterestCover As New RadLabel
            lblInterestCover.ID = "lblInterestCover"
            lblInterestCover.Text = "Interest cover"
            lblInterestCover.Style.Add("font-size", "8pt")
            lblInterestCover.Style.Add("text-align", "left")
            lblInterestCover.Style.Add("font-weight", "normal")
            tblCell.Controls.Add(lblInterestCover)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("padding-left", "5px")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLinkInterestCoverValue As New HyperLink
            oLinkInterestCoverValue.Text = Nn(GetVariableDefineValue("InterestCover", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
            oLinkInterestCoverValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=InterestCover&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            oLinkInterestCoverValue.Style.Add("text-decoration", "none")
            oLinkInterestCoverValue.Style.Add("cursor", "pointer")
            oLinkInterestCoverValue.Style.Add("font-size", "8pt")
            oLinkInterestCoverValue.Style.Add("text-align", "right")
            oLinkInterestCoverValue.BackColor = Drawing.Color.Transparent

            tblCell.Controls.Add(oLinkInterestCoverValue)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblDashboard.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' DEBTS
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblDebtX As New RadLabel
            lblDebtX.ID = "lblDebtX"
            lblDebtX.Text = "Debts"
            lblDebtX.Style.Add("font-size", "8pt")
            lblDebtX.Style.Add("text-align", "left")
            lblDebtX.Style.Add("font-weight", "normal")
            tblCell.Controls.Add(lblDebtX)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("padding-left", "5px")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLinkDebtXValue As New HyperLink
            oLinkDebtXValue.Text = Nn(GetVariableDefineValue("DebtX", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
            oLinkDebtXValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=DebtX&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            oLinkDebtXValue.Style.Add("text-decoration", "none")
            oLinkDebtXValue.Style.Add("cursor", "pointer")
            oLinkDebtXValue.Style.Add("font-size", "8pt")
            oLinkDebtXValue.Style.Add("text-align", "right")
            oLinkDebtXValue.BackColor = Drawing.Color.Transparent

            tblCell.Controls.Add(oLinkDebtXValue)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblDashboard.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' PROCESS
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblProcess As New RadLabel
            lblProcess.ID = "lblProcess"
            lblProcess.Text = "PROCESS"
            lblProcess.Style.Add("font-size", "8pt")
            lblProcess.Style.Add("text-align", "left")
            lblProcess.Style.Add("font-weight", "bold")
            lblProcess.Style.Add("color", "#ffffff")
            tblCell.Controls.Add(lblProcess)
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("padding-left", "20px")
            tblCell.Style.Add("background", "#3e6c86")
            tblCell.ColumnSpan = 2
            tblRow.Cells.Add(tblCell)

            tblDashboard.Rows.Add(tblRow)

            ' Commercial performance
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLBLCommercialPerformance As New RadLabel
            oLBLCommercialPerformance.Text = "Commercial performance"
            oLBLCommercialPerformance.ID = "lblCommercialPerformance"
            oLBLCommercialPerformance.Style.Add("font-size", "8pt")
            oLBLCommercialPerformance.Style.Add("text-align", "left")
            oLBLCommercialPerformance.Style.Add("font-weight", "bold")
            tblCell.Controls.Add(oLBLCommercialPerformance)
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("padding-left", "5px")
            tblCell.Style.Add("background", "#e7e7e7")
            tblCell.ColumnSpan = 2
            tblRow.Cells.Add(tblCell)

            tblDashboard.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' REVENUES PER SHOP
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblRevenuesPerShop As New RadLabel
            lblRevenuesPerShop.ID = "lblRevenuesPerShop"
            lblRevenuesPerShop.Text = "Revenues per shop"
            lblRevenuesPerShop.Style.Add("font-size", "8pt")
            lblRevenuesPerShop.Style.Add("text-align", "left")
            lblRevenuesPerShop.Style.Add("font-weight", "normal")
            tblCell.Controls.Add(lblRevenuesPerShop)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("padding-left", "5px")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLinkRevenuesPerShopValue As New HyperLink
            oLinkRevenuesPerShopValue.Text = Nn(GetVariableDefineValue("RevenPerShop", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
            oLinkRevenuesPerShopValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=RevenPerShop&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            oLinkRevenuesPerShopValue.Style.Add("text-decoration", "none")
            oLinkRevenuesPerShopValue.Style.Add("cursor", "pointer")
            oLinkRevenuesPerShopValue.Style.Add("font-size", "8pt")
            oLinkRevenuesPerShopValue.Style.Add("text-align", "right")
            oLinkRevenuesPerShopValue.BackColor = Drawing.Color.Transparent

            tblCell.Controls.Add(oLinkRevenuesPerShopValue)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblDashboard.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' REVENUES PER PERSON
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblRevenuesPerPerson As New RadLabel
            lblRevenuesPerPerson.ID = "lblRevenuesPerPerson"
            lblRevenuesPerPerson.Text = "Revenues per person"
            lblRevenuesPerPerson.Style.Add("font-size", "8pt")
            lblRevenuesPerPerson.Style.Add("text-align", "left")
            lblRevenuesPerPerson.Style.Add("font-weight", "normal")
            tblCell.Controls.Add(lblRevenuesPerPerson)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("padding-left", "5px")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLinkRevenuesPerPersonValue As New HyperLink
            oLinkRevenuesPerPersonValue.Text = Nn(GetVariableDefineValue("RevenPerPerso", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
            oLinkRevenuesPerPersonValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=RevenPerPerso&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            oLinkRevenuesPerPersonValue.Style.Add("text-decoration", "none")
            oLinkRevenuesPerPersonValue.Style.Add("cursor", "pointer")
            oLinkRevenuesPerPersonValue.Style.Add("font-size", "8pt")
            oLinkRevenuesPerPersonValue.Style.Add("text-align", "right")
            oLinkRevenuesPerPersonValue.BackColor = Drawing.Color.Transparent

            tblCell.Controls.Add(oLinkRevenuesPerPersonValue)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblDashboard.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' SATURAZIONE NEGOZI
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblSaturNegoz As New RadLabel
            lblSaturNegoz.ID = "lblSaturNegoz"
            lblSaturNegoz.Text = "Use spaces in stores"
            lblSaturNegoz.Style.Add("font-size", "8pt")
            lblSaturNegoz.Style.Add("text-align", "left")
            lblSaturNegoz.Style.Add("font-weight", "normal")
            tblCell.Controls.Add(lblSaturNegoz)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("padding-left", "5px")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLinkSaturNegozValue As New HyperLink
            oLinkSaturNegozValue.Text = Nn(GetVariableDefineValue("SaturNegoz", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
            oLinkSaturNegozValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=SaturNegoz&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            oLinkSaturNegozValue.Style.Add("text-decoration", "none")
            oLinkSaturNegozValue.Style.Add("cursor", "pointer")
            oLinkSaturNegozValue.Style.Add("font-size", "8pt")
            oLinkSaturNegozValue.Style.Add("text-align", "right")
            oLinkSaturNegozValue.BackColor = Drawing.Color.Transparent

            tblCell.Controls.Add(oLinkSaturNegozValue)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblDashboard.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' WHOLESALESERVED
            For Each oRowItm As DataRow In oDTItems.Rows
                tblRow = New TableRow
                tblCell = New TableCell
                Dim lblWholesaleServed As New RadLabel
                lblWholesaleServed.ID = "lblWholesaleServed_" & Nz(oRowItm("VariableName"))
                lblWholesaleServed.Text = "Wholesalers - % accepted - " & Nz(oRowItm("VariableName"))
                lblWholesaleServed.Style.Add("font-size", "8pt")
                lblWholesaleServed.Style.Add("text-align", "left")
                lblWholesaleServed.Style.Add("font-weight", "normal")
                tblCell.Controls.Add(lblWholesaleServed)
                tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
                tblCell.Style.Add("padding-left", "5px")
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                Dim oLinkWholesaleServedValue As New HyperLink
                oLinkWholesaleServedValue.Text = Nn(GetVariableDefineValue("WholesaleServed", Session("IDTeam"), oRowItm("IDVariable"), 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
                oLinkWholesaleServedValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=WholesaleServed&II=" & oRowItm("IDVariable") & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                oLinkWholesaleServedValue.Style.Add("text-decoration", "none")
                oLinkWholesaleServedValue.Style.Add("cursor", "pointer")
                oLinkWholesaleServedValue.Style.Add("font-size", "8pt")
                oLinkWholesaleServedValue.Style.Add("text-align", "right")
                oLinkWholesaleServedValue.BackColor = Drawing.Color.Transparent

                tblCell.Controls.Add(oLinkWholesaleServedValue)
                tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
                tblCell.Style.Add("text-align", "right")
                tblRow.Cells.Add(tblCell)

                tblDashboard.Rows.Add(tblRow)
            Next
            ' ---------------------------------------------------------------------------------

            ' Operative performance
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblOperativePerformance As New RadLabel
            lblOperativePerformance.ID = "lblOperativePerformance"
            lblOperativePerformance.Text = "Operative performance"
            lblOperativePerformance.Style.Add("font-size", "8pt")
            lblOperativePerformance.Style.Add("text-align", "left")
            lblOperativePerformance.Style.Add("font-weight", "bold")
            tblCell.Controls.Add(lblOperativePerformance)
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("padding-left", "5px")
            tblCell.Style.Add("background", "#e7e7e7")
            tblCell.ColumnSpan = 2
            tblRow.Cells.Add(tblCell)

            tblDashboard.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' STOCK DAYS
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblStockDays As New RadLabel
            lblStockDays.ID = "lblStockDays"
            lblStockDays.Text = "Stock days"
            lblStockDays.Style.Add("font-size", "8pt")
            lblStockDays.Style.Add("text-align", "left")
            lblStockDays.Style.Add("font-weight", "normal")
            tblCell.Controls.Add(lblStockDays)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("padding-left", "5px")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLinkStockDaysValue As New HyperLink
            oLinkStockDaysValue.Text = Nn(GetVariableDefineValue("StockRotation", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
            oLinkStockDaysValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=StockRotation&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            oLinkStockDaysValue.Style.Add("text-decoration", "none")
            oLinkStockDaysValue.Style.Add("cursor", "pointer")
            oLinkStockDaysValue.Style.Add("font-size", "8pt")
            oLinkStockDaysValue.Style.Add("text-align", "right")
            oLinkStockDaysValue.BackColor = Drawing.Color.Transparent

            tblCell.Controls.Add(oLinkStockDaysValue)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblDashboard.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' PRODUCTION CAPACITY
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblProductionCapacity As New RadLabel
            lblProductionCapacity.ID = "lblProductionCapacity"
            lblProductionCapacity.Text = "Production capacity"
            lblProductionCapacity.Style.Add("font-size", "8pt")
            lblProductionCapacity.Style.Add("text-align", "left")
            lblProductionCapacity.Style.Add("font-weight", "normal")
            tblCell.Controls.Add(lblProductionCapacity)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("padding-left", "5px")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLinkProductionCapacityValue As New HyperLink
            oLinkProductionCapacityValue.Text = Nn(GetVariableDefineValue("TotalCapacProdu", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
            oLinkProductionCapacityValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=TotalCapacProdu&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            oLinkProductionCapacityValue.Style.Add("text-decoration", "none")
            oLinkProductionCapacityValue.Style.Add("cursor", "pointer")
            oLinkProductionCapacityValue.Style.Add("font-size", "8pt")
            oLinkProductionCapacityValue.Style.Add("text-align", "right")
            oLinkProductionCapacityValue.BackColor = Drawing.Color.Transparent

            tblCell.Controls.Add(oLinkProductionCapacityValue)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblDashboard.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' % of the production capacity used
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblProductionCapacityPerc As New RadLabel
            lblProductionCapacityPerc.ID = "lblProductionCapacityPerc"
            lblProductionCapacityPerc.Text = "% of the production capacity used"
            lblProductionCapacityPerc.Style.Add("font-size", "8pt")
            lblProductionCapacityPerc.Style.Add("text-align", "left")
            lblProductionCapacityPerc.Style.Add("font-weight", "normal")
            tblCell.Controls.Add(lblProductionCapacityPerc)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("padding-left", "5px")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLinkProductionCapacityPercValue As New HyperLink
            oLinkProductionCapacityPercValue.Text = Nn(GetVariableDefineValue("PerceProduUsed", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
            oLinkProductionCapacityPercValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=PerceProduUsed&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            oLinkProductionCapacityPercValue.Style.Add("text-decoration", "none")
            oLinkProductionCapacityPercValue.Style.Add("cursor", "pointer")
            oLinkProductionCapacityPercValue.Style.Add("font-size", "8pt")
            oLinkProductionCapacityPercValue.Style.Add("text-align", "right")
            oLinkProductionCapacityPercValue.BackColor = Drawing.Color.Transparent

            tblCell.Controls.Add(oLinkProductionCapacityPercValue)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblDashboard.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' Average hours per active machine
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblAverageMachine As New RadLabel
            lblAverageMachine.ID = "lblAverageMachine"
            lblAverageMachine.Text = "Average hours per active machine"
            lblAverageMachine.Style.Add("font-size", "8pt")
            lblAverageMachine.Style.Add("text-align", "left")
            lblAverageMachine.Style.Add("font-weight", "normal")
            tblCell.Controls.Add(lblAverageMachine)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("padding-left", "5px")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell

            Dim oLinkAverageMachineValue As New HyperLink

            Dim TotalCapacProdu As Double = Nn(GetVariableDefineValue("TotalCapacProdu", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame")))
            Dim PerceProduUsed As Double = Nn(GetVariableDefineValue("PerceProduUsed", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame")))
            Dim PlantTousePerio As Double = Nn(GetVariableDefineValue("PlantTousePerio", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame")))

            oLinkAverageMachineValue.Text = "0"
            If PerceProduUsed <> 0 AndAlso PlantTousePerio <> 0 Then
                oLinkAverageMachineValue.Text = ((TotalCapacProdu * PerceProduUsed / 100) / PlantTousePerio).ToString("N0")
            End If
            oLinkAverageMachineValue.Style.Add("text-decoration", "none")
            oLinkAverageMachineValue.Style.Add("cursor", "default")
            oLinkAverageMachineValue.Style.Add("font-size", "8pt")
            oLinkAverageMachineValue.Style.Add("text-align", "right")
            oLinkAverageMachineValue.BackColor = Drawing.Color.Transparent

            tblCell.Controls.Add(oLinkAverageMachineValue)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)


            tblDashboard.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' Hourly average cost per active machine
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblAverageCostMachine As New RadLabel
            lblAverageCostMachine.ID = "lblAverageCostMachine"
            lblAverageCostMachine.Text = "Hourly average cost per active machine"
            lblAverageCostMachine.Style.Add("font-size", "8pt")
            lblAverageCostMachine.Style.Add("text-align", "left")
            lblAverageCostMachine.Style.Add("font-weight", "normal")
            tblCell.Controls.Add(lblAverageCostMachine)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("padding-left", "5px")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell

            Dim oLinkAverageCostMachineValue As New HyperLink

            Dim ProduFixedCosts As Double = Nn(GetVariableDefineValue("ProduFixedCosts", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame")))
            Dim SinkiFundPlant As Double = Nn(GetVariableDefineValue("SinkiFundPlant", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame")))
            Dim LeasiTotalCost As Double = Nn(GetVariableDefineValue("LeasiTotalCost", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame")))
            TotalCapacProdu = Nn(GetVariableDefineValue("TotalCapacProdu", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame")))
            PerceProduUsed = Nn(GetVariableDefineValue("PerceProduUsed", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame")))

            oLinkAverageCostMachineValue.Text = "0"
            If (TotalCapacProdu + PerceProduUsed) <> 0 Then
                oLinkAverageCostMachineValue.Text = ((ProduFixedCosts + SinkiFundPlant + LeasiTotalCost) / (TotalCapacProdu + PerceProduUsed)).ToString("N2")
            End If
            oLinkAverageCostMachineValue.Style.Add("text-decoration", "none")
            oLinkAverageCostMachineValue.Style.Add("cursor", "default")
            oLinkAverageCostMachineValue.Style.Add("font-size", "8pt")
            oLinkAverageCostMachineValue.Style.Add("text-align", "right")
            oLinkAverageCostMachineValue.BackColor = Drawing.Color.Transparent

            tblCell.Controls.Add(oLinkAverageCostMachineValue)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblDashboard.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' CUSTOMERS
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblCustomer As New RadLabel
            lblCustomer.ID = "lblCustomer"
            lblCustomer.Text = "Customer"
            lblCustomer.Style.Add("font-size", "8pt")
            lblCustomer.Style.Add("text-align", "left")
            lblCustomer.Style.Add("font-weight", "bold")
            tblCell.Controls.Add(lblCustomer)
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("padding-left", "5px")
            tblCell.Style.Add("background", "#e7e7e7")
            tblCell.ColumnSpan = 2
            tblRow.Cells.Add(tblCell)

            tblDashboard.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' CLIENTSERVED
            For Each oRowItm As DataRow In oDTItems.Rows
                tblRow = New TableRow
                tblCell = New TableCell
                Dim lblWholesaleServed As New RadLabel
                lblWholesaleServed.ID = "lblWholesaleServed_" & Nz(oRowItm("VariableNameDefault"))
                lblWholesaleServed.Text = "Customer satisfaction - customer served - " & Nz(oRowItm("VariableName"))
                lblWholesaleServed.Style.Add("font-size", "8pt")
                lblWholesaleServed.Style.Add("text-align", "left")
                lblWholesaleServed.Style.Add("font-weight", "normal")
                tblCell.Controls.Add(lblWholesaleServed)
                tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
                tblCell.Style.Add("padding-left", "5px")
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                Dim oLinkWholesaleServedValue As New HyperLink
                oLinkWholesaleServedValue.Text = Nn(GetVariableDefineValue("ClientServed", Session("IDTeam"), oRowItm("IDVariable"), 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
                oLinkWholesaleServedValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=ClientServed&II=" & oRowItm("IDVariable") & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                oLinkWholesaleServedValue.Style.Add("text-decoration", "none")
                oLinkWholesaleServedValue.Style.Add("cursor", "pointer")
                oLinkWholesaleServedValue.Style.Add("font-size", "8pt")
                oLinkWholesaleServedValue.Style.Add("text-align", "right")
                oLinkWholesaleServedValue.BackColor = Drawing.Color.Transparent
                tblCell.Controls.Add(oLinkWholesaleServedValue)
                tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
                tblCell.Style.Add("text-align", "right")
                tblRow.Cells.Add(tblCell)

                tblDashboard.Rows.Add(tblRow)
            Next
            ' ---------------------------------------------------------------------------------

            ' MARKET SHARE PER VOLUME
            For Each oRowItm As DataRow In oDTItems.Rows
                tblRow = New TableRow
                tblCell = New TableCell
                Dim lblMarketShareVolume As New RadLabel
                lblMarketShareVolume.ID = "lblMarketShareVolume_" & Nz(oRowItm("VariableName"))
                lblMarketShareVolume.Text = "Market share per volume - " & Nz(oRowItm("VariableName"))
                lblMarketShareVolume.Style.Add("font-size", "8pt")
                lblMarketShareVolume.Style.Add("text-align", "left")
                lblMarketShareVolume.Style.Add("font-weight", "normal")
                tblCell.Controls.Add(lblMarketShareVolume)
                tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
                tblCell.Style.Add("padding-left", "5px")
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                Dim oLinkMarketShareVolumeValue As New HyperLink
                oLinkMarketShareVolumeValue.Text = Nn(GetVariableDefineValue("MarketShare", Session("IDTeam"), oRowItm("IDVariable"), 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
                oLinkMarketShareVolumeValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=MarketShare&II=" & oRowItm("IDVariable") & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                oLinkMarketShareVolumeValue.Style.Add("text-decoration", "none")
                oLinkMarketShareVolumeValue.Style.Add("cursor", "pointer")
                oLinkMarketShareVolumeValue.Style.Add("font-size", "8pt")
                oLinkMarketShareVolumeValue.Style.Add("text-align", "right")
                oLinkMarketShareVolumeValue.BackColor = Drawing.Color.Transparent

                tblCell.Controls.Add(oLinkMarketShareVolumeValue)
                tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
                tblCell.Style.Add("text-align", "right")
                tblRow.Cells.Add(tblCell)

                tblDashboard.Rows.Add(tblRow)
            Next
            ' ---------------------------------------------------------------------------------

            ' MARKET SHARE IN VALUE
            For Each oRowItm As DataRow In oDTItems.Rows
                tblRow = New TableRow
                tblCell = New TableCell
                Dim lblMarketShareValue As New RadLabel
                lblMarketShareValue.ID = "lblMarketShareValue_" & Nz(oRowItm("VariableName"))
                lblMarketShareValue.Text = "Market share in value - " & Nz(oRowItm("VariableName"))
                lblMarketShareValue.Style.Add("font-size", "8pt")
                lblMarketShareValue.Style.Add("text-align", "left")
                lblMarketShareValue.Style.Add("font-weight", "normal")
                tblCell.Controls.Add(lblMarketShareValue)
                tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
                tblCell.Style.Add("padding-left", "5px")
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                Dim oLinkMarketShareValueVal As New HyperLink
                oLinkMarketShareValueVal.Text = Nn(GetVariableDefineValue("MarketPriceShare", Session("IDTeam"), oRowItm("IDVariable"), 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
                oLinkMarketShareValueVal.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=MarketPriceShare&II=" & oRowItm("IDVariable") & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                oLinkMarketShareValueVal.Style.Add("text-decoration", "none")
                oLinkMarketShareValueVal.Style.Add("cursor", "pointer")
                oLinkMarketShareValueVal.Style.Add("font-size", "8pt")
                oLinkMarketShareValueVal.Style.Add("text-align", "right")
                oLinkMarketShareValueVal.BackColor = Drawing.Color.Transparent
                tblCell.Controls.Add(oLinkMarketShareValueVal)
                tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
                tblCell.Style.Add("text-align", "right")
                tblRow.Cells.Add(tblCell)

                tblDashboard.Rows.Add(tblRow)
            Next
            ' ---------------------------------------------------------------------------------

            ' Perceived Use Value
            For Each oRowItm As DataRow In oDTItems.Rows
                tblRow = New TableRow
                tblCell = New TableCell
                Dim lblPreceivedUseValue As New RadLabel
                lblPreceivedUseValue.ID = "lblPreceivedUseValue_" & Nz(oRowItm("VariableName"))
                lblPreceivedUseValue.Text = "Perceived use value - " & Nz(oRowItm("VariableName"))
                lblPreceivedUseValue.Style.Add("font-size", "8pt")
                lblPreceivedUseValue.Style.Add("text-align", "left")
                lblPreceivedUseValue.Style.Add("font-weight", "normal")
                tblCell.Controls.Add(lblPreceivedUseValue)
                tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
                tblCell.Style.Add("padding-left", "5px")
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                Dim oLinkPreceivedUseValueVal As New HyperLink
                oLinkPreceivedUseValueVal.Text = Nn(GetVariableDefineValue("PerceValueItem", Session("IDTeam"), oRowItm("IDVariable"), 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
                oLinkPreceivedUseValueVal.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=PerceValueItem&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                oLinkPreceivedUseValueVal.Style.Add("text-decoration", "none")
                oLinkPreceivedUseValueVal.Style.Add("cursor", "pointer")
                oLinkPreceivedUseValueVal.Style.Add("font-size", "8pt")
                oLinkPreceivedUseValueVal.Style.Add("text-align", "right")
                oLinkPreceivedUseValueVal.BackColor = Drawing.Color.Transparent

                tblCell.Controls.Add(oLinkPreceivedUseValueVal)
                tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
                tblCell.Style.Add("text-align", "right")
                tblRow.Cells.Add(tblCell)

                tblDashboard.Rows.Add(tblRow)
            Next
            ' ---------------------------------------------------------------------------------

            ' INNOVATION
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblInnovation As New RadLabel
            lblInnovation.ID = "lblInnovation"
            lblInnovation.Text = "Innovation"
            lblInnovation.Style.Add("font-size", "8pt")
            lblInnovation.Style.Add("text-align", "left")
            lblInnovation.Style.Add("font-weight", "bold")
            tblCell.Controls.Add(lblInnovation)
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("padding-left", "5px")
            tblCell.Style.Add("background", "#e7e7e7")
            tblCell.ColumnSpan = 2
            tblRow.Cells.Add(tblCell)

            tblDashboard.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' Product innovation quality (vs. the mkt average +/-)
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblProductInnovation As New RadLabel
            lblProductInnovation.ID = "lblProductInnovation"
            lblProductInnovation.Text = "Product innovation quality (vs. the mkt average +/-)"
            lblProductInnovation.Style.Add("font-size", "8pt")
            lblProductInnovation.Style.Add("text-align", "left")
            lblProductInnovation.Style.Add("font-weight", "normal")
            tblCell.Controls.Add(lblProductInnovation)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("padding-left", "5px")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLinkProductInnovationValue As New HyperLink
            oLinkProductInnovationValue.Text = Nn(GetVariableDefineValue("PerceQuality", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
            oLinkProductInnovationValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=PerceQuality&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            oLinkProductInnovationValue.Style.Add("text-decoration", "none")
            oLinkProductInnovationValue.Style.Add("cursor", "pointer")
            oLinkProductInnovationValue.Style.Add("font-size", "8pt")
            oLinkProductInnovationValue.Style.Add("text-align", "right")
            oLinkProductInnovationValue.BackColor = Drawing.Color.Transparent

            tblCell.Controls.Add(oLinkProductInnovationValue)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblDashboard.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' Training Level - Staff
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblTrainingLevel As New RadLabel
            lblTrainingLevel.ID = "lblTrainingLevel"
            lblTrainingLevel.Text = "Training Level - Staff"
            lblTrainingLevel.Style.Add("font-size", "8pt")
            lblTrainingLevel.Style.Add("text-align", "left")
            lblTrainingLevel.Style.Add("font-weight", "normal")
            tblCell.Controls.Add(lblTrainingLevel)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("padding-left", "5px")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLinkTrainingLevelValue As New HyperLink
            oLinkTrainingLevelValue.Text = Nn(GetVariableState("StaffTrainLevel", Session("IDTeam"), m_SiteMaster.PeriodGetCurrent, "", Session("IDGame"))).ToString("N2")
            oLinkTrainingLevelValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=StaffTrainLevel&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            oLinkTrainingLevelValue.Style.Add("text-decoration", "none")
            oLinkTrainingLevelValue.Style.Add("cursor", "pointer")
            oLinkTrainingLevelValue.Style.Add("font-size", "8pt")
            oLinkTrainingLevelValue.Style.Add("text-align", "right")
            oLinkTrainingLevelValue.BackColor = Drawing.Color.Transparent
            tblCell.Controls.Add(oLinkTrainingLevelValue)
            tblCell.Style.Add("text-align", "right")
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblRow.Cells.Add(tblCell)

            tblDashboard.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------


            ' Training Level - Sales persons
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblTrainingLevelPersons As New RadLabel
            lblTrainingLevelPersons.ID = "lblTrainingLevelPersons"
            lblTrainingLevelPersons.Text = "Training Level - Sales persons"
            lblTrainingLevelPersons.Style.Add("font-size", "8pt")
            lblTrainingLevelPersons.Style.Add("text-align", "left")
            lblTrainingLevelPersons.Style.Add("font-weight", "normal")
            tblCell.Controls.Add(lblTrainingLevelPersons)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("padding-left", "5px")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLinkTrainingLevelPersonsValue As New HyperLink
            oLinkTrainingLevelPersonsValue.Text = Nn(GetVariableState("PointTrainLevel", Session("IDTeam"), m_SiteMaster.PeriodGetCurrent, "", Session("IDGame"))).ToString("N2")
            oLinkTrainingLevelPersonsValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=PointTrainLevel&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            oLinkTrainingLevelPersonsValue.Style.Add("text-decoration", "none")
            oLinkTrainingLevelPersonsValue.Style.Add("cursor", "pointer")
            oLinkTrainingLevelPersonsValue.Style.Add("font-size", "8pt")
            oLinkTrainingLevelPersonsValue.Style.Add("text-align", "right")
            oLinkTrainingLevelPersonsValue.BackColor = Drawing.Color.Transparent

            tblCell.Controls.Add(oLinkTrainingLevelPersonsValue)
            tblCell.Style.Add("text-align", "right")
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblRow.Cells.Add(tblCell)

            tblDashboard.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------
        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub ChiefExecutive_Dashboard_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        m_SessionData = Session("SessionData")

        m_SiteMaster = Me.Master

        m_SiteMaster.LoadPeriodsValid = Session("IDRole") = "P"

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oCBOPeriod As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOPeriodValid As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriodValid"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler m_SiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler m_SiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(Session("IDGame")) ' Ultimo periodo
        oCBOPeriod.SelectedValue = Session("IDPeriodValid")
        m_SiteMaster.PeriodChange = Session("IDPeriodValid")

        Session("NameTeam") = GetTeamName(Session("IDTeam"))
        oCBOTeam.SelectedValue = Session("IDTeam")
        m_SiteMaster.TeamChange = Session("IDTeam")

        ' Gestione del pannello delle decisioni concesse
        modalPopup.OpenerElementID = lnkHelp.ClientID
        modalPopup.Modal = False
        modalPopup.VisibleTitlebar = True

        LoadHelp()
    End Sub

    Private Sub ChiefExecutive_Dashboard_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        If Not Page.IsPostBack Then
            If Session("IDRole") = "P" Then
                m_SiteMaster.ReloadPeriodsCalculate()
            End If
            LoadDataDashboard()

            btnTranslate.Visible = Session("IDRole").Contains("D")
            ' Controllo tutti gli oggetti presenti
            Dim oTranslation As New TranslationObjectForm(Me, g_DAL, Nz(Session("LanguageActive")))

        End If
    End Sub

    Private Sub btnTranslate_Click(sender As Object, e As EventArgs) Handles btnTranslate.Click
        Session("PageOrigin") = Me.ToString().Substring(4, Me.ToString().Substring(4).Length - 5) + ".aspx"
        Response.Redirect("PageTranslation.aspx")
        Response.End()
    End Sub

#Region "PANNELLO HELP"

    Private Sub LoadHelp()
        lblHelp.Text = "FINANCIAL INDICATORS
                    (*these financial KPIs must be consiered when calculated at the end of the year)</p>
                  <p>ROE <br>
                            &emsp; Net Profit of the year / (retained earnings + net capital)</p>
                  <p>ROCE <br>
                            &emsp; EBIT for the year / (retained earnings + net capital + long term loans)</p>
                  <p>ROS <br>
                            &emsp; Operating profit of the year / revenues of the year</p>
                  <p>EBITDA <br>
                            &emsp; (Profit before tax + Interests + Loss on Sales + Depreciation of machines and vehicles) / Revenues</p>
                  <p>Current ratio <br>
                        &emsp; (Cash + CreditorsReceivables + Stock)  / (Tax + DebtsPayables)</p>
                  <p>Acid Test <br>
                        &emsp; (Cash + CreditorsReceivables )  / (Tax + DebtsPayables)                    </p>
                  <p>Interests cover <br>
                        &emsp; (EBIT)  / (Begative Interests)</p>
                  <p>Debts <br>
                        &emsp; CreditsReceivables  / (Earnings + Capital + Loans) </p>
                  <p>Perceived Use Value <br> 
                    &emsp; Compared Quality Level * importance of Quality + Compared Attractiveness <br> 
                    &emsp; * Importance of Attractiveness <br> 
                    &emsp; /(Importance of Quality + Importance of Attractiveness)
                </p>"
    End Sub

#End Region

End Class
