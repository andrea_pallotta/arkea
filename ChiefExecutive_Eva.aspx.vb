﻿Imports System.Data
Imports APS.APSUtility
Imports APPCore.Utility
Imports DALC4NET
Imports Telerik.Web.UI

Partial Class ChiefExecutive_Eva
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadData()
        Message.Visible = False

        UpdatePanelMain.Update()
    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadData()

        Message.Visible = False
        UpdatePanelMain.Update()
    End Sub

    Private Sub LoadData()
        tblEVA.Rows.Clear()
        LoadDataEva()
    End Sub

    Private Sub LoadDataEva()
        Dim tblHeaderCell As TableCell
        Dim tblHeaderRow As TableRow

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Try
            CheckVariablesTranslations()

            ' TITOLO
            tblHeaderRow = New TableRow
            tblHeaderCell = New TableHeaderCell
            Dim oLBLNo As New RadLabel
            oLBLNo.Text = "EVA"
            oLBLNo.ForeColor = Drawing.Color.White
            tblHeaderCell.Controls.Add(oLBLNo)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.ColumnSpan = 2
            tblHeaderCell.Style.Add("background", "#25536F")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            tblHeaderRow.Cells.Add(tblHeaderCell)

            ' CAPITAL
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblHeaderTitle As New RadLabel
            lblHeaderTitle.ID = "lblHeaderTitle"
            lblHeaderTitle.Text = "The surplus earned on Capital"
            lblHeaderTitle.Style.Add("font-size", "8pt")
            lblHeaderTitle.Style.Add("text-align", "left")
            lblHeaderTitle.Style.Add("font-weight", "bold")
            lblHeaderTitle.Style.Add("color", "#ffffff")
            tblCell.Controls.Add(lblHeaderTitle)
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("padding-left", "20px")
            tblCell.Style.Add("background", "#3e6c86")
            tblCell.ColumnSpan = 2
            tblRow.Cells.Add(tblCell)

            tblEVA.Rows.Add(tblRow)

            ' Capital
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblCapital As New RadLabel
            lblCapital.ID = "lblCapital"
            lblCapital.Text = "Capital"
            lblCapital.Style.Add("font-size", "8pt")
            lblCapital.Style.Add("text-align", "left")
            lblCapital.Style.Add("font-weight", "bold")
            tblCell.Controls.Add(lblCapital)
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("padding-left", "5px")
            tblCell.Style.Add("background", "#e7e7e7")
            tblCell.ColumnSpan = 2
            tblRow.Cells.Add(tblCell)

            tblEVA.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' RETAINED EARNINGS
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblProfCUM As New RadLabel
            lblProfCUM.ID = "lblProfCUM"
            lblProfCUM.Text = "Retained earnings"
            lblProfCUM.Style.Add("font-size", "8pt")
            lblProfCUM.Style.Add("text-align", "left")
            lblProfCUM.Style.Add("font-weight", "normal")
            tblCell.Controls.Add(lblProfCUM)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("padding-left", "5px")
            tblCell.Style.Add("width", "75%")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLinkProfCUMValue As New HyperLink
            oLinkProfCUMValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=UtilsEndPerio&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            oLinkProfCUMValue.Style.Add("text-decoration", "none")
            oLinkProfCUMValue.Style.Add("cursor", "pointer")
            oLinkProfCUMValue.Text = Nn(GetVariableDefineValue("UtilsEndPerio", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N0")
            oLinkProfCUMValue.Style.Add("font-size", "8pt")
            oLinkProfCUMValue.Style.Add("text-align", "right")
            oLinkProfCUMValue.BackColor = Drawing.Color.Transparent

            tblCell.Controls.Add(oLinkProfCUMValue)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblEVA.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' SHARE CAPITAL
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblNetCapital As New RadLabel
            lblNetCapital.ID = "lblNetCapital"
            lblNetCapital.Text = "Share capital"
            lblNetCapital.Style.Add("font-size", "8pt")
            lblNetCapital.Style.Add("text-align", "left")
            lblNetCapital.Style.Add("font-weight", "normal")
            tblCell.Controls.Add(lblNetCapital)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("padding-left", "5px")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLinkNetCapitalValue As New HyperLink
            oLinkNetCapitalValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=NetCapital&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            oLinkNetCapitalValue.Style.Add("text-decoration", "none")
            oLinkNetCapitalValue.Style.Add("cursor", "pointer")
            oLinkNetCapitalValue.Text = Nn(GetVariableState("NetCapital", Session("IDTeam"), m_SiteMaster.PeriodGetCurrent, "", Session("IDGame"))).ToString("N2")
            oLinkNetCapitalValue.Style.Add("font-size", "8pt")
            oLinkNetCapitalValue.Style.Add("text-align", "right")
            oLinkNetCapitalValue.BackColor = Drawing.Color.Transparent

            tblCell.Controls.Add(oLinkNetCapitalValue)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblEVA.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' Equity
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblEquity As New RadLabel
            lblEquity.ID = "lblEquity"
            lblEquity.Text = "Equity"
            lblEquity.Style.Add("font-size", "8pt")
            lblEquity.Style.Add("text-align", "left")
            lblEquity.Style.Add("font-weight", "bold")
            tblCell.Controls.Add(lblEquity)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("padding-left", "40px")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLinkEquityValue As New HyperLink
            oLinkEquityValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=Equity&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            oLinkEquityValue.Style.Add("text-decoration", "none")
            oLinkEquityValue.Style.Add("cursor", "pointer")
            oLinkEquityValue.Text = Nn(GetVariableDefineValue("Equity", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
            oLinkEquityValue.Style.Add("font-size", "8pt")
            oLinkEquityValue.Style.Add("text-align", "right")
            oLinkEquityValue.BackColor = Drawing.Color.Transparent

            tblCell.Controls.Add(oLinkEquityValue)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblEVA.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' Long term loan 
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblLoansEndPerio As New RadLabel
            lblLoansEndPerio.ID = "lblLoansEndPerio"
            lblLoansEndPerio.Text = "Long term loan"
            lblLoansEndPerio.Style.Add("font-size", "8pt")
            lblLoansEndPerio.Style.Add("text-align", "left")
            lblLoansEndPerio.Style.Add("font-weight", "normal")
            tblCell.Controls.Add(lblLoansEndPerio)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("padding-left", "5px")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLinkLoansEndPerioValue As New HyperLink
            oLinkLoansEndPerioValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=LoansEndPerio&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            oLinkLoansEndPerioValue.Style.Add("text-decoration", "none")
            oLinkLoansEndPerioValue.Style.Add("cursor", "pointer")
            oLinkLoansEndPerioValue.Text = Nn(GetVariableDefineValue("LoansEndPerio", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
            oLinkLoansEndPerioValue.Style.Add("font-size", "8pt")
            oLinkLoansEndPerioValue.Style.Add("text-align", "right")
            oLinkLoansEndPerioValue.BackColor = Drawing.Color.Transparent

            tblCell.Controls.Add(oLinkLoansEndPerioValue)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblEVA.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' Total invested capital
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblTotalInvestedCapital As New RadLabel
            lblTotalInvestedCapital.ID = "lblTotalInvestedCapital"
            lblTotalInvestedCapital.Text = "Total invested capital"
            lblTotalInvestedCapital.Style.Add("font-size", "8pt")
            lblTotalInvestedCapital.Style.Add("text-align", "left")
            lblTotalInvestedCapital.Style.Add("font-weight", "bold")
            tblCell.Controls.Add(lblTotalInvestedCapital)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("padding-left", "40px")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLinkLTCapitalValue As New HyperLink
            oLinkLTCapitalValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=LTCapital&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            oLinkLTCapitalValue.Style.Add("text-decoration", "none")
            oLinkLTCapitalValue.Style.Add("cursor", "pointer")
            oLinkLTCapitalValue.Text = Nn(GetVariableDefineValue("LTCapital", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
            oLinkLTCapitalValue.Style.Add("font-size", "8pt")
            oLinkLTCapitalValue.Style.Add("text-align", "right")
            oLinkLTCapitalValue.BackColor = Drawing.Color.Transparent

            tblCell.Controls.Add(oLinkLTCapitalValue)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblEVA.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' Empty row
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLblEmptyRow As New RadLabel
            oLblEmptyRow.Text = " "
            oLblEmptyRow.Style.Add("font-size", "8pt")
            oLblEmptyRow.Style.Add("text-align", "left")
            oLblEmptyRow.Style.Add("font-weight", "bold")
            tblCell.Controls.Add(oLblEmptyRow)
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("padding-left", "5px")
            tblCell.Style.Add("background", "#ffffff")
            tblCell.ColumnSpan = 2
            tblRow.Cells.Add(tblCell)

            tblEVA.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' OPERATING PROFIT PER P&L
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblOperatingProfit As New RadLabel
            lblOperatingProfit.ID = "lblOperatingProfit"
            lblOperatingProfit.Text = "OPERATING PROFIT PER P&L"
            lblOperatingProfit.Style.Add("font-size", "8pt")
            lblOperatingProfit.Style.Add("text-align", "left")
            lblOperatingProfit.Style.Add("font-weight", "bold")
            tblCell.Controls.Add(lblOperatingProfit)
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("padding-left", "5px")
            tblCell.Style.Add("background", "#e7e7e7")
            tblCell.ColumnSpan = 2
            tblRow.Cells.Add(tblCell)

            tblEVA.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' Profit before tax
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblProfitBeforeTax As New RadLabel
            lblProfitBeforeTax.ID = "lblProfitBeforeTax"
            lblProfitBeforeTax.Text = "Profit before tax"
            lblProfitBeforeTax.Style.Add("font-size", "8pt")
            lblProfitBeforeTax.Style.Add("text-align", "left")
            lblProfitBeforeTax.Style.Add("font-weight", "normal")
            tblCell.Controls.Add(lblProfitBeforeTax)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("padding-left", "5px")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLinkCumulBeforTaxValue As New HyperLink
            oLinkCumulBeforTaxValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=CumulBeforTax&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            oLinkCumulBeforTaxValue.Style.Add("text-decoration", "none")
            oLinkCumulBeforTaxValue.Style.Add("cursor", "pointer")
            oLinkCumulBeforTaxValue.Text = Nn(GetVariableDefineValue("CumulBeforTax", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
            oLinkCumulBeforTaxValue.Style.Add("font-size", "8pt")
            oLinkCumulBeforTaxValue.Style.Add("text-align", "right")
            oLinkCumulBeforTaxValue.BackColor = Drawing.Color.Transparent

            tblCell.Controls.Add(oLinkCumulBeforTaxValue)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblEVA.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' LOAN INTEREST
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblLoanInterest As New RadLabel
            lblLoanInterest.ID = "lblLoanInterest"
            lblLoanInterest.Text = "Loan interest"
            lblLoanInterest.Style.Add("font-size", "8pt")
            lblLoanInterest.Style.Add("text-align", "left")
            lblLoanInterest.Style.Add("font-weight", "normal")
            tblCell.Controls.Add(lblLoanInterest)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("padding-left", "5px")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLinkCumulNegatInterValue As New HyperLink
            oLinkCumulNegatInterValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=CumulNegatInter&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            oLinkCumulNegatInterValue.Style.Add("text-decoration", "none")
            oLinkCumulNegatInterValue.Style.Add("cursor", "pointer")
            oLinkCumulNegatInterValue.Text = Nn(GetVariableDefineValue("CumulNegatInter", m_SiteMaster.TeamIDGet, 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
            oLinkCumulNegatInterValue.Style.Add("font-size", "8pt")
            oLinkCumulNegatInterValue.Style.Add("text-align", "right")
            oLinkCumulNegatInterValue.BackColor = Drawing.Color.Transparent

            tblCell.Controls.Add(oLinkCumulNegatInterValue)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblEVA.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' LESS TAX
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblLessTax As New RadLabel
            lblLessTax.ID = "lblLessTax"
            lblLessTax.Text = "Less tax"
            lblLessTax.Style.Add("font-size", "8pt")
            lblLessTax.Style.Add("text-align", "left")
            lblLessTax.Style.Add("font-weight", "normal")
            tblCell.Controls.Add(lblLessTax)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("padding-left", "5px")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLinkTassePerioCumulValue As New HyperLink
            oLinkTassePerioCumulValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=TassePerioCumul&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            oLinkTassePerioCumulValue.Style.Add("text-decoration", "none")
            oLinkTassePerioCumulValue.Style.Add("cursor", "pointer")
            oLinkTassePerioCumulValue.Text = Nn(GetVariableDefineValue("TassePerioCumul", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
            oLinkTassePerioCumulValue.Style.Add("font-size", "8pt")
            oLinkTassePerioCumulValue.Style.Add("text-align", "right")
            oLinkTassePerioCumulValue.BackColor = Drawing.Color.Transparent

            tblCell.Controls.Add(oLinkTassePerioCumulValue)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblEVA.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' Operating profit after tax
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblOperatingProfitAfterTax As New RadLabel
            lblOperatingProfitAfterTax.ID = "lblOperatingProfitAfterTax"
            lblOperatingProfitAfterTax.Text = "Operating profit after tax"
            lblOperatingProfitAfterTax.Style.Add("font-size", "8pt")
            lblOperatingProfitAfterTax.Style.Add("text-align", "left")
            lblOperatingProfitAfterTax.Style.Add("font-weight", "bold")
            tblCell.Controls.Add(lblOperatingProfitAfterTax)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("padding-left", "40px")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLinkOperaProfiAfterTaxValue As New HyperLink
            oLinkOperaProfiAfterTaxValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=OperaProfiAfterTax&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            oLinkOperaProfiAfterTaxValue.Style.Add("text-decoration", "none")
            oLinkOperaProfiAfterTaxValue.Style.Add("cursor", "pointer")
            oLinkOperaProfiAfterTaxValue.Text = Nn(GetVariableDefineValue("OperaProfiAfterTax", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
            oLinkOperaProfiAfterTaxValue.Style.Add("font-size", "8pt")
            oLinkOperaProfiAfterTaxValue.Style.Add("text-align", "right")
            oLinkOperaProfiAfterTaxValue.BackColor = Drawing.Color.Transparent

            tblCell.Controls.Add(oLinkOperaProfiAfterTaxValue)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblEVA.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' Return on capital %
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblReturnOnCapital As New RadLabel
            lblReturnOnCapital.ID = "lblReturnOnCapital"
            lblReturnOnCapital.Text = "Return on capital %"
            lblReturnOnCapital.Style.Add("font-size", "8pt")
            lblReturnOnCapital.Style.Add("text-align", "left")
            lblReturnOnCapital.Style.Add("font-weight", "bolad")
            tblCell.Controls.Add(lblReturnOnCapital)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("padding-left", "40px")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLinkReturnOnCapitalValue As New HyperLink
            oLinkReturnOnCapitalValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=ReturOnCapital&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            oLinkReturnOnCapitalValue.Style.Add("text-decoration", "none")
            oLinkReturnOnCapitalValue.Style.Add("cursor", "pointer")
            oLinkReturnOnCapitalValue.Text = Nn(GetVariableDefineValue("ReturOnCapital", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
            oLinkReturnOnCapitalValue.Style.Add("font-size", "8pt")
            oLinkReturnOnCapitalValue.Style.Add("text-align", "right")
            oLinkReturnOnCapitalValue.BackColor = Drawing.Color.Transparent

            tblCell.Controls.Add(oLinkReturnOnCapitalValue)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblEVA.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' Empty row
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLblEmptyRow2 As New RadLabel
            oLblEmptyRow2.Text = " "
            oLblEmptyRow2.Style.Add("font-size", "8pt")
            oLblEmptyRow2.Style.Add("text-align", "left")
            oLblEmptyRow2.Style.Add("font-weight", "bold")
            tblCell.Controls.Add(oLblEmptyRow2)
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("padding-left", "5px")
            tblCell.Style.Add("background", "#ffffff")
            tblCell.ColumnSpan = 2
            tblRow.Cells.Add(tblCell)

            tblEVA.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' COST OF CAPITAL
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblCostOFCapital As New RadLabel
            lblCostOFCapital.ID = "lblCostOFCapital"
            lblCostOFCapital.Text = "COST OF CAPITAL"
            lblCostOFCapital.Style.Add("font-size", "8pt")
            lblCostOFCapital.Style.Add("text-align", "left")
            lblCostOFCapital.Style.Add("font-weight", "bold")
            tblCell.Controls.Add(lblCostOFCapital)
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("padding-left", "5px")
            tblCell.Style.Add("background", "#e7e7e7")
            tblCell.ColumnSpan = 2
            tblRow.Cells.Add(tblCell)

            tblEVA.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' Interest rate
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblInterestRate As New RadLabel
            lblInterestRate.ID = "lblInterestRate"
            lblInterestRate.Text = "Interest rate"
            lblInterestRate.Style.Add("font-size", "8pt")
            lblInterestRate.Style.Add("text-align", "left")
            lblInterestRate.Style.Add("font-weight", "normal")
            tblCell.Controls.Add(lblInterestRate)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("padding-left", "5px")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLinkInterestRateValue As New HyperLink
            oLinkInterestRateValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=InterRateYear&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            oLinkInterestRateValue.Style.Add("text-decoration", "none")
            oLinkInterestRateValue.Style.Add("cursor", "pointer")
            oLinkInterestRateValue.Text = Nn(GetVariableDefineValue("InterRateYear", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
            oLinkInterestRateValue.Style.Add("font-size", "8pt")
            oLinkInterestRateValue.Style.Add("text-align", "right")
            oLinkInterestRateValue.BackColor = Drawing.Color.Transparent

            tblCell.Controls.Add(oLinkInterestRateValue)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblEVA.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' Less tax
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblLessTaxCapital As New RadLabel
            lblLessTaxCapital.ID = "lblLessTaxCapital"
            lblLessTaxCapital.Text = "Less tax"
            lblLessTaxCapital.Style.Add("font-size", "8pt")
            lblLessTaxCapital.Style.Add("text-align", "left")
            lblLessTaxCapital.Style.Add("font-weight", "normal")
            tblCell.Controls.Add(lblLessTaxCapital)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("padding-left", "5px")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLinkLessTaxCapitalValue As New HyperLink
            oLinkLessTaxCapitalValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=InterNetRate&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            oLinkLessTaxCapitalValue.Style.Add("text-decoration", "none")
            oLinkLessTaxCapitalValue.Style.Add("cursor", "pointer")
            oLinkLessTaxCapitalValue.Text = Nn(GetVariableDefineValue("InterNetRate", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
            oLinkLessTaxCapitalValue.Style.Add("font-size", "8pt")
            oLinkLessTaxCapitalValue.Style.Add("text-align", "right")
            oLinkLessTaxCapitalValue.BackColor = Drawing.Color.Transparent

            tblCell.Controls.Add(oLinkLessTaxCapitalValue)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblEVA.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' Actual cost
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblActualCost As New RadLabel
            lblActualCost.ID = "lblActualCost"
            lblActualCost.Text = "Actual cost to company of debt"
            lblActualCost.Style.Add("font-size", "8pt")
            lblActualCost.Style.Add("text-align", "left")
            lblActualCost.Style.Add("font-weight", "normal")
            tblCell.Controls.Add(lblActualCost)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("padding-left", "5px")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLinkActualCostValue As New HyperLink
            oLinkActualCostValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=ActualCostOfDebt&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            oLinkActualCostValue.Style.Add("text-decoration", "none")
            oLinkActualCostValue.Style.Add("cursor", "pointer")
            oLinkActualCostValue.Text = Nn(GetVariableDefineValue("ActualCostOfDebt", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
            oLinkActualCostValue.Style.Add("font-size", "8pt")
            oLinkActualCostValue.Style.Add("text-align", "right")
            oLinkActualCostValue.BackColor = Drawing.Color.Transparent

            tblCell.Controls.Add(oLinkActualCostValue)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblEVA.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' Empty row
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLblEmptyRow3 As New RadLabel
            oLblEmptyRow3.Text = " "
            oLblEmptyRow3.Style.Add("font-size", "8pt")
            oLblEmptyRow3.Style.Add("text-align", "left")
            oLblEmptyRow3.Style.Add("font-weight", "bold")
            tblCell.Controls.Add(oLblEmptyRow3)
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("padding-left", "5px")
            tblCell.Style.Add("background", "#ffffff")
            tblCell.ColumnSpan = 2
            tblRow.Cells.Add(tblCell)

            tblEVA.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' Cost of equity
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblStockEquity As New RadLabel
            lblStockEquity.ID = "lblStockEquity"
            lblStockEquity.Text = "Cost of equity"
            lblStockEquity.Style.Add("font-size", "8pt")
            lblStockEquity.Style.Add("text-align", "left")
            lblStockEquity.Style.Add("font-weight", "normal")
            tblCell.Controls.Add(lblStockEquity)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("padding-left", "5px")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLinkCostOfEquitValue As New HyperLink
            oLinkCostOfEquitValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=CostOfEquit&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            oLinkCostOfEquitValue.Style.Add("text-decoration", "none")
            oLinkCostOfEquitValue.Style.Add("cursor", "pointer")
            oLinkCostOfEquitValue.Text = Nn(GetVariableDefineValue("CostOfEquit", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
            oLinkCostOfEquitValue.Style.Add("font-size", "8pt")
            oLinkCostOfEquitValue.Style.Add("text-align", "right")
            oLinkCostOfEquitValue.BackColor = Drawing.Color.Transparent

            tblCell.Controls.Add(oLinkCostOfEquitValue)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblEVA.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' WACC %
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblWACC As New RadLabel
            lblWACC.ID = "lblWACC"
            lblWACC.Text = "WACC %"
            lblWACC.Style.Add("font-size", "8pt")
            lblWACC.Style.Add("text-align", "left")
            lblWACC.Style.Add("font-weight", "bold")
            tblCell.Controls.Add(lblWACC)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("padding-left", "40px")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLinkWACClValue As New HyperLink
            oLinkWACClValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=WeighCostCapit&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            oLinkWACClValue.Style.Add("text-decoration", "none")
            oLinkWACClValue.Style.Add("cursor", "pointer")
            oLinkWACClValue.Text = Nn(GetVariableDefineValue("WeighCostCapit", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
            oLinkWACClValue.Style.Add("font-size", "8pt")
            oLinkWACClValue.Style.Add("text-align", "right")
            oLinkWACClValue.BackColor = Drawing.Color.Transparent

            tblCell.Controls.Add(oLinkWACClValue)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblEVA.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' Empty row
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLblEmptyRow4 As New RadLabel
            oLblEmptyRow4.Text = " "
            oLblEmptyRow4.Style.Add("font-size", "8pt")
            oLblEmptyRow4.Style.Add("text-align", "left")
            oLblEmptyRow4.Style.Add("font-weight", "bold")
            tblCell.Controls.Add(oLblEmptyRow4)
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("padding-left", "5px")
            tblCell.Style.Add("background", "#ffffff")
            tblCell.ColumnSpan = 2
            tblRow.Cells.Add(tblCell)

            tblEVA.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' EVA CALCULATION
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblEVACalculation As New RadLabel
            lblEVACalculation.ID = "lblEVACalculation"
            lblEVACalculation.Text = "EVA CALCULATION"
            lblEVACalculation.Style.Add("font-size", "8pt")
            lblEVACalculation.Style.Add("text-align", "left")
            lblEVACalculation.Style.Add("font-weight", "bold")
            tblCell.Controls.Add(lblEVACalculation)
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("padding-left", "5px")
            tblCell.Style.Add("background", "#e7e7e7")
            tblCell.ColumnSpan = 2
            tblRow.Cells.Add(tblCell)

            tblEVA.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' Capital * Spread
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblCapitalSpread As New RadLabel
            lblCapitalSpread.ID = "lblCapitalSpread"
            lblCapitalSpread.Text = "Capital * Spread"
            lblCapitalSpread.Style.Add("font-size", "8pt")
            lblCapitalSpread.Style.Add("text-align", "left")
            lblCapitalSpread.Style.Add("font-weight", "bold")
            tblCell.Controls.Add(lblCapitalSpread)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("padding-left", "40px")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLinlCapitalSpreadValue As New HyperLink
            oLinlCapitalSpreadValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=EVA1&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            oLinlCapitalSpreadValue.Style.Add("text-decoration", "none")
            oLinlCapitalSpreadValue.Style.Add("cursor", "pointer")
            oLinlCapitalSpreadValue.Text = Nn(GetVariableDefineValue("EVA1", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
            oLinlCapitalSpreadValue.Style.Add("font-size", "8pt")
            oLinlCapitalSpreadValue.Style.Add("text-align", "right")
            oLinlCapitalSpreadValue.BackColor = Drawing.Color.Transparent
            tblCell.Controls.Add(oLinlCapitalSpreadValue)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblEVA.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' Empty row
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLblEmptyRow5 As New RadLabel
            oLblEmptyRow5.Text = " "
            oLblEmptyRow5.Style.Add("font-size", "8pt")
            oLblEmptyRow5.Style.Add("text-align", "left")
            oLblEmptyRow5.Style.Add("font-weight", "bold")
            tblCell.Controls.Add(oLblEmptyRow5)
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("padding-left", "5px")
            tblCell.Style.Add("background", "#ffffff")
            tblCell.ColumnSpan = 2
            tblRow.Cells.Add(tblCell)

            tblEVA.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' Return to investments
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblReturnToInvestments As New RadLabel
            lblReturnToInvestments.ID = "lblReturnToInvestments"
            lblReturnToInvestments.Text = "a) Return to investments"
            lblReturnToInvestments.Style.Add("font-size", "8pt")
            lblReturnToInvestments.Style.Add("text-align", "left")
            lblReturnToInvestments.Style.Add("font-weight", "normal")
            tblCell.Controls.Add(lblReturnToInvestments)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("padding-left", "10px")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLinkReturnToInvestmentsValue As New HyperLink
            oLinkReturnToInvestmentsValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=ReturnToInves&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            oLinkReturnToInvestmentsValue.Style.Add("text-decoration", "none")
            oLinkReturnToInvestmentsValue.Style.Add("cursor", "pointer")
            oLinkReturnToInvestmentsValue.Text = Nn(GetVariableDefineValue("ReturnToInves", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
            oLinkReturnToInvestmentsValue.Style.Add("font-size", "8pt")
            oLinkReturnToInvestmentsValue.Style.Add("text-align", "right")
            oLinkReturnToInvestmentsValue.BackColor = Drawing.Color.Transparent

            tblCell.Controls.Add(oLinkReturnToInvestmentsValue)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblEVA.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' Cost of capital
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblCostOfCapital1 As New RadLabel
            lblCostOfCapital1.ID = "lblCostOfCapital1"
            lblCostOfCapital1.Text = "b) Cost of capital"
            lblCostOfCapital1.Style.Add("font-size", "8pt")
            lblCostOfCapital1.Style.Add("text-align", "left")
            lblCostOfCapital1.Style.Add("font-weight", "normal")
            tblCell.Controls.Add(lblCostOfCapital1)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("padding-left", "10px")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLinkCostOfCapital1Value As New HyperLink
            oLinkCostOfCapital1Value.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=CostOfCapital&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            oLinkCostOfCapital1Value.Style.Add("text-decoration", "none")
            oLinkCostOfCapital1Value.Style.Add("cursor", "pointer")
            oLinkCostOfCapital1Value.Text = Nn(GetVariableDefineValue("CostOfCapital", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
            oLinkCostOfCapital1Value.Style.Add("font-size", "8pt")
            oLinkCostOfCapital1Value.Style.Add("text-align", "right")
            oLinkCostOfCapital1Value.BackColor = Drawing.Color.Transparent

            tblCell.Controls.Add(oLinkCostOfCapital1Value)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblEVA.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

            ' EVA = a-b
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblEVAa_b As New RadLabel
            lblEVAa_b.ID = "lblEVAa_b"
            lblEVAa_b.Text = "EVA = a - b "
            lblEVAa_b.Style.Add("font-size", "8pt")
            lblEVAa_b.Style.Add("text-align", "left")
            lblEVAa_b.Style.Add("font-weight", "normal")
            tblCell.Controls.Add(lblEVAa_b)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("padding-left", "15px")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLinkEVAa_bValue As New HyperLink
            oLinkEVAa_bValue.Style.Add("text-decoration", "none")
            oLinkEVAa_bValue.Style.Add("cursor", "default")
            oLinkEVAa_bValue.Text = (-Nn(GetVariableDefineValue("CostOfCapital", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))) + Nn(GetVariableDefineValue("ReturnToInves", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame")))).ToString("N2")
            oLinkEVAa_bValue.Style.Add("font-size", "8pt")
            oLinkEVAa_bValue.Style.Add("text-align", "right")
            oLinkEVAa_bValue.BackColor = Drawing.Color.Transparent

            tblCell.Controls.Add(oLinkEVAa_bValue)
            tblCell.Style.Add("border-bottom", "1px solid #3e6c86")
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblEVA.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------

        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub ChiefExecutive_EVA_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        m_SessionData = Session("SessionData")

        m_SiteMaster = Me.Master

        m_SiteMaster.LoadPeriodsValid = Session("IDRole") = "P"

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oCBOPeriod As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOPeriodValid As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriodValid"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler m_SiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler m_SiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(Session("IDGame")) ' Ultimo periodo
        oCBOPeriod.SelectedValue = Session("IDPeriodValid")
        m_SiteMaster.PeriodChange = Session("IDPeriodValid")

        Session("NameTeam") = GetTeamName(Session("IDTeam"))
        oCBOTeam.SelectedValue = Session("IDTeam")
        m_SiteMaster.TeamChange = Session("IDTeam")

        ' Gestione del pannello delle decisioni concesse
        modalHelp.OpenerElementID = lnkHelp.ClientID
        modalHelp.Modal = False
        modalHelp.VisibleTitlebar = True

        LoadHelp()
    End Sub

    Private Sub ChiefExecutive_EVA_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        If Not Page.IsPostBack Then
            If Session("IDRole") = "P" Then
                m_SiteMaster.ReloadPeriodsCalculate()
            End If
            LoadData()

            btnTranslate.Visible = Session("IDRole").Contains("D")
            ' Controllo tutti gli oggetti presenti
            Dim oTranslation As New TranslationObjectForm(Me, g_DAL, Nz(Session("LanguageActive")))

        End If
    End Sub
    Private Sub btnTranslate_Click(sender As Object, e As EventArgs) Handles btnTranslate.Click
        Session("PageOrigin") = Me.ToString().Substring(4, Me.ToString().Substring(4).Length - 5) + ".aspx"
        Response.Redirect("PageTranslation.aspx")
        Response.End()
    End Sub

#Region "PANNELLO HELP"

    Private Sub LoadHelp()
        lblHelp.Text = "<p>WACC <br>
                        Weighted  Average Cost of Capital (WACC) = Debt cost as % of capital + equity cost as %  of capital</p>
                        <p>Cost of  Equity<br>
                          We suggest  that cost of equity in this environment is 10%</p>
                        <p>&nbsp;</p>
                        <p>Capital *  Spread<br>
                          A  performance metric that is equal to the difference between a company's weighted  average cost of capital (WACC) and its return on invested capital (ROIC)</p>
                        <p>Return On  Capital (%)<br>
                          ROI</p>
                        <p>&nbsp;</p>
                        <p>EVA<br>
                      Economic  Value Added</p>"
    End Sub

#End Region

    Private Sub CheckVariablesTranslations()
        ' Controllo la presenza delle variabili nella tabella delle variabili del gioco
        VerifiyVariableExists("UtilsEndPerio", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)
        VerifiyVariableExists("NetCapital", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)
        VerifiyVariableExists("Equity", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)
        VerifiyVariableExists("LoansEndPerio", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)
        VerifiyVariableExists("LTCapital", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)
        VerifiyVariableExists("CumulBeforTax", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)
        VerifiyVariableExists("CumulNegatInter", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)
        VerifiyVariableExists("TassePerioCumul", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)
        VerifiyVariableExists("OperaProfiAfterTax", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)
        VerifiyVariableExists("ReturOnCapital", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)
        VerifiyVariableExists("InterRateYear", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)
        VerifiyVariableExists("InterNetRate", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)
        VerifiyVariableExists("ActualCostOfDebt", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)
        VerifiyVariableExists("CostOfEquit", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)
        VerifiyVariableExists("WeighCostCapit", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)
        VerifiyVariableExists("EVA1", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)
        VerifiyVariableExists("ReturnToInves", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)
        VerifiyVariableExists("CostOfCapital", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)
    End Sub

End Class
