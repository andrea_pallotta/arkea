﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="ChiefExecutive_Home.aspx.vb" Inherits="ChiefExecutive_Home" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="cntMain" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:UpdatePanel ID="pnlMainUpdate" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <h2>
                <asp:Label ID="lblWelcome" runat="server" Text="Chief executive"></asp:Label>
            </h2>

            <div class="clr"></div>

            <h3>
                <asp:Label ID="lblYourOffice" runat="server" Text="Your office"></asp:Label>
            </h3>

            <div class="row">
                <div class="col-md-12">
                    <asp:Image ID="imgChiefOfficer" runat="server" ImageAlign="Middle" ImageUrl="~/Images/ChiefOfficer.jpg" />

                    <asp:ImageMap ID="ImageMap1" runat="server"></asp:ImageMap>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="clr"></div>

    <div class="row">
        <div class="divTableCell" style="text-align: right;">
            <telerik:radbutton id="btnTranslate" runat="server" text="Translate" enableajaxskinrendering="true" tooltip=""></telerik:radbutton>
        </div>
    </div>

</asp:Content>

