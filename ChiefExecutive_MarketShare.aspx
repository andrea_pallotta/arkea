﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="ChiefExecutive_MarketShare.aspx.vb" Inherits="ChiefExecutive_MarketShare" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="cntMain" ContentPlaceHolderID="MainContent" runat="Server" ClientIDMode="Static">

    <asp:UpdatePanel ID="pnlMain" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <h2>
                <asp:Label ID="lblWelcome" runat="server" Text="Chief executive"></asp:Label>
            </h2>

            <div class="clr"></div>

            <h3>
                <asp:Label ID="lblTitolo" runat="server" Text="Market share"></asp:Label>
            </h3>

            <div class="row">

                <telerik:RadHtmlChart ID="grfMarketShare" runat="server" Width="100%">
                    <ChartTitle Text=""></ChartTitle>

                    <PlotArea>
                        <Series>
                            <telerik:LineSeries Name="" DataFieldY="MarketShare">
                                <Appearance FillStyle-BackgroundColor="#B67C00" />
                                <LineAppearance LineStyle="Smooth" Width="3px" />
                                <TooltipsAppearance DataFormatString="{0:2N}" BackgroundColor="White" Color="#B67C00" />
                                <LabelsAppearance Visible="false" />
                            </telerik:LineSeries>
                        </Series>

                        <XAxis>
                        </XAxis>

                        <YAxis>
                            <LabelsAppearance DataFormatString="{0}" />
                        </YAxis>

                    </PlotArea>

                    <Legend>
                        <Appearance Visible="true" Position="Top" />
                    </Legend>
                </telerik:RadHtmlChart>
            </div>

            <div class="row" style="min-height: 30px;">
            </div>

            <div class="row">
                <asp:Table ClientIDMode="Static" ID="tblMarketShare" runat="server">
                </asp:Table>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel ID="pnlMessage" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row">
                <telerik:RadLabel runat="server" ID="lblMessage" Visible="false"></telerik:RadLabel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="clr"></div>

    <div class="row">
        <div class="divTableCell" style="text-align: right;">
            <telerik:RadButton ID="btnTranslate" runat="server" Text="Translate" EnableAjaxSkinRendering="true" ToolTip=""></telerik:RadButton>
        </div>
    </div>

</asp:Content>
