﻿Imports System.Data
Imports APS.APSUtility
Imports APPCore.Utility
Imports Telerik.Web.UI

Partial Class ChiefExecutive_MarketShare
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadData()
        lblMessage.Visible = False

        pnlMain.Update()
        pnlMessage.Update()
    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadData()

        lblMessage.Visible = False
        pnlMain.Update()
        pnlMessage.Update()
    End Sub

    Private Sub LoadData()
        LoadDataGraphMarketShare()
    End Sub

    Private Sub LoadDataGraphMarketShare()
        Dim sSQL As String
        Dim oDTTableResult As DataTable
        Dim oDTItems As DataTable = HandleGetItemsPeriodVariables(Session("IDGame"), Nz(Session("LanguageActive")))
        Dim sItemName As String = ""
        Dim sIDPeriod As String = ""

        grfMarketShare.PlotArea.Series.Clear()
        grfMarketShare.PlotArea.XAxis.Items.Clear()

        sSQL = "SELECT P.Descrizione AS Period, C.Value, V.VariableName, I.VariableLabel, I.IDVariable, C.IDItem, C.IDPeriod " _
             & "FROM Variables_Calculate_Define V " _
             & "INNER JOIN Variables_Calculate_Value C ON V.Id = C.IDVariable " _
             & "INNER JOIN BGOL_Periods P ON C.IDPeriod = P.Id " _
             & "INNER JOIN vItems I ON C.IDItem = I.IDVariable " _
             & "WHERE LTRIM(RTRIM(V.VariableName)) = 'MarketShare' AND C.IDPlayer = " & Nni(Session("IDTeam")) & " AND C.IDPeriod <= " & m_SiteMaster.PeriodGetCurrent
        If Session("IDRole").Contains("P") Then
            sSQL &= " AND ISNULL(C.Simulation, 0) = 0 "
        End If
        oDTTableResult = g_DAL.ExecuteDataTable(sSQL)

        ' Recupero tutti i periodi generati fino adesso
        sSQL = "SELECT * FROM BGOL_Games_Periods BGP " _
             & "INNER JOIN BGOL_Periods BP ON BGP.IDPeriodo = BP.Id " _
             & "WHERE BGP.IDGame = " & Session("IDGame") & " AND BP.NumStep <= " & Session("CurrentStep")
        If Session("IDRole").Contains("P") Then
            sSQL &= " AND ISNULL(BGP.Simulation, 0) = 0 "
        End If
        sSQL &= " ORDER BY BP.NumStep "
        Dim oDTPeriodi As DataTable = g_DAL.ExecuteDataTable(sSQL)

        Dim oSeriesData As LineSeries
        For Each oRowItm As DataRow In oDTItems.Rows
            oSeriesData = New LineSeries
            For Each oRowPeriod As DataRow In oDTPeriodi.Rows
                sItemName = oRowItm("VariableName").ToString

                Dim oDRValue As DataRow

                oDRValue = oDTTableResult.Select("IDperiod = " & oRowPeriod("IdPeriodo") & " AND IDItem = " & oRowItm("IdVariable")).FirstOrDefault

                If Not IsNothing(oDRValue) Then
                    Dim oItem As New SeriesItem
                    oItem.YValue = Nn(oDRValue("Value")).ToString("N2")
                    oItem.TooltipValue = Nz(oRowItm("VariableName"))
                    oSeriesData.Items.Add(oItem)
                    sIDPeriod &= Nz(oRowPeriod("IDPeriodo")) & ";"
                End If
            Next

            If sIDPeriod.EndsWith(";") Then sIDPeriod.TrimEnd(";")

            grfMarketShare.PlotArea.Series.Add(oSeriesData)
            oSeriesData.Name = sItemName
            oSeriesData.VisibleInLegend = True
            oSeriesData.LabelsAppearance.Visible = True
        Next

        For Each oRowPeriod As DataRow In oDTPeriodi.Rows
            If sIDPeriod.Contains(oRowPeriod("IdPeriodo")) Then
                Dim newAxisItem As New AxisItem(Nz(oRowPeriod("Descrizione")))
                grfMarketShare.PlotArea.XAxis.Items.Add(newAxisItem)
            End If
        Next

        grfMarketShare.PlotArea.XAxis.MinorGridLines.Visible = False
        grfMarketShare.PlotArea.YAxis.MinorGridLines.Visible = False
        grfMarketShare.Legend.Appearance.Visible = True
        grfMarketShare.Legend.Appearance.Position = HtmlChart.ChartLegendPosition.Top

        LoadDataMarketShare()

    End Sub

    Private Sub LoadDataMarketShare()
        Dim oDTItems As DataTable = HandleGetItemsPeriodVariables(Session("IDGame"), Nz(Session("LanguageActive")))

        Dim oDTPlayers As DataTable = LoadTeamsGame(Session("IDGame"))

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Try
            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header vuota
            tblCell = New TableCell
            Dim lblTextHeader As New RadLabel
            lblTextHeader.ID = "lblTextHeader"
            lblTextHeader.Text = ""
            lblTextHeader.Style.Add("color", "#ffffff")
            tblCell.Controls.Add(lblTextHeader)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.Style.Add("background", "Transparent")
            tblHeaderCell.Style.Add("color", "#ffffff")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            ' Recupero gli Items per la costruzione delle colonne
            For Each oRowItm As DataRow In oDTItems.Rows
                ' Cella del prezzo di vendita
                tblHeaderCell = New TableHeaderCell
                tblHeaderCell.Text = Nz(oRowItm("VariableName"))
                tblHeaderCell.BorderStyle = BorderStyle.None
                tblHeaderRow.Cells.Add(tblHeaderCell)
            Next
            tblHeaderRow.BorderStyle = BorderStyle.None
            tblHeaderRow.Style.Add("margin-top", "10px")

            tblMarketShare.Rows.Add(tblHeaderRow)

            ' Ciclo sui plaer/Item per recuperare l'informazione corretta
            For Each oRowPLayer As DataRow In oDTPlayers.Rows
                tblRow = New TableRow
                tblCell = New TableCell

                tblCell.Text = Nz(oRowPLayer("TeamName"))
                tblRow.Cells.Add(tblCell)
                For Each oRowItm As DataRow In oDTItems.Rows
                    ' Cella del prezzo di vendita
                    tblCell = New TableCell
                    tblCell.Text = Nn(GetVariableDefineValue("MarketShare", Session("IDTeam"), Nni(oRowItm("IDVariable")), 0, m_SiteMaster.PeriodGetPrev, Session("IDGame"))).ToString("N1")
                    tblCell.Style.Add("text-align", "right")
                    tblCell.Style.Add("padding-right", "10px")
                    tblCell.BorderStyle = BorderStyle.None
                    tblRow.Cells.Add(tblCell)
                Next
                tblMarketShare.Rows.Add(tblRow)
            Next

        Catch ex As Exception
            lblMessage.Text = ex.Message
            lblMessage.Visible = True
        End Try

    End Sub

    Private Sub ChiefExecutive_MarketShare_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        m_SessionData = Session("SessionData")

        m_SiteMaster = Me.Master

        m_SiteMaster.LoadPeriodsValid = Session("IDRole") = "P"

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oCBOPeriod As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOPeriodValid As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriodValid"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler m_SiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler m_SiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(Session("IDGame")) ' Ultimo periodo
        oCBOPeriod.SelectedValue = Session("IDPeriodValid")
        m_SiteMaster.PeriodChange = Session("IDPeriodValid")

        Session("NameTeam") = GetTeamName(Session("IDTeam"))
        oCBOTeam.SelectedValue = Session("IDTeam")
        m_SiteMaster.TeamChange = Session("IDTeam")

        SQLConnClose(g_DAL.GetConnObject)
    End Sub

    Private Sub ChiefExecutive_MarketShare_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        If Not Page.IsPostBack Then
            If Session("IDRole") = "P" Then
                m_SiteMaster.ReloadPeriodsCalculate()
            End If
            LoadData()

            btnTranslate.Visible = Session("IDRole").Contains("D")

        End If

        ' Controllo tutti gli oggetti presenti
        Dim oTranslation As New TranslationObjectForm(Me, g_DAL, Nz(Session("LanguageActive")))

    End Sub
    Private Sub btnTranslate_Click(sender As Object, e As EventArgs) Handles btnTranslate.Click
        Session("PageOrigin") = Me.ToString().Substring(4, Me.ToString().Substring(4).Length - 5) + ".aspx"
        Response.Redirect("PageTranslation.aspx")
        Response.End()
    End Sub

End Class
