﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="ChiefExecutive_PUV_Player.aspx.vb" Inherits="ChiefExecutive_PUV_Player" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.1.Web, Version=16.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="cntMain" ContentPlaceHolderID="MainContent" runat="Server" ClientIDMode="Static">

    <asp:UpdatePanel ID="UpdatePanelMain" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <h2>
                <asp:Label ID="lblWelcome" runat="server" Text="Chief executive"></asp:Label>
            </h2>

            <div class="clr"></div>

            <h3>
                <asp:Label ID="lblTitolo" runat="server" Text="PUV"></asp:Label>
            </h3>

            <div class="divTableRow">

                <div class="divTableCell" style="width: 341px;">
                    <telerik:RadComboBox ID="cboItems" runat="server" Width="50%" EmptyMessage="< Select item... >"
                        RenderMode="Lightweight" RenderingMode="Full" AutoPostBack="true">
                    </telerik:RadComboBox>
                </div>

            </div>

            <div class="clr"></div>

            <div class="divTableRow">

                <div class="divTableCell">
                    <asp:Table ClientIDMode="Static" ID="tblPUV" runat="server" Style="width: 682px;">
                    </asp:Table>
                </div>

            </div>

            <div class="clr"></div>

            <div class="divTableRow">
                <telerik:RadHtmlChart ID="grfPUV" runat="server" Width="682px" Height="350px" Visible="false">
                    <ChartTitle Text="PUV"></ChartTitle>

                    <PlotArea>

                        <XAxis AxisCrossingValue="0" MinValue="0" MaxValue="280" Step="50">
                            <MajorGridLines Width="1"></MajorGridLines>
                            <MinorGridLines Width="1"></MinorGridLines>
                            <LabelsAppearance DataFormatString="{2:N0}" RotationAngle="0" Skip="0" Step="1">
                            </LabelsAppearance>

                        </XAxis>

                        <YAxis AxisCrossingValue="50" MaxValue="80" MinValue="50" Step="10">
                            <LabelsAppearance RotationAngle="0" Skip="0" Step="1">
                            </LabelsAppearance>
                            <MajorGridLines Width="1"></MajorGridLines>
                            <MinorGridLines Width="1"></MinorGridLines>

                        </YAxis>

                    </PlotArea>

                </telerik:RadHtmlChart>
            </div>

            <div class="clr"></div>

            <div class="divTableRow" style="margin-top: 30px;">
                <dxchartsui:WebChartControl ID="ccPUV" runat="server" Width="682px" Height="450px" CrosshairEnabled="True">
                    <ClientSideEvents CustomDrawCrosshair="function(s, e) {
                        for (var i=0;i&lt;e.crosshairElements.length;i++){
                            e.crosshairElements[i].LabelElement.font.fontSize = 9;
                            }
                        }"></ClientSideEvents>

                </dxchartsui:WebChartControl>
            </div>

            <div class="clr"></div>

            <div class="divTableRow">
                <asp:PlaceHolder runat="server" ID="Message" Visible="false">
                    <p class="text-danger">
                        <asp:Literal runat="server" ID="MessageText" />
                    </p>
                </asp:PlaceHolder>
            </div>

            <div class="clr"></div>

            <div class="divTableRow" style="margin-top: 50px;">
                <div class="divTableCell" style="text-align: right;">
                    <telerik:RadButton ID="btnTranslate" runat="server" Text="Translate" EnableAjaxSkinRendering="true" ToolTip=""></telerik:RadButton>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
