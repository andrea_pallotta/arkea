﻿Imports System.Data
Imports APS
Imports APPCore.Utility
Imports DevExpress.XtraCharts
Imports Telerik.Web.UI
Imports DevExpress.Data

Partial Class ChiefExecutive_PUV_Player
    Inherits System.Web.UI.Page

    Private m_SiteMaster As SiteMaster

    Private m_SessionData As MU_SessionData

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        tblPUV.Rows.Clear()

        ccPUV.ClearSelection()
        grfPUV.Controls.Clear()

        LoadData()
        UpdatePanelMain.Update()
    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        tblPUV.Rows.Clear()
        LoadData()
        UpdatePanelMain.Update()
    End Sub

    Private Sub ChiefExecutive_PUV_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        Message.Visible = False

        If Not Page.IsPostBack Then
            If Session("CurrentStep") >= 2 Then
                cboItems.Visible = True
                If Session("IDRole") = "P" Then
                    m_SiteMaster.ReloadPeriodsCalculate()
                End If

            Else
                cboItems.Visible = False
                MessageText.Text = "No data vailable before this period"
                Message.Visible = True
            End If

            btnTranslate.Visible = Session("IDRole").Contains("D")

        End If

        ' Controllo tutti gli oggetti presenti
        Dim oTranslation As New TranslationObjectForm(Me, g_DAL, Nz(Session("LanguageActive")))
    End Sub

    Private Sub LoadItems()
        Dim oDTItems As DataTable
        Try
            cboItems.Items.Clear()
            cboItems.DataSource = Nothing
            oDTItems = LoadItemsGame(Session("IDgame"), Nz(Session("LanguageActive")))
            cboItems.DataSource = oDTItems
            cboItems.DataValueField = "IdVariable"
            cboItems.DataTextField = "VariableName"
            cboItems.DataBind()
            cboItems.SelectedIndex = 0

        Catch ex As Exception
            Throw New ApplicationException("DataChart.aspx -> LoadItems", ex)
        End Try
    End Sub

    Private Sub LoadData()
        ' Prima di tutto controllo che l'attuale periodo - 3 abbia dei valori, devo controllare l'ultimo quarto
        ' se non ci sono periodi utili, ad esempio sono 2 non faccio vedere niente, se sono al 3 posso iniziare a far vedere qualcosa
        Dim sSQL As String

        Dim oDTPlayers As DataTable = LoadTeamsGame(Session("IDGame"))

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell
        Dim iIDCurrentStepMeno3 As Integer

        Dim QualiGoodsOffer As Double
        Dim CompoMarkeQuali As Double
        Dim ImporCompoQuali As Double
        Dim IndexOfAttraction As Double
        Dim ImporCompoAttra As Double

        Try
            ' Recupero l'ID del periodo attuale -2, o meglio CurrentStep -2
            sSQL = "SELECT ID FROM BGOL_Periods WHERE IDGame = " & Session("IDGame") & " AND NumStep = " & Session("CurrentStep") - 2
            iIDCurrentStepMeno3 = g_DAL.ExecuteScalar(sSQL)

            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header vuota
            tblHeaderCell.Text = ""
            tblHeaderCell.Style.Add("background", "Transparent")
            tblHeaderCell.Style.Add("width", "372px")
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderRow.Cells.Add(tblHeaderCell)

            ' Cella del prezzodi vendita
            tblHeaderCell = New TableHeaderCell
            tblHeaderCell.Text = "Value (€)"
            tblHeaderCell.Style.Add("width", "145px")
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderRow.Cells.Add(tblHeaderCell)

            ' Cella del prezzo di vendita
            tblHeaderCell = New TableHeaderCell
            tblHeaderCell.Text = "Price (€)"
            tblHeaderCell.Style.Add("width", "145px")
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.Style.Add("margin-left", "20px")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            tblHeaderRow.BorderStyle = BorderStyle.None
            tblHeaderRow.Style.Add("margin-top", "10px")

            tblPUV.Rows.Add(tblHeaderRow)

            ' Inizio a ciclare sui player per recuperare i valori che mi servono al popolamento della tabella
            For Each oRowP As DataRow In oDTPlayers.Rows
                ' Etichetta della variabile
                tblRow = New TableRow

                ' Setto il nome del player/team
                tblCell = New TableCell
                tblCell.Text = oRowP("TeamName")
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)

                ' Setto il valore del valore
                tblCell = New TableCell

                Dim oLblValue As New RadLabel
                QualiGoodsOffer = GetVariableDefineValue("QualiGoodsOffer", oRowP("ID"), Nni(cboItems.SelectedValue), 0, iIDCurrentStepMeno3, Session("IDGame"))
                CompoMarkeQuali = GetVariableDefineValue("CompoMarkeQuali", 0, Nni(cboItems.SelectedValue), 0, iIDCurrentStepMeno3, Session("IDGame"))
                ImporCompoQuali = GetVariableBoss("ImporCompoQuali", 0, iIDCurrentStepMeno3, "", Session("IDGame"))
                IndexOfAttraction = GetVariableDefineValue("IndexOfAttraction", oRowP("ID"), 0, 0, iIDCurrentStepMeno3, Session("IDGame"))
                ImporCompoAttra = GetVariableBoss("ImporCompoAttra", 0, iIDCurrentStepMeno3, "", Session("IDGame"))

                tblCell.Text = Nn(((QualiGoodsOffer / CompoMarkeQuali * ImporCompoQuali) + (IndexOfAttraction * ImporCompoAttra)) / (ImporCompoQuali + ImporCompoAttra)).ToString("N2")
                tblCell.BorderStyle = BorderStyle.None
                tblCell.Style.Add("text-align", "right")
                tblRow.Cells.Add(tblCell)

                ' Setto il valore del prezzo
                tblCell = New TableCell
                Dim oLblPrice As New RadLabel

                tblCell.Text = Nn(Nn(GetVariableDefineValue("DiscoPriceOffer", oRowP("ID"), Nni(cboItems.SelectedValue), 0, iIDCurrentStepMeno3, Session("IDGame"))) _
                               / Nn(GetVariableDefineValue("CompoDiscoPrice", 0, Nni(cboItems.SelectedValue), 0, iIDCurrentStepMeno3, Session("IDGame")))).ToString("N2")

                tblCell.Style.Add("text-align", "right")
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)

                tblPUV.Rows.Add(tblRow)

                ' Riga vuota
                tblRow = New TableRow
                tblCell = New TableCell
                tblCell.ColumnSpan = 4
                tblCell.Text = "<hr/>"
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)
                tblPUV.Rows.Add(tblRow)
            Next

            LoadDataGraphPUV()

        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub LoadDataGraphPUV()
        Dim oDTPlayers As DataTable = LoadTeamsGame(Session("IDGame"))
        Dim oDTItems As DataTable = LoadItemsGame(Session("IDGame"), Nz(Session("LanguageActive")))
        Dim xValue As Decimal
        Dim yValue As Decimal

        Try
            grfPUV.PlotArea.Series.Clear()
            grfPUV.PlotArea.XAxis.Items.Clear()

            ccPUV.ClearSelection()
            ccPUV.Series.Clear()

            ' Costruisco il grafico
            Dim oSeriesData As ScatterSeries
            Dim oItem As ScatterSeriesItem

            Dim iContaPlayer As Integer = 1

            ' Avvio la costruzione delle prime serie
            For Each drPlayer As DataRow In oDTPlayers.Rows
                oSeriesData = New ScatterSeries
                oItem = New ScatterSeriesItem

                xValue = Nn(GetVariableDefineValue("DiscoPriceOffer", drPlayer("Id"), Nni(cboItems.SelectedValue), 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
                yValue = Nn(GetVariableDefineValue("MappaPosizPUV", drPlayer("Id"), Nni(cboItems.SelectedValue), 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
                oSeriesData.SeriesItems.Add(xValue, yValue)
                oSeriesData.VisibleInLegend = True
                oSeriesData.Name = Nz(drPlayer("TeamName")) & " - Q0"
                oSeriesData.MarkersAppearance.MarkersType = HtmlChart.MarkersType.Triangle
                oSeriesData.MarkersAppearance.Size = 10

                grfPUV.PlotArea.Series.Add(oSeriesData)

                LoadSeriesDev(oSeriesData.Name, xValue, yValue, iContaPlayer, Nz(drPlayer("Color")))

                ' Seconda serie
                oSeriesData = New ScatterSeries
                xValue = Nn(GetVariableDefineValue("DiscoPriceOffer", drPlayer("Id"), Nni(cboItems.SelectedValue), 0, m_SiteMaster.PeriodGetPrev, Session("IDGame"))).ToString("N2")
                yValue = Nn(GetVariableDefineValue("MappaPosizPUV", drPlayer("Id"), Nni(cboItems.SelectedValue), 0, m_SiteMaster.PeriodGetPrev, Session("IDGame"))).ToString("N2")
                oSeriesData.SeriesItems.Add(xValue, yValue)

                oSeriesData.VisibleInLegend = True
                oSeriesData.Name = Nz(drPlayer("TeamName")) & " - Q1"
                oSeriesData.MarkersAppearance.MarkersType = HtmlChart.MarkersType.Cross
                oSeriesData.MarkersAppearance.Size = 10

                grfPUV.PlotArea.Series.Add(oSeriesData)

                LoadSeriesDev(oSeriesData.Name, xValue, yValue, iContaPlayer, Nz(drPlayer("Color")))

                ' terza serie
                oSeriesData = New ScatterSeries
                xValue = Nn(GetVariableDefineValue("DiscoPriceOffer", drPlayer("Id"), Nni(cboItems.SelectedValue), 0, m_SiteMaster.PeriodGetMeno2, Session("IDGame"))).ToString("N2")
                yValue = Nn(GetVariableDefineValue("MappaPosizPUV", drPlayer("Id"), Nni(cboItems.SelectedValue), 0, m_SiteMaster.PeriodGetMeno2, Session("IDGame"))).ToString("N2")
                oSeriesData.SeriesItems.Add(xValue, yValue)

                oSeriesData.VisibleInLegend = True
                oSeriesData.Name = Nz(drPlayer("TeamName")) & " - Q2"
                oSeriesData.MarkersAppearance.MarkersType = HtmlChart.MarkersType.Circle
                oSeriesData.MarkersAppearance.Size = 10

                grfPUV.PlotArea.Series.Add(oSeriesData)

                LoadSeriesDev(oSeriesData.Name, xValue, yValue, iContaPlayer, Nz(drPlayer("Color")))

                iContaPlayer += 1
            Next

            ' Aggiungo le serie dei prezzi 

            ' Average
            '---------------------------------------------------
            oSeriesData = New ScatterSeries
            xValue = Nn(GetVariableDefineValue("CompoDiscoPrice", 0, Nni(cboItems.SelectedValue), 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
            yValue = Nn(GetVariableDefineValue("CompoMarkeQuali", 0, Nni(cboItems.SelectedValue), 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
            oSeriesData.SeriesItems.Add(xValue, yValue)

            oSeriesData.VisibleInLegend = True
            oSeriesData.Name = "Avg. - Q0"
            oSeriesData.MarkersAppearance.MarkersType = HtmlChart.MarkersType.Square
            grfPUV.PlotArea.Series.Add(oSeriesData)

            LoadSeriesDev(oSeriesData.Name, xValue, yValue, DevExpress.XtraCharts.MarkerKind.Cross, "#ff4500")

            '---------------------------------------------------
            oSeriesData = New ScatterSeries
            xValue = Nn(GetVariableDefineValue("CompoDiscoPrice", 0, Nni(cboItems.SelectedValue), 0, m_SiteMaster.PeriodGetMeno2, Session("IDGame"))).ToString("N2")
            yValue = Nn(GetVariableDefineValue("CompoMarkeQuali", 0, Nni(cboItems.SelectedValue), 0, m_SiteMaster.PeriodGetMeno2, Session("IDGame"))).ToString("N2")
            oSeriesData.SeriesItems.Add(xValue, yValue)

            oSeriesData.VisibleInLegend = True
            oSeriesData.Name = "Avg. - Q1"
            oSeriesData.MarkersAppearance.MarkersType = HtmlChart.MarkersType.Square
            grfPUV.PlotArea.Series.Add(oSeriesData)

            LoadSeriesDev(oSeriesData.Name, xValue, yValue, DevExpress.XtraCharts.MarkerKind.Cross, "#ff4500")

            '---------------------------------------------------
            oSeriesData = New ScatterSeries
            xValue = Nn(GetVariableDefineValue("CompoDiscoPrice", 0, Nni(cboItems.SelectedValue), 0, m_SiteMaster.PeriodGetMeno2, Session("IDGame"))).ToString("N2")
            yValue = Nn(GetVariableDefineValue("CompoMarkeQuali", 0, Nni(cboItems.SelectedValue), 0, m_SiteMaster.PeriodGetMeno2, Session("IDGame"))).ToString("N2")
            oSeriesData.SeriesItems.Add(xValue, yValue)

            oSeriesData.VisibleInLegend = True
            oSeriesData.Name = "Avg. - Q2"
            oSeriesData.MarkersAppearance.MarkersType = HtmlChart.MarkersType.Square
            grfPUV.PlotArea.Series.Add(oSeriesData)

            LoadSeriesDev(oSeriesData.Name, xValue, yValue, DevExpress.XtraCharts.MarkerKind.Cross, "#ff4500")

            grfPUV.Legend.Appearance.Position = HtmlChart.ChartLegendPosition.Bottom
            grfPUV.Legend.Appearance.Orientation = HtmlChart.ChartLegendOrientation.Horizontal
            grfPUV.Legend.Appearance.Align = HtmlChart.ChartLegendAlign.Start

            ccPUV.Legend.Direction = DevExpress.XtraCharts.LegendDirection.TopToBottom
            ccPUV.Legend.AlignmentHorizontal = DevExpress.XtraCharts.LegendAlignmentHorizontal.LeftOutside
            ccPUV.Legend.AlignmentVertical = DevExpress.XtraCharts.LegendAlignmentVertical.Center
        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub LoadSeriesDev(NomeSerie As String, xValue As Double, yValue As Double, Marker As DevExpress.XtraCharts.MarkerKind, Color As String)
        Dim oSeries As New DevExpress.XtraCharts.Series(NomeSerie, DevExpress.XtraCharts.ViewType.ScatterLine)

        oSeries.Points.Add(New DevExpress.XtraCharts.SeriesPoint(xValue, yValue))

        oSeries.ShowInLegend = True
        oSeries.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Auto
        oSeries.CheckedInLegend = True

        Dim view As DevExpress.XtraCharts.ScatterLineSeriesView = CType(oSeries.View, DevExpress.XtraCharts.ScatterLineSeriesView)
        view.PointMarkerOptions.Kind = Marker
        view.PointMarkerOptions.Size = 10
        view.Color = System.Drawing.ColorTranslator.FromHtml(Color)

        view.EnableAntialiasing = DevExpress.Utils.DefaultBoolean.Default

        ccPUV.Series.Add(oSeries)

    End Sub

    Private Sub ChiefExecutive_PUV_Player_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        m_SessionData = Session("SessionData")

        m_SiteMaster = Me.Master

        m_SiteMaster.LoadPeriodsValid = Session("IDRole") = "P"

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oCBOPeriod As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOPeriodValid As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriodValid"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler m_SiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler m_SiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(Session("IDGame")) ' Ultimo periodo
        oCBOPeriod.SelectedValue = Session("IDPeriodValid")
        m_SiteMaster.PeriodChange = Session("IDPeriodValid")

        Session("NameTeam") = GetTeamName(Session("IDTeam"))
        oCBOTeam.SelectedValue = Session("IDTeam")
        m_SiteMaster.TeamChange = Session("IDTeam")

        LoadItems()

        LoadData()

    End Sub

    Private Sub cboItems_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles cboItems.SelectedIndexChanged
        tblPUV.Rows.Clear()
        LoadData()
        UpdatePanelMain.Update()
    End Sub
    Private Sub btnTranslate_Click(sender As Object, e As EventArgs) Handles btnTranslate.Click
        Session("PageOrigin") = Me.ToString().Substring(4, Me.ToString().Substring(4).Length - 5) + ".aspx"
        Response.Redirect("PageTranslation.aspx")
        Response.End()
    End Sub

End Class
