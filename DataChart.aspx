﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="DataChart.aspx.vb" Inherits="DataChart" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><%: Page.Title %>- ARKEA</title>

    <link href="Content/Site.css" rel="stylesheet" type="text/css" />
    <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />

    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />

</head>

<body>
    <form id="frmMain" runat="server">
        <asp:ScriptManager runat="server" AsyncPostBackTimeout="200000">
            <Scripts>
                <%--To learn more about bundling scripts in ScriptManager see http://go.microsoft.com/fwlink/?LinkID=301884 --%>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="jquery" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="respond" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>

        <div class="mainChart">

            <!-- MAIN START -->
            <div class="mainbar">
                <div class="article">
                    <div style="min-height: 25px;" />

                    <!-- CONTENT MAIN START -->
                    <div class="divTable">
                        <div class="divTableRow">
                            <div class="divTableCell">
                                <h2>
                                    <asp:Label ID="lblWelcome" runat="server" Text="Chart"></asp:Label>
                                </h2>
                            </div>
                        </div>
                    </div>

                    <div class="divTable">
                        <div class="divTableRow">
                            <div class="divTableCell">
                                <h3>
                                    <asp:Label ID="lblTitolo" runat="server" Text=""></asp:Label>
                                </h3>
                            </div>
                        </div>
                    </div>

                    <div class="divTable">
                        <div class="divTableRow">

                            <div class="divTableCell">
                                <telerik:RadComboBox ID="cboTeam" runat="server" Width="50%" EmptyMessage="< Select team...>"
                                    RenderMode="Lightweight" RenderingMode="Full" AutoPostBack="true">
                                </telerik:RadComboBox>
                            </div>
                            <div class="divTableCell">
                                <telerik:RadComboBox ID="cboItems" runat="server" Width="50%" EmptyMessage="< Select item...>"
                                    RenderMode="Lightweight" RenderingMode="Full" AutoPostBack="true">
                                </telerik:RadComboBox>
                            </div>
                        </div>

                        <div class="divTableRow">
                            <div class="divTableCell">
                                <telerik:RadCheckBox runat="server" ID="chkChangeGraph" ClientIDMode="Static" Text="Use column" AutoPostBack="true">
                                </telerik:RadCheckBox>
                            </div>
                        </div>

                    </div>

                    <!-- GESTIONE DEI PLAYER E DEGLI EVENTUALI ITEM E DEI PERIODI -->
                    <div id="ChartRow" style="width: 1000px;">
                        <telerik:RadHtmlChart ID="grfGeneric" runat="server" Width="990px">
                            <ChartTitle Text=""></ChartTitle>

                            <PlotArea>
                                <Series>
                                    <telerik:LineSeries Name="VariableName" DataFieldY="Value">
                                        <Appearance FillStyle-BackgroundColor="#B67C00" />
                                        <LineAppearance LineStyle="Smooth" Width="3px" />
                                        <TooltipsAppearance DataFormatString="{0:###.##0}" BackgroundColor="White" Color="#B67C00" />
                                        <LabelsAppearance Visible="false" />
                                    </telerik:LineSeries>
                                </Series>

                                <XAxis>
                                </XAxis>

                                <YAxis>
                                    <LabelsAppearance DataFormatString="N" />
                                </YAxis>

                            </PlotArea>

                            <Legend>
                                <Appearance Visible="true" Position="Top" />
                            </Legend>
                        </telerik:RadHtmlChart>
                    </div>

                    <div id="ChartRowColumn" style="width: 1000px;">
                        <telerik:RadHtmlChart ID="grfColumn" runat="server" Width="990px">
                            <ChartTitle Text=""></ChartTitle>

                            <PlotArea>
                                <Series>
                                    <telerik:ColumnSeries Name="VariableName" DataFieldY="Value">
                                        <Appearance FillStyle-BackgroundColor="#B67C00" />
                                        <TooltipsAppearance DataFormatString="{0:###.##0}" BackgroundColor="White" Color="#B67C00" />
                                        <LabelsAppearance Visible="false" />
                                    </telerik:ColumnSeries>
                                </Series>

                                <XAxis>
                                </XAxis>

                                <YAxis>
                                    <LabelsAppearance DataFormatString="N" />
                                </YAxis>

                            </PlotArea>

                            <Legend>
                                <Appearance Visible="true" Position="Top" />
                            </Legend>
                        </telerik:RadHtmlChart>
                    </div>

                    <!-- CONTENT MAIN STOP -->

                </div>
            </div>

            <div class="clr"></div>

            <div class="row">
                <asp:PlaceHolder runat="server" ID="Message" Visible="false">
                    <p class="text-danger">
                        <asp:Literal runat="server" ID="MessageText" />
                    </p>
                </asp:PlaceHolder>
                <asp:HiddenField ID="hfNomeVariabile" runat="server" ClientIDMode="Static" />
                <asp:HiddenField ID="hfIDItem" runat="server" ClientIDMode="Static" />
                <asp:HiddenField ID="hfIDAge" runat="server" ClientIDMode="Static" />
                <asp:HiddenField ID="hfPageTitle" runat="server" ClientIDMode="Static" />
            </div>
            <!-- MAIN STOP -->



            <%-- </div>

            </div>--%>
        </div>

    </form>
</body>
</html>
