﻿Imports System.Data
Imports APS.APSUtility
Imports APPCore.Utility
Imports Telerik.Web.UI
Imports DALC4NET

Partial Class DataChart
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData

    Private m_ItemsTable As DataTable
    Private m_TeamsTable As DataTable

    Private Sub LoadTeams()
        Dim oDTTeams As DataTable

        Try
            Message.Visible = False

            cboTeam.Items.Clear()
            cboTeam.DataSource = Nothing
            oDTTeams = LoadTeamsGame(Nni(Session("IDGame")))
            cboTeam.DataSource = oDTTeams
            cboTeam.DataValueField = "Id"
            cboTeam.DataTextField = "TeamName"
            cboTeam.DataBind()

            If Nni(Session("IDTeamMaster")) <> 0 Then
                cboTeam.SelectedValue = Session("IDTeamMaster")
            Else
                cboTeam.SelectedValue = Session("IDTeam")
            End If

            If Nni(cboTeam.SelectedValue) <> 0 Then
                ViewState("IDTeam") = Nni(cboTeam.SelectedValue)
                ViewState("NameTeam") = Nz(cboTeam.Text)
            End If

            If Session("IDRole").Contains("P") Then
                cboTeam.Enabled = False
            Else
                cboTeam.Enabled = True
            End If

            cboTeam.Items.Insert(0, New RadComboBoxItem("All players", String.Empty))

        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub LoadItems()
        Dim oDTItems As DataTable
        Try
            cboItems.Items.Clear()
            cboItems.DataSource = Nothing
            oDTItems = LoadItemsGame(Session("IDgame"), Nz(Session("LanguageActive")))
            cboItems.DataSource = oDTItems
            cboItems.DataValueField = "IdVariable"
            cboItems.DataTextField = "VariableName"
            cboItems.DataBind()
            If hfIDItem.Value <> "" AndAlso Nni(hfIDItem.Value, 0) > 0 Then
                cboItems.SelectedValue = Nni(hfIDItem.Value)
            Else
                cboItems.SelectedValue = ""
            End If

            cboItems.Items.Insert(0, New RadComboBoxItem("All items", String.Empty))

        Catch ex As Exception
            Throw New ApplicationException("DataChart.aspx -> LoadItems", ex)
        End Try
    End Sub

    Private Sub DataChart_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            m_SessionData = Session("SessionData")

            ' Recupero i parametri standard definiti
            ' NV = Nome variabile
            ' II = ID Item
            ' IA = ID Age
            ' TP = Titolo pagina
            hfNomeVariabile.Value = Nz(Request.QueryString("VN"))
            hfIDItem.Value = Nni(Request.QueryString("II"), 0)
            hfIDAge.Value = Nni(Request.QueryString("IA"))
            hfPageTitle.Value = Nz(Request.QueryString("TP"))

            LoadTeams()
            LoadItems()

            If hfIDItem.Value <> "" AndAlso Nni(hfIDItem.Value) > 0 Then
                cboItems.SelectedValue = Nni(hfIDItem.Value)
            End If

            ' Forzo il caricamento del team che ha aperto la pagina, il passaggio della sola variabile di sessione mi ha creato qualche problema
            ' nell'apertura della pagina
            cboTeam.SelectedValue = m_SessionData.Utente.IDTeam
            ViewState("IDTeam") = m_SessionData.Utente.IDTeam

            LoadDataGraph()

            ' Con il nome della variabile vado a recuperare la traduzione e/o l'eventuale titolo della pagina, se non trovo niente uso il nome stesso della variabile
            lblWelcome.Text = GetVariableTranslation(Session("IDGame"), hfNomeVariabile.Value, Nz(Session("LanguageActive")))

            grfColumn.Visible = False
        End If
    End Sub

    Private Sub LoadDataGraph()
        Dim sSQL As String
        Dim oDTVariable As DataTable

        Dim bPlayer As Boolean
        Dim bItem As Boolean
        Dim bAge As Boolean

        Dim bEnabledPlayer As Boolean = False
        Dim bEnabledItems As Boolean = False

        Dim bVariableBoss As Boolean
        Dim bVariableState As Boolean
        Dim bVariableDecisionsPlayer As Boolean
        Dim bVariableDecisionsDeveloper As Boolean
        Dim bVariableCalculate As Boolean

        Dim oDAL As New dbhelper

        Try
            m_SessionData = Session("SessionData")

            ' Controllo il tipo di variabile che mi viene passata
            ' VARIABILE DEL BOSS
            sSQL = "SELECT ID FROM Decisions_Boss WHERE Decision = '" & hfNomeVariabile.Value & "' AND IDGame = " & Session("IDGame")
            bVariableBoss = Nni(oDAL.ExecuteScalar(sSQL)) > 0

            ' VARIABILE DI STATO
            sSQL = "SELECT ID FROM Variables_State WHERE Name = '" & hfNomeVariabile.Value & "' AND IDGame = " & Session("IDGame")
            bVariableState = Nni(oDAL.ExecuteScalar(sSQL))

            ' VARIABILE DEL PLAYER
            sSQL = "SELECT ID FROM Decisions_Players WHERE VariableName = '" & hfNomeVariabile.Value & "' AND IDGame = " & Session("IDGame")
            bVariableDecisionsPlayer = Nni(oDAL.ExecuteScalar(sSQL))

            ' VARIBILE DEVELOPER
            sSQL = "SELECT ID FROM Decisions_Developer WHERE VariableName = '" & hfNomeVariabile.Value & "' AND IDGame = " & Session("IDGame")
            bVariableDecisionsDeveloper = Nni(oDAL.ExecuteScalar(sSQL))

            'VARIABILE CALCOLATA
            sSQL = "SELECT ID FROM Variables_Calculate_Define WHERE VariableName = '" & hfNomeVariabile.Value & "' AND IDGame = " & Session("IDGame")
            bVariableCalculate = Nni(oDAL.ExecuteScalar(sSQL))

            grfGeneric.ChartTitle.Text = hfPageTitle.Value

            ' Carico la tabella degli items
            m_ItemsTable = HandleGetItemsPeriodVariables(Session("IDGame"), Nz(Session("LanguageActive")))

            ' Carico la tabella dei players
            m_TeamsTable = LoadTeamsGame(Session("IDGame"))

            ' Non posso selezionare tutti i player su tutti gli item
            ' Nel caso vengano selezionati allora procedo con la selezione di tutti i players, ma solo di un item alla volta
            If Session("IDRole").Contains("P") Then
                cboTeam.SelectedValue = ViewState("IDTeam")
            End If

            If cboTeam.SelectedValue = "" AndAlso cboItems.SelectedValue = "" Then
                If Nni(hfIDItem.Value) > 0 Then
                    cboItems.SelectedValue = hfIDItem.Value
                Else
                    cboItems.SelectedIndex = 1
                End If
            End If

            ' *************************************************************************************************************************************
            ' VARIABILE DEL BOSS
            If bVariableBoss Then
                ' Controllo se la variabile usata è una lista di items o di age
                sSQL = "SELECT TOP 1 DBV.Value " _
                     & "FROM Decisions_Boss D " _
                     & "LEFT JOIN Decisions_Boss_Value DBV ON D.Id = DBV.IDDecisionBoss " _
                     & "WHERE D.Decision = '" & hfNomeVariabile.Value & "' AND IDGame = " & Session("IDGame")
                Dim sValue As String = Nz(oDAL.ExecuteScalar(sSQL))

                If sValue.Contains("List") Then ' Lista di valori, items o age
                    ' Controllo se player
                    If sValue.Contains("player") Then
                        sSQL = "SELECT Decision, DBVL.Value, P.Descrizione AS Period, DBVL.IDPeriod, DBVL.VariableName " _
                             & "FROM Decisions_Boss D " _
                             & "LEFT JOIN Decisions_Boss_Value DBV ON D.Id = DBV.IDDecisionBoss " _
                             & "LEFT JOIN Decisions_Boss_Value_List DBVL ON DBV.id = DBVL.IdDecisionBossValue " _
                             & "INNER JOIN BGOL_Periods P ON DBVL.IDPeriod = P.Id " _
                             & "WHERE LTrim(RTrim(D.Decision)) = '" & hfNomeVariabile.Value & "' AND DBVL.VariableName LIKE (%" & GetTeamName(Nni(cboTeam.SelectedValue)) & ") " _
                             & "AND D.IDGame = " & Session("IDGame")
                        bPlayer = True
                        bEnabledPlayer = True

                    ElseIf sValue.Contains("item") Then ' o item
                        sSQL = "SELECT Decision, DBVL.Value, P.Descrizione As Period, DBVL.IDPeriod, DBVL.VariableName " _
                             & "FROM Decisions_Boss D " _
                             & "LEFT JOIN Decisions_Boss_Value DBV On D.Id = DBV.IDDecisionBoss " _
                             & "LEFT JOIN Decisions_Boss_Value_List DBVL On DBV.id = DBVL.IdDecisionBossValue " _
                             & "INNER JOIN BGOL_Periods P On DBVL.IDPeriod = P.Id " _
                             & "WHERE LTrim(RTrim(D.Decision)) = '" & hfNomeVariabile.Value & "' AND D.IDGame = " & Session("IDGame")
                        bItem = True
                        bEnabledItems = True

                    End If
                Else
                    sSQL = "SELECT Decision AS VariableName, DBVL.Value, P.Descrizione AS Period, DBVL.IDPeriod " _
                         & "FROM Decisions_Boss D " _
                         & "LEFT JOIN Decisions_Boss_Value DBV ON D.Id = DBV.IDDecisionBoss " _
                         & "INNER JOIN BGOL_Periods P ON DBV.IDPeriod = P.Id " _
                         & "WHERE LTrim(RTrim(D.Decision)) = '" & hfNomeVariabile.Value & "' AND D.IDGame = " & Session("IDGame")

                End If
                ' carico la tabella dei risultati
                oDTVariable = oDAL.ExecuteDataTable(sSQL)

                ' Passo la tabella alla procedura che permette il caricamento del grafico
                LoadDataBossVariable(oDTVariable, bItem, bPlayer)

                cboItems.Enabled = bEnabledItems
                cboTeam.Enabled = bEnabledPlayer

            End If
            ' *************************************************************************************************************************************

            ' *************************************************************************************************************************************
            ' VARIABILE DI STATO
            If bVariableState Then
                sSQL = "SELECT VS.ID, VS.VariableLabel, VS.IDGame, VS.Name, " _
                     & "VSV.Id AS IDVariableStateValue, VSV.IDPeriod, VSV.IDPlayer, VSV.IDItem, VSV.IDAge, VSV.IDVariable, VSV.Value, VSV.Simulation, " _
                     & "VSVL.IDVariableState, VSVL.ID, VSVL.VariableName , VSVL.Value AS ValueList, VSVL.IDItem AS IDItemList, " _
                     & "VSVL.IDAge AS IDAgeList, VSVL.IDPeriod AS IDPeriodList, VSVL.IDPlayer AS IDPLayerList, VSVL.Simulation  " _
                     & "FROM Variables_State VS " _
                     & "INNER JOIN Variables_State_Value VSV ON VS.ID = VSV.IDVariable " _
                     & "LEFT JOIN Variables_State_Value_List VSVL ON VSV.Id = VSVL.IDVariableState " _
                     & "WHERE VS.Name = '" & hfNomeVariabile.Value & "' AND VS.IDGame = " & Session("IDGame")
                oDTVariable = oDAL.ExecuteDataTable(sSQL)
                LoadDataStateVariable(oDTVariable)

                cboItems.Enabled = False
                cboTeam.Enabled = False

                For Each drVariable As DataRow In oDTVariable.Rows
                    If Nz(drVariable("VariableName")) <> "" Then
                        cboItems.Enabled = Nni(drVariable("IDItemList")) > 0
                        cboTeam.Enabled = Nni(drVariable("IDPlayerList")) > 0

                    Else
                        cboItems.Enabled = Nni(drVariable("IDItem")) > 0
                        cboTeam.Enabled = Nni(drVariable("IDPlayer")) > 0

                    End If

                Next
            End If
            ' *************************************************************************************************************************************

            ' *************************************************************************************************************************************
            If bVariableDecisionsPlayer Then
                ' Controllo se la variabile usata è una lista di items o di age
                sSQL = "SELECT TOP 1 DPV.VariableValue " _
                     & "FROM Decisions_Players D " _
                     & "LEFT JOIN Decisions_Players_Value DPV ON D.Id = DPV.IDDecisionPlayer " _
                     & "WHERE D.VariableName = '" & hfNomeVariabile.Value & "' AND D.IDGame = " & Session("IDGame")
                Dim sValue As String = Nz(oDAL.ExecuteScalar(sSQL))

                If sValue.ToUpper.Contains("LIST") Then ' Lista di valori, items  
                    ' Controllo se player
                    If sValue.ToUpper.Contains("ITEM") Then ' o item
                        sSQL = "SELECT D.VariableName AS Decision, DPVL.Value, P.Descrizione AS Period, DPVL.IDPeriod, DPVL.VariableName, DPVL.IDPlayer, DPVL.IDItem " _
                             & "FROM Decisions_Players D " _
                             & "LEFT JOIN Decisions_Players_Value DPV ON D.Id = DPV.IDDecisionPlayer " _
                             & "LEFT JOIN Decisions_Players_Value_List DPVL ON DPV.ID = DPVL.IDDecisionValue " _
                             & "INNER JOIN BGOL_Periods P ON DPVL.IDPeriod = P.Id " _
                             & "WHERE LTrim(RTrim(D.VariableName)) = '" & hfNomeVariabile.Value & "' AND D.IDGame = " & Session("IDGame")

                    End If
                Else
                    sSQL = "SELECT D.VariableName AS Decision, DPV.Value, P.Descrizione AS Period, DPV.IDPeriod, DPV.IDPlayer " _
                         & "FROM Decisions_Players D " _
                         & "LEFT JOIN Decisions_Players_Value DPV ON D.Id = DPV.IDDecisionPlayer " _
                         & "INNER JOIN BGOL_Periods P ON DBVL.IDPeriod = P.Id " _
                         & "WHERE LTrim(RTrim(D.VariableName)) = '" & hfNomeVariabile.Value & "' AND D.IDGame = " & Session("IDGame")

                End If
                ' carico la tabella dei risultati
                oDTVariable = oDAL.ExecuteDataTable(sSQL)

                ' Passo la tabella alla procedura che permette il caricamento del grafico
                For Each drData As DataRow In oDTVariable.Rows
                    If Not IsNothing(drData("VariableName")) Then
                        cboItems.Enabled = Nni(drData("IDItem")) > 0
                        cboTeam.Enabled = Nni(drData("IDPlayer")) > 0

                    Else
                        cboItems.Enabled = False
                        cboTeam.Enabled = Nni(drData("IDPlayer")) > 0
                    End If

                Next
                LoadDataDecisionPlayerVariable(oDTVariable)

            End If
            ' *************************************************************************************************************************************

            ' *************************************************************************************************************************************
            ' VARIABILE CALCOLATA
            If bVariableCalculate Then
                ' Controllo che la variabile selezionata abbia anche valori legati ai player
                If Nz(cboTeam.SelectedValue) <> "" Then
                    sSQL = "SELECT C.ID " _
                         & "FROM Variables_Calculate_Define V " _
                         & "INNER Join Variables_Calculate_Value C On V.Id = C.IDVariable " _
                         & "INNER JOIN BGOL_Periods P On C.IDPeriod = P.Id " _
                         & "WHERE LTrim(RTrim(V.VariableName)) = '" & hfNomeVariabile.Value & "' AND IDPlayer = " & Nni(cboTeam.SelectedValue) _
                         & " AND V.IDGame = " & Session("IDGame")
                    bPlayer = Nni(oDAL.ExecuteScalar(sSQL)) > 0
                Else
                    bPlayer = False
                End If

                ' Controllo che la variabile abbia valori anche per Items
                If Nz(cboItems.SelectedValue) <> "" Then
                    sSQL = "SELECT C.ID " _
                         & "FROM Variables_Calculate_Define V " _
                         & "INNER Join Variables_Calculate_Value C ON V.Id = C.IDVariable " _
                         & "INNER JOIN BGOL_Periods P ON C.IDPeriod = P.Id " _
                         & "WHERE LTrim(RTrim(V.VariableName)) = '" & hfNomeVariabile.Value & "' AND C.IDItem = " & hfIDItem.Value _
                         & " AND V.IDGame = " & Session("IDGame")
                    bItem = Nni(oDAL.ExecuteScalar(sSQL)) > 0
                Else
                    bItem = False
                End If

                ' Controllo che la variabile abbia valori anche per Age
                If Nni(hfIDAge.Value) > 0 Then
                    sSQL = "SELECT C.ID " _
                         & "FROM Variables_Calculate_Define V " _
                         & "INNER Join Variables_Calculate_Value C ON V.Id = C.IDVariable " _
                         & "INNER JOIN BGOL_Periods P ON C.IDPeriod = P.Id " _
                         & "WHERE LTrim(RTrim(V.VariableName)) = '" & hfNomeVariabile.Value & "' AND C.IDAge = " & hfIDAge.Value _
                         & " AND V.IDGame = " & Session("IDGame")
                    bAge = Nni(oDAL.ExecuteScalar(sSQL)) > 0
                Else
                    bAge = False
                End If

                ' Controllo che sia una variabile di tipo VARIBILE CALCOLATA
                sSQL = "SELECT C.IDPlayer, VariableName, Value, C.IDItem AS Item, P.Descrizione AS Period, C.IDPeriod, C.IDItem, C.IDAge " _
                     & "FROM Variables_Calculate_Define V " _
                     & "INNER Join Variables_Calculate_Value C ON V.Id = C.IDVariable " _
                     & "INNER JOIN BGOL_Periods P ON C.IDPeriod = P.Id " _
                     & "WHERE LTrim(RTrim(V.VariableName)) = '" & hfNomeVariabile.Value & "' " _
                     & " AND V.IDGame = " & Session("IDGame")

                If bPlayer AndAlso Session("IDRole").Contains("P") Then
                    sSQL &= " AND C.IDPlayer = " & Nni(cboTeam.SelectedValue)
                End If

                If hfIDAge.Value > 0 AndAlso bAge Then
                    sSQL &= " AND C.IDAge = " & hfIDAge.Value
                End If

                oDTVariable = oDAL.ExecuteDataTable(sSQL)
                For Each drVariable As DataRow In oDTVariable.Rows
                    cboTeam.Enabled = Nni(drVariable("IDPlayer")) > 0
                    cboItems.Enabled = Nni(drVariable("IDItem")) > 0
                Next

                If oDTVariable.Rows.Count > 0 Then
                    LoadDataCalculateVariable(oDTVariable)
                End If

            End If
            ' *************************************************************************************************************************************

            If Session("IDRole").Contains("P") Then
                cboTeam.SelectedValue = Nni(ViewState("IDTeam"))
                cboTeam.Enabled = False
            End If

            SQLConnClose(oDAL.GetConnObject)

            oDAL = Nothing

            grfGeneric.PlotArea.CommonTooltipsAppearance.DataFormatString = "N"
            grfGeneric.PlotArea.YAxis.LabelsAppearance.DataFormatString = "N"

            grfColumn.PlotArea.CommonTooltipsAppearance.DataFormatString = "N"
            grfColumn.PlotArea.YAxis.LabelsAppearance.DataFormatString = "N"

        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try

    End Sub

    Private Sub LoadDataBossVariable(DataGraph As DataTable, Items As Boolean, Players As Boolean)
        Dim sSQL As String
        Dim sItemName As String = ""
        Dim sIDPeriod As String = ""

        Try
            m_SessionData = Session("SessionData")

            ' Recupero tutti i periodi generati fino adesso
            sSQL = "SELECT * FROM BGOL_Games_Periods BGP " _
                 & "INNER JOIN BGOL_Periods BP ON BGP.IDPeriodo = BP.Id " _
                 & "WHERE BGP.IDGame = " & Session("IDGame") & " AND BP.NumStep <= " & Session("CurrentStep")
            If Session("IDRole").Contains("P") Then
                sSQL &= " AND ISNULL(BGP.Simulation, 0) = 0 "
            End If
            sSQL &= " ORDER BY BP.NumStep "
            Dim oDTPeriodi As DataTable = g_DAL.ExecuteDataTable(sSQL)

            grfGeneric.PlotArea.Series.Clear()
            grfGeneric.PlotArea.XAxis.Items.Clear()

            ' Ciclo sulle righe del datatable per costruire 
            Dim oSeriesData As LineSeries
            Dim oColSeries As ColumnSeries

            Dim oDRItems As DataRow()
            Dim oDRPlayers As DataRow()

            If Items Then
                cboItems.Enabled = False
                oDRItems = m_ItemsTable.Select("")

                For Each oRowItm As DataRow In oDRItems
                    oSeriesData = New LineSeries
                    oColSeries = New ColumnSeries

                    For Each oRowPeriod As DataRow In oDTPeriodi.Rows
                        sItemName = oRowItm("VariableName").ToString

                        Dim oDRValue As DataRow

                        oDRValue = DataGraph.Select("IDPeriod = " & oRowPeriod("IdPeriodo") & " AND VariableName LIKE '%" & oRowItm("VariableNameDefault") & "'").FirstOrDefault

                        If Not IsNothing(oDRValue) Then
                            Dim oItem As New SeriesItem
                            oItem.YValue = Nn(oDRValue("Value")).ToString("N2")
                            oItem.TooltipValue = Nz(oRowItm("VariableName"))
                            oSeriesData.Items.Add(oItem)
                            oColSeries.Items.Add(oItem)
                            sIDPeriod &= Nz(oRowPeriod("IDPeriodo")) & ";"
                        End If
                    Next

                    If sIDPeriod.EndsWith(";") Then sIDPeriod = sIDPeriod.TrimEnd(";")

                    ' Grafico generio (linee)
                    grfGeneric.PlotArea.Series.Add(oSeriesData)
                    oSeriesData.Name = sItemName
                    oSeriesData.VisibleInLegend = True
                    oSeriesData.LabelsAppearance.Visible = False

                    ' Grafico a colonne
                    grfColumn.PlotArea.Series.Add(oColSeries)
                    oColSeries.Name = sItemName
                    oColSeries.VisibleInLegend = True
                    oColSeries.LabelsAppearance.Visible = False

                Next

            ElseIf Players Then
                cboItems.Enabled = True
                oDRPlayers = m_TeamsTable.Select("")

                For Each oRowPlayer As DataRow In oDRPlayers
                    oSeriesData = New LineSeries
                    oColSeries = New ColumnSeries

                    For Each oRowPeriod As DataRow In oDTPeriodi.Rows
                        Dim oDRValue As DataRow

                        oDRValue = DataGraph.Select("IDPeriod = " & oRowPeriod("IdPeriodo") & " AND VariableName LIKE '%" & oRowPlayer("TeamName") & "%'").FirstOrDefault

                        If Not IsNothing(oDRValue) Then
                            Dim oItem As New SeriesItem
                            oItem.YValue = Nn(oDRValue("Value")).ToString("N2")
                            oItem.TooltipValue = Nz(oRowPlayer("TeamName"))
                            oSeriesData.Items.Add(oItem)
                            oColSeries.Items.Add(oItem)
                            sIDPeriod &= Nz(oRowPeriod("IDPeriodo")) & ";"
                        End If
                    Next

                    If sIDPeriod.EndsWith(";") Then sIDPeriod = sIDPeriod.TrimEnd(";")

                    ' Grafico generico (linee)
                    grfGeneric.PlotArea.Series.Add(oSeriesData)
                    oSeriesData.Name = sItemName
                    oSeriesData.VisibleInLegend = True
                    oSeriesData.LabelsAppearance.Visible = False

                    ' Grafico a colonne
                    grfColumn.PlotArea.Series.Add(oColSeries)
                    oColSeries.Name = sItemName
                    oColSeries.VisibleInLegend = True
                    oColSeries.LabelsAppearance.Visible = False

                Next

            Else

            End If

            For Each oRowPeriod As DataRow In oDTPeriodi.Rows
                If sIDPeriod.Contains(oRowPeriod("IdPeriodo")) Then
                    Dim newAxisItem As New AxisItem(Nz(oRowPeriod("Descrizione")))
                    grfGeneric.PlotArea.XAxis.Items.Add(newAxisItem)
                    grfColumn.PlotArea.XAxis.Items.Add(newAxisItem)
                End If
            Next

            grfGeneric.PlotArea.XAxis.LabelsAppearance.RotationAngle = 45
            grfGeneric.PlotArea.XAxis.MinorGridLines.Visible = False
            grfGeneric.PlotArea.YAxis.MinorGridLines.Visible = False
            grfGeneric.Legend.Appearance.Visible = True
            grfGeneric.Legend.Appearance.Position = HtmlChart.ChartLegendPosition.Top

            ' Grafico a colonne
            grfColumn.PlotArea.XAxis.LabelsAppearance.RotationAngle = 45
            grfColumn.PlotArea.XAxis.MinorGridLines.Visible = False
            grfColumn.PlotArea.YAxis.MinorGridLines.Visible = False
            grfColumn.Legend.Appearance.Visible = True
            grfColumn.Legend.Appearance.Position = HtmlChart.ChartLegendPosition.Top


        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub LoadDataStateVariable(DataGraph As DataTable)
        Dim sSQL As String
        Dim sItemName As String = ""
        Dim sIDPeriod As String = ""
        Dim sTeamName As String = ""
        Dim oSeriesData As LineSeries
        Dim oColSeriesData As ColumnSeries

        Try
            ' Controllo se sto caricando una variabile di tipo lista o a valore secco
            If DataGraph.Rows.Count > 0 Then
                m_SessionData = Session("SessionData")

                ' Recupero tutti i periodi generati fino adesso
                sSQL = "SELECT * FROM BGOL_Games_Periods BGP " _
                     & "INNER JOIN BGOL_Periods BP ON BGP.IDPeriodo = BP.Id " _
                     & "WHERE BGP.IDGame = " & Session("IDGame") & " AND BP.NumStep <= " & Session("CurrentStep")
                If Session("IDRole").Contains("P") Then
                    sSQL &= " AND ISNULL(BGP.Simulation, 0) = 0 "
                End If
                sSQL &= " ORDER BY BP.NumStep "
                Dim oDTPeriodi As DataTable = g_DAL.ExecuteDataTable(sSQL)

                grfGeneric.PlotArea.Series.Clear()
                grfGeneric.PlotArea.XAxis.Items.Clear()

                grfColumn.PlotArea.Series.Clear()
                grfColumn.PlotArea.XAxis.Items.Clear()

                ' Ciclo sulle righe del datatable per costruire 
                If Nz(DataGraph.Rows(0)("ValueList")) <> "" Then
                    ' Controllo se sono presenti gli items
                    If Nni(DataGraph.Rows(0)("IDItemList")) <> 0 Then
                        If Nz(cboItems.SelectedValue) = "" AndAlso Nni(cboTeam.SelectedValue) > 0 Then ' Ho selezionato tutti gli items e un solo item
                            For Each oRowItm As DataRow In m_ItemsTable.Rows
                                oSeriesData = New LineSeries
                                oColSeriesData = New ColumnSeries

                                For Each oRowPeriod As DataRow In oDTPeriodi.Rows
                                    sItemName = oRowItm("VariableName").ToString

                                    Dim oDRValue As DataRow

                                    oDRValue = DataGraph.Select("IDPeriodList = " & oRowPeriod("IdPeriodo") & " AND IDItemList = " _
                                                                & oRowItm("IDVariable") & " AND IDPlayerList = " & Nni(cboTeam.SelectedValue)).FirstOrDefault

                                    If Not IsNothing(oDRValue) Then
                                        Dim oItem As New SeriesItem
                                        oItem.YValue = Nn(oDRValue("ValueList")).ToString("N2")
                                        oItem.TooltipValue = Nz(oRowItm("VariableName"))
                                        oSeriesData.Items.Add(oItem)
                                        oColSeriesData.Items.Add(oItem)
                                        sIDPeriod &= Nz(oRowPeriod("IDPeriodo")) & ";"
                                    End If
                                Next

                                If sIDPeriod.EndsWith(";") Then sIDPeriod = sIDPeriod.TrimEnd(";")

                                ' Grafico generico (Linee)
                                grfGeneric.PlotArea.Series.Add(oSeriesData)
                                oSeriesData.Name = sItemName
                                oSeriesData.VisibleInLegend = True
                                oSeriesData.LabelsAppearance.Visible = False

                                ' Grafico a colonne
                                grfColumn.PlotArea.Series.Add(oColSeriesData)
                                oColSeriesData.Name = sItemName
                                oColSeriesData.VisibleInLegend = True
                                oColSeriesData.LabelsAppearance.Visible = False
                            Next

                        ElseIf Nni(cboItems.SelectedValue) > 0 AndAlso Nz(cboTeam.SelectedValue) <> "" Then ' un item un team
                            oSeriesData = New LineSeries
                            oColSeriesData = New ColumnSeries
                            Dim oDRValue As DataRow
                            Dim dValue As Double

                            For Each oRowPeriod As DataRow In oDTPeriodi.Rows
                                sItemName = GetItemName(Nni(cboItems.SelectedValue))

                                oDRValue = DataGraph.Select("IDPeriodList = " & oRowPeriod("IdPeriodo") & " AND IDItemList = " & Nni(cboItems.SelectedValue) _
                                                            & " AND IDPlayerList = " & Nni(cboTeam.SelectedValue)).FirstOrDefault

                                If IsNothing(oDRValue) Then
                                    ' Provo a cercare nella tabella valori
                                    oDRValue = DataGraph.Select("IDPeriod = " & oRowPeriod("IdPeriodo") & " AND IDItem = " & Nni(cboItems.SelectedValue) _
                                                                & " AND IDPlayer = " & Nni(cboTeam.SelectedValue)).FirstOrDefault
                                    dValue = Nn(oDRValue("Value")).ToString("N2")
                                Else
                                    dValue = Nn(oDRValue("ValueList")).ToString("N2")
                                End If

                                Dim oItem As New SeriesItem
                                oItem.YValue = dValue
                                oItem.TooltipValue = Nz(cboItems.Text)
                                oSeriesData.Items.Add(oItem)
                                oColSeriesData.Items.Add(oItem)
                                sIDPeriod &= Nz(oRowPeriod("IDPeriodo")) & ";"
                            Next

                            If sIDPeriod.EndsWith(";") Then sIDPeriod = sIDPeriod.TrimEnd(";")

                            grfGeneric.PlotArea.Series.Add(oSeriesData)
                            oSeriesData.Name = sItemName
                            oSeriesData.VisibleInLegend = True
                            oSeriesData.LabelsAppearance.Visible = False

                            ' Grafico a colonne
                            grfColumn.PlotArea.Series.Add(oColSeriesData)
                            oColSeriesData.Name = sItemName
                            oColSeriesData.VisibleInLegend = True
                            oColSeriesData.LabelsAppearance.Visible = False

                        ElseIf Nni(cboItems.SelectedValue) > 0 AndAlso Nz(cboTeam.SelectedValue) = "" Then ' Ho selezionato tutti i Teams e un solo item

                            For Each oRowPlayer As DataRow In m_TeamsTable.Rows
                                oSeriesData = New LineSeries
                                oColSeriesData = New ColumnSeries

                                sTeamName = GetTeamName(oRowPlayer("ID"))
                                For Each oRowPeriod As DataRow In oDTPeriodi.Rows
                                    Dim oDRValue As DataRow
                                    oDRValue = DataGraph.Select("IDPeriodList = " & oRowPeriod("IdPeriodo") & " AND IDItemList = " _
                                                                & Nni(cboItems.SelectedValue) & " AND IDPlayerList = " & Nni(oRowPlayer("ID"))).FirstOrDefault

                                    If Not IsNothing(oDRValue) Then
                                        Dim oItem As New SeriesItem
                                        oItem.YValue = Nn(oDRValue("ValueList")).ToString("N2")
                                        oItem.TooltipValue = sTeamName
                                        oSeriesData.Items.Add(oItem)
                                        oColSeriesData.Items.Add(oItem)
                                        sIDPeriod &= Nz(oRowPeriod("IDPeriodo")) & ";"
                                    End If
                                Next

                                If sIDPeriod.EndsWith(";") Then sIDPeriod = sIDPeriod.TrimEnd(";")

                                grfGeneric.PlotArea.Series.Add(oSeriesData)
                                oSeriesData.Name = sTeamName
                                oSeriesData.VisibleInLegend = True
                                oSeriesData.LabelsAppearance.Visible = False

                                ' Grafico a colonne
                                grfColumn.PlotArea.Series.Add(oColSeriesData)
                                oColSeriesData.Name = sItemName
                                oColSeriesData.VisibleInLegend = True
                                oColSeriesData.LabelsAppearance.Visible = False
                            Next

                        End If

                    ElseIf Nni(DataGraph.Rows(0)("IDPlayerList")) <> 0 Then
                        If Nz(cboItems.SelectedValue) = "" Then ' Ho selezionato tutti gli items
                            For Each oRowItm As DataRow In m_ItemsTable.Rows
                                oSeriesData = New LineSeries
                                oColSeriesData = New ColumnSeries

                                For Each oRowPeriod As DataRow In oDTPeriodi.Rows
                                    sItemName = oRowItm("VariableName").ToString

                                    Dim oDRValue As DataRow

                                    oDRValue = DataGraph.Select("IDPeriod = " & oRowPeriod("IdPeriodo") & " AND IDItemList = " _
                                                                & oRowItm("IDVariable") & " AND IDPlayer = " & Nni(cboTeam.SelectedValue)).FirstOrDefault

                                    If Not IsNothing(oDRValue) Then
                                        Dim oItem As New SeriesItem
                                        oItem.YValue = Nn(oDRValue("Value")).ToString("N2")
                                        oItem.TooltipValue = Nz(oRowItm("VariableName"))
                                        oSeriesData.Items.Add(oItem)
                                        oColSeriesData.Items.Add(oItem)
                                        sIDPeriod &= Nz(oRowPeriod("IDPeriodo")) & ";"
                                    End If
                                Next

                                If sIDPeriod.EndsWith(";") Then sIDPeriod = sIDPeriod.TrimEnd(";")

                                grfGeneric.PlotArea.Series.Add(oSeriesData)
                                oSeriesData.Name = sItemName
                                oSeriesData.VisibleInLegend = True
                                oSeriesData.LabelsAppearance.Visible = False

                                ' Grafico a colonne
                                grfColumn.PlotArea.Series.Add(oColSeriesData)
                                oColSeriesData.Name = sItemName
                                oColSeriesData.VisibleInLegend = True
                                oColSeriesData.LabelsAppearance.Visible = False
                            Next

                        Else
                            If Nz(cboTeam.SelectedValue) = "" Then ' Ho selezionato tutti i Teams
                                For Each oRowTeam As DataRow In m_TeamsTable.Rows
                                    oSeriesData = New LineSeries
                                    oColSeriesData = New ColumnSeries

                                    For Each oRowPeriod As DataRow In oDTPeriodi.Rows
                                        sItemName = oRowTeam("TeamName").ToString

                                        Dim oDRValue As DataRow

                                        oDRValue = DataGraph.Select("IDPeriod = " & oRowPeriod("IdPeriodo") & " AND IDItemList = " & Nni(cboItems.SelectedValue)).FirstOrDefault

                                        If Not IsNothing(oDRValue) Then
                                            Dim oItem As New SeriesItem
                                            oItem.YValue = Nn(oDRValue("ValueList")).ToString("N2")
                                            oItem.TooltipValue = Nz(oRowTeam("TeamName"))
                                            oSeriesData.Items.Add(oItem)
                                            oColSeriesData.Items.Add(oItem)
                                            sIDPeriod &= Nz(oRowPeriod("IDPeriodo")) & ";"
                                        End If
                                    Next

                                    If sIDPeriod.EndsWith(";") Then sIDPeriod = sIDPeriod.TrimEnd(";")

                                    grfGeneric.PlotArea.Series.Add(oSeriesData)
                                    oSeriesData.Name = sItemName
                                    oSeriesData.VisibleInLegend = True
                                    oSeriesData.LabelsAppearance.Visible = False

                                    ' Grafico a colonne
                                    grfColumn.PlotArea.Series.Add(oColSeriesData)
                                    oColSeriesData.Name = sItemName
                                    oColSeriesData.VisibleInLegend = True
                                    oColSeriesData.LabelsAppearance.Visible = False
                                Next

                                If sIDPeriod.EndsWith(";") Then sIDPeriod = sIDPeriod.TrimEnd(";")

                                grfGeneric.PlotArea.Series.Add(oSeriesData)
                                oSeriesData.Name = sItemName
                                oSeriesData.VisibleInLegend = True
                                oSeriesData.LabelsAppearance.Visible = False

                                ' Grafico a colonne
                                grfColumn.PlotArea.Series.Add(oColSeriesData)
                                oColSeriesData.Name = sItemName
                                oColSeriesData.VisibleInLegend = True
                                oColSeriesData.LabelsAppearance.Visible = False

                            End If
                        End If
                    End If

                Else
                    ' Controllo se su player
                    If Nni(DataGraph.Rows(0)("IDPlayer")) <> 0 AndAlso Nni(DataGraph.Rows(0)("IDItem")) = 0 Then
                        If Nz(cboTeam.SelectedValue) = "" Then
                            For Each oRowTeam As DataRow In m_TeamsTable.Rows
                                oSeriesData = New LineSeries
                                oColSeriesData = New ColumnSeries

                                For Each oRowPeriod As DataRow In oDTPeriodi.Rows
                                    Dim oDRValue As DataRow

                                    oDRValue = DataGraph.Select("IDPeriod = " & oRowPeriod("IdPeriodo") & " AND IDPlayer = " & Nni(oRowTeam("ID"))).FirstOrDefault

                                    If Not IsNothing(oDRValue) Then
                                        Dim oItem As New SeriesItem
                                        oItem.YValue = Nn(oDRValue("Value")).ToString("N2")
                                        oItem.TooltipValue = Nz(oRowTeam("TeamName"))
                                        oSeriesData.Items.Add(oItem)
                                        oColSeriesData.Items.Add(oItem)
                                        sIDPeriod &= Nz(oRowPeriod("IDPeriodo")) & ";"
                                    End If
                                Next

                                If sIDPeriod.EndsWith(";") Then sIDPeriod = sIDPeriod.TrimEnd(";")

                                grfGeneric.PlotArea.Series.Add(oSeriesData)
                                oSeriesData.Name = Nz(oRowTeam("TeamName"))
                                oSeriesData.VisibleInLegend = True
                                oSeriesData.LabelsAppearance.Visible = False

                                ' Grafico a colonne
                                grfColumn.PlotArea.Series.Add(oColSeriesData)
                                oColSeriesData.Name = sItemName
                                oColSeriesData.VisibleInLegend = True
                                oColSeriesData.LabelsAppearance.Visible = False
                            Next

                        Else
                            oSeriesData = New LineSeries
                            oColSeriesData = New ColumnSeries

                            For Each oRowPeriod As DataRow In oDTPeriodi.Rows
                                Dim oDRValue As DataRow

                                oDRValue = DataGraph.Select("IDPeriod = " & oRowPeriod("IdPeriodo") & " AND IDPlayer = " & Nni(cboTeam.SelectedValue)).FirstOrDefault

                                If Not IsNothing(oDRValue) Then
                                    Dim oItem As New SeriesItem
                                    oItem.YValue = Nn(oDRValue("Value")).ToString("N2")
                                    oItem.TooltipValue = Nz(cboTeam.Text)
                                    oSeriesData.Items.Add(oItem)
                                    oColSeriesData.Items.Add(oItem)
                                    sIDPeriod &= Nz(oRowPeriod("IDPeriodo")) & ";"
                                End If
                            Next
                        End If


                        If sIDPeriod.EndsWith(";") Then sIDPeriod = sIDPeriod.TrimEnd(";")

                        grfGeneric.PlotArea.Series.Add(oSeriesData)
                        oSeriesData.Name = sItemName
                        oSeriesData.VisibleInLegend = True
                        oSeriesData.LabelsAppearance.Visible = False

                        ' Grafico a colonne
                        grfColumn.PlotArea.Series.Add(oColSeriesData)
                        oColSeriesData.Name = sItemName
                        oColSeriesData.VisibleInLegend = True
                        oColSeriesData.LabelsAppearance.Visible = False
                    End If

                End If

                For Each oRowPeriod As DataRow In oDTPeriodi.Rows
                    If sIDPeriod.Contains(oRowPeriod("IdPeriodo")) Then
                        Dim newAxisItem As New AxisItem(Nz(oRowPeriod("Descrizione")))
                        grfGeneric.PlotArea.XAxis.Items.Add(newAxisItem)
                        grfColumn.PlotArea.XAxis.Items.Add(newAxisItem)
                    End If
                Next

                grfGeneric.PlotArea.XAxis.LabelsAppearance.RotationAngle = 45
                grfGeneric.PlotArea.XAxis.MinorGridLines.Visible = False
                grfGeneric.PlotArea.YAxis.MinorGridLines.Visible = False
                grfGeneric.Legend.Appearance.Visible = True
                grfGeneric.Legend.Appearance.Position = HtmlChart.ChartLegendPosition.Top

                ' Grafico a colonne
                grfColumn.PlotArea.XAxis.LabelsAppearance.RotationAngle = 45
                grfColumn.PlotArea.XAxis.MinorGridLines.Visible = False
                grfColumn.PlotArea.YAxis.MinorGridLines.Visible = False
                grfColumn.Legend.Appearance.Visible = True
                grfColumn.Legend.Appearance.Position = HtmlChart.ChartLegendPosition.Top

            End If

        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub LoadDataDecisionPlayerVariable(DataGraph As DataTable)
        Dim sSQL As String
        Dim sItemName As String = ""
        Dim sIDPeriod As String = ""
        Dim drItem As DataRow

        Try
            m_SessionData = Session("SessionData")

            ' Recupero tutti i periodi generati fino adesso
            sSQL = "SELECT * FROM BGOL_Games_Periods BGP " _
                 & "INNER JOIN BGOL_Periods BP ON BGP.IDPeriodo = BP.Id " _
                 & "WHERE BGP.IDGame = " & Session("IDGame") & " AND BP.NumStep < " & Session("CurrentStep")
            If Session("IDRole").Contains("P") Then
                sSQL &= " AND ISNULL(BGP.Simulation, 0) = 0 "
            End If
            sSQL &= " ORDER BY BP.NumStep "
            Dim oDTPeriodi As DataTable = g_DAL.ExecuteDataTable(sSQL)

            grfGeneric.PlotArea.Series.Clear()
            grfGeneric.PlotArea.XAxis.Items.Clear()

            grfColumn.PlotArea.Series.Clear()
            grfColumn.PlotArea.XAxis.Items.Clear()

            ' Ciclo sulle righe del datatable per costruire 
            Dim oSeriesData As LineSeries
            Dim oColSeries As ColumnSeries

            Dim oDRItems As DataRow()

            oDRItems = m_ItemsTable.Select("")

            If Nz(cboItems.SelectedValue) = "" Then

                For Each oRowItm As DataRow In m_ItemsTable.Rows
                    oSeriesData = New LineSeries
                    oColSeries = New ColumnSeries

                    For Each oRowPeriod As DataRow In oDTPeriodi.Rows
                        sItemName = oRowItm("VariableName").ToString

                        Dim oDRValue As DataRow

                        If cboTeam.Enabled Then
                            If cboTeam.SelectedValue <> "" Then
                                oDRValue = DataGraph.Select("IDPeriod = " & oRowPeriod("IdPeriodo") & " AND VariableName LIKE '%" & oRowItm("VariableNameDefault") _
                                                            & "' AND IDPlayer = " & Nni(cboTeam.SelectedValue)).FirstOrDefault
                            Else

                                ' Con il nome della variabile selezionata, recupero il nome originale della variabile
                                drItem = m_ItemsTable.Select("VariableName = '" & Nz(cboItems.Text) & "' ").FirstOrDefault
                                oDRValue = DataGraph.Select("IDPeriod = " & oRowPeriod("IdPeriodo") & " AND VariableName LIKE '%" & Nz(drItem("VariableNameDefault")) & "'").FirstOrDefault
                            End If

                        Else

                            ' Con il nome della variabile selezionata, recupero il nome originale della variabile
                            drItem = m_ItemsTable.Select("VariableName = '" & Nz(cboItems.Text) & "' ").FirstOrDefault
                            oDRValue = DataGraph.Select("IDPeriod = " & oRowPeriod("IdPeriodo") & " AND VariableName LIKE '%" & Nz(drItem("VariableNameDefault")) & "'").FirstOrDefault
                        End If

                        If Not IsNothing(oDRValue) Then
                            Dim oItem As New SeriesItem
                            oItem.YValue = Nn(oDRValue("Value")).ToString("N2")
                            oItem.TooltipValue = Nz(oRowItm("VariableName"))
                            oSeriesData.Items.Add(oItem)
                            oColSeries.Items.Add(oItem)
                            sIDPeriod &= Nz(oRowPeriod("IDPeriodo")) & ";"

                        End If
                    Next

                    If sIDPeriod.EndsWith(";") Then sIDPeriod = sIDPeriod.TrimEnd(";")

                    ' Grafico standard
                    grfGeneric.PlotArea.Series.Add(oSeriesData)
                    oSeriesData.Name = sItemName
                    oSeriesData.VisibleInLegend = True
                    oSeriesData.LabelsAppearance.Visible = False

                    ' Grafico a colonne
                    grfColumn.PlotArea.Series.Add(oColSeries)
                    oColSeries.Name = sItemName
                    oColSeries.VisibleInLegend = True
                    oColSeries.LabelsAppearance.Visible = True
                Next
            Else
                If cboTeam.Enabled Then
                    If cboTeam.SelectedValue = "" Then
                        Dim sTeamName As String

                        For Each oRowPlayer As DataRow In m_TeamsTable.Rows
                            oSeriesData = New LineSeries
                            oColSeries = New ColumnSeries

                            sTeamName = GetTeamName(oRowPlayer("ID"))
                            sItemName = Nz(cboItems.Text)

                            For Each oRowPeriod As DataRow In oDTPeriodi.Rows
                                Dim oDRValue As DataRow
                                oDRValue = DataGraph.Select("IDPeriod = " & oRowPeriod("IdPeriodo") & " AND VariableName LIKE '%" & sItemName & "' AND IDPlayer = " _
                                                            & Nni(oRowPlayer("ID"))).FirstOrDefault

                                If Not IsNothing(oDRValue) Then
                                    Dim oItem As New SeriesItem
                                    oItem.YValue = Nn(oDRValue("Value")).ToString("N2")
                                    oItem.TooltipValue = sTeamName
                                    oSeriesData.Items.Add(oItem)
                                    oColSeries.Items.Add(oItem)
                                    sIDPeriod &= Nz(oRowPeriod("IDPeriodo")) & ";"
                                End If
                            Next

                            If sIDPeriod.EndsWith(";") Then sIDPeriod = sIDPeriod = sIDPeriod.TrimEnd(";")

                            ' Grafico generico
                            grfGeneric.PlotArea.Series.Add(oSeriesData)
                            oSeriesData.Name = sTeamName
                            oSeriesData.VisibleInLegend = True
                            oSeriesData.LabelsAppearance.Visible = False

                            ' Grafico a colonne
                            grfColumn.PlotArea.Series.Add(oColSeries)
                            oColSeries.Name = sTeamName
                            oColSeries.VisibleInLegend = True
                            oColSeries.LabelsAppearance.Visible = True
                        Next

                    Else

                        oSeriesData = New LineSeries
                        oColSeries = New ColumnSeries

                        For Each oRowPeriod As DataRow In oDTPeriodi.Rows
                            sItemName = GetItemName(Nni(cboItems.SelectedValue))

                            Dim oDRValue As DataRow

                            oDRValue = DataGraph.Select("IDPeriod = " & oRowPeriod("IdPeriodo") & " AND VariableName LIKE '%" _
                                                        & sItemName & "' AND IDPlayer = " & Nni(cboTeam.SelectedValue)).FirstOrDefault

                            If Not IsNothing(oDRValue) Then
                                Dim oItem As New SeriesItem
                                oItem.YValue = Nn(oDRValue("Value")).ToString("N2")
                                oItem.TooltipValue = Nni(cboItems.SelectedValue)
                                oSeriesData.Items.Add(oItem)
                                oColSeries.Items.Add(oItem)
                                sIDPeriod &= Nz(oRowPeriod("IDPeriodo")) & ";"
                            End If
                        Next

                        If sIDPeriod.EndsWith(";") Then sIDPeriod = sIDPeriod.TrimEnd(";")

                        ' Grfico generico
                        grfGeneric.PlotArea.Series.Add(oSeriesData)
                        oSeriesData.Name = cboItems.Text
                        oSeriesData.VisibleInLegend = True
                        oSeriesData.LabelsAppearance.Visible = False

                        ' Grafico a colonne
                        grfColumn.PlotArea.Series.Add(oColSeries)
                        oColSeries.Name = cboItems.Text
                        oColSeries.VisibleInLegend = True
                        oColSeries.LabelsAppearance.Visible = True

                    End If
                Else

                    For Each oRowPeriod As DataRow In oDTPeriodi.Rows
                        oSeriesData = New LineSeries
                        oColSeries = New ColumnSeries

                        sItemName = GetItemName(Nni(cboItems.SelectedValue))

                        Dim oDRValue As DataRow

                        If cboTeam.Enabled Then
                            If cboTeam.SelectedValue <> "" Then
                                oDRValue = DataGraph.Select("IDPeriod = " & oRowPeriod("IdPeriodo") & " AND VariableName LIKE '%" & sItemName _
                                                            & "' AND IDPlayer = " & Nni(cboTeam.SelectedValue)).FirstOrDefault
                            Else

                                oDRValue = DataGraph.Select("IDPeriod = " & oRowPeriod("IdPeriodo") & " AND VariableName LIKE '%" & sItemName & "'").FirstOrDefault
                            End If

                        Else
                            oDRValue = DataGraph.Select("IDPeriod = " & oRowPeriod("IdPeriodo") & " AND VariableName LIKE '%" & sItemName & "'").FirstOrDefault
                        End If

                        If Not IsNothing(oDRValue) Then
                            Dim oItem As New SeriesItem
                            oItem.YValue = Nn(oDRValue("Value")).ToString("N2")
                            oItem.TooltipValue = sItemName
                            oSeriesData.Items.Add(oItem)
                            oColSeries.Items.Add(oItem)
                            sIDPeriod &= Nz(oRowPeriod("IDPeriodo")) & ";"
                        End If

                        If sIDPeriod.EndsWith(";") Then sIDPeriod = sIDPeriod.TrimEnd(";")

                        ' Grafico di default
                        grfGeneric.PlotArea.Series.Add(oSeriesData)
                        oSeriesData.Name = sItemName
                        oSeriesData.VisibleInLegend = True
                        oSeriesData.LabelsAppearance.Visible = False

                        ' Grafico a colonne
                        grfColumn.PlotArea.Series.Add(oColSeries)
                        oColSeries.Name = sItemName
                        oColSeries.VisibleInLegend = True
                        oColSeries.LabelsAppearance.Visible = True
                    Next

                End If

            End If

            For Each oRowPeriod As DataRow In oDTPeriodi.Rows
                If sIDPeriod.Contains(oRowPeriod("IdPeriodo")) Then
                    Dim newAxisItem As New AxisItem(Nz(oRowPeriod("Descrizione")))
                    grfGeneric.PlotArea.XAxis.Items.Add(newAxisItem)
                    grfColumn.PlotArea.XAxis.Items.Add(newAxisItem)
                End If
            Next

            ' Grafico standard
            grfGeneric.PlotArea.XAxis.LabelsAppearance.RotationAngle = 45
            grfGeneric.PlotArea.XAxis.MinorGridLines.Visible = False
            grfGeneric.PlotArea.YAxis.MinorGridLines.Visible = False
            grfGeneric.Legend.Appearance.Visible = True
            grfGeneric.Legend.Appearance.Position = HtmlChart.ChartLegendPosition.Top

            ' Grafico a colonne
            grfColumn.PlotArea.XAxis.LabelsAppearance.RotationAngle = 45
            grfColumn.PlotArea.XAxis.MinorGridLines.Visible = False
            grfColumn.PlotArea.YAxis.MinorGridLines.Visible = False
            grfColumn.Legend.Appearance.Visible = True
            grfColumn.Legend.Appearance.Position = HtmlChart.ChartLegendPosition.Top

        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub LoadDataCalculateVariable(DataGraph As DataTable)
        Dim sSQL As String
        Dim sItemName As String = ""
        Dim sTeamName As String = ""
        Dim sIDPeriod As String = ""

        Dim bPlayer As Boolean = False
        Dim bItem As Boolean = False
        Dim bAge As Boolean = False

        Try
            m_SessionData = Session("SessionData")

            ' Recupero tutti i periodi generati fino adesso
            sSQL = "SELECT * FROM BGOL_Games_Periods BGP " _
                 & "INNER JOIN BGOL_Periods BP ON BGP.IDPeriodo = BP.Id " _
                 & "WHERE BGP.IDGame = " & Session("IDGame") & " AND BP.NumStep <= " & Session("CurrentStep")
            If Session("IDRole").Contains("P") Then
                sSQL &= " AND ISNULL(BGP.Simulation, 0) = 0 "
            End If
            sSQL &= " ORDER BY BP.NumStep "
            Dim oDTPeriodi As DataTable = g_DAL.ExecuteDataTable(sSQL)

            grfGeneric.PlotArea.Series.Clear()
            grfGeneric.PlotArea.XAxis.Items.Clear()

            grfColumn.PlotArea.Series.Clear()
            grfColumn.PlotArea.XAxis.Items.Clear()

            ' Ciclo sulle righe del datatable per costruire 
            Dim oSeriesData As LineSeries
            Dim oColSeriesData As ColumnSeries

            ' Controllo che sia una variabile che gestisce items
            For Each oRowData As DataRow In DataGraph.Rows
                If Nni(oRowData("IDItem")) > 0 Then
                    bItem = True
                    Exit For
                End If

                If Nni(oRowData("IDAge")) > 0 Then
                    bAge = True
                    Exit For
                End If
            Next

            Dim oDRItems As DataRow()

            If Session("IDRole").Contains("P") Then
                ' Ho un item su cui intervenire
                If Nz(cboItems.SelectedValue) <> "" AndAlso bItem Then
                    oDRItems = m_ItemsTable.Select("IDVariable = " & cboItems.SelectedValue)
                Else
                    oDRItems = m_ItemsTable.Select("")
                End If

                If bItem Then
                    For Each oRowItm As DataRow In oDRItems
                        oSeriesData = New LineSeries
                        oColSeriesData = New ColumnSeries

                        For Each oRowPeriod As DataRow In oDTPeriodi.Rows
                            sItemName = oRowItm("VariableName").ToString

                            Dim oDRValue As DataRow

                            oDRValue = DataGraph.Select("IDPeriod = " & oRowPeriod("IdPeriodo") & " AND IDItem = " & oRowItm("IDVariable")).FirstOrDefault

                            If Not IsNothing(oDRValue) Then
                                Dim oItem As New SeriesItem
                                oItem.YValue = Nn(oDRValue("Value"))
                                oItem.TooltipValue = Nz(oRowItm("VariableName"))
                                oSeriesData.Items.Add(oItem)
                                oColSeriesData.Items.Add(oItem)
                                sIDPeriod &= Nz(oRowPeriod("IDPeriodo")) & ";"
                            End If
                        Next

                        If sIDPeriod.EndsWith(";") Then sIDPeriod = sIDPeriod.TrimEnd(";")

                        ' Grafico generico
                        grfGeneric.PlotArea.Series.Add(oSeriesData)
                        oSeriesData.Name = sItemName
                        oSeriesData.VisibleInLegend = True
                        oSeriesData.LabelsAppearance.Visible = False

                        ' Grafico a colonne
                        grfColumn.PlotArea.Series.Add(oColSeriesData)
                        oColSeriesData.Name = sItemName
                        oColSeriesData.VisibleInLegend = True
                        oColSeriesData.LabelsAppearance.Visible = False

                    Next

                Else
                    oSeriesData = New LineSeries
                    oColSeriesData = New ColumnSeries

                    For Each oRowPeriod As DataRow In oDTPeriodi.Rows
                        Dim oDRValue As DataRow

                        oDRValue = DataGraph.Select("IDPeriod = " & oRowPeriod("IdPeriodo")).FirstOrDefault

                        If Not IsNothing(oDRValue) Then
                            Dim oItem As New SeriesItem
                            oItem.YValue = Nn(oDRValue("Value"))
                            oItem.TooltipValue = GetTeamName(Nni(cboTeam.SelectedValue))
                            oSeriesData.Items.Add(oItem)
                            oColSeriesData.Items.Add(oItem)
                            sIDPeriod &= Nz(oRowPeriod("IDPeriodo")) & ";"
                        End If
                    Next

                    If sIDPeriod.EndsWith(";") Then sIDPeriod = sIDPeriod.TrimEnd(";")

                    ' Grafico generico
                    grfGeneric.PlotArea.Series.Add(oSeriesData)
                    oSeriesData.Name = sItemName
                    oSeriesData.VisibleInLegend = True
                    oSeriesData.LabelsAppearance.Visible = False

                    ' Grafico a colonne
                    grfColumn.PlotArea.Series.Add(oColSeriesData)
                    oColSeriesData.Name = sItemName
                    oColSeriesData.VisibleInLegend = True
                    oColSeriesData.LabelsAppearance.Visible = False
                End If

            Else
                ' Sono boss/tutor/developer, quindi posso scegliere un player e vedere tutti i suoi ITem, oppure tutti i player e vedere solo l'item selezionato
                ' Controllo il datatable che abbia dei player al suo interno
                Dim bPLayers As Boolean = False
                Dim bItems As Boolean = False

                For Each drValue As DataRow In DataGraph.Rows
                    bPlayer = Nni(drValue("IDPlayer")) > 0
                    bItems = Nni(drValue("IDItem")) > 0
                Next

                If Nz(cboTeam.SelectedValue) <> "" AndAlso cboTeam.Enabled Then ' ho selezionato un team
                    ' Ho un item su cui intervenire
                    If bItem Then
                        If cboItems.SelectedValue <> "" AndAlso cboItems.SelectedValue > 0 Then
                            oDRItems = m_ItemsTable.Select("IDVariable = " & cboItems.SelectedValue)
                        Else
                            oDRItems = m_ItemsTable.Select("")
                        End If

                        For Each oRowItm As DataRow In oDRItems
                            oSeriesData = New LineSeries
                            oColSeriesData = New ColumnSeries

                            For Each oRowPeriod As DataRow In oDTPeriodi.Rows
                                sItemName = oRowItm("VariableName").ToString

                                Dim oDRValue As DataRow

                                oDRValue = DataGraph.Select("IDPeriod = " & oRowPeriod("IdPeriodo") & " AND IDItem = " & oRowItm("IDVariable") & " AND IDPlayer = " _
                                                            & Nni(cboTeam.SelectedValue)).FirstOrDefault

                                If Not IsNothing(oDRValue) Then
                                    Dim oItem As New SeriesItem
                                    oItem.YValue = Nn(oDRValue("Value"))
                                    oItem.TooltipValue = Nz(oRowItm("VariableName"))
                                    oSeriesData.Items.Add(oItem)
                                    oColSeriesData.Items.Add(oItem)
                                    sIDPeriod &= Nz(oRowPeriod("IDPeriodo")) & ";"
                                End If
                            Next

                            If sIDPeriod.EndsWith(";") Then sIDPeriod = sIDPeriod.TrimEnd(";")

                            ' Grafico generico
                            grfGeneric.PlotArea.Series.Add(oSeriesData)
                            oSeriesData.Name = sItemName
                            oSeriesData.VisibleInLegend = True
                            oSeriesData.LabelsAppearance.Visible = False

                            ' Grafico a colonne
                            grfColumn.PlotArea.Series.Add(oColSeriesData)
                            oColSeriesData.Name = sItemName
                            oColSeriesData.VisibleInLegend = True
                            oColSeriesData.LabelsAppearance.Visible = False
                        Next

                    Else
                        oSeriesData = New LineSeries
                        oColSeriesData = New ColumnSeries

                        For Each oRowPeriod As DataRow In oDTPeriodi.Rows
                            Dim oDRValue As DataRow

                            If Nz(cboTeam.SelectedValue) <> "" AndAlso Nni(cboTeam.SelectedValue) > 0 Then
                                oDRValue = DataGraph.Select("IDPeriod = " & oRowPeriod("IdPeriodo") & " AND IDPlayer = " & Nni(cboTeam.SelectedValue)).FirstOrDefault
                            Else
                                oDRValue = DataGraph.Select("IDPeriod = " & oRowPeriod("IdPeriodo")).FirstOrDefault
                            End If

                            If Not IsNothing(oDRValue) Then
                                Dim oItem As New SeriesItem
                                oItem.YValue = Nn(oDRValue("Value")).ToString("N")
                                oItem.TooltipValue = GetTeamName(Nni(cboTeam.SelectedValue))
                                oSeriesData.Items.Add(oItem)
                                oColSeriesData.Items.Add(oItem)
                                sIDPeriod &= Nz(oRowPeriod("IDPeriodo")) & ";"
                            End If
                        Next

                        If sIDPeriod.EndsWith(";") Then sIDPeriod = sIDPeriod.TrimEnd(";")

                        ' Grafico generico
                        grfGeneric.PlotArea.Series.Add(oSeriesData)
                        oSeriesData.Name = GetTeamName(Nni(cboTeam.SelectedValue))
                        oSeriesData.VisibleInLegend = True
                        oSeriesData.LabelsAppearance.Visible = False

                        ' Grafico a colonne
                        grfColumn.PlotArea.Series.Add(oColSeriesData)
                        oColSeriesData.Name = GetTeamName(Nni(cboTeam.SelectedValue))
                        oColSeriesData.VisibleInLegend = True
                        oColSeriesData.LabelsAppearance.Visible = False
                    End If

                Else ' Costruisco un grafico con tutti player per quell'item selezionato

                    ' Controllo che sia abilitato l'accesso ai player
                    If cboTeam.Enabled Then
                        For Each oDRPlayer As DataRow In m_TeamsTable.Rows
                            oSeriesData = New LineSeries
                            oColSeriesData = New ColumnSeries

                            For Each oRowPeriod As DataRow In oDTPeriodi.Rows
                                sTeamName = oDRPlayer("TeamName")

                                Dim oDRValue As DataRow
                                ' Posso scegliere anche un Item
                                If cboItems.Enabled Then
                                    If cboItems.SelectedValue <> "" Then
                                        oDRValue = DataGraph.Select("IDPeriod = " & oRowPeriod("IdPeriodo") & " AND IDPlayer = " _
                                                                    & oDRPlayer("ID") & " AND IDItem = " & Nni(cboItems.SelectedValue)).FirstOrDefault

                                    End If
                                Else
                                    oDRValue = DataGraph.Select("IDPeriod = " & oRowPeriod("IdPeriodo") & " AND IDPlayer = " & oDRPlayer("ID")).FirstOrDefault

                                End If

                                If Not IsNothing(oDRValue) Then
                                    Dim oItem As New SeriesItem
                                    oItem.YValue = Nn(oDRValue("Value"))
                                    oItem.TooltipValue = sTeamName
                                    oSeriesData.Items.Add(oItem)
                                    oColSeriesData.Items.Add(oItem)
                                    oColSeriesData.LabelsAppearance.Position = HtmlChart.BarColumnLabelsPosition.InsideEnd
                                    sIDPeriod &= Nz(oRowPeriod("IDPeriodo")) & ";"
                                End If
                            Next

                            If sIDPeriod.EndsWith(";") Then sIDPeriod = sIDPeriod.TrimEnd(";")

                            ' Grafico generico
                            grfGeneric.PlotArea.Series.Add(oSeriesData)
                            oSeriesData.Name = sTeamName
                            oSeriesData.VisibleInLegend = True
                            oSeriesData.LabelsAppearance.Visible = False

                            ' Grafico a colonne
                            grfColumn.PlotArea.Series.Add(oColSeriesData)
                            oColSeriesData.Name = sTeamName
                            oColSeriesData.VisibleInLegend = True
                            oColSeriesData.LabelsAppearance.Visible = False
                        Next
                    Else
                        If cboItems.Enabled Then
                            If cboItems.SelectedValue <> "" AndAlso cboItems.SelectedValue > 0 Then
                                oDRItems = m_ItemsTable.Select("IDVariable = " & cboItems.SelectedValue)
                            Else
                                oDRItems = m_ItemsTable.Select("")
                            End If

                            For Each oRowItm As DataRow In oDRItems
                                oSeriesData = New LineSeries
                                oColSeriesData = New ColumnSeries

                                For Each oRowPeriod As DataRow In oDTPeriodi.Rows
                                    sItemName = oRowItm("VariableName").ToString

                                    Dim oDRValue As DataRow

                                    oDRValue = DataGraph.Select("IDPeriod = " & oRowPeriod("IdPeriodo") & " AND IDItem = " & oRowItm("IDVariable")).FirstOrDefault

                                    If Not IsNothing(oDRValue) Then
                                        Dim oItem As New SeriesItem
                                        oItem.YValue = Nn(oDRValue("Value"))
                                        oItem.TooltipValue = Nz(oRowItm("VariableName"))
                                        oSeriesData.Items.Add(oItem)
                                        oColSeriesData.Items.Add(oItem)
                                        sIDPeriod &= Nz(oRowPeriod("IDPeriodo")) & ";"
                                    End If
                                Next

                                If sIDPeriod.EndsWith(";") Then sIDPeriod = sIDPeriod.TrimEnd(";")

                                ' Grafico generico
                                grfGeneric.PlotArea.Series.Add(oSeriesData)
                                oSeriesData.Name = sItemName
                                oSeriesData.VisibleInLegend = True
                                oSeriesData.LabelsAppearance.Visible = False

                                ' Grafico a colonne
                                grfColumn.PlotArea.Series.Add(oColSeriesData)
                                oColSeriesData.Name = sItemName
                                oColSeriesData.VisibleInLegend = True
                                oColSeriesData.LabelsAppearance.Visible = False
                            Next
                        End If
                    End If

                End If
            End If

            For Each oRowPeriod As DataRow In oDTPeriodi.Rows
                If sIDPeriod.Contains(oRowPeriod("IdPeriodo")) Then
                    Dim newAxisItem As New AxisItem(Nz(oRowPeriod("Descrizione")))
                    grfGeneric.PlotArea.XAxis.Items.Add(newAxisItem)
                    grfColumn.PlotArea.XAxis.Items.Add(newAxisItem)
                End If
            Next

            ' Grafico generico
            grfGeneric.PlotArea.XAxis.MinorGridLines.Visible = False
            grfGeneric.PlotArea.YAxis.MinorGridLines.Visible = False
            grfGeneric.Legend.Appearance.Visible = True
            grfGeneric.Legend.Appearance.Position = HtmlChart.ChartLegendPosition.Top

            ' Grafico a colonne
            grfColumn.PlotArea.XAxis.MinorGridLines.Visible = False
            grfColumn.PlotArea.YAxis.MinorGridLines.Visible = False
            grfColumn.Legend.Appearance.Visible = True
            grfColumn.Legend.Appearance.Position = HtmlChart.ChartLegendPosition.Top

        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub cboTeam_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles cboTeam.SelectedIndexChanged
        LoadDataGraph()
    End Sub

    Private Sub cboItems_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles cboItems.SelectedIndexChanged
        LoadDataGraph()
    End Sub

    Private Sub chkChangeGraph_Click(sender As Object, e As EventArgs) Handles chkChangeGraph.Click
        If chkChangeGraph.Checked Then
            grfColumn.Visible = True
            grfGeneric.Visible = False
        Else
            grfColumn.Visible = False
            grfGeneric.Visible = True
        End If
    End Sub

End Class
