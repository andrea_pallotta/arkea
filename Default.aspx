﻿<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Default.aspx.vb" Inherits="_Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="cntMain" ContentPlaceHolderID="MainContent" runat="Server" ClientIDMode="Static">

    <asp:UpdatePanel ID="UpdatePanelMain" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <h2>
                <asp:Label ID="lblWelcome" runat="server" Text="Welcome home"></asp:Label>
            </h2>

            <div class="divTableRow">
                <div clientidmode="Static" id="divLeft" runat="server" style="width: 50%; float: left; padding-right: 20px;" class="defBackground">
                </div>

                <div clientidmode="Static" id="divRight" runat="server" style="float: right; width: 50%; padding-left: 20px;">
                </div>
            </div>

            <div class="clr"></div>

            <div class="row">
                <div class="divTableCell" style="text-align: right;">
                    <telerik:radbutton id="btnTranslate" runat="server" text="Translate" enableajaxskinrendering="true" tooltip=""></telerik:radbutton>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
