﻿Imports APS
Imports APPCore.Utility

Partial Class _Default
    Inherits Page

    Private m_SessionData As MU_SessionData

    Private Sub _Default_Load(sender As Object, e As EventArgs) Handles Me.Load
        m_SessionData = Session("SessionData")

        ' Recupero l'eventuale IDPlayer, eventualmente rimando alla LOGIN
        Dim iIDPlayer As Integer = m_SessionData.Utente.ID

        If IsNothing(m_SessionData) Then
            PageRedirect("~/Account/Login.aspx", "", "")
        End If

        If Not Page.IsPostBack Then
            Me.Page.Master.FindControl("cboDiv").Visible = False
            If iIDPlayer = 0 Then
                PageRedirect("~/Account/Login.aspx", "", "")
            End If
            btnTranslate.Visible = Session("IDRole").Contains("D")
        End If

        ' Controllo tutti gli oggetti presenti
        Dim oTranslation As New TranslationObjectForm(Me, g_DAL, Nz(Session("LanguageActive")))

        lblWelcome.Text &= "<br/>" & m_SessionData.Utente.Nome
    End Sub

    Private Sub btnTranslate_Click(sender As Object, e As EventArgs) Handles btnTranslate.Click
        Session("PageOrigin") = Me.ToString().Substring(4, Me.ToString().Substring(4).Length - 5) + ".aspx"
        Response.Redirect("PageTranslation.aspx")
        Response.End()
    End Sub

End Class