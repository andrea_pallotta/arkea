﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Developer_Menu_Manager.aspx.vb" Inherits="Developer_Menu_Manager" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="cntMain" ContentPlaceHolderID="MainContent" runat="Server" ClientIDMode="Static">

    <asp:UpdatePanel ID="UpdatePanelMain" runat="server" UpdateMode="Conditional">

        <Triggers>
            <asp:PostBackTrigger ControlID="btnSubMenuClose" />
            <asp:PostBackTrigger ControlID="btnCloseSubMenuTranslation" />
            <asp:PostBackTrigger ControlID="btnHideMenu" />
            <asp:PostBackTrigger ControlID="btnHideTranslation" />     
        </Triggers>

        <ContentTemplate>
            <h2>
                <asp:Label ID="lblWelcome" runat="server" Text=""></asp:Label>
            </h2>

            <div class="clr"></div>

            <%--------------------------------------------------------------------------------------------------------------------------------------------------------------------%>
            <%-- TITOLO DELLA PAGINA --%>
            <%--------------------------------------------------------------------------------------------------------------------------------------------------------------------%>
            <div id="subTitle">
                <div id="subLeft" style="float: left; width: 20%; padding-left: 10px">
                    <h3>
                        <asp:Label ID="lblTitolo" runat="server" Text="Definizione menù"></asp:Label>
                    </h3>
                </div>

                <div id="subRight" style="float: right; padding-right: 5px;">
                    <div id="divNewMenu" style="margin-top: 10px; margin-bottom: 5px; text-align: right;">
                        <telerik:RadButton ID="btnNewMenu" runat="server" Text="New menu"
                            RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                        </telerik:RadButton>
                    </div>
                </div>
            </div>

            <div class="clr"></div>

            <%--------------------------------------------------------------------------------------------------------------------------------------------------------------------%>
            <%-- GRIGLIA MENU MASTER --%>
            <%--------------------------------------------------------------------------------------------------------------------------------------------------------------------%>
            <div class="row">
                <asp:Panel ID="pnlTableMaster" runat="server">
                    <telerik:RadGrid ID="grdMenuMasterItem" runat="server" RenderMode="Lightweight" AllowPaging="True" AllowSorting="True" EnableLinqExpressions="false"
                        OnNeedDataSource="grdMenuMasterItem_NeedDataSource" Width="670px" AllowFilteringByColumn="True" Culture="it-IT" GroupPanelPosition="Top" PageSize="50"
                        ClientSettings-EnablePostBackOnRowClick="true">

                        <ClientSettings>
                            <Selecting AllowRowSelect="True" EnableDragToSelectRows="false" />
                        </ClientSettings>

                        <MasterTableView AutoGenerateColumns="false" TableLayout="Fixed" CssClass="RadGrid_WebBlue"
                            DataKeyNames="MenuMasterID, MenuMasterDescription, MenuMasterOrder, MenuMasterPage, MenuMasterVisible">

                            <Columns>
                                <telerik:GridButtonColumn CommandName="Modify" Text="Edit" UniqueName="EditColumn"
                                    ButtonType="ImageButton" ImageUrl="~/Content/GridTelerik/Edit.gif">
                                    <HeaderStyle Width="20px" />
                                </telerik:GridButtonColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="MenuMasterDescription" HeaderText="Menu" UniqueName="MenuMasterDescription"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                    <HeaderStyle Width="75px" />
                                </telerik:GridBoundColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="MenuMasterPage" HeaderText="Rif. pagina" UniqueName="MenuMasterPage"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                    <HeaderStyle Width="75px" />
                                </telerik:GridBoundColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="MenuMasterOrder" HeaderText="Num. ordine" UniqueName="MenuMasterOrder"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                    <HeaderStyle Width="75px" />
                                </telerik:GridBoundColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridButtonColumn CommandName="Translate" Text="Translate" UniqueName="Translate"
                                    ButtonType="ImageButton" ImageUrl="Images/Translate.ico" Resizable="false">
                                    <HeaderStyle Width="20px" />
                                </telerik:GridButtonColumn>
                            </Columns>

                        </MasterTableView>
                    </telerik:RadGrid>
                </asp:Panel>
            </div>

            <div class="clr"></div>

            <%--------------------------------------------------------------------------------------------------------------------------------------------------------------------%>
            <%-- GRIGLIA MENU ITEM --%>
            <%--------------------------------------------------------------------------------------------------------------------------------------------------------------------%>
            <div class="row" style="margin-top: 25px;">
                <telerik:RadLabel runat="server" ID="lblSubMenuLabel" Text=""></telerik:RadLabel>
            </div>

            <div class="clr"></div>

            <div class="row">
                <asp:Panel ID="pnlTableDetail" runat="server">
                    <telerik:RadGrid ID="grdMenuDetailItem" runat="server" RenderMode="Lightweight" AllowPaging="True" AllowSorting="True" EnableLinqExpressions="false"
                        OnNeedDataSource="grdMenuDetailItem_NeedDataSource" Width="670px" AllowFilteringByColumn="True" Culture="it-IT" GroupPanelPosition="Top" PageSize="50">

                        <MasterTableView AutoGenerateColumns="false" TableLayout="Fixed" CssClass="RadGrid_WebBlue" CommandItemDisplay="Top"
                            DataKeyNames="IdMenuMaster, MenuDetailID, MenuDetailDescription, MenuDetailPage, MenuDetailOrder, MenuDetailVisible, IsVisibleTo">

                            <CommandItemTemplate>
                                <asp:Button ID="btnAddSubMenuItem" Text="Add new sub menu item" runat="server" OnClick="AddSubMenuItem_Click" CssClass="RadButton RadButton_Default rbButton rbRounded btnShadows"></asp:Button>
                            </CommandItemTemplate>

                            <Columns>
                                <telerik:GridButtonColumn CommandName="Modify" Text="Edit" UniqueName="EditColumn"
                                    ButtonType="ImageButton" ImageUrl="~/Content/GridTelerik/Edit.gif">
                                    <HeaderStyle Width="20px" />
                                </telerik:GridButtonColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="MenuDetailDescription" HeaderText="Menu" UniqueName="MenuDetailDescription"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                    <HeaderStyle Width="75px" />
                                </telerik:GridBoundColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="MenuDetailPage" HeaderText="Rif. pagina" UniqueName="MenuDetailPage"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                    <HeaderStyle Width="75px" />
                                </telerik:GridBoundColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="MenuDetailOrder" HeaderText="Ordinamento" UniqueName="MenuDetailOrder"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                    <HeaderStyle Width="75px" />
                                </telerik:GridBoundColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="MenuDetailVisible" HeaderText="Visibile" UniqueName="MenuDetailVisible"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                    <HeaderStyle Width="75px" />
                                </telerik:GridBoundColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="IsVisibleTo" HeaderText="Visibile a" UniqueName="IsVisibleTo"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                    <HeaderStyle Width="75px" />
                                </telerik:GridBoundColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridButtonColumn CommandName="Translate" Text="Translate" UniqueName="Translate"
                                    ButtonType="ImageButton" ImageUrl="Images/Translate.ico" Resizable="false">
                                    <HeaderStyle Width="20px" />
                                </telerik:GridButtonColumn>
                            </Columns>

                        </MasterTableView>

                    </telerik:RadGrid>

                </asp:Panel>
            </div>

            <div class="clr"></div>

            <%--------------------------------------------------------------------------------------------------------------------------------------------------------------------%>
            <%-- PANNELLO DI GESTIONE DEI MENU PRINCIPALI --%>
            <%--------------------------------------------------------------------------------------------------------------------------------------------------------------------%>
            <div class="row">
                <input id="hdnPanelMenu" type="hidden" name="hddclickMenuMaster" runat="server" />
                <ajaxToolkit:ModalPopupExtender ID="mpeMain" runat="server" PopupControlID="pnlNewMenu" TargetControlID="hdnPanelMenu"
                    BackgroundCssClass="modalBackground" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>

                <asp:UpdatePanel ID="pnlMenuUpdate" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel runat="server" ID="pnlNewMenu" CssClass="modalPopup" Style="display: none;">
                            <div class="pnlheader">
                                <asp:Label ID="lblPanelTitle" runat="server" Text="Manage menu"></asp:Label>
                            </div>

                            <div class="pnlbody">
                                <!-- GESTIONE DELLA TABELLA CON DIV -->
                                <div class="divTable">
                                    <div class="divTableBody">
                                        <div class="divTableRow">
                                            <div class="divTableCell" style="width: 200px;">
                                                <asp:Label ID="lblMenuDescription" runat="server" Text="Description menu"></asp:Label>
                                            </div>
                                            <div class="divTableCell" style="width: 580px;">
                                                <telerik:RadTextBox ID="txtMenutDescription" runat="server" RenderMode="Lightweight" ClientIDMode="Static" Resize="None">
                                                </telerik:RadTextBox>
                                            </div>
                                        </div>

                                        <div class="divTableRow">
                                            <div class="divTableCell" style="width: 200px;">
                                                <asp:Label ID="lblMenuRefPage" runat="server" Text="Ref. page"></asp:Label>
                                            </div>
                                            <div class="divTableCell" style="width: 580px;">
                                                <telerik:RadTextBox ID="txtMenuRefPage" runat="server" RenderMode="Lightweight" ClientIDMode="Static" Resize="None">
                                                </telerik:RadTextBox>
                                            </div>
                                        </div>

                                        <div class="divTableRow">
                                            <div class="divTableCell" style="width: 200px;">
                                                <asp:Label ID="lblMenuOrderNum" runat="server" Text="Order num."></asp:Label>
                                            </div>
                                            <div class="divTableCell" style="width: 580px;">
                                                <telerik:RadTextBox ID="txtMenuOrderNum" runat="server" RenderMode="Lightweight" ClientIDMode="Static" Resize="None">
                                                </telerik:RadTextBox>
                                            </div>
                                        </div>

                                        <div class="divTableRow">
                                            <div class="divTableCell" style="width: 200px;"></div>
                                            <div class="divTableCell" style="width: 580px;">
                                                <telerik:RadCheckBox ID="chkVisible" runat="server" Text="Visible" AutoPostBack="false" RenderMode="Lightweight"></telerik:RadCheckBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="pnlfooter">
                                <telerik:RadButton ID="btnHideMenu" runat="server" Text="Close" Width="80px"
                                    RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                                </telerik:RadButton>

                                <telerik:RadButton ID="btnOKMenu" runat="server" Text="Save" Width="80px"
                                    RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                                </telerik:RadButton>
                            </div>

                            <div style="text-align: center;">
                                <asp:Label runat="server" ID="txtMessageMenu" Visible="false"></asp:Label>
                            </div>

                        </asp:Panel>

                    </ContentTemplate>

                </asp:UpdatePanel>
            </div>

            <div class="clr"></div>

            <%--------------------------------------------------------------------------------------------------------------------------------------------------------------------%>
            <%-- PANNELLO DI GESTIONE DELLE TRADUZIONI DEI MENU PRINCIPALI --%>
            <%--------------------------------------------------------------------------------------------------------------------------------------------------------------------%>
            <div class="row">
                <input id="hdnPanelTranslationsMenu" type="hidden" name="hddclickMenuMasterTranslation" runat="server" />
                <ajaxToolkit:ModalPopupExtender ID="mpeTranslateMenu" runat="server" PopupControlID="pnlTranslateMenu" TargetControlID="hdnPanelTranslationsMenu"
                    BackgroundCssClass="modalBackground" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>

                <asp:Panel runat="server" ID="pnlTranslateMenu" CssClass="modalPopup" Style="display: none;">
                    <asp:UpdatePanel ID="pnlUpdateTranslateMenu" runat="server" UpdateMode="Conditional">

                        <ContentTemplate>

                            <div class="pnlheader">
                                <asp:Label ID="lblTitleTranslation" runat="server" Text="Manage translation menu"></asp:Label>
                            </div>

                            <div class="pnlbody">
                                <!-- GESTIONE DELLA TABELLA CON DIV -->
                                <div class="divTable">
                                    <div class="divTableBody">
                                        <div class="divTableRow">
                                            <div class="divTableCell" style="width: 90%">
                                                <asp:Label ID="lblMenuName" runat="server" Text="Menu name" Width="100px"></asp:Label>
                                                <telerik:RadTextBox ID="txtMenuNameTranslate" runat="server" RenderMode="Lightweight" Width="370px" ClientIDMode="Static" ReadOnly="true"></telerik:RadTextBox>
                                            </div>
                                        </div>

                                        <div class="divTableRow">
                                            <div class="divTableCell" style="width: 90%">
                                                <asp:Label ID="lblSelectLanguage" runat="server" Text="Select language" Width="100px"></asp:Label>
                                                <telerik:RadComboBox ID="cboLanguage" runat="server" Width="370px" EmptyMessage="< Select language...>"
                                                    RenderMode="Lightweight" RenderingMode="Full" ZIndex="1000000" ClientIDMode="Static">
                                                </telerik:RadComboBox>
                                            </div>
                                        </div>

                                        <div class="divTableRow">
                                            <div class="divTableCell" style="width: 90%">
                                                <asp:Label ID="lblMenuTranslation" runat="server" Text="Menu translation" Width="100px"></asp:Label>
                                                <telerik:RadTextBox ID="txtMenuTranslation" runat="server" RenderMode="Lightweight" Width="370px" ClientIDMode="Static" Resize="None">
                                                </telerik:RadTextBox>
                                            </div>
                                        </div>

                                        <div class="divTableRow">
                                            <div class="divTableCell" style="width: 90%">
                                                <asp:Label ID="lblTranslationAvailable" runat="server" Text="Translations available" Width="250px"></asp:Label>
                                                <asp:Panel ID="pnlGridTranslation" runat="server">
                                                    <telerik:RadGrid ID="grdTranslationMenu" runat="server" RenderMode="Lightweight" AllowPaging="True" AllowSorting="True"
                                                        CellSpacing="0" GridLines="None" OnNeedDataSource="grdTranslationsMenu_NeedDataSource" Width="480px" PageSize="30">
                                                        <MasterTableView AutoGenerateColumns="false" TableLayout="Fixed" CssClass="RadGrid_WebBlue"
                                                            DataKeyNames="ID, IDMenu, IDLanguage, Translation"
                                                            NoMasterRecordsText="No translations available">

                                                            <Columns>
                                                                <telerik:GridButtonColumn CommandName="Modify" Text="Edit" UniqueName="EditColumnTranslation"
                                                                    ButtonType="ImageButton" ImageUrl="~/Content/GridTelerik/Edit.gif">
                                                                    <HeaderStyle Width="20px" />
                                                                </telerik:GridButtonColumn>
                                                            </Columns>

                                                            <Columns>
                                                                <telerik:GridBoundColumn DataField="Translation" HeaderText="Menu" UniqueName="Translation">
                                                                    <HeaderStyle Width="230px" />
                                                                </telerik:GridBoundColumn>
                                                            </Columns>

                                                            <Columns>
                                                                <telerik:GridBoundColumn DataField="Code" HeaderText="Lang." UniqueName="Code">
                                                                    <HeaderStyle Width="80px" />
                                                                </telerik:GridBoundColumn>
                                                            </Columns>

                                                        </MasterTableView>
                                                    </telerik:RadGrid>

                                                    <asp:PlaceHolder runat="server" ID="MessageTranslation" Visible="false">
                                                        <p class="text-danger">
                                                            <asp:Literal runat="server" ID="txtMessageTranslation" />
                                                        </p>
                                                    </asp:PlaceHolder>

                                                </asp:Panel>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="pnlfooter">
                                <telerik:RadButton ID="btnHideTranslation" runat="server" Text="Close" Width="80px"
                                    RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                                </telerik:RadButton>

                                <telerik:RadButton ID="btnSaveTranslation" runat="server" Text="Save" Width="80px"
                                    RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                                </telerik:RadButton>
                            </div>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>

            </div>

            <div class="clr"></div>

            <%--------------------------------------------------------------------------------------------------------------------------------------------------------------------%>
            <%-- PANNELLO DI GESTIONE DEI SOTTOMENU --%>
            <%--------------------------------------------------------------------------------------------------------------------------------------------------------------------%>
            <div class="row">
                <input id="hddSubMenu" type="hidden" name="hddclickSubMenuItem" runat="server" />
                <ajaxToolkit:ModalPopupExtender ID="mpeSubMenuItem" runat="server" PopupControlID="pnlSubMenuItem" TargetControlID="hddSubMenu"
                    BackgroundCssClass="modalBackground" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>

                <asp:Panel runat="server" ID="pnlSubMenuItem" CssClass="modalPopup" Style="display: none;">
                    <asp:UpdatePanel ID="pnlSubMenuItemUpdate" runat="server" UpdateMode="Conditional">

                        <ContentTemplate>

                            <div class="pnlheader">
                                <asp:Label ID="lblSubMenuTitle" runat="server" Text="Manage menu"></asp:Label>
                            </div>

                            <div class="pnlbody">
                                <!-- GESTIONE DELLA TABELLA CON DIV -->
                                <div class="divTable">
                                    <div class="divTableBody">
                                        <div class="divTableRow">
                                            <div class="divTableCell" style="width: 200px;">
                                                <asp:Label ID="lblSubMenuName" runat="server" Text="Description menu"></asp:Label>
                                            </div>
                                            <div class="divTableCell" style="width: 580px;">
                                                <telerik:RadTextBox ID="txtSubMenuName" runat="server" RenderMode="Lightweight" ClientIDMode="Static" Resize="None">
                                                </telerik:RadTextBox>
                                            </div>
                                        </div>

                                        <div class="divTableRow">
                                            <div class="divTableCell" style="width: 200px;">
                                                <asp:Label ID="lblSubMenuRefPage" runat="server" Text="Ref. page"></asp:Label>
                                            </div>
                                            <div class="divTableCell" style="width: 580px;">
                                                <telerik:RadTextBox ID="txtSubMenuRefPage" runat="server" RenderMode="Lightweight" ClientIDMode="Static" Resize="None">
                                                </telerik:RadTextBox>
                                            </div>
                                        </div>

                                        <div class="divTableRow">
                                            <div class="divTableCell" style="width: 200px;">
                                                <asp:Label ID="lblSubMenuOrderNum" runat="server" Text="Order num."></asp:Label>
                                            </div>
                                            <div class="divTableCell" style="width: 580px;">
                                                <telerik:RadTextBox ID="txtSubMenuOrderNum" runat="server" RenderMode="Lightweight" ClientIDMode="Static" Resize="None">
                                                </telerik:RadTextBox>
                                            </div>
                                        </div>

                                        <div class="divTableRow">
                                            <div class="divTableCell" style="width: 200px;"></div>
                                            <div class="divTableCell" style="width: 580px;">
                                                <telerik:RadCheckBox ID="chkSubMenuVisible" runat="server" Text="Visible" AutoPostBack="false" RenderMode="Lightweight"></telerik:RadCheckBox>
                                            </div>
                                        </div>

                                        <div class="divTableRow">
                                            <div class="divTableCell" style="width: 200px;">
                                                <asp:Label ID="lblSubMenuVisibleTo" runat="server" Text="Visible to"></asp:Label>
                                            </div>
                                            <div class="divTableCell" style="width: 580px;">
                                                <telerik:RadTextBox ID="txtSubMenuVisibleTo" runat="server" RenderMode="Lightweight" ClientIDMode="Static" Resize="None">
                                                </telerik:RadTextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="pnlfooter">
                                <telerik:RadButton ID="btnSubMenuClose" runat="server" Text="Close" Width="80px"
                                    RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                                </telerik:RadButton>

                                <telerik:RadButton ID="btnSubMenuSave" runat="server" Text="Save" Width="80px"
                                    RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                                </telerik:RadButton>

                                <div class="row" style="min-height: 20px;"></div>

                                <div class="row">
                                    <asp:PlaceHolder runat="server" ID="MessageSubMenu" Visible="false">
                                        <p class="text-danger">
                                            <asp:Literal runat="server" ID="MessageSubMenuText" />
                                        </p>
                                    </asp:PlaceHolder>
                                </div>
                            </div>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>

            </div>
            
            <%--------------------------------------------------------------------------------------------------------------------------------------------------------------------%>
            <%-- PANNELLO DI GESTIONE DELLE TRADUZIONI DEI SOTTO MENU --%>
            <%--------------------------------------------------------------------------------------------------------------------------------------------------------------------%>
            <div class="row">
                <input id="hdnPanelTranslationsSubMenu" type="hidden" name="hddclickSubMenuTranslation" runat="server" />
                <ajaxToolkit:ModalPopupExtender ID="mpeTranslateSubMenu" runat="server" PopupControlID="pnlTranslateSubMenu" TargetControlID="hdnPanelTranslationsSubMenu"
                    BackgroundCssClass="modalBackground" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>

                <asp:Panel runat="server" ID="pnlTranslateSubMenu" CssClass="modalPopup" Style="display: none;">
                    <asp:UpdatePanel ID="pnlUpdateTranslateSubMenu" runat="server" UpdateMode="Conditional">

                        <ContentTemplate>

                            <div class="pnlheader">
                                <asp:Label ID="lblTitleTranslationSubMenu" runat="server" Text="Manage translation menu"></asp:Label>
                            </div>

                            <div class="pnlbody">
                                <!-- GESTIONE DELLA TABELLA CON DIV -->
                                <div class="divTable">
                                    <div class="divTableBody">
                                        <div class="divTableRow">
                                            <div class="divTableCell" style="width: 90%">
                                                <asp:Label ID="lblSubMenuNameTranslation" runat="server" Text="Menu name" Width="100px"></asp:Label>
                                                <telerik:RadTextBox ID="txtSubMenuNameTranslation" runat="server" RenderMode="Lightweight" Width="370px" ClientIDMode="Static" ReadOnly="true"></telerik:RadTextBox>
                                            </div>
                                        </div>

                                        <div class="divTableRow">
                                            <div class="divTableCell" style="width: 90%">
                                                <asp:Label ID="lblSelectLanguageSubMenu" runat="server" Text="Select language" Width="100px"></asp:Label>
                                                <telerik:RadComboBox ID="cboLanguageSubMenu" runat="server" Width="370px" EmptyMessage="< Select language...>"
                                                    RenderMode="Lightweight" RenderingMode="Full" ZIndex="1000000" ClientIDMode="Static">
                                                </telerik:RadComboBox>
                                            </div>
                                        </div>

                                        <div class="divTableRow">
                                            <div class="divTableCell" style="width: 90%">
                                                <asp:Label ID="lblSubMenuTranslation" runat="server" Text="Menu translation" Width="100px"></asp:Label>
                                                <telerik:RadTextBox ID="txtSubMenuTranslation" runat="server" RenderMode="Lightweight" Width="370px" ClientIDMode="Static" Resize="None">
                                                </telerik:RadTextBox>
                                            </div>
                                        </div>

                                        <div class="divTableRow">
                                            <div class="divTableCell" style="width: 90%">
                                                <asp:Label ID="lblTranslationSubMenuAvailable" runat="server" Text="Translations available" Width="250px"></asp:Label>
                                                <asp:Panel ID="pnlGridSubMenuTranslation" runat="server">
                                                    <telerik:RadGrid ID="grdTranslationSubMenu" runat="server" RenderMode="Lightweight" AllowPaging="True" AllowSorting="True"
                                                        CellSpacing="0" GridLines="None" OnNeedDataSource="grdTranslationSubMenu_NeedDataSource" Width="480px" PageSize="30">
                                                        <MasterTableView AutoGenerateColumns="false" TableLayout="Fixed" CssClass="RadGrid_WebBlue"
                                                            DataKeyNames="ID, IDMenu, IDLanguage, Translation, Code "
                                                            NoMasterRecordsText="No translations available">

                                                            <Columns>
                                                                <telerik:GridButtonColumn CommandName="Modify" Text="Edit" UniqueName="EditColumnTranslation"
                                                                    ButtonType="ImageButton" ImageUrl="~/Content/GridTelerik/Edit.gif">
                                                                    <HeaderStyle Width="20px" />
                                                                </telerik:GridButtonColumn>
                                                            </Columns>

                                                            <Columns>
                                                                <telerik:GridBoundColumn DataField="Translation" HeaderText="Menu" UniqueName="Translation">
                                                                    <HeaderStyle Width="230px" />
                                                                </telerik:GridBoundColumn>
                                                            </Columns>

                                                            <Columns>
                                                                <telerik:GridBoundColumn DataField="Code" HeaderText="Lang." UniqueName="Code">
                                                                    <HeaderStyle Width="80px" />
                                                                </telerik:GridBoundColumn>
                                                            </Columns>

                                                        </MasterTableView>
                                                    </telerik:RadGrid>

                                                    <asp:PlaceHolder runat="server" ID="MessageTranslationSubMenu" Visible="false">
                                                        <p class="text-danger">
                                                            <asp:Literal runat="server" ID="MessageTranslationSubMenuText" />
                                                        </p>
                                                    </asp:PlaceHolder>

                                                </asp:Panel>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="pnlfooter">
                                <telerik:RadButton ID="btnCloseSubMenuTranslation" runat="server" Text="Close" Width="80px"
                                    RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                                </telerik:RadButton>

                                <telerik:RadButton ID="btnSaveSubMenuTranslation" runat="server" Text="Save" Width="80px"
                                    RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                                </telerik:RadButton>
                            </div>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>

            </div>

            <div class="clr"></div>


            <div class="row" style="min-height: 20px;"></div>

            <div class="row">
                <asp:PlaceHolder runat="server" ID="Message" Visible="false">
                    <p class="text-danger">
                        <asp:Literal runat="server" ID="MessageText" />
                    </p>
                </asp:PlaceHolder>
            </div>

            <div class="clr"></div>

            <div class="row">
                <div class="divTableCell" style="text-align: right;">
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
