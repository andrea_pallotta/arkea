﻿Imports System.Data
Imports Telerik.Web.UI
Imports APS
Imports APPCore.Utility
Imports DALC4NET

Partial Class Developer_Menu_Manager
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster

    Private Sub LoadLanguages()
        Dim sSQL As String
        Dim oDTLanguages As DataTable

        sSQL = "SELECT -1 AS ID, '' AS Language UNION ALL SELECT ID, Language FROM Languages ORDER BY ID"
        oDTLanguages = g_DAL.ExecuteDataTable(sSQL)

        ' Caricamento della combo per la traduzione dei menu principali
        cboLanguage.DataSource = oDTLanguages
        cboLanguage.DataValueField = "ID"
        cboLanguage.DataTextField = "Language"
        cboLanguage.DataBind()

        cboLanguage.SelectedValue = -1

        ' Caricamento della combo per la traduzione dei sotto menu
        cboLanguageSubMenu.DataSource = oDTLanguages
        cboLanguageSubMenu.DataValueField = "ID"
        cboLanguageSubMenu.DataTextField = "Language"
        cboLanguageSubMenu.DataBind()

        cboLanguageSubMenu.SelectedValue = -1

    End Sub

    Private Sub ClearPanelMenu()
        txtMenutDescription.Text = ""
        txtMenuRefPage.Text = ""
        txtMenuOrderNum.Text = ""
        chkVisible.Checked = False

        txtMessageMenu.Visible = False
    End Sub

    Private Sub ClearPanelSubMenu()
        txtSubMenuName.Text = ""
        txtSubMenuRefPage.Text = ""
        txtSubMenuOrderNum.Text = ""
        chkSubMenuVisible.Checked = False

        MessageSubMenu.Visible = False
    End Sub

    Private Sub ClearPanelMenuTranslation()
        cboLanguage.SelectedValue = -1
        txtMenuTranslation.Text = ""
    End Sub

    Private Sub ClearPanelSubMenuTranslation()
        cboLanguage.SelectedValue = -1
        txtSubMenuTranslation.Text = ""
        grdTranslationSubMenu.Rebind()
    End Sub

    Protected Sub grdMenuMasterItem_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs)
        grdMenuMasterItem.DataSource = LoadDataMasterMenu()
    End Sub

    Private Function LoadDataMasterMenu() As DataTable
        Dim sSQL As String

        sSQL = "SELECT MM.Id AS MenuMasterID, MM.Description AS MenuMasterDescription, MM.OrderNum AS MenuMasterOrder, " _
             & "MM.RefPage AS MenuMasterPage, MM.Visible AS MenuMasterVisible " _
             & "FROM Menu_Master MM WHERE MM.IDGame = " & Session("IDGame") & " " _
             & "ORDER BY MM.OrderNum "

        Return g_DAL.ExecuteDataTable(sSQL)
    End Function

    Protected Sub grdMenuDetailItem_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs)
        grdMenuDetailItem.DataSource = LoadDataDetailItem()
    End Sub

    Private Function LoadDataMasterMenuTranslation() As DataTable
        Dim sSQL As String

        If Not IsNothing(Session("MenuMasterID")) AndAlso Nni(Session("MenuMasterID")) > 0 Then
            sSQL = "SELECT MMT.ID, IDMenu, IDLanguage, Translation, L.Code " _
                 & "FROM Menu_Master_Translation MMT " _
                 & "LEFT JOIN Languages L ON MMT.IDLanguage = L.ID " _
                 & "WHERE IDMenu = " & Session("MenuMasterID") & " "

            If g_DAL Is Nothing Then g_DAL = New DBHelper
            Return g_DAL.ExecuteDataTable(sSQL)

        Else
            Return Nothing
        End If

    End Function

    Private Function LoadDataSubMenuTranslation() As DataTable
        Dim sSQL As String

        If Not IsNothing(Session("SubMenuID")) AndAlso Nni(Session("SubMenuID")) > 0 Then
            sSQL = "SELECT MDT.ID, IDMenu, IDLanguage, Translation, L.Code " _
                 & "FROM Menu_Detail_Translation MDT " _
                 & "LEFT JOIN Languages L ON MDT.IDLanguage = L.ID " _
                 & "WHERE MDT.IDMenu = " & Session("SubMenuID") & " "

            Return g_DAL.ExecuteDataTable(sSQL)

        Else
            Return Nothing
        End If

    End Function

    Protected Sub grdTranslationsMenu_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs)
        grdTranslationMenu.DataSource = LoadDataMasterMenuTranslation()
    End Sub

    Protected Sub grdTranslationSubMenu_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs)
        grdTranslationSubMenu.DataSource = LoadDataSubMenuTranslation()
    End Sub

    Private Function LoadDataDetailItem() As DataTable
        Dim sSQL As String
        Dim dtRet As DataTable

        lblSubMenuLabel.Visible = False

        Try
            If Not IsNothing(grdMenuMasterItem.SelectedValues) AndAlso Nni(grdMenuMasterItem.SelectedValues("MenuMasterID")) > 0 Then

                sSQL = "SELECT MD.Id AS MenuDetailID, MD.IdMenuMaster, MD.Description AS MenuDetailDescription, MD.RefPage AS MenuDetailPage, MD.OrderNum AS MenuDetailOrder, " _
                     & "MD.Visible AS MenuDetailVisible, MD.IsVisibleTo " _
                     & "FROM Menu_Detail MD WHERE MD.IDMenuMaster = " & Nni(grdMenuMasterItem.SelectedValues("MenuMasterID")) & " " _
                     & "ORDER BY MD.OrderNum "

                dtRet = g_DAL.ExecuteDataTable(sSQL)

                lblSubMenuLabel.Text = "Menu -> " & Nz(Session("DescMenuMaster"))
                lblSubMenuLabel.Visible = True

                Return dtRet
            Else
                Return Nothing
                lblSubMenuLabel.Visible = False
            End If
        Catch ex As Exception
            If ex.HResult <> "-2146233086" Then ' Errore generato da una non selezione nella griglia, al momento lo bypasso così... (brutto brutto)
                MessageText.Text = "LoadDataDetailItem -> " & ex.Message
            End If
        End Try

    End Function

    Private Sub Developer_Menu_Manager_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If grdMenuMasterItem.SelectedIndexes.Count = 0 AndAlso grdMenuDetailItem.SelectedIndexes.Count = 0 Then
            grdMenuMasterItem.SelectedIndexes.Add(-1)
            grdMenuDetailItem.SelectedIndexes.Add(-1)
        End If
    End Sub

    Private Sub grdMenuMasterItem_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdMenuMasterItem.SelectedIndexChanged
        Session("IDMenuMaster") = Nni(grdMenuMasterItem.SelectedValues("MenuMasterID"))
        Session("DescMenuMaster") = Nz(grdMenuMasterItem.SelectedValues("MenuMasterDescription"))
        grdMenuDetailItem.Rebind()
        pnlSubMenuItemUpdate.Update()
    End Sub

    Private Sub grdMenuMasterItem_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles grdMenuMasterItem.ItemCommand
        If e.CommandName.ToUpper <> "PAGE" AndAlso e.CommandName.ToUpper <> "SORT" AndAlso e.CommandName.ToUpper <> "FILTER" Then
            Session("MenuSelected") = DirectCast(e.Item, GridDataItem)

            If e.CommandName.ToUpper = "MODIFY" Then
                ClearPanelMenu()
                mpeMain.Show()
                UpdatePanelMain.Update()

            ElseIf e.CommandName.ToUpper = "TRANSLATE" Then
                ClearPanelMenuTranslation()
                mpeTranslateMenu.Show()
                UpdatePanelMain.Update()

            End If
        End If
    End Sub

    Private Sub pnlMenuUpdate_PreRender(sender As Object, e As EventArgs) Handles pnlMenuUpdate.PreRender
        Dim oItem As GridDataItem = DirectCast(Session("MenuSelected"), GridDataItem)

        ' Procedo con il caricamento dei dati da inserire nel panello
        ' Recupero l'id della variabile selezionata
        If Not oItem Is Nothing Then
            lblPanelTitle.Text = "Manage - " & oItem.GetDataKeyValue("MenuMasterDescription").ToString

            Session("MenuMasterID") = oItem.GetDataKeyValue("MenuMasterID")

            txtMenutDescription.Text = Nz(oItem.GetDataKeyValue("MenuMasterDescription"))
            txtMenuRefPage.Text = Nz(oItem.GetDataKeyValue("MenuMasterPage"))
            txtMenuOrderNum.Text = Nz(oItem.GetDataKeyValue("MenuMasterOrder"))
            chkVisible.Checked = Nb(oItem.GetDataKeyValue("MenuMasterVisible"))
        End If

    End Sub

    Private Sub btnNewMenu_Click(sender As Object, e As EventArgs) Handles btnNewMenu.Click
        Session.Remove("MenuSelected")
        Session.Remove("MenuMasterID")
        ClearPanelMenu()
        mpeMain.Show()
    End Sub

    Private Sub btnOKMenu_Click(sender As Object, e As EventArgs) Handles btnOKMenu.Click
        Dim sSQL As String

        ' Procedo con l'aggiornamento della nuova voce di menu, altrimenti aggiorno quella esistente
        If Not IsNothing(Session("MenuMasterID")) AndAlso Nni(Session("MenuMasterID")) > 0 Then
            sSQL = "UPDATE Menu_Master SET " _
                 & "Description = '" & txtMenutDescription.Text & "', " _
                 & "OrderNum = " & txtMenuOrderNum.Text & ", " _
                 & "RefPage = '" & txtMenuRefPage.Text & "', " _
                 & "Visible = " & Boolean_To_Bit(chkVisible.Checked) & " " _
                 & "WHERE ID = " & Nni(Session("MenuMasterID"))
        Else
            sSQL = "INSERT INTO Menu_Master (Description, OrderNum, RefPage, Visible, IDGame) VALUES " _
                 & "('" & txtMenutDescription.Text & "', " & txtMenuOrderNum.Text & ", '" & txtMenuRefPage.Text & "', " & Boolean_To_Bit(chkVisible.Checked) & ", " & Session("IDGame") & ") "

        End If

        If sSQL <> "" Then
            g_DAL.ExecuteNonQuery(sSQL)
            txtMessageMenu.Text = "Save completed..."
            txtMessageMenu.Visible = True
            grdMenuMasterItem.Rebind()

            UpdatePanelMain.Update()
        End If
    End Sub

    Private Sub Developer_Menu_Manager_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsNothing(m_SessionData) Then
            PageRedirect("ErrorTimeout.aspx", "", "")
        End If

        If Not Page.IsPostBack Then
            LoadLanguages()
        End If

    End Sub

    Private Sub pnlUpdateTranslateMenu_PreRender(sender As Object, e As EventArgs) Handles pnlUpdateTranslateMenu.PreRender
        Dim oItem As GridDataItem = DirectCast(Session("MenuSelected"), GridDataItem)

        ' Procedo con il caricamento dei dati da inserire nel panello
        ' Recupero l'id della variabile selezionata
        If Not oItem Is Nothing Then
            Session("MenuMasterID") = oItem.GetDataKeyValue("MenuMasterID")

            txtMenuNameTranslate.Text = Nz(oItem.GetDataKeyValue("MenuMasterDescription"))

            grdTranslationMenu.Rebind()
        End If
    End Sub

    Private Sub btnSaveTranslation_Click(sender As Object, e As EventArgs) Handles btnSaveTranslation.Click
        ' Procedo con il savataggio delle informazioni
        Dim sSQL As String

        If Nz(cboLanguage.SelectedValue) = "" OrElse Nni(cboLanguage.SelectedValue) <= 0 Then
            txtMessageTranslation.Text = "Please select a language"
            MessageTranslation.Visible = True
            mpeTranslateMenu.Show()
            pnlUpdateTranslateMenu.Update()

            Exit Sub
        End If

        ' Controllo se andare in modifica o inserimento
        If Nni(Session("IDTranslation")) > 0 Then ' Modifica
            sSQL = "UPDATE Menu_Master_Translation SET Translation = '" & HttpContext.Current.Server.HtmlEncode(CStrSql(txtMenuTranslation.Text)) & "' " _
                 & "WHERE ID = " & Nni(Session("IDTranslation"))
        Else ' Inserimento
            sSQL = "INSERT INTO Menu_Master_Translation (IDMenu, IDLanguage, Translation) VALUES (" _
                 & "" & Nni(Session("MenuMasterID")) & ", " & Nni(cboLanguage.SelectedValue) & ", '" & HttpContext.Current.Server.HtmlEncode(CStrSql(txtMenuTranslation.Text)) & "') "
        End If
        g_DAL.ExecuteNonQuery(sSQL)

        If Nni(Session("IDTranslation"), 0) = 0 Then ' Sono in inserimento, procedo con il caricamento dell'ultimo id inserito
            sSQL = "SELECT MAX(ID) FROM Menu_Master_Translation "
            Session("MenuMasterIDTranslation") = Nni(g_DAL.ExecuteScalar(sSQL))
        End If

        grdTranslationMenu.DataSource = Nothing
        grdTranslationMenu.Rebind()

        txtMessageTranslation.Text = "Update completed"
        MessageTranslation.Visible = True

        ClearPanelMenuTranslation()

        pnlUpdateTranslateMenu.Update()

        Session.Remove("IDTranslation")

        mpeTranslateMenu.Show()

    End Sub

    Private Sub grdTranslationMenu_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles grdTranslationMenu.ItemCommand
        If e.CommandName.ToUpper <> "PAGE" Then
            If e.CommandName.ToUpper = "MODIFY" Then
                Dim oItem As GridDataItem = DirectCast(e.Item, GridDataItem)
                Session("MenuMasterTranslationSelected") = DirectCast(e.Item, GridDataItem)

                ' Procedo con il caricamento dei dati da inserire nel panello
                ' Recupero l'id della variabile selezionata
                If Not oItem Is Nothing Then
                    Session("IDTranslation") = Nni(oItem.GetDataKeyValue("ID"))
                    txtMenuTranslation.Text = HttpContext.Current.Server.HtmlEncode(CStrSql(oItem.GetDataKeyValue("Translation")))
                    cboLanguage.SelectedValue = Nni(oItem.GetDataKeyValue("IDLanguage"))
                End If
                pnlUpdateTranslateMenu.Update()
                mpeTranslateMenu.Show()
            End If
        End If
    End Sub

    Private Sub btnHideTranslation_Click(sender As Object, e As EventArgs) Handles btnHideTranslation.Click
        Session.Remove("MenuMasterTranslationSelected")
        ClearPanelMenuTranslation()
        mpeTranslateMenu.Hide()
    End Sub

    Protected Sub AddSubMenuItem_Click(sender As Object, e As EventArgs)
        Session.Remove("SubMenuSelected")
        Session.Remove("SubMenuID")
        ClearPanelSubMenu()
        mpeSubMenuItem.Show()
    End Sub

    Private Sub btnSubMenuClose_Click(sender As Object, e As EventArgs) Handles btnSubMenuClose.Click
        Session.Remove("SubMenuSelected")
        Session.Remove("SubMenuID")
        Session.Remove("SubMenuIDMenuMaster")
        ClearPanelSubMenu()
        mpeSubMenuItem.Hide()
    End Sub

    Private Sub pnlSubMenuItemUpdate_PreRender(sender As Object, e As EventArgs) Handles pnlSubMenuItemUpdate.PreRender
        Dim oItem As GridDataItem = DirectCast(Session("SubMenuSelected"), GridDataItem)

        ' Procedo con il caricamento dei dati da inserire nel panello
        ' Recupero l'id della variabile selezionata
        If Not oItem Is Nothing Then
            Session("SubMenuID") = oItem.GetDataKeyValue("MenuDetailID")

            txtSubMenuName.Text = Nz(oItem.GetDataKeyValue("MenuDetailDescription"))
            txtSubMenuRefPage.Text = Nz(oItem.GetDataKeyValue("MenuDetailPage"))
            txtSubMenuOrderNum.Text = Nz(oItem.GetDataKeyValue("MenuDetailOrder"))
            chkSubMenuVisible.Checked = Nb(oItem.GetDataKeyValue("MenuDetailVisible"))
            txtSubMenuVisibleTo.Text = Nz(oItem.GetDataKeyValue("IsVisibleTo"))

            grdTranslationSubMenu.Rebind()
        End If
    End Sub

    Private Sub grdMenuDetailItem_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles grdMenuDetailItem.ItemCommand
        If e.CommandName.ToUpper <> "PAGE" AndAlso e.CommandName.ToUpper <> "SORT" AndAlso e.CommandName.ToUpper <> "FILTER" Then

            If e.CommandName.ToUpper = "MODIFY" Then
                Session("SubMenuSelected") = DirectCast(e.Item, GridDataItem)
                ClearPanelSubMenu()
                mpeSubMenuItem.Show()
                UpdatePanelMain.Update()

            ElseIf e.CommandName.ToUpper = "TRANSLATE" Then
                Session("SubMenuSelected") = DirectCast(e.Item, GridDataItem)
                ClearPanelSubMenu()
                mpeTranslateSubMenu.Show()
                UpdatePanelMain.Update()

            End If
        End If
    End Sub

    Private Sub btnSubMenuSave_Click(sender As Object, e As EventArgs) Handles btnSubMenuSave.Click
        Dim sSQL As String

        ' Procedo con l'aggiornamento della nuova voce di menu, altrimenti aggiorno quella esistente
        If Not IsNothing(Session("SubMenuID")) AndAlso Nni(Session("SubMenuID")) > 0 Then
            sSQL = "UPDATE Menu_Detail SET " _
                 & "Description = '" & txtSubMenuName.Text & "', " _
                 & "RefPage = '" & txtSubMenuRefPage.Text & "', " _
                 & "OrderNum = " & txtSubMenuOrderNum.Text & ", " _
                 & "Visible = " & Boolean_To_Bit(chkSubMenuVisible.Checked) & ", " _
                 & "IsVisibleTo = '" & txtSubMenuVisibleTo.Text & "' " _
                 & "WHERE ID = " & Nni(Session("SubMenuID"))
        Else
            sSQL = "INSERT INTO Menu_Detail (IDMenuMaster, Description, OrderNum, RefPage, Visible, IsVisibleTo) VALUES " _
                 & "(" & Nni(Session("IDMenuMaster")) & ", '" & txtSubMenuName.Text & "', " & txtSubMenuOrderNum.Text & ", '" _
                 & txtSubMenuRefPage.Text & "', " & Boolean_To_Bit(chkSubMenuVisible.Checked) & ", '" & txtSubMenuVisibleTo.Text & "') "

        End If

        If sSQL <> "" Then
            g_DAL.ExecuteNonQuery(sSQL)
            MessageSubMenuText.Text = "Save completed..."
            MessageSubMenu.Visible = True
            grdMenuDetailItem.Rebind()

            UpdatePanelMain.Update()
        End If
    End Sub

    Private Sub pnlUpdateTranslateSubMenu_PreRender(sender As Object, e As EventArgs) Handles pnlUpdateTranslateSubMenu.PreRender
        Dim oItem As GridDataItem = DirectCast(Session("SubMenuSelected"), GridDataItem)


        ' Procedo con il caricamento dei dati da inserire nel panello
        ' Recupero l'id della variabile selezionata
        If Not oItem Is Nothing Then
            Session("SubMenuIDTranslation") = oItem.GetDataKeyValue("ID")
            Session("SubMenuID") = oItem.GetDataKeyValue("MenuDetailID")

            txtSubMenuNameTranslation.Text = Nz(oItem.GetDataKeyValue("MenuDetailDescription"))

            grdTranslationSubMenu.Rebind()
        End If
        mpeTranslateSubMenu.Show()
    End Sub

    Private Sub btnCloseSubMenuTranslation_Click(sender As Object, e As EventArgs) Handles btnCloseSubMenuTranslation.Click
        Session.Remove("SubMenuSelected")
        Session.Remove("SubMenuID")
        Session.Remove("SubMenuIDTranslation")
        ClearPanelSubMenuTranslation()
        mpeTranslateSubMenu.Hide()
    End Sub

    Private Sub btnSaveSubMenuTranslation_Click(sender As Object, e As EventArgs) Handles btnSaveSubMenuTranslation.Click
        Dim sSQL As String

        ' Procedo con l'aggiornamento della nuova voce di menu, altrimenti aggiorno quella esistente
        sSQL = "DELETE FROM Menu_Detail_Translation " _
             & "WHERE IDLanguage = " & Nni(cboLanguageSubMenu.SelectedValue) & " " _
             & "AND IDMenu= " & Nni(Session("SubMenuID")) & " "
        g_DAL.ExecuteNonQuery(sSQL)

        sSQL = "INSERT INTO Menu_Detail_Translation (IDMenu, IDLanguage, Translation) VALUES " _
                   & "(" & Nni(Session("SubMenuID")) & ", " & Nni(cboLanguageSubMenu.SelectedValue) & ", '" & Nz(txtSubMenuTranslation.Text) & "') "
        g_DAL.ExecuteNonQuery(sSQL)
        MessageTranslationSubMenuText.Text = "Save completed..."
        MessageTranslationSubMenu.Visible = True
        grdTranslationSubMenu.Rebind()
        pnlUpdateTranslateSubMenu.Update()
        mpeTranslateSubMenu.Show()

    End Sub

    Private Sub grdTranslationSubMenu_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles grdTranslationSubMenu.ItemCommand
        If e.CommandName.ToUpper <> "PAGE" Then
            If e.CommandName.ToUpper = "MODIFY" Then

                Dim oItem As GridDataItem = DirectCast(e.Item, GridDataItem)
                Session("SubMenuTranslationSelected") = DirectCast(e.Item, GridDataItem)

                ' Procedo con il caricamento dei dati da inserire nel panello
                ' Recupero l'id della variabile selezionata
                If Not oItem Is Nothing Then
                    Session("SubMenuIDTranslation") = Nni(oItem.GetDataKeyValue("ID"))
                    txtSubMenuTranslation.Text = HttpContext.Current.Server.HtmlEncode(CStrSql(oItem.GetDataKeyValue("Translation")))
                    cboLanguageSubMenu.SelectedValue = Nni(oItem.GetDataKeyValue("IDLanguage"))
                End If

            End If
        End If
    End Sub

End Class
