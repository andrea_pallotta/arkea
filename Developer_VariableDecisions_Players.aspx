﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Developer_VariableDecisions_Players.aspx.vb" Inherits="Developer_VariableDecisions_Players" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="cntMain" ContentPlaceHolderID="MainContent" runat="Server" ClientIDMode="Static">

    <asp:UpdatePanel runat="server" ID="pnlMain" UpdateMode="Conditional">
        <ContentTemplate>
            <h2>
                <asp:Label ID="lblWelcome" runat="server" Text="Variables manager" />
            </h2>

            <div class="clr"></div>

            <div id="subTitle">
                <div id="subLeft" style="float: left; width: 30%;">
                    <h3>
                        <asp:Label ID="lblTitle" runat="server" Text="Developer decisions"></asp:Label>
                    </h3>
                </div>

                <div id="subRight" style="float: right; padding-right: 5px;">
                    <div id="divNewDecision" style="margin-top: 10px; margin-bottom: 5px; text-align: right;">
                        <telerik:RadButton ID="btnNewDecision" runat="server" Text="New decision"
                            RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                        </telerik:RadButton>
                    </div>
                </div>
            </div>

            <div class="row" style="padding-bottom: 5px;">
                <asp:Panel ID="pnlTable" runat="server">
                    <asp:Table ClientIDMode="Static" ID="tblDeveloperDecisions" runat="server" CssClass="DecisionsBoss">

                    </asp:Table>

                    <div id="divConfirmButton" style="margin-top: 20px; margin-bottom: 5px; text-align: center;">
                        <telerik:RadButton ID="btnSave" runat="server" Text="Confirm" Width="60%"
                            RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                        </telerik:RadButton>
                    </div>

                    <asp:PlaceHolder runat="server" ID="Message" Visible="false">
                        <p class="text-danger">
                            <asp:Literal runat="server" ID="MessageText" />
                        </p>
                    </asp:PlaceHolder>

                </asp:Panel>
            </div>

            <div class="clr"></div>

            <div class="row">
                <ajaxToolkit:ModalPopupExtender ID="mpeMain" runat="server" PopupControlID="pnlNewDecision" TargetControlID="btnNewDecision"
                    BackgroundCssClass="modalBackground">
                </ajaxToolkit:ModalPopupExtender>

                <asp:Panel runat="server" ID="pnlNewDecision" CssClass="modalPopup">
                    <div class="pnlheader">
                        <asp:Label ID="lblPanelTitle" runat="server" Text="Manage developer decision"></asp:Label>
                    </div>

                    <div class="pnlbody">
                        <!-- GESTIONE DELLA TABELLA CON DIV -->
                        <div class="divTable">
                            <div class="divTableBody">
                                <div class="divTableRow">
                                    <div class="divTableCell" style="width: 200px;">
                                        <asp:Label runat="server" ID="lblVariableName" Text="Nome variabile"></asp:Label>
                                    </div>
                                    <div class="divTableCell" style="width: 580px;">
                                        <telerik:RadTextBox ID="txtVariableName" runat="server" RenderMode="Lightweight" Width="82%" ClientIDMode="Static"></telerik:RadTextBox>
                                    </div>
                                </div>

                                <div class="divTableRow">
                                    <div class="divTableCell" style="width: 200px;">
                                        <asp:Label runat="server" ID="lblVariableLabelGroup" Text="Gruppo variabile"></asp:Label>
                                    </div>
                                    <div class="divTableCell" style="width: 580px;">
                                        <telerik:RadTextBox ID="txtVariableLabelGroup" runat="server" RenderMode="Lightweight" ClientIDMode="Static"></telerik:RadTextBox>
                                    </div>
                                </div>

                                <div class="divTableRow">
                                    <div class="divTableCell" style="width: 200px;">
                                        <asp:Label runat="server" ID="lblVariableLabel" Text="Descrizione"></asp:Label>
                                    </div>
                                    <div class="divTableCell" style="width: 580px;">
                                        <telerik:RadTextBox ID="txtVariableLabel" runat="server" RenderMode="Lightweight" ClientIDMode="Static"></telerik:RadTextBox>
                                    </div>
                                </div>

                                <div class="divTableRow">
                                    <div class="divTableCell" style="width: 200px;">
                                        <asp:Label runat="server" ID="lblDataType" Text="Tipo dato"></asp:Label>
                                    </div>
                                    <div class="divTableCell" style="width: 580px;">
                                        <telerik:RadComboBox ID="cboDataType" runat="server" EmptyMessage="< Select data type >"
                                            RenderMode="Lightweight" RenderingMode="Full" ZIndex="1000000" ClientIDMode="Static">
                                        </telerik:RadComboBox>
                                    </div>
                                </div>

                                <div class="divTableRow">
                                    <div class="divTableCell"></div>
                                    <div class="divTableCell">
                                        <telerik:RadCheckBox ID="chkItems" runat="server" Text="List of items" AutoPostBack="false" RenderMode="Lightweight"></telerik:RadCheckBox>
                                    </div>
                                </div>

                                <div class="divTableRow">
                                    <div class="divTableCell"></div>
                                    <div class="divTableCell">
                                        <telerik:RadCheckBox ID="chkPlayers" runat="server" Text="List of players" AutoPostBack="false" RenderMode="Lightweight"></telerik:RadCheckBox>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="pnlfooter">
                        <telerik:RadButton ID="btnHide" runat="server" Text="Cancel" Width="80px"
                            RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                        </telerik:RadButton>

                        <telerik:RadButton ID="btnOK" runat="server" Text="Save" Width="80px"
                            RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                        </telerik:RadButton>
                    </div>

                </asp:Panel>
            </div>

            <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="pnlMain">
                <ProgressTemplate>
                    <asp:Panel ID="Panel1" CssClass="overlay" runat="server">
                        <asp:Panel ID="Panel2" CssClass="loader" runat="server">
                            <img alt="" src="Images/Loading.gif" />
                        </asp:Panel>
                    </asp:Panel>
                </ProgressTemplate>
            </asp:UpdateProgress>

        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
