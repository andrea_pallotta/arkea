﻿Imports System.Data
Imports APS
Imports APPCore.Utility
Imports DALC4NET
Imports Telerik.Web.UI

Partial Class Developer_VariableDecisions_Players
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster

    Private Sub EnableSave()
        If Nni(Session("IDPeriod")) = HandleGetMaxPeriodValid(Session("IDGame")) Then
            btnSave.Enabled = True
        Else
            If Session("IDRole") = "P" Then
                btnSave.Enabled = False

            Else
                btnSave.Enabled = True
            End If
        End If
    End Sub

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadDecisions()
        EnableSave()
        Message.Visible = False

        pnlMain.Update()
    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadDecisions()
        EnableSave()
        Message.Visible = False

        pnlMain.Update()
    End Sub

    Private Sub LoadDecisions()
        Dim sSQL As String
        Dim oDTDecisions As DataTable
        Dim oDTDecisionsValue As DataTable
        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Dim sLabelLink As String = ""
        Dim sNomeLink As String = ""

        Dim sNomeTextBox As String = ""
        Dim sValue As String = ""

        Dim sTraduzione As String = ""

        Dim iIDDataType As Integer = 0

        ' Recupero le informazioni necessarie per costruirmi l'header della tabella HTML
        ' Recupero tutte le VariableLabelGroup per costruire i vari header, se ci sono 

        Dim oDTHeader As DataTable = LoadDeveloperDecisionsLabelGroup(Session("IDGame"))

        tblDeveloperDecisions.Rows.Clear()

        Try
            ' Allineo le decisioni con la tabella che contiene tutte le variabili, mi serve per la traduzione
            ' Cancello gli eventuali dati del vecchio game
            Dim oprmID As New DBParameter("@IDGame", Nni(Session("IDGame")), DbType.Int32)
            Dim oprmCollection As New DBParameterCollection()
            oprmCollection.Add(oprmID)
            g_DAL.ExecuteNonQuery("sp_Decisions_Developer_Sync_Variables", oprmCollection, CommandType.StoredProcedure)

            ' Procedo con il caricamento delle decisioni developer
            For Each drHeader As DataRow In oDTHeader.Rows
                ' Costruisco la riga Header
                tblHeaderRow = New TableHeaderRow
                tblHeaderCell = New TableHeaderCell

                tblHeaderCell.Text = Nz(drHeader("VariableLabelGroup"))
                tblHeaderCell.ColumnSpan = 3
                tblHeaderCell.BorderStyle = BorderStyle.None

                tblHeaderRow.Cells.Add(tblHeaderCell)

                tblDeveloperDecisions.Rows.Add(tblHeaderRow)

                ' Recupero tutte le decisini che hanno questo raggruppamento
                sSQL = "SELECT D.Id, D.IDDataType, D.IDGame, D.IDGroup, D.IDVariable, D.VariableLabel, ISNULL(LTRIM(RTRIM(VariableLabelGroup)), 'Decisions developer') AS VariableLabelGroup, D.VariableName " _
                     & "FROM Decisions_Developer D " _
                     & "WHERE D.IDGame = " & Nni(Session("IDGame")) _
                     & " AND ISNULL(LTRIM(RTRIM(VariableLabelGroup)), 'Decisions developer') = '" & Nz(drHeader("VariableLabelGroup")) & "' "
                oDTDecisions = g_DAL.ExecuteDataTable(sSQL)

                ' Ciclo sulle variabili appena caricate
                For Each drDecisions As DataRow In oDTDecisions.Rows
                    ' Con la decisione appena caricata recupero solo quelle valide del periodo su cui sono posizionato
                    sSQL = "SELECT DBV.Id, DBV.IDDecisionDeveloper, DBV.IDItem, DBV.IDPeriod, DBV.IDPlayer, DBV.Simulation, DBV.Value " _
                         & "FROM Decisions_Developer_Value DBV " _
                         & "WHERE DBV.IDDecisionDeveloper = " & Nni(drDecisions("ID")) _
                         & " AND ISNULL(IDPeriod, " & m_SiteMaster.PeriodGetCurrent & ") = " & m_SiteMaster.PeriodGetCurrent _
                         & " AND ISNULL(IDPlayer, " & m_SiteMaster.TeamIDGet & ") = " & m_SiteMaster.TeamIDGet
                    oDTDecisionsValue = g_DAL.ExecuteDataTable(sSQL)

                    For Each drDecisionsValue As DataRow In oDTDecisionsValue.Rows
                        If Nz(drDecisionsValue("Value")).ToUpper.Contains("LIST OF") Then ' al 17/01/2017 questa parte non è presente... vedremo in futuro
                            sNomeTextBox = Nz(drDecisions("VariableName")) & "_ID" & Nni(drDecisionsValue("ID")) & "_DecisionsValueList"
                        Else
                            sTraduzione = GetVariableTranslation(Session("IDGame"), Nni(drDecisions("IDVariable")), Nz(Session("LanguageActive")))
                            sLabelLink = sTraduzione
                            iIDDataType = Nni(drDecisions("IDDataType"))
                            sValue = Nz(drDecisionsValue("Value"))

                            sNomeTextBox = Nz(drDecisions("VariableName")) & "_ID" & Nni(drDecisionsValue("ID")) & "_DecisionsValue"
                            sNomeLink = "lnk_" & Nz(drDecisions("VariableName")) & "_ID" & Nni(drDecisionsValue("ID")) & "_DecisionsValue_IDDecisionDeveloper" & drDecisions("ID")

                            If Nni(drDecisionsValue("IDItem")) > 0 Then
                                sLabelLink = GetItemName(drDecisionsValue("IDItem")) & "<br/>" & sLabelLink
                                sNomeTextBox &= GetItemName(drDecisionsValue("IDItem"))
                                sNomeLink &= "_Item" & GetItemName(drDecisionsValue("IDItem"))
                            End If

                            ' Controllo la presenza del link button, se non lo trovo lo inserisco, altrimenti esco dal ciclo e passo al successivo
                            Dim oLnk As LinkButton = TryCast(FindControlRecursive(Page, sNomeLink), LinkButton)
                            AddTableRow(oLnk, sLabelLink, sNomeLink, sNomeTextBox, sValue.Trim, iIDDataType)

                        End If

                    Next
                Next

                ' Inserisco una riga di separazione tra i diversi HEADER
                tblRow = New TableRow
                tblCell = New TableCell

                tblCell.ColumnSpan = 3
                tblCell.BorderStyle = BorderStyle.None
                tblCell.Style.Add("min-height", "15px")
                tblCell.BorderStyle = BorderStyle.None

                tblRow.Cells.Add(tblCell)

                tblDeveloperDecisions.Rows.Add(tblRow)
            Next

            tblRow = New TableRow
            tblCell = New TableCell

            tblCell.ColumnSpan = 3
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("min-height", "10px")
            tblCell.BorderStyle = BorderStyle.None

            tblRow.Cells.Add(tblCell)

            tblDeveloperDecisions.Rows.Add(tblRow)

        Catch ex As Exception
            MessageText.Text = "Developer_VariableDecisions_Players.aspx -> LoadDecisions" & "<br/>" & ex.Message & "<br/>"
            Message.Visible = True
        End Try
    End Sub

    Private Sub AddTableRow(Link As LinkButton, LinkLabel As String, LinkID As String, TextID As String, TextValue As String, IDDataType As Integer)
        Dim tblRow As TableRow
        Dim tblCell As TableCell

        If Link Is Nothing Then
            ' Costruisco la riga della variabile
            ' Costruisco la riga delle decisione
            tblRow = New TableRow
            tblCell = New TableCell

            ' Etichetta della variabile
            Link = New LinkButton
            Link.Text = LinkLabel
            Link.ID = LinkID
            AddHandler Link.Click, AddressOf lnkButton_Click

            tblCell.Controls.Add(Link)

            tblRow.Cells.Add(tblCell)

            ' Campo testo per la memorizzazione e la visualizzazione della varibile
            Dim oTxtBox As New RadTextBox
            tblCell = New TableCell
            oTxtBox.Text = "0,00"

            oTxtBox.ID = TextID
            If TextValue <> "" Then
                oTxtBox.Text = Nn(TextValue).ToString("N2")
            End If

            oTxtBox.Attributes.Add("runat", "server")
            oTxtBox.Style.Add("text-align", "right")
            oTxtBox.Style.Add("RenderMode", "Lightweight")
            oTxtBox.ClientIDMode = ClientIDMode.Static
            oTxtBox.EnableViewState = True

            tblCell.Controls.Add(oTxtBox)
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            ' Etichetta per la definizione del tipo di variabile
            tblCell = New TableCell
            tblCell.Text = ReturnStandardLabel_Decisions_Variables(IDDataType)
            tblRow.Cells.Add(tblCell)

            tblDeveloperDecisions.Rows.Add(tblRow)
        End If
    End Sub

    Private Sub Developer_VariableDecisions_Players_Init(sender As Object, e As EventArgs) Handles Me.Init
        m_SessionData = Session("SessionData")

        m_SiteMaster = Me.Master

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oCBOPeriod As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler m_SiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler m_SiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(m_SessionData.Utente.Games.IDGame) ' Ultimo periodo
        oCBOPeriod.SelectedValue = Session("IDPeriodValid")
        m_SiteMaster.PeriodChange = Session("IDPeriodValid")

        Session("NameTeam") = GetTeamName(Session("IDTeam"))
        oCBOTeam.SelectedValue = Session("IDTeam")
        m_SiteMaster.TeamChange = Session("IDTeam")

        LoadDecisions()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim sSQL As String = ""
        Dim sIDLinkButton As String
        Dim sNomeTextbox As String
        Dim sDataTextBox As List(Of String)
        Dim sNomeVariabile As String
        Dim iIDUpdate As Integer
        Dim sTipoTabella As String
        Dim oDTDecisions As DataTable
        Dim oDTDecisionsValue As DataTable
        Dim oTXT As RadTextBox = Nothing

        Try
            pnlMain.Update()

            Message.Visible = False

            ' Recupero tutte le decisini che hanno questo raggruppamento
            sSQL = "SELECT D.Id, D.IDDataType, D.IDGame, D.IDGroup, D.IDVariable, D.VariableLabel, D.VariableLabelGroup, D.VariableName " _
                 & "FROM Decisions_Developer D " _
                 & "WHERE D.IDGame = " & Nni(Session("IDGame"))
            oDTDecisions = g_DAL.ExecuteDataTable(sSQL)

            For Each drDecision As DataRow In oDTDecisions.Rows
                ' Con la decisione appena caricata recupero solo quelle valide del periodo su cui sono posizionato
                sSQL = "SELECT DBV.Id, DBV.IDDecisionDeveloper, DBV.IDItem, DBV.IDPeriod, DBV.IDPlayer, DBV.Simulation, DBV.Value " _
                     & "FROM Decisions_Developer_Value DBV " _
                     & "WHERE DBV.IDDecisionDeveloper = " & Nni(drDecision("ID")) _
                     & " AND ISNULL(IDPeriod, " & m_SiteMaster.PeriodGetCurrent & ") = " & m_SiteMaster.PeriodGetCurrent _
                     & " AND ISNULL(IDPlayer, " & m_SiteMaster.TeamIDGet & ") = " & m_SiteMaster.TeamIDGet
                oDTDecisionsValue = g_DAL.ExecuteDataTable(sSQL)

                For Each drDecisionValue As DataRow In oDTDecisionsValue.Rows
                    If Nz(drDecisionValue("Value")).ToUpper.Contains("LIST OF") Then
                        sNomeTextbox = Nz(drDecision("VariableName")) & "_ID" & Nni(drDecisionValue("ID")) & "_DecisionsValueList"

                    Else
                        sNomeTextbox = Nz(drDecision("VariableName")) & "_ID" & Nni(drDecisionValue("ID")) & "_DecisionsValue"
                        If Nni(drDecisionValue("IDItem")) > 0 Then
                            sNomeTextbox &= GetItemName(drDecisionValue("IDItem"))
                        End If

                        oTXT = TryCast(FindControlRecursive(Page, sNomeTextbox), RadTextBox)

                        If Not IsNothing(oTXT) Then
                            sIDLinkButton = oTXT.ID
                            sDataTextBox = New List(Of String)(sIDLinkButton.Split("_"c))
                            ' Nome della variabile as string
                            sNomeVariabile = sDataTextBox(0)

                            ' ID per aggiornamento
                            iIDUpdate = sDataTextBox(1).Replace("ID", "")

                            ' Tabella da aggiornare
                            sTipoTabella = sDataTextBox(2)

                            If sTipoTabella.ToUpper.Contains("DECISIONSVALUE") Then
                                sSQL = "UPDATE Decisions_Developer_Value SET Value = '" & oTXT.Text & "' " _
                                     & "WHERE ID = " & iIDUpdate

                            ElseIf sTipoTabella.ToUpper.Contains("DECISIONSVALUELIST") Then
                                sSQL = "UPDATE Decisions_Developer_Value_List SET Value = '" & oTXT.Text & "' " _
                                     & "WHERE ID = " & iIDUpdate

                            End If

                            g_DAL.ExecuteNonQuery(sSQL)

                        End If
                    End If

                Next
            Next

            MessageText.Text = "Save completed"
            Message.Visible = True
        Catch ex As Exception
            MessageText.Text = "btnSave_Click" & "<br/>" & ex.Message & "<br/>"
            Message.Visible = True
        End Try

    End Sub

    Private Sub Developer_VariableDecisions_Players_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsNothing(m_SessionData) Then
            PageRedirect("ErrorTimeout.aspx", "", "")
        End If

        If Not IsPostBack() Then
            LoadDecisions()
            LoadDataType()
        End If

        EnableSave()
    End Sub

#Region "GESTIONE DEL PANNELLO DETTAGLIO"

    Private Sub LoadDataType()
        Dim oDTDataType As DataTable = HandleLoadComboBox_Variables_DataType()

        Try
            cboDataType.Items.Clear()
            cboDataType.DataSource = Nothing
            cboDataType.DataSource = oDTDataType
            cboDataType.DataValueField = "Id"
            cboDataType.DataTextField = "TypeDescription"
            cboDataType.DataBind()
            cboDataType.SelectedValue = -1
        Catch ex As Exception
            MessageText.Text = "Error -> HandleLoadDataType -> " & ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub ClearPanelNewDecision()
        Session("IDVariableLoad") = Nothing
        Session("IDVariable") = Nothing
        Session("TypeVariable") = Nothing

        txtVariableName.Text = ""
        txtVariableLabel.Text = ""
        txtVariableLabelGroup.Text = ""

        cboDataType.SelectedValue = -1

        chkItems.Checked = False
        chkPlayers.Checked = False

    End Sub

    Private Sub btnHide_Click(sender As Object, e As EventArgs) Handles btnHide.Click
        ClearPanelNewDecision()
        mpeMain.Hide()
    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        Dim sSQL As String

        ' Procedo con il salvataggio dei dati inseriti
        Try
            Message.Visible = False

            If Nni(Session("IDVariableLoad")) <> 0 Then ' Ho caricato una variabile già presente in archivio
                If txtVariableName.Text <> "" Then
                    sSQL = "UPDATE Decisions_Developer SET " _
                         & "VariableName = '" & CStrSql(txtVariableName.Text) & "', " _
                         & "VariableLabelGroup = '" & CStrSql(txtVariableLabelGroup.Text) & "', " _
                         & "VariableLabel = '" & CStrSql(txtVariableLabel.Text) & "', " _
                         & "IDDataType = " & Nni(cboDataType.SelectedValue) & " " _
                         & "WHERE ID = " & Nni(Session("IDDecisionDeveloper"))
                    g_DAL.ExecuteNonQuery(sSQL)
                End If

            Else ' Altrimenti verifico la presenza della descrizione e procedo con l'inserimento di una nuova variabile decisionale del developer
                If txtVariableName.Text <> "" Then
                    sSQL = "INSERT INTO Decisions_Developer (IDGame, VariableName, VariableLabelGroup, VariableLabel, IDDataType) VALUES (" & Nni(Session("IDGame")) & ", '" _
                         & CStrSql(txtVariableName.Text) & "', '" & CStrSql(txtVariableLabelGroup.Text) & "', '" & CStrSql(txtVariableLabel.Text) & "', " & cboDataType.SelectedValue & ") "
                    g_DAL.ExecuteNonQuery(sSQL)

                    ' recupero l'ultimo id inserito
                    sSQL = "SELECT MAX(ID) FROM Decisions_Developer WHERE IDGame = " & Nni(Session("idgame"))
                    Session("IDDecisionDeveloper") = Nni(g_DAL.ExecuteScalar(sSQL))
                End If

            End If

            ' Vado a generare i record nella tabella delle decision value per il periodo selezionato
            sSQL = "SELECT ID FROM Decisions_Developer_Value WHERE IDDecisionDeveloper = " & Nni(Session("IDDecisionDeveloper"))
            Dim iIDVaribleDecisionValue As Integer = Nni(g_DAL.ExecuteScalar(sSQL))

            If iIDVaribleDecisionValue = 0 Then
                ' Carico player e items
                Dim oDTPlayers As DataTable = LoadTeamsGame(Session("IDGame"))
                Dim oDTItems As DataTable = LoadItemsGame(Session("IDGame"), Nz(Session("LanguageActive")))

                ' Inserisco i dati nella tabella del dettaglio valori
                If chkPlayers.Checked Then
                    If chkItems.Checked Then
                        For Each drPlayer As DataRow In oDTPlayers.Rows
                            For Each drItem As DataRow In oDTItems.Rows
                                sSQL = "INSERT INTO Decisions_Developer_Value (IDDecisionDeveloper, IDPeriod, Value, IDPLayer, IDItem) VALUES (" _
                                     & Nni(Session("IDVariableLoad")) & ", " & m_SiteMaster.PeriodGetCurrent & ", '0,00', " & Nni(drPlayer("ID")) & ", " & Nni(drItem("IDVariable")) & ") "
                                g_DAL.ExecuteNonQuery(sSQL)
                            Next
                        Next

                    Else
                        For Each drPlayer As DataRow In oDTPlayers.Rows
                            sSQL = "INSERT INTO Decisions_Developer_Value (IDDecisionDeveloper, IDPeriod, Value, IDPLayer) VALUES (" _
                                 & Nni(Session("IDVariableLoad")) & ", " & m_SiteMaster.PeriodGetCurrent & ", '0,00', " & Nni(drPlayer("ID")) & ") "
                            g_DAL.ExecuteNonQuery(sSQL)
                        Next

                    End If
                Else
                    If chkItems.Checked Then
                        For Each drItem As DataRow In oDTItems.Rows
                            sSQL = "INSERT INTO Decisions_Developer_Value (IDDecisionDeveloper, IDPeriod, Value, IDItem) VALUES (" _
                                 & Nni(Session("IDVariableLoad")) & ", " & m_SiteMaster.PeriodGetCurrent & ", '0,00', " & Nni(drItem("IDVariable")) & ") "
                            g_DAL.ExecuteNonQuery(sSQL)
                        Next

                    End If
                End If
            End If
        Catch ex As Exception
            MessageText.Text = "Error btnOK_Click -> " & ex.Message
            Message.Visible = True
        End Try
    End Sub

#End Region

    Private Sub lnkButton_Click(sender As Object, e As EventArgs)
        Dim oLnk As LinkButton = CType(sender, LinkButton)
        Dim sVariable As String = oLnk.ID
        Dim sDataLink As List(Of String) = New List(Of String)(sVariable.Split("_"c))
        Dim sSQL As String

        Session("IDVariableLoad") = Nni(sDataLink(2).Replace("ID", ""))
        Session("IDDecisionDeveloper") = Nni(sDataLink(4).Replace("IDDecisionDeveloper", ""))

        ' Aggiungo i dati al pannello 
        If sDataLink(3).ToUpper = "DECISIONSVALUELIST" Then
            sSQL = "SELECT D.*, DBV.* " _
               & "FROM Decisions_Developer D " _
               & "LEFT JOIN Decisions_Developer_Value DBV ON D.Id = DBV.IDDecisionDeveloper " _
               & "LEFT JOIN Decisions_Developer_Value_List DBVL ON DBV.id = DBVL.IDDecisionValue " _
               & "WHERE DBVL.ID = " & Session("IDVariableLoad")

        ElseIf sDataLink(3).ToUpper = "DECISIONSVALUE" Then
            sSQL = "SELECT D.*, DBV.* " _
                 & "FROM Decisions_Developer D " _
                 & "LEFT JOIN Decisions_Developer_Value DBV ON D.Id = DBV.IDDecisionDeveloper  " _
                 & "WHERE DBV.ID = " & Session("IDVariableLoad")

            Dim oDTData As DataTable = g_DAL.ExecuteDataTable(sSQL)
            For Each drData As DataRow In oDTData.Rows
                txtVariableName.Text = Nz(drData("VariableName"))
                txtVariableLabel.Text = Nz(drData("VariableLabel"))
                txtVariableLabelGroup.Text = Nz(drData("VariableLabelGroup"))

                cboDataType.SelectedValue = Nni(drData("IDDataType"))
                chkPlayers.Checked = Nni(drData("IDPlayer")) > 0

                chkItems.Checked = Nni(drData("IDItem")) > 0
            Next

        End If

        ViewState("Conta") += 1
        mpeMain.Show()

    End Sub

    Private Sub btnNewDecision_Click(sender As Object, e As EventArgs) Handles btnNewDecision.Click
        ViewState("Conta") += 1
    End Sub

    Private Sub pnlMain_Load(sender As Object, e As EventArgs) Handles pnlMain.Load
        If Not ViewState("Conta") Is Nothing AndAlso Nni(ViewState("Conta")) = 3 Then
            Response.Redirect("Developer_VariableDecisions_Players.aspx")
            ViewState("Conta") = 0
        End If
    End Sub

End Class
