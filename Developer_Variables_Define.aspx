﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Developer_Variables_Define.aspx.vb" Inherits="Developer_Variables_Define" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="cntMain" ContentPlaceHolderID="MainContent" runat="Server" ClientIDMode="Static">
    <link href="Content/GridTelerik.css" rel="stylesheet" type="text/css" />

    <asp:UpdatePanel runat="server" ID="pnlMain" UpdateMode="Conditional">
        <ContentTemplate>
            <h2>
                <asp:Label ID="lblWelcome" runat="server" Text="Developer/Designer" />
            </h2>

            <div class="clr"></div>

            <div id="subTitle">
                <div id="subLeft" style="float: left; width: 20%;">
                    <h3>
                        <asp:Label ID="lblTitle" runat="server" Text="Define variables"></asp:Label>
                    </h3>
                </div>

                <div id="subRight" style="float: right; padding-right: 5px;">
                    <div id="divNewVariable" style="margin-top: 10px; margin-bottom: 5px; text-align: right;">
                        <telerik:RadButton ID="btnNewVariable" runat="server" Text="New variable"
                            RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                        </telerik:RadButton>
                    </div>
                </div>
            </div>

            <div class="clr"></div>

            <div class="row" style="padding-bottom: 5px;">
                <asp:Panel ID="pnlTable" runat="server">
                    <telerik:RadGrid ID="grdVariables" runat="server" RenderMode="Lightweight" AllowPaging="True" AllowSorting="True" EnableLinqExpressions="false"
                        OnNeedDataSource="grdVariables_NeedDataSource" Width="670px" AllowFilteringByColumn="True" Culture="it-IT" GroupPanelPosition="Top">
                        <MasterTableView AutoGenerateColumns="false" TableLayout="Fixed" CssClass="RadGrid_WebBlue" PageSize="50"
                            DataKeyNames="ID, IDGroup, IDDataType, IDCategory, IDDefinition">

                            <Columns>
                                <telerik:GridButtonColumn CommandName="Modify" Text="Edit" UniqueName="EditColumn"
                                    ButtonType="ImageButton" ImageUrl="~/Content/GridTelerik/Edit.gif">
                                    <HeaderStyle Width="20px" />
                                </telerik:GridButtonColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="VariableName" HeaderText="Variable" UniqueName="VariableName"
                                    FilterControlWidth="100px" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                    <HeaderStyle Width="100px" />
                                </telerik:GridBoundColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="VariableLabel" HeaderText="Label [def.]" UniqueName="VariableLabel"
                                    FilterControlWidth="130px" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                    <HeaderStyle Width="150px" />
                                </telerik:GridBoundColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="GroupDescription" HeaderText="Group" UniqueName="GroupDescription"
                                    FilterControlWidth="90px" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                    <HeaderStyle Width="100px" />
                                </telerik:GridBoundColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="DefaultValue" HeaderText="Def. value" UniqueName="DefaultValue"
                                    FilterControlWidth="70px" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                    <HeaderStyle Width="80px" />
                                </telerik:GridBoundColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridButtonColumn CommandName="Translate" Text="Translate" UniqueName="Translate"
                                    ButtonType="ImageButton" ImageUrl="Images/Translate.ico" Resizable="false">
                                    <HeaderStyle Width="20px" />
                                </telerik:GridButtonColumn>
                            </Columns>

                            <%--COLONNE NASCOSTE--%>
                            <Columns>
                                <telerik:GridBoundColumn DataField="ID" HeaderText="ID" UniqueName="ID" Visible="false" />
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="IDGroup" HeaderText="IDGroup" UniqueName="IDGroup" Visible="false" />
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="IDDataType" HeaderText="IDDataType" UniqueName="IDDataType" Visible="false" />
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="IDCategory" HeaderText="IDCategory" UniqueName="IDCategory" Visible="false" />
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="IDDefinition" HeaderText="IDDefinition" UniqueName="IDDefinition" Visible="false" />
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="TypeDescription" HeaderText="TypeDescription" UniqueName="TypeDescription" Visible="false" />
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="Category" HeaderText="Category" UniqueName="Category" Visible="false" />
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="DefinitionDescription" HeaderText="DefinitionDescription" UniqueName="DefinitionDescription" Visible="false" />
                            </Columns>

                        </MasterTableView>
                    </telerik:RadGrid>

                </asp:Panel>
            </div>

        </ContentTemplate>

    </asp:UpdatePanel>

    <div class="clr"></div>

    <div class="row">
        <asp:UpdatePanel ID="pnlVariableDetailUpdate" runat="server" UpdateMode="Conditional">
            <ContentTemplate>

                <input id="hdnPanelVariable" type="hidden" name="hddClick" runat="server" />
                <ajaxToolkit:ModalPopupExtender ID="mpeMain" runat="server" PopupControlID="pnlNewVariable" TargetControlID="hdnPanelVariable"
                    BackgroundCssClass="modalBackground" DropShadow="true" CancelControlID="btnHide">
                </ajaxToolkit:ModalPopupExtender>

                <asp:Panel runat="server" ID="pnlNewVariable" CssClass="modalPopup">
                    <div class="pnlheader">
                        <asp:Label ID="lblPanelTitle" runat="server" Text="Manage game variable"></asp:Label>
                    </div>

                    <div class="pnlbody">
                        <!-- GESTIONE DELLA TABELLA CON DIV -->
                        <div class="divTable">
                            <div class="divTableBody">
                                <div class="divTableRow">
                                    <div class="divTableCell" style="width: 200px;">Variable name</div>
                                    <div class="divTableCell" style="width: 580px;">
                                        <telerik:RadTextBox ID="txtVariableName" runat="server" RenderMode="Lightweight" ClientIDMode="Static"></telerik:RadTextBox>
                                    </div>
                                </div>

                                <div class="divTableRow">
                                    <div class="divTableCell">Variable label</div>
                                    <div class="divTableCell">
                                        <telerik:RadTextBox ID="txtVariableLabel" runat="server" RenderMode="Lightweight" ClientIDMode="Static"></telerik:RadTextBox>
                                    </div>
                                </div>

                                <div class="divTableRow">
                                    <div class="divTableCell">Decision group</div>
                                    <div class="divTableCell">
                                        <telerik:RadComboBox ID="cboGroups" runat="server" EmptyMessage="< Select group...>"
                                            RenderMode="Lightweight" RenderingMode="Full" ZIndex="1000000" ClientIDMode="Static">
                                        </telerik:RadComboBox>
                                    </div>
                                </div>

                                <div class="divTableRow">
                                    <div class="divTableCell">Data type</div>
                                    <div class="divTableCell">
                                        <telerik:RadComboBox ID="cboDataTypes" runat="server" EmptyMessage="< Select data type...>"
                                            RenderMode="Lightweight" RenderingMode="Full" ZIndex="1000000" ClientIDMode="Static">
                                        </telerik:RadComboBox>
                                    </div>
                                </div>

                                <div class="divTableRow">
                                    <div class="divTableCell">Variable category</div>
                                    <div class="divTableCell">
                                        <telerik:RadComboBox ID="cboCategories" runat="server" EmptyMessage="< Select category...>"
                                            RenderMode="Lightweight" RenderingMode="Full" ZIndex="1000000" ClientIDMode="Static">
                                        </telerik:RadComboBox>
                                    </div>
                                </div>

                                <div class="divTableRow">
                                    <div class="divTableCell">Variable definition</div>
                                    <div class="divTableCell">
                                        <telerik:RadComboBox ID="cboDefinitions" runat="server" EmptyMessage="< Select definition...>"
                                            RenderMode="Lightweight" RenderingMode="Full" ZIndex="1000000" ClientIDMode="Static">
                                        </telerik:RadComboBox>
                                    </div>
                                </div>

                                <div class="divTableRow">
                                    <div class="divTableCell">List of item</div>
                                    <div class="divTableCell">
                                        <telerik:RadCheckBox ID="chkItem" runat="server" Text="" AutoPostBack="false" RenderMode="Lightweight"></telerik:RadCheckBox>
                                    </div>
                                </div>

                                <div class="divTableRow">
                                    <div class="divTableCell">Page (group) title</div>
                                    <div class="divTableCell">
                                        <telerik:RadTextBox ID="txtGroupVariable" runat="server" RenderMode="Lightweight" ClientIDMode="Static"></telerik:RadTextBox>
                                    </div>
                                </div>

                                <div class="divTableRow">
                                    <div class="divTableCell">Default value</div>
                                    <div class="divTableCell">
                                        <telerik:RadTextBox ID="txtDefaultValue" runat="server" RenderMode="Lightweight" ClientIDMode="Static"></telerik:RadTextBox>
                                    </div>
                                </div>

                                <div class="divTableRow">
                                    <div class="divTableCell">Order visibility</div>
                                    <div class="divTableCell">
                                        <telerik:RadTextBox ID="txtOrderVisibility" runat="server" RenderMode="Lightweight" ClientIDMode="Static"></telerik:RadTextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="pnlfooter">
                        <telerik:RadButton ID="btnHide" runat="server" Text="Close" Width="80px"
                            RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                        </telerik:RadButton>

                        <telerik:RadButton ID="btnOK" runat="server" Text="Save" Width="80px"
                            RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                        </telerik:RadButton>
                    </div>

                    <div style="text-align: center;">
                        <asp:Label runat="server" ID="txtMessageVariable" Visible="false"></asp:Label>
                    </div>

                </asp:Panel>

            </ContentTemplate>

        </asp:UpdatePanel>
    </div>

    <div class="clr"></div>

    <div class="row">

        <asp:UpdatePanel ID="pnlVariablesTranslate" runat="server" UpdateMode="Conditional">
            <ContentTemplate>

                <%--<input id="hdnPanelTranslations" type="hidden" name="hddclick" runat="server" />--%>
                <asp:Button ID="btnPanelTranslation" runat="server" Style="display: none;" />

                <ajaxToolkit:ModalPopupExtender ID="mpeTranslate" runat="server" PopupControlID="pnlTranslate" TargetControlID="btnPanelTranslation"
                    BackgroundCssClass="modalBackground" DropShadow="true" CancelControlID="btnHideTranslation">
                </ajaxToolkit:ModalPopupExtender>

                <asp:Panel runat="server" ID="pnlTranslate" CssClass="modalPopup Translations">
                    <div class="pnlheader">
                        <asp:Label ID="lblTitleTranslation" runat="server" Text="Manage translation variable"></asp:Label>
                    </div>

                    <div class="pnlbody">
                        <!-- GESTIONE DELLA TABELLA CON DIV -->
                        <div class="divTable">
                            <div class="divTableBody">
                                <div class="divTableRow">
                                    <div class="divTableCell" style="width: 25%">Variable name</div>
                                    <div class="divTableCell">
                                        <telerik:RadTextBox ID="txtVariableNameTranslate" runat="server" RenderMode="Lightweight" Width="75%" ClientIDMode="Static"></telerik:RadTextBox>
                                    </div>
                                </div>

                                <div class="divTableRow">
                                    <div class="divTableCell" style="width: 25%">Select new language</div>
                                    <div class="divTableCell">
                                        <telerik:RadComboBox ID="cboLanguage" runat="server" Width="75%" EmptyMessage="< Select language...>"
                                            RenderMode="Lightweight" RenderingMode="Full" ZIndex="1000000" ClientIDMode="Static">
                                        </telerik:RadComboBox>
                                    </div>
                                </div>

                                <div class="divTableRow">
                                    <div class="divTableCell" style="width: 25%">Variable translation</div>
                                    <div class="divTableCell">
                                        <telerik:RadTextBox ID="txtVariableTranslation" runat="server" RenderMode="Lightweight" Width="75%" ClientIDMode="Static"></telerik:RadTextBox>
                                    </div>
                                </div>

                                <div class="divTableRow">
                                    <div class="divTableCell">Translations available</div>
                                    <div class="divTableCell">
                                        <asp:Panel ID="Panel3" runat="server">
                                            <telerik:RadGrid ID="grdTranslation" runat="server" RenderMode="Lightweight" AllowPaging="True" AllowSorting="True"
                                                CellSpacing="0" GridLines="None" OnNeedDataSource="grdTranslations_NeedDataSource" Width="420px">
                                                <MasterTableView AutoGenerateColumns="false" TableLayout="Fixed" CssClass="RadGrid_WebBlue"
                                                    DataKeyNames="ID, IDLanguage, IDVariable" NoMasterRecordsText="No translations available">
                                                    <Columns>
                                                        <telerik:GridButtonColumn CommandName="Modify" Text="Edit" UniqueName="EditColumnTranslation"
                                                            ButtonType="ImageButton" ImageUrl="~/Content/GridTelerik/Edit.gif">
                                                            <HeaderStyle Width="20px" />
                                                        </telerik:GridButtonColumn>
                                                    </Columns>

                                                    <Columns>
                                                        <telerik:GridBoundColumn DataField="VariableName" HeaderText="Variable" UniqueName="VariableName">
                                                            <HeaderStyle Width="100px" />
                                                        </telerik:GridBoundColumn>
                                                    </Columns>

                                                    <Columns>
                                                        <telerik:GridBoundColumn DataField="Translation" HeaderText="Translation" UniqueName="Translation">
                                                            <HeaderStyle Width="180px" />
                                                        </telerik:GridBoundColumn>
                                                    </Columns>

                                                    <Columns>
                                                        <telerik:GridBoundColumn DataField="Code" HeaderText="Lang." UniqueName="Code">
                                                            <HeaderStyle Width="80px" />
                                                        </telerik:GridBoundColumn>
                                                    </Columns>

                                                    <Columns>
                                                        <telerik:GridButtonColumn CommandName="DeleteTranslation" Text="Delete" UniqueName="Delete" ConfirmText="Are you sure you want to delete de selected item?"
                                                            ButtonType="ImageButton" ImageUrl="Images/Delete16.png" Resizable="false">
                                                            <HeaderStyle Width="20px" />
                                                        </telerik:GridButtonColumn>
                                                    </Columns>

                                                </MasterTableView>
                                            </telerik:RadGrid>

                                            <asp:PlaceHolder runat="server" ID="MessageTranslation" Visible="false">
                                                <p class="text-danger">
                                                    <asp:Literal runat="server" ID="txtMessageTranslation" />
                                                </p>
                                            </asp:PlaceHolder>

                                        </asp:Panel>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="pnlfooter">
                        <telerik:RadButton ID="btnHideTranslation" runat="server" Text="Close" Width="80px"
                            RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                        </telerik:RadButton>

                        <telerik:RadButton ID="btnSaveTranslation" runat="server" Text="Save" Width="80px"
                            RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                        </telerik:RadButton>
                    </div>

                </asp:Panel>
            </ContentTemplate>

        </asp:UpdatePanel>
    </div>

</asp:Content>
