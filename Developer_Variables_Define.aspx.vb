﻿
Imports Telerik.Web.UI
Imports System.Data
Imports APS
Imports APPCore.Utility
Imports DALC4NET

Partial Class Developer_Variables_Define
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster

    Private Sub HandleLoadVariablesGroups()
        Dim sSQL As String
        Dim oDAL As New DBHelper
        Dim oDTGroups As DataTable

        sSQL = "SELECT -1 AS ID, '' AS GroupDescription UNION ALL SELECT ID, GroupDescription FROM Variables_Groups WHERE IDGame = " & Session("IDGame") & " ORDER BY ID "
        oDTGroups = oDAL.ExecuteDataTable(sSQL)

        cboGroups.DataSource = oDTGroups
        cboGroups.DataValueField = "ID"
        cboGroups.DataTextField = "GroupDescription"
        cboGroups.DataBind()

        cboGroups.SelectedValue = -1

        SQLConnClose(oDAL.GetConnObject)
    End Sub

    Private Sub HandleLoadDataTypes()
        Dim sSQL As String
        Dim oDAL As New DBHelper
        Dim oDTDataTypes As DataTable

        sSQL = "SELECT -1 AS ID, '' AS TypeDescription UNION ALL SELECT ID, TypeDescription FROM Variables_Type ORDER BY ID"
        oDTDataTypes = oDAL.ExecuteDataTable(sSQL)

        cboDataTypes.DataSource = oDTDataTypes
        cboDataTypes.DataValueField = "ID"
        cboDataTypes.DataTextField = "TypeDescription"
        cboDataTypes.DataBind()

        cboDataTypes.SelectedValue = -1

        SQLConnClose(oDAL.GetConnObject)
    End Sub

    Private Sub HandleLoadCategories()
        Dim sSQL As String
        Dim oDAL As New DBHelper
        Dim oDTVariablesCategories As DataTable

        sSQL = "SELECT -1 AS ID, '' AS Category UNION ALL SELECT ID, Category FROM Variables_Category ORDER BY ID"
        oDTVariablesCategories = oDAL.ExecuteDataTable(sSQL)

        cboCategories.DataSource = oDTVariablesCategories
        cboCategories.DataValueField = "ID"
        cboCategories.DataTextField = "Category"
        cboCategories.DataBind()

        cboCategories.SelectedValue = -1

        SQLConnClose(oDAL.GetConnObject)
    End Sub

    Private Sub HandleLoadDefinitions()
        Dim sSQL As String
        Dim oDAL As New DBHelper
        Dim oDTDefinitions As DataTable

        sSQL = "SELECT -1 AS ID, '' AS DefinitionDescription UNION ALL SELECT ID, DefinitionDescription FROM Variables_Definition ORDER BY ID"
        oDTDefinitions = oDAL.ExecuteDataTable(sSQL)

        cboDefinitions.DataSource = oDTDefinitions
        cboDefinitions.DataValueField = "ID"
        cboDefinitions.DataTextField = "DefinitionDescription"
        cboDefinitions.DataBind()

        cboDefinitions.SelectedValue = -1

        SQLConnClose(oDAL.GetConnObject)
    End Sub

    Private Sub HandleLoadLanguages()
        Dim sSQL As String
        Dim oDAL As New DBHelper
        Dim oDTLanguages As DataTable

        sSQL = "SELECT -1 AS ID, '' AS Language UNION ALL SELECT ID, Language FROM Languages ORDER BY ID"
        oDTLanguages = oDAL.ExecuteDataTable(sSQL)

        cboLanguage.DataSource = oDTLanguages
        cboLanguage.DataValueField = "ID"
        cboLanguage.DataTextField = "Language"
        cboLanguage.DataBind()

        cboLanguage.SelectedValue = -1

        SQLConnClose(oDAL.GetConnObject)
    End Sub

    Private Function HandleLoadDataVariables() As DataTable
        Dim sSQL As String
        Dim oDAL As New DBHelper
        Dim oDT As DataTable

        sSQL = "SELECT V.ID, V.IDGroup, V.IDDataType, V.IDCategory, V.IDDefinition, " _
             & "V.VariableName, V.VariableLabel, VG.GroupDescription, VT.TypeDescription, V.OrderVisibility, " _
             & "VC.Category, VD.DefinitionDescription, V.ListOfItems, V.VariableGroupLabel, V.DefaultValue " _
             & "FROM Variables V " _
             & "LEFT JOIN Variables_Groups VG ON V.IDGroup = VG.Id AND VG.IDGame = " & Session("IDGame") & " " _
             & "LEFT JOIN Variables_Type VT ON V.IDDataType = VT.Id " _
             & "LEFT JOIN Variables_Category VC ON V.IDCategory = VC.Id " _
             & "LEFT JOIN Variables_Definition VD ON V.IDDefinition = VD.ID " _
             & "WHERE V.IDGame = " & Session("IDGame")
        oDT = oDAL.ExecuteDataTable(sSQL)

        Return oDT
    End Function

    Private Function HandleLoadDataVariablesTranslations() As DataTable
        Dim sSQL As String
        Dim oDAL As New DBHelper
        Dim oDT As DataTable

        sSQL = "SELECT T.ID, V.ID AS IDVariable, T.IDLanguage, V.VariableName, V.VariableLabel, T.Translation, L.Code " _
             & "FROM Variables V " _
             & "LEFT JOIN Variables_Translations T ON V.ID = T.IDVariable " _
             & "LEFT JOIN Languages L ON T.IDLanguage = L.ID " _
             & "WHERE V.ID = " & Nni(Session("IDVariable"))
        oDT = oDAL.ExecuteDataTable(sSQL)

        SQLConnClose(oDAL.GetConnObject)

        Return oDT
    End Function

    Private Sub ClearPanel()
        If Not Session("IDVariable") Is Nothing Then Session("IDVariable") = 0
        txtVariableLabel.Text = ""
        txtVariableName.Text = ""
        cboCategories.SelectedValue = -1
        cboDataTypes.SelectedValue = -1
        cboGroups.SelectedValue = -1
        cboDefinitions.SelectedValue = -1
        chkItem.Checked = False
        txtGroupVariable.Text = ""
        txtDefaultValue.Text = ""
        txtOrderVisibility.Text = ""
        txtMessageVariable.Visible = False
        txtMessageVariable.Text = ""
        Dim oPNL As UpdatePanel = TryCast(FindControlRecursive(Page, "pnlVariableDetailUpdate"), UpdatePanel)
        oPNL.Update()
    End Sub

    Private Sub ClearPanelTranslations()
        If Not Session("IDVariableTranslation") Is Nothing Then Session("IDVariableTranslation") = 0
        txtVariableTranslation.Text = ""
        txtVariableNameTranslate.Text = ""
        cboLanguage.SelectedValue = -1
    End Sub

    Protected Sub grdVariables_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs)
        grdVariables.DataSource = HandleLoadDataVariables()
    End Sub

    Protected Sub grdTranslations_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs)
        grdTranslation.DataSource = HandleLoadDataVariablesTranslations()
    End Sub

    Private Sub Developer_Variables_Define_Init(sender As Object, e As EventArgs) Handles Me.Init
        m_SessionData = Session("SessionData")
    End Sub

    Private Sub Developer_Variables_Define_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsNothing(m_SessionData) Then
            PageRedirect("ErrorTimeout.aspx", "", "")
        End If

        If Not Page.IsPostBack Then

            HandleLoadCategories()
            HandleLoadDataTypes()
            HandleLoadDefinitions()
            HandleLoadVariablesGroups()

            HandleLoadLanguages()

            ClearPanel()
            ClearPanelTranslations()
        End If
    End Sub

    Private Sub grdVariables_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles grdVariables.ItemCommand
        If e.CommandName.ToUpper <> "PAGE" AndAlso e.CommandName.ToUpper <> "SORT" AndAlso e.CommandName.ToUpper <> "FILTER" Then
            Session("ItemSelected") = DirectCast(e.Item, GridDataItem)

            If e.CommandName.ToUpper = "MODIFY" Then
                ClearPanel()
                Dim oPNL As UpdatePanel = TryCast(FindControlRecursive(Page, "pnlVariableDetailUpdate"), UpdatePanel)
                oPNL.Update()
                mpeMain.Show()

            ElseIf e.CommandName.ToUpper = "TRANSLATE" Then
                ClearPanelTranslations()
                Dim oPNL As UpdatePanel = TryCast(FindControlRecursive(Page, "pnlVariablesTranslate"), UpdatePanel)
                oPNL.Update()
                mpeTranslate.Show()

            End If
        End If
    End Sub

    Private Sub grdTranslation_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles grdTranslation.ItemCommand
        If e.CommandName.ToUpper <> "PAGE" Then
            If e.CommandName.ToUpper = "MODIFY" Then
                Session("ItemTranslationSelected") = DirectCast(e.Item, GridDataItem)
                Dim oItem As GridDataItem = DirectCast(Session("ItemTranslationSelected"), GridDataItem)
                Dim oDT As DataTable = HandleLoadDataVariablesTranslations()

                ' Procedo con il caricamento dei dati da inserire nel panello
                ' Recupero l'id della variabile selezionata
                If Not oItem Is Nothing Then
                    Dim iID As Integer = oItem.GetDataKeyValue("ID")
                    Dim oDRData As DataRow = oDT.Select("ID = " & iID).FirstOrDefault
                    If Not oDRData Is Nothing Then
                        txtVariableNameTranslate.Text = Nz(oDRData("VariableName")) & " - " & Nz(oDRData("VariableLabel"))
                        txtVariableNameTranslate.ReadOnly = True

                        cboLanguage.SelectedValue = Nni(oDRData("IDLanguage"))

                        txtVariableTranslation.Text = Nz(oDRData("Translation"))

                        Session("IDVariableTranslation") = iID
                    End If
                End If
                mpeTranslate.Show()

            ElseIf e.CommandName.ToUpper = "DELETETRANSLATION" Then
                Session("ItemTranslationSelected") = DirectCast(e.Item, GridDataItem)
                Dim oItem As GridDataItem = DirectCast(Session("ItemTranslationSelected"), GridDataItem)

                Dim sSql As String = "DELETE FROM Variables_Translations WHERE ID = " & oItem.GetDataKeyValue("ID")
                g_DAL.ExecuteNonQuery(sSql)

                grdTranslation.DataSource = Nothing
                grdTranslation.Rebind()

                mpeTranslate.Show()
            End If
        End If
    End Sub

    Private Sub btnNewVariable_Click(sender As Object, e As EventArgs) Handles btnNewVariable.Click
        ClearPanel()
        mpeMain.Show()
    End Sub

    Private Sub btnHide_Click(sender As Object, e As EventArgs) Handles btnHide.Click
        ClearPanel()
        mpeMain.Hide()

        grdVariables.Rebind()
    End Sub

    Private Sub btnHideTranslation_Click(sender As Object, e As EventArgs) Handles btnHideTranslation.Click
        ClearPanelTranslations()
        mpeTranslate.Hide()

        grdVariables.Rebind()
    End Sub

    Private Sub Developer_Variables_Define_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        If Not Session("IDVariable") Is Nothing Then Session.Remove("IDVariable")
        If Not Session("IDVariableTranslation") Is Nothing Then Session.Remove("IDVariableTranslation")
        If Not Session("ItemSelected") Is Nothing Then Session.Remove("ItemSelected")
        If Not Session("ItemTranslationSelected") Is Nothing Then Session.Remove("ItemTranslationSelected")
    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        ' Procedo con il savataggio delle informazioni
        Dim sSQL As String
        Dim oDAL As New DBHelper

        If txtOrderVisibility.Text = "" Then txtOrderVisibility.Text = "0"

        ' Controllo se andare in modifica o inserimento
        If Nni(Session("IDVariable")) > 0 Then ' Modifica
            sSQL = "UPDATE Variables Set " _
                 & "IDGroup = " & Nni(cboGroups.SelectedValue) & ", " _
                 & "IDDataType = " & Nni(cboDataTypes.SelectedValue) & ", " _
                 & "IDCategory = " & Nni(cboCategories.SelectedValue) & ", " _
                 & "IDDefinition = " & Nni(cboDefinitions.SelectedValue) & ", " _
                 & "VariableName = '" & CStrSql(txtVariableName.Text) & "', " _
                 & "VariableLabel = '" & CStrSql(txtVariableLabel.Text) & "', " _
                 & "ListOfItems = " & Boolean_To_Bit(chkItem.Checked) & ", " _
                 & "VariableGroupLabel = '" & CStrSql(txtGroupVariable.Text) & "', " _
                 & "DefaultValue = '" & CStrSql(txtDefaultValue.Text) & "', " _
                 & "OrderVisibility = " & Nni(txtOrderVisibility.Text, 0) & " " _
                 & "WHERE ID = " & Nni(Session("IDVariable"))

        Else ' Inserimento
            sSQL = "INSERT INTO Variables (VariableName, VariableLabel, IDGroup, IDDataType, IDCategory, IDDefinition, ListOfItems, VariableGroupLabel, DefaultValue, OrderVisibility, IDGame) VALUES (" _
                 & "'" & txtVariableName.Text & "', '" & txtVariableLabel.Text & "', " & Nni(cboGroups.SelectedValue) & ", " _
                 & Nni(cboDataTypes.SelectedValue) & ", " & Nni(cboCategories.SelectedValue) & ", " & Nni(cboDefinitions.SelectedValue) & ", " _
                 & Boolean_To_Bit(chkItem.Checked) & ", '" & CStrSql(txtGroupVariable.Text) & "', '" & CStrSql(txtDefaultValue.Text) & "', " _
                 & Nni(txtOrderVisibility.Text, 0) & ", " & Session("IDGame") & ") "

        End If
        oDAL.ExecuteNonQuery(sSQL)

        If Nni(Session("IDVariable"), 0) = 0 Then ' Sono in inserimento, procedo con il caricamento dell'ultimo id inserito
            sSQL = "SELECT MAX(ID) FROM Variables "
            Session("IDVariable") = Nni(oDAL.ExecuteScalar(sSQL))
        End If

        SQLConnClose(oDAL.GetConnObject)

        ClearPanel()

        grdVariables.DataSource = Nothing
        grdVariables.Rebind()

        pnlMain.Update()

        mpeMain.Show()

        txtMessageVariable.Text = "Update completed"
        txtMessageVariable.Visible = True

    End Sub

    Private Sub btnSaveTranslation_Click(sender As Object, e As EventArgs) Handles btnSaveTranslation.Click
        ' Procedo con il savataggio delle informazioni
        Dim sSQL As String
        Dim oDAL As New DBHelper

        If Nz(cboLanguage.SelectedValue) = "" OrElse Nni(cboLanguage.SelectedValue) <= 0 Then
            txtMessageTranslation.Text = "Please select a language"
            MessageTranslation.Visible = True
            mpeTranslate.Show()
            pnlMain.Update()
            Exit Sub
        End If

        ' Controllo se andare in modifica o inserimento
        If Nni(Session("IDVariableTranslation")) > 0 Then ' Modifica
            sSQL = "UPDATE Variables_Translations SET " _
                 & "IDLanguage = " & Nni(cboLanguage.SelectedValue) & ", " _
                 & "IDVariable = " & Nni(Session("IDVariable")) & ", " _
                 & "Translation = '" & CStrSql(txtVariableTranslation.Text) & "' " _
                 & "WHERE ID = " & Nni(Session("IDVariableTranslation"))
        Else ' Inserimento
            sSQL = "INSERT INTO Variables_Translations (IDLanguage, IDVariable, Translation) VALUES (" _
                 & "" & Nni(cboLanguage.SelectedValue) & ", " & Nni(Session("IDVariable")) & ", '" & CStrSql(txtVariableTranslation.Text) & "') "
        End If
        oDAL.ExecuteNonQuery(sSQL)

        If Nni(Session("IDVariableTranslation"), 0) = 0 Then ' Sono in inserimento, procedo con il caricamento dell'ultimo id inserito
            sSQL = "SELECT MAX(ID) FROM Variables_Translations "
            Session("IDVariableTranslation") = Nni(oDAL.ExecuteScalar(sSQL))
        End If

        SQLConnClose(oDAL.GetConnObject)

        HandleLoadDataVariablesTranslations()

        grdTranslation.DataSource = Nothing
        grdTranslation.Rebind()

        ClearPanelTranslations()

        pnlVariablesTranslate.Update()

        txtMessageTranslation.Text = "Update completed"
        MessageTranslation.Visible = True

        pnlMain.Update()

        mpeTranslate.Show()

    End Sub

    Private Sub pnlNewVariable_PreRender(sender As Object, e As EventArgs) Handles pnlNewVariable.PreRender
        Dim oItem As GridDataItem = DirectCast(Session("ItemSelected"), GridDataItem)
        Dim oDT As DataTable = HandleLoadDataVariables()

        ' Procedo con il caricamento dei dati da inserire nel panello
        ' Recupero l'id della variabile selezionata
        If Not oItem Is Nothing Then
            Dim iID As Integer = oItem.GetDataKeyValue("ID")
            Dim oDRData As DataRow = oDT.Select("ID = " & iID).FirstOrDefault
            If Not oDRData Is Nothing Then
                txtVariableLabel.Text = Nz(oDRData("VariableLabel"))
                txtVariableName.Text = Nz(oDRData("VariableName"))
                If Nni(oDRData("IDCategory")) > 0 Then cboCategories.SelectedValue = Nni(oDRData("IDCategory"))
                If Nni(oDRData("IDDataType")) > 0 Then cboDataTypes.SelectedValue = Nni(oDRData("IDDataType"))
                If Nni(oDRData("IDGroup")) > 0 Then cboGroups.SelectedValue = Nni(oDRData("IDGroup"))
                If Nni(oDRData("IDDefinition")) > 0 Then cboDefinitions.SelectedValue = Nni(oDRData("IDDefinition"))
                chkItem.Checked = Nb(oDRData("ListOfItems"))
                txtGroupVariable.Text = Nz(oDRData("VariableGroupLabel"))
                txtDefaultValue.Text = Nz(oDRData("DefaultValue"))
                txtOrderVisibility.Text = Nni(oDRData("OrderVisibility"))
                Session("IDVariable") = iID
                'MessageVariable.Visible = False
            End If
        End If
    End Sub

    Private Sub pnlTranslate_PreRender(sender As Object, e As EventArgs) Handles pnlTranslate.PreRender
        ' Carico le variabili presenti in archivio
        HandleLoadDataVariablesTranslations()

        Dim oItem As GridDataItem = DirectCast(Session("ItemSelected"), GridDataItem)
        Dim oItemTranslation As GridDataItem = DirectCast(Session("ItemTranslationSelected"), GridDataItem)
        Dim oDT As DataTable = HandleLoadDataVariables()

        ' Prima carico i dati della variabile selezionata, poi controllo se ci sono delle traduzioni già inserite
        If Not oItem Is Nothing Then
            Dim oDRDataVariable As DataRow = oDT.Select("ID = " & oItem.GetDataKeyValue("ID")).FirstOrDefault
            txtVariableNameTranslate.Text = Nz(oDRDataVariable("VariableName")) & " - " & Nz(oDRDataVariable("VariableLabel"))
            txtVariableNameTranslate.ReadOnly = True
        End If

        MessageTranslation.Visible = False

        grdTranslation.DataSource = Nothing
        grdTranslation.Rebind()
    End Sub

End Class
