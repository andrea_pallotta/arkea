﻿Imports System.Data
Imports APS
Imports APPCore.Utility
Imports DALC4NET
Imports Telerik.Web.UI

Partial Class Developer_Variables_State
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadVariableState()
        Message.Visible = False

        If Nni(Session("IDPeriod")) = HandleGetMaxPeriodValid(Session("IDGame")) Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        pnlMain.Update()
    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadVariableState()
        Message.Visible = False

        If Nni(Session("IDPeriod")) = HandleGetMaxPeriodValid(Session("IDGame")) Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        pnlMain.Update()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim sSQL As String
        Dim oDTVariableState As DataTable
        Dim sVariableState As String
        Dim iIDVariableState As Integer
        Dim sVariableName As String = ""
        Dim sNomeTextBox As String = ""

        Dim oDAL As New DBHelper

        Dim oTXT As RadTextBox = Nothing

        Message.Visible = False

        ' Carico le decisioni che può prendere il boss
        oDTVariableState = HandleLoadVariableState(Nni(Session("IDGame")))
        For Each oRowVariableState As DataRow In oDTVariableState.Rows
            'If Nz(oRowVariableState("Name")).ToUpper.Contains("NETCAPITAL") Then
            '    MessageText.Text = "Save completed"
            'End If
            ' Per ogni variabile vado a ricercare il relativo oggetto di testo
            ' nel caso in cui sia su una variabile di tipo lista, allora procedo in modo diverso
            ' cerco i textbox con suffisso var_ e salvo i dati nella apposita tabella
            sVariableState = Nz(oRowVariableState("Name"))
            iIDVariableState = Nni(oRowVariableState("ID"))

            ' Con i dati appena recuperati, vado a prendere tutte le variabili presenti nella lista dei valori
            ' controllo anche che non siano valori di tipo lista, altrimenti procedo con il salvataggio all'interno della tabella _List
            Dim oDTVariablesStateValue As DataTable = HandleLoadVariableStateValue(iIDVariableState, m_SiteMaster.PeriodGetCurrent, Session("IDTeam"))

            ' Per ogni riga eseguo tutti i controlli ed eventuali salvataggi
            For Each oRowVariableStateValue As DataRow In oDTVariablesStateValue.Rows
                ' Controllo se di tipo lista
                Dim sValore As String = Nz(oRowVariableStateValue("Value"))
                If sValore.Contains("LIST OF ") Then

                    ' Carico tutti i valori relativi a questo tipo di variabile, dalla tabella value vado alla value_list
                    Dim oDTVariablesStateValueList As DataTable = HandleLoadVariableStateValueList(Nni(oRowVariableStateValue("ID")), m_SiteMaster.PeriodGetCurrent, Session("IDTeam"))

                    For Each oRowVariableValueList As DataRow In oDTVariablesStateValueList.Rows
                        sVariableName = Nz(oRowVariableValueList("VariableName"))
                        sNomeTextBox = sVariableName

                        oTXT = TryCast(FindControlRecursive(Page, sNomeTextBox), RadTextBox)
                        If Not IsNothing(oTXT) Then
                            HandleSaveVariableStateValueList(sVariableName, oTXT.Text, Nni(oRowVariableState("IDDataType")), oRowVariableValueList("ID"), oDAL)
                            If Nni(Session("IDTeamM&U")) > 0 Then
                                If Nni(Session("IDTeam")) = Nni(Session("IDTeamMaster")) Then
                                    Dim iIDVariableList As Integer
                                    sSQL = "SELECT ID FROM Variables_State_Value_List WHERE VariableName = '" & sVariableName.Replace(Session("TeamNameMaster"), Session("TeamNameM&U")) & "' AND IDPeriod = " & m_SiteMaster.PeriodGetCurrent
                                    iIDVariableList = Nni(oDAL.ExecuteScalar(sSQL))

                                    HandleSaveVariableStateValueList(sVariableName.Replace(Session("TeamNameMaster"), Session("TeamNameM&U")), oTXT.Text, Nni(oRowVariableState("IDDataType")), iIDVariableList, oDAL)
                                End If
                            End If
                        End If
                    Next

                Else
                    ' Inserisco il valore secco nella tabella variable value
                    ' var_83453_51_NetCapital_0_0
                    sNomeTextBox = "var_" & Nni(oRowVariableStateValue("ID")) & "_" & Nni(oRowVariableStateValue("IDPlayer")) & "_" & Nz(oRowVariableState("Name")) & "_" & Nni(oRowVariableStateValue("IDItem")) & "_" & Nni(oRowVariableStateValue("IDAge"))

                    oTXT = TryCast(FindControlRecursive(Page, sNomeTextBox), RadTextBox)
                    If Not IsNothing(oTXT) Then
                        ' Splitto i valori del no
                        ' Controllo la presenza del record nel database, altrimenti lo inserisco
                        sSQL = "UPDATE Variables_State_Value SET Value = '" & oTXT.Text & "' WHERE ID = " & Nni(oRowVariableStateValue("ID"))
                        oDAL.ExecuteNonQuery(sSQL)

                        If Nni(Session("IDTeamM&U")) > 0 Then
                            If Nni(Session("IDTeam")) = Nni(Session("IDTeamMaster")) Then
                                ' Recupero l'ID di M&U e procedo con il salvataggio
                                Dim iID As Integer
                                sSQL = "SELECT ID FROM Variables_State_Value WHERE IDPlayer = " & Session("IDTeamM&U") & " AND IDPeriod = " & m_SiteMaster.PeriodGetCurrent & " AND IDVariable = " & oRowVariableStateValue("IDVariable")
                                iID = Nni(oDAL.ExecuteScalar(sSQL))
                                sSQL = "UPDATE Variables_State_Value SET Value = '" & oTXT.Text & "' WHERE ID = " & iID
                                oDAL.ExecuteNonQuery(sSQL)
                            End If
                        End If
                    End If

                End If
            Next

        Next

        oDAL.GetConnObject.Close()
        oDAL.GetConnObject.Dispose()
        oDAL = Nothing

        MessageText.Text = "Save completed"
        Message.Visible = True

    End Sub

    Private Sub HandleSaveVariableStateValueList(VariableName As String, VariableValue As String, IDDataType As Integer, IDVariableStateValueList As Integer, DAL As DBHelper)
        Dim sSQL As String

        Dim iIDPeriodo As Integer = m_SiteMaster.PeriodGetCurrent

        ' Prima di tutto recupero il tipo di varibile per fare le opportune verifiche
        Dim bTypeVariableISCorrect As Boolean = ReturnVariableDataTypeISCorrect(IDDataType, VariableValue)

        If bTypeVariableISCorrect Then ' Tutto bene procedo con il salvataggio delle informazioni

            ' Se sono all'inzio del game, forse il periodo è 0, lo inizializzo a 1 per sicurezza
            If m_SiteMaster.PeriodGetCurrent <= 0 Then iIDPeriodo = 1

            If IDVariableStateValueList > 0 Then
                sSQL = "UPDATE Variables_State_Value_List SET Value = '" & VariableValue & "' WHERE ID = " & IDVariableStateValueList
            Else
                sSQL = "INSERT INTO Variables_State_Value_List (IDVariableState, VariableName, Value,  IDPeriod) VALUES " _
                     & "(" & IDVariableStateValueList & ",'" & VariableName & "', '" & VariableValue & "', " & iIDPeriodo & ") "
            End If
            DAL.ExecuteNonQuery(sSQL)

        End If
    End Sub

    Private Sub AddTableRow(Link As LinkButton, LinkLabel As String, LinkID As String, TextID As String, TextValue As String, IDDataType As Integer)
        Dim tblRow As TableRow
        Dim tblCell As TableCell

        If Link Is Nothing Then
            ' Costruisco la riga della variabile
            ' Costruisco la riga delle decisione
            tblRow = New TableRow
            tblCell = New TableCell

            ' Etichetta della variabile
            Link = New LinkButton
            Link.Text = LinkLabel
            Link.ID = LinkID
            AddHandler Link.Click, AddressOf lnkButton_Click

            tblCell.Controls.Add(Link)

            tblRow.Cells.Add(tblCell)

            ' Campo testo per la memorizzazione e la visualizzazione della varibile
            Dim oTxtBox As New RadTextBox
            tblCell = New TableCell
            oTxtBox.Text = "0,00"

            oTxtBox.ID = TextID
            If TextValue <> "" Then
                oTxtBox.Text = TextValue
                oTxtBox.Text = Nn(oTxtBox.Text).ToString("N2")
            End If

            oTxtBox.Attributes.Add("runat", "server")
            oTxtBox.Style.Add("text-align", "right")
            oTxtBox.Style.Add("RenderMode", "Lightweight")
            oTxtBox.ClientIDMode = ClientIDMode.Static
            oTxtBox.EnableViewState = True

            tblCell.Controls.Add(oTxtBox)
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            ' Etichetta per la definizione del tipo di variabile
            tblCell = New TableCell
            tblCell.Text = ReturnStandardLabel_Decisions_Variables(IDDataType)
            tblRow.Cells.Add(tblCell)

            tblVariableState.Rows.Add(tblRow)
        End If
    End Sub

    Private Sub LoadVariableState()
        Dim sSQL As String
        Dim sNomeItem As String

        Dim sIDGruppi As String = ""

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Dim sLabelLink As String = ""
        Dim sNomeLink As String = ""

        Dim sNomeTextBox As String = ""
        Dim sValue As String = ""

        Dim iIDDataType As Integer = 0
        Dim iPeriodo As Integer

        If Session("IDRole").Contains("B") Or Session("IDRole").Contains("D") Or Session("IDRole").Contains("T") Then
            iPeriodo = m_SiteMaster.PeriodGetCurrent
        Else
            iPeriodo = m_SiteMaster.PeriodGetPrev
        End If

        ' Procedo al caricamento dei gruppi delle variabili
        sSQL = "SELECT DISTINCT IDGroup FROM Variables_State WHERE IDGame = " & Nni(Session("IDGame"))
        Dim oDTGroups As DataTable = g_DAL.ExecuteDataTable(sSQL)
        For Each oRowGrp As DataRow In oDTGroups.Rows
            sIDGruppi &= Nni(oRowGrp("IDGroup")) & ","
        Next

        ' Tronco la stringa che contiene tutti gli id dei gruppi da caricare, l'ultimo carattere sicurmente è una virgola
        If sIDGruppi.EndsWith(",") Then
            sIDGruppi = sIDGruppi.Remove(sIDGruppi.Length - 1)
        End If

        ' Carico solo i gruppi a cui sono associate le varibili di stato, mi evito dei cicli macchina inutili per saltare gli eventuali gruppi
        ' non associati ad alcuna variabile
        If sIDGruppi <> "" AndAlso sIDGruppi <> "0" Then
            sSQL = "SELECT ID, GroupDescription FROM Variables_Groups WHERE IDGame = " & Nni(Session("IDGame")) & " AND ID IN(" & sIDGruppi & ") "
        Else
            sSQL = "SELECT ID, GroupDescription FROM Variables_Groups WHERE IDGame = " & Nni(Session("IDGame"))
        End If

        ' Recupero le informazioni necessarie per costruirmi l'header della tabella HTML
        Dim oDTHeader As DataTable = g_DAL.ExecuteDataTable(sSQL)

        tblVariableState.Rows.Clear()

        ' Per ogni riga di intestazione inizio a costruire la tabella
        For Each oRowHeader As DataRow In oDTHeader.Rows
            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            tblHeaderCell.Text = Nz(oRowHeader("GroupDescription"))
            tblHeaderCell.ColumnSpan = 3
            tblHeaderCell.BorderStyle = BorderStyle.None

            tblHeaderRow.Cells.Add(tblHeaderCell)

            tblVariableState.Rows.Add(tblHeaderRow)

            ' Recupero tutte le variabili per il sottotitolo selezionato
            Dim oDTVariableState As DataTable = HandleLoadVariableState(Nni(Session("IDGame")))

            ' Carico tutte le variabili relative a questo titolo:
            Dim oDRVariableState As DataRow() = oDTVariableState.Select("IDGroup = " & Nni(oRowHeader("ID")))

            For Each oRowVariableState As DataRow In oDRVariableState
                ' Per ogni varibile di stato presente procedo con il controllo sul tipo da caricare
                Dim oDTVariableStateValue As DataTable = HandleLoadVariableStateValue(Nni(oRowVariableState("ID")), iPeriodo, Session("IDTeam"))

                For Each oRowVariableStateValue As DataRow In oDTVariableStateValue.Rows
                    ' Se non contiene la dicitura LIST allora provvedo all'inserimento secco del valore
                    If Nz(oRowVariableStateValue("Value")).Contains("LIST OF") Then
                        ' Sono su UNa variabile di tipo lista valori
                        ' Carico la il datatable relativo alla variabile interessata

                        Dim oDTVariableStateValueList As DataTable = HandleLoadVariableStateValueList(Nni(oRowVariableStateValue("ID")), iPeriodo, Session("IDTeam"))
                        For Each oRowVariableStateValueList As DataRow In oDTVariableStateValueList.Rows
                            sLabelLink = Nz(oRowVariableState("VariableLabel"))
                            iIDDataType = Nni(oRowVariableState("IDDataType"))
                            sValue = Nz(oRowVariableStateValueList("Value"))

                            sNomeTextBox = Nz(oRowVariableStateValueList("VariableName"))

                            ' Splitto il nome della variabile per recuperare le informazioni necessarie
                            Dim sSplitName() As String = Nz(oRowVariableStateValueList("VariableName")).Split("_")
                            sNomeItem = sSplitName(sSplitName.Length - 1)

                            sLabelLink = sSplitName(1) & " - " & sSplitName(3) & " <br/> " & sLabelLink & " <br/> " & sNomeItem

                            If Nni(oRowVariableStateValueList("IDPlayer")) > 0 Then
                                sNomeLink = "value_Player_" & Nni(oRowVariableStateValueList("IDPlayer")).ToString & "_" & Nz(oRowVariableState("Name")) & "_" & Nni(oRowVariableStateValueList("ID")).ToString

                            ElseIf Nni(oRowVariableStateValueList("IDItem")) > 0 Then
                                sNomeLink = "value_Item_" & Nni(oRowVariableStateValueList("IDItem")).ToString & "_" & Nz(oRowVariableState("Name")) & "_" & Nni(oRowVariableStateValueList("ID")).ToString

                            ElseIf Nni(oRowVariableStateValueList("IDAge")) > 0 Then
                                sNomeLink = "value_Age_" & Nni(oRowVariableStateValueList("IDAge")).ToString & "_" & Nz(oRowVariableState("Name")) & "_" & Nni(oRowVariableStateValueList("ID")).ToString

                            End If

                            ' Controllo la presenza del link button, se non lo trovo lo inserisco, altrimenti esco dal ciclo e passo al successivo
                            Dim oLnk As LinkButton = TryCast(FindControlRecursive(Page, sNomeLink), LinkButton)
                            AddTableRow(oLnk, sLabelLink, sNomeLink, sNomeTextBox, sValue.Trim, iIDDataType)
                        Next

                    Else
                        ' Inserisco la variabile con valore secco
                        ' Recupero il nome dell'IDItem, del team e dell'age
                        sNomeItem = GetItemName(Nni(oRowVariableStateValue("IDItem")))
                        Dim sNomeTeam As String = GetTeamName(Nni(Session("IDGame")), Nni(oRowVariableStateValue("IDPlayer")))
                        Dim sNomeAge As String = GetAgeName(Nni(oRowVariableStateValue("IDAge")))

                        sLabelLink = sNomeTeam
                        If sNomeItem <> "" Then
                            sLabelLink &= " - " & sNomeItem
                        End If

                        If sNomeAge <> "" Then
                            sLabelLink &= " - " & sNomeAge
                        End If

                        sLabelLink = sLabelLink & "<br/>" & Nz(oRowVariableState("VariableLabel"))
                        iIDDataType = Nni(oRowVariableState("IDDataType"))
                        sNomeLink = "value_" & Nni(oRowVariableStateValue("IDVariable")) & "_" & Nz(oRowVariableState("Name")) & "_" & Nni(oRowVariableStateValue("ID"))
                        sNomeTextBox = "var_" & Nni(oRowVariableStateValue("ID")) & "_" & Nni(oRowVariableStateValue("IDPlayer")) & "_" & Nz(oRowVariableState("Name")) & "_" & Nni(oRowVariableStateValue("IDItem")) & "_" & Nni(oRowVariableStateValue("IDAge"))
                        sValue = Nz(oRowVariableStateValue("Value"))

                        ' Controllo la presenza del link button, se non lo trovo lo inserisco, altrimenti esco dal ciclo e passo al successivo
                        Dim oLnk As LinkButton
                        'If Nz(oRowVariableState("Name")) = "VariaDeltaStock" Then
                        '    oLnk = TryCast(FindControlRecursive(Page, sNomeLink), LinkButton)
                        'Else
                        oLnk = TryCast(FindControlRecursive(Page, sNomeLink), LinkButton)
                        'End If
                        AddTableRow(oLnk, sLabelLink, sNomeLink, sNomeTextBox, sValue.Trim, iIDDataType)

                    End If
                Next
            Next

            tblRow = New TableRow
            tblCell = New TableCell

            tblCell.ColumnSpan = 3
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("min-height", "10px")
            tblCell.BorderStyle = BorderStyle.None

            tblRow.Cells.Add(tblCell)

            tblVariableState.Rows.Add(tblRow)
        Next
    End Sub

#Region "GESTIONE DEL PANNELLO"

    Private Sub LoadDataType()
        Dim oDTDataType As DataTable = HandleLoadComboBox_Variables_DataType()

        Try
            cboDataType.Items.Clear()
            cboDataType.DataSource = Nothing
            cboDataType.DataSource = oDTDataType
            cboDataType.DataValueField = "Id"
            cboDataType.DataTextField = "TypeDescription"
            cboDataType.DataBind()
            cboDataType.SelectedValue = -1
        Catch ex As Exception
            MessageText.Text = "Error -> HandleLoadDataType -> " & ex.Message
            Message.Visible = True

            Throw New ApplicationException("Developer_Variables_State.aspx -> HandleLoadDataType", ex)
        End Try
    End Sub

    Private Sub ClearPanelNewDecision()
        Session("IDVariableLoad") = Nothing
        Session("IDVariable") = Nothing
        Session("TypeVariable") = Nothing

        txtVariable.Text = ""
        txtVariableLabel.Text = ""

        cboDataType.SelectedValue = -1
        cboGroup.SelectedValue = -1

        chkItem.Checked = False
        chkPlayers.Checked = False
        chkAge.Checked = False
    End Sub

    Private Sub LoadGroups()
        Dim oDTGroups As DataTable = HandleLoadVariablesGroups()

        Try
            cboGroup.Items.Clear()
            cboGroup.DataSource = Nothing
            cboGroup.DataSource = oDTGroups
            cboGroup.DataValueField = "Id"
            cboGroup.DataTextField = "GroupDescription"
            cboGroup.DataBind()
            cboGroup.SelectedValue = -1
        Catch ex As Exception
            MessageText.Text = "Error -> LoadGroups -> " & ex.Message
            Message.Visible = True

            Throw New ApplicationException("Developer_Variables_State.aspx -> LoadGroups", ex)
        End Try
    End Sub

    Private Sub btnHide_Click(sender As Object, e As EventArgs) Handles btnHide.Click
        ClearPanelNewDecision()

        mpeMain.Hide()
    End Sub

    Private Sub SaveVariableStateData(Trans As IDbTransaction, IDVariableState As Integer)
        Dim sSQL As String
        Dim iIDVariableStateValue As Integer

        ' Con l'ID della variabile mi recupero anche la descrizione per comporre il nome della varibile da salvare nella tabella di tipo lista
        Dim sNomeVariabile As String = txtVariable.Text.Trim.Replace("'", " ")

        ' Controllo de deve essere una lista di ITEMS, PLAYERS, AGE
        If chkPlayers.Checked AndAlso chkItem.Checked Then '[LIST OF PLAYERS$ITEMS]
            ' Carico tutti i Players presenti in archivio
            Dim oDTPlayers As DataTable = HandleLoadPlayersGame(Nni(Session("IDGame")))
            Dim oDTItem As DataTable = LoadItemsGame(Nni(Session("IDGame")), "")

            ' Inserisco la variabile master per poi procedere con l'inserimento dell'elenco delle variabili
            sSQL = "INSERT INTO Variables_State_Value (IDVariable, Value) VALUES (" & IDVariableState & ", '[LIST OF PLAYERS$ITEMS]') "
            g_DAL.ExecuteNonQuery(sSQL, Trans)
            ' Recupero l'ID della variabile appena inserita
            sSQL = "SELECT MAX(ID) FROM Variables_State_Value WHERE IDVariable = " & IDVariableState
            iIDVariableStateValue = Nni(g_DAL.ExecuteScalar(sSQL, Trans))

            ' Ciclo su tutti i PLAYERS e AGE per effettuare l'inserimento dei valori
            For Each oRowPlayer As DataRow In oDTPlayers.Rows
                ' Con l'ID del player ado a caricare il nome del team
                Dim sTeamName As String = GetTeamName(Nni(Session("IDGame")), Nni(oRowPlayer("ID")))
                For Each oRowItem As DataRow In oDTItem.Rows
                    ' var_M&U_RawStockQuali_Fabrics
                    Dim sVariableName As String = "var_" & sTeamName & "_" & txtVariable.Text.Trim.Replace("'", " ") & "_" & Nz(oRowItem("VariableName"))
                    sSQL = "INSERT INTO Variables_State_Value_List (IDVariableState, IDPlayer, IDItem, IDPeriod, VariableName, Value) VALUES (" _
                         & iIDVariableStateValue & ", " & Nni(oRowPlayer("ID")) & ", " & Nni(oRowItem("ID")) & ", '" & sVariableName & "', '" & txtValue.Text.Trim & "') "
                    g_DAL.ExecuteNonQuery(sSQL, Trans)
                Next
            Next

        ElseIf chkPlayers.Checked AndAlso chkAge.Checked Then '[LIST OF PLAYERS$AGE]
            ' Carico tutti i Players presenti in archivio
            Dim oDTPlayers As DataTable = HandleLoadPlayersGame(Nni(Session("IDGame")))
            Dim ODTAge As DataTable = HandleLoadAgeData(Nni(Session("IDGame")))

            ' Inserisco la variabile master per poi procedere con l'inserimento dell'elenco delle variabili
            sSQL = "INSERT INTO Variables_State_Value (IDVariable, Value) VALUES (" & IDVariableState & ", '[LIST OF PLAYERS$AGE]') "
            g_DAL.ExecuteNonQuery(sSQL, Trans)
            ' Recupero l'ID della variabile appena inserita
            sSQL = "SELECT MAX(ID) FROM Variables_State_Value WHERE IDVariable = " & IDVariableState
            iIDVariableStateValue = Nni(g_DAL.ExecuteScalar(sSQL, Trans))

            ' Ciclo su tutti i PLAYERS e ITEMS per effettuare l'inserimento dei valori
            For Each oRowPlayer As DataRow In oDTPlayers.Rows
                ' Con l'ID del player ado a caricare il nome del team
                Dim sTeamName As String = GetTeamName(Nni(Session("IDGame")), Nni(oRowPlayer("ID")))
                For Each oRowAge As DataRow In ODTAge.Rows
                    Dim sVariableName As String = "var_" & sTeamName & "_" & txtVariable.Text.Trim.Replace("'", " ") & "_" & Nz(oRowAge("VariableName"))
                    sSQL = "INSERT INTO Variables_State_Value_List (IDVariableState, IDPlayer, IDItem, IDPeriod, VariableName, Value) VALUES (" _
                         & iIDVariableStateValue & ", " & Nni(oRowPlayer("ID")) & ", " & Nni(oRowAge("ID")) & ", '" & sVariableName & "', '" & txtValue.Text.Trim & "') "
                    g_DAL.ExecuteNonQuery(sSQL, Trans)
                Next
            Next

        ElseIf chkPlayers.Checked AndAlso (Not chkAge.Checked AndAlso Not chkItem.Checked) Then
            ' Inserisco valori univoci solo per le variabili di stato legate al player
            ' Carico tutti i Players presenti in archivio
            Dim oDTPlayers As DataTable = HandleLoadPlayersGame(Nni(Session("IDGame")))
            ' Ciclo su tutti i PLAYERS  per effettuare l'inserimento dei valori
            For Each oRowPlayer As DataRow In oDTPlayers.Rows
                sSQL = "INSERT INTO Variables_State_Value (IDVariable, IDPlayer, IDPeriod, Value) VALUES (" & IDVariableState & ", " & Nni(oRowPlayer("ID")) & ", " _
                     & "1,  '" & txtValue.Text.Trim & "') "
                g_DAL.ExecuteNonQuery(sSQL, Trans)
            Next

        ElseIf chkItem.Checked AndAlso (Not chkAge.Checked AndAlso Not chkPlayers.Checked) Then
            ' Inserisco valori univoci solo per le variabili di stato legate all'item
            Dim oDTItem As DataTable = LoadItemsGame(Nni(Session("IDGame")), "")
            ' Ciclo su tutti i PLAYERS  per effettuare l'inserimento dei valori
            For Each oRowItem As DataRow In oDTItem.Rows
                sSQL = "INSERT INTO Variables_State_Value (IDVariable, IDItem, IDPeriod, Value) VALUES (" & IDVariableState & ", " & Nni(oRowItem("ID")) & ", " _
                     & "1,  '" & txtValue.Text.Trim & "') "
                g_DAL.ExecuteNonQuery(sSQL, Trans)
            Next

        ElseIf chkAge.Checked AndAlso (Not chkItem.Checked AndAlso Not chkPlayers.Checked) Then
            ' Inserisco valori univoci solo per le variabili di stato legate all'item
            Dim oDTAge As DataTable = HandleLoadAgeData(Nni(Session("IDGame")))
            ' Ciclo su tutti i PLAYERS  per effettuare l'inserimento dei valori
            For Each oRowAge As DataRow In oDTAge.Rows
                sSQL = "INSERT INTO Variables_State_Value (IDVariable, IDItem, IDPeriod, Value) VALUES (" & IDVariableState & ", " & Nni(oRowAge("ID")) & ", " _
                     & "1,  '" & txtValue.Text.Trim & "') "
                g_DAL.ExecuteNonQuery(sSQL, Trans)
            Next

        Else
            ' Inserisco la variabile con il valore fisso, senza riferimenti a tabelle esterne di alcun tipo
            sSQL = "INSERT INTO Variables_State_Value (IDVariable, Value) VALUES (" & IDVariableState & ", '" & txtValue.Text.Trim & "') "
            g_DAL.ExecuteNonQuery(sSQL, Trans)
        End If
    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        Dim sSQL As String = ""

        Dim bVerificaTransazione As Boolean = True
        Dim oTrans As IDbTransaction = Nothing

        Dim iIDVariableState As Integer
        Dim iIDVariableStateValue As Integer

        Try
            Message.Visible = False

            ' Ho una variabile valida, procedo con l'aggiornamento
            ' Controllo che esista la descrizione per il titolo altrimenti la creo
            oTrans = g_DAL.BeginTransaction()

            ' Procedo con il salvataggio della nuova variabile di stato o modifica di quella esistente
            If Nni(Session("IDVariableLoad")) <> 0 Then
                If txtVariable.Text <> "" Then
                    sSQL = "UPDATE Variables_State SET IDGroup = " & Nni(cboGroup.SelectedValue) & "', Name = '" & txtVariable.Text.Trim.Replace("'", " ") & "', " _
                         & "VariableLabel = '" & txtVariableLabel.Text.Trim.Replace("'", " ") & "' " _
                         & "WHERE ID = " & Nni(Session("IDVariableLoad"))
                    g_DAL.ExecuteNonQuery(sSQL, oTrans)
                End If

            Else ' Altrimenti verifico la presenza della descrizione e procedo con l'inserimento di una nuova
                If txtVariable.Text <> "" Then
                    sSQL = "INSERT INTO Variables_State (IDGame, IDGroup, Name, VariableLabel) VALUES (" & Nni(Session("IDGame")) & ", " _
                         & Nni(cboGroup.SelectedValue) & ", '" & txtVariable.Text.Trim.Replace("'", " ") & "', '" & txtVariableLabel.Text.Trim.Replace("'", " ") & "') "
                    g_DAL.ExecuteNonQuery(sSQL, oTrans)

                    ' Recupero l'ultimo ID inserito
                    sSQL = "Select MAX(ID) FROM Variables_State WHERE IDGame = " & Nni(Session("IDGame"))
                    iIDVariableState = Nni(g_DAL.ExecuteScalar(sSQL, oTrans))
                End If

            End If

            ' ##################################### INSERIMENTO NUOVI VALORI ##############################################################################
            ' Se ho inserito una nuova variabile provvedo anche all'inserimento del valore di default nella tabella dei valori
            If sSQL.Contains("INSERT") Then
                SaveVariableStateData(oTrans, iIDVariableState)

            Else
                ' ##################################### AGGIORNAMENTO VALORI ##############################################################################

                ' Cancello tutti i valori legati a questa variabile, ovviamente lascio solo quello che è presente nella tabelle Variable_State
                ' La tabella master di tutte le variabili di stato 
                ' Prima di tutto recupero l'ID della tabella di mezzo, Variables_State_Value, così posso anche cancellare i valori nella tabella
                ' di dettaglio 
                sSQL = "SELECT ID FROM Variables_State_Value WHERE IDVariable = " & Nni(Session("IDVariableLoad"))
                iIDVariableStateValue = Nni(g_DAL.ExecuteScalar(sSQL, oTrans))

                ' Cancello i valori di dettaglio
                sSQL = "DELETE FROM Variables_State_Value_List WHERE IDVariableState = " & iIDVariableStateValue
                g_DAL.ExecuteScalar(sSQL, oTrans)

                ' Cancello tutti i valori presenti nella tabella intermedia dei valori
                sSQL = "DELETE FROM Variables_State_Value WHERE IDVariable = " & iIDVariableStateValue
                g_DAL.ExecuteScalar(sSQL, oTrans)

                SaveVariableStateData(oTrans, iIDVariableState)

            End If

            g_DAL.CommitTransaction(oTrans)
            oTrans.Dispose()

            bVerificaTransazione = False

            SQLConnClose(g_DAL.GetConnObject)

            LoadVariableState()
            pnlMain.Update()

            ClearPanelNewDecision()

            mpeMain.Hide()
        Catch ex As Exception
            If Not oTrans Is Nothing AndAlso bVerificaTransazione Then g_DAL.RollbackTransaction(oTrans)
            MessageText.Text = "Error btnOK_Click -> " & ex.Message
            Message.Visible = True
        End Try

    End Sub

#End Region

    Private Sub lnkButton_Click(sender As Object, e As EventArgs)
        Dim oLnk As LinkButton = CType(sender, LinkButton)
        Dim sVariable As String = oLnk.ID

        ' Splitto l'ID per identificare se ho cliccato su un link di tipo lista o di tipo valore
        Dim sVariabileSplit() As String = sVariable.Split("_")

        ' Carico il valore da controllare per il caricamento dei dati nel pannello
        Dim sVariableType As String = sVariabileSplit(1).ToUpper

        ' Controllo il primo item dell'array 
        If sVariableType.Contains("PLAYER") Or sVariableType.Contains("ITEM") Or sVariableType.Contains("AGE") Then
            Session("TypeVariable") = "LIST"

        ElseIf sVariabileSplit(1).ToUpper.Contains("SINGLE") Then
            Session("TypeVariable") = "SINGLEVALUE"

        End If

        Session("IDVariableLoad") = Nni(sVariabileSplit(4))
        mpeMain.Show()

    End Sub

    Private Sub Developer_Variables_State_Init(sender As Object, e As EventArgs) Handles Me.Init
        m_SessionData = Session("SessionData")

        m_SiteMaster = Me.Master

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oCBOPeriod As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler m_SiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler m_SiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(m_SessionData.Utente.Games.IDGame) ' Ultimo periodo
        oCBOPeriod.SelectedValue = Session("IDPeriodValid")
        m_SiteMaster.PeriodChange = Session("IDPeriodValid")

        Session("NameTeam") = GetTeamName(Session("IDTeam"))
        oCBOTeam.SelectedValue = Session("IDTeam")
        m_SiteMaster.TeamChange = Session("IDTeam")

        LoadVariableState()

    End Sub

    Private Sub Developer_Variables_State_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsNothing(m_SessionData) Then
            PageRedirect("ErrorTimeout.aspx", "", "")
        End If

        If Not IsPostBack Then
            LoadDataType()
            LoadGroups()
        End If
    End Sub

    Private Sub HandleSaveVariableList(ID As Integer, IDDecisionBossValue As Integer, VariableName As String, IDPeriod As Integer, Trans As IDbTransaction)
        If ID > 0 Then ' Sono in aggiornamento

        Else ' Sono in inserimento

        End If
        SQLConnClose(g_DAL.GetConnObject)
    End Sub

    Private Sub btnNewDecision_Click(sender As Object, e As EventArgs) Handles btnNewDecision.Click
        ' Svuoto eventuali variabili di sessione usate
        Session("IDVariable") = 0
        Session("TypeVariable") = ""
        txtVariable.Text = ""
        txtVariableLabel.Text = ""
        txtValue.Text = ""
        cboDataType.SelectedValue = -1
        cboGroup.SelectedValue = -1
        chkItem.Checked = False
        chkPlayers.Checked = False
        chkAge.Checked = False
    End Sub

    Private Sub pnlNewDecision_PreRender(sender As Object, e As EventArgs) Handles pnlNewDecision.PreRender
        If Not Session("IDVariableLoad") Is Nothing Then
            Dim sSQL As String = ""

            ' Carico i campi del pannello
            ' Recupero tutto le informazioni necessarie per caricare i dati
            ' Controllo il tipo di variabile da caricare
            If Session("TypeVariable") = "SINGLEVALUE" Then
                sSQL = "SELECT VS.ID, VS.IDGame, VS.Name, VS.VariableLabel, VS.IDGroup, VS.IDDataType, " _
                     & "VSV.Id AS IDVariableStateValue, VSV.IDPlayer AS IDPlayerValue, VSV.IDItem AS IDItemValue, VSV.IDAge AS IDAgeValue, VSV.Value AS VariableStateValue_Value, " _
                     & "FROM Variables_State VS " _
                     & "INNER JOIN Variables_State_Value VSV ON VS.Id = VSV.IDVariable " _
                     & "WHERE VSV.ID = " & Nni(Session("IDVariableLoad"))

            ElseIf Session("TypeVariable") = "LIST" Then
                sSQL = "SELECT VS.ID, VS.IDGame, VS.Name, VS.VariableLabel, VS.IDGroup, VS.IDDataType, " _
                     & "VSV.Id AS IDVariableStateValue, VSV.IDPlayer AS IDPlayerValue, VSV.IDItem AS IDItemValue, VSV.IDAge AS IDAgeValue, VSV.Value AS VariableStateValue_Value, " _
                     & "VSVL.IDAge, VSVL.IDItem, VSVL.IDPeriod, VSVL.IDPlayer, VSVL.VariableName, VSVL.Value " _
                     & "FROM Variables_State VS " _
                     & "INNER JOIN Variables_State_Value VSV ON VS.Id = VSV.IDVariable " _
                     & "INNER JOIN Variables_State_Value_List VSVL ON VSV.ID = VSVL.IDVariableState " _
                     & "WHERE VSVL.ID = " & Nni(Session("IDVariableLoad"))

            End If

            Dim oDTDataToLoad As DataTable = g_DAL.ExecuteDataTable(sSQL)
            For Each oRow As DataRow In oDTDataToLoad.Rows
                cboGroup.SelectedValue = Nni(oRow("IDGroup"))
                cboDataType.SelectedValue = Nni(oRow("IDDataType"))

                txtVariableLabel.Text = Nz(oRow("VariableLabel"))
                txtVariable.Text = Nz(oRow("Name"))

                If Session("TypeVariable") = "LIST" Then
                    If Nz(oRow("VariableStateValue_Value")).ToUpper.Contains("[LIST OF PLAYERS$ITEMS]") Then
                        chkItem.Checked = True
                        chkPlayers.Checked = True
                    End If

                    If Nz(oRow("VariableStateValue_Value")).ToUpper.Contains("[LIST OF PLAYERS$AGE]") Then
                        chkPlayers.Checked = True
                        chkAge.Checked = True
                    End If

                    If Nz(oRow("VariableStateValue_Value")).ToUpper.Contains("AGE") Then
                        chkPlayers.Checked = True
                    End If

                    txtValue.Text = Nz(oRow("Value"))

                ElseIf Session("TypeVariable") = "SINGLEVALUE" Then
                    txtValue.Text = Nz(oRow("Value"))

                    chkPlayers.Checked = Nb(oRow("IDPlayerValue"))
                    chkItem.Checked = Nb(oRow("IDItemValue"))
                    chkAge.Checked = Nb(oRow("IDAgeValue"))
                End If

            Next
        End If
    End Sub

End Class
