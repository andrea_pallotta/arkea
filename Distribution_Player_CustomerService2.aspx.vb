﻿Imports System.Data
Imports APS.APSUtility
Imports APPCore.Utility
Imports Telerik.Web.UI

Partial Class Distribution_Player_CustomerService2
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        Message.Visible = False
        LoadData()
        UpdatePanelMain.Update()
    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        Message.Visible = False
        LoadData()
        UpdatePanelMain.Update()
    End Sub

    Private Sub Distribution_Player_CustomerService2_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        m_SessionData = Session("SessionData")

        m_SiteMaster = Me.Master

        m_SiteMaster.LoadPeriodsValid = Session("IDRole") = "P"

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oCBOPeriod As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOPeriodValid As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriodValid"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler m_SiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler m_SiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(Session("IDGame")) ' Ultimo periodo
        oCBOPeriod.SelectedValue = Session("IDPeriodValid")
        m_SiteMaster.PeriodChange = Session("IDPeriodValid")

        Session("NameTeam") = GetTeamName(Session("IDTeam"))
        oCBOTeam.SelectedValue = Session("IDTeam")
        m_SiteMaster.TeamChange = Session("IDTeam")


        ' Gestione del pannello delle decisioni concesse
        modalPopup.OpenerElementID = lnkHelp.ClientID
        modalPopup.Modal = False
        modalPopup.VisibleTitlebar = True

        LoadHelp()

    End Sub

    Private Sub LoadData()
        LoadTableCustomerService()
    End Sub

    Private Sub LoadTableCustomerService()
        Dim dTotalExpenditure As Double
        Dim dTotalRevenMarke As Double

        Dim sSQL As String

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Try
            tblCustomerService.Rows.Clear()
            lblTitolo.Text = "Customer service "

            sSQL = "SELECT SUM(CAST(REPLACE(Value, ',', '.') AS Decimal(18, 9))) / COUNT(C.ID) " _
                 & "FROM Variables_Calculate_Define V " _
                 & "INNER JOIN Variables_Calculate_Value C ON V.Id = C.IDVariable " _
                 & "WHERE LTRIM(RTRIM(V.VariableName)) = 'TrainServiAllow' AND IDperiod = " & Session("IDPeriod")
            If Session("IDRole") = "P" Then
                sSQL &= " AND ISNULL(C.Simulation, 0) = 0 "
            End If
            dTotalExpenditure = Nn(g_DAL.ExecuteScalar(sSQL))

            dTotalRevenMarke = GetVariableDefineValue("TotalRevenMarke", Session("IDTeam"), 0, 0, Session("IDPeriod"), Session("IDGame"))

            If dTotalExpenditure = 0 AndAlso dTotalRevenMarke = 0 Then
                grfCustomerServiceGraph.Visible = False
                lblTitolo.Text = "Customer service " & "<br/>" & "<br/>" & "No data available for this period"
                Exit Sub
            End If

            grfCustomerServiceGraph.Visible = True
            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header vuota
            Dim oLBLTitleGrid As New RadLabel
            oLBLTitleGrid.ID = "lblGridTitle"
            oLBLTitleGrid.Text = "Customer service"
            oLBLTitleGrid.ForeColor = Drawing.Color.White
            tblHeaderCell.Controls.Add(oLBLTitleGrid)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.ColumnSpan = 2
            tblHeaderCell.Style.Add("background", "#525252")

            tblHeaderRow.BorderStyle = BorderStyle.None
            tblHeaderRow.Style.Add("margin-top", "10px")
            tblHeaderRow.Cells.Add(tblHeaderCell)
            tblCustomerService.Rows.Add(tblHeaderRow)

            ' Aggiungo le righe di dettaglio
            ' Prima riga
            tblRow = New TableRow
            tblCell = New TableCell

            Dim oLBLMarketAverageExpenditure As New RadLabel
            oLBLMarketAverageExpenditure.ID = "lblMarketAverageExpenditure"
            oLBLMarketAverageExpenditure.Text = "Market average expenditure"
            oLBLMarketAverageExpenditure.ForeColor = Drawing.Color.DarkRed
            tblCell.Controls.Add(oLBLMarketAverageExpenditure)
            tblCell.Style.Add("width", "60%")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLBLMarketAverageExpenditureValue As New RadLabel
            oLBLMarketAverageExpenditureValue.ID = "lblMarketAverageExpenditureValue"
            oLBLMarketAverageExpenditureValue.Text = dTotalExpenditure.ToString("N0")
            tblCell.Controls.Add(oLBLMarketAverageExpenditureValue)
            tblCell.Style.Add("width", "40%")
            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCustomerService.Rows.Add(tblRow)

            ' Seconda riga
            tblRow = New TableRow
            tblCell = New TableCell

            Dim oLBLExpenditure As New RadLabel
            oLBLExpenditure.ID = "lblExpenditure"
            oLBLExpenditure.Text = "Expenditure team"
            oLBLExpenditure.ForeColor = Drawing.Color.DarkRed
            tblCell.Controls.Add(oLBLExpenditure)
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim lnkTrainServiAllow As New HyperLink
            lnkTrainServiAllow.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=TrainServiAllow&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            lnkTrainServiAllow.Style.Add("text-decoration", "none")
            lnkTrainServiAllow.Style.Add("cursor", "pointer")

            lnkTrainServiAllow.Text = Nn(GetVariableDefineValue("TrainServiAllow", Session("IDTeam"), 0, 0, Session("IDPeriod"), Session("IDGame"))).ToString("N0")

            tblCell.Controls.Add(lnkTrainServiAllow)

            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblCustomerService.Rows.Add(tblRow)

            ' Terza riga
            tblRow = New TableRow
            tblCell = New TableCell

            Dim oLBLTrainServiAllow As New RadLabel
            oLBLTrainServiAllow.ID = "lblTrainServiAllow"
            oLBLTrainServiAllow.Text = "Ratio on revenues"
            oLBLTrainServiAllow.ForeColor = Drawing.Color.DarkRed
            tblCell.Controls.Add(oLBLTrainServiAllow)
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLBLTrainServiAllowValue As New RadLabel
            oLBLTrainServiAllowValue.ID = "lblTrainServiAllowValue"
            If dTotalRevenMarke > 0 Then
                oLBLTrainServiAllowValue.Text = (Nn(GetVariableDefineValue("TrainServiAllow", Session("IDTeam"), 0, 0, Session("IDPeriod"), Session("IDGame"))) / dTotalRevenMarke).ToString("P2")
            Else
                oLBLTrainServiAllowValue.Text = 0.ToString("P2")
            End If
            tblCell.Controls.Add(oLBLTrainServiAllowValue)
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblCustomerService.Rows.Add(tblRow)

            LoadDataGraphCustomerService()
        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub LoadDataGraphCustomerService()
        Dim sSQL As String
        Dim oDTTableGraph As New DataTable
        Dim oDTTableResult As DataTable
        Dim dRatioPersoReven As Double
        Dim sPeriod As String

        ' Preparo il datatable che ospiterà i dati
        oDTTableGraph.Columns.Add("TrainServiAllow", GetType(Double))
        oDTTableGraph.Columns.Add("Period", GetType(String))

        sSQL = "SELECT P.Descrizione AS Period, SUM(CAST(REPLACE(Value, ',', '.') AS Decimal(18, 9))) / COUNT(C.ID) AS Value " _
             & " FROM Variables_Calculate_Define V " _
             & "INNER JOIN Variables_Calculate_Value C ON V.Id = C.IDVariable " _
             & "INNER JOIN BGOL_Periods P ON C.IDPeriod = P.Id " _
             & "WHERE LTRIM(RTRIM(V.VariableName)) = 'TrainServiAllow' AND C.IDPlayer = " & Nni(Session("IDTeam")) & " " _
             & " AND C.IDPeriod <= " & m_SiteMaster.PeriodGetCurrent
        If Session("IDRole") = "P" Then
            sSQL &= " AND ISNULL(C.Simulation, 0) = 0 "
        End If
        sSQL &= "GROUP BY P.Descrizione"
        oDTTableResult = g_DAL.ExecuteDataTable(sSQL)

        For Each oRow As DataRow In oDTTableResult.Rows
            dRatioPersoReven = Nn(oRow("Value"))
            sPeriod = Nz(oRow("Period"))

            oDTTableGraph.Rows.Add(dRatioPersoReven.ToString("N0"), sPeriod)
        Next

        grfCustomerServiceGraph.DataSource = oDTTableGraph
        grfCustomerServiceGraph.DataBind()

        grfCustomerServiceGraph.PlotArea.XAxis.DataLabelsField = "Period"
        grfCustomerServiceGraph.PlotArea.XAxis.LabelsAppearance.TextStyle.Bold = False
        grfCustomerServiceGraph.PlotArea.XAxis.LabelsAppearance.TextStyle.FontSize = 9
        grfCustomerServiceGraph.PlotArea.XAxis.EnableBaseUnitStepAuto = True
    End Sub

    Private Sub Distribution_Player_CustomerService2_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        If Not Page.IsPostBack Then
            If Session("IDRole") = "P" Then
                m_SiteMaster.ReloadPeriodsCalculate()
            End If
            LoadData()

            btnTranslate.Visible = Session("IDRole").Contains("D")
            ' Controllo tutti gli oggetti presenti
            Dim oTranslation As New TranslationObjectForm(Me, g_DAL, Nz(Session("LanguageActive")))

        End If
    End Sub
    Private Sub btnTranslate_Click(sender As Object, e As EventArgs) Handles btnTranslate.Click
        Session("PageOrigin") = Me.ToString().Substring(4, Me.ToString().Substring(4).Length - 5) + ".aspx"
        Response.Redirect("PageTranslation.aspx")
        Response.End()
    End Sub

#Region "PANNELLO HELP"

    Private Sub LoadHelp()
        lblHelp.Text = "<p><strong>Marketing Average expenditures</strong><br>
                      <strong>It is the market average of the period</strong></p>
                    <p><strong>&nbsp;</strong></p>
                    <p><strong>Your Expenditures</strong><br>
                        <strong>It is your company expenditure for the period</strong></p>
                    <p><strong>&nbsp;</strong></p>
                    <p><strong>Ratio on revenues</strong><br>
                        <strong>It is the rate of customer service on revenues.</strong></p>
                  <p>&nbsp;</p>"
    End Sub

#End Region

End Class
