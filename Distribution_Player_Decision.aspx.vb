﻿Imports System.Data
Imports APS
Imports APPCore.Utility
Imports DALC4NET
Imports Telerik.Web.UI

Partial Class Distribuzione_Player_Decision
    Inherits System.Web.UI.Page

    Private m_Variable As List(Of String)

    Private m_Separator As String = "."

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster

    Private Sub EnableSave()
        If Nni(Session("IDPeriod")) = HandleGetMaxPeriodValid(Session("IDGame")) Then
            btnSave.Enabled = True
        Else
            If Session("IDRole") = "P" Then
                btnSave.Enabled = False

            ElseIf Session("IDRole") = "D" Then
                btnSave.Enabled = True
            End If
        End If
    End Sub

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        HandleLoadPlayerDecision()
        Message.Visible = False

        EnableSave()

        pnlMain.Update()
    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        HandleLoadPlayerDecision()
        Message.Visible = False
        pnlMain.Update()
    End Sub

    Private Sub Distribuzione_Player_Decision_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        m_SessionData = Session("SessionData")

        If IsNothing(m_SessionData) Then
            PageRedirect("~/Account/Login.aspx", "", "")
        End If

        m_SiteMaster = Me.Master

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oCBOPeriod As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler m_SiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler m_SiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(m_SessionData.Utente.Games.IDGame) ' Ultimo periodo
        oCBOPeriod.SelectedValue = Session("IDPeriodValid")
        m_SiteMaster.PeriodChange = Session("IDPeriodValid")

        Session("NameTeam") = GetTeamName(Nni(Session("IDTeam")))
        oCBOTeam.SelectedValue = Session("IDTeam")
        m_SiteMaster.TeamChange = Session("IDTeam")

        ' Aggiungo le variabili da gestire nella lista
        m_Variable = New List(Of String)
        m_Variable.Add("MarkeExpenRequi")
        m_Variable.Add("TrainServiRequi")
        m_Variable.Add("CentrStoreRequi")

        m_Variable.Add("PerifStoreRequi")

        m_Variable.Add("VehicRequi")
        m_Variable.Add("NewPersoStaff")
        m_Variable.Add("NewPersoPoint")

        m_Variable.Add("AddesTotalPerso")

        HandleLoadPlayerDecision()

        ' Gestione del pannello delle decisioni concesse
        modalAllow.OpenerElementID = btnAllow.ClientID
        modalAllow.Modal = False
        modalAllow.VisibleTitlebar = True

        ' Gestione del pannello delle decisioni concesse
        modalHelp.OpenerElementID = lnkHelp.ClientID
        modalHelp.Modal = False
        modalHelp.VisibleTitlebar = True

        LoadHelp()

        LoadDataDecisionsAllowed()
    End Sub

    Private Sub HandleLoadPlayerDecision()
        Dim bAggiungiHeader As Boolean = False

        Dim oDTDecisionsPlayer As DataTable
        Dim oDTDecisionsPlayerValue As DataTable

        Dim sHeader As String = ""

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Dim sNomeVariabile As String
        Dim sTraduzione As String
        Dim sTraduzioneExternalObject As String

        Dim bRigaVisibile As Boolean = False

        tblPlayerDecision.Controls.Clear()

        ' Carico la tabella delle intestazioni da usare
        oDTDecisionsPlayer = HandleLoadGameDecisionPlayer(m_SessionData.Utente.Games.IDGame, eVariablesCateogry.Distribution)

        ' Recupero le informazioni necessarie per costruirmi l'header della tabella HTML
        Dim oDTHeader As DataTable = oDTDecisionsPlayer

        ' Per ogni riga di intestazione inizio a costruire la tabella
        For Each oRowHeader As DataRow In oDTHeader.Rows
            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell
            If Nz(oRowHeader("VariableLabelGroup")) = "0" Or Nz(oRowHeader("VariableLabelGroup")) = "" Then
                Dim lblDecisionsPayer As New RadLabel
                lblDecisionsPayer.ID = "lblDecisionsPayer"
                lblDecisionsPayer.Text = "Decision player"
                lblDecisionsPayer.ForeColor = Drawing.Color.White
                tblHeaderCell.Controls.Add(lblDecisionsPayer)

                bAggiungiHeader = True
            Else
                If sHeader <> Nz(oRowHeader("VariableLabelGroup")) Then
                    Dim lblVariableLabelGroup As New RadLabel
                    lblVariableLabelGroup.ID = "lblVariableLabelGroup_" & Nz(oRowHeader("VariableLabelGroup"))
                    lblVariableLabelGroup.Text = Nz(oRowHeader("VariableLabelGroup"))
                    lblVariableLabelGroup.ForeColor = Drawing.Color.White
                    tblHeaderCell.Controls.Add(lblVariableLabelGroup)
                    sHeader = Nz(oRowHeader("VariableLabelGroup"))
                    bAggiungiHeader = True
                Else
                    bAggiungiHeader = False
                End If

            End If

            tblHeaderCell.ColumnSpan = 3
            tblHeaderCell.BorderStyle = BorderStyle.None

            tblHeaderRow.Cells.Add(tblHeaderCell)
            tblHeaderRow.BorderStyle = BorderStyle.None
            tblHeaderRow.Style.Add("margin-top", "10px")

            ' Carico i valori della variabile, e controllo eventualmente se è una variabile dipendente da qualche lista
            oDTDecisionsPlayerValue = HandleLoadGamePeriodDecisionPlayerValue(m_SiteMaster.PeriodGetCurrent, Nni(oRowHeader("ID")))

            ' Aggiungo la l'header solo se ho del testo di identificazione del gruppo
            If bAggiungiHeader Then
                tblPlayerDecision.Rows.Add(tblHeaderRow)
                bAggiungiHeader = False
            End If

            ' Ciclo sui valori delle varibili trovate
            For Each oRowVariableValue As DataRow In oDTDecisionsPlayerValue.Rows
                sTraduzione = GetVariableTranslation(Session("IDGame"), Nni(oRowVariableValue("IDVariable")), Nz(Session("LanguageActive")))
                If Nz(oRowVariableValue("VariableValue")).ToUpper = "[LIST OF ITEMS]" Then ' Lista di ITEMS/Product
                    ' Carico i valori presenti nella tabella Decisions_Players_value_List
                    Dim oDTValoriPLayer As DataTable = HandleGetValueDecisionPlayer(Nni(oRowVariableValue("ID")), Nni(Session("IDTeam")), m_SiteMaster.PeriodGetCurrent)

                    If oDTValoriPLayer.Rows.Count > 0 Then
                        For Each oROWValore As DataRow In oDTValoriPLayer.Rows
                            bRigaVisibile = False
                            ' Recupero la traduzione corretta dell'item 
                            sTraduzioneExternalObject = GetItemTranslation(Nni(oROWValore("IDItem")), Nz(Session("LanguageActive")))

                            sNomeVariabile = Nz(oROWValore("VariableName"))
                            ' Etichetta della variabile
                            tblRow = New TableRow
                            tblCell = New TableCell

                            ' Recupero la traduzione in lingua
                            tblCell.Text = sTraduzione & " - " & sTraduzioneExternalObject
                            tblCell.BorderStyle = BorderStyle.None
                            tblCell.Style.Add("width", "65%")
                            tblRow.Cells.Add(tblCell)

                            ' Creo i controlli necessari ed eventualmente li popolo
                            ' Carico i valori esistenti, altrimenti li creo
                            Dim oTxtBox As New TextBox
                            tblCell = New TableCell
                            oTxtBox.Text = Nn(oROWValore("Value")).ToString("N0")
                            oTxtBox.ID = Nz(sNomeVariabile)
                            oTxtBox.Attributes.Add("runat", "server")
                            oTxtBox.Style.Add("text-align", "right")
                            oTxtBox.ClientIDMode = ClientIDMode.Static
                            oTxtBox.EnableViewState = True
                            oTxtBox.TextMode = TextBoxMode.Number 
                            ' Controllo se la decisione deve essere abilitata in un determinato periodo oppure se devo solo disabilitarla
                            If Nni(oRowHeader("EnabledFromPeriod"), 0) > 0 Then
                                oTxtBox.Enabled = Nni(Session("IDPeriod")) >= (Nni(oRowHeader("EnabledFromPeriod"), 0))
                                bRigaVisibile = Nni(Session("IDPeriod")) >= (Nni(oRowHeader("EnabledFromPeriod"), 0))

                            Else
                                oTxtBox.Enabled = Nb(oRowHeader("Attiva"))
                                bRigaVisibile = Nb(oRowHeader("Attiva"))
                            End If

                            ' Aggiungo il textbox appena creato alla cella
                            tblCell.Controls.Add(oTxtBox)
                            tblCell.Style.Add("text-align", "right")
                            tblCell.BorderStyle = BorderStyle.None
                            tblCell.Style.Add("width", "35%")
                            tblRow.Cells.Add(tblCell)
                            If bRigaVisibile Then
                                tblPlayerDecision.Rows.Add(tblRow)
                            End If

                        Next

                    End If

                Else
                    Dim oDTValoriPlayer As DataTable = HandleLoadGamePeriodDecisionPlayerValue(m_SiteMaster.PeriodGetCurrent, Nni(oRowVariableValue("IDDecisionPlayer")))
                    For Each oRowData As DataRow In oDTValoriPlayer.Rows
                        bRigaVisibile = False
                        If Nni(oRowData("IDPlayer")) = Nni(Session("IDTeam")) Then
                            sNomeVariabile = "var_" & Nz(oRowData("IDPlayer") & "_" & Nz(oRowData("VariableName"))).Replace(" ", "")
                            ' Controllo la presenza di un eventuale textbox con lo stesso ID
                            ' Se presente esco, altrimenti lo creo correttamente
                            Dim oTXT As TextBox = TryCast(FindControlRecursive(Page, sNomeVariabile), TextBox)
                            If oTXT Is Nothing Then
                                ' Etichetta della variabile
                                tblRow = New TableRow
                                tblCell = New TableCell
                                tblCell.Text = sTraduzione
                                tblCell.BorderStyle = BorderStyle.None
                                tblCell.Style.Add("width", "65%")
                                tblRow.Cells.Add(tblCell)

                                ' Creo i controlli necessari ed eventualmente li popolo
                                ' Carico i valori esistenti, altrimenti li creo
                                Dim oTxtBox As New TextBox
                                tblCell = New TableCell

                                ' Recupero il valore secco della decisione del player nel periodo
                                Dim sValore As String = HandleLoadGamePeriodDecisionPlayerValueSingle(m_SiteMaster.PeriodGetCurrent, Nni(Session("IDTeam")), Nni(oRowData("IDDecisionPlayer")))
                                oTxtBox.Text = Nn(sValore).ToString("N0")
                                oTxtBox.ID = Nz(sNomeVariabile)
                                oTxtBox.Attributes.Add("runat", "server")
                                oTxtBox.Style.Add("text-align", "right")
                                oTxtBox.ClientIDMode = ClientIDMode.Static
                                oTxtBox.EnableViewState = True
                                oTxtBox.TextMode = TextBoxMode.Number
                                ' Controllo se la decisione deve essere abilitata in un determinato periodo oppure se devo solo disabilitarla
                                If Nni(oRowHeader("EnabledFromPeriod"), 0) > 0 Then
                                    oTxtBox.Enabled = Nni(Session("IDPeriod")) >= (Nni(oRowHeader("EnabledFromPeriod"), 0))
                                    bRigaVisibile = Nni(Session("IDPeriod")) >= (Nni(oRowHeader("EnabledFromPeriod"), 0))

                                Else
                                    oTxtBox.Enabled = Nb(oRowHeader("Attiva"))
                                    bRigaVisibile = Nb(oRowHeader("Attiva"))

                                End If

                                ' Aggiungo il textbox appena creato alla cella
                                tblCell.Controls.Add(oTxtBox)
                                tblCell.Style.Add("text-align", "right")
                                tblCell.BorderStyle = BorderStyle.None
                                tblCell.Style.Add("width", "35%")
                                tblRow.Cells.Add(tblCell)

                                If bRigaVisibile Then
                                    tblPlayerDecision.Rows.Add(tblRow)
                                End If
                            End If
                        End If
                    Next
                End If
            Next
            sHeader = Nz(oRowHeader("VariableLabelGroup"))
        Next

    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim oDTDecisionsPlayer As DataTable = HandleLoadGameDecisionPlayer(m_SessionData.Utente.Games.IDGame, eVariablesCateogry.Distribution)
        Dim sNomeTextBox As String
        Dim sNomeTeam As String = Nz(m_SiteMaster.TeamNameGet).Replace(" ", "")
        Dim sSQL As String
        Dim oDTDecisionPlayerValue As DataTable
        Dim oTXT As TextBox
        Dim oDAL As New DBHelper

        Message.Visible = False

        ' Controllo che effettivamente possa salvare e non sia scaduto il termine delle decisioni
        If Not IsPossibleToSaveDataPeriod(Session("IDGame"), Session("IDPeriod")) Then
            MessageText.Text = "Time over for this period"
            Message.Visible = True

            pnlMain.Update()

            btnSave.Enabled = False

            Exit Sub
        End If

        For Each oRow As DataRow In oDTDecisionsPlayer.Rows
            ' Controllo se la varibile recuperata è di tipo lista altrimenti passo alle variabili con inserimento valore secco
            sSQL = "SELECT VariableValue FROM Decisions_Players_Value WHERE IDDecisionPlayer = " & Nni(oRow("ID"))
            Dim sVariableType As String = Nz(oDAL.ExecuteScalar(sSQL))

            sSQL = "SELECT ID FROM Decisions_Players_Value WHERE IDDecisionPlayer = " & Nni(oRow("ID"))
            Dim iIDDecisionPlayerValue As Integer = Nni(oDAL.ExecuteScalar(sSQL))

            If sVariableType.ToUpper.Contains("LIST OF") Then
                ' Recupero i dati della variabile interessata per cercare anche il Textbox che contiene i valori da salvare
                sSQL = "SELECT * FROM Decisions_Players_Value_List WHERE IDDecisionValue =" & iIDDecisionPlayerValue & " AND IDPlayer = " & Session("IDTeam")
                Dim oDTDecisionsValueList As DataTable = oDAL.ExecuteDataTable(sSQL)

                For Each oRowValueList As DataRow In oDTDecisionsValueList.Rows
                    sNomeTextBox = Nz(oRowValueList("VariableName"))

                    oTXT = TryCast(FindControlRecursive(Page, sNomeTextBox), TextBox)
                    If oTXT IsNot Nothing AndAlso oTXT.Text <> "" Then
                        ' Vado a salvare i valori corretti nella tabella di riferimento
                        sSQL = "SELECT * FROM Decisions_Players_Value_List WHERE IDPlayer = " & Nni(Session("IDTeam")) _
                             & " And IDDecisionValue = " & iIDDecisionPlayerValue & " And VariableName = '" & Nz(oRowValueList("VariableName")) & "' "
                        oDTDecisionPlayerValue = oDAL.ExecuteDataTable(sSQL)
                        If oDTDecisionPlayerValue.Rows.Count > 0 Then ' Ci sono dati vado in update
                            sSQL = "UPDATE Decisions_Players_Value_List SET Value = '" & oTXT.Text & "' " _
                                 & "WHERE IDPlayer = " & Nni(Session("IDTeam")) & " AND IDDecisionValue = " & iIDDecisionPlayerValue & " " _
                                 & "AND IDPeriod = " & Nni(Session("IDPeriod")) & " AND VariableName = '" & Nz(oRowValueList("VariableName")) & "' "
                            oDAL.ExecuteNonQuery(sSQL)

                            If Nni(Session("IDTeamM&U")) > 0 Then
                                If Nni(Session("IDTeam")) = Nni(Session("IDTeamMaster")) Then
                                    sSQL = "UPDATE Decisions_Players_Value_List SET Value = '" & oTXT.Text & "' " _
                                         & "WHERE IDPlayer = " & Nni(Session("IDTeamM&U")) & " AND IDDecisionValue = " & iIDDecisionPlayerValue & " " _
                                         & "AND IDPeriod = " & Nni(Session("IDPeriod")) & " AND VariableName = '" & Nz(oRowValueList("VariableName")) & "' "
                                    oDAL.ExecuteNonQuery(sSQL)
                                End If
                            End If
                        End If
                    End If
                Next

            Else
                ' Recupero tutti i campi che non necessitano di items o oggetti esterni
                ' E' fallita la ricerca, provo a ricercare il textbox nelle variabili a uso singolo, senza gestione del list of
                sSQL = "SELECT * FROM Decisions_Players_Value WHERE IDDecisionPlayer = " & Nni(oRow("ID"))
                Dim oDTDataValue As DataTable = oDAL.ExecuteDataTable(sSQL)

                For Each oRowDataValue As DataRow In oDTDataValue.Rows
                    sNomeTextBox = "var_" & Nz(oRowDataValue("IDPlayer") & "_" & Nz(oRow("VariableName")))

                    oTXT = TryCast(FindControlRecursive(Page, sNomeTextBox), TextBox)
                    If oTXT IsNot Nothing AndAlso oTXT.Text <> "" Then
                        sSQL = "UPDATE Decisions_Players_Value SET VariableValue = '" & oTXT.Text & "' WHERE IDDecisionPlayer = " & Nni(oRow("ID")) & " AND  IDPlayer = " & Nni(Session("IDTeam")) _
                             & " AND ISNULL(IDPeriod, " & m_SiteMaster.PeriodGetCurrent & ") = " & Nni(Session("IDPeriod"))
                        oDAL.ExecuteNonQuery(sSQL)

                        If Nni(Session("IDTeamM&U")) > 0 Then
                            If Nni(Session("IDTeam")) = Nni(Session("IDTeamMaster")) Then
                                sSQL = "UPDATE Decisions_Players_Value SET VariableValue = '" & oTXT.Text & "' WHERE IDDecisionPlayer = " & Nni(oRow("ID")) & " AND  IDPlayer = " & Nni(Session("IDTeamM&U")) _
                                     & " AND ISNULL(IDPeriod, " & Nni(Session("IDPeriod")) & ") = " & Nni(Session("IDPeriod"))
                                oDAL.ExecuteNonQuery(sSQL)
                            End If
                        End If
                    End If
                Next

            End If

        Next

        SQLConnClose(oDAL.GetConnObject)

        oDAL = Nothing

        MessageText.Text = "Save completed"
        Message.Visible = True
    End Sub

#Region "Pannello del concesso"

    Private Sub LoadDataDecisionsAllowed()
        Try
            ' Rimuovo i controlli dal DIV di gestione della tabella Pivot
            If divTableMaster.Controls.Count > 0 Then
                For Each oControl As Control In divTableMaster.Controls
                    divTableMaster.Controls.Remove(oControl)
                Next
            End If

            'VENDITORI 

            ' NewPointAllow
            LoadPivotGrid("NewPointAllow", "NewPointAllow", "NewPointAllow", "Venditori concessi", m_SiteMaster.PeriodGetPrev)

            ' NewStaffAllow
            LoadPivotGrid("NewStaffAllow", "NewStaffAllow", "NewStaffAllow", "Staff concesso", m_SiteMaster.PeriodGetPrev)
            ' ****************************************************************************************************************

            ' FORMAZIONE

            ' AddesTotalAllow
            LoadPivotGrid("AddesTotalAllow", "AddesTotalAllow", "AddesTotalAllow", "Formazione concessa", m_SiteMaster.PeriodGetPrev)
            ' ****************************************************************************************************************

            ' SERVIZIO AL CLIENTE

            ' TrainServiAllow
            LoadPivotGrid("TrainServiAllow", "TrainServiAllow", "TrainServiAllow", "Customer service concesso", m_SiteMaster.PeriodGetPrev)

            ' ****************************************************************************************************************

            ' PROMOZIONI (FABRICS/FITTINGS/FURNITURE)

            ' MarkeExpenAllow
            LoadPivotGrid("MarkeExpenAllow", "MarkeExpenAllow", "MarkeExpenAllow", "Promozioni concesse (MarkeExpenAllow)", m_SiteMaster.PeriodGetPrev)

            ' TotalMarkeAllow
            LoadPivotGrid("TotalMarkeAllow", "TotalMarkeAllow", "TotalMarkeAllow", "Spese totali marketing e promozioni (TotalMarkeAllow)", m_SiteMaster.PeriodGetPrev)

            ' ****************************************************************************************************************

            ' NEGOZI CENTRALI (NUOVI / NON RINNOVATI)

            ' CentrStoreAllow
            LoadPivotGrid("CentrStoreAllow", "CentrStoreAllow", "CentrStoreAllow", "Nuovi negozi centrali in affitto", m_SiteMaster.PeriodGetPrev)

            ' CentrAlienAllow
            LoadPivotGrid("CentrAlienAllow", "CentrAlienAllow", "CentrAlienAllow", "Negozi centrali - affitti non rinnovati", m_SiteMaster.PeriodGetPrev)
            ' ****************************************************************************************************************

            ' NEGOZI PERIFERICI (NUOVI / NON RINNOVATI)

            ' PerifStoreAllow
            LoadPivotGrid("PerifStoreAllow", "PerifStoreAllow", "PerifStoreAllow", "Nuovi negozi periferici in affitto", m_SiteMaster.PeriodGetPrev)

            ' Affitti non rinnovati negozi periferici
            LoadPivotGrid("PerifScrapAllow", "PerifScrapAllow", "PerifScrapAllow", "Affitti non rinnovati - negozi perifer.", m_SiteMaster.PeriodGetPrev)
            ' ****************************************************************************************************************

            ' VEICOLI
            ' VehicAllow
            LoadPivotGrid("VehicAllow", "VehicAllow", "VehicAllow", "Veicoli concessi", m_SiteMaster.PeriodGetPrev)
            ' ****************************************************************************************************************

        Catch ex As Exception
            MessageText.Text = "Error: <br/> " & ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub LoadPivotGrid(NomeVariabile As String, RowField As String, IDPivotGrid As String, TitoloPivotGrid As String, IDPeriodo As Integer)
        Dim sSQL As String
        Dim oDAL As New DBHelper
        Dim oDTDataSourceGrid As DataTable
        Dim iPeriodo As Integer = IDPeriodo

        ' Carico Totale macchinari attivi
        sSQL = "SELECT VCD.VariableName, ISNULL(V.VariableLabel, '" & TitoloPivotGrid & "') AS " & RowField & ", VCV.IDPlayer, VCV.IDItem, VCV.IDAge, VCV.Value, 0.0 AS ValueDef, " _
             & "ISNULL(T.TeamName, '') AS TeamName, I.VariableLabel AS Item " _
             & "FROM Variables_Calculate_Define VCD " _
             & "LEFT JOIN Variables_Calculate_Value VCV ON VCD.id = VCV.IDVariable " _
             & "LEFT JOIN Variables V ON VCD.VariableName = V.VariableName AND VCD.IDGame = V.IDGame  " _
             & "LEFT JOIN BGOL_Teams T ON VCV.IDPlayer = T.Id " _
             & "LEFT JOIN vItems I ON VCV.IDItem = I.IDVariable " _
             & "WHERE VCD.IDGame = " & Nni(Session("IDGame")) & " AND VCD.VariableName = '" & NomeVariabile & "' AND VCV.IDPeriod = " & iPeriodo
        If Session("IDRole").Contains("P") Then
            sSQL &= "AND VCV.IDPlayer = " & Session("IDTeam")
        End If
        oDTDataSourceGrid = oDAL.ExecuteDataTable(sSQL)

        ' Converto i valori dei risultati da varchar a numerici con con decimali
        For Each oRow As DataRow In oDTDataSourceGrid.Rows
            If Nz(oRow("Value")) = "" Then
                oRow("Value") = "0.0"
            End If
            oRow("ValueDef") = Math.Round(Nn(oRow("Value"), "0"), 2)
        Next

        Dim oPivot As New Pivot(oDTDataSourceGrid)
        Dim dtPivot As New DataTable
        Select Case NomeVariabile
            Case "AddesTotalAllow", "CentrStoreAllow", "NewPointAllow", "NewStaffAllow", "PerifStoreAllow", "TrainServiAllow", "VehicAllow", "PerifScrapAllow",
                 "CentrAlienAllow", "TotalMarkeAllow"
                dtPivot = oPivot.PivotData("TeamName", "ValueDef", AggregateFunction.Sum, RowField)

            Case "MarkeExpenAllow"
                dtPivot = oPivot.PivotData("TeamName", "ValueDef", AggregateFunction.Sum, "Item")

            Case Else
                dtPivot = oPivot.PivotData(RowField, "ValueDef", AggregateFunction.Sum, RowField)

        End Select

        Dim oGridView As New GridView
        oGridView.ID = IDPivotGrid
        oGridView.CssClass = "tableAllow"
        AddHandler oGridView.RowCreated, AddressOf grdPivot_RowCreated
        AddHandler oGridView.RowDataBound, AddressOf grdPivot_RowDataBound
        oGridView.DataSource = dtPivot
        oGridView.DataBind()

        Dim oLabel As New Label
        oLabel.Text = TitoloPivotGrid
        divTableMaster.Controls.Add(oLabel)
        divTableMaster.Controls.Add(oGridView)

        Dim ControlRow As New Literal()
        ControlRow.Text = "<hr/> <br/>"

        divTableMaster.Controls.Add(ControlRow)

        oPivot = Nothing
    End Sub

    Protected Sub grdPivot_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.Header Then
            MergeHeader(DirectCast(sender, GridView), e.Row, 1)
        End If
    End Sub

    Protected Sub grdPivot_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            If e.Row.DataItemIndex >= 0 Then
                e.Row.Cells(0).BackColor = System.Drawing.ColorTranslator.FromHtml("#55A2DF")
                e.Row.Cells(0).ForeColor = System.Drawing.ColorTranslator.FromHtml("#fff")
            End If
        End If
    End Sub

    Private Sub MergeHeader(ByVal gv As GridView, ByVal row As GridViewRow, ByVal PivotLevel As Integer)
        Dim iCount As Integer = 1
        Dim iContaColonneHeader = 0

        For iCount = 1 To PivotLevel
            Dim oGridViewRow As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
            Dim Header = (row.Cells.Cast(Of TableCell)().[Select](Function(x) GetHeaderText(x.Text, iCount, PivotLevel))).GroupBy(Function(x) x)

            For Each v In Header
                Dim cell As New TableHeaderCell()
                If iContaColonneHeader > 0 Then
                    cell.Text = v.Key.Substring(v.Key.LastIndexOf(m_Separator) + 1)
                Else
                    cell.Text = ""
                End If

                cell.ColumnSpan = v.Count()

                oGridViewRow.Cells.Add(cell)
                iContaColonneHeader += 1
            Next
            gv.Controls(0).Controls.AddAt(row.RowIndex, oGridViewRow)
        Next
        row.Visible = False
    End Sub

    Private Function GetHeaderText(ByVal s As String, ByVal i As Integer, ByVal PivotLevel As Integer) As String
        If Not s.Contains(m_Separator) AndAlso i <> PivotLevel Then
            Return String.Empty
        Else
            Dim Index As Integer = NthIndexOf(s, m_Separator, i)
            If Index = -1 Then
                Return s
            End If
            Return s.Substring(0, Index)
        End If
    End Function

    Private Function NthIndexOf(ByVal str As String, ByVal SubString As String, ByVal n As Integer) As Integer
        Dim x As Integer = -1
        For i As Integer = 0 To n - 1
            x = str.IndexOf(SubString, x + 1)
            If x = -1 Then
                Return x
            End If
        Next
        Return x
    End Function

#End Region

    Private Sub Distribuzione_Player_Decision_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        If Not IsPostBack Then

            If Session("IDRole") = "P" Then m_SiteMaster.HandleReloadPeriods(HandleGetMaxPeriodValid(Session("IDGame")))

            btnTranslate.Visible = Session("IDRole").Contains("D")
            btnManageDecisions.Visible = Session("IDRole").Contains("D")

        End If

        ' Controllo tutti gli oggetti presenti
        Dim oTranslation As New TranslationObjectForm(Me, g_DAL, Nz(Session("LanguageActive")))

        EnableSave

    End Sub

    Private Sub btnTranslate_Click(sender As Object, e As EventArgs) Handles btnTranslate.Click
        Session("PageOrigin") = Me.ToString().Substring(4, Me.ToString().Substring(4).Length - 5) + ".aspx"
        Response.Redirect("PageTranslation.aspx")
        Response.End()
    End Sub

    Private Sub btnManageDecisions_Click(sender As Object, e As EventArgs) Handles btnManageDecisions.Click
        Session("PageOrigin") = Me.ToString().Substring(4, Me.ToString().Substring(4).Length - 5) + ".aspx"
        Response.Redirect("PageManageDecisions.aspx")
        Response.End()
    End Sub

#Region "PANNELLO HELP"

    Private Sub LoadHelp()
        lblHelp.Text = "<p><strong>Promotions</strong><br>
                      <strong>The amount you decide to spend on promotional  activities for each product in the period. </strong><br>
                      <strong>&nbsp;There is  an immediate and cumulative effect of all expenditures.</strong><br>
                      <strong>&nbsp;Relationship  to market size and market share is not directly proportional.</strong><br>
                      <strong>If you are unable to meet your orders, they may  be offered to competitors.</strong></p>
                    <p><strong>Customer Service</strong><br>
                        <strong>Investments to improve the level of customer  service in your retail outlets eg. more check-outs/creche/baby  changing-rooms/restaurants/parking etc. </strong><br>
                        <strong>These investments will increase the  attractiveness of your outlet(s). </strong></p>
                    <p><strong>Shops</strong><br>
                        <strong>Retail outlets may have a City Centre or  Out-of-Town Shopping Mall location</strong><br>
                        <strong>The level of sales is influenced by the number  of retail outlets, and their location. Generally, the more outlets the greater  your potential to increase sales.</strong><br>
                        <strong>City centre outlets improve company image.</strong><br>
                        <strong>You may change the number and location of your  outlets by investment or disinvestment. Newly acquired outlets come into  service immediately. &nbsp;There is a penalty  clause if you do not wish to renew the rental of an outlet, this being equal to  a quarter rental. (Put in a negative figure if you do not wish to renew!)</strong><br>
                        <strong>There is a charge per period for Rent and  Council Tax for each outlet. See The Journal.</strong></p>
                    <p><strong>Transports</strong><br>
                        <strong>Finished goods cannot be stored at the factory  so you are required to have sufficient transport each period to deliver your  products to your central warehouse. Goods for the wholesale market go directly  to the wholesaler who incurs the transport costs.</strong><br>
                        <strong>Vehicles may be bought at a cost of 80,000E.  Each vehicle has a capacity of 30,000 space units. Vehicles come on line in the  following period. Depreciation is at a rate of 1/12 per quarter on the original  value. There is no resale market for vehicles. There is a fixed charge of 0.3 &nbsp;per space unit for all goods delivered to the  central warehouse.</strong><br>
                        <strong>The different products have different space  requirements; fittings take up 1 space unit, fabrics 5 space units and  furniture 10 space units. Any combination of products is possible in the same  vehicle.</strong><br>
                        <strong>Emergency Hire </strong><br>
                        <strong>Where you have insufficient transport to  immediately move the products to your central warehouse, an external supply of  transport is automatically engaged at a hiring cost of 0.75 per space unit.</strong></p>
                    <p><strong>Human Resources</strong><br>
                        <strong>Employees in your retail outlets fall into 2  categories - Salespersons and Supervisory Staff. See The Journal for Salaries</strong><br>
                        <strong>The number you employ in each category can be  increased or decreased. </strong><br>
                        <strong>A positive figure = increasing by hiring</strong><br>
                        <strong>A negative figure = reduction in levels (which  cannot exceed 10% of current levels in any one period).&lt;br&gt;</strong><br>
                        <strong>To increase effectiveness you can employ more  persons, or train, or both.</strong><br>
                        <strong>To maintain the level of effectiveness of all  your employees you may invest in training to improve their effectiveness  leading indirectly to increased sales and profitability.</strong> </p>
                  <p>&nbsp;</p>"
    End Sub

#End Region

End Class
