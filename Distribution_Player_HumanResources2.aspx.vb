﻿Imports System.Data
Imports APS.APSUtility
Imports APPCore.Utility
Imports Telerik.Web.UI

Partial Class Distribution_Player_HumanResources2
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadDataPage()
        Message.Visible = False
        pnlMain.Update()
    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadDataPage()
        Message.Visible = False
        pnlMain.Update()
    End Sub

    Private Sub Distribution_Player_HumanResources2_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        m_SessionData = Session("SessionData")

        m_SiteMaster = Me.Master

        m_SiteMaster.LoadPeriodsValid = Session("IDRole") = "P"

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oCBOPeriod As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOPeriodValid As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriodValid"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler m_SiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler m_SiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(Session("IDGame")) ' Ultimo periodo
        oCBOPeriod.SelectedValue = Session("IDPeriodValid")
        m_SiteMaster.PeriodChange = Session("IDPeriodValid")

        Session("NameTeam") = GetTeamName(Session("IDTeam"))
        oCBOTeam.SelectedValue = Session("IDTeam")
        m_SiteMaster.TeamChange = Session("IDTeam")

        ' Gestione del pannello delle decisioni concesse
        modalPopup.OpenerElementID = lnkHelp.ClientID
        modalPopup.Modal = False
        modalPopup.VisibleTitlebar = True

    End Sub

    Private Sub Distribution_Player_HumanResources2_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        If Not Page.IsPostBack Then
            If Session("IDRole") = "P" Then
                m_SiteMaster.ReloadPeriodsCalculate()
            End If
            LoadDataPage()

            btnTranslate.Visible = Session("IDRole").Contains("D")

        End If
        ' Controllo tutti gli oggetti presenti
        Dim oTranslation As New TranslationObjectForm(Me, g_DAL, Nz(Session("LanguageActive")))
    End Sub

    Private Sub btnTranslate_Click(sender As Object, e As EventArgs) Handles btnTranslate.Click
        Session("PageOrigin") = Me.ToString().Substring(4, Me.ToString().Substring(4).Length - 5) + ".aspx"
        Response.Redirect("PageTranslation.aspx")
        Response.End()
    End Sub

    Private Sub LoadDataPage()
        tblHumanResources.Rows.Clear()
        LoadDataGridHR()

        LoadHelp()
    End Sub

    Private Sub LoadDataGridHR()
        Dim CurreSalesPeopl As Double = Nn(GetVariableDefineValue("CurreSalesPeopl", Session("IDTeam"), 0, 0, Session("IDPeriod"), Session("IDGame"))).ToString("N0")
        Dim CurreSuperStaff As Double = Nn(GetVariableDefineValue("CurreSuperStaff", Session("IDTeam"), 0, 0, Session("IDPeriod"), Session("IDGame"))).ToString("N0")
        Dim CurreOutleSubur As Double = Nn(GetVariableDefineValue("CurreOutleSubur", Session("IDTeam"), 0, 0, Session("IDPeriod"), Session("IDGame"))).ToString("N0")
        Dim CurreOutleCentr As Double = Nn(GetVariableDefineValue("CurreOutleCentr", Session("IDTeam"), 0, 0, Session("IDPeriod"), Session("IDGame"))).ToString("N0")

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Try
            If CurreSalesPeopl = 0 AndAlso CurreSuperStaff = 0 AndAlso CurreOutleCentr = 0 Then
                lblTitolo.Text &= "<br/>" & "<br/>" & "No data available for this period"
                grfRatioSalaries.Visible = False
                Exit Try
            Else
                grfRatioSalaries.Visible = True
                lblTitolo.Text = "Human resources"
            End If

            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header vuota
            tblHeaderCell.Text = ""
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.Style.Add("width", "20%")
            tblHeaderCell.Style.Add("background", "transparent")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            ' Cella No.
            tblHeaderCell = New TableHeaderCell
            Dim lblNo As New RadLabel
            lblNo.ID = "oLBLNo"
            lblNo.Text = "No."
            lblNo.ForeColor = Drawing.Color.White
            tblHeaderCell.Controls.Add(lblNo)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.Style.Add("width", "20%")
            tblHeaderCell.Style.Add("background", "#525252")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            ' Average No. per store
            tblHeaderCell = New TableHeaderCell
            Dim lblNoStore As New RadLabel
            lblNoStore.ID = "lblNoStore"
            lblNoStore.Text = "Average No. per store"
            lblNoStore.ForeColor = Drawing.Color.White
            tblHeaderCell.Controls.Add(lblNoStore)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.Style.Add("width", "20%")
            tblHeaderCell.Style.Add("background", "#525252")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            ' Effectiveness
            tblHeaderCell = New TableHeaderCell
            Dim lblEffectiveness As New RadLabel
            lblEffectiveness.ID = "lblEffectiveness "
            lblEffectiveness.Text = "Effectiveness"
            lblEffectiveness.ForeColor = Drawing.Color.White
            tblHeaderCell.Controls.Add(lblEffectiveness)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.Style.Add("width", "20%")
            tblHeaderCell.Style.Add("background", "#525252")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            ' Average No. per store
            tblHeaderCell = New TableHeaderCell
            Dim lblLevelTraining As New RadLabel
            lblLevelTraining.ID = "lblLevelTraining"
            lblLevelTraining.Text = "Level of training"
            lblLevelTraining.ForeColor = Drawing.Color.White
            tblHeaderCell.Controls.Add(lblLevelTraining)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.Style.Add("width", "20%")
            tblHeaderCell.Style.Add("background", "#525252")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            tblHumanResources.Rows.Add(tblHeaderRow)

            ' RIGHE DI DETTAGLIO
            ' Staff
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblStaff As New RadLabel
            lblStaff.ID = "lblStaff"
            lblStaff.Text = "Staff"
            lblStaff.ForeColor = Drawing.Color.DarkRed
            tblCell.Controls.Add(lblStaff)
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell

            Dim lnkCurreSuperStaff As New HyperLink
            lnkCurreSuperStaff.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=CurreSuperStaff&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            lnkCurreSuperStaff.Style.Add("text-decoration", "none")
            lnkCurreSuperStaff.Style.Add("cursor", "pointer")

            lnkCurreSuperStaff.Text = Nn(GetVariableDefineValue("CurreSuperStaff", Session("IDTeam"), 0, 0, Session("IDPeriod"), Session("IDGame"))).ToString("N0")
            lnkCurreSuperStaff.Style.Add("font-size", "9pt")
            lnkCurreSuperStaff.Style.Add("text-align", "left")
            lnkCurreSuperStaff.Style.Add("font-weight", "normal")
            tblCell.Controls.Add(lnkCurreSuperStaff)

            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim lblCurreSuperStaff As New RadLabel
            lblCurreSuperStaff.Text = (CurreSuperStaff / (CurreOutleSubur + CurreOutleCentr)).ToString("N1")
            lblCurreSuperStaff.Style.Add("font-size", "9pt")
            lblCurreSuperStaff.Style.Add("text-align", "left")
            lblCurreSuperStaff.Style.Add("font-weight", "normal")
            tblCell.Controls.Add(lblCurreSuperStaff)
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell

            Dim lnkTableEffecStaff As New HyperLink
            lnkTableEffecStaff.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=TableEffecStaff&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            lnkTableEffecStaff.Style.Add("text-decoration", "none")
            lnkTableEffecStaff.Style.Add("cursor", "pointer")

            lnkTableEffecStaff.Text = Nn(GetVariableDefineValue("TableEffecStaff", Session("IDTeam"), 0, 0, Session("IDPeriod"), Session("IDGame"))).ToString("N2") & " %"

            lnkTableEffecStaff.Style.Add("font-size", "9pt")
            lnkTableEffecStaff.Style.Add("text-align", "left")
            lnkTableEffecStaff.Style.Add("font-weight", "normal")

            tblCell.Controls.Add(lnkTableEffecStaff)

            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell

            Dim lnkStaffTrainLevel As New HyperLink
            lnkStaffTrainLevel.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=StaffTrainLevel&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            lnkStaffTrainLevel.Style.Add("text-decoration", "none")
            lnkStaffTrainLevel.Style.Add("cursor", "pointer")

            lnkStaffTrainLevel.Text = Nn(GetVariableState("StaffTrainLevel", Session("IDTeam"), Session("IDPeriod"), "", Session("IDGame"))).ToString("N2") & " %"

            lnkStaffTrainLevel.Style.Add("font-size", "9pt")
            lnkStaffTrainLevel.Style.Add("text-align", "left")
            lnkStaffTrainLevel.Style.Add("font-weight", "normal")

            tblCell.Controls.Add(lnkStaffTrainLevel)

            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblHumanResources.Rows.Add(tblRow)

            '  Sales persons
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblSalesPersons As New RadLabel
            lblSalesPersons.ID = "lblSalesPersons"
            lblSalesPersons.Text = "Sales persons"
            lblSalesPersons.ForeColor = Drawing.Color.DarkRed
            tblCell.Controls.Add(lblSalesPersons)
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell

            Dim lnkCurreSalesPeopl As New HyperLink
            lnkCurreSalesPeopl.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=CurreSalesPeopl&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            lnkCurreSalesPeopl.Style.Add("text-decoration", "none")
            lnkCurreSalesPeopl.Style.Add("cursor", "pointer")

            lnkCurreSalesPeopl.Text = Nn(GetVariableDefineValue("CurreSalesPeopl", Session("IDTeam"), 0, 0, Session("IDPeriod"), Session("IDGame"))).ToString("N0")
            lnkCurreSalesPeopl.Style.Add("font-size", "9pt")
            lnkCurreSalesPeopl.Style.Add("text-align", "left")
            lnkCurreSalesPeopl.Style.Add("font-weight", "normal")
            tblCell.Controls.Add(lnkCurreSalesPeopl)

            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim lblSalesPersonAverage As New RadLabel
            lblSalesPersonAverage.Text = (CurreSalesPeopl / (CurreOutleSubur + CurreOutleCentr)).ToString("N1")
            lblSalesPersonAverage.Style.Add("font-size", "9pt")
            lblSalesPersonAverage.Style.Add("text-align", "left")
            lblSalesPersonAverage.Style.Add("font-weight", "normal")
            tblCell.Controls.Add(lblSalesPersonAverage)
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell

            Dim lnkTableEffecPoint As New HyperLink
            lnkTableEffecPoint.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=TableEffecPoint&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            lnkTableEffecPoint.Style.Add("text-decoration", "none")
            lnkTableEffecPoint.Style.Add("cursor", "pointer")

            lnkTableEffecPoint.Text = Nn(GetVariableDefineValue("TableEffecPoint", Session("IDTeam"), 0, 0, Session("IDPeriod"), Session("IDGame"))).ToString("N2") & " %"
            lnkTableEffecPoint.Style.Add("font-size", "9pt")
            lnkTableEffecPoint.Style.Add("text-align", "left")
            lnkTableEffecPoint.Style.Add("font-weight", "normal")

            tblCell.Controls.Add(lnkTableEffecPoint)

            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim lnkPointTrainLevel As New HyperLink
            lnkPointTrainLevel.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=PointTrainLevel&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            lnkPointTrainLevel.Style.Add("text-decoration", "none")
            lnkPointTrainLevel.Style.Add("cursor", "pointer")

            lnkPointTrainLevel.Text = Nn(GetVariableState("PointTrainLevel", Session("IDTeam"), Session("IDPeriod"), "", Session("IDGame"))).ToString("N2") & " %"
            lnkPointTrainLevel.Style.Add("font-size", "9pt")
            lnkPointTrainLevel.Style.Add("text-align", "left")
            lnkPointTrainLevel.Style.Add("font-weight", "normal")

            tblCell.Controls.Add(lnkPointTrainLevel)

            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblHumanResources.Rows.Add(tblRow)

            ' Caricamento del grafico
            LoadDataGraph()

        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub LoadDataGraph()
        Dim sSQL As String
        Dim oDTTableGraph As New DataTable
        Dim oDTTableResult As DataTable
        Dim dRatioPersoReven As Double
        Dim sPeriod As String

        ' Preparo il datatable che ospiterà i dati
        oDTTableGraph.Columns.Add("RatioPersoReven", GetType(Double))
        oDTTableGraph.Columns.Add("Period", GetType(String))

        sSQL = "SELECT P.Descrizione AS Period, C.Value " _
             & "FROM Variables_Calculate_Define V " _
             & "INNER JOIN Variables_Calculate_Value C ON V.Id = C.IDVariable " _
             & "INNER JOIN BGOL_Periods P ON C.IDPeriod = P.Id " _
             & "WHERE LTRIM(RTRIM(V.VariableName)) = 'RatioPersoReven' AND C.IDPlayer = " & Nni(Session("IDTeam")) _
             & " AND C.IDPeriod <= " & m_SiteMaster.PeriodGetCurrent & " AND V.IDGame = " & Session("IDGame")
        If Session("IDRole") = "P" Then
            sSQL &= " AND ISNULL(C.Simulation, 0) = 0 "
        End If
        oDTTableResult = g_DAL.ExecuteDataTable(sSQL)

        For Each oRow As DataRow In oDTTableResult.Rows
            dRatioPersoReven = Nn(oRow("Value"))
            sPeriod = Nz(oRow("Period"))

            oDTTableGraph.Rows.Add(dRatioPersoReven.ToString("N2"), sPeriod)
        Next

        grfRatioSalaries.DataSource = oDTTableGraph
        grfRatioSalaries.DataBind()

        grfRatioSalaries.PlotArea.XAxis.DataLabelsField = "Period"
        grfRatioSalaries.PlotArea.XAxis.LabelsAppearance.TextStyle.Bold = False
        grfRatioSalaries.PlotArea.XAxis.LabelsAppearance.TextStyle.FontSize = 9
        grfRatioSalaries.PlotArea.XAxis.EnableBaseUnitStepAuto = True
    End Sub


#Region "PANNELLO HELP"

    Private Sub LoadHelp()
        lblHelp.Text = "<p><strong>Staff</strong><br>
                      <strong>Here you can check the total number of staff  employed, their training level and their effectiveness. The effectiveness  affects your capacity to reach the 90% of maximum sales on total goods  available. To increase the effectiveness you may increase the number of  employees or their training level. The effectiveness is related to the quantity  of outlets (and location), the total amount of goods to sell and the ability of  the staff to manage the sales persons in the outlets. </strong></p>
                    <p><strong>&nbsp;</strong></p>
                    <p><strong>Sales persons</strong><br>
                        <strong>Here you can check the total number of sales  people employed, their training level and their effectiveness. To increase the  effectiveness you may increase the number of employees or their training level.</strong> </p>
                  <p>&nbsp;</p>"
    End Sub

#End Region

End Class
