﻿Imports System.Data
Imports APS.APSUtility
Imports APPCore.Utility
Imports DALC4NET
Imports Telerik.Web.UI

Partial Class Distribution_Player_Promotions
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster
    Private m_IDPeriodoMax As Integer

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        Message.Visible = False
        LoadData()
        UpdatePanelMain.Update()
    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        Message.Visible = False
        LoadData()
        UpdatePanelMain.Update()
    End Sub

    Private Sub Distribution_Player_Sales2_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        m_SessionData = Session("SessionData")

        m_SiteMaster = Me.Master

        m_SiteMaster.LoadPeriodsValid = Session("IDRole") = "P"

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oCBOPeriod As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOPeriodValid As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriodValid"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler m_SiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler m_SiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(Session("IDGame")) ' Ultimo periodo
        oCBOPeriod.SelectedValue = Session("IDPeriodValid")
        m_SiteMaster.PeriodChange = Session("IDPeriodValid")

        Session("NameTeam") = GetTeamName(Session("IDTeam"))
        oCBOTeam.SelectedValue = Session("IDTeam")
        m_SiteMaster.TeamChange = Session("IDTeam")

        ' Gestione del pannello delle decisioni concesse
        modalPopup.OpenerElementID = lnkHelp.ClientID
        modalPopup.Modal = False
        modalPopup.VisibleTitlebar = True

    End Sub

    Private Sub LoadData()
        tblPromotions.Rows.Clear()

        LoadPromotions()
        LoadDataGraph()

        LoadHelp()
    End Sub

    Private Sub LoadPromotions()
        Dim oDTItems As DataTable = HandleGetItemsPeriodVariables(Session("IDGame"), Nz(Session("LanguageActive")))

        Dim iTotNumPlayers As Integer = LoadTeamsGame(Session("IDGame")).Rows.Count

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Try
            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header vuota
            tblHeaderCell.Text = ""
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderRow.Cells.Add(tblHeaderCell)

            ' Recupero gli Items per la costruzione delle colonne
            For Each oRowItm As DataRow In oDTItems.Rows
                ' Cella del prezzo di vendita
                tblHeaderCell = New TableHeaderCell
                tblHeaderCell.Text = Nz(oRowItm("VariableName"))
                tblHeaderCell.BorderStyle = BorderStyle.None
                tblHeaderRow.Cells.Add(tblHeaderCell)
            Next

            tblHeaderRow.BorderStyle = BorderStyle.None
            tblHeaderRow.Style.Add("margin-top", "10px")

            tblPromotions.Rows.Add(tblHeaderRow)

            ' Aggiungo le righe di dettaglio
            ' Riga Market average expenditure
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLBLMarketAverageExpenditure As New RadLabel
            oLBLMarketAverageExpenditure.ID = "lblMarketAverageExpenditure"
            oLBLMarketAverageExpenditure.Text = "Market average expenditure"
            oLBLMarketAverageExpenditure.Style.Add("color", "darkred")
            tblCell.Controls.Add(oLBLMarketAverageExpenditure)
            tblCell.Style.Add("width", "40%")
            tblRow.Cells.Add(tblCell)

            For Each oRowItm As DataRow In oDTItems.Rows
                tblCell = New TableCell
                tblCell.Text = (CalculateSumItem(oRowItm("IDVariable")) / iTotNumPlayers).ToString("N0")
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)
            Next
            tblPromotions.Rows.Add(tblRow)

            ' Riga Expenditure Team
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLBLExpenditure As New RadLabel
            oLBLExpenditure.ID = "lblExpenditure"
            oLBLExpenditure.Text = "Expenditure of team"
            oLBLExpenditure.Style.Add("color", "darkred")
            tblCell.Controls.Add(oLBLExpenditure)
            tblCell.Style.Add("width", "20%")
            tblRow.Cells.Add(tblCell)

            For Each oRowItm As DataRow In oDTItems.Rows
                tblCell = New TableCell

                Dim lnkMarkeExpenAllow As New HyperLink
                lnkMarkeExpenAllow.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=MarkeExpenAllow&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                lnkMarkeExpenAllow.Style.Add("text-decoration", "none")
                lnkMarkeExpenAllow.Style.Add("cursor", "pointer")

                lnkMarkeExpenAllow.Text = Nn(GetVariableDefineValue("MarkeExpenAllow", Session("IDTeam"), oRowItm("IDVariable"), 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")

                tblCell.Controls.Add(lnkMarkeExpenAllow)

                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)
            Next
            tblPromotions.Rows.Add(tblRow)

            ' Riga del Ratio on revenues
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLBLRatioOnRevenues As New RadLabel
            oLBLRatioOnRevenues.ID = "lblRatioOnRevenues"
            oLBLRatioOnRevenues.Text = "Ratio on revenues"
            oLBLRatioOnRevenues.Style.Add("color", "darkred")
            tblCell.Controls.Add(oLBLRatioOnRevenues)
            tblCell.Style.Add("width", "20%")
            tblRow.Cells.Add(tblCell)

            For Each oRowItm As DataRow In oDTItems.Rows
                Dim dTotalRevenues As Double = Nn(GetVariableDefineValue("ProduRevenMarke", Session("IDTeam"), oRowItm("IDVariable"), 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame")))
                tblCell = New TableCell
                Dim dValore As Double = Nn(GetVariableDefineValue("MarkeExpenAllow", Session("IDTeam"), oRowItm("IDVariable"), 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame")))
                If dTotalRevenues > 0 Then
                    tblCell.Text = ((dValore / dTotalRevenues) * 100).ToString("N2")
                Else
                    tblCell.Text = 0.ToString("N0")
                End If
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)
            Next
            tblPromotions.Rows.Add(tblRow)

        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Function CalculateSumItem(IDItem As Integer) As Double
        Dim oDTPlayers As DataTable = LoadTeamsGame(Session("IDGame"))
        Dim dRet As Double = 0

        For Each oRowPlayer As DataRow In oDTPlayers.Rows
            dRet += Nn(GetVariableDefineValue("MarkeExpenAllow", oRowPlayer("ID"), IDItem, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame")))
        Next

        Return dRet
    End Function

    Private Function CalculateSumItem(IDItem As Integer, IDPeriod As Integer) As Double
        Dim oDTPlayers As DataTable = LoadTeamsGame(Session("IDGame"))
        Dim dRet As Double = 0

        For Each oRowPlayer As DataRow In oDTPlayers.Rows
            dRet += Nn(GetVariableDefineValue("MarkeExpenAllow", oRowPlayer("ID"), IDItem, 0, IDPeriod, Session("IDGame")))
        Next

        Return dRet
    End Function

    Private Sub LoadDataGraph()
        Dim sSQL As String
        Dim oDTItems As DataTable = HandleGetItemsPeriodVariables(Session("IDGame"), Nz(Session("LanguageActive")))
        Dim sItemName As String
        Dim dValue As Double = 0
        Dim iTotNumPlayers As Integer = LoadTeamsGame(Session("IDGame")).Rows.Count

        grfMarketInvestments.PlotArea.Series.Clear()
        grfMarketInvestments.PlotArea.XAxis.Items.Clear()

        ' Recupero tutti i periodi generati fino adesso
        sSQL = "SELECT * FROM BGOL_Games_Periods BGP " _
             & "INNER JOIN BGOL_Periods BP ON BGP.IDPeriodo = BP.Id "
        If Session("StepToPlay") = Session("CurrentStep") Then
            sSQL &= "WHERE BGP.IDGame = " & Session("IDGame") & " AND BP.NumStep < " & Session("CurrentStep")
        Else
            sSQL &= "WHERE BGP.IDGame = " & Session("IDGame") & " AND BP.NumStep <= " & Session("CurrentStep")
        End If
        sSQL &= " ORDER BY BP.NumStep "
        Dim oDTPeriodi As DataTable = g_DAL.ExecuteDataTable(sSQL)

        Dim oSeriesData As LineSeries
        For Each oRowItm As DataRow In oDTItems.Rows
            sItemName = oRowItm("VariableName").ToString

            oSeriesData = New LineSeries
            For Each oRowPeriod As DataRow In oDTPeriodi.Rows

                dValue = CalculateSumItem(oRowItm("IDVariable"), oRowPeriod("IdPeriodo"))
                dValue = dValue / iTotNumPlayers

                If Not IsNothing(dValue) Then
                    Dim oItem As New SeriesItem
                    oItem.YValue = dValue.ToString("N2")
                    oItem.TooltipValue = Nz(oRowItm("VariableName"))
                    oSeriesData.Items.Add(oItem)
                End If
            Next
            grfMarketInvestments.PlotArea.Series.Add(oSeriesData)
            oSeriesData.Name = sItemName
            oSeriesData.VisibleInLegend = True
            oSeriesData.LabelsAppearance.Visible = True
        Next

        For Each oRowPeriod As DataRow In oDTPeriodi.Rows
            Dim newAxisItem As New AxisItem(Nz(oRowPeriod("Descrizione")))
            grfMarketInvestments.PlotArea.XAxis.Items.Add(newAxisItem)
        Next

        grfMarketInvestments.PlotArea.XAxis.MinorGridLines.Visible = False
        grfMarketInvestments.PlotArea.YAxis.MinorGridLines.Visible = False
        grfMarketInvestments.Legend.Appearance.Visible = True
        grfMarketInvestments.Legend.Appearance.Position = HtmlChart.ChartLegendPosition.Top

    End Sub

    Private Sub Distribution_Player_Promotions_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        If Not Page.IsPostBack Then
            If Session("IDRole") = "P" Then
                m_SiteMaster.ReloadPeriodsCalculate()
            End If
            LoadData()

            btnTranslate.Visible = Session("IDRole").Contains("D")
            ' Controllo tutti gli oggetti presenti
            Dim oTranslation As New TranslationObjectForm(Me, g_DAL, Nz(Session("LanguageActive")))

        End If
    End Sub

    Private Sub btnTranslate_Click(sender As Object, e As EventArgs) Handles btnTranslate.Click
        Session("PageOrigin") = Me.ToString().Substring(4, Me.ToString().Substring(4).Length - 5) + ".aspx"
        Response.Redirect("PageTranslation.aspx")
        Response.End()
    End Sub

#Region "PANNELLO HELP"

    Private Sub LoadHelp()
        lblHelp.Text = "<p><strong>Promotion</strong><br>
                      <strong>This is the promotion budget of the company per  product</strong></p>
                    <p><strong>Market average expenditure</strong><br>
                        <strong>It is the market average expenditure in the  period</strong></p>
                    <p><strong>Expenditure </strong><br>
                        <strong>&nbsp;It is  your company expenditure for the period</strong></p>
                    <p><strong>Ratio on revenues</strong><br>
                        <strong>It is the percentage of promotion on the  revenues of the period</strong></p>
                  <p>&nbsp;</p>"
    End Sub

#End Region

End Class
