﻿Imports System.Data
Imports APS.APSUtility
Imports APPCore.Utility
Imports DALC4NET
Imports Telerik.Web.UI

Partial Class Distribution_Player_Sales2
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster
    Private m_IDPeriodoMax As Integer

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        Message.Visible = False
        LoadData()
        UpdatePanelMain.Update()
    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        Message.Visible = False
        LoadData()
        UpdatePanelMain.Update()
    End Sub

    Private Sub Distribution_Player_Sales2_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        m_SessionData = Session("SessionData")

        m_SiteMaster = Me.Master

        m_SiteMaster.LoadPeriodsValid = Session("IDRole") = "P"

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oCBOPeriod As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOPeriodValid As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriodValid"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler m_SiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler m_SiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(Session("IDGame")) ' Ultimo periodo
        oCBOPeriod.SelectedValue = Session("IDPeriodValid")
        m_SiteMaster.PeriodChange = Session("IDPeriodValid")

        Session("NameTeam") = GetTeamName(Session("IDTeam"))
        oCBOTeam.SelectedValue = Session("IDTeam")
        m_SiteMaster.TeamChange = Session("IDTeam")

        ' Gestione del pannello delle decisioni concesse
        modalPopup.OpenerElementID = lnkHelp.ClientID
        modalPopup.Modal = False
        modalPopup.VisibleTitlebar = True

        LoadHelp()
    End Sub

    Private Sub LoadData()
        tblGoodsAvailable.Rows.Clear()
        tblRetailSales.Rows.Clear()

        LoadDataTableGoodsAvailable()
        LoadDataTableRetailSales()
    End Sub

    Private Sub LoadDataTableGoodsAvailable()
        Dim oDTItems As DataTable = HandleGetItemsPeriodVariables(Session("IDGame"), Nz(Session("LanguageActive")))

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Try
            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header vuota
            tblHeaderCell.Text = ""
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderRow.Cells.Add(tblHeaderCell)

            ' Recupero gli Items per la costruzione delle colonne
            For Each oRowItm As DataRow In oDTItems.Rows
                ' Cella del prezzo di vendita
                tblHeaderCell = New TableHeaderCell
                tblHeaderCell.Text = Nz(oRowItm("VariableName"))
                tblHeaderCell.BorderStyle = BorderStyle.None
                tblHeaderCell.ColumnSpan = 2
                tblHeaderRow.Cells.Add(tblHeaderCell)
            Next

            tblHeaderRow.BorderStyle = BorderStyle.None
            tblHeaderRow.Style.Add("margin-top", "10px")

            tblGoodsAvailable.Rows.Add(tblHeaderRow)

            ' Controllo la presenza delle variabili nella tabella delle variabili del gioco
            VerifiyVariableExists("StockFinisGoods", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)
            VerifiyVariableExists("QualiStockFinis", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)

            VerifiyVariableExists("ProduReque", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)
            VerifiyVariableExists("ProduTotalQuali", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)

            VerifiyVariableExists("SalesInterMarke", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)

            VerifiyVariableExists("ExtraAllow", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)
            VerifiyVariableExists("ExtraProduQuali", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)

            VerifiyVariableExists("TotalOfferCusto", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)
            VerifiyVariableExists("QualiGoodsOffer", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)

            ' Aggiungo le righe di dettaglio
            ' Riga del beginning stock
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblBeginningStock As New RadLabel
            lblBeginningStock.ID = "lblBeginningStock"
            lblBeginningStock.Text = "Beginning stock"
            tblCell.Controls.Add(lblBeginningStock)
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)
            For Each oRowItm As DataRow In oDTItems.Rows
                Dim sNomeVariabile As String = "var_" & m_SiteMaster.TeamNameGet & "_StockFinisGoods_" & oRowItm("VariableName")
                tblCell = New TableCell
                ' Creazione dell'oggetto linked server per rimandare alla pagina dei grafici
                Dim oLinkStockFinisGoods As New HyperLink
                oLinkStockFinisGoods.ID = sNomeVariabile
                oLinkStockFinisGoods.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=StockFinisGoods&II=" & oRowItm("IDVariable") & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                oLinkStockFinisGoods.Style.Add("text-decoration", "none")
                oLinkStockFinisGoods.Style.Add("cursor", "pointer")

                oLinkStockFinisGoods.Text = Nn(GetVariableState("StockFinisGoods", m_SiteMaster.TeamIDGet, m_SiteMaster.PeriodGetCurrent, sNomeVariabile, Session("IDGame"), 0, oRowItm("IDVariable"))).ToString("N0")
                tblCell.Controls.Add(oLinkStockFinisGoods)
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)

                sNomeVariabile = "var_" & m_SiteMaster.TeamNameGet & "_QualiStockFinis_" & oRowItm("VariableName")
                tblCell = New TableCell

                Dim oLinkQualiStockFinis As New HyperLink
                oLinkQualiStockFinis.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=QualiStockFinis&II=" & oRowItm("IDVariable") & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                oLinkQualiStockFinis.Style.Add("text-decoration", "none")
                oLinkQualiStockFinis.Style.Add("cursor", "pointer")

                oLinkQualiStockFinis.Text = Nn(GetVariableState("QualiStockFinis", m_SiteMaster.TeamIDGet, m_SiteMaster.PeriodGetCurrent, sNomeVariabile, Session("IDGame"), 0, oRowItm("IDVariable"))).ToString("N2")

                tblCell.Controls.Add(oLinkQualiStockFinis)

                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)
            Next
            tblGoodsAvailable.Rows.Add(tblRow)

            ' Riga del finished goods
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblProduction As New RadLabel
            lblProduction.ID = "lblProduction"
            lblProduction.Text = "Production"
            tblCell.Controls.Add(lblProduction)
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)
            For Each oRowItm As DataRow In oDTItems.Rows
                tblCell = New TableCell
                Dim oLinkProduReque As New HyperLink
                oLinkProduReque.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=ProduReque&II=" & oRowItm("IDVariable") & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                oLinkProduReque.Style.Add("text-decoration", "none")
                oLinkProduReque.Style.Add("cursor", "pointer")

                oLinkProduReque.Text = Nn(GetVariableDefineValue("ProduReque", m_SiteMaster.TeamIDGet, oRowItm("IDVariable"), 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N0")
                tblCell.Controls.Add(oLinkProduReque)
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                Dim oLinkProduTotalQuali As New HyperLink
                oLinkProduTotalQuali.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=ProduTotalQuali&II=" & oRowItm("IDVariable") & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                oLinkProduTotalQuali.Style.Add("text-decoration", "none")
                oLinkProduTotalQuali.Style.Add("cursor", "pointer")

                oLinkProduTotalQuali.Text = Nn(GetVariableDefineValue("ProduTotalQuali", m_SiteMaster.TeamIDGet, oRowItm("IDVariable"), 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")

                tblCell.Controls.Add(oLinkProduTotalQuali)

                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)
            Next
            tblGoodsAvailable.Rows.Add(tblRow)

            ' Riga del Sales to Wholesalers
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblSalesToWholesalers As New RadLabel
            lblSalesToWholesalers.ID = "lblSalesToWholesalers"
            lblSalesToWholesalers.Text = "Sales to Wholesalers"
            tblCell.Controls.Add(lblSalesToWholesalers)
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)
            For Each oRowItm As DataRow In oDTItems.Rows
                tblCell = New TableCell
                Dim oLinkSalesInterMarke As New HyperLink
                oLinkSalesInterMarke.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=SalesInterMarke&II=" & oRowItm("IDVariable") & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                oLinkSalesInterMarke.Style.Add("text-decoration", "none")
                oLinkSalesInterMarke.Style.Add("cursor", "pointer")

                oLinkSalesInterMarke.Text = Nn(GetVariableDefineValue("SalesInterMarke", m_SiteMaster.TeamIDGet, oRowItm("IDVariable"), 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N0")
                tblCell.Controls.Add(oLinkSalesInterMarke)
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                Dim oLinkProduTotalQuali As New HyperLink
                oLinkProduTotalQuali.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=ProduTotalQuali&II=" & oRowItm("IDVariable") & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                oLinkProduTotalQuali.Style.Add("text-decoration", "none")
                oLinkProduTotalQuali.Style.Add("cursor", "pointer")

                oLinkProduTotalQuali.Text = Nn(GetVariableDefineValue("ProduTotalQuali", m_SiteMaster.TeamIDGet, oRowItm("IDVariable"), 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")

                tblCell.Controls.Add(oLinkProduTotalQuali)

                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)
            Next
            tblGoodsAvailable.Rows.Add(tblRow)

            ' Riga del  Purchase of finished goods
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblPurchaseFinishedGoods As New RadLabel
            lblPurchaseFinishedGoods.ID = "lblPurchaseFinishedGoods"
            lblPurchaseFinishedGoods.Text = "Purchase of finished goods"
            tblCell.Controls.Add(lblPurchaseFinishedGoods)
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)
            For Each oRowItm As DataRow In oDTItems.Rows
                tblCell = New TableCell
                Dim oLinkExtraAllow As New HyperLink
                oLinkExtraAllow.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=ExtraAllow&II=" & oRowItm("IDVariable") & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                oLinkExtraAllow.Style.Add("text-decoration", "none")
                oLinkExtraAllow.Style.Add("cursor", "pointer")

                oLinkExtraAllow.Text = Nn(GetVariableDefineValue("ExtraAllow", m_SiteMaster.TeamIDGet, oRowItm("IDVariable"), 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N0")

                tblCell.Controls.Add(oLinkExtraAllow)

                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                Dim oLinkExtraProduQuali As New HyperLink
                oLinkExtraProduQuali.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=ExtraProduQuali&II=" & oRowItm("IDVariable") & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                oLinkExtraProduQuali.Style.Add("text-decoration", "none")
                oLinkExtraProduQuali.Style.Add("cursor", "pointer")

                oLinkExtraProduQuali.Text = Nn(GetVariableDefineValue("ExtraProduQuali", m_SiteMaster.TeamIDGet, oRowItm("IDVariable"), 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")

                tblCell.Controls.Add(oLinkExtraProduQuali)

                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)
            Next
            tblGoodsAvailable.Rows.Add(tblRow)

            ' Riga del Goods available
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblGoodsAvailable As New RadLabel
            lblGoodsAvailable.ID = "lblGoodsAvailable"
            lblGoodsAvailable.Text = "Goods available"
            tblCell.Controls.Add(lblGoodsAvailable)
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)
            For Each oRowItm As DataRow In oDTItems.Rows
                tblCell = New TableCell
                Dim oLinkTotalOfferCusto As New HyperLink
                oLinkTotalOfferCusto.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=TotalOfferCusto&II=" & oRowItm("IDVariable") & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                oLinkTotalOfferCusto.Style.Add("text-decoration", "none")
                oLinkTotalOfferCusto.Style.Add("cursor", "pointer")

                oLinkTotalOfferCusto.Text = Nn(GetVariableDefineValue("TotalOfferCusto", m_SiteMaster.TeamIDGet, oRowItm("IDVariable"), 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N0")

                tblCell.Controls.Add(oLinkTotalOfferCusto)

                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                Dim oLinkQualiGoodsOffer As New HyperLink
                oLinkQualiGoodsOffer.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=QualiGoodsOffer&II=" & oRowItm("IDVariable") & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                oLinkQualiGoodsOffer.Style.Add("text-decoration", "none")
                oLinkQualiGoodsOffer.Style.Add("cursor", "pointer")

                oLinkQualiGoodsOffer.Text = Nn(GetVariableDefineValue("QualiGoodsOffer", m_SiteMaster.TeamIDGet, oRowItm("IDVariable"), 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")

                tblCell.Controls.Add(oLinkQualiGoodsOffer)

                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)
            Next
            tblGoodsAvailable.Rows.Add(tblRow)

        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub LoadDataTableRetailSales()
        Dim oDTItems As DataTable = HandleGetItemsPeriodVariables(Session("IDGame"), Nz(Session("LanguageActive")))

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Try
            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header vuota
            Dim lblSalesStockQuarter As New RadLabel
            lblSalesStockQuarter.ID = "lblSalesStockQuarter"
            lblSalesStockQuarter.Text = "Sales and stock of the quarter"
            tblHeaderCell.Controls.Add(lblSalesStockQuarter)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.ColumnSpan = oDTItems.Rows.Count + 1
            tblHeaderRow.Cells.Add(tblHeaderCell)
            tblHeaderRow.BorderStyle = BorderStyle.None
            tblHeaderRow.Style.Add("margin-top", "10px")
            tblRetailSales.Rows.Add(tblHeaderRow)

            ' Prima cella della seconda riga dell'Header vuota
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell
            tblHeaderCell.Text = ""
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderRow.Cells.Add(tblHeaderCell)
            For Each oRowItm As DataRow In oDTItems.Rows
                ' Cella del prezzo di vendita
                tblHeaderCell = New TableHeaderCell
                tblHeaderCell.Text = Nz(oRowItm("VariableName"))
                tblHeaderCell.BorderStyle = BorderStyle.None
                tblHeaderRow.Cells.Add(tblHeaderCell)
            Next
            tblHeaderRow.BorderStyle = BorderStyle.None
            tblHeaderRow.Style.Add("margin-top", "5px")
            tblRetailSales.Rows.Add(tblHeaderRow)


            ' Controllo la presenza delle variabili nella tabella delle variabili del gioco
            VerifiyVariableExists("TotalOfferCusto", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)
            VerifiyVariableExists("MaximSellaGoods", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)
            VerifiyVariableExists("TotalProduSold", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)
            VerifiyVariableExists("FirstOrderAlloc", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)
            VerifiyVariableExists("ResidDemanAlloc", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)
            VerifiyVariableExists("FinisGoodsStock", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)

            ' Aggiungo le righe di dettaglio
            ' Riga del Goods available
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblGoodsAvailable As New RadLabel
            lblGoodsAvailable.ID = "lblGoodsAvailableRetailSales"
            lblGoodsAvailable.Text = "Goods available"
            tblCell.Controls.Add(lblGoodsAvailable)
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)
            For Each oRowItm As DataRow In oDTItems.Rows
                tblCell = New TableCell
                Dim oLinkTotalOfferCusto As New HyperLink
                oLinkTotalOfferCusto.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=TotalOfferCusto&II=" & oRowItm("IDVariable") & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                oLinkTotalOfferCusto.Style.Add("text-decoration", "none")
                oLinkTotalOfferCusto.Style.Add("cursor", "pointer")

                oLinkTotalOfferCusto.Text = Nn(GetVariableDefineValue("TotalOfferCusto", m_SiteMaster.TeamIDGet, oRowItm("IDVariable"), 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N0")

                tblCell.Controls.Add(oLinkTotalOfferCusto)

                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)
            Next
            tblRetailSales.Rows.Add(tblRow)

            ' Riga del Maximum sales
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblMaimumSales As New RadLabel
            lblMaimumSales.ID = "lblMaimumSales"
            lblMaimumSales.Text = "Maximum sales"
            tblCell.Controls.Add(lblMaimumSales)
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)
            For Each oRowItm As DataRow In oDTItems.Rows
                tblCell = New TableCell
                Dim oLinkMaximSellaGoods As New HyperLink
                oLinkMaximSellaGoods.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=MaximSellaGoods&II=" & oRowItm("IDVariable") & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                oLinkMaximSellaGoods.Style.Add("text-decoration", "none")
                oLinkMaximSellaGoods.Style.Add("cursor", "pointer")

                oLinkMaximSellaGoods.Text = Nn(GetVariableDefineValue("MaximSellaGoods", m_SiteMaster.TeamIDGet, oRowItm("IDVariable"), 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N0")

                tblCell.Controls.Add(oLinkMaximSellaGoods)

                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)
            Next
            tblRetailSales.Rows.Add(tblRow)

            ' Riga del Retail sales of the month
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblRetailSalesMonth As New RadLabel
            lblRetailSalesMonth.ID = "lblRetailsSalesMonth"
            lblRetailSalesMonth.Text = "Retail sales of the month"
            tblCell.Controls.Add(lblRetailSalesMonth)
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)
            For Each oRowItm As DataRow In oDTItems.Rows
                tblCell = New TableCell
                Dim oLinkTotalProduSold As New HyperLink
                oLinkTotalProduSold.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=TotalProduSold&II=" & oRowItm("IDVariable") & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                oLinkTotalProduSold.Style.Add("text-decoration", "none")
                oLinkTotalProduSold.Style.Add("cursor", "pointer")

                oLinkTotalProduSold.Text = Nn(GetVariableDefineValue("TotalProduSold", m_SiteMaster.TeamIDGet, oRowItm("IDVariable"), 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N0")

                tblCell.Controls.Add(oLinkTotalProduSold)

                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)
            Next
            tblRetailSales.Rows.Add(tblRow)

            ' Local customer base
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblLocalCustomerBase As New RadLabel
            lblLocalCustomerBase.ID = "lblLocalCustomerBase"
            lblLocalCustomerBase.Text = "Local customer base"
            tblCell.Controls.Add(lblLocalCustomerBase)
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("padding-left", "10px")
            tblRow.Cells.Add(tblCell)

            For Each oRowItm As DataRow In oDTItems.Rows
                tblCell = New TableCell
                Dim oLinkFirstOrderAlloc As New HyperLink
                oLinkFirstOrderAlloc.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=FirstOrderAlloc&II=" & oRowItm("IDVariable") & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                oLinkFirstOrderAlloc.Style.Add("text-decoration", "none")
                oLinkFirstOrderAlloc.Style.Add("cursor", "pointer")

                oLinkFirstOrderAlloc.Text = Nn(GetVariableDefineValue("FirstOrderAlloc", m_SiteMaster.TeamIDGet, oRowItm("IDVariable"), 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N0")

                tblCell.Controls.Add(oLinkFirstOrderAlloc)
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)
            Next
            tblRetailSales.Rows.Add(tblRow)

            ' Remaining captured clients
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblRemainingCapturedClients As New RadLabel
            lblRemainingCapturedClients.ID = "lblRemainingCapturedClients"
            lblRemainingCapturedClients.Text = "Remaining captured clients"
            tblCell.Controls.Add(lblRemainingCapturedClients)
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("padding-left", "10px")
            tblRow.Cells.Add(tblCell)

            For Each oRowItm As DataRow In oDTItems.Rows
                tblCell = New TableCell
                Dim oLinkResidDemanAlloc As New HyperLink
                oLinkResidDemanAlloc.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=ResidDemanAlloc&II=" & oRowItm("IDVariable") & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                oLinkResidDemanAlloc.Style.Add("text-decoration", "none")
                oLinkResidDemanAlloc.Style.Add("cursor", "pointer")

                oLinkResidDemanAlloc.Text = Nn(GetVariableDefineValue("ResidDemanAlloc", m_SiteMaster.TeamIDGet, oRowItm("IDVariable"), 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N0")

                tblCell.Controls.Add(oLinkResidDemanAlloc)

                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)
            Next
            tblRetailSales.Rows.Add(tblRow)

            ' Riga del Final Stock (at end of the quarter)
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblFinalStock As New RadLabel
            lblFinalStock.ID = "lblFinalStock"
            lblFinalStock.Text = "Final Stock (at end of the quarter)"
            tblCell.Controls.Add(lblFinalStock)
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)
            For Each oRowItm As DataRow In oDTItems.Rows
                tblCell = New TableCell
                Dim oLinkFinisGoodsStock As New HyperLink
                oLinkFinisGoodsStock.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=FinisGoodsStock&II=" & oRowItm("IDVariable") & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                oLinkFinisGoodsStock.Style.Add("text-decoration", "none")
                oLinkFinisGoodsStock.Style.Add("cursor", "pointer")

                oLinkFinisGoodsStock.Text = Nn(GetVariableDefineValue("FinisGoodsStock", m_SiteMaster.TeamIDGet, oRowItm("IDVariable"), 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N0")

                tblCell.Controls.Add(oLinkFinisGoodsStock)

                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)
            Next
            tblRetailSales.Rows.Add(tblRow)

        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try

    End Sub

    Private Sub Distribution_Player_Sales2_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        If Not Page.IsPostBack Then
            If Session("IDRole") = "P" Then
                m_SiteMaster.ReloadPeriodsCalculate()
            End If
            LoadData()

            btnTranslate.Visible = Session("IDRole").Contains("D")
            ' Controllo tutti gli oggetti presenti
            Dim oTranslation As New TranslationObjectForm(Me, g_DAL, Nz(Session("LanguageActive")))

        End If
    End Sub

    Private Sub btnTranslate_Click(sender As Object, e As EventArgs) Handles btnTranslate.Click
        Session("PageOrigin") = Me.ToString().Substring(4, Me.ToString().Substring(4).Length - 5) + ".aspx"
        Response.Redirect("PageTranslation.aspx")
        Response.End()
    End Sub

#Region "PANNELLO HELP"

    Private Sub LoadHelp()
        lblHelp.Text = "<p><strong>-Beginning stock</strong><br/>
                        It is the  stock at the beginning of the period (eg. products not sold &nbsp;in the previous period).<br/><br/>
                        <strong>-Production </strong><br/>
                        This is the  production allowed in the period.<br/><br/>
                        <strong>-Sales to wholesalers</strong><br/>
                        Sales to  the wholesalers come out of production of the period (it is not possible to  sell from stock to the wholesalers.)<br/><br/>
                        <strong>-Purchase of finished goods</strong><br/>
                        These are  the finished products bought-in in the period<br/><br/>
                        <strong>-Goods available</strong><br/>
                        It is the  total available to retail in the period. This is made up from existing stock at  the beginning of the period + the quantity bought-in + the quantity produced  &amp;#8211; the quantity sold to the wholesalers.<br/>
                        It is not  possible to sell more than 90% of the goods available.<br/><br/>
                        <strong>-Maximum sales</strong><br/>
                        The maximum  quantity available to sell cannot exceed 90% of the goods available to retail. Whenever  the amount is lower than 90 %, possible reasons are people effectiveness (staff  and sales people) and/or the total space available for the retailing goods  (space for sales is limited by the space units of your outlets, which are  40,000 for central outlets and 55,000 for out-of-town  outlets).&lt;br&gt;&lt;/td&gt;<br/><br/>
                        <strong>-Total sales</strong><br/>
                        Total sales  are made up of first choice and additional sales.<br/><br/>
                        <strong>-First choice</strong><br/>
                        First  choice sales are your actual retail sales and are the market response to your  decisions <br/><br/>
                        <strong>-Second level sales (Second choice)</strong><br/>
                        When  customers decide to buy from one of your competitors but are unsuccessful, they  will look for a different supplier. &nbsp;If  you succeed to sell to these customers, this kind of sale is recorded as an &nbsp;additional sale;. Additional sales are orders  which a competitor has not been able to satisfy and you have benefitted. <br/><br/>
                        <strong>-Final stock</strong><br/>
                        The stock  at the end of the period is the difference between goods available and total  sales. This becomes the new stock at the beginning of the following period.</p>"
    End Sub

#End Region

End Class
