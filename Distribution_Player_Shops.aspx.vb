﻿Imports System.Data
Imports APS.APSUtility
Imports APPCore.Utility
Imports Telerik.Web.UI

Partial Class Distribution_Player_Shops
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadDataPage()

        Message.Visible = False
        pnlMain.Update()
    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadDataPage()

        Message.Visible = False
        pnlMain.Update()
    End Sub

    Private Sub Distribution_Player_Shops_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        m_SessionData = Session("SessionData")

        m_SiteMaster = Me.Master

        m_SiteMaster.LoadPeriodsValid = Session("IDRole") = "P"

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oCBOPeriod As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOPeriodValid As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriodValid"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler m_SiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler m_SiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(Session("IDGame")) ' Ultimo periodo
        oCBOPeriod.SelectedValue = Session("IDPeriodValid")
        m_SiteMaster.PeriodChange = Session("IDPeriodValid")

        Session("NameTeam") = GetTeamName(Session("IDTeam"))
        oCBOTeam.SelectedValue = Session("IDTeam")
        m_SiteMaster.TeamChange = Session("IDTeam")

        ' Gestione del pannello delle decisioni concesse
        modalPopup.OpenerElementID = lnkHelp.ClientID
        modalPopup.Modal = False
        modalPopup.VisibleTitlebar = True

    End Sub

    Private Sub Distribution_Player_Shops_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        If Not Page.IsPostBack Then
            If Session("IDRole") = "P" Then
                m_SiteMaster.ReloadPeriodsCalculate()
            End If
            LoadDataPage()
            btnTranslate.Visible = Session("IDRole").Contains("D")

        End If

        ' Controllo tutti gli oggetti presenti
        Dim oTranslation As New TranslationObjectForm(Me, g_DAL, Nz(Session("LanguageActive")))
    End Sub

    Private Sub btnTranslate_Click(sender As Object, e As EventArgs) Handles btnTranslate.Click
        Session("PageOrigin") = Me.ToString().Substring(4, Me.ToString().Substring(4).Length - 5) + ".aspx"
        Response.Redirect("PageTranslation.aspx")
        Response.End()
    End Sub

    Private Sub LoadDataPage()
        tblShops.Rows.Clear()
        tblShopsSaturation.Rows.Clear()

        LoadGridSales()

        LoadHelp()
    End Sub

    Private Sub LoadGridSales()
        Dim sSQL As String

        ' Ciclo su tutti i players per il caricamento della griglia
        Dim oDTPlayers As DataTable = LoadTeamsGame(Session("IDGame"))

        Dim dTotalCentral As Double = 0
        Dim dTotalPerif As Double = 0

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        ' Costruisco la riga Header
        tblHeaderRow = New TableHeaderRow
        tblHeaderCell = New TableHeaderCell

        ' Controllo la presenza di dati per il periodo
        sSQL = "SELECT COUNT(C.ID) AS ID " _
             & "FROM Variables_Calculate_Define V " _
             & "INNER JOIN Variables_Calculate_Value C ON V.Id = C.IDVariable " _
             & "WHERE LTRIM(RTRIM(V.VariableName)) IN('CurreOutleCentr', 'CurreOutleSubur') " _
             & "AND IDGame = " & Session("IDGame") & " AND C.IDPeriod = " & Session("IDPeriod")
        If Session("IDRole") = "P" Then
            sSQL &= " AND ISNULL(C.Simulation, 0) = 0 "
        End If
        Dim oDTData As DataTable = g_DAL.ExecuteDataTable(sSQL)

        If Nni(oDTData.Rows(0)("ID")) > 1 Then
            lblTitolo.Text = "Shops"

            ' Prima cella della prima riga dell'Header vuota
            tblHeaderCell.Text = ""
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.Style.Add("width", "50%")
            tblHeaderCell.Style.Add("background", "transparent")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            ' Cella No.
            tblHeaderCell = New TableHeaderCell
            Dim lblCentrals As New RadLabel
            lblCentrals.ID = "lblCentrals"
            lblCentrals.Text = "Centrals"
            lblCentrals.ForeColor = Drawing.Color.White
            tblHeaderCell.Controls.Add(lblCentrals)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.Style.Add("width", "25%")
            tblHeaderCell.Style.Add("background", "#525252")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            ' Average No. per store
            tblHeaderCell = New TableHeaderCell
            Dim lblOutOfTown As New RadLabel
            lblOutOfTown.ID = "lblOutOfTown"
            lblOutOfTown.Text = "Out of town"
            lblOutOfTown.ForeColor = Drawing.Color.White
            tblHeaderCell.Controls.Add(lblOutOfTown)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.Style.Add("width", "25%")
            tblHeaderCell.Style.Add("background", "#525252")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            tblShops.Rows.Add(tblHeaderRow)

            For Each oRow As DataRow In oDTPlayers.Rows
                tblRow = New TableRow
                tblCell = New TableCell
                Dim oLBLStaff As New RadLabel
                oLBLStaff.Text = oRow("TeamName")
                oLBLStaff.ForeColor = Drawing.Color.DarkRed
                tblCell.Controls.Add(oLBLStaff)
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                Dim oLinkCurreOutleCentr As New HyperLink
                oLinkCurreOutleCentr.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=CurreOutleCentr&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                oLinkCurreOutleCentr.Style.Add("text-decoration", "none")
                oLinkCurreOutleCentr.Style.Add("cursor", "pointer")
                oLinkCurreOutleCentr.Text = Nn(GetVariableDefineValue("CurreOutleCentr", oRow("ID"), 0, 0, Session("IDPeriod"), Session("IDGame"))).ToString("N0")
                oLinkCurreOutleCentr.Style.Add("font-size", "9pt")
                oLinkCurreOutleCentr.Style.Add("text-align", "right")
                oLinkCurreOutleCentr.Style.Add("font-weight", "normal")
                tblCell.Style.Add("text-align", "right")
                tblCell.Controls.Add(oLinkCurreOutleCentr)
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)

                dTotalCentral += Nn(oLinkCurreOutleCentr.Text)

                tblCell = New TableCell
                Dim oLinkCurreOutleSubur As New HyperLink
                oLinkCurreOutleSubur.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=CurreOutleSubur&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                oLinkCurreOutleSubur.Style.Add("text-decoration", "none")
                oLinkCurreOutleSubur.Style.Add("cursor", "pointer")

                oLinkCurreOutleSubur.Text = Nn(GetVariableDefineValue("CurreOutleSubur", oRow("ID"), 0, 0, Session("IDPeriod"), Session("IDGame"))).ToString("N0")
                oLinkCurreOutleSubur.Style.Add("font-size", "9pt")
                oLinkCurreOutleSubur.Style.Add("text-align", "right")
                oLinkCurreOutleSubur.Style.Add("font-weight", "normal")
                tblCell.Style.Add("text-align", "right")
                tblCell.Controls.Add(oLinkCurreOutleSubur)
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)

                dTotalPerif += Nn(oLinkCurreOutleSubur.Text)

                tblShops.Rows.Add(tblRow)
            Next

            ' Riga dei totali
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblTotalShops As New RadLabel
            lblTotalShops.ID = "lblTotalShops"
            lblTotalShops.Text = "TOTAL"
            lblTotalShops.Style.Add("font-size", "9pt")
            lblTotalShops.Style.Add("text-align", "left")
            lblTotalShops.Style.Add("font-weight", "bold")
            lblTotalShops.Style.Add("color", "#ffffff")
            tblCell.Controls.Add(lblTotalShops)
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("padding-left", "20px")
            tblCell.Style.Add("background", "#3e6c86")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLBLTotalCentral As New RadLabel
            oLBLTotalCentral.Text = dTotalCentral.ToString("N0")
            oLBLTotalCentral.Style.Add("font-size", "9pt")
            oLBLTotalCentral.Style.Add("text-align", "right")
            oLBLTotalCentral.Style.Add("color", "#ffffff")
            oLBLTotalCentral.BackColor = Drawing.Color.Transparent
            tblCell.Controls.Add(oLBLTotalCentral)
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("text-align", "right")
            tblCell.Style.Add("background", "#3e6c86")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLBLTotPerif As New RadLabel
            oLBLTotPerif.Text = dTotalPerif.ToString("N0")
            oLBLTotPerif.Style.Add("font-size", "9pt")
            oLBLTotPerif.Style.Add("text-align", "right")
            oLBLTotPerif.Style.Add("color", "#ffffff")
            oLBLTotPerif.BackColor = Drawing.Color.Transparent
            tblCell.Controls.Add(oLBLTotPerif)
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("text-align", "right")
            tblCell.Style.Add("background", "#3e6c86")
            tblRow.Cells.Add(tblCell)

            tblShops.Rows.Add(tblRow)

            LoadGridSalesSaturation()
        Else
            lblTitolo.Text = "Shops" & "<br/>" & "<br/>" & "No data available for this period"
            tblShops.Rows.Clear()
        End If

    End Sub

    Private Sub LoadGridSalesSaturation()
        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Dim iNumPlayers As Integer = LoadTeamsGame(Session("IDGame")).Rows.Count

        ' Costruisco la riga Header
        tblHeaderRow = New TableHeaderRow
        tblHeaderCell = New TableHeaderCell

        ' Cella Header
        tblHeaderCell = New TableHeaderCell
        Dim oLBLCentrals As New RadLabel
        oLBLCentrals.Text = "Shops saturation"
        oLBLCentrals.ID = "lblShopsSaturationHeader"
        oLBLCentrals.ForeColor = Drawing.Color.White
        tblHeaderCell.Controls.Add(oLBLCentrals)
        tblHeaderCell.BorderStyle = BorderStyle.None
        tblHeaderCell.Style.Add("width", "25%")
        tblHeaderCell.Style.Add("background", "#525252")
        tblHeaderCell.ColumnSpan = 3
        tblHeaderRow.Cells.Add(tblHeaderCell)

        tblShopsSaturation.Rows.Add(tblHeaderRow)

        ' Riga 1
        tblRow = New TableRow
        tblCell = New TableCell
        Dim lblOperativeShops As New RadLabel
        lblOperativeShops.ID = "lblOperativeShops"
        lblOperativeShops.Text = "Operative shops"
        lblOperativeShops.ForeColor = Drawing.Color.DarkRed
        tblCell.Controls.Add(lblOperativeShops)
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim lblUnits As New RadLabel
        lblUnits.ID = "lblUnits"
        lblUnits.Text = "units"
        lblUnits.ForeColor = Drawing.Color.DarkRed
        tblCell.Controls.Add(lblUnits)
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim oLBLOperativeShopValue As New RadLabel
        oLBLOperativeShopValue.Text = (Nn(GetVariableDefineValue("CurreOutleCentr", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))) _
                                    + Nn(GetVariableDefineValue("CurreOutleSubur", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))))
        oLBLOperativeShopValue.Style.Add("font-size", "9pt")
        oLBLOperativeShopValue.Style.Add("text-align", "right")
        oLBLOperativeShopValue.Style.Add("font-weight", "normal")
        tblCell.Style.Add("text-align", "right")
        tblCell.Controls.Add(oLBLOperativeShopValue)
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)
        tblShopsSaturation.Rows.Add(tblRow)

        ' Riga 2
        tblRow = New TableRow
        tblCell = New TableCell
        Dim lblSpaceAvailable As New RadLabel
        lblSpaceAvailable.ID = "lblSpaceAvailable"
        lblSpaceAvailable.Text = "Space available"
        lblSpaceAvailable.ForeColor = Drawing.Color.DarkRed
        tblCell.Controls.Add(lblSpaceAvailable)
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim lblSpaceUnits As New RadLabel
        lblSpaceUnits.Text = "Space units"
        lblSpaceUnits.ID = "lblSpaceUnits"
        lblSpaceUnits.ForeColor = Drawing.Color.DarkRed
        tblCell.Controls.Add(lblSpaceUnits)
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim oLINKSpaceUnitsValue As New HyperLink
        oLINKSpaceUnitsValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=MaximSpaceAvail&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        oLINKSpaceUnitsValue.Style.Add("text-decoration", "none")
        oLINKSpaceUnitsValue.Style.Add("cursor", "pointer")

        oLINKSpaceUnitsValue.Text = Nn(GetVariableDefineValue("MaximSpaceAvail", Session("IDTeam"), 0, 0, Session("IDPeriod"), Session("IDGame"))).ToString("N0")
        oLINKSpaceUnitsValue.Style.Add("font-size", "9pt")
        oLINKSpaceUnitsValue.Style.Add("text-align", "right")
        oLINKSpaceUnitsValue.Style.Add("font-weight", "normal")
        tblCell.Style.Add("text-align", "right")
        tblCell.Controls.Add(oLINKSpaceUnitsValue)
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)
        tblShopsSaturation.Rows.Add(tblRow)

        ' Riga 3
        tblRow = New TableRow
        tblCell = New TableCell
        Dim lblSpaceRequired As New RadLabel
        lblSpaceRequired.ID = "lblSpaceRequired"
        lblSpaceRequired.Text = "Space required"
        lblSpaceRequired.ForeColor = Drawing.Color.DarkRed
        tblCell.Controls.Add(lblSpaceRequired)
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim lblSpaceRequiredUnits As New RadLabel
        lblSpaceRequiredUnits.ID = "lblSpaceRequiredUnits"
        lblSpaceRequiredUnits.Text = "space units"
        lblSpaceRequiredUnits.ForeColor = Drawing.Color.DarkRed
        tblCell.Controls.Add(lblSpaceRequiredUnits)
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim oLINKSpaceRequiredUnitsValue As New HyperLink
        oLINKSpaceRequiredUnitsValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=TotalVolumSpace&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        oLINKSpaceRequiredUnitsValue.Style.Add("text-decoration", "none")
        oLINKSpaceRequiredUnitsValue.Style.Add("cursor", "pointer")

        oLINKSpaceRequiredUnitsValue.Text = Nn(GetVariableDefineValue("TotalVolumSpace", Session("IDTeam"), 0, 0, Session("IDPeriod"), Session("IDGame"))).ToString("N0")
        oLINKSpaceRequiredUnitsValue.Style.Add("font-size", "9pt")
        oLINKSpaceRequiredUnitsValue.Style.Add("text-align", "right")
        oLINKSpaceRequiredUnitsValue.Style.Add("font-weight", "normal")
        tblCell.Style.Add("text-align", "right")
        tblCell.Controls.Add(oLINKSpaceRequiredUnitsValue)
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)
        tblShopsSaturation.Rows.Add(tblRow)

        ' Riga 4
        tblRow = New TableRow
        tblCell = New TableCell
        Dim lblShopSaturation As New RadLabel
        lblShopSaturation.ID = "lblShopSaturation"
        lblShopSaturation.Text = "Shops saturation"
        lblShopSaturation.ForeColor = Drawing.Color.DarkRed
        tblCell.Controls.Add(lblShopSaturation)
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim oLBLShopSaturationUnits As New RadLabel
        oLBLShopSaturationUnits.Text = "%"
        oLBLShopSaturationUnits.ForeColor = Drawing.Color.DarkRed
        tblCell.Controls.Add(oLBLShopSaturationUnits)
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim oLBLShopSaturationUnitsValue As New RadLabel
        oLBLShopSaturationUnitsValue.Text = (Nn(oLINKSpaceRequiredUnitsValue.Text) / oLINKSpaceUnitsValue.Text).ToString("P1")
        oLBLShopSaturationUnitsValue.Style.Add("font-size", "9pt")
        oLBLShopSaturationUnitsValue.Style.Add("text-align", "right")
        oLBLShopSaturationUnitsValue.Style.Add("font-weight", "normal")
        tblCell.Style.Add("text-align", "right")
        tblCell.Controls.Add(oLBLShopSaturationUnitsValue)
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)
        tblShopsSaturation.Rows.Add(tblRow)

    End Sub

#Region "PANNELLO HELP"

    Private Sub LoadHelp()
        lblHelp.Text = "<p><strong>Shops</strong></p><br/>
                        <p><strong>There is no limit to the number of shops.</strong><br/><br/>
                        <strong>There are two types of shops: central and  out-of-town with different capacity and different impact on attraction of  customers.</strong> </p>"
    End Sub

#End Region

End Class
