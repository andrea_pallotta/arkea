﻿Imports System.Data
Imports APS.APSUtility
Imports APPCore.Utility
Imports DALC4NET
Imports Telerik.Web.UI

Partial Class Distribution_Player_Transport2
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster
    Private m_IDPeriodoMax As Integer

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        Message.Visible = False
        LoadData()
        UpdatePanelMain.Update()
    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        Message.Visible = False
        LoadData()
        UpdatePanelMain.Update()
    End Sub

    Private Sub Distribution_Player_Transport2_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        m_SessionData = Session("SessionData")

        m_SiteMaster = Me.Master

        m_SiteMaster.LoadPeriodsValid = Session("IDRole") = "P"

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oCBOPeriod As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOPeriodValid As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriodValid"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler m_SiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler m_SiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(Session("IDGame")) ' Ultimo periodo
        oCBOPeriod.SelectedValue = Session("IDPeriodValid")
        m_SiteMaster.PeriodChange = Session("IDPeriodValid")

        Session("NameTeam") = GetTeamName(Session("IDTeam"))
        oCBOTeam.SelectedValue = Session("IDTeam")
        m_SiteMaster.TeamChange = Session("IDTeam")

        ' Gestione del pannello delle decisioni concesse
        modalPopup.OpenerElementID = lnkHelp.ClientID
        modalPopup.Modal = False
        modalPopup.VisibleTitlebar = True

        LoadHelp()
    End Sub

    Private Sub LoadData()
        tblVehicles.Rows.Clear()
        tblTransportCapacity.Rows.Clear()

        LoadDataVehicles()
        LoadDataTransportCapacity()
    End Sub

    Private Sub LoadDataVehicles()
        Dim oDTAge As DataTable = HandleLoadAgeData(Session("IDGame"))
        Dim iNumQuarter As Integer = 0

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Dim iTotVehicles As Integer

        Try
            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header vuota
            Dim oLBLTitleGrid As New RadLabel
            oLBLTitleGrid.ID = "lblGridTitle"
            oLBLTitleGrid.Text = "Vehicles"
            oLBLTitleGrid.ForeColor = Drawing.Color.White
            tblHeaderCell.Controls.Add(oLBLTitleGrid)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.ColumnSpan = 2
            tblHeaderCell.Style.Add("background", "#525252")

            tblHeaderRow.BorderStyle = BorderStyle.None
            tblHeaderRow.Style.Add("margin-top", "10px")
            tblHeaderRow.Cells.Add(tblHeaderCell)
            tblVehicles.Rows.Add(tblHeaderRow)

            ' Prima riga di intestazione
            tblCell = New TableCell
            tblRow = New TableRow

            Dim oLBLAgeQuarters As New RadLabel
            oLBLAgeQuarters.ID = "lblAgeQuarters"
            oLBLAgeQuarters.Text = "Age (quarters)"
            oLBLAgeQuarters.ForeColor = Drawing.Color.White
            tblCell.Controls.Add(oLBLAgeQuarters)
            tblCell.Style.Add("width", "60%")
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("background", "#3e6c86")
            tblCell.Style.Add("padding-left", "5px")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLBLNumVehicles As New RadLabel
            oLBLNumVehicles.ID = "lblNumVehicles"
            oLBLNumVehicles.Text = "Num."
            oLBLNumVehicles.ForeColor = Drawing.Color.White
            tblCell.Controls.Add(oLBLNumVehicles)
            tblCell.Style.Add("width", "40%")
            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("background", "#3e6c86")
            tblRow.Cells.Add(tblCell)

            tblVehicles.Rows.Add(tblRow)

            Dim iIDQuarter As Integer = 1

            For Each oRowAge As DataRow In oDTAge.Rows
                ' Righe dati
                Dim oLBLQuarter As New RadLabel

                oLBLQuarter.ID = "Q" & iNumQuarter

                tblRow = New TableRow
                tblCell = New TableCell
                iNumQuarter += 3

                ' Conto gli anni
                Dim iTotaleQuarter As Double = iNumQuarter / 12
                Dim sNomeVariabileLista As String

                Select Case iTotaleQuarter
                    Case 1
                        oLBLQuarter.Text = "(1 year)" & " " & iNumQuarter

                    Case 2
                        oLBLQuarter.Text = "(2 years)" & " " & iNumQuarter

                    Case 3
                        oLBLQuarter.Text = "(3 years)" & " " & iNumQuarter

                    Case 4
                        oLBLQuarter.Text = "(4 years)" & " " & iNumQuarter

                    Case 5
                        oLBLQuarter.Text = "(5 years)" & " " & iNumQuarter

                    Case 6
                        oLBLQuarter.Text = "(6 years)" & " " & iNumQuarter

                    Case 7
                        oLBLQuarter.Text = "(7 years)" & " " & iNumQuarter

                    Case 8
                        oLBLQuarter.Text = "(8 years)" & " " & iNumQuarter

                    Case 9
                        oLBLQuarter.Text = "(9 years)" & " " & iNumQuarter

                    Case 10
                        oLBLQuarter.Text = "(10 years)" & " " & iNumQuarter

                    Case Else
                        oLBLQuarter.Text = iNumQuarter
                End Select
                oLBLQuarter.ForeColor = Drawing.Color.Brown

                tblCell.Controls.Add(oLBLQuarter)
                tblCell.BorderStyle = BorderStyle.None
                tblCell.Style.Add("padding-left", "5px")
                tblRow.Cells.Add(tblCell)

                sNomeVariabileLista = "var_" & m_SiteMaster.TeamNameGet & "_TotalVehicOwned_" & oRowAge("AgeDescription")
                tblCell = New TableCell

                Dim lnkTotalVehicOwned As New HyperLink
                lnkTotalVehicOwned.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=TotalVehicOwned&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                lnkTotalVehicOwned.Style.Add("text-decoration", "none")
                lnkTotalVehicOwned.Style.Add("cursor", "pointer")
                lnkTotalVehicOwned.ID = "lblVehiclesNumber" & iNumQuarter

                lnkTotalVehicOwned.Text = Nn(GetVariableState("TotalVehicOwned", Session("IDTeam"), m_SiteMaster.PeriodGetCurrent, sNomeVariabileLista, Session("IDGame"), oRowAge("AgeDescription"))).ToString("N0")

                tblCell.Controls.Add(lnkTotalVehicOwned)

                tblCell.BorderStyle = BorderStyle.None
                tblCell.Style.Add("text-align", "right")
                tblRow.Cells.Add(tblCell)

                iTotVehicles += Nni(lnkTotalVehicOwned.Text)

                tblVehicles.Rows.Add(tblRow)
            Next

            ' Riga di riepilogo
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLBLTotVehicles As New RadLabel
            oLBLTotVehicles.Text = "Total vehicles"
            oLBLTotVehicles.Style.Add("font-size", "8pt")
            oLBLTotVehicles.Style.Add("text-align", "left")
            oLBLTotVehicles.Style.Add("font-weight", "bold")
            oLBLTotVehicles.Style.Add("color", "#ffffff")
            tblCell.Controls.Add(oLBLTotVehicles)
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("padding-left", "5px")
            tblCell.Style.Add("background", "#3e6c86")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLBLTotVehiclesNum As New RadLabel
            oLBLTotVehiclesNum.Text = iTotVehicles.ToString("N0")
            oLBLTotVehiclesNum.Style.Add("font-size", "8pt")
            oLBLTotVehiclesNum.Style.Add("text-align", "right")
            oLBLTotVehiclesNum.Style.Add("font-weight", "bold")
            oLBLTotVehiclesNum.Style.Add("color", "#ffffff")
            tblCell.Controls.Add(oLBLTotVehiclesNum)
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("padding-left", "20px")
            tblCell.Style.Add("background", "#3e6c86")
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblVehicles.Rows.Add(tblRow)
        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub LoadDataTransportCapacity()
        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Try
            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header vuota
            Dim oLBLTitleGrid As New RadLabel
            oLBLTitleGrid.ID = "lblGridTitleTransport"
            oLBLTitleGrid.Text = "Transport capacity (space units)"
            oLBLTitleGrid.ForeColor = Drawing.Color.White
            tblHeaderCell.Controls.Add(oLBLTitleGrid)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.ColumnSpan = 2
            tblHeaderCell.Style.Add("background", "#525252")

            tblHeaderRow.BorderStyle = BorderStyle.None
            tblHeaderRow.Style.Add("margin-top", "10px")
            tblHeaderRow.Cells.Add(tblHeaderCell)
            tblTransportCapacity.Rows.Add(tblHeaderRow)

            ' Prima riga
            tblCell = New TableCell
            tblRow = New TableRow

            Dim oLBLNeeded As New RadLabel
            oLBLNeeded.ID = "lblNeeded"
            oLBLNeeded.Text = "Needed"
            oLBLNeeded.ForeColor = Drawing.Color.DarkRed
            tblCell.Controls.Add(oLBLNeeded)
            tblCell.Style.Add("width", "60%")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell

            Dim lnkTotalTransRequi As New HyperLink
            lnkTotalTransRequi.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=TotalTransRequi&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            lnkTotalTransRequi.Style.Add("text-decoration", "none")
            lnkTotalTransRequi.Style.Add("cursor", "pointer")

            lnkTotalTransRequi.Text = Nn(GetVariableDefineValue("TotalTransRequi", Session("IDTeam"), 0, 0, Session("IDPeriod"), Session("IDGame"))).ToString("N0")

            lnkTotalTransRequi.ForeColor = Drawing.Color.Black

            tblCell.Controls.Add(lnkTotalTransRequi)

            tblCell.Style.Add("width", "40%")
            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblTransportCapacity.Rows.Add(tblRow)

            ' Seconda riga
            tblCell = New TableCell
            tblRow = New TableRow

            Dim oLBLTotalOwned As New RadLabel
            oLBLTotalOwned.ID = "lblTotalOwned"
            oLBLTotalOwned.Text = "Total owned vehicles"
            oLBLTotalOwned.ForeColor = Drawing.Color.DarkRed
            tblCell.Controls.Add(oLBLTotalOwned)
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell

            Dim lnkTotalCapacTrans As New HyperLink
            lnkTotalCapacTrans.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=TotalCapacTrans&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            lnkTotalCapacTrans.Style.Add("text-decoration", "none")
            lnkTotalCapacTrans.Style.Add("cursor", "pointer")

            lnkTotalCapacTrans.Text = Nn(GetVariableDefineValue("TotalCapacTrans", Session("IDTeam"), 0, 0, Session("IDPeriod"), Session("IDGame"))).ToString("N0")
            lnkTotalCapacTrans.ForeColor = Drawing.Color.Black

            tblCell.Controls.Add(lnkTotalCapacTrans)

            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblTransportCapacity.Rows.Add(tblRow)

            ' Terza riga
            tblCell = New TableCell
            tblRow = New TableRow

            Dim oLBLExternaleHire As New RadLabel
            oLBLExternaleHire.ID = "lblExternalHire"
            oLBLExternaleHire.Text = "External hire"
            oLBLExternaleHire.ForeColor = Drawing.Color.DarkRed
            tblCell.Controls.Add(oLBLExternaleHire)
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell

            Dim lnkExtraTransNeede As New HyperLink
            lnkExtraTransNeede.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=ExtraTransNeede&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            lnkExtraTransNeede.Style.Add("text-decoration", "none")
            lnkExtraTransNeede.Style.Add("cursor", "pointer")

            lnkExtraTransNeede.Text = Nn(GetVariableDefineValue("ExtraTransNeede", Session("IDTeam"), 0, 0, Session("IDPeriod"), Session("IDGame"))).ToString("N0")
            lnkExtraTransNeede.ForeColor = Drawing.Color.Black
            tblCell.Controls.Add(lnkExtraTransNeede)

            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblTransportCapacity.Rows.Add(tblRow)
        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub Distribution_Player_Transport2_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        If Not Page.IsPostBack Then
            If Session("IDRole") = "P" Then
                m_SiteMaster.ReloadPeriodsCalculate()
            End If
            LoadData()

            btnTranslate.Visible = Session("IDRole").Contains("D")
            ' Controllo tutti gli oggetti presenti
            Dim oTranslation As New TranslationObjectForm(Me, g_DAL, Nz(Session("LanguageActive")))

        End If
    End Sub

    Private Sub btnTranslate_Click(sender As Object, e As EventArgs) Handles btnTranslate.Click
        Session("PageOrigin") = Me.ToString().Substring(4, Me.ToString().Substring(4).Length - 5) + ".aspx"
        Response.Redirect("PageTranslation.aspx")
        Response.End()
    End Sub

#Region "PANNELLO HELP"

    Private Sub LoadHelp()
        lblHelp.Text = "<p><strong>Vehicles</strong><br>
                      <strong>It is the total number of existing owned  vehicles (it does not include those ordered in the current period</strong></p>
                    <p><strong>Needed</strong><br>
                        <strong>This is the number of units of transport  required to deliver your products from the production plant to the retailing  outlets</strong></p>
                    <p><strong>Total &nbsp;owned vehicles</strong><br>
                        <strong>The internal capacity is calculated on the  number of owned vehicles and their age. The transport capacity will be reduced  every period by 1/12th of the original transport capacity. The cost of internal  transport is 0.3 per space unit.</strong></p>
                    <p><strong>External hire</strong><br>
                        <strong>The external transport capacity is  automatically available to ensure all production is transported to the outlets.  The cost of external transport &nbsp;is 0.75  per space unit</strong></p>
                  <p>&nbsp;</p>"
    End Sub

#End Region

End Class
