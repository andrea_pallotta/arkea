﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Enviroment_Player_BusinessNews.aspx.vb" Inherits="Enviroment_Player_BusinessNews" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="cntMain" ContentPlaceHolderID="MainContent" runat="Server" ClientIDMode="Static">

    <asp:UpdatePanel ID="UpdatePanelMain" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <h2>
                <asp:Label ID="lblWelcome" runat="server" Text="Business news"></asp:Label>
            </h2>

            <div class="clr"></div>

            <h3>
                <asp:Label ID="lblTitolo" runat="server" Text="" Width="100%"></asp:Label>
            </h3>

            <div class="row">
                <asp:Table ClientIDMode="Static" ID="tblProductionCosts" runat="server">
                </asp:Table>
            </div>

            <div class="clr"></div>

            <div class="row" style="min-height: 10px;"></div>

            <div class="row">
                <telerik:RadHtmlChart ID="grfProductionCosts" runat="server" Width="100%">
                    <ChartTitle Text="Cost of production"></ChartTitle>
                </telerik:RadHtmlChart>
            </div>

            <div class="clr"></div>

            <div class="row" style="min-height: 30px;"></div>

            <div class="row">
                <asp:Table ClientIDMode="Static" ID="tblSalaries" runat="server" Style="width: 50%; float: left;">
                </asp:Table>

                <asp:Table ClientIDMode="Static" ID="tblRent" runat="server" Style="float: right; width: 50%; padding-left: 20px;">
                </asp:Table>
            </div>

            <div class="clr"></div>

            <div class="row" style="min-height: 10px;"></div>

            <div class="row">
                <telerik:RadHtmlChart ID="grfCostTrend" runat="server" Width="100%">
                    <ChartTitle Text="Cost trend"></ChartTitle>

                    <PlotArea>
                        <Series>
                            <telerik:LineSeries Name="Rental of a central store" DataFieldY="NewCentrCost">
                                <%--<Appearance FillStyle-BackgroundColor="#B67C00" />--%>
                                <LineAppearance LineStyle="Smooth" Width="3px" />
                                <TooltipsAppearance DataFormatString="{0:2N}" BackgroundColor="White" Color="#B67C00" />
                                <LabelsAppearance Visible="false" />
                            </telerik:LineSeries>

                            <telerik:LineSeries Name="A suburban store rent (/ Current)" DataFieldY="NewStoreCost">
                                <%--<Appearance FillStyle-BackgroundColor="#F1B940" />--%>
                                <LineAppearance LineStyle="Smooth" Width="3px" />
                                <TooltipsAppearance DataFormatString="{0:2N}" BackgroundColor="White" Color="#F1B940" />
                                <LabelsAppearance Visible="false" />
                            </telerik:LineSeries>

                            <telerik:LineSeries Name="Supervisor salaries" DataFieldY="PersoCostStaff">
                                <%--<Appearance FillStyle-BackgroundColor="#936500" />--%>
                                <LineAppearance LineStyle="Smooth" Width="3px" />
                                <TooltipsAppearance DataFormatString="{0:2N}" BackgroundColor="White" Color="#936500" />
                                <LabelsAppearance Visible="false" />
                            </telerik:LineSeries>

                            <telerik:LineSeries Name="Person salaries" DataFieldY="PersoCostPoint">
                                <%--<Appearance FillStyle-BackgroundColor="#E09C0E" />--%>
                                <LineAppearance LineStyle="Smooth" Width="3px" />
                                <TooltipsAppearance DataFormatString="{0:2N}" BackgroundColor="White" Color="#E09C0E" />
                                <LabelsAppearance Visible="false" />
                            </telerik:LineSeries>

                        </Series>

                        <XAxis>
                        </XAxis>

                        <YAxis>
                            <LabelsAppearance DataFormatString="{0}" />
                        </YAxis>

                    </PlotArea>

                    <Legend>
                        <Appearance Visible="true" Position="Top" />
                    </Legend>
                </telerik:RadHtmlChart>
            </div>

            <div class="row">
                <asp:PlaceHolder runat="server" ID="Message" Visible="false">
                    <p class="text-danger">
                        <asp:Literal runat="server" ID="MessageText" />
                    </p>
                </asp:PlaceHolder>
            </div>

            <div class="clr"></div>

            <div class="row">
                <div class="divTableCell" style="text-align: right;">
                    <telerik:RadButton ID="btnTranslate" runat="server" Text="Translate" EnableAjaxSkinRendering="true" ToolTip=""></telerik:RadButton>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
