﻿Imports System.Data
Imports APS.APSUtility
Imports APPCore.Utility
Imports DALC4NET
Imports Telerik.Web.UI

Partial Class Enviroment_Player_BusinessNews
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadData()
        Message.Visible = False

        UpdatePanelMain.Update()
    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadData()

        Message.Visible = False
        UpdatePanelMain.Update()
    End Sub

    Private Sub LoadData()
        tblProductionCosts.Rows.Clear()
        tblSalaries.Rows.Clear()
        tblRent.Rows.Clear()

        LoadDataProductionCosts()
        LoadDataSalaries()
        LoadDataRent()
    End Sub

    Private Sub LoadDataProductionCosts()
        Dim oDTItems As DataTable = HandleGetItemsPeriodVariables(Session("IDGame"), Nz(Session("LanguageActive")))

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Try
            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header vuota
            Dim oLBLTitleGrid As New RadLabel
            oLBLTitleGrid.ID = "lblGridTitle"
            oLBLTitleGrid.Text = "Production costs"
            oLBLTitleGrid.ForeColor = Drawing.ColorTranslator.FromHtml("#525252")
            tblHeaderCell.Controls.Add(oLBLTitleGrid)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.Style.Add("background", "#B4B3B2")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            Dim oLBLPriceGrid As New RadLabel
            tblHeaderCell = New TableHeaderCell
            oLBLPriceGrid.ID = "lblGridCost"
            oLBLPriceGrid.Text = "Cost per unit"
            oLBLPriceGrid.ForeColor = Drawing.Color.White
            tblHeaderCell.Controls.Add(oLBLPriceGrid)
            tblHeaderCell.Style.Add("background", "#525252")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            tblProductionCosts.Rows.Add(tblHeaderRow)

            ' Ciclo sugli item per la costruzione delle righe
            For Each oRowItm As DataRow In oDTItems.Rows
                tblRow = New TableRow
                tblCell = New TableCell
                Dim oLBLItemName As New RadLabel
                oLBLItemName.ID = "lblItem_" & oRowItm("VariableNameDefault")
                oLBLItemName.Text = oRowItm("VariableName")
                oLBLItemName.Style.Add("color", "darkred")
                tblCell.Controls.Add(oLBLItemName)
                tblCell.Style.Add("width", "55%")
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                tblCell.Text = Nn(GetVariableBoss("ProduUnitCost", 0, Session("IDPeriod"), "ProduUnitCost_" & oRowItm("VariableNameDefault"), Session("IDGame"))).ToString("N1")
                tblCell.BorderStyle = BorderStyle.None
                tblCell.Style.Add("text-align", "right")
                tblRow.Cells.Add(tblCell)

                tblProductionCosts.Rows.Add(tblRow)
            Next

            If tblProductionCosts.Rows.Count > 1 Then
                LoadGraphProductionCosts()
                LoadDataGraphProductionTrend()
                grfProductionCosts.Visible = True
                grfCostTrend.Visible = True
            Else
                grfProductionCosts.Visible = False
                grfCostTrend.Visible = False
            End If

        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub LoadGraphProductionCosts()
        Dim sSQL As String

        Dim oDTItems As DataTable = HandleGetItemsPeriodVariables(Session("IDGame"), Nz(Session("LanguageActive")))

        Dim sItemName As String
        Dim dValue As Double = 0

        Dim iTotNumPlayers As Integer = HandleLoadPlayersGame(Session("IDGame")).Rows.Count

        grfProductionCosts.PlotArea.Series.Clear()
        grfProductionCosts.PlotArea.XAxis.Items.Clear()

        ' Recupero tutti i periodi generati fino adesso
        sSQL = "SELECT * FROM BGOL_Games_Periods BGP " _
             & "INNER JOIN BGOL_Periods BP ON BGP.IDPeriodo = BP.Id " _
             & "WHERE BGP.IDGame = " & Session("IDGame") & " AND BP.NumStep < " & Session("CurrentStep")
        If Session("IDRole").Contains("P") Then
            sSQL &= " AND ISNULL(Simulation, 0) = 0"
        End If
        sSQL &= " ORDER BY BP.NumStep "
        Dim oDTPeriodi As DataTable = g_DAL.ExecuteDataTable(sSQL)

        Dim oSeriesData As LineSeries
        For Each oRowItm As DataRow In oDTItems.Rows
            sItemName = oRowItm("VariableName").ToString

            oSeriesData = New LineSeries
            For Each oRowPeriod As DataRow In oDTPeriodi.Rows

                dValue = Nn(GetVariableBoss("ProduUnitCost", 0, oRowPeriod("IDPeriodo"), "ProduUnitCost_" & Nz(oRowItm("VariableNameDefault")), Session("IDGame")))

                If Not IsNothing(dValue) Then
                    Dim oItem As New SeriesItem
                    oItem.YValue = dValue.ToString("N2")
                    oItem.TooltipValue = Nz(oRowItm("VariableName"))
                    oSeriesData.Items.Add(oItem)
                End If
            Next
            grfProductionCosts.PlotArea.Series.Add(oSeriesData)
            oSeriesData.Name = sItemName
            oSeriesData.VisibleInLegend = True
            oSeriesData.LabelsAppearance.Visible = True
        Next

        For Each oRowPeriod As DataRow In oDTPeriodi.Rows
            Dim newAxisItem As New AxisItem(Nz(oRowPeriod("Descrizione")))
            grfProductionCosts.PlotArea.XAxis.Items.Add(newAxisItem)
        Next

        grfProductionCosts.PlotArea.XAxis.MinorGridLines.Visible = False
        grfProductionCosts.PlotArea.YAxis.MinorGridLines.Visible = False
        grfProductionCosts.Legend.Appearance.Visible = True
        grfProductionCosts.Legend.Appearance.Position = HtmlChart.ChartLegendPosition.Top

        grfProductionCosts.Visible = True
    End Sub

    Private Sub LoadDataSalaries()
        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Try
            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header vuota
            Dim oLBLTitleGrid As New RadLabel
            oLBLTitleGrid.ID = "lblGridTitleSalaries"
            oLBLTitleGrid.Text = "Salaries"
            oLBLTitleGrid.ForeColor = Drawing.ColorTranslator.FromHtml("#525252")
            tblHeaderCell.Controls.Add(oLBLTitleGrid)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.Style.Add("background", "#B4B3B2")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            Dim oLBLCol1 As New RadLabel
            tblHeaderCell = New TableHeaderCell
            oLBLCol1.ID = "lblGridCol1"
            oLBLCol1.Text = "Yearly salary"
            oLBLCol1.ForeColor = Drawing.Color.White
            tblHeaderCell.Controls.Add(oLBLCol1)
            tblHeaderCell.Style.Add("background", "#525252")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            tblSalaries.Rows.Add(tblHeaderRow)

            ' Riga SUPERVISORS
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLBLSupervisors As New RadLabel
            oLBLSupervisors.ID = "lblSuperVisors"
            oLBLSupervisors.Text = "Supervisors"
            oLBLSupervisors.Style.Add("color", "darkred")
            tblCell.Controls.Add(oLBLSupervisors)
            tblCell.Style.Add("width", "75%")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            tblCell.Text = Nn(GetVariableBoss("PersoCostStaff", 0, Session("IDPeriod"), "", Session("IDGame"))).ToString("N0")
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblSalaries.Rows.Add(tblRow)

            ' Riga SALES Persons
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLBLSalesPersons As New RadLabel
            oLBLSalesPersons.ID = "lblSalesPersons"
            oLBLSalesPersons.Text = "Sales persons"
            oLBLSalesPersons.Style.Add("color", "darkred")
            tblCell.Controls.Add(oLBLSalesPersons)
            tblCell.Style.Add("width", "75%")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            tblCell.Text = Nn(GetVariableBoss("PersoCostPoint", 0, Session("IDPeriod"), "", Session("IDGame"))).ToString("N0")
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblSalaries.Rows.Add(tblRow)

            If tblProductionCosts.Rows.Count > 1 Then
                LoadGraphProductionCosts()
                grfProductionCosts.Visible = True
                tblRow = New TableRow
                tblCell = New TableCell
                Dim lblSalaryDescription As New RadLabel
                lblSalaryDescription.ID = "lblSalaryDescription"
                lblSalaryDescription.Text = "Currently no shortage of skilled  manufacturing workers in the industry.<br>
                                             Statutory severance pay for  workers released early from employment contract and down-sizing head-count 
                                             in retail business is limited to 10% per quarter.<br>
                                             Training in this industry is  optional."
                tblCell.Controls.Add(lblSalaryDescription)
                tblCell.BorderStyle = BorderStyle.None
                tblCell.Style.Add("text-align", "left")
                tblCell.ColumnSpan = 2
                tblRow.Cells.Add(tblCell)
                tblSalaries.Rows.Add(tblRow)
            Else
                grfProductionCosts.Visible = False
            End If

        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub LoadDataRent()
        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Try
            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header vuota
            Dim oLBLTitleGrid As New RadLabel
            oLBLTitleGrid.ID = "lblGridTitleRent"
            oLBLTitleGrid.Text = "Rent"
            oLBLTitleGrid.ForeColor = Drawing.ColorTranslator.FromHtml("#525252")
            tblHeaderCell.Controls.Add(oLBLTitleGrid)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.Style.Add("background", "#B4B3B2")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            Dim oLBLCol1 As New RadLabel
            tblHeaderCell = New TableHeaderCell
            oLBLCol1.ID = "lblGridYearlyRent"
            oLBLCol1.Text = "Yearly rent"
            oLBLCol1.ForeColor = Drawing.Color.White
            tblHeaderCell.Controls.Add(oLBLCol1)
            tblHeaderCell.Style.Add("background", "#525252")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            tblRent.Rows.Add(tblHeaderRow)

            ' Riga CENTRAL SHOPS
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLBLCentralShops As New RadLabel
            oLBLCentralShops.ID = "lblCentralShops"
            oLBLCentralShops.Text = "Central shops"
            oLBLCentralShops.Style.Add("color", "darkred")
            tblCell.Controls.Add(oLBLCentralShops)
            tblCell.Style.Add("width", "75%")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            tblCell.Text = Nn(GetVariableBoss("NewCentrCost", 0, Session("IDPeriod"), "", Session("IDGame"))).ToString("N0")
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblRent.Rows.Add(tblRow)

            ' Riga OUT OF TOWN SHOPS
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLBLSalesPersons As New RadLabel
            oLBLSalesPersons.ID = "lblOuOfTownShops"
            oLBLSalesPersons.Text = "Out of town shops"
            oLBLSalesPersons.Style.Add("color", "darkred")
            tblCell.Controls.Add(oLBLSalesPersons)
            tblCell.Style.Add("width", "75%")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            tblCell.Text = Nn(GetVariableBoss("NewStoreCost", 0, Session("IDPeriod"), "", Session("IDGame"))).ToString("N0")
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblRent.Rows.Add(tblRow)

        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub LoadDataGraphProductionTrend()
        Dim sSQL As String
        Dim oDTTableGraph As New DataTable

        Dim dValue As Double = 0

        ' Recupero tutti i periodi generati fino adesso
        sSQL = "SELECT * FROM BGOL_Games_Periods BGP " _
             & "INNER JOIN BGOL_Periods BP ON BGP.IDPeriodo = BP.Id " _
             & "WHERE BGP.IDGame = " & Session("IDGame") & " AND BP.NumStep <= " & Session("CurrentStep")
        If Session("IDRole").Contains("P") Then
            sSQL &= " AND ISNULL(Simulation, 0) = 0"
        End If
        sSQL &= " ORDER BY BP.NumStep "

        Dim oDTPeriodi As DataTable = g_DAL.ExecuteDataTable(sSQL)

        '' Preparo il datatable che ospiterà i dati
        oDTTableGraph.Columns.Add("NewCentrCost", GetType(Double))
        oDTTableGraph.Columns.Add("NewStoreCost", GetType(Double))
        oDTTableGraph.Columns.Add("PersoCostStaff", GetType(Double))
        oDTTableGraph.Columns.Add("PersoCostPoint", GetType(Double))
        oDTTableGraph.Columns.Add("Period", GetType(String))

        For Each oRowPeriod As DataRow In oDTPeriodi.Rows
            oDTTableGraph.Rows.Add(Nn(GetVariableBoss("NewCentrCost", 0, oRowPeriod("IDPeriodo"), "", Session("IDGame"))).ToString("N2"),
                                   Nn(GetVariableBoss("NewStoreCost", 0, oRowPeriod("IDPeriodo"), "", Session("IDGame"))).ToString("N2"),
                                   Nn(GetVariableBoss("PersoCostStaff", 0, oRowPeriod("IDPeriodo"), "", Session("IDGame"))).ToString("N2"),
                                   Nn(GetVariableBoss("PersoCostPoint", 0, oRowPeriod("IDPeriodo"), "", Session("IDGame"))),
                                   Nz(oRowPeriod("Descrizione")))
        Next

        grfCostTrend.DataSource = oDTTableGraph
        grfCostTrend.DataBind()

        grfCostTrend.PlotArea.XAxis.DataLabelsField = "Period"
        grfCostTrend.PlotArea.XAxis.LabelsAppearance.TextStyle.Bold = True

        grfCostTrend.PlotArea.XAxis.LabelsAppearance.RotationAngle = 45
        grfCostTrend.PlotArea.XAxis.EnableBaseUnitStepAuto = True

        grfCostTrend.Visible = True
    End Sub

    Private Sub Enviroment_Player_BusinessNews_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        m_SessionData = Session("SessionData")

        m_SiteMaster = Me.Master

        m_SiteMaster.LoadPeriodsValid = Session("IDRole") = "P"

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oCBOPeriod As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOPeriodValid As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriodValid"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler m_SiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler m_SiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(Session("IDGame")) ' Ultimo periodo
        oCBOPeriod.SelectedValue = Session("IDPeriodValid")
        m_SiteMaster.PeriodChange = Session("IDPeriodValid")

        Session("NameTeam") = GetTeamName(Session("IDTeam"))
        oCBOTeam.SelectedValue = Session("IDTeam")
        m_SiteMaster.TeamChange = Session("IDTeam")

        SQLConnClose(g_DAL.GetConnObject)
    End Sub

    Private Sub Enviroment_Player_BusinessNews_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        If Not Page.IsPostBack Then
            If Session("IDRole") = "P" Then
                m_SiteMaster.HandleReloadPeriods(HandleGetMaxPeriodValid(Session("IDGame")))
            End If
            LoadData()

            btnTranslate.Visible = Session("IDRole").Contains("D")
            ' Controllo tutti gli oggetti presenti
            Dim oTranslation As New TranslationObjectForm(Me, g_DAL, Nz(Session("LanguageActive")))

        End If
    End Sub

    Private Sub btnTranslate_Click(sender As Object, e As EventArgs) Handles btnTranslate.Click
        Session("PageOrigin") = Me.ToString().Substring(4, Me.ToString().Substring(4).Length - 5) + ".aspx"
        Response.Redirect("PageTranslation.aspx")
        Response.End()
    End Sub

End Class
