﻿Imports System.Data
Imports APS.APSUtility
Imports APPCore.Utility
Imports DALC4NET
Imports Telerik.Web.UI

Partial Class Enviroment_Player_CustomerPerception
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadData()
        Message.Visible = False

        UpdatePanelMain.Update()
    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadData()

        Message.Visible = False
        UpdatePanelMain.Update()
    End Sub

    Private Sub LoadData()
        tblCompetitiveFactors.Rows.Clear()

        LoadDataCompetitiveFactors()
        LoadDataSensitivityBrandFactors()
    End Sub

    Private Sub LoadDataCompetitiveFactors()
        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        ' Recupero il periodo successivo 
        Dim iIDPeriodoNext As Integer = HandleGetMaxPeriodValidNext(Session("IDGame"), m_SiteMaster.PeriodGetCurrent)

        Try
            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header  
            Dim oLBLGridTitleCompetitiveFactors As New RadLabel
            oLBLGridTitleCompetitiveFactors.ID = "lblGridTitleCompetitiveFactors"
            oLBLGridTitleCompetitiveFactors.Text = "Competitiveness factors"
            oLBLGridTitleCompetitiveFactors.ForeColor = Drawing.Color.White
            tblHeaderCell.Controls.Add(oLBLGridTitleCompetitiveFactors)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.ColumnSpan = 2
            tblHeaderCell.Style.Add("background", "#525252")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            tblCompetitiveFactors.Rows.Add(tblHeaderRow)

            ' Ciclo sugli item per la costruzione delle righe

            tblRow = New TableRow
            tblCell = New TableCell

            ' QUALITY
            Dim oLBLQuality As New RadLabel
            oLBLQuality.ID = "lblQuality"
            oLBLQuality.Text = "Quality"
            oLBLQuality.Style.Add("color", "darkred")
            tblCell.Controls.Add(oLBLQuality)
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            tblCell.Text = (Nn(GetVariableBoss("ImporCompoQuali", 0, iIDPeriodoNext, "", Session("IDGame"))) * 10).ToString("N0")
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblCompetitiveFactors.Rows.Add(tblRow)

            ' PRICE
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLBLPrice As New RadLabel
            oLBLPrice.ID = "lblPrice"
            oLBLPrice.Text = "Price"
            oLBLPrice.Style.Add("color", "darkred")
            tblCell.Controls.Add(oLBLPrice)
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            tblCell.Text = (Nn(GetVariableBoss("ImporCompoPrice", 0, iIDPeriodoNext, "", Session("IDGame"))) * 10).ToString("N0")
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblCompetitiveFactors.Rows.Add(tblRow)

            ' BRAND
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLBLBrand As New RadLabel
            oLBLBrand.ID = "lblBrand"
            oLBLBrand.Text = "Brand"
            oLBLBrand.Style.Add("color", "darkred")
            tblCell.Controls.Add(oLBLBrand)
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            tblCell.Text = (Nn(GetVariableBoss("ImporCompoAttra", 0, iIDPeriodoNext, "", Session("IDGame"))) * 10).ToString("N0")
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblCompetitiveFactors.Rows.Add(tblRow)

            LoadDataGraphCompetitiveFactors()
        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub LoadDataGraphCompetitiveFactors()
        Dim sSQL As String
        Dim oDTTableGraph As New DataTable

        Dim dValue As Double = 0

        ' Recupero tutti i periodi generati fino adesso
        sSQL = "SELECT * FROM BGOL_Games_Periods BGP " _
             & "INNER JOIN BGOL_Periods BP ON BGP.IDPeriodo = BP.Id " _
             & "WHERE BGP.IDGame = " & Session("IDGame") & " AND BP.NumStep <= " & Session("CurrentStep")
        If Session("IDRole").Contains("P") Then
            sSQL &= " AND ISNULL(Simulation, 0) = 0"
        End If
        sSQL &= " ORDER BY BP.NumStep "

        Dim oDTPeriodi As DataTable = g_DAL.ExecuteDataTable(sSQL)

        '' Preparo il datatable che ospiterà i dati
        oDTTableGraph.Columns.Add("Quality", GetType(Double))
        oDTTableGraph.Columns.Add("Price", GetType(Double))
        oDTTableGraph.Columns.Add("Brand", GetType(Double))
        oDTTableGraph.Columns.Add("Period", GetType(String))

        For Each oRowPeriod As DataRow In oDTPeriodi.Rows
            oDTTableGraph.Rows.Add(Nn(GetVariableBoss("ImporCompoQuali", 0, oRowPeriod("IDPeriodo"), "", Session("IDGame"))).ToString("N2"),
                                   Nn(GetVariableBoss("ImporCompoPrice", 0, oRowPeriod("IDPeriodo"), "", Session("IDGame"))).ToString("N2"),
                                   Nn(GetVariableBoss("ImporCompoAttra", 0, oRowPeriod("IDPeriodo"), "", Session("IDGame"))).ToString("N2"),
                                   Nz(oRowPeriod("Descrizione")))
        Next

        grfCompetitiveFactors.DataSource = oDTTableGraph
        grfCompetitiveFactors.DataBind()

        grfCompetitiveFactors.PlotArea.XAxis.DataLabelsField = "Period"
        grfCompetitiveFactors.PlotArea.XAxis.LabelsAppearance.TextStyle.Bold = True

        grfCompetitiveFactors.PlotArea.XAxis.LabelsAppearance.RotationAngle = 45
        grfCompetitiveFactors.PlotArea.XAxis.EnableBaseUnitStepAuto = True

        grfCompetitiveFactors.Visible = True
    End Sub

    Private Sub LoadDataSensitivityBrandFactors()
        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        ' Recupero il periodo successivo 
        Dim iIDPeriodoNext As Integer = HandleGetMaxPeriodValidNext(Session("IDGame"), m_SiteMaster.PeriodGetCurrent)

        Try
            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header  
            Dim oLBLGridTitleBrandFactors As New RadLabel
            oLBLGridTitleBrandFactors.ID = "lblGridTitleBrandFactors"
            oLBLGridTitleBrandFactors.Text = "Brand factors"
            oLBLGridTitleBrandFactors.ForeColor = Drawing.Color.White
            tblHeaderCell.Controls.Add(oLBLGridTitleBrandFactors)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.ColumnSpan = 2
            tblHeaderCell.Style.Add("background", "#525252")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            tblSensitivityBrandFactors.Rows.Add(tblHeaderRow)

            tblRow = New TableRow
            tblCell = New TableCell

            ' LOCATION
            Dim lblLocation As New RadLabel
            lblLocation.ID = "lblLocation"
            lblLocation.Text = "Location"
            lblLocation.Style.Add("color", "darkred")
            tblCell.Controls.Add(lblLocation)
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            tblCell.Text = (Nn(GetVariableBoss("ImporNumbeStore", 0, iIDPeriodoNext, "", Session("IDGame"))) * 10).ToString("N0")
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)
            tblSensitivityBrandFactors.Rows.Add(tblRow)

            ' ADVERTISING
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblAdvertising As New RadLabel
            lblAdvertising.ID = "lblAdvertising"
            lblAdvertising.Text = "Advertising"
            lblAdvertising.Style.Add("color", "darkred")
            tblCell.Controls.Add(lblAdvertising)
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            tblCell.Text = (Nn(GetVariableBoss("ImporAdverQuali", 0, iIDPeriodoNext, "", Session("IDGame"))) * 10).ToString("N0")
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblSensitivityBrandFactors.Rows.Add(tblRow)

            ' CUSTOMER SERVICE
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblCustomerService As New RadLabel
            lblCustomerService.ID = "lblCustomerService"
            lblCustomerService.Text = "Customer service"
            lblCustomerService.Style.Add("color", "darkred")
            tblCell.Controls.Add(lblCustomerService)
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            tblCell.Text = (Nn(GetVariableBoss("ImporTrainQuali", 0, iIDPeriodoNext, "", Session("IDGame"))) * 10).ToString("N0")
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblSensitivityBrandFactors.Rows.Add(tblRow)

            ' COMAPANY IMAGE
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblCompanyImage As New RadLabel
            lblCompanyImage.ID = "lblCompanyImage"
            lblCompanyImage.Text = "Company image"
            lblCompanyImage.Style.Add("color", "darkred")
            tblCell.Controls.Add(lblCompanyImage)
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            tblCell.Text = (Nn(GetVariableBoss("ImporPreviShare", 0, iIDPeriodoNext, "", Session("IDGame"))) * 10).ToString("N0")
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblSensitivityBrandFactors.Rows.Add(tblRow)

            LoadDataGraphSensitivityBrandFactors()
        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub LoadDataGraphSensitivityBrandFactors()
        Dim sSQL As String
        Dim oDTTableGraph As New DataTable

        Dim dValue As Double = 0

        ' Recupero tutti i periodi generati fino adesso
        sSQL = "SELECT * FROM BGOL_Games_Periods BGP " _
             & "INNER JOIN BGOL_Periods BP ON BGP.IDPeriodo = BP.Id " _
             & "WHERE BGP.IDGame = " & Session("IDGame") & " AND BP.NumStep <= " & Session("CurrentStep")
        If Session("IDRole").Contains("P") Then
            sSQL &= " AND ISNULL(Simulation, 0) = 0"
        End If
        sSQL &= " ORDER BY BP.NumStep "

        Dim oDTPeriodi As DataTable = g_DAL.ExecuteDataTable(sSQL)

        '' Preparo il datatable che ospiterà i dati
        oDTTableGraph.Columns.Add("ImporNumbeStore", GetType(Double))
        oDTTableGraph.Columns.Add("ImporAdverQuali", GetType(Double))
        oDTTableGraph.Columns.Add("ImporTrainQuali", GetType(Double))
        oDTTableGraph.Columns.Add("ImporPreviShare", GetType(Double))
        oDTTableGraph.Columns.Add("Period", GetType(String))

        For Each oRowPeriod As DataRow In oDTPeriodi.Rows
            oDTTableGraph.Rows.Add(Nn(GetVariableBoss("ImporNumbeStore", 0, oRowPeriod("IDPeriodo"), "", Session("IDGame"))).ToString("N2"),
                                   Nn(GetVariableBoss("ImporAdverQuali", 0, oRowPeriod("IDPeriodo"), "", Session("IDGame"))).ToString("N2"),
                                   Nn(GetVariableBoss("ImporTrainQuali", 0, oRowPeriod("IDPeriodo"), "", Session("IDGame"))).ToString("N2"),
                                   Nn(GetVariableBoss("ImporPreviShare", 0, oRowPeriod("IDPeriodo"), "", Session("IDGame"))).ToString("N2"),
                                   Nz(oRowPeriod("Descrizione")))
        Next

        grfBrand.DataSource = oDTTableGraph
        grfBrand.DataBind()

        grfBrand.PlotArea.XAxis.DataLabelsField = "Period"
        grfBrand.PlotArea.XAxis.LabelsAppearance.TextStyle.Bold = True

        grfBrand.PlotArea.XAxis.LabelsAppearance.RotationAngle = 45
        grfBrand.PlotArea.XAxis.EnableBaseUnitStepAuto = True

        grfBrand.Visible = True
    End Sub

    Private Sub Enviroment_Player_CustomerPerception_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        m_SessionData = Session("SessionData")

        m_SiteMaster = Me.Master

        m_SiteMaster.LoadPeriodsValid = Session("IDRole") = "P"

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oCBOPeriod As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOPeriodValid As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriodValid"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler m_SiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler m_SiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(Session("IDGame")) ' Ultimo periodo
        oCBOPeriod.SelectedValue = Session("IDPeriodValid")
        m_SiteMaster.PeriodChange = Session("IDPeriodValid")

        Session("NameTeam") = GetTeamName(Session("IDTeam"))
        oCBOTeam.SelectedValue = Session("IDTeam")
        m_SiteMaster.TeamChange = Session("IDTeam")

        ' Gestione del pannello delle decisioni concesse
        modalHelp.OpenerElementID = lnkHelp.ClientID
        modalHelp.Modal = False
        modalHelp.VisibleTitlebar = True

        LoadHelp()

    End Sub

    Private Sub Enviroment_Player_CustomerPerception_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        If Not Page.IsPostBack Then
            If Session("IDRole") = "P" Then
                m_SiteMaster.HandleReloadPeriods(HandleGetMaxPeriodValid(Session("IDGame")))
            End If
            LoadData()

            btnTranslate.Visible = Session("IDRole").Contains("D")

        End If

        ' Controllo tutti gli oggetti presenti
        Dim oTranslation As New TranslationObjectForm(Me, g_DAL, Nz(Session("LanguageActive")))

    End Sub

    Private Sub btnTranslate_Click(sender As Object, e As EventArgs) Handles btnTranslate.Click
        Session("PageOrigin") = Me.ToString().Substring(4, Me.ToString().Substring(4).Length - 5) + ".aspx"
        Response.Redirect("PageTranslation.aspx")
        Response.End()
    End Sub

#Region "PANNELLO HELP"

    Private Sub LoadHelp()
        lblHelp.Text = "<p><strong>Critical factor of attraction</strong> (brand)<br>
                    There are four different factors that affect the index of attraction which <br>
in turn affects a companys&amp;#8217; capacity to attract customers. High <br>
values of each of these factors will have a higher impact on the customers.</p>
                    <p>&nbsp;</p>
                    <p><strong>Critical factor of competitiveness </strong><br>
                      There are three different factors that affect the competitive index which <br>
in turn affects company image. High values of each of these factors will <br>
have a higher impact on the customers.</p>"
    End Sub

#End Region


End Class

