﻿Imports System.Data
Imports APS.APSUtility
Imports APPCore.Utility
Imports DALC4NET
Imports Telerik.Web.UI

Partial Class Enviroment_Player_GoodsNews
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster
    Private m_IDItem As Integer = 1

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadData()
        Message.Visible = False

        UpdatePanelMain.Update()
    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadData()

        Message.Visible = False
        UpdatePanelMain.Update()
    End Sub

    Private Sub LoadData()
        tblImportedRawMaterial.Rows.Clear()
        tblLocalRowMaterial.Rows.Clear()
        tblBoughtIn.Rows.Clear()

        lblTitolo.BackColor = Drawing.ColorTranslator.FromHtml("#e7e7e7")
        lblTitolo2.BackColor = Drawing.ColorTranslator.FromHtml("#e7e7e7")

        LoadDataLocalRawMaterial()
        LoadDataImportedRawMaterial()
        LoadDataBoughtIn()
    End Sub

    Private Sub LoadDataLocalRawMaterial()
        Dim oDTItems As DataTable = HandleGetItemsPeriodVariables(Session("IDGame"), Nz(Session("LanguageActive")))

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        ' Recupero il periodo successivo per decretare il trend
        Dim iIDPeriodoNext As Integer = HandleGetMaxPeriodValidNext(Session("IDGame"), m_SiteMaster.PeriodGetCurrent)
        Dim iIDPeriodLastYear As Integer = GetLastYearPeriod(Session("IDGame"), Session("CurrentStep"), Session("LunghezzaPeriodo"))

        Dim dTrendNext As Double
        Dim dTrendOld As Double

        Try
            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header vuota
            Dim lblGridTitle As New RadLabel
            lblGridTitle.ID = "lblGridTitleLocal"
            lblGridTitle.Text = "Local raw material"
            lblGridTitle.ForeColor = Drawing.ColorTranslator.FromHtml("#525252")
            tblHeaderCell.Controls.Add(lblGridTitle)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.Style.Add("background", "#B4B3B2")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            Dim lblGridPrice As New RadLabel
            tblHeaderCell = New TableHeaderCell
            lblGridPrice.ID = "lblGridPrice"
            lblGridPrice.Text = "Price"
            lblGridPrice.ForeColor = Drawing.Color.White
            tblHeaderCell.Controls.Add(lblGridPrice)
            tblHeaderCell.Style.Add("background", "#525252")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            Dim lblGridQuality As New RadLabel
            tblHeaderCell = New TableHeaderCell
            lblGridQuality.ID = "lblGridQuality"
            lblGridQuality.Text = "Quality"
            lblGridQuality.ForeColor = Drawing.Color.White
            tblHeaderCell.Controls.Add(lblGridQuality)
            tblHeaderCell.Style.Add("background", "#525252")
            tblHeaderRow.BorderStyle = BorderStyle.None
            tblHeaderRow.Style.Add("margin-top", "10px")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            tblLocalRowMaterial.Rows.Add(tblHeaderRow)

            ' Ciclo sugli item per la costruzione delle righe
            For Each oRowItm As DataRow In oDTItems.Rows
                tblRow = New TableRow
                tblCell = New TableCell
                Dim lblItemName As New RadLabel
                lblItemName.ID = "lblItem_" & oRowItm("VariableNameDefault")
                lblItemName.Text = oRowItm("VariableName")
                lblItemName.Style.Add("color", "darkred")
                tblCell.Controls.Add(lblItemName)
                tblCell.Style.Add("width", "55%")
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                tblCell.Text = Nn(GetVariableBoss("RawEuropCost", 0, Session("IDPeriod"), "RawEuropCost_" & oRowItm("VariableNameDefault"), Session("IDGame"))).ToString("N1")
                tblCell.BorderStyle = BorderStyle.None
                tblCell.Style.Add("text-align", "right")
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                tblCell.Text = Nn(GetVariableBoss("RawEuropQuali", 0, Session("IDPeriod"), "RawEuropQuali_" & oRowItm("VariableNameDefault"), Session("IDGame"))).ToString("N1")
                tblCell.BorderStyle = BorderStyle.None
                tblCell.Style.Add("text-align", "right")
                tblRow.Cells.Add(tblCell)

                tblLocalRowMaterial.Rows.Add(tblRow)
            Next

            ' Riga vuota
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblemptyRow As New RadLabel
            lblemptyRow.ID = "lblItem_Empty"
            lblemptyRow.Text = ""
            lblemptyRow.Style.Add("color", "darkred")
            tblCell.Controls.Add(lblemptyRow)
            tblCell.ColumnSpan = 3
            tblCell.Style.Add("min-height", "20px")
            tblRow.Cells.Add(tblCell)

            ' Aggiungo i riferimenti dei periodi precedenti
            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header vuota
            Dim lblGridSubTitleLocal As New RadLabel
            lblGridSubTitleLocal.ID = "lblGridSubTitleLocal"
            lblGridSubTitleLocal.Text = ""
            lblGridSubTitleLocal.ForeColor = Drawing.Color.Transparent
            tblHeaderCell.Controls.Add(lblGridSubTitleLocal)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.Style.Add("background", "#ffffff")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            Dim lblPriceGridPreviousQ As New RadLabel
            tblHeaderCell = New TableHeaderCell
            lblPriceGridPreviousQ.ID = "lblPriceGridPreviousQ"
            lblPriceGridPreviousQ.Text = "Price trend on previous Q"
            lblPriceGridPreviousQ.ForeColor = Drawing.Color.White
            tblHeaderCell.Controls.Add(lblPriceGridPreviousQ)
            tblHeaderCell.Style.Add("background", "#525252")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            Dim lblTrendLastYear As New RadLabel
            tblHeaderCell = New TableHeaderCell
            lblTrendLastYear.ID = "lblTrendLastYear"
            lblTrendLastYear.Text = "Trend on last year"
            lblTrendLastYear.ForeColor = Drawing.Color.White
            tblHeaderCell.Controls.Add(lblTrendLastYear)
            tblHeaderCell.Style.Add("background", "#525252")
            tblHeaderRow.BorderStyle = BorderStyle.None
            tblHeaderRow.Style.Add("margin-top", "10px")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            tblLocalRowMaterial.Rows.Add(tblHeaderRow)

            ' Ciclo sugli item per la costruzione delle righe
            For Each oRowItm As DataRow In oDTItems.Rows
                tblRow = New TableRow
                tblCell = New TableCell
                Dim lblItemNameQ As New RadLabel
                lblItemNameQ.ID = "lblItem_" & oRowItm("VariableNameDefault")
                lblItemNameQ.Text = oRowItm("VariableName")
                lblItemNameQ.Style.Add("color", "darkred")
                tblCell.Controls.Add(lblItemNameQ)
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                dTrendNext = Nn(GetVariableBoss("RawEuropCost", 0, m_SiteMaster.PeriodGetCurrent, "RawEuropCost_" & oRowItm("VariableNameDefault"), Session("IDGame"))).ToString("N1")
                dTrendOld = Nn(GetVariableBoss("RawEuropCost", 0, m_SiteMaster.PeriodGetPrev, "RawEuropCost_" & oRowItm("VariableNameDefault"), Session("IDGame"))).ToString("N1")

                If dTrendNext > dTrendOld Then
                    tblCell.Text = "On increase"
                ElseIf dTrendNext = dTrendOld Then
                    tblCell.Text = "Steady"
                Else
                    tblCell.Text = "On decrease"
                End If
                tblCell.BorderStyle = BorderStyle.None
                tblCell.Style.Add("text-align", "right")
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                dTrendNext = Nn(GetVariableBoss("RawEuropCost", 0, m_SiteMaster.PeriodGetCurrent, "RawEuropCost_" & oRowItm("VariableNameDefault"), Session("IDGame"))).ToString("N1")
                dTrendOld = Nn(GetVariableBoss("RawEuropCost", 0, iIDPeriodLastYear, "RawEuropCost_" & oRowItm("VariableNameDefault"), Session("IDGame"))).ToString("N1")

                If dTrendNext > dTrendOld Then
                    tblCell.Text = "On increase"
                ElseIf dTrendNext = dTrendOld Then
                    tblCell.Text = "Steady"
                Else
                    tblCell.Text = "On decrease"
                End If
                tblCell.BorderStyle = BorderStyle.None
                tblCell.Style.Add("text-align", "right")
                tblRow.Cells.Add(tblCell)

                tblLocalRowMaterial.Rows.Add(tblRow)
            Next
        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub LoadDataImportedRawMaterial()
        Dim oDTItems As DataTable = HandleGetItemsPeriodVariables(Session("IDGame"), Nz(Session("LanguageActive")))

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        ' Recupero il periodo successivo per decretare il trend
        Dim iIDPeriodoNext As Integer = HandleGetMaxPeriodValidNext(Session("IDGame"), m_SiteMaster.PeriodGetCurrent)
        Dim iIDPeriodLastYear As Integer = GetLastYearPeriod(Session("IDGame"), Session("CurrentStep"), Session("LunghezzaPeriodo"))

        Dim dTrendNext As Double
        Dim dTrendOld As Double

        Try
            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header vuota
            Dim lblGridTitleImported As New RadLabel
            lblGridTitleImported.ID = "lblGridTitleImported"
            lblGridTitleImported.Text = "Local raw material"
            lblGridTitleImported.ForeColor = Drawing.ColorTranslator.FromHtml("#525252")
            tblHeaderCell.Controls.Add(lblGridTitleImported)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.Style.Add("background", "#B4B3B2")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            Dim lblGridPriceImported As New RadLabel
            tblHeaderCell = New TableHeaderCell
            lblGridPriceImported.ID = "lblGridPriceImported"
            lblGridPriceImported.Text = "Price"
            lblGridPriceImported.ForeColor = Drawing.Color.White
            tblHeaderCell.Controls.Add(lblGridPriceImported)
            tblHeaderCell.Style.Add("background", "#525252")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            Dim lblGridQualityImported As New RadLabel
            tblHeaderCell = New TableHeaderCell
            lblGridQualityImported.ID = "lblGridQualityImported"
            lblGridQualityImported.Text = "Quality"
            lblGridQualityImported.ForeColor = Drawing.Color.White
            tblHeaderCell.Controls.Add(lblGridQualityImported)
            tblHeaderCell.Style.Add("background", "#525252")
            tblHeaderRow.BorderStyle = BorderStyle.None
            tblHeaderRow.Style.Add("margin-top", "10px")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            tblImportedRawMaterial.Rows.Add(tblHeaderRow)

            ' Ciclo sugli item per la costruzione delle righe
            For Each oRowItm As DataRow In oDTItems.Rows
                tblRow = New TableRow
                tblCell = New TableCell
                Dim lblItemNameImported As New RadLabel
                lblItemNameImported.ID = "lblItemImported_" & oRowItm("VariableNameDefault")
                lblItemNameImported.Text = oRowItm("VariableName")
                lblItemNameImported.Style.Add("color", "darkred")
                tblCell.Controls.Add(lblItemNameImported)
                tblCell.Style.Add("width", "55%")
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                tblCell.Text = Nn(GetVariableBoss("RawNICCost", 0, Session("IDPeriod"), "RawNICCost_" & oRowItm("VariableNameDefault"), Session("IDGame"))).ToString("N1")
                tblCell.BorderStyle = BorderStyle.None
                tblCell.Style.Add("text-align", "right")
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                tblCell.Text = Nn(GetVariableBoss("RawNICQuali", 0, Session("IDPeriod"), "RawNICQuali_" & oRowItm("VariableNameDefault"), Session("IDGame"))).ToString("N1")
                tblCell.BorderStyle = BorderStyle.None
                tblCell.Style.Add("text-align", "right")
                tblRow.Cells.Add(tblCell)

                tblImportedRawMaterial.Rows.Add(tblRow)
            Next

            ' Riga vuota
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLBLemptyRow As New RadLabel
            oLBLemptyRow.ID = "lblItem_Empty"
            oLBLemptyRow.Text = ""
            oLBLemptyRow.Style.Add("color", "darkred")
            tblCell.Controls.Add(oLBLemptyRow)
            tblCell.ColumnSpan = 3
            tblCell.Style.Add("min-height", "20px")
            tblRow.Cells.Add(tblCell)

            ' Aggiungo i riferimenti dei periodi precedenti
            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header vuota
            Dim lblGridSubTitleImported As New RadLabel
            lblGridSubTitleImported.ID = "lblGridSubTitleImported"
            lblGridSubTitleImported.Text = ""
            lblGridSubTitleImported.ForeColor = Drawing.Color.Transparent
            tblHeaderCell.Controls.Add(lblGridSubTitleImported)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.Style.Add("background", "#ffffff")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            Dim lblPriceGridPreviousQImported As New RadLabel
            tblHeaderCell = New TableHeaderCell
            lblPriceGridPreviousQImported.ID = "lblPriceGridPreviousQImported"
            lblPriceGridPreviousQImported.Text = "Price trend on previous Q"
            lblPriceGridPreviousQImported.ForeColor = Drawing.Color.White
            tblHeaderCell.Controls.Add(lblPriceGridPreviousQImported)
            tblHeaderCell.Style.Add("background", "#525252")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            Dim lblTrendLastYearImported As New RadLabel
            tblHeaderCell = New TableHeaderCell
            lblTrendLastYearImported.ID = "lblTrendLastYearImported"
            lblTrendLastYearImported.Text = "Trend on last year"
            lblTrendLastYearImported.ForeColor = Drawing.Color.White
            tblHeaderCell.Controls.Add(lblTrendLastYearImported)
            tblHeaderCell.Style.Add("background", "#525252")
            tblHeaderRow.BorderStyle = BorderStyle.None
            tblHeaderRow.Style.Add("margin-top", "10px")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            tblImportedRawMaterial.Rows.Add(tblHeaderRow)

            ' Ciclo sugli item per la costruzione delle righe
            For Each oRowItm As DataRow In oDTItems.Rows
                tblRow = New TableRow
                tblCell = New TableCell
                Dim lblItemNameQImported As New RadLabel
                lblItemNameQImported.ID = "lblItemIMported_" & oRowItm("VariableNameDefault")
                lblItemNameQImported.Text = oRowItm("VariableName")
                lblItemNameQImported.Style.Add("color", "darkred")
                tblCell.Controls.Add(lblItemNameQImported)
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                dTrendNext = Nn(GetVariableBoss("RawNICCost", 0, iIDPeriodoNext, "RawNICCost_" & oRowItm("VariableNameDefault"), Session("IDGame"))).ToString("N1")
                dTrendOld = Nn(GetVariableBoss("RawNICCost", 0, m_SiteMaster.PeriodGetPrev, "RawNICCost_" & oRowItm("VariableNameDefault"), Session("IDGame"))).ToString("N1")

                If dTrendNext > dTrendOld Then
                    tblCell.Text = "On increase"
                ElseIf dTrendNext = dTrendOld Then
                    tblCell.Text = "Steady"
                Else
                    tblCell.Text = "On decrease"
                End If
                tblCell.BorderStyle = BorderStyle.None
                tblCell.Style.Add("text-align", "right")
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                dTrendNext = Nn(GetVariableBoss("RawNICCost", 0, iIDPeriodoNext, "RawNICCost_" & oRowItm("VariableNameDefault"), Session("IDGame"))).ToString("N1")
                dTrendOld = Nn(GetVariableBoss("RawNICCost", 0, iIDPeriodLastYear, "RawNICCost_" & oRowItm("VariableNameDefault"), Session("IDGame"))).ToString("N1")

                If dTrendNext > dTrendOld Then
                    tblCell.Text = "On increase"
                ElseIf dTrendNext = dTrendOld Then
                    tblCell.Text = "Steady"
                Else
                    tblCell.Text = "On decrease"
                End If
                tblCell.BorderStyle = BorderStyle.None
                tblCell.Style.Add("text-align", "right")
                tblRow.Cells.Add(tblCell)

                tblImportedRawMaterial.Rows.Add(tblRow)
            Next
        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub LoadDataBoughtIn()
        Dim oDTItems As DataTable = HandleGetItemsPeriodVariables(Session("IDGame"), Nz(Session("LanguageActive")))

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Try
            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header vuota
            Dim oLBLTitleGrid As New RadLabel
            oLBLTitleGrid.ID = "lblGridTitleBoughtIn"
            oLBLTitleGrid.Text = "Bought-in"
            oLBLTitleGrid.ForeColor = Drawing.ColorTranslator.FromHtml("#525252")
            tblHeaderCell.Controls.Add(oLBLTitleGrid)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.Style.Add("background", "#B4B3B2")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            Dim oLBLPriceGrid As New RadLabel
            tblHeaderCell = New TableHeaderCell
            oLBLPriceGrid.ID = "lblGridPriceBoughtIn"
            oLBLPriceGrid.Text = "Price"
            oLBLPriceGrid.ForeColor = Drawing.Color.White
            tblHeaderCell.Controls.Add(oLBLPriceGrid)
            tblHeaderCell.Style.Add("background", "#525252")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            Dim oLBLPriceQuality As New RadLabel
            tblHeaderCell = New TableHeaderCell
            oLBLPriceQuality.ID = "lblGridQualityBoughtIn"
            oLBLPriceQuality.Text = "Quality"
            oLBLPriceQuality.ForeColor = Drawing.Color.White
            tblHeaderCell.Controls.Add(oLBLPriceQuality)
            tblHeaderCell.Style.Add("background", "#525252")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            Dim oLBLDiscQuantity As New RadLabel
            tblHeaderCell = New TableHeaderCell
            oLBLDiscQuantity.ID = "lblGridDiscQuantity"
            oLBLDiscQuantity.Text = "Min. discounted quantity" & "<br/>" & "(discount threshold)"
            oLBLDiscQuantity.ForeColor = Drawing.Color.White
            tblHeaderCell.Controls.Add(oLBLDiscQuantity)
            tblHeaderCell.Style.Add("background", "#525252")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            Dim oLBLDiscOver As New RadLabel
            tblHeaderCell = New TableHeaderCell
            oLBLDiscOver.ID = "lblGridDiscOver"
            oLBLDiscOver.Text = "Discount over the threshold"
            oLBLDiscOver.ForeColor = Drawing.Color.White
            tblHeaderCell.Controls.Add(oLBLDiscOver)
            tblHeaderCell.Style.Add("background", "#525252")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            tblHeaderRow.BorderStyle = BorderStyle.None
            tblBoughtIn.Rows.Add(tblHeaderRow)

            ' Ciclo sugli item per la costruzione delle righe
            For Each oRowItm As DataRow In oDTItems.Rows
                tblRow = New TableRow
                tblCell = New TableCell
                Dim oLBLItemName As New RadLabel
                oLBLItemName.ID = "lblItem_" & oRowItm("VariableNameDefault")
                oLBLItemName.Text = oRowItm("VariableName")
                oLBLItemName.Style.Add("color", "darkred")
                tblCell.Controls.Add(oLBLItemName)
                tblCell.Style.Add("width", "20%")
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                tblCell.Text = Nn(GetVariableBoss("ExtraProduCost", 0, Session("IDPeriod"), "ExtraProduCost_" & oRowItm("VariableNameDefault"), Session("IDGame"))).ToString("N1")
                tblCell.BorderStyle = BorderStyle.None
                tblCell.Style.Add("text-align", "right")
                tblCell.Style.Add("width", "20%")
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                tblCell.Text = Nn(GetVariableBoss("ExtraProduQuali", 0, Session("IDPeriod"), "ExtraProduQuali_" & oRowItm("VariableNameDefault"), Session("IDGame"))).ToString("N1")
                tblCell.BorderStyle = BorderStyle.None
                tblCell.Style.Add("text-align", "right")
                tblCell.Style.Add("width", "20%")
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                tblCell.Text = Nn(GetVariableBoss("ThresDiscoExtra", 0, Session("IDPeriod"), "ThresDiscoExtra_" & oRowItm("VariableNameDefault"), Session("IDGame"))).ToString("N0")
                tblCell.BorderStyle = BorderStyle.None
                tblCell.Style.Add("text-align", "right")
                tblCell.Style.Add("width", "20%")
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                tblCell.Text = Nn(GetVariableBoss("DiscoLevelExtra", 0, Session("IDPeriod"), "ExtraProduQuali_" & oRowItm("VariableNameDefault"), Session("IDGame"))).ToString("N0")
                tblCell.BorderStyle = BorderStyle.None
                tblCell.Style.Add("text-align", "right")
                tblCell.Style.Add("width", "20%")
                tblRow.Cells.Add(tblCell)

                tblBoughtIn.Rows.Add(tblRow)
            Next
        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub Enviroment_Player_GoodsNews_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        m_SessionData = Session("SessionData")

        m_SiteMaster = Me.Master

        m_SiteMaster.LoadPeriodsValid = Session("IDRole") = "P"

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oCBOPeriod As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOPeriodValid As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriodValid"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler m_SiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler m_SiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(Session("IDGame")) ' Ultimo periodo
        oCBOPeriod.SelectedValue = Session("IDPeriodValid")
        m_SiteMaster.PeriodChange = Session("IDPeriodValid")

        Session("NameTeam") = GetTeamName(Session("IDTeam"))
        oCBOTeam.SelectedValue = Session("IDTeam")
        m_SiteMaster.TeamChange = Session("IDTeam")

        SQLConnClose(g_DAL.GetConnObject)
    End Sub

    Private Sub Enviroment_Player_GoodsNews_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        If Not Page.IsPostBack Then
            If Session("IDRole") = "P" Then
                m_SiteMaster.HandleReloadPeriods(HandleGetMaxPeriodValid(Session("IDGame")))
            End If
            LoadData()

            btnTranslate.Visible = Session("IDRole").Contains("D")
            ' Controllo tutti gli oggetti presenti
            Dim oTranslation As New TranslationObjectForm(Me, g_DAL, Nz(Session("LanguageActive")), Nni(Session("IDGame")))

        End If
    End Sub

    Private Sub btnTranslate_Click(sender As Object, e As EventArgs) Handles btnTranslate.Click
        Session("PageOrigin") = Me.ToString().Substring(4, Me.ToString().Substring(4).Length - 5) + ".aspx"
        Response.Redirect("PageTranslation.aspx")
        Response.End()
    End Sub

End Class
