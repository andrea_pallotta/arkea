﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Enviroment_Player_MarketTrends.aspx.vb" Inherits="Enviroment_Player_MarketTrends" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="cntMain" ContentPlaceHolderID="MainContent" runat="Server" ClientIDMode="Static">

    <asp:UpdatePanel ID="UpdatePanelMain" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <h2>
                <asp:Label ID="lblWelcome" runat="server" Text="News economic"></asp:Label>
            </h2>

            <div class="clr"></div>

            <h3>
                <asp:Label ID="lblTitolo" runat="server" Text="Retail market trend"></asp:Label>
            </h3>

            <div class="row">
                <asp:Table ClientIDMode="Static" ID="tblForecastVariation" runat="server">
                </asp:Table>
            </div>

            <div class="clr"></div>

            <div class="row" style="min-height: 30px;"></div>

            <div class="row" style="vertical-align: top">
                <asp:Table ClientIDMode="Static" ID="tblForecastTrend" runat="server">
                </asp:Table>
            </div>

            <div class="clr"></div>

            <div class="row" style="min-height: 30px;"></div>

            <div class="row">
                <telerik:RadHtmlChart ID="grfTrendGraph" runat="server" Width="100%">
                    <ChartTitle Text="Economic trend"></ChartTitle>
                </telerik:RadHtmlChart>
            </div>

            <div class="clr"></div>

            <div class="row" style="min-height: 50px;"></div>

            <div class="row">
                <h2>
                    <asp:Label ID="lblTitolo2" runat="server" Text="Wholesale world news" Width="100%"></asp:Label>
                </h2>
            </div>

            <div class="row">
                <asp:Table ClientIDMode="Static" ID="tblWholesaleContract" runat="server">
                </asp:Table>
            </div>

            <div class="clr"></div>

            <div class="row" style="min-height: 30px;"></div>

            <div class="row">
                <telerik:RadHtmlChart ID="grfWholesaleGraph" runat="server" Width="100%">
                    <ChartTitle Text="Wholesalers demand"></ChartTitle>
                </telerik:RadHtmlChart>
            </div>

            <div class="row" style="min-height: 20px;"></div>

            <div class="row">
                <asp:PlaceHolder runat="server" ID="Message" Visible="false">
                    <p class="text-danger">
                        <asp:Literal runat="server" ID="MessageText" />
                    </p>
                </asp:PlaceHolder>
            </div>
            <div class="clr"></div>

            <div class="row">
                <div class="divTableCell" style="text-align: right;">
                    <telerik:RadButton ID="btnTranslate" runat="server" Text="Translate" EnableAjaxSkinRendering="true" ToolTip=""></telerik:RadButton>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
