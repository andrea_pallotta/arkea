﻿Imports System.Data
Imports APS.APSUtility
Imports APPCore.Utility
Imports DALC4NET
Imports Telerik.Web.UI

Partial Class Enviroment_Player_MarketTrends
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadData()
        Message.Visible = False

        UpdatePanelMain.Update()
    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadData()

        Message.Visible = False
        UpdatePanelMain.Update()
    End Sub

    Private Sub LoadData()
        tblForecastVariation.Rows.Clear()
        tblForecastTrend.Rows.Clear()
        tblWholesaleContract.Rows.Clear()
        grfTrendGraph.Visible = False
        grfWholesaleGraph.Visible = False

        LoadDataForecast()
        LoadDataForecastTrend()
        LoadDataWholesaleContract()

        lblTitolo2.BackColor = Drawing.ColorTranslator.FromHtml("#e7e7e7")
    End Sub

    Private Sub LoadDataForecast()
        Dim oDTItems As DataTable = HandleGetItemsPeriodVariables(Session("IDGame"), Nz(Session("LanguageActive")))

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        ' Recupero il periodo successivo per decretare il trend
        Dim iIDPeriodoNext As Integer = HandleGetMaxPeriodValidNext(Session("IDGame"), m_SiteMaster.PeriodGetCurrent)
        Dim iIDPeriodLastYear As Integer = GetLastYearPeriod(Session("IDGame"), Session("CurrentStep"), Session("LunghezzaPeriodo"))

        Dim sNomeVariabile As String

        Try
            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header vuota
            Dim oLBLTitleGrid As New RadLabel
            oLBLTitleGrid.ID = "lblGridTitleForecast"
            oLBLTitleGrid.Text = "Forecast variation in Demand (%)"
            oLBLTitleGrid.ForeColor = Drawing.Color.White
            tblHeaderCell.Controls.Add(oLBLTitleGrid)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.Style.Add("background", "#525252")

            tblHeaderRow.BorderStyle = BorderStyle.None
            tblHeaderRow.Style.Add("margin-top", "10px")
            tblHeaderRow.Cells.Add(tblHeaderCell)
            tblHeaderRow.Style.Add("width", "50%")


            ' Previous quarters
            tblHeaderCell = New TableHeaderCell
            Dim oLBLPreviousQuarters As New RadLabel
            oLBLPreviousQuarters.ID = "lblPreviousQuarters"
            oLBLPreviousQuarters.Text = "Previous quarters"
            oLBLPreviousQuarters.Style.Add("font-size", "8pt")
            oLBLPreviousQuarters.Style.Add("text-align", "left")
            oLBLPreviousQuarters.Style.Add("font-weight", "bold")
            oLBLPreviousQuarters.Style.Add("color", "#ffffff")
            tblHeaderCell.Controls.Add(oLBLPreviousQuarters)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.Style.Add("background", "#3e6c86")
            tblHeaderCell.Style.Add("width", "25%")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            ' Delta last years
            tblHeaderCell = New TableHeaderCell
            Dim oLBLDeltaLastYear As New RadLabel
            oLBLDeltaLastYear.ID = "lblDeltaLastYears"
            oLBLDeltaLastYear.Text = "Delta last year"
            oLBLDeltaLastYear.Style.Add("font-size", "8pt")
            oLBLDeltaLastYear.Style.Add("text-align", "left")
            oLBLDeltaLastYear.Style.Add("font-weight", "bold")
            oLBLDeltaLastYear.Style.Add("color", "#ffffff")
            tblHeaderCell.Controls.Add(oLBLDeltaLastYear)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.Style.Add("background", "#3e6c86")
            tblHeaderCell.Style.Add("width", "25%")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            tblForecastVariation.Rows.Add(tblHeaderRow)

            ' Ciclo sugli item per la costruzione delle righe
            For Each oRowItm As DataRow In oDTItems.Rows
                tblRow = New TableRow
                tblCell = New TableCell
                Dim oLBLItemName As New RadLabel
                oLBLItemName.ID = "lblItem_Forecast_" & oRowItm("VariableNameDefault")
                oLBLItemName.Text = oRowItm("VariableName")
                oLBLItemName.Style.Add("color", "darkred")
                tblCell.Controls.Add(oLBLItemName)
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                sNomeVariabile = "EconoTrendProdu_" & oRowItm("VariableNameDefault")
                tblCell.Text = ((Nn(GetVariableBoss("EconoTrendProdu", 0, m_SiteMaster.PeriodGetCurrent, sNomeVariabile, Session("IDGame"))) _
                             - Nn(GetVariableBoss("EconoTrendProdu", 0, m_SiteMaster.PeriodGetPrev, sNomeVariabile, Session("IDGame")))) _
                             / Nn(GetVariableBoss("EconoTrendProdu", 0, m_SiteMaster.PeriodGetPrev, sNomeVariabile, Session("IDGame"))) _
                             * 100).ToString("N2")

                tblCell.BorderStyle = BorderStyle.None
                tblCell.Style.Add("text-align", "right")
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                sNomeVariabile = "EconoTrendProdu_" & oRowItm("VariableNameDefault")
                If iIDPeriodLastYear > 0 Then
                    tblCell.Text = ((Nn(GetVariableBoss("EconoTrendProdu", 0, iIDPeriodoNext, sNomeVariabile, Session("IDGame"))) _
                                 - Nn(GetVariableBoss("EconoTrendProdu", 0, iIDPeriodLastYear, sNomeVariabile, Session("IDGame")))) _
                                 / Nn(GetVariableBoss("EconoTrendProdu", 0, iIDPeriodLastYear, sNomeVariabile, Session("IDGame"))) _
                                 * 100).ToString("N2")
                Else
                    tblCell.Text = "No data available"
                End If


                tblCell.BorderStyle = BorderStyle.None
                tblCell.Style.Add("text-align", "right")
                tblRow.Cells.Add(tblCell)

                tblForecastVariation.Rows.Add(tblRow)
            Next
        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub LoadDataForecastTrend()
        Dim oDTItems As DataTable = HandleGetItemsPeriodVariables(Session("IDGame"), Nz(Session("LanguageActive")))

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        ' Recupero il periodo successivo per decretare il trend
        Dim iIDPeriodoNext As Integer = HandleGetMaxPeriodValidNext(Session("IDGame"), m_SiteMaster.PeriodGetCurrent)
        Dim iIDPeriodLastYear As Integer = GetLastYearPeriod(Session("IDGame"), Session("CurrentStep"), Session("LunghezzaPeriodo"))

        Dim sNomeVariabile As String
        Dim dTrendNext As Double
        Dim dTrendCurrent As Double
        Dim dTrendLastYear As Double

        Try
            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header vuota
            Dim oLBLTitleGrid As New RadLabel
            oLBLTitleGrid.ID = "lblGridTitleForecastTrend"
            oLBLTitleGrid.Text = "Forecast trend"
            oLBLTitleGrid.ForeColor = Drawing.Color.White
            tblHeaderCell.Controls.Add(oLBLTitleGrid)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.Style.Add("background", "#525252")

            tblHeaderRow.BorderStyle = BorderStyle.None
            tblHeaderRow.Style.Add("margin-top", "10px")
            tblHeaderRow.Cells.Add(tblHeaderCell)
            tblHeaderRow.Style.Add("width", "50%")

            ' Previous quarters
            tblHeaderCell = New TableHeaderCell
            Dim oLBLPreviousQuarters As New RadLabel
            oLBLPreviousQuarters.ID = "lblPreviousQuartersTrend"
            oLBLPreviousQuarters.Text = "Previous quarters"
            oLBLPreviousQuarters.Style.Add("font-size", "8pt")
            oLBLPreviousQuarters.Style.Add("text-align", "left")
            oLBLPreviousQuarters.Style.Add("font-weight", "bold")
            oLBLPreviousQuarters.Style.Add("color", "#ffffff")
            tblHeaderCell.Controls.Add(oLBLPreviousQuarters)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.Style.Add("background", "#3e6c86")
            tblHeaderCell.Style.Add("width", "25%")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            ' Delta last years
            tblHeaderCell = New TableHeaderCell
            Dim oLBLDeltaLastYear As New RadLabel
            oLBLDeltaLastYear.ID = "lblDeltaLastYearTrend"
            oLBLDeltaLastYear.Text = "Delta last year"
            oLBLDeltaLastYear.Style.Add("font-size", "8pt")
            oLBLDeltaLastYear.Style.Add("text-align", "left")
            oLBLDeltaLastYear.Style.Add("font-weight", "bold")
            oLBLDeltaLastYear.Style.Add("color", "#ffffff")
            tblHeaderCell.Controls.Add(oLBLDeltaLastYear)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.Style.Add("background", "#3e6c86")
            tblHeaderCell.Style.Add("width", "25%")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            tblForecastTrend.Rows.Add(tblHeaderRow)

            ' Ciclo sugli item per la costruzione delle righe
            For Each oRowItm As DataRow In oDTItems.Rows
                tblRow = New TableRow
                tblCell = New TableCell
                Dim oLBLItemName As New RadLabel
                oLBLItemName.ID = "lblItem_ForecastTrend_" & oRowItm("VariableNameDefault")
                oLBLItemName.Text = oRowItm("VariableName")
                oLBLItemName.Style.Add("color", "darkred")
                tblCell.Controls.Add(oLBLItemName)
                tblRow.Cells.Add(tblCell)

                sNomeVariabile = "EconoTrendProdu_" & oRowItm("VariableNameDefault")
                dTrendNext = Nn(GetVariableBoss("EconoTrendProdu", 0, iIDPeriodoNext, sNomeVariabile, Session("IDGame")))
                dTrendCurrent = Nn(GetVariableBoss("EconoTrendProdu", 0, m_SiteMaster.PeriodGetPrev, sNomeVariabile, Session("IDGame")))
                dTrendLastYear = Nn(GetVariableBoss("EconoTrendProdu", 0, iIDPeriodLastYear, sNomeVariabile, Session("IDGame")))

                tblCell = New TableCell
                If dTrendNext > dTrendCurrent Then
                    tblCell.Text = "on increase"
                End If
                If dTrendNext = dTrendCurrent Then
                    tblCell.Text = "steady"
                End If
                If dTrendNext < dTrendCurrent Then
                    tblCell.Text = "on decrease"
                End If

                tblCell.BorderStyle = BorderStyle.None
                tblCell.Style.Add("text-align", "right")
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                If iIDPeriodLastYear > 0 Then
                    If dTrendLastYear > dTrendCurrent Then
                        tblCell.Text = "on increase"
                    End If
                    If dTrendLastYear = dTrendCurrent Then
                        tblCell.Text = "steady"
                    End If
                    If dTrendLastYear < dTrendCurrent Then
                        tblCell.Text = "on decrease"
                    End If
                Else
                    tblCell.Text = "No data available"
                End If

                tblCell.BorderStyle = BorderStyle.None
                tblCell.Style.Add("text-align", "right")
                tblRow.Cells.Add(tblCell)

                tblForecastTrend.Rows.Add(tblRow)
            Next

            If tblForecastTrend.Rows.Count > 1 Then
                LoadDataTrendGraph()
                grfTrendGraph.Visible = True

                LoadDataWholesaleGraph()
                grfWholesaleGraph.Visible = True
            End If

        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub LoadDataTrendGraph()
        Dim sSQL As String
        Dim oDTItems As DataTable = HandleGetItemsPeriodVariables(Session("IDGame"), Nz(Session("LanguageActive")))
        Dim sItemName As String
        Dim dValue As Double = 0
        Dim iTotNumPlayers As Integer = HandleLoadPlayersGame(Session("IDGame")).Rows.Count

        grfTrendGraph.PlotArea.Series.Clear()
        grfTrendGraph.PlotArea.XAxis.Items.Clear()

        ' Recupero tutti i periodi generati fino adesso
        sSQL = "SELECT * FROM BGOL_Games_Periods BGP " _
             & "INNER JOIN BGOL_Periods BP ON BGP.IDPeriodo = BP.Id " _
             & "WHERE BGP.IDGame = " & Session("IDGame") & " AND BP.NumStep <= " & Session("CurrentStep") _
             & " ORDER BY BP.NumStep "
        Dim oDTPeriodi As DataTable = g_DAL.ExecuteDataTable(sSQL)

        Dim oSeriesData As LineSeries
        For Each oRowItm As DataRow In oDTItems.Rows
            sItemName = oRowItm("VariableName").ToString

            oSeriesData = New LineSeries
            For Each oRowPeriod As DataRow In oDTPeriodi.Rows

                dValue = Nn(GetVariableBoss("EconoTrendProdu", 0, oRowPeriod("IDPeriodo"), "EconoTrendProdu_" & Nz(oRowItm("VariableNameDefault")), Session("IDGame")))

                If Not IsNothing(dValue) Then
                    Dim oItem As New SeriesItem
                    oItem.YValue = dValue.ToString("N2")
                    oItem.TooltipValue = Nz(oRowItm("VariableName"))
                    oSeriesData.Items.Add(oItem)
                End If
            Next
            grfTrendGraph.PlotArea.Series.Add(oSeriesData)
            oSeriesData.Name = sItemName
            oSeriesData.VisibleInLegend = True
            oSeriesData.LabelsAppearance.Visible = True
        Next

        For Each oRowPeriod As DataRow In oDTPeriodi.Rows
            Dim newAxisItem As New AxisItem(Nz(oRowPeriod("Descrizione")))
            grfTrendGraph.PlotArea.XAxis.Items.Add(newAxisItem)
        Next

        grfTrendGraph.PlotArea.XAxis.MinorGridLines.Visible = False
        grfTrendGraph.PlotArea.YAxis.MinorGridLines.Visible = False
        grfTrendGraph.Legend.Appearance.Visible = True
        grfTrendGraph.Legend.Appearance.Position = HtmlChart.ChartLegendPosition.Top

        grfTrendGraph.Visible = True
    End Sub

    Private Sub LoadDataWholesaleContract()
        Dim oDTItems As DataTable = HandleGetItemsPeriodVariables(Session("IDGame"), Nz(Session("LanguageActive")))

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        ' Recupero il periodo successivo per decretare il trend
        Dim iIDPeriodoNext As Integer = HandleGetMaxPeriodValidNext(Session("IDGame"), m_SiteMaster.PeriodGetCurrent)
        Dim iIDPeriodLastYear As Integer = GetLastYearPeriod(Session("IDGame"), Session("CurrentStep"), Session("LunghezzaPeriodo"))

        Dim sNomeVariabile As String

        Try
            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header vuota
            Dim oLBLTitleGrid As New RadLabel
            oLBLTitleGrid.ID = "lblGridTitleWholesaleContract"
            oLBLTitleGrid.Text = "Wholesale Contracts for Tender for the current period"
            oLBLTitleGrid.ForeColor = Drawing.Color.White
            tblHeaderCell.Controls.Add(oLBLTitleGrid)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderRow.Style.Add("width", "25%")
            tblHeaderCell.Style.Add("background", "#525252")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            ' Total volume requested
            tblHeaderCell = New TableHeaderCell
            Dim oLBLPreviousQuarters As New RadLabel
            oLBLPreviousQuarters.ID = "lblTotalVolumeRequested"
            oLBLPreviousQuarters.Text = "Total volume requested"
            oLBLPreviousQuarters.Style.Add("font-size", "8pt")
            oLBLPreviousQuarters.Style.Add("text-align", "left")
            oLBLPreviousQuarters.Style.Add("font-weight", "bold")
            oLBLPreviousQuarters.Style.Add("color", "#ffffff")
            tblHeaderCell.Controls.Add(oLBLPreviousQuarters)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.Style.Add("background", "#3e6c86")
            tblHeaderCell.Style.Add("width", "25%")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            ' Minimum accepted quality
            tblHeaderCell = New TableHeaderCell
            Dim oLBLDeltaLastYear As New RadLabel
            oLBLDeltaLastYear.ID = "lblMinimiìumAcceptedQuality"
            oLBLDeltaLastYear.Text = "Minimum accepted quality %"
            oLBLDeltaLastYear.Style.Add("font-size", "8pt")
            oLBLDeltaLastYear.Style.Add("text-align", "left")
            oLBLDeltaLastYear.Style.Add("font-weight", "bold")
            oLBLDeltaLastYear.Style.Add("color", "#ffffff")
            tblHeaderCell.Controls.Add(oLBLDeltaLastYear)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.Style.Add("background", "#3e6c86")
            tblHeaderCell.Style.Add("width", "25%")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            ' Maximum accepted price 
            tblHeaderCell = New TableHeaderCell
            Dim oLBLMaximumAcceptedPrice As New RadLabel
            oLBLMaximumAcceptedPrice.ID = "lblMaximumAcceptedPrice"
            oLBLMaximumAcceptedPrice.Text = "Maximum accepted price (€)"
            oLBLMaximumAcceptedPrice.Style.Add("font-size", "8pt")
            oLBLMaximumAcceptedPrice.Style.Add("text-align", "left")
            oLBLMaximumAcceptedPrice.Style.Add("font-weight", "bold")
            oLBLMaximumAcceptedPrice.Style.Add("color", "#ffffff")
            tblHeaderCell.Controls.Add(oLBLMaximumAcceptedPrice)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.Style.Add("background", "#3e6c86")
            tblHeaderCell.Style.Add("width", "25%")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            tblWholesaleContract.Rows.Add(tblHeaderRow)

            ' Ciclo sugli item per la costruzione delle righe
            For Each oRowItm As DataRow In oDTItems.Rows
                tblRow = New TableRow
                tblCell = New TableCell

                Dim oLBLItemName As New RadLabel
                oLBLItemName.ID = "lblItem_Wholesalers_" & oRowItm("VariableNameDefault")
                oLBLItemName.Text = oRowItm("VariableName")
                oLBLItemName.Style.Add("color", "darkred")
                tblCell.Controls.Add(oLBLItemName)
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                sNomeVariabile = "QuantRequiInter_" & oRowItm("VariableNameDefault")
                tblCell.Text = Nn(GetVariableBoss("QuantRequiInter", 0, iIDPeriodoNext, sNomeVariabile, Session("IDGame")))
                tblCell.BorderStyle = BorderStyle.None
                tblCell.Style.Add("text-align", "right")
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                sNomeVariabile = "LowesQualiAccep_" & oRowItm("VariableNameDefault")
                tblCell.Text = Nn(GetVariableBoss("LowesQualiAccep", 0, iIDPeriodoNext, sNomeVariabile, Session("IDGame")))
                tblCell.BorderStyle = BorderStyle.None
                tblCell.Style.Add("text-align", "right")
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                sNomeVariabile = "HighePriceAccep_" & oRowItm("VariableNameDefault")
                tblCell.Text = Nn(GetVariableBoss("HighePriceAccep", 0, iIDPeriodoNext, sNomeVariabile, Session("IDGame")))
                tblCell.BorderStyle = BorderStyle.None
                tblCell.Style.Add("text-align", "right")
                tblRow.Cells.Add(tblCell)

                tblWholesaleContract.Rows.Add(tblRow)
            Next

        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub LoadDataWholesaleGraph()
        Dim sSQL As String
        Dim oDTItems As DataTable = HandleGetItemsPeriodVariables(Session("IDGame"), Nz(Session("LanguageActive")))
        Dim sItemName As String
        Dim dValue As Double = 0
        Dim iTotNumPlayers As Integer = HandleLoadPlayersGame(Session("IDGame")).Rows.Count

        grfWholesaleGraph.PlotArea.Series.Clear()
        grfWholesaleGraph.PlotArea.XAxis.Items.Clear()

        ' Recupero tutti i periodi generati fino adesso
        sSQL = "SELECT * FROM BGOL_Games_Periods BGP " _
             & "INNER JOIN BGOL_Periods BP ON BGP.IDPeriodo = BP.Id " _
             & "WHERE BGP.IDGame = " & Session("IDGame") & " AND BP.NumStep <= " & Session("CurrentStep") _
             & " ORDER BY BP.NumStep "
        Dim oDTPeriodi As DataTable = g_DAL.ExecuteDataTable(sSQL)

        Dim oSeriesData As LineSeries
        For Each oRowItm As DataRow In oDTItems.Rows
            sItemName = oRowItm("VariableName").ToString

            oSeriesData = New LineSeries
            For Each oRowPeriod As DataRow In oDTPeriodi.Rows

                dValue = Nn(GetVariableBoss("QuantRequiInter", 0, oRowPeriod("IDPeriodo"), "QuantRequiInter_" & Nz(oRowItm("VariableNameDefault")), Session("IDGame")))

                If Not IsNothing(dValue) Then
                    Dim oItem As New SeriesItem
                    oItem.YValue = dValue.ToString("N2")
                    oItem.TooltipValue = Nz(oRowItm("VariableName"))
                    oSeriesData.Items.Add(oItem)
                End If
            Next
            grfWholesaleGraph.PlotArea.Series.Add(oSeriesData)
            oSeriesData.Name = sItemName
            oSeriesData.VisibleInLegend = True
            oSeriesData.LabelsAppearance.Visible = True
        Next

        For Each oRowPeriod As DataRow In oDTPeriodi.Rows
            Dim newAxisItem As New AxisItem(Nz(oRowPeriod("Descrizione")))
            grfWholesaleGraph.PlotArea.XAxis.Items.Add(newAxisItem)
        Next

        grfWholesaleGraph.PlotArea.XAxis.MinorGridLines.Visible = False
        grfWholesaleGraph.PlotArea.YAxis.MinorGridLines.Visible = False
        grfWholesaleGraph.Legend.Appearance.Visible = True
        grfWholesaleGraph.Legend.Appearance.Position = HtmlChart.ChartLegendPosition.Top

        grfWholesaleGraph.Visible = True
    End Sub

    Private Sub Enviroment_Player_MarketTrends_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        m_SessionData = Session("SessionData")

        m_SiteMaster = Me.Master

        m_SiteMaster.LoadPeriodsValid = Session("IDRole") = "P"

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oCBOPeriod As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOPeriodValid As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriodValid"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler m_SiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler m_SiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(Session("IDGame")) ' Ultimo periodo
        oCBOPeriod.SelectedValue = Session("IDPeriodValid")
        m_SiteMaster.PeriodChange = Session("IDPeriodValid")

        Session("NameTeam") = GetTeamName(Session("IDTeam"))
        oCBOTeam.SelectedValue = Session("IDTeam")
        m_SiteMaster.TeamChange = Session("IDTeam")

    End Sub

    Private Sub Enviroment_Player_MarketTrends_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        If Not Page.IsPostBack Then
            If Session("IDRole") = "P" Then
                m_SiteMaster.HandleReloadPeriods(HandleGetMaxPeriodValid(Session("IDGame")))
            End If
            LoadData()

            btnTranslate.Visible = Session("IDRole").Contains("D")
            ' Controllo tutti gli oggetti presenti
            Dim oTranslation As New TranslationObjectForm(Me, g_DAL, Nz(Session("LanguageActive")))

        End If
    End Sub

    Private Sub btnTranslate_Click(sender As Object, e As EventArgs) Handles btnTranslate.Click
        Session("PageOrigin") = Me.ToString().Substring(4, Me.ToString().Substring(4).Length - 5) + ".aspx"
        Response.Redirect("PageTranslation.aspx")
        Response.End()
    End Sub

End Class
