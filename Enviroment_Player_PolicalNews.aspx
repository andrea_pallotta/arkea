﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Enviroment_Player_PolicalNews.aspx.vb" Inherits="Enviroment_Player_PolicalNews" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="cntMain" ContentPlaceHolderID="MainContent" runat="Server" ClientIDMode="Static">

    <asp:UpdatePanel ID="UpdatePanelMain" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <h2>
                <asp:Label ID="lblWelcome" runat="server" Text=""></asp:Label>
            </h2>

            <div class="clr"></div>

            <h3>
                <asp:Label ID="lblTitolo" runat="server" Text="Political news"></asp:Label>
            </h3>

            <div class="row">
                <asp:Table ClientIDMode="Static" ID="tblData" runat="server">
                </asp:Table>
            </div>

            <div class="clr"></div>

            <div class="row" style="min-height: 20px;"></div>

            <div class="row">
                <asp:PlaceHolder runat="server" ID="Message" Visible="false">
                    <p class="text-danger">
                        <asp:Literal runat="server" ID="MessageText" />
                    </p>
                </asp:PlaceHolder>
            </div>

            <div class="clr"></div>

            <div class="row">
                <div class="divTableCell" style="text-align: right;">
                    <telerik:RadButton ID="btnTranslate" runat="server" Text="Translate" EnableAjaxSkinRendering="true" ToolTip="" ></telerik:RadButton>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
