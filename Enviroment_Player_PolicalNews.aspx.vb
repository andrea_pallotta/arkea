﻿Imports System.Data
Imports APS.APSUtility
Imports APPCore.Utility
Imports DALC4NET
Imports Telerik.Web.UI

Partial Class Enviroment_Player_PolicalNews
    Inherits System.Web.UI.Page


    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadData()
        Message.Visible = False

        UpdatePanelMain.Update()
    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadData()

        Message.Visible = False
        UpdatePanelMain.Update()
    End Sub

    Private Sub Enviroment_Player_PolicalNews_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        m_SessionData = Session("SessionData")

        m_SiteMaster = Me.Master

        m_SiteMaster.LoadPeriodsValid = Session("IDRole") = "P"

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oCBOPeriod As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOPeriodValid As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriodValid"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler m_SiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler m_SiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(Session("IDGame")) ' Ultimo periodo
        oCBOPeriod.SelectedValue = Session("IDPeriodValid")
        m_SiteMaster.PeriodChange = Session("IDPeriodValid")

        Session("NameTeam") = GetTeamName(Session("IDTeam"))
        oCBOTeam.SelectedValue = Session("IDTeam")
        m_SiteMaster.TeamChange = Session("IDTeam")

    End Sub

    Private Sub Enviroment_Player_PolicalNews_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        If Not Page.IsPostBack Then
            'Dim ListOfPhysicalControls As List(Of Control)

            btnTranslate.Visible = (m_SessionData.Utente.Ruolo.IDRuolo = "D")

            If Session("IDRole") = "P" Then
                m_SiteMaster.HandleReloadPeriods(HandleGetMaxPeriodValid(Session("IDGame")))
            End If
            LoadData()

            ' Controllo tutti gli oggetti presenti
            Dim oTranslation As New TranslationObjectForm(Me, g_DAL, Nz(Session("LanguageActive")))

        End If
    End Sub

    Private Sub LoadData()
        tblData.Rows.Clear()

        LoadDataTable()
    End Sub

    Private Sub LoadDataTable()
        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        ' Recupero il periodo successivo 
        Dim iIDPeriodoNext As Integer = HandleGetMaxPeriodValidNext(Session("IDGame"), m_SiteMaster.PeriodGetCurrent)

        Try
            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header  
            Dim oLBLGridTitleFactors As New RadLabel
            oLBLGridTitleFactors.ID = "lblGridTitleFactors"
            oLBLGridTitleFactors.Text = "Today broadcast"
            oLBLGridTitleFactors.ForeColor = Drawing.Color.White
            tblHeaderCell.Controls.Add(oLBLGridTitleFactors)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.ColumnSpan = 2
            tblHeaderCell.Style.Add("background", "#525252")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            tblData.Rows.Add(tblHeaderRow)

            tblRow = New TableRow
            tblCell = New TableCell

            ' TaxRatePerce
            Dim oLblFiscalRate As New RadLabel
            oLblFiscalRate.ID = "lblFiscalRate"
            oLblFiscalRate.Text = "Tax fiscal rate"
            oLblFiscalRate.Style.Add("color", "darkred")
            tblCell.Controls.Add(oLblFiscalRate)
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            tblCell.Text = Nn(GetVariableBoss("TaxRatePerce", 0, m_SiteMaster.PeriodGetCurrent, "", Session("IDGame"))).ToString("N2")
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)
            tblData.Rows.Add(tblRow)

            ' TaxRateCredi
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLblTaxRateCredit As New RadLabel
            oLblTaxRateCredit.ID = "lblTaxRateCredit"
            oLblTaxRateCredit.Text = "Tax credit rate"
            oLblTaxRateCredit.Style.Add("color", "darkred")
            tblCell.Controls.Add(oLblTaxRateCredit)
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            tblCell.Text = Nn(GetVariableBoss("TaxRateCredi", Session("IDTeam"), m_SiteMaster.PeriodGetCurrent, "TaxRateCredi_" & GetTeamName(Session("IDTeam")), Session("IDGame"))).ToString("N2")
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblData.Rows.Add(tblRow)

            ' Sconfinamento fido
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLblTaxCrediExtra As New RadLabel
            oLblTaxCrediExtra.ID = "lblTaxCrediExtra"
            oLblTaxCrediExtra.Text = "Trust encroachment"
            oLblTaxCrediExtra.Style.Add("color", "darkred")
            tblCell.Controls.Add(oLblTaxCrediExtra)
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            tblCell.Text = Nn(GetVariableBoss("TaxCrediExtra", 0, m_SiteMaster.PeriodGetCurrent, "", Session("IDGame"))).ToString("N2")
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblData.Rows.Add(tblRow)

            ' DiscoTermiMese
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLblDiscoTermiMese As New RadLabel
            oLblDiscoTermiMese.ID = "lblDiscoTermiMese"
            oLblDiscoTermiMese.Text = "Discount for advanced payment"
            oLblDiscoTermiMese.Style.Add("color", "darkred")
            tblCell.Controls.Add(oLblDiscoTermiMese)
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            tblCell.Text = Nn(GetVariableBoss("DiscoTermiMese", 0, iIDPeriodoNext, "", Session("IDGame"))).ToString("N2")
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblData.Rows.Add(tblRow)

            ' RicarTermiMese
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLblRicarTermiMese As New RadLabel
            oLblRicarTermiMese.ID = "lblRicarTermiMese"
            oLblRicarTermiMese.Text = "Penalty for delayed payment"
            oLblRicarTermiMese.Style.Add("color", "darkred")
            tblCell.Controls.Add(oLblRicarTermiMese)
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            tblCell.Text = Nn(GetVariableBoss("RicarTermiMese", 0, iIDPeriodoNext, "", Session("IDGame"))).ToString("N2")
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblData.Rows.Add(tblRow)

        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub btnTranslate_Click(sender As Object, e As EventArgs) Handles btnTranslate.Click
        Session("PageOrigin") = Me.ToString().Substring(4, Me.ToString().Substring(4).Length - 5) + ".aspx"
        Response.Redirect("PageTranslation.aspx")
        Response.End()
    End Sub

End Class
