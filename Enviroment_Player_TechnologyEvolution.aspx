﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Enviroment_Player_TechnologyEvolution.aspx.vb" Inherits="Enviroment_Player_TechnologyEvolution" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="cntMain" ContentPlaceHolderID="MainContent" runat="Server" ClientIDMode="Static">

    <asp:UpdatePanel ID="UpdatePanelMain" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <h2>
                <asp:Label ID="lblWelcome" runat="server" Text="External factors"></asp:Label>
            </h2>

            <div class="clr"></div>

            <div class="divTable">
                <div class="divTableRow">
                    <div class="divTableCell">
                        <h3>
                            <asp:Label ID="lblTitolo" runat="server" Text="Technology evolution"></asp:Label>
                        </h3>
                    </div>
                    <div class="divTableCell" style="text-align: right; font-size: 8pt">
                        <telerik:RadLinkButton ID="lnkHelp" runat="server" Text="Help" EnableAjaxSkinRendering="true" ToolTip="Show help"></telerik:RadLinkButton>
                    </div>
                </div>
            </div>

            <div class="row">
                <asp:Table ClientIDMode="Static" ID="tblQualityEmpty" runat="server" Style="width: 50%; float: left;">
                </asp:Table>

                <asp:Table ClientIDMode="Static" ID="tblQuality" runat="server" Style="float: right; width: 50%; padding-left: 20px;">
                </asp:Table>
            </div>

            <div class="clr"></div>

            <div class="row" style="min-height: 30px;"></div>

            <div class="row">
                <telerik:RadHtmlChart ID="grfQuality" runat="server" Width="100%">
                    <ChartTitle Text="Quality trend"></ChartTitle>

                    <PlotArea>
                        <Series>
                            <telerik:ColumnSeries Name="Quality" DataFieldY="AbsolQualiProce">
                                <Appearance FillStyle-BackgroundColor="#B67C00" />
                                <TooltipsAppearance DataFormatString="{0:2N}" BackgroundColor="White" Color="#B67C00" />
                                <LabelsAppearance Visible="false" />
                            </telerik:ColumnSeries>
                        </Series>

                        <XAxis>
                        </XAxis>

                        <YAxis>
                            <LabelsAppearance DataFormatString="{0}" />
                        </YAxis>

                    </PlotArea>

                    <Legend>
                        <Appearance Visible="true" Position="Top" />
                    </Legend>
                </telerik:RadHtmlChart>
            </div>

            <div class="clr"></div>

            <div class="row" style="min-height: 50px;"></div>

            <div class="row">
                <asp:Table ClientIDMode="Static" ID="tblTrendEmpty" runat="server" Style="width: 50%; float: left;">
                </asp:Table>

                <asp:Table ClientIDMode="Static" ID="tblTrend" runat="server" Style="float: right; width: 50%; padding-left: 20px;">
                </asp:Table>
            </div>

            <div class="clr"></div>

            <div class="row" style="min-height: 30px;"></div>

            <div class="row">
                <telerik:RadHtmlChart ID="grfTrend" runat="server" Width="100%">
                    <ChartTitle Text="Gap of variancy"></ChartTitle>

                    <PlotArea>
                        <Series>
                            <telerik:ColumnSeries Name="Gap" DataFieldY="VariaTechnQuali">
                                <Appearance FillStyle-BackgroundColor="#B67C00" />
                                <TooltipsAppearance DataFormatString="{0:2N}" BackgroundColor="White" Color="#B67C00" />
                                <LabelsAppearance Visible="false" />
                            </telerik:ColumnSeries>
                        </Series>

                        <XAxis>
                        </XAxis>

                        <YAxis>
                            <LabelsAppearance DataFormatString="{0}" />
                        </YAxis>

                    </PlotArea>

                    <Legend>
                        <Appearance Visible="true" Position="Top" />
                    </Legend>
                </telerik:RadHtmlChart>
            </div>

            <div class="row" style="min-height: 20px;"></div>

            <div class="row">
                <asp:PlaceHolder runat="server" ID="Message" Visible="false">
                    <p class="text-danger">
                        <asp:Literal runat="server" ID="MessageText" />
                    </p>
                </asp:PlaceHolder>
            </div>

            <telerik:RadWindow RenderMode="Lightweight" ID="modalHelp" runat="server" Width="520px" Height="450px" CenterIfModal="false"
                Style="z-index: 100001;" BorderStyle="None" Behaviors="Close, Move" Title="Help">
                <ContentTemplate>
                    <div style="padding: 10px; text-align: left;">
                        <div id="divTableMaster" runat="server" style="margin-bottom: 10px;">
                            <asp:Label runat="server" ID="lblHelp" Visible="true">

                            </asp:Label>
                        </div>
                    </div>

                </ContentTemplate>
            </telerik:RadWindow>


            <div class="clr"></div>

            <div class="row">
                <div class="divTableCell" style="text-align: right;">
                    <telerik:RadButton ID="btnTranslate" runat="server" Text="Translate" EnableAjaxSkinRendering="true" ToolTip=""></telerik:RadButton>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
