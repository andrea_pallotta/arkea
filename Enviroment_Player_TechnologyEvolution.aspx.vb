﻿Imports System.Data
Imports APS.APSUtility
Imports APPCore.Utility
Imports DALC4NET
Imports Telerik.Web.UI

Partial Class Enviroment_Player_TechnologyEvolution
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadData()
        Message.Visible = False

        UpdatePanelMain.Update()
    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadData()

        Message.Visible = False
        UpdatePanelMain.Update()
    End Sub

    Private Sub LoadData()
        tblQuality.Rows.Clear()
        tblTrend.Rows.Clear()

        LoadDataQuality()
        LoadDataTrend()
    End Sub

    Private Sub LoadDataQuality()
        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        ' Recupero il periodo successivo 
        Dim iIDPeriodoNext As Integer = HandleGetMaxPeriodValidNext(Session("IDGame"), m_SiteMaster.PeriodGetCurrent)

        Try
            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header  
            Dim oLBLGridTitleQuality As New RadLabel
            oLBLGridTitleQuality.ID = "lblGridTitleQuality"
            oLBLGridTitleQuality.Text = "Trend of Average Quality"
            oLBLGridTitleQuality.ForeColor = Drawing.Color.White
            tblHeaderCell.Controls.Add(oLBLGridTitleQuality)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.ColumnSpan = 2
            tblHeaderCell.Style.Add("background", "#525252")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            tblQuality.Rows.Add(tblHeaderRow)

            tblRow = New TableRow
            tblCell = New TableCell

            ' QUALITY
            Dim oLBLQuality As New RadLabel
            oLBLQuality.ID = "lblQualityPercentage"
            oLBLQuality.Text = "Quality percentage"
            oLBLQuality.Style.Add("color", "darkred")
            tblCell.Controls.Add(oLBLQuality)
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            tblCell.Text = Nn(GetVariableState("AbsolQualiProce", 0, m_SiteMaster.PeriodGetCurrent, "", Session("IDGame"))).ToString("N2")
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblQuality.Rows.Add(tblRow)

            LoadDataGraphQuality()
        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub LoadDataGraphQuality()
        Dim sSQL As String
        Dim oDTTableGraph As New DataTable

        Dim dValue As Double = 0

        ' Recupero tutti i periodi generati fino adesso
        sSQL = "SELECT * FROM BGOL_Games_Periods BGP " _
             & "INNER JOIN BGOL_Periods BP ON BGP.IDPeriodo = BP.Id " _
             & "WHERE BGP.IDGame = " & Session("IDGame") & " AND BP.NumStep <= " & Session("CurrentStep") - 1
        If Session("IDRole").Contains("P") Then
            sSQL &= " AND ISNULL(Simulation, 0) = 0"
        End If
        sSQL &= " ORDER BY BP.NumStep "

        Dim oDTPeriodi As DataTable = g_DAL.ExecuteDataTable(sSQL)

        '' Preparo il datatable che ospiterà i dati
        oDTTableGraph.Columns.Add("AbsolQualiProce", GetType(Double))
        oDTTableGraph.Columns.Add("Period", GetType(String))

        For Each oRowPeriod As DataRow In oDTPeriodi.Rows
            oDTTableGraph.Rows.Add(Nn(GetVariableState("AbsolQualiProce", 0, oRowPeriod("IDPeriodo"), "", Session("IDGame"))).ToString("N2"),
                                   Nz(oRowPeriod("Descrizione")))
        Next

        grfQuality.DataSource = oDTTableGraph
        grfQuality.DataBind()

        grfQuality.PlotArea.XAxis.DataLabelsField = "Period"
        grfQuality.PlotArea.XAxis.LabelsAppearance.TextStyle.Bold = True

        grfQuality.PlotArea.XAxis.LabelsAppearance.RotationAngle = 45
        grfQuality.PlotArea.XAxis.EnableBaseUnitStepAuto = True

        grfQuality.Visible = True
    End Sub

    Private Sub LoadDataTrend()
        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        ' Recupero il periodo successivo 
        Dim iIDPeriodoNext As Integer = HandleGetMaxPeriodValidNext(Session("IDGame"), m_SiteMaster.PeriodGetCurrent)

        Try
            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header  
            Dim oLBLGridTitleTrend As New RadLabel
            oLBLGridTitleTrend.ID = "lblGridTitleTrend"
            oLBLGridTitleTrend.Text = " Trend of quality gap"
            oLBLGridTitleTrend.ForeColor = Drawing.Color.White
            tblHeaderCell.Controls.Add(oLBLGridTitleTrend)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.ColumnSpan = 2
            tblHeaderCell.Style.Add("background", "#525252")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            tblTrend.Rows.Add(tblHeaderRow)

            tblRow = New TableRow
            tblCell = New TableCell

            ' QUALITY
            Dim oLBLTrend As New RadLabel
            oLBLTrend.ID = "lblTrend"
            oLBLTrend.Text = "Index of variancy"
            oLBLTrend.Style.Add("color", "darkred")
            tblCell.Controls.Add(oLBLTrend)
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            tblCell.Text = Nn(GetVariableDefineValue("VariaTechnQuali", 0, 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblTrend.Rows.Add(tblRow)

            LoadDataGraphTrend()
        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub LoadDataGraphTrend()
        Dim sSQL As String
        Dim oDTTableGraph As New DataTable

        Dim dValue As Double = 0

        ' Recupero tutti i periodi generati fino adesso
        sSQL = "SELECT * FROM BGOL_Games_Periods BGP " _
             & "INNER JOIN BGOL_Periods BP ON BGP.IDPeriodo = BP.Id " _
             & "WHERE BGP.IDGame = " & Session("IDGame") & " AND BP.NumStep <= " & Session("CurrentStep") - 1
        If Session("IDRole").Contains("P") Then
            sSQL &= " AND ISNULL(Simulation, 0) = 0"
        End If
        sSQL &= " ORDER BY BP.NumStep "

        Dim oDTPeriodi As DataTable = g_DAL.ExecuteDataTable(sSQL)

        '' Preparo il datatable che ospiterà i dati
        oDTTableGraph.Columns.Add("VariaTechnQuali", GetType(Double))
        oDTTableGraph.Columns.Add("Period", GetType(String))

        For Each oRowPeriod As DataRow In oDTPeriodi.Rows
            oDTTableGraph.Rows.Add(Nn(GetVariableDefineValue("VariaTechnQuali", 0, 0, 0, oRowPeriod("IDPeriodo"), Session("IDGame"))),
                                   Nz(oRowPeriod("Descrizione")))
        Next

        grfTrend.DataSource = oDTTableGraph
        grfTrend.DataBind()

        grfTrend.PlotArea.XAxis.DataLabelsField = "Period"
        grfTrend.PlotArea.XAxis.LabelsAppearance.TextStyle.Bold = True

        grfTrend.PlotArea.XAxis.LabelsAppearance.RotationAngle = 45
        grfTrend.PlotArea.XAxis.EnableBaseUnitStepAuto = True

        grfTrend.Visible = True
    End Sub

    Private Sub Enviroment_Player_TechnologyEvolution_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        m_SessionData = Session("SessionData")

        m_SiteMaster = Me.Master

        m_SiteMaster.LoadPeriodsValid = Session("IDRole") = "P"

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oCBOPeriod As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOPeriodValid As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriodValid"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler m_SiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler m_SiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(Session("IDGame")) ' Ultimo periodo
        oCBOPeriod.SelectedValue = Session("IDPeriodValid")
        m_SiteMaster.PeriodChange = Session("IDPeriodValid")

        Session("NameTeam") = GetTeamName(Session("IDTeam"))
        oCBOTeam.SelectedValue = Session("IDTeam")
        m_SiteMaster.TeamChange = Session("IDTeam")


        ' Gestione del pannello delle decisioni concesse
        modalHelp.OpenerElementID = lnkHelp.ClientID
        modalHelp.Modal = False
        modalHelp.VisibleTitlebar = True

        LoadHelp()
    End Sub

    Private Sub Enviroment_Player_TechnologyEvolution_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        If Not Page.IsPostBack Then
            If Session("IDRole") = "P" Then
                m_SiteMaster.ReloadPeriodsCalculate()
            End If
            LoadData()

            btnTranslate.Visible = Session("IDRole").Contains("D")
            ' Controllo tutti gli oggetti presenti
            Dim oTranslation As New TranslationObjectForm(Me, g_DAL, Nz(Session("LanguageActive")))

        End If
    End Sub

    Private Sub btnTranslate_Click(sender As Object, e As EventArgs) Handles btnTranslate.Click
        Session("PageOrigin") = Me.ToString().Substring(4, Me.ToString().Substring(4).Length - 5) + ".aspx"
        Response.Redirect("PageTranslation.aspx")
        Response.End()
    End Sub

#Region "PANNELLO HELP"

    Private Sub LoadHelp()
        lblHelp.Text = "<p>Quality trend<br>
                        The quality trend reflects the average expenditures that the competitors did during the periods.</p>
                        <p>&nbsp;</p>
                        <p>The variancy in Quality<br>
                        This index shows the gap between the average of the highest and lowest  investors in technology.  An increase means the gap is growing between competitors in the quarter. </p>"
    End Sub

#End Region

End Class
