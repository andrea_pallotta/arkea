﻿Imports Telerik.Web.UI
Imports APS.APSUtility
Imports APPCore.Utility

Partial Class Faq
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster

    Private Sub Faq_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        m_SessionData = Session("SessionData")

        m_SiteMaster = Me.Master

        m_SiteMaster.LoadPeriodsValid = Session("IDRole") = "P"

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oCBOPeriod As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOPeriodValid As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriodValid"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler m_SiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler m_SiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(Session("IDGame")) ' Ultimo periodo
        oCBOPeriod.SelectedValue = Session("IDPeriodValid")
        m_SiteMaster.PeriodChange = Session("IDPeriodValid")

        Session("NameTeam") = GetTeamName(Session("IDTeam"))
        oCBOTeam.SelectedValue = Session("IDTeam")
        m_SiteMaster.TeamChange = Session("IDTeam")

    End Sub

    Private Sub Faq_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        If Not Page.IsPostBack Then
            LoadFaq()

            btnTranslate.Visible = Session("IDRole").Contains("D")
        End If

        ' Controllo tutti gli oggetti presenti
        Dim oTranslation As New TranslationObjectForm(Me, g_DAL, Nz(Session("LanguageActive")))
    End Sub

    Private Sub btnTranslate_Click(sender As Object, e As EventArgs) Handles btnTranslate.Click
        Session("PageOrigin") = Me.ToString().Substring(4, Me.ToString().Substring(4).Length - 5) + ".aspx"
        Response.Redirect("PageTranslation.aspx")
        Response.End()
    End Sub

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)

    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)

    End Sub

    Private Sub LoadFaq()
        lblFaq.Text = "<p><strong>FAQ &ndash; EURONET &ndash; ARKE2020</strong><strong> </strong></p>
                        <p><strong>E' la prima volta che  entro nel game. Cosa devo fare?&nbsp;</strong><br>
                          Per prima cosa &egrave; utile leggere il manuale, dove sono  contenute tutte le informazioni sul game e sul contesto di riferimento. Quindi  potresti entrare nelle singole aree funzionali, prendendo  confidenza con le informazioni sparse nei vari locali. Per qualsiasi domanda sono a tua  disposizione i tutor, che puoi contattare entrando nella sezione &quot;Tutor on  line&quot;.</p>
                        <p><strong>Come si prendono le  decisioni ?&nbsp;</strong><br>
                          Le decisioni sono raggruppate in quattro aree: Marketing,  Operation, Distribuzione, Finanza e controllo. Si accede alle decisioni da  ciascuna area, oppure direttamente dalla Sala riunioni. Le caselle contengono le decisioni, che possono essere modificate in qualsiasi momento  (ovviamente prima della scadenza). Una volta cambiate le decisioni occorre  confermarle premendo il tasto &quot;conferma&quot;.</p>
                        <p><strong>Posso cambiare le  decisioni prese?&nbsp;</strong><br>
                          Le decisioni possono essere modificate in qualsiasi momento  prima della scadenza. </p>
                        <p><strong>Esiste una scadenza  per le decisioni?&nbsp;</strong><br>
                          La scadenza viene detrminata dall'amministratore della sessione che la comunica a tutti i team leader.</p>
                        <p><strong>Quando saranno  disponibili i risultati?&nbsp;</strong><br>
                          Dopo la scadenza delle decisioni, occorre attendere un breve  periodo per l'elaborazione dei risultati. La lunghezza del periodo dipende dal  Tutor. In genere pu&ograve; variare da pochi minuti a un'ora. Puoi verificare se &egrave;  stato elaborato un nuovo periodo, osservando il periodo di gioco presente nella  casella a discesa in alto a sinistra.</p>
                        <p><strong>Le mie decisioni  hanno un effetto immediato sui risultati?&nbsp;</strong><br>
                          No, per vedere i risultati &egrave; necessario attendere la  scadenza delle decisioni. Solo con l'elaborazione del nuovo mese sar&agrave; possibile  vedere i risultati conseguenti alle decisioni della propria azienda e delle  altre aziende concorrenti.<br>
                          Esiste per&ograve; un'eccezione: la capacit&agrave; produttiva e il budget  per le decisioni (sezione Check on-line) vengono calcolati immediatamente. Ci&ograve;  permette di verificare immediatamente l'impatto delle proprie decisioni sulla  produzione e sulla liquidit&agrave; necessaria</p>
                        <p><strong>Come posso sapere se  ho liquidit&agrave; sufficiente a soddisfare le decisioni?&nbsp;</strong><br>
                          Ogni volta che inserisci una decisione e premi il tasto  conferma, il sistema calcola la liquidit&agrave; necessaria e la raffronta con la  liquidit&agrave; disponibile. Puoi verificarlo nella sezione Check on-line.</p>
                        <p><strong>Non ho sufficiente  liquidit&agrave;: cosa posso fare?&nbsp;</strong></p>
                        Prova a rivedere le tue decisioni oppure chiedi un  prestito alla banca (decisioni Finanza e controllo). Attenzione, il tuo livello  di affidamento non pu&ograve; superare quello massimo concesso dalla banca (vedi  Finanza e Controllo--&gt;Affidamento)
                        <p class='testopiccolotitolo'>&nbsp;</p></td>"
    End Sub

End Class
