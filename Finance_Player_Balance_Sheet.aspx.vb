﻿Imports System.Data
Imports APS
Imports APPCore.Utility
Imports DALC4NET
Imports Telerik.Web.UI

Partial Class Finance_Player_Balance_Sheet
    Inherits System.Web.UI.Page

    Private m_SiteMaster As SiteMaster

    Private m_SessionData As MU_SessionData
    Private m_Separator As String = "."

    Private m_IDPeriodoMax As Integer

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadData()
        UpdatePanelMain.Update()
    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadData()
        UpdatePanelMain.Update()
    End Sub

    Private Sub Finance_Player_Balance_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        m_SessionData = Session("SessionData")

        m_SiteMaster = Me.Master

        m_SiteMaster.LoadPeriodsValid = Session("IDRole") = "P"

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oCBOPeriod As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOPeriodValid As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriodValid"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler m_SiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler m_SiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(Session("IDGame")) ' Ultimo periodo
        oCBOPeriod.SelectedValue = Session("IDPeriodValid")
        m_SiteMaster.PeriodChange = Session("IDPeriodValid")

        Session("NameTeam") = GetTeamName(Session("IDTeam"))
        oCBOTeam.SelectedValue = Session("IDTeam")
        m_SiteMaster.TeamChange = Session("IDTeam")

        Message.Visible = False

        ' Gestione del pannello delle decisioni concesse
        modalHelp.OpenerElementID = lnkHelp.ClientID
        modalHelp.Modal = False
        modalHelp.VisibleTitlebar = True

        LoadHelp()
    End Sub

    Private Sub LoadData()
        ' ATTIVO
        Dim NetPlants As Double
        Dim NetVehicles As Double
        Dim CashEndPerio As Double
        Dim DebtsEndPerio As Double
        Dim RawEndPerio As Double
        Dim StockEndPerio As Double

        ' PASSIVO
        Dim CredEndPerio As Double
        Dim TaxPerioFunds As Double
        Dim LoansEndPerio As Double
        Dim NetCapital As Double
        Dim UtilsEndPerio As Double
        Dim TotalAssets As Double
        Dim TotalLiabilities As Double

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        tblBalanceSheet.Rows.Clear()

        Try
            tblBalanceSheet.Rows.Clear()

            ' ATTIVO
            NetPlants = GetVariableDefineValue("NetPlants", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))
            NetVehicles = GetVariableDefineValue("NetVehicles", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))
            CashEndPerio = GetVariableDefineValue("CashEndPerio", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))
            DebtsEndPerio = GetVariableDefineValue("DebtsEndPerio", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))
            RawEndPerio = GetVariableDefineValue("RawEndPerio", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))
            StockEndPerio = GetVariableDefineValue("StockEndPerio", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))

            ' PASSIVO
            CredEndPerio = GetVariableDefineValue("CrediEndPerio", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))
            TaxPerioFunds = GetVariableDefineValue("TaxPerioFunds", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))
            LoansEndPerio = GetVariableDefineValue("LoansEndPerio", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))
            UtilsEndPerio = GetVariableDefineValue("UtilsEndPerio", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))
            NetCapital = GetVariableState("NetCapital", Session("IDTeam"), m_SiteMaster.PeriodGetCurrent, "", Session("IDGame"))
            TotalAssets = GetVariableDefineValue("TotalAssets", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))
            TotalLiabilities = GetVariableDefineValue("TotalLiabilities", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))

            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Cella del prezzo di vendita
            tblHeaderCell = New TableHeaderCell
            Dim lblAssets As New RadLabel
            lblAssets.ID = "lblAssets"
            lblAssets.Text = "Assets"
            lblAssets.ForeColor = System.Drawing.Color.White
            tblHeaderCell.Controls.Add(lblAssets)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.ColumnSpan = "2"
            tblHeaderRow.Cells.Add(tblHeaderCell)

            tblHeaderCell = New TableHeaderCell
            tblHeaderCell.Text = " "
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.Style.Add("background", "white")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            ' Cella della quantità
            tblHeaderCell = New TableHeaderCell
            Dim lblLiabilities As New RadLabel
            lblLiabilities.ID = "lblLiabilities"
            lblLiabilities.Text = "Liabilities"
            lblLiabilities.ForeColor = System.Drawing.Color.White
            tblHeaderCell.Controls.Add(lblLiabilities)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.ColumnSpan = "2"
            tblHeaderRow.Cells.Add(tblHeaderCell)

            tblHeaderRow.BorderStyle = BorderStyle.None
            tblHeaderRow.Style.Add("margin-top", "10px")

            tblBalanceSheet.Rows.Add(tblHeaderRow)
            tblRow = New TableRow

            ' Riga vuota
            tblRow = New TableRow
            tblCell = New TableCell
            tblCell.ColumnSpan = 5
            tblCell.Text = "<hr/>"
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)
            tblBalanceSheet.Rows.Add(tblRow)

            ' Aggiungo le righe di riepilogo 
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblFixedAssets As New RadLabel
            lblFixedAssets.ID = "lblFixedAssets"
            lblFixedAssets.Text = "Fixed Assets"
            tblCell.Controls.Add(lblFixedAssets)
            tblCell.BorderStyle = BorderStyle.None
            tblCell.ColumnSpan = "2"
            tblCell.Style.Add("font-size", "9pt")
            tblCell.Style.Add("text-align", "center")
            tblCell.Style.Add("color", "#348DD1")
            tblCell.Style.Add("font-weight", "bold")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            tblCell.Text = " "
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim lblCurrentLiabilities As New RadLabel
            lblCurrentLiabilities.ID = "lblCurrentLiabilities"
            lblCurrentLiabilities.Text = "Current Liabilities"
            tblCell.Controls.Add(lblCurrentLiabilities)
            tblCell.BorderStyle = BorderStyle.None
            tblCell.ColumnSpan = "2"
            tblCell.Style.Add("font-size", "9pt")
            tblCell.Style.Add("text-align", "center")
            tblCell.Style.Add("color", "#348DD1")
            tblCell.Style.Add("font-weight", "bold")
            tblRow.Cells.Add(tblCell)

            tblBalanceSheet.Rows.Add(tblRow)

            ' PRIMA RIGA DATI
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblNetMachineValue As New RadLabel
            lblNetMachineValue.ID = "lblNetMachineValue"
            lblNetMachineValue.Text = "Net machine value"
            tblCell.Controls.Add(lblNetMachineValue)
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("font-weight", "bold")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim lnkNetPlants As New HyperLink
            lnkNetPlants.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=NetPlants&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            lnkNetPlants.Style.Add("text-decoration", "none")
            lnkNetPlants.Style.Add("cursor", "pointer")
            lnkNetPlants.Style.Add("text-align", "right")
            lnkNetPlants.Text = NetPlants.ToString("N2")
            tblCell.Controls.Add(lnkNetPlants)
            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            tblCell.Text = " "
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim lblCreditorsPayables As New RadLabel
            lblCreditorsPayables.ID = "lblCreditorsPayables"
            lblCreditorsPayables.Text = "Creditors/Payables"
            tblCell.Controls.Add(lblCreditorsPayables)
            tblCell.Style.Add("font-weight", "bold")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim lnkCredEndPerio As New HyperLink
            lnkCredEndPerio.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=CrediEndPerio&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            lnkCredEndPerio.Style.Add("text-decoration", "none")
            lnkCredEndPerio.Style.Add("cursor", "pointer")
            lnkCredEndPerio.Style.Add("text-align", "right")
            lnkCredEndPerio.Text = CredEndPerio.ToString("N2")
            tblCell.Controls.Add(lnkCredEndPerio)

            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblBalanceSheet.Rows.Add(tblRow)
            ' -------------------------------------------------

            ' SECONDA RIGA DATI
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblNetVehicleValue As New RadLabel
            lblNetVehicleValue.ID = "lblNetVehicleValue"
            lblNetVehicleValue.Text = "Net vehicle value"
            tblCell.Controls.Add(lblNetVehicleValue)
            tblCell.Style.Add("font-weight", "bold")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim lnkNetVehicles As New HyperLink
            lnkNetVehicles.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=NetVehicles&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            lnkNetVehicles.Style.Add("text-decoration", "none")
            lnkNetVehicles.Style.Add("cursor", "pointer")
            lnkNetVehicles.Text = NetVehicles.ToString("N2")

            tblCell.Controls.Add(lnkNetVehicles)

            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            tblCell.Text = " "
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim lblTaxProvision As New RadLabel
            lblTaxProvision.ID = "lblTaxProvision"
            lblTaxProvision.Text = "Tax provision"
            tblCell.Controls.Add(lblTaxProvision)
            tblCell.Style.Add("font-weight", "bold")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim lnkTaxPerioFunds As New HyperLink
            lnkTaxPerioFunds.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=TaxPerioFunds&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            lnkTaxPerioFunds.Style.Add("text-decoration", "none")
            lnkTaxPerioFunds.Style.Add("cursor", "pointer")

            lnkTaxPerioFunds.Text = TaxPerioFunds.ToString("N2")

            tblCell.Controls.Add(lnkTaxPerioFunds)
            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblBalanceSheet.Rows.Add(tblRow)
            ' -------------------------------------------------
            ' TERZA RIGA DATI
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblWorkingCapital As New RadLabel
            lblWorkingCapital.ID = "lblWorkingCapital"
            lblWorkingCapital.Text = "Working capital"
            lblWorkingCapital.ForeColor = System.Drawing.Color.White
            tblCell.Controls.Add(lblWorkingCapital)
            tblCell.BorderStyle = BorderStyle.None
            tblCell.ColumnSpan = "2"
            tblCell.Style.Add("background", "#FF6C32")
            tblCell.Style.Add("color", "#FFFFFF")
            tblCell.Style.Add("padding-left", "5px")
            tblCell.Style.Add("font-weight", "bold")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            tblCell.Text = " "
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim lblLoans As New RadLabel
            lblLoans.ID = "lblWorkingCapital"
            lblLoans.Text = "Loans"
            tblCell.Controls.Add(lblLoans)
            tblCell.Style.Add("font-weight", "bold")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell

            Dim lnkLoansEndPerio As New HyperLink
            lnkLoansEndPerio.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=LoansEndPerio&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            lnkLoansEndPerio.Style.Add("text-decoration", "none")
            lnkLoansEndPerio.Style.Add("cursor", "pointer")

            lnkLoansEndPerio.Text = LoansEndPerio.ToString("N2")

            tblCell.Controls.Add(lnkLoansEndPerio)

            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblBalanceSheet.Rows.Add(tblRow)
            ' -------------------------------------------------
            ' QUARTA RIGA DATI
            tblRow = New TableRow
            tblCell = New TableCell

            Dim lblCash As New RadLabel
            lblCash.ID = "lblCash"
            lblCash.Text = "Cash"
            tblCell.Controls.Add(lblCash)
            tblCell.Style.Add("font-weight", "bold")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim lnkCashEndPerio As New HyperLink
            lnkCashEndPerio.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=CashEndPerio&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            lnkCashEndPerio.Style.Add("text-decoration", "none")
            lnkCashEndPerio.Style.Add("cursor", "pointer")

            lnkCashEndPerio.Text = CashEndPerio.ToString("N2")

            tblCell.Controls.Add(lnkCashEndPerio)
            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            tblCell.Text = " "
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            tblCell.Text = ""
            tblCell.Style.Add("font-weight", "bold")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            tblCell.Text = ""
            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblBalanceSheet.Rows.Add(tblRow)
            ' -------------------------------------------------
            ' QUINTA RIGA DATI
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblDebtorsReceivables As New RadLabel
            lblDebtorsReceivables.ID = "lblDebtorsReceivables"
            lblDebtorsReceivables.Text = "Debtors/Receivables"
            tblCell.Controls.Add(lblDebtorsReceivables)
            tblCell.Style.Add("font-weight", "bold")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell

            Dim lnkDebtsEndPerio As New HyperLink
            lnkDebtsEndPerio.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=DebtsEndPerio&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            lnkDebtsEndPerio.Style.Add("text-decoration", "none")
            lnkDebtsEndPerio.Style.Add("cursor", "pointer")

            lnkDebtsEndPerio.Text = DebtsEndPerio.ToString("N2")

            tblCell.Controls.Add(lnkDebtsEndPerio)
            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            tblCell.Text = " "
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim lblNetAssets As New RadLabel
            lblNetAssets.ID = "lblNetAssets"
            lblNetAssets.Text = "Net assets"
            lblNetAssets.ForeColor = System.Drawing.Color.White
            tblCell.Style.Add("font-weight", "bold")
            tblCell.BorderStyle = BorderStyle.None
            tblCell.ColumnSpan = "2"
            tblCell.Style.Add("background", "#11738E")
            tblCell.Style.Add("color", "#FFFFFF")
            tblCell.Style.Add("padding-left", "5px")
            tblCell.Controls.Add(lblNetAssets)
            tblRow.Cells.Add(tblCell)

            tblBalanceSheet.Rows.Add(tblRow)
            ' -------------------------------------------------
            ' SESTA RIGA DATI
            tblRow = New TableRow
            tblCell = New TableCell

            Dim lblRawMaterialStockValue As New RadLabel
            lblRawMaterialStockValue.ID = "lblRawMaterialStockValue"
            lblRawMaterialStockValue.Text = "Raw materials stock value"
            tblCell.Controls.Add(lblRawMaterialStockValue)
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("font-weight", "bold")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim lnkRawEndPerio As New HyperLink
            lnkRawEndPerio.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=RawEndPerio&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            lnkRawEndPerio.Style.Add("text-decoration", "none")
            lnkRawEndPerio.Style.Add("cursor", "pointer")

            lnkRawEndPerio.Text = RawEndPerio.ToString("N2")

            tblCell.Controls.Add(lnkRawEndPerio)
            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            tblCell.Text = " "
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim lblRetainedearnings As New RadLabel
            lblRetainedearnings.ID = "lblRetainedearnings"
            lblRetainedearnings.Text = "Retained earnings"
            tblCell.Controls.Add(lblRetainedearnings)
            tblCell.Style.Add("font-weight", "bold")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim lnkUtilsEndPerio As New HyperLink
            lnkUtilsEndPerio.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=UtilsEndPerio&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            lnkUtilsEndPerio.Style.Add("text-decoration", "none")
            lnkUtilsEndPerio.Style.Add("cursor", "pointer")

            lnkUtilsEndPerio.Text = UtilsEndPerio.ToString("N2")

            tblCell.Controls.Add(lnkUtilsEndPerio)
            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblBalanceSheet.Rows.Add(tblRow)
            ' -------------------------------------------------
            ' SETTIMA RIGA DATI
            tblRow = New TableRow
            tblCell = New TableCell

            Dim lblFinishedGoodsStockValue As New RadLabel
            lblFinishedGoodsStockValue.ID = "lblFinishedGoodsStockValue"
            lblFinishedGoodsStockValue.Text = "Finished goods stock value"
            tblCell.Controls.Add(lblFinishedGoodsStockValue)
            tblCell.Style.Add("font-weight", "bold")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell

            Dim lnkStockEndPerio As New HyperLink
            lnkStockEndPerio.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=StockEndPerio&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            lnkStockEndPerio.Style.Add("text-decoration", "none")
            lnkStockEndPerio.Style.Add("cursor", "pointer")

            lnkStockEndPerio.Text = StockEndPerio.ToString("N2")

            tblCell.Controls.Add(lnkStockEndPerio)
            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            tblCell.Text = " "
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim lblShareCapital As New RadLabel
            lblShareCapital.ID = "lblShareCapital"
            lblShareCapital.Text = "Share capital"
            tblCell.Controls.Add(lblShareCapital)
            tblCell.Style.Add("font-weight", "bold")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell

            Dim lnkNetCapital As New HyperLink
            lnkNetCapital.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=NetCapital&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            lnkNetCapital.Style.Add("text-decoration", "none")
            lnkNetCapital.Style.Add("cursor", "pointer")

            lnkNetCapital.Text = NetCapital.ToString("N2")

            tblCell.Controls.Add(lnkNetCapital)
            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblBalanceSheet.Rows.Add(tblRow)
            ' -------------------------------------------------
            ' SETTIMA RIGA DATI
            tblRow = New TableRow
            tblCell = New TableCell
            tblCell.Text = "<hr/>"
            tblCell.ColumnSpan = "5"
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)
            tblBalanceSheet.Rows.Add(tblRow)
            ' -------------------------------------------------
            ' OTTAVA RIGA DATI
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblTotalAssets As New RadLabel
            lblTotalAssets.ID = "lblTotalAssets"
            lblTotalAssets.Text = "TOTAL ASSETS"
            tblCell.Controls.Add(lblTotalAssets)
            tblCell.Style.Add("font-size", "9pt")
            tblCell.Style.Add("text-align", "right")
            tblCell.Style.Add("color", "#FF9507")
            tblCell.Style.Add("font-weight", "bold")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim lnkTotalAssets As New HyperLink
            lnkTotalAssets.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=TotalAssets&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            lnkTotalAssets.Style.Add("text-decoration", "none")
            lnkTotalAssets.Style.Add("cursor", "pointer")

            lnkTotalAssets.Text = TotalAssets.ToString("N2")

            tblCell.Controls.Add(lnkTotalAssets)
            tblCell.Style.Add("font-size", "9pt")
            tblCell.Style.Add("text-align", "right")
            tblCell.Style.Add("color", "#FF9507")
            tblCell.Style.Add("font-weight", "bold")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            tblCell.Text = " "
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim lblTotalLiabilities As New RadLabel
            lblTotalLiabilities.ID = "lblTotalLiabilities"
            lblTotalLiabilities.Text = "TOTAL LIABILITIES"
            tblCell.Controls.Add(lblTotalLiabilities)
            tblCell.Style.Add("font-size", "9pt")
            tblCell.Style.Add("text-align", "right")
            tblCell.Style.Add("color", "#FF9507")
            tblCell.Style.Add("font-weight", "bold")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim lnkTotalLiabilities As New HyperLink
            lnkTotalLiabilities.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=TotalAssets&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            lnkTotalLiabilities.Style.Add("text-decoration", "none")
            lnkTotalLiabilities.Style.Add("cursor", "pointer")

            lnkTotalLiabilities.Text = TotalLiabilities.ToString("N2")

            tblCell.Controls.Add(lnkTotalLiabilities)
            tblCell.Style.Add("font-size", "9pt")
            tblCell.Style.Add("text-align", "right")
            tblCell.Style.Add("color", "#FF9507")
            tblCell.Style.Add("font-weight", "bold")
            tblCell.BorderStyle = BorderStyle.None
            tblRow.Cells.Add(tblCell)

            tblBalanceSheet.Rows.Add(tblRow)

            SQLConnClose(g_DAL.GetConnObject)
        Catch ex As Exception
            MessageText.Text = "Error: <br/> " & ex.Message
            Message.Visible = True
        End Try

    End Sub

    Private Sub Finance_Player_Balance_Sheet_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        If Not Page.IsPostBack Then
            If Session("IDRole") = "P" Then
                m_SiteMaster.ReloadPeriodsCalculate()
            End If
            LoadData()

            btnTranslate.Visible = Session("IDRole").Contains("D")

        End If
        ' Controllo tutti gli oggetti presenti
        Dim oTranslation As New TranslationObjectForm(Me, g_DAL, Nz(Session("LanguageActive")))
    End Sub

    Private Sub btnTranslate_Click(sender As Object, e As EventArgs) Handles btnTranslate.Click
        Session("PageOrigin") = Me.ToString().Substring(4, Me.ToString().Substring(4).Length - 5) + ".aspx"
        Response.Redirect("PageTranslation.aspx")
        Response.End()
    End Sub

#Region "PANNELLO HELP"

    Private Sub LoadHelp()
        lblHelp.Text = "<p>Net machine  value<br>
                    It is the  total value of the owned machines (Net present value; purchase value;  depreciation over the periods)</p>
                    <p>Net vehicle  value<br>
                      It is the  total value of the vehicles (Net present value; purchase value ; depreciation  over the periods)</p>
                    <p>Cash <br>
                      It is the  cash available to use in the current quarter less commitments. </p>
                    <p>Debtors/  Receivables<br>
                      1/3 retail  revenues &nbsp;+ wholesale revenues x terms of  payment/3<br>
                      These are  due to be paid in the following period</p>
                    <p>Raw  materials stock value<br>
                      It is the  total value of raw materials in stock (based on LIFO method)</p>
                    <p>Finished  goods stock value<br>
                      It is the  total value of finished goods in stock (based on LIFO method)</p>
                    <p>Creditors/Payables<br>
                      1/3 of raw  materials (import and local) + warehouse costs + transport costs <br>
                      + fixed  costs&nbsp; + advertisement + customer service  + environmental expenditures<br>
                      +  automation technology + finished goods bought-in<br>
                      These are  due to be paid in the following period</p>
                    <p>Tax  provision<br>
                      Tax provision  increases at the end of the year if the company has made a profit. Payment of  tax is due at the beginning of each new year from cash. </p>
                    <p>Long Term  Loan<br>
                      Existing  loan in the business. </p>
                    <p>Retained  earnings<br>
                      These are  the cumulative profits retained in the business for re-investment.<br>
                        &nbsp;R.E.'s are referred to as a liablility in the  balance sheet because they are owned by the shareholders.</p>
                    <p>Share capital<br>
                  This is the  portion of the company's equity that has been obtained from the shareholders.</p>"
    End Sub

#End Region
End Class
