﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Finance_Player_Bank_Loan.aspx.vb" Inherits="Finance_Player_Bank_Loan" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="cntMain" ContentPlaceHolderID="MainContent" runat="Server" ClientIDMode="Static">

    <asp:UpdatePanel ID="UpdatePanelMain" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <h2>
                <asp:Label ID="lblWelcome" runat="server" Text="Finance"></asp:Label>
            </h2>

            <div class="clr"></div>

            <div class="divTable">
                <div class="divTableRow">
                    <div class="divTableCell">
                        <h3>
                            <asp:Label ID="lblTitolo" runat="server" Text="Bank loan"></asp:Label>
                        </h3>
                    </div>
                    <div class="divTableCell" style="text-align: right; font-size: 8pt">
                        <telerik:RadLinkButton ID="lnkHelp" runat="server" Text="Help" EnableAjaxSkinRendering="true" ToolTip="Show help"></telerik:RadLinkButton>
                    </div>
                </div>
            </div>

            <div class="row">

                <asp:Table ClientIDMode="Static" ID="tblBankLoan" runat="server">
                </asp:Table>
            </div>

            <div class="row">
                <telerik:RadHtmlChart ID="grfBorrowingLimit" runat="server">
                    <ChartTitle></ChartTitle>

                    <PlotArea>
                        <Series>
                            <telerik:AreaSeries Name="Borrowing limit" DataFieldY="ActuaMaximLoans">
                                <TooltipsAppearance DataFormatString="{0:2N}" BackgroundColor="#e7e7e7" Color="Blue" />
                                <LabelsAppearance Visible="false" RotationAngle="-90" Color="Black" />
                                <Appearance FillStyle-BackgroundColor="#2359FC" />
                                <LineAppearance Width="1"></LineAppearance>
                                <MarkersAppearance MarkersType="Circle" BackgroundColor="Blue" Size="6" BorderColor="Blue"
                                    BorderWidth="2"></MarkersAppearance>
                            </telerik:AreaSeries>

                            <telerik:AreaSeries Name="Loans" DataFieldY="LoansLevel">
                                <Appearance FillStyle-BackgroundColor="#B67C00" />
                                <LineAppearance LineStyle="Smooth" Width="3px" />
                                <TooltipsAppearance DataFormatString="{0:2N}" BackgroundColor="#e7e7e7" Color="Black" />
                                <LabelsAppearance Visible="false" />
                                <LineAppearance Width="1"></LineAppearance>
                                <MarkersAppearance MarkersType="Circle" BackgroundColor="White" Size="6" BorderColor="Red"
                                    BorderWidth="2"></MarkersAppearance>
                            </telerik:AreaSeries>
                        </Series>

                        <XAxis>
                        </XAxis>

                        <YAxis>
                            <LabelsAppearance DataFormatString="{0}" />
                        </YAxis>

                    </PlotArea>

                    <Legend>
                        <Appearance Visible="true" Position="Top" />
                    </Legend>
                </telerik:RadHtmlChart>
            </div>

            <asp:PlaceHolder runat="server" ID="Message" Visible="false">
                <p class="text-danger">
                    <asp:Literal runat="server" ID="MessageText" />
                </p>
            </asp:PlaceHolder>

            <telerik:RadWindow RenderMode="Lightweight" ID="modalHelp" runat="server" Width="520px" Height="450px" CenterIfModal="false"
                Style="z-index: 100001;" BorderStyle="None" Behaviors="Close, Move" Title="Help">
                <ContentTemplate>
                    <div style="padding: 10px; text-align: left;">
                        <div id="divTableMaster" runat="server" style="margin-bottom: 10px;">
                            <asp:Label runat="server" ID="lblHelp" Visible="true">

                            </asp:Label>
                        </div>
                    </div>

                </ContentTemplate>
            </telerik:RadWindow>
            <div class="clr"></div>

            <div class="row">
                <div class="divTableCell" style="text-align: right;">
                    <telerik:RadButton ID="btnTranslate" runat="server" Text="Translate" EnableAjaxSkinRendering="true" ToolTip=""></telerik:RadButton>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
