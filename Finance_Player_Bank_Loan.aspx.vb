﻿Imports System.Data
Imports APS
Imports APPCore.Utility
Imports DALC4NET
Imports Telerik.Web.UI

Partial Class Finance_Player_Bank_Loan
    Inherits System.Web.UI.Page

    Private m_SiteMaster As SiteMaster

    Private m_SessionData As MU_SessionData
    Private m_Separator As String = "."

    Private m_IDPeriodoMax As Integer

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadData()
        UpdatePanelMain.Update()
    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadData()
        UpdatePanelMain.Update()
    End Sub

    Private Sub Finance_Player_Bank_Loan_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        m_SessionData = Session("SessionData")

        m_SiteMaster = Me.Master

        m_SiteMaster.LoadPeriodsValid = Session("IDRole") = "P"

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oCBOPeriod As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOPeriodValid As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriodValid"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler m_SiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler m_SiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(Session("IDGame")) ' Ultimo periodo
        oCBOPeriod.SelectedValue = Session("IDPeriodValid")
        m_SiteMaster.PeriodChange = Session("IDPeriodValid")

        Session("NameTeam") = GetTeamName(Session("IDTeam"))
        oCBOTeam.SelectedValue = Session("IDTeam")
        m_SiteMaster.TeamChange = Session("IDTeam")

        Message.Visible = False

        ' Gestione del pannello delle decisioni concesse
        modalHelp.OpenerElementID = lnkHelp.ClientID
        modalHelp.Modal = False
        modalHelp.VisibleTitlebar = True

        LoadHelp()
    End Sub

    Private Sub LoadData()
        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As TableHeaderCell
        Dim tblRow As TableRow
        Dim tblCell As TableCell
        Dim iPeriodoMaxValid As Integer = HandleGetMaxPeriodValidNext(Session("IDGame"), m_SiteMaster.PeriodGetCurrent)

        lblTitolo.Text = "Bank loan"

        grfBorrowingLimit.Visible = True

        Try
            tblBankLoan.Rows.Clear()

            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            tblHeaderCell = New TableHeaderCell
            Dim lblBorrowingLimitInformation As New RadLabel
            lblBorrowingLimitInformation.ID = "lblBorrowingLimitInformation"
            lblBorrowingLimitInformation.Text = "Borrowing limit information"
            tblHeaderCell.Controls.Add(lblBorrowingLimitInformation)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.ColumnSpan = 2
            tblHeaderCell.Style.Add("background", "#525252")

            tblHeaderRow.Cells.Add(tblHeaderCell)

            tblHeaderRow.BorderStyle = BorderStyle.None
            tblHeaderRow.Style.Add("margin-top", "5px")

            tblBankLoan.Rows.Add(tblHeaderRow)

            ' PRIMA RIGA DATI
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLBLMaximumBorrowingLimit As New RadLabel
            oLBLMaximumBorrowingLimit.Text = "Maximum borrowing limit"
            oLBLMaximumBorrowingLimit.ID = "oLBLMaximumBorrowingLimit"
            oLBLMaximumBorrowingLimit.Style.Add("font-size", "8pt")
            oLBLMaximumBorrowingLimit.Style.Add("text-align", "left")
            oLBLMaximumBorrowingLimit.Style.Add("font-weight", "normal")
            tblCell.Controls.Add(oLBLMaximumBorrowingLimit)
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("padding-left", "5px")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLBLMaximumBorrowingLimitValue As New RadLabel
            oLBLMaximumBorrowingLimitValue.ID = "oLBLMaximumBorrowingLimitValue"
            oLBLMaximumBorrowingLimitValue.Text = Nn(GetVariableState("ActuaMaximLoans", Session("IDTeam"), Session("IDPeriod"), "", Session("IDGame"))).ToString("N2")
            oLBLMaximumBorrowingLimitValue.Style.Add("font-size", "8pt")
            oLBLMaximumBorrowingLimitValue.Style.Add("text-align", "right")
            oLBLMaximumBorrowingLimitValue.BackColor = Drawing.Color.Transparent
            tblCell.Controls.Add(oLBLMaximumBorrowingLimitValue)
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblBankLoan.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------------------------------

            ' SECONDA RIGA DATI
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLBLMoneyBorrowedToDate As New RadLabel
            oLBLMoneyBorrowedToDate.Text = "Money borrowed to date"
            oLBLMoneyBorrowedToDate.ID = "oLBLMoneyBorrowedToDate"
            oLBLMoneyBorrowedToDate.Style.Add("font-size", "8pt")
            oLBLMoneyBorrowedToDate.Style.Add("text-align", "left")
            oLBLMoneyBorrowedToDate.Style.Add("font-weight", "normal")
            tblCell.Controls.Add(oLBLMoneyBorrowedToDate)
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("padding-left", "5px")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLBLMoneyBorrowedToDateValue As New RadLabel
            oLBLMoneyBorrowedToDateValue.Text = Nn(GetVariableState("LoansLevel", Session("IDTeam"), Session("IDPeriod"), "", Session("IDGame"))).ToString("N2")
            oLBLMoneyBorrowedToDateValue.Style.Add("font-size", "8pt")
            oLBLMoneyBorrowedToDateValue.Style.Add("text-align", "right")
            oLBLMoneyBorrowedToDateValue.BackColor = Drawing.Color.Transparent
            tblCell.Controls.Add(oLBLMoneyBorrowedToDateValue)
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblBankLoan.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------------------------------

            ' INSERISCO LA RIGA CURRENT BORROWING LIMIT
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLBLCurrentBorrowingLimit As New RadLabel
            oLBLCurrentBorrowingLimit.Text = "Current borrowing limit"
            oLBLCurrentBorrowingLimit.ID = "oLBLCurrentBorrowingLimit"
            oLBLCurrentBorrowingLimit.Style.Add("font-size", "8pt")
            oLBLCurrentBorrowingLimit.Style.Add("text-align", "left")
            oLBLCurrentBorrowingLimit.Style.Add("font-weight", "bold")
            tblCell.Controls.Add(oLBLCurrentBorrowingLimit)
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("padding-left", "5px")
            tblCell.Style.Add("background", "#e7e7e7")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLBLCostGoodsSoldValue As New RadLabel
            oLBLCostGoodsSoldValue.Text = (Nn(GetVariableState("ActuaMaximLoans", Session("IDTeam"), Session("IDPeriod"), "", Session("IDGame"))) - Nn(GetVariableState("LoansLevel", Session("IDTeam"), Session("IDPeriod"), "", Session("IDGame")))).ToString("N2")
            oLBLCostGoodsSoldValue.Style.Add("font-size", "8pt")
            oLBLCostGoodsSoldValue.Style.Add("text-align", "right")
            tblCell.Controls.Add(oLBLCostGoodsSoldValue)
            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("background", "#e7e7e7")
            tblRow.Cells.Add(tblCell)

            tblBankLoan.Rows.Add(tblRow)
            ' ---------------------------------------------------------------------------------------------------------

            LoadGrafico()

        Catch ex As Exception
            MessageText.Text = "Error: <br/> " & ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub LoadGrafico()
        Dim sSQL As String
        Dim oDTTableResult As DataTable
        Dim oDTTableGraph As New DataTable
        Dim dActuaMaximLoans As Double
        Dim dLoansLevel As Double
        Dim sPeriod As String
        Dim iContaRighe As Integer = 1

        Try
            '' Preparo il datatable che ospiterà i dati
            oDTTableGraph.Columns.Add("ActuaMaximLoans", GetType(Double))
            oDTTableGraph.Columns.Add("LoansLevel", GetType(Double))
            oDTTableGraph.Columns.Add("Period", GetType(String))

            sSQL = "SELECT VS.ID, VS.VariableLabel, VS.IDGame, VS.Name, P.Descrizione AS Period, " _
                 & "VSV.Id AS IDVariableStateValue, VSV.IDPeriod, VSV.IDPlayer, VSV.IDItem, VSV.IDAge, VSV.IDVariable, VSV.Value " _
                 & "FROM Variables_State VS " _
                 & "INNER JOIN Variables_State_Value VSV ON VS.ID = VSV.IDVariable " _
                 & "INNER JOIN BGOL_Periods P ON VSV.IDPeriod = P.Id " _
                 & "WHERE  VS.Name IN('ActuaMaximLoans', 'LoansLevel') " _
                 & "AND VSV.IDPeriod <= " & Session("IDPeriod") & " " _
                 & "AND VSV.IDPlayer = " & Nni(Session("IDTeam")) _
                 & " AND VS.IDGame = " & Session("IDGame") _
                 & " ORDER BY VSV.IDPeriod, VSV.IDPlayer, VSV.ID"
            oDTTableResult = g_DAL.ExecuteDataTable(sSQL)

            For Each oRow As DataRow In oDTTableResult.Rows
                If Nz(oRow("Name")) = "ActuaMaximLoans" Then dActuaMaximLoans = Nn(oRow("Value"))

                If Nz(oRow("Name")) = "LoansLevel" Then dLoansLevel = Nn(oRow("Value"))

                sPeriod = Nz(oRow("Period"))

                If iContaRighe = 2 Then
                    oDTTableGraph.Rows.Add(dActuaMaximLoans.ToString("N2"), dLoansLevel.ToString("N2"), sPeriod)
                    sPeriod = ""
                    iContaRighe = 1
                Else
                    iContaRighe += 1
                End If
            Next

            grfBorrowingLimit.DataSource = oDTTableGraph
            grfBorrowingLimit.DataBind()

            grfBorrowingLimit.PlotArea.XAxis.DataLabelsField = "Period"
            grfBorrowingLimit.PlotArea.XAxis.LabelsAppearance.TextStyle.Bold = False

            grfBorrowingLimit.PlotArea.XAxis.LabelsAppearance.RotationAngle = 45
            grfBorrowingLimit.PlotArea.XAxis.EnableBaseUnitStepAuto = True
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Finance_Player_Bank_Loan_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        If Not Page.IsPostBack Then
            If Session("IDRole") = "P" Then
                m_SiteMaster.HandleReloadPeriods(HandleGetMaxPeriodValid(Session("IDGame")))
            End If
            LoadData()

            btnTranslate.Visible = Session("IDRole").Contains("D")
            ' Controllo tutti gli oggetti presenti
            Dim oTranslation As New TranslationObjectForm(Me, g_DAL, Nz(Session("LanguageActive")))

        End If
    End Sub

    Private Sub btnTranslate_Click(sender As Object, e As EventArgs) Handles btnTranslate.Click
        Session("PageOrigin") = Me.ToString().Substring(4, Me.ToString().Substring(4).Length - 5) + ".aspx"
        Response.Redirect("PageTranslation.aspx")
        Response.End()
    End Sub

#Region "PANNELLO HELP"

    Private Sub LoadHelp()
        lblHelp.Text = "<p>Maximum &nbsp;borrowing limit<br>
The credit  rating is per company. Different factors are taken into account by the bank and  may vary radically from one period to another. If the credit rate is lower than  the company loans outstanding at that point in time, the company will not be  forced to repay the difference.</p>
                    <p>Money borrowed  to date<br>
                  This is the  level of loan the company has borrowed from the bank to date.</p>"
    End Sub

#End Region

End Class
