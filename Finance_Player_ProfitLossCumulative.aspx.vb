﻿Imports System.Data
Imports APS
Imports APPCore.Utility
Imports DALC4NET
Imports Telerik.Web.UI

Partial Class Finance_Player_ProfitLossCumulative
    Inherits System.Web.UI.Page

    Private m_SiteMaster As SiteMaster

    Private m_SessionData As MU_SessionData
    Private m_Separator As String = "."

    Private m_IDPeriodoMax As Integer

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        divTableMaster.Controls.Clear()
        LoadData()
        UpdatePanelMain.Update()
    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        divTableMaster.Controls.Clear()
        LoadData()
        UpdatePanelMain.Update()
    End Sub

    Protected Sub grdPivot_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.Header Then
            MergeHeader(DirectCast(sender, GridView), e.Row, 1)
        End If
    End Sub

    Protected Sub grdPivot_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            If e.Row.DataItemIndex >= 0 Then
                e.Row.Cells(0).BackColor = System.Drawing.ColorTranslator.FromHtml("#55A2DF")
                e.Row.Cells(0).ForeColor = System.Drawing.ColorTranslator.FromHtml("#fff")
            End If
        End If
    End Sub

    Private Sub MergeHeader(ByVal gv As GridView, ByVal row As GridViewRow, ByVal PivotLevel As Integer)
        Dim iCount As Integer = 1
        Dim iContaColonneHeader = 0

        For iCount = 1 To PivotLevel
            Dim oGridViewRow As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
            Dim Header = (row.Cells.Cast(Of TableCell)().[Select](Function(x) GetHeaderText(x.Text, iCount, PivotLevel))).GroupBy(Function(x) x)

            For Each v In Header
                Dim cell As New TableHeaderCell()
                If iContaColonneHeader > 0 Then
                    cell.Text = v.Key.Substring(v.Key.LastIndexOf(m_Separator) + 1)
                Else
                    cell.Text = ""
                End If

                cell.ColumnSpan = v.Count()

                oGridViewRow.Cells.Add(cell)
                iContaColonneHeader += 1
            Next
            gv.Controls(0).Controls.AddAt(row.RowIndex, oGridViewRow)
        Next
        row.Visible = False
    End Sub

    Private Function GetHeaderText(ByVal s As String, ByVal i As Integer, ByVal PivotLevel As Integer) As String
        If Not s.Contains(m_Separator) AndAlso i <> PivotLevel Then
            Return String.Empty
        Else
            Dim Index As Integer = NthIndexOf(s, m_Separator, i)
            If Index = -1 Then
                Return s
            End If
            Return s.Substring(0, Index)
        End If
    End Function

    Private Function NthIndexOf(ByVal str As String, ByVal SubString As String, ByVal n As Integer) As Integer
        Dim x As Integer = -1
        For i As Integer = 0 To n - 1
            x = str.IndexOf(SubString, x + 1)
            If x = -1 Then
                Return x
            End If
        Next
        Return x
    End Function

    Private Sub Finance_Player_ProfiLossC_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        m_SessionData = Session("SessionData")

        m_SiteMaster = Me.Master

        m_SiteMaster.LoadPeriodsValid = Session("IDRole") = "P"

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oCBOPeriod As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOPeriodValid As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriodValid"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler m_SiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler m_SiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(Session("IDGame")) ' Ultimo periodo
        oCBOPeriod.SelectedValue = Session("IDPeriodValid")
        m_SiteMaster.PeriodChange = Session("IDPeriodValid")

        Session("NameTeam") = GetTeamName(Session("IDTeam"))
        oCBOTeam.SelectedValue = Session("IDTeam")
        m_SiteMaster.TeamChange = Session("IDTeam")

        Message.Visible = False
    End Sub

    Private Sub LoadPivotGrid(NomeVariabile As String, RowField As String, IDPivotGrid As String, TitoloPivotGrid As String)
        Dim sSQL As String
        Dim oDAL As New DBHelper
        Dim oDTDataSourceGrid As DataTable
        Dim iPeriodo As Integer

        If Session("IDRole").Contains("B") Or Session("IDRole").Contains("D") Or Session("IDRole").Contains("T") Then
            iPeriodo = m_SiteMaster.PeriodGetCurrent
        Else
            iPeriodo = m_SiteMaster.PeriodGetPrev
        End If
        ' Carico Totale macchinari attivi
        sSQL = "SELECT VCD.VariableName, ISNULL(V.VariableLabel, '" & TitoloPivotGrid & "') AS " & RowField & ", VCV.IDPlayer, VCV.IDItem, VCV.IDAge, VCV.Value, 0.0 AS ValueDef, " _
             & "ISNULL(T.TeamName, '') AS TeamName, I.VariableLabel AS Item " _
             & "FROM Variables_Calculate_Define VCD " _
             & "LEFT JOIN Variables_Calculate_Value VCV ON VCD.id = VCV.IDVariable " _
             & "LEFT JOIN Variables V ON VCD.VariableName = V.VariableName AND VCD.IDGame = V.IDGame " _
             & "LEFT JOIN BGOL_Teams T ON VCV.IDPlayer = T.Id " _
             & "LEFT JOIN vItems I ON VCV.IDItem = I.IDVariable " _
             & "WHERE VCD.IDGame = " & Nni(Session("IDGame")) & " AND VCD.VariableName = '" & NomeVariabile & "' AND VCV.IDPeriod = " & iPeriodo
        If Session("IDRole").Contains("P") Then
            sSQL &= "AND VCV.IDPlayer = " & Session("IDTeam")
        End If
        oDTDataSourceGrid = oDAL.ExecuteDataTable(sSQL)

        ' Converto i valori dei risultati da varchar a numerici con con decimali
        For Each oRow As DataRow In oDTDataSourceGrid.Rows
            If Nz(oRow("Value")) = "" Then
                oRow("Value") = "0"
            End If
            oRow("ValueDef") = Math.Round(Nn(oRow("Value"), "0"), 2)
            ' Controllo che RowField non sia blank
            If Nz(oRow(RowField)) = "" Then
                oRow(RowField) = TitoloPivotGrid
            End If
        Next

        Dim oPivot As New Pivot(oDTDataSourceGrid)
        Dim dtPivot As New DataTable

        ' Imposto le visualizzazione sulla base del nome della variabile
        Select Case NomeVariabile
            Case "BeginRawValue", "ValueStockBegin"
                dtPivot = oPivot.PivotData("TeamName", "ValueDef", AggregateFunction.Sum, "Item")

            Case Else
                If Nz(oDTDataSourceGrid.Rows(0)("TeamName")) <> "" Then
                    dtPivot = oPivot.PivotData("TeamName", "ValueDef", AggregateFunction.Sum, RowField)

                ElseIf Nz(oDTDataSourceGrid.Rows(0)("Item")) <> "" Then
                    dtPivot = oPivot.PivotData("Item", "ValueDef", AggregateFunction.Sum, RowField)

                End If

        End Select

        Dim oGridView As New GridView
        oGridView.ID = IDPivotGrid
        AddHandler oGridView.RowCreated, AddressOf grdPivot_RowCreated
        AddHandler oGridView.RowDataBound, AddressOf grdPivot_RowDataBound
        oGridView.DataSource = dtPivot
        oGridView.DataBind()

        Dim oLabel As New Label
        oLabel.Text = TitoloPivotGrid
        divTableMaster.Controls.Add(oLabel)
        divTableMaster.Controls.Add(oGridView)

        Dim ControlRow As New Literal()
        ControlRow.Text = "<hr/> <br/>"

        divTableMaster.Controls.Add(ControlRow)

        oPivot = Nothing
    End Sub

    Private Sub LoadData()
        Try
            ' Rimuovo i controlli dal DIV di gestione della tabella Pivot
            If divTableMaster.Controls.Count > 0 Then
                For Each oControl As Control In divTableMaster.Controls
                    divTableMaster.Controls.Remove(oControl)
                Next
            End If

            ' CumulRevenTotal
            LoadPivotGrid("CumulRevenTotal", "CumulRevenTotal", "CumulRevenTotal", "Total revenues (CumulRevenTotal)")

            ' DischRevenTotal
            LoadPivotGrid("DischRevenTotal", "DischRevenTotal", "DischRevenTotal", "Total revenues (D)")

            ' CumulDeltaRaw
            LoadPivotGrid("CumulDeltaRaw", "CumulDeltaRaw", "CumulDeltaRaw", "Delta raw materials (CumulDeltaRaw)")

            ' DischDeltaRaw
            LoadPivotGrid("DischDeltaRaw", "DischDeltaRaw", "DischDeltaRaw", "Delta raw materials (D)")

            ' CumulDeltaStock 
            LoadPivotGrid("CumulDeltaStock", "CumulDeltaStock", "CumulDeltaStock", "Delta stock of finished goods (CumulDeltaStock)")

            ' DischDeltaStock
            LoadPivotGrid("DischDeltaStock", "DischDeltaStock", "DischDeltaStock", "Delta stock of finished goods (D)")

            ' CumulCostPurch
            LoadPivotGrid("CumulCostPurch", "CumulCostPurch", "CumulCostPurch", "Cost of purchase (CumulCostPurch)")

            ' DischCostPurch
            LoadPivotGrid("DischCostPurch", "DischCostPurch", "DischCostPurch", "Cost of purchase (D)")

            ' CumulDirecCosts
            LoadPivotGrid("CumulDirecCosts", "CumulDirecCosts", "CumulDirecCosts", "Direct costs (CumulDirecCosts)")

            ' DischDirecCosts
            LoadPivotGrid("DischDirecCosts", "DischDirecCosts", "DischDirecCosts", "Direct costs (D)")

            ' CumulSinkiPlant
            LoadPivotGrid("CumulSinkiPlant", "CumulSinkiPlant", "CumulSinkiPlant", "Plant depreciation (CumulSinkiPlant)")

            ' DischSinkiPlant
            LoadPivotGrid("DischSinkiPlant", "DischSinkiPlant", "DischSinkiPlant", "Plant depreciation (D)")

            ' CumulLeasiCosts
            LoadPivotGrid("CumulLeasiCosts", "CumulLeasiCosts", "CumulLeasiCosts", "Leasing (CumulLeasiCosts)")

            ' DischLeasiCosts
            LoadPivotGrid("DischLeasiCosts", "DischLeasiCosts", "DischLeasiCosts", "Leasing (D)")

            ' CumulOutleCosts
            LoadPivotGrid("CumulOutleCosts", "CumulOutleCosts", "CumulOutleCosts", "Outlets (CumulOutleCosts)")

            ' DischOutleCosts
            LoadPivotGrid("DischOutleCosts", "DischOutleCosts", "DischOutleCosts", "Outlets (D)")

            ' CumulExtraProdu
            LoadPivotGrid("CumulExtraProdu", "CumulExtraProdu", "CumulExtraProdu", "Bought-in (CumulExtraProdu)")

            ' DischExtraProdu
            LoadPivotGrid("DischExtraProdu", "DischExtraProdu", "DischExtraProdu", "Bought-in (D)")

            ' CumulWarehCosts
            LoadPivotGrid("CumulWarehCosts", "CumulWarehCosts", "CumulWarehCosts", "Warehouse (CumulWarehCosts)")

            ' DischWarehCosts
            LoadPivotGrid("DischWarehCosts", "DischWarehCosts", "DischWarehCosts", "Warehouse (D)")

            ' CumulTransCosts
            LoadPivotGrid("CumulTransCosts", "CumulTransCosts", "CumulTransCosts", "Transport (CumulTransCosts)")

            ' DischTransCosts
            LoadPivotGrid("DischTransCosts", "DischTransCosts", "DischTransCosts", "Transport (D)")

            ' CumulSinkiVehic
            LoadPivotGrid("CumulSinkiVehic", "CumulSinkiVehic", "AmmortamentoDeiVeicoli", "Vehicles depreciation (C)")

            ' DischSinkiVehic
            LoadPivotGrid("DischSinkiVehic", "DischSinkiVehic", "DischSinkiVehic", "Vehicles depreciation (D)")

            ' CumulFixedCosts
            LoadPivotGrid("CumulFixedCosts", "CumulFixedCosts", "CumulFixedCosts", "Fixed costs (CumulFixedCosts)")

            ' DischCostsFixed
            LoadPivotGrid("DischCostsFixed", "DischCostsFixed", "DischCostsFixed", "Fixed costs (D)")

            ' DischFixedCosts
            LoadPivotGrid("DischFixedCosts", "DischFixedCosts", "DischFixedCosts", "Fixed costs (D)")

            ' CumulPersoCosts
            LoadPivotGrid("CumulPersoCosts", "CumulPersoCosts", "CumulPersoCosts", "Human resources (CumulPersoCosts)")

            ' DischPersoCosts
            LoadPivotGrid("DischPersoCosts", "DischPersoCosts", "DischPersoCosts", "Human resources (D)")

            ' CumulExpenCosts
            LoadPivotGrid("CumulExpenCosts", "CumulExpenCosts", "CumulExpenCosts", "Total expenses (CumulExpenCosts)")

            ' DischExpenCosts
            LoadPivotGrid("DischExpenCosts", "DischExpenCosts", "DischExpenCosts", "Total expenses (D)")

            ' CumulDebtsOff
            LoadPivotGrid("CumulDebtsOff", "CumulDebtsOff", "MinusvalenzeDaAlienazioneMacchinari", "Debts written off / loss on sale (C)")

            ' DischDebtsOff
            LoadPivotGrid("DischDebtsOff", "DischDebtsOff", "DischDebtsOff", "Debts written off / loss on sale (D)")

            ' CumulPositInter
            LoadPivotGrid("CumulPositInter", "CumulPositInter", "CumulPositInter", "Interests (CumulPositInter)")

            ' DischPositInter
            LoadPivotGrid("DischPositInter", "DischPositInter", "DischPositInter", "Interests (+) (D)")

            ' CumulNegatInter
            LoadPivotGrid("CumulNegatInter", "CumulNegatInter", "CumulNegatInter", "Loans Interests (CumulNegatInter)")

            ' DischNegatInter
            LoadPivotGrid("DischNegatInter", "DischNegatInter", "DischNegatInter", "Interests (-) (D)")

            ' CumulBeforTax
            LoadPivotGrid("CumulBeforTax", "CumulBeforTax", "CumulBeforTax", "Profit before tax (CumulBeforTax)")

            ' DischBeforTax
            LoadPivotGrid("DischBeforTax", "DischBeforTax", "DischBeforTax", "Profit before tax (D)")

            ' CumulCostsTechn
            LoadPivotGrid("CumulCostsTechn", "CumulCostsTechn", "CumulCostsTechn", "CumulCostsTechn")

            ' DischCostsTechn
            LoadPivotGrid("DischCostsTechn", "DischCostsTechn", "DischCostsTechn", "DischCostsTechn")

            Message.Visible = False

        Catch ex As Exception
            MessageText.Text = "Error: <br/> " & ex.Message
            Message.Visible = True
        End Try

    End Sub

    Private Sub Finance_Player_ProfitLossCumulative_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        If Not Page.IsPostBack Then
            If Session("IDRole") = "P" Then
                m_SiteMaster.HandleReloadPeriods(HandleGetMaxPeriodValid(Session("IDGame")))
            End If
            LoadData()
        End If
    End Sub

End Class
