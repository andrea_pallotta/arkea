﻿
Imports APS
Imports APPCore.Utility
Imports Telerik.Web.UI

Partial Class Finance_Player_Profit_Loss
    Inherits System.Web.UI.Page

    Private m_SiteMaster As SiteMaster

    Private m_SessionData As MU_SessionData
    Private m_Separator As String = "."

    Private m_FatturatoTotale As Double
    Private m_FatturatoTotaleCum As Double

    Private m_CostoVenduto As Double
    Private m_CostoVendutoCum As Double

    Private m_CostoProduzione As Double
    Private m_CostoProduzioneCum As Double

    Private m_CostoLogistica As Double
    Private m_CostoLogisticaCum As Double

    Private m_CostoCommerciale As Double
    Private m_CostoCommercialeCum As Double

    Private m_IDPeriodoMax As Integer

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadData()
        UpdatePanelMain.Update()
    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadData()
        UpdatePanelMain.Update()
    End Sub

    Private Sub Finance_Player_Balance_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        m_SessionData = Session("SessionData")

        m_SiteMaster = Me.Master

        m_SiteMaster.LoadPeriodsValid = Session("IDRole") = "P"

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oCBOPeriod As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOPeriodValid As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriodValid"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler m_SiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler m_SiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(Session("IDGame")) ' Ultimo periodo
        oCBOPeriod.SelectedValue = Session("IDPeriodValid")
        m_SiteMaster.PeriodChange = Session("IDPeriodValid")

        Session("NameTeam") = GetTeamName(Session("IDTeam"))
        oCBOTeam.SelectedValue = Session("IDTeam")
        m_SiteMaster.TeamChange = Session("IDTeam")

        Message.Visible = False

        ' Gestione del pannello delle decisioni concesse
        modalHelp.OpenerElementID = lnkHelp.ClientID
        modalHelp.Modal = False
        modalHelp.VisibleTitlebar = True

        LoadHelp()
    End Sub

    Private Sub LoadData()
        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As TableHeaderCell

        Try
            tblProfitLoss.Rows.Clear()

            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima colonna
            tblHeaderCell = New TableHeaderCell
            tblHeaderCell.Text = " "
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.Style.Add("width", "60%")
            tblHeaderCell.Style.Add("background", "transparent")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            ' Seconda colonna
            tblHeaderCell = New TableHeaderCell
            Dim oLBLQuarter As New RadLabel
            oLBLQuarter.Text = "QUARTER"
            oLBLQuarter.ForeColor = Drawing.Color.White
            tblHeaderCell.Controls.Add(oLBLQuarter)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.Style.Add("width", "20%")
            tblHeaderCell.Style.Add("background", "#525252")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            ' Terza colonna
            tblHeaderCell = New TableHeaderCell
            Dim oLBLYEARCumulative As New RadLabel
            oLBLYEARCumulative.Text = "YEAR CUMULATIVE"
            oLBLYEARCumulative.ForeColor = Drawing.Color.White
            tblHeaderCell.Controls.Add(oLBLYEARCumulative)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.Style.Add("background", "#525252")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            tblHeaderRow.BorderStyle = BorderStyle.None
            tblHeaderRow.Style.Add("margin-top", "5px")

            tblProfitLoss.Rows.Add(tblHeaderRow)

            TotalSalesRevenues()
            ProductionOverHeads()
            LogisticsCosts()
            CommercialCosts()
            OperatingProfit()
            Finance()
            TotalCosts()
            Tax()

        Catch ex As Exception
            MessageText.Text = "Error: <br/> " & ex.Message
            Message.Visible = True
        End Try

    End Sub

    Private Sub TotalSalesRevenues()
        Dim tblRow As TableRow
        Dim tblCell As TableCell

        ' Aggiungo le righe di riepilogo TOTAL SALES REVENUES 
        tblRow = New TableRow
        tblCell = New TableCell
        Dim oLBLTotSalesRev As New RadLabel
        oLBLTotSalesRev.ID = "lblTotSalesRev"
        oLBLTotSalesRev.Text = "TOTAL SALES REVENUES"
        oLBLTotSalesRev.Style.Add("font-size", "8pt")
        oLBLTotSalesRev.Style.Add("text-align", "left")
        oLBLTotSalesRev.Style.Add("font-weight", "bold")
        oLBLTotSalesRev.Style.Add("color", "#ffffff")
        tblCell.Controls.Add(oLBLTotSalesRev)
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("padding-left", "20px")
        tblCell.Style.Add("background", "#3e6c86")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim oLBLTotFatt As New RadLabel
        oLBLTotFatt.Text = Nn(GetVariableDefineValue("FattuTotalPerio", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")

        Dim lnkFattuTotalPerio As New HyperLink
        lnkFattuTotalPerio.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=FattuTotalPerio&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkFattuTotalPerio.Style.Add("text-decoration", "none")
        lnkFattuTotalPerio.Style.Add("cursor", "pointer")

        lnkFattuTotalPerio.Text = Nn(GetVariableDefineValue("FattuTotalPerio", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
        lnkFattuTotalPerio.Style.Add("font-size", "8pt")
        lnkFattuTotalPerio.Style.Add("text-align", "right")
        lnkFattuTotalPerio.Style.Add("color", "#ffffff")
        lnkFattuTotalPerio.BackColor = Drawing.Color.Transparent

        tblCell.Controls.Add(lnkFattuTotalPerio)

        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("text-align", "right")
        tblCell.Style.Add("background", "#3e6c86")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell

        Dim lnkFattuTotalCumul As New HyperLink
        lnkFattuTotalCumul.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=FattuTotalCumul&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkFattuTotalCumul.Style.Add("text-decoration", "none")
        lnkFattuTotalCumul.Style.Add("cursor", "pointer")

        lnkFattuTotalCumul.Text = Nn(GetVariableDefineValue("FattuTotalCumul", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")

        lnkFattuTotalCumul.Style.Add("font-size", "8pt")
        lnkFattuTotalCumul.Style.Add("text-align", "right")
        lnkFattuTotalCumul.Style.Add("color", "#ffffff")
        tblCell.Controls.Add(lnkFattuTotalCumul)

        tblCell.Style.Add("text-align", "right")
        tblCell.Style.Add("background", "#3e6c86")
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblProfitLoss.Rows.Add(tblRow)

        ' RIGHE DI DETTAGLIO
        ' PRIMA RIGA
        tblRow = New TableRow
        tblCell = New TableCell
        Dim oLBLRawMaterialsBought As New RadLabel
        oLBLRawMaterialsBought.Text = "Raw Materials Bought"
        oLBLRawMaterialsBought.ID = "oLBLRawMaterialsBought"
        oLBLRawMaterialsBought.Style.Add("font-size", "8pt")
        oLBLRawMaterialsBought.Style.Add("text-align", "left")
        oLBLRawMaterialsBought.Style.Add("font-weight", "normal")
        tblCell.Controls.Add(oLBLRawMaterialsBought)
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("padding-left", "5px")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        ' GetVariableDefineValue("CostPurchTotal", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))
        Dim lnkCostPurchTotal As New HyperLink
        lnkCostPurchTotal.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=CostPurchTotal&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkCostPurchTotal.Style.Add("text-decoration", "none")
        lnkCostPurchTotal.Style.Add("cursor", "pointer")

        lnkCostPurchTotal.Text = Nn(GetVariableDefineValue("CostPurchTotal", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
        lnkCostPurchTotal.Style.Add("font-size", "8pt")
        lnkCostPurchTotal.Style.Add("text-align", "right")
        lnkCostPurchTotal.BackColor = Drawing.Color.Transparent
        tblCell.Controls.Add(lnkCostPurchTotal)

        tblCell.Controls.Add(lnkCostPurchTotal)

        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("text-align", "right")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim lnkCumulCostPurch As New HyperLink

        lnkCumulCostPurch.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=CumulCostPurch&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkCumulCostPurch.Style.Add("text-decoration", "none")
        lnkCumulCostPurch.Style.Add("cursor", "pointer")

        lnkCumulCostPurch.Text = Nn(GetVariableDefineValue("CumulCostPurch", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
        lnkCumulCostPurch.Style.Add("font-size", "8pt")
        lnkCumulCostPurch.Style.Add("text-align", "right")

        tblCell.Controls.Add(lnkCumulCostPurch)

        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblProfitLoss.Rows.Add(tblRow)
        ' ---------------------------------------------------------------------------------------------------------

        ' SECONDA RIGA
        tblRow = New TableRow
        tblCell = New TableCell
        Dim oLBLRawMaterialeHeldInStock As New RadLabel
        oLBLRawMaterialeHeldInStock.ID = "oLBLRawMaterialeHeldInStock"
        oLBLRawMaterialeHeldInStock.Text = "Adjust for raw materials held in stock"
        oLBLRawMaterialeHeldInStock.Style.Add("font-size", "8pt")
        oLBLRawMaterialeHeldInStock.Style.Add("text-align", "left")
        oLBLRawMaterialeHeldInStock.Style.Add("font-weight", "normal")
        tblCell.Controls.Add(oLBLRawMaterialeHeldInStock)
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("padding-left", "5px")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell

        Dim lnkDeltaRawValue As New HyperLink
        lnkDeltaRawValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=DeltaRawValue&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkDeltaRawValue.Style.Add("text-decoration", "none")
        lnkDeltaRawValue.Style.Add("cursor", "pointer")

        lnkDeltaRawValue.Text = Nn(GetVariableDefineValue("DeltaRawValue", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
        lnkDeltaRawValue.Style.Add("font-size", "8pt")
        lnkDeltaRawValue.Style.Add("text-align", "right")
        lnkDeltaRawValue.BackColor = Drawing.Color.Transparent

        tblCell.Controls.Add(lnkDeltaRawValue)

        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("text-align", "right")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell

        Dim lnkCumulDeltaRaw As New HyperLink
        lnkCumulDeltaRaw.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=CumulDeltaRaw&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkCumulDeltaRaw.Style.Add("text-decoration", "none")
        lnkCumulDeltaRaw.Style.Add("cursor", "pointer")

        lnkCumulDeltaRaw.Text = Nn(GetVariableDefineValue("CumulDeltaRaw", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")

        lnkCumulDeltaRaw.Style.Add("font-size", "8pt")
        lnkCumulDeltaRaw.Style.Add("text-align", "right")
        tblCell.Controls.Add(lnkCumulDeltaRaw)

        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblProfitLoss.Rows.Add(tblRow)
        ' ---------------------------------------------------------------------------------------------------------

        ' TERZA RIGA
        tblRow = New TableRow
        tblCell = New TableCell
        Dim oLBLProductionDirectCost As New RadLabel
        oLBLProductionDirectCost.Text = "Production direct cost"
        oLBLProductionDirectCost.ID = "oLBLProductionDirectCost"
        oLBLProductionDirectCost.Style.Add("font-size", "8pt")
        oLBLProductionDirectCost.Style.Add("text-align", "left")
        oLBLProductionDirectCost.Style.Add("font-weight", "normal")
        tblCell.Controls.Add(oLBLProductionDirectCost)
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("padding-left", "5px")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell

        Dim lnkDirecTotalCosts As New HyperLink
        lnkDirecTotalCosts.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=DirecTotalCosts&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkDirecTotalCosts.Style.Add("text-decoration", "none")
        lnkDirecTotalCosts.Style.Add("cursor", "pointer")

        lnkDirecTotalCosts.Text = Nn(GetVariableDefineValue("DirecTotalCosts", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
        lnkDirecTotalCosts.Style.Add("font-size", "8pt")
        lnkDirecTotalCosts.Style.Add("text-align", "right")
        lnkDirecTotalCosts.BackColor = Drawing.Color.Transparent
        tblCell.Controls.Add(lnkDirecTotalCosts)

        tblCell.Controls.Add(lnkDirecTotalCosts)
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("text-align", "right")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell

        Dim lnkCumulDirecCosts As New HyperLink
        lnkCumulDirecCosts.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=CumulDirecCosts&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkCumulDirecCosts.Style.Add("text-decoration", "none")
        lnkCumulDirecCosts.Style.Add("cursor", "pointer")

        lnkCumulDirecCosts.Text = Nn(GetVariableDefineValue("CumulDirecCosts", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")

        lnkCumulDirecCosts.Style.Add("font-size", "8pt")
        lnkCumulDirecCosts.Style.Add("text-align", "right")

        tblCell.Controls.Add(lnkCumulDirecCosts)

        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblProfitLoss.Rows.Add(tblRow)
        ' ---------------------------------------------------------------------------------------------------------

        ' QUARTA RIGA
        tblRow = New TableRow
        tblCell = New TableCell
        Dim oLBLBoughtinFinishGoods As New RadLabel
        oLBLBoughtinFinishGoods.Text = "Bought-in  finished goods"
        oLBLBoughtinFinishGoods.ID = "oLBLBoughtinFinishGoods"
        oLBLBoughtinFinishGoods.Style.Add("font-size", "8pt")
        oLBLBoughtinFinishGoods.Style.Add("text-align", "left")
        oLBLBoughtinFinishGoods.Style.Add("font-weight", "normal")
        tblCell.Controls.Add(oLBLBoughtinFinishGoods)
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("padding-left", "5px")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell

        Dim lnkExtraMoneyAllow As New HyperLink
        lnkExtraMoneyAllow.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=ExtraMoneyAllow&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkExtraMoneyAllow.Style.Add("text-decoration", "none")
        lnkExtraMoneyAllow.Style.Add("cursor", "pointer")

        lnkExtraMoneyAllow.Text = Nn(GetVariableDefineValue("ExtraMoneyAllow", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
        lnkExtraMoneyAllow.Style.Add("font-size", "8pt")
        lnkExtraMoneyAllow.Style.Add("text-align", "right")
        lnkExtraMoneyAllow.BackColor = Drawing.Color.Transparent

        tblCell.Controls.Add(lnkExtraMoneyAllow)

        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("text-align", "right")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim lnkCumulExtraProdu As New HyperLink
        lnkCumulExtraProdu.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=CumulExtraProdu&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkCumulExtraProdu.Style.Add("text-decoration", "none")
        lnkCumulExtraProdu.Style.Add("cursor", "pointer")

        lnkCumulExtraProdu.Text = Nn(GetVariableDefineValue("CumulExtraProdu", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
        lnkCumulExtraProdu.Style.Add("font-size", "8pt")
        lnkCumulExtraProdu.Style.Add("text-align", "right")

        tblCell.Controls.Add(lnkCumulExtraProdu)

        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblProfitLoss.Rows.Add(tblRow)
        ' ---------------------------------------------------------------------------------------------------------

        ' QUINTA RIGA
        tblRow = New TableRow
        tblCell = New TableCell
        Dim oLBLFinishGoodsStock As New RadLabel
        oLBLFinishGoodsStock.Text = "Adjust for finished goods held in stock"
        oLBLFinishGoodsStock.ID = "oLBLFinishGoodsStock"
        oLBLFinishGoodsStock.Style.Add("font-size", "8pt")
        oLBLFinishGoodsStock.Style.Add("text-align", "left")
        oLBLFinishGoodsStock.Style.Add("font-weight", "normal")
        tblCell.Controls.Add(oLBLFinishGoodsStock)
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("padding-left", "5px")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell

        Dim lnkDeltaStockValue As New HyperLink
        lnkDeltaStockValue.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=DeltaStockValue&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkDeltaStockValue.Style.Add("text-decoration", "none")
        lnkDeltaStockValue.Style.Add("cursor", "pointer")

        lnkDeltaStockValue.Text = Nn(GetVariableDefineValue("DeltaStockValue", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")

        lnkDeltaStockValue.Style.Add("font-size", "8pt")
        lnkDeltaStockValue.Style.Add("text-align", "right")
        lnkDeltaStockValue.BackColor = Drawing.Color.Transparent
        tblCell.Controls.Add(lnkDeltaStockValue)

        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("text-align", "right")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim lnkCumulDeltaStock As New HyperLink
        lnkCumulDeltaStock.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=CumulDeltaStock&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkCumulDeltaStock.Style.Add("text-decoration", "none")
        lnkCumulDeltaStock.Style.Add("cursor", "pointer")

        lnkCumulDeltaStock.Text = Nn(GetVariableDefineValue("CumulDeltaStock", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
        lnkCumulDeltaStock.Style.Add("font-size", "8pt")
        lnkCumulDeltaStock.Style.Add("text-align", "right")
        tblCell.Controls.Add(lnkCumulDeltaStock)

        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblProfitLoss.Rows.Add(tblRow)
        ' ---------------------------------------------------------------------------------------------------------

        ' INSERISCO LA RIGA COST OF GOODS SOLD
        tblRow = New TableRow
        tblCell = New TableCell
        Dim oLBLCostGoodsSold As New RadLabel
        oLBLCostGoodsSold.Text = "COST OF GOODS SOLD"
        oLBLCostGoodsSold.ID = "oLBLCostGoodsSold"
        oLBLCostGoodsSold.Style.Add("font-size", "8pt")
        oLBLCostGoodsSold.Style.Add("text-align", "left")
        oLBLCostGoodsSold.Style.Add("font-weight", "bold")
        tblCell.Controls.Add(oLBLCostGoodsSold)
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("padding-left", "5px")
        tblCell.Style.Add("background", "#e7e7e7")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell

        Dim lnkCostoVenduto As New HyperLink
        lnkCostoVenduto.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=CostoVenduto&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkCostoVenduto.Style.Add("text-decoration", "none")
        lnkCostoVenduto.Style.Add("cursor", "pointer")

        lnkCostoVenduto.Text = Nn(GetVariableDefineValue("CostoVenduto", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
        lnkCostoVenduto.Style.Add("font-size", "8pt")
        lnkCostoVenduto.Style.Add("text-align", "right")
        tblCell.Controls.Add(lnkCostoVenduto)

        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("background", "#e7e7e7")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell

        Dim lnkCostoVenduCumul As New HyperLink
        lnkCostoVenduCumul.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=CostoVenduCumul&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkCostoVenduCumul.Style.Add("text-decoration", "none")
        lnkCostoVenduCumul.Style.Add("cursor", "pointer")

        lnkCostoVenduCumul.Text = Nn(GetVariableDefineValue("CostoVenduCumul", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
        lnkCostoVenduCumul.Style.Add("font-size", "8pt")
        lnkCostoVenduCumul.Style.Add("text-align", "right")
        tblCell.Controls.Add(lnkCostoVenduCumul)

        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("background", "#e7e7e7")
        tblRow.Cells.Add(tblCell)

        tblProfitLoss.Rows.Add(tblRow)
        ' ---------------------------------------------------------------------------------------------------------

        ' INSERISCO LA RIGA GROSS PROFIT
        tblRow = New TableRow
        tblCell = New TableCell
        Dim oLBLGrossProfit As New RadLabel
        oLBLGrossProfit.Text = "GROSS PROFIT"
        oLBLGrossProfit.ID = "oLBLGrossProfit"
        oLBLGrossProfit.Style.Add("font-size", "8pt")
        oLBLGrossProfit.Style.Add("text-align", "left")
        oLBLGrossProfit.Style.Add("font-weight", "bold")
        tblCell.Controls.Add(oLBLGrossProfit)
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("padding-left", "5px")
        tblCell.Style.Add("background", "#e7e7e7")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim lnkGrossProfit As New HyperLink
        lnkGrossProfit.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=GrossProfit&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkGrossProfit.Style.Add("text-decoration", "none")
        lnkGrossProfit.Style.Add("cursor", "pointer")

        lnkGrossProfit.Text = Nn(GetVariableDefineValue("GrossProfit", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
        lnkGrossProfit.Style.Add("font-size", "8pt")
        lnkGrossProfit.Style.Add("text-align", "right")
        tblCell.Controls.Add(lnkGrossProfit)

        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("background", "#e7e7e7")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim lnkCumulGrossProfit As New HyperLink
        lnkCumulGrossProfit.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=ProduBasicRequi&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkCumulGrossProfit.Style.Add("text-decoration", "none")
        lnkCumulGrossProfit.Style.Add("cursor", "pointer")

        lnkCumulGrossProfit.Text = Nn(GetVariableDefineValue("CumulGrossProfit", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
        lnkCumulGrossProfit.Style.Add("font-size", "8pt")
        lnkCumulGrossProfit.Style.Add("text-align", "right")
        tblCell.Controls.Add(lnkCumulGrossProfit)

        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("background", "#e7e7e7")
        tblRow.Cells.Add(tblCell)

        tblProfitLoss.Rows.Add(tblRow)
    End Sub

    Private Sub ProductionOverHeads()
        Dim tblRow As TableRow
        Dim tblCell As TableCell

        ' PERIODO
        Dim LeasiTotalCost As Double = GetVariableDefineValue("LeasiTotalCost", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))
        Dim TechnAutomAllow As Double = GetVariableDefineValue("TechnAutomAllow", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))

        ' CUMULATO 
        Dim CumulLeasiCosts As Double = GetVariableDefineValue("CumulLeasiCosts", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))
        Dim CumulCostsTechn As Double = GetVariableDefineValue("CumulCostsTechn", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))

        ' RIGHE DI DETTAGLIO
        ' PRIMA RIGA
        tblRow = New TableRow
        tblCell = New TableCell
        Dim oLBLProductionFixedCost As New RadLabel
        oLBLProductionFixedCost.Text = "Production fixed cost"
        oLBLProductionFixedCost.ID = "oLBLProductionFixedCost"
        oLBLProductionFixedCost.Style.Add("font-size", "8pt")
        oLBLProductionFixedCost.Style.Add("text-align", "left")
        oLBLProductionFixedCost.Style.Add("font-weight", "normal")
        tblCell.Controls.Add(oLBLProductionFixedCost)
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("padding-left", "5px")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell

        Dim lnkFixedProduCosts As New HyperLink
        lnkFixedProduCosts.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=FixedProduCosts&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkFixedProduCosts.Style.Add("text-decoration", "none")
        lnkFixedProduCosts.Style.Add("cursor", "pointer")

        lnkFixedProduCosts.Text = Nn(GetVariableDefineValue("FixedProduCosts", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
        lnkFixedProduCosts.Style.Add("font-size", "8pt")
        lnkFixedProduCosts.Style.Add("text-align", "right")
        lnkFixedProduCosts.BackColor = Drawing.Color.Transparent

        tblCell.Controls.Add(lnkFixedProduCosts)

        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("text-align", "right")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell

        Dim lnkCumulFixedCosts As New HyperLink
        lnkCumulFixedCosts.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=CumulFixedCosts&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkCumulFixedCosts.Style.Add("text-decoration", "none")
        lnkCumulFixedCosts.Style.Add("cursor", "pointer")

        lnkCumulFixedCosts.Text = Nn(GetVariableDefineValue("CumulFixedCosts", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
        lnkCumulFixedCosts.Style.Add("font-size", "8pt")
        lnkCumulFixedCosts.Style.Add("text-align", "right")

        tblCell.Controls.Add(lnkCumulFixedCosts)

        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblProfitLoss.Rows.Add(tblRow)
        ' ---------------------------------------------------------------------------------------------------------

        ' SECONDA RIGA
        tblRow = New TableRow
        tblCell = New TableCell
        Dim oLBLMachineDepraciation As New RadLabel
        oLBLMachineDepraciation.Text = "Machine depreciation"
        oLBLMachineDepraciation.ID = "oLBLMachineDepraciation"
        oLBLMachineDepraciation.Style.Add("font-size", "8pt")
        oLBLMachineDepraciation.Style.Add("text-align", "left")
        oLBLMachineDepraciation.Style.Add("font-weight", "normal")
        tblCell.Controls.Add(oLBLMachineDepraciation)
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("padding-left", "5px")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell

        Dim lnkSinkiFundPlant As New HyperLink
        lnkSinkiFundPlant.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=SinkiFundPlant&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkSinkiFundPlant.Style.Add("text-decoration", "none")
        lnkSinkiFundPlant.Style.Add("cursor", "pointer")

        lnkSinkiFundPlant.Text = Nn(GetVariableDefineValue("SinkiFundPlant", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
        lnkSinkiFundPlant.Style.Add("font-size", "8pt")
        lnkSinkiFundPlant.Style.Add("text-align", "right")
        lnkSinkiFundPlant.BackColor = Drawing.Color.Transparent

        tblCell.Controls.Add(lnkSinkiFundPlant)

        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("text-align", "right")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell

        Dim lnkCumulSinkiPlant As New HyperLink
        lnkCumulSinkiPlant.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=CumulSinkiPlant&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkCumulSinkiPlant.Style.Add("text-decoration", "none")
        lnkCumulSinkiPlant.Style.Add("cursor", "pointer")

        lnkCumulSinkiPlant.Text = Nn(GetVariableDefineValue("CumulSinkiPlant", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
        lnkCumulSinkiPlant.Style.Add("font-size", "8pt")
        lnkCumulSinkiPlant.Style.Add("text-align", "right")

        tblCell.Controls.Add(lnkCumulSinkiPlant)

        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblProfitLoss.Rows.Add(tblRow)
        ' ---------------------------------------------------------------------------------------------------------

        ' TERZA RIGA
        tblRow = New TableRow
        tblCell = New TableCell
        Dim oLBLLeasedMachineCost As New RadLabel
        oLBLLeasedMachineCost.Text = "Leased Machine Cost"
        oLBLLeasedMachineCost.ID = "oLBLLeasedMachineCost"
        oLBLLeasedMachineCost.Style.Add("font-size", "8pt")
        oLBLLeasedMachineCost.Style.Add("text-align", "left")
        oLBLLeasedMachineCost.Style.Add("font-weight", "normal")
        tblCell.Controls.Add(oLBLLeasedMachineCost)
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("padding-left", "5px")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell

        Dim lnkLeasiTotalCost As New HyperLink
        lnkLeasiTotalCost.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=LeasiTotalCost&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkLeasiTotalCost.Style.Add("text-decoration", "none")
        lnkLeasiTotalCost.Style.Add("cursor", "pointer")

        lnkLeasiTotalCost.Text = LeasiTotalCost.ToString("N2")
        lnkLeasiTotalCost.Style.Add("font-size", "8pt")
        lnkLeasiTotalCost.Style.Add("text-align", "right")
        lnkLeasiTotalCost.BackColor = Drawing.Color.Transparent

        tblCell.Controls.Add(lnkLeasiTotalCost)

        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("text-align", "right")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell

        Dim lnkCumulLeasiCosts As New HyperLink
        lnkCumulLeasiCosts.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=CumulLeasiCosts&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkCumulLeasiCosts.Style.Add("text-decoration", "none")
        lnkCumulLeasiCosts.Style.Add("cursor", "pointer")

        lnkCumulLeasiCosts.Text = CumulLeasiCosts.ToString("N2")
        lnkCumulLeasiCosts.Style.Add("font-size", "8pt")
        lnkCumulLeasiCosts.Style.Add("text-align", "right")

        tblCell.Controls.Add(lnkCumulLeasiCosts)

        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblProfitLoss.Rows.Add(tblRow)
        ' ---------------------------------------------------------------------------------------------------------

        ' QUARTA RIGA
        tblRow = New TableRow
        tblCell = New TableCell
        Dim oLBLAutomationTechnology As New RadLabel
        oLBLAutomationTechnology.Text = "Automation Technology"
        oLBLAutomationTechnology.ID = "oLBLAutomationTechnology"
        oLBLAutomationTechnology.Style.Add("font-size", "8pt")
        oLBLAutomationTechnology.Style.Add("text-align", "left")
        oLBLAutomationTechnology.Style.Add("font-weight", "normal")
        tblCell.Controls.Add(oLBLAutomationTechnology)
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("padding-left", "5px")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell

        Dim lnkTechnAutomAllow As New HyperLink
        lnkTechnAutomAllow.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=TechnAutomAllow&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkTechnAutomAllow.Style.Add("text-decoration", "none")
        lnkTechnAutomAllow.Style.Add("cursor", "pointer")

        lnkTechnAutomAllow.Text = TechnAutomAllow.ToString("N2")
        lnkTechnAutomAllow.Style.Add("font-size", "8pt")
        lnkTechnAutomAllow.Style.Add("text-align", "right")
        lnkTechnAutomAllow.BackColor = Drawing.Color.Transparent

        tblCell.Controls.Add(lnkTechnAutomAllow)

        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("text-align", "right")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell 'CumulCostsTechn

        Dim lnkCumulCostsTechn As New HyperLink
        lnkCumulCostsTechn.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=CumulCostsTechn&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkCumulCostsTechn.Style.Add("text-decoration", "none")
        lnkCumulCostsTechn.Style.Add("cursor", "pointer")

        lnkCumulCostsTechn.Text = CumulCostsTechn.ToString("N2")
        lnkCumulCostsTechn.Style.Add("font-size", "8pt")
        lnkCumulCostsTechn.Style.Add("text-align", "right")

        tblCell.Controls.Add(lnkCumulCostsTechn)

        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblProfitLoss.Rows.Add(tblRow)
        ' ---------------------------------------------------------------------------------------------------------

        ' INSERISCO LA RIGA PRODUCTION OVERHEADS
        tblRow = New TableRow
        tblCell = New TableCell
        Dim oLBLGrossProfit As New RadLabel
        oLBLGrossProfit.Text = "PRODUCTION OVERHEADS"
        oLBLGrossProfit.ID = "oLBLGrossProfit"
        oLBLGrossProfit.Style.Add("font-size", "8pt")
        oLBLGrossProfit.Style.Add("text-align", "left")
        oLBLGrossProfit.Style.Add("font-weight", "bold")
        tblCell.Controls.Add(oLBLGrossProfit)
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("padding-left", "5px")
        tblCell.Style.Add("background", "#e7e7e7")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim lnkCostTotProduz As New HyperLink
        lnkCostTotProduz.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=CostTotProduz&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkCostTotProduz.Style.Add("text-decoration", "none")
        lnkCostTotProduz.Style.Add("cursor", "pointer")

        lnkCostTotProduz.Text = Nn(GetVariableDefineValue("CostTotProduz", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
        lnkCostTotProduz.Style.Add("font-size", "8pt")
        lnkCostTotProduz.Style.Add("text-align", "right")

        tblCell.Controls.Add(lnkCostTotProduz)

        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("background", "#e7e7e7")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell

        Dim lnkCostoTotalPrCumul As New HyperLink
        lnkCostoTotalPrCumul.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=ProduBasicRequi&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkCostoTotalPrCumul.Style.Add("text-decoration", "none")
        lnkCostoTotalPrCumul.Style.Add("cursor", "pointer")

        lnkCostoTotalPrCumul.Text = Nn(GetVariableDefineValue("CostoTotalPrCumul", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
        lnkCostoTotalPrCumul.Style.Add("font-size", "8pt")
        lnkCostoTotalPrCumul.Style.Add("text-align", "right")
        tblCell.Controls.Add(lnkCostoTotalPrCumul)

        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("background", "#e7e7e7")
        tblRow.Cells.Add(tblCell)

        tblProfitLoss.Rows.Add(tblRow)
        ' ---------------------------------------------------------------------------------------------------------

        m_CostoProduzione = Nn(GetVariableDefineValue("CostTotProduz", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
        m_CostoProduzioneCum = Nn(GetVariableDefineValue("CostoTotalPrCumul", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")

    End Sub

    Private Sub LogisticsCosts()
        Dim tblRow As TableRow
        Dim tblCell As TableCell

        ' PERIODO
        Dim TotalTransCosts As Double = GetVariableDefineValue("TotalTransCosts", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))
        Dim SinkiFundVehic As Double = GetVariableDefineValue("SinkiFundVehic", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))
        Dim TotalWarehCosts As Double = GetVariableDefineValue("TotalWarehCosts", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))

        ' CUMULATO
        Dim CumulTransCosts As Double = GetVariableDefineValue("CumulTransCosts", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))
        Dim CumulSinkiVehic As Double = GetVariableDefineValue("CumulSinkiVehic", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))
        Dim CumulWarehCosts As Double = GetVariableDefineValue("CumulWarehCosts", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))

        ' RIGHE DI DETTAGLIO
        ' PRIMA RIGA
        tblRow = New TableRow
        tblCell = New TableCell
        Dim oLBLTotalTransCosts As New RadLabel
        oLBLTotalTransCosts.Text = "Total transport cost"
        oLBLTotalTransCosts.ID = "oLBLTotalTransCosts"
        oLBLTotalTransCosts.Style.Add("font-size", "8pt")
        oLBLTotalTransCosts.Style.Add("text-align", "left")
        oLBLTotalTransCosts.Style.Add("font-weight", "normal")
        tblCell.Controls.Add(oLBLTotalTransCosts)
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("padding-left", "5px")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell

        Dim lnkTotalTransCosts As New HyperLink
        lnkTotalTransCosts.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=TotalTransCosts&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkTotalTransCosts.Style.Add("text-decoration", "none")
        lnkTotalTransCosts.Style.Add("cursor", "pointer")

        lnkTotalTransCosts.Text = TotalTransCosts.ToString("N2")
        lnkTotalTransCosts.Style.Add("font-size", "8pt")
        lnkTotalTransCosts.Style.Add("text-align", "right")
        lnkTotalTransCosts.BackColor = Drawing.Color.Transparent

        tblCell.Controls.Add(lnkTotalTransCosts)

        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("text-align", "right")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell

        Dim lnkCumulTransCosts As New HyperLink
        lnkCumulTransCosts.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=CumulTransCosts&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkCumulTransCosts.Style.Add("text-decoration", "none")
        lnkCumulTransCosts.Style.Add("cursor", "pointer")

        lnkCumulTransCosts.Text = CumulTransCosts.ToString("N2")

        lnkCumulTransCosts.Style.Add("font-size", "8pt")
        lnkCumulTransCosts.Style.Add("text-align", "right")

        tblCell.Controls.Add(lnkCumulTransCosts)

        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblProfitLoss.Rows.Add(tblRow)
        ' ---------------------------------------------------------------------------------------------------------

        ' SECONDA RIGA
        tblRow = New TableRow
        tblCell = New TableCell
        Dim oLBLVehiclesDepraciation As New RadLabel
        oLBLVehiclesDepraciation.Text = "Vehicles depreciation"
        oLBLVehiclesDepraciation.ID = "oLBLVehiclesDepraciation"
        oLBLVehiclesDepraciation.Style.Add("font-size", "8pt")
        oLBLVehiclesDepraciation.Style.Add("text-align", "left")
        oLBLVehiclesDepraciation.Style.Add("font-weight", "normal")
        tblCell.Controls.Add(oLBLVehiclesDepraciation)
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("padding-left", "5px")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim lnkSinkiFundVehic As New HyperLink
        lnkSinkiFundVehic.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=SinkiFundVehic&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkSinkiFundVehic.Style.Add("text-decoration", "none")
        lnkSinkiFundVehic.Style.Add("cursor", "pointer")

        lnkSinkiFundVehic.Text = SinkiFundVehic.ToString("N2")
        lnkSinkiFundVehic.Style.Add("font-size", "8pt")
        lnkSinkiFundVehic.Style.Add("text-align", "right")
        lnkSinkiFundVehic.BackColor = Drawing.Color.Transparent

        tblCell.Controls.Add(lnkSinkiFundVehic)

        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("text-align", "right")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim oLBLVehiclesDepraciationValueCum As New RadLabel
        oLBLVehiclesDepraciationValueCum.Text = CumulSinkiVehic.ToString("N2")

        Dim lnkCumulSinkiVehic As New HyperLink
        lnkCumulSinkiVehic.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=CumulSinkiVehic&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkCumulSinkiVehic.Style.Add("text-decoration", "none")
        lnkCumulSinkiVehic.Style.Add("cursor", "pointer")

        lnkCumulSinkiVehic.Text = CumulSinkiVehic.ToString("N2")
        lnkCumulSinkiVehic.Style.Add("font-size", "8pt")
        lnkCumulSinkiVehic.Style.Add("text-align", "right")

        tblCell.Controls.Add(lnkCumulSinkiVehic)

        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblProfitLoss.Rows.Add(tblRow)
        ' ---------------------------------------------------------------------------------------------------------

        ' TERZA RIGA
        tblRow = New TableRow
        tblCell = New TableCell
        Dim oLBLWarehouseCost As New RadLabel
        oLBLWarehouseCost.Text = "Warehouse cost"
        oLBLWarehouseCost.ID = "oLBLWarehouseCost"
        oLBLWarehouseCost.Style.Add("font-size", "8pt")
        oLBLWarehouseCost.Style.Add("text-align", "left")
        oLBLWarehouseCost.Style.Add("font-weight", "normal")
        tblCell.Controls.Add(oLBLWarehouseCost)
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("padding-left", "5px")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell

        Dim lnkTotalWarehCosts As New HyperLink
        lnkTotalWarehCosts.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=TotalWarehCosts&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkTotalWarehCosts.Style.Add("text-decoration", "none")
        lnkTotalWarehCosts.Style.Add("cursor", "pointer")

        lnkTotalWarehCosts.Text = TotalWarehCosts.ToString("N2")

        lnkTotalWarehCosts.Style.Add("font-size", "8pt")
        lnkTotalWarehCosts.Style.Add("text-align", "right")
        lnkTotalWarehCosts.BackColor = Drawing.Color.Transparent

        tblCell.Controls.Add(lnkTotalWarehCosts)

        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("text-align", "right")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell

        Dim lnkCumulWarehCosts As New HyperLink
        lnkCumulWarehCosts.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=CumulWarehCosts&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkCumulWarehCosts.Style.Add("text-decoration", "none")
        lnkCumulWarehCosts.Style.Add("cursor", "pointer")

        lnkCumulWarehCosts.Text = CumulWarehCosts.ToString("N2")

        lnkCumulWarehCosts.Style.Add("font-size", "8pt")
        lnkCumulWarehCosts.Style.Add("text-align", "right")

        tblCell.Controls.Add(lnkCumulWarehCosts)

        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblProfitLoss.Rows.Add(tblRow)
        ' ---------------------------------------------------------------------------------------------------------

        ' INSERISCO LA RIGA LOGISTICS COSTS
        tblRow = New TableRow
        tblCell = New TableCell
        Dim oLBLLogisticsCosts As New RadLabel
        oLBLLogisticsCosts.Text = "LOGISTICS COSTS"
        oLBLLogisticsCosts.ID = "oLBLLogisticsCosts"
        oLBLLogisticsCosts.Style.Add("font-size", "8pt")
        oLBLLogisticsCosts.Style.Add("text-align", "left")
        oLBLLogisticsCosts.Style.Add("font-weight", "bold")
        tblCell.Controls.Add(oLBLLogisticsCosts)
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("padding-left", "5px")
        tblCell.Style.Add("background", "#e7e7e7")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell

        Dim lnkCostoLogistica As New HyperLink
        lnkCostoLogistica.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=CostoLogistica&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkCostoLogistica.Style.Add("text-decoration", "none")
        lnkCostoLogistica.Style.Add("cursor", "pointer")

        lnkCostoLogistica.Text = Nn(GetVariableDefineValue("CostoLogistica", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")

        lnkCostoLogistica.Style.Add("font-size", "8pt")
        lnkCostoLogistica.Style.Add("text-align", "right")

        tblCell.Controls.Add(lnkCostoLogistica)

        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("background", "#e7e7e7")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim lnkCostoLogisCumul As New HyperLink
        lnkCostoLogisCumul.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=ProduBasicRequi&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkCostoLogisCumul.Style.Add("text-decoration", "none")
        lnkCostoLogisCumul.Style.Add("cursor", "pointer")

        lnkCostoLogisCumul.Text = Nn(GetVariableDefineValue("CostoLogisCumul", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
        lnkCostoLogisCumul.Style.Add("font-size", "8pt")
        lnkCostoLogisCumul.Style.Add("text-align", "right")

        tblCell.Controls.Add(lnkCostoLogisCumul)

        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("background", "#e7e7e7")
        tblRow.Cells.Add(tblCell)

        tblProfitLoss.Rows.Add(tblRow)
        ' ---------------------------------------------------------------------------------------------------------

        m_CostoLogistica = Nn(GetVariableDefineValue("CostoLogistica", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
        m_CostoLogisticaCum = Nn(GetVariableDefineValue("CostoLogisCumul", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")

    End Sub

    Private Sub CommercialCosts()
        Dim tblRow As TableRow
        Dim tblCell As TableCell

        ' PERIODO
        ' Solo altre spese
        Dim TechnAutomAllow As Double = GetVariableDefineValue("TechnAutomAllow", m_SiteMaster.TeamIDGet, 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))
        Dim AddesTotalAllow As Double = GetVariableDefineValue("AddesTotalAllow", m_SiteMaster.TeamIDGet, 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))
        Dim TrainServiAllow As Double = GetVariableDefineValue("TrainServiAllow", m_SiteMaster.TeamIDGet, 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))
        Dim TotalMarkeAllow As Double = GetVariableDefineValue("TotalMarkeAllow", m_SiteMaster.TeamIDGet, 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))
        Dim AdverAllow As Double = GetVariableDefineValue("AdverAllow", m_SiteMaster.TeamIDGet, 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))
        Dim GreenAllow As Double = GetVariableDefineValue("GreenAllow", m_SiteMaster.TeamIDGet, 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))
        Dim AltreSpese As Double = GetVariableDefineValue("AltreSpese", m_SiteMaster.TeamIDGet, 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))

        Dim TotalCostsOutle As Double = GetVariableDefineValue("TotalCostsOutle", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))
        Dim CostsPersoPoint As Double = GetVariableDefineValue("CostsPersoPoint", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))
        Dim CostsPersoStaff As Double = GetVariableDefineValue("CostsPersoStaff", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))

        ' Salari
        Dim Salari As Double = CostsPersoPoint + CostsPersoStaff

        '' Totale costi

        ' CUMULATO
        Dim CumulExpenCosts As Double = GetVariableDefineValue("CumulExpenCosts", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))
        Dim CumulOutleCosts As Double = GetVariableDefineValue("CumulOutleCosts", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))
        Dim CumulPersoCosts As Double = GetVariableDefineValue("CumulPersoCosts", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))

        ' RIGHE DI DETTAGLIO
        ' PRIMA RIGA
        tblRow = New TableRow
        tblCell = New TableCell
        Dim oLBLCommercialExpenditure As New RadLabel
        oLBLCommercialExpenditure.Text = "Commercial Expenditure"
        oLBLCommercialExpenditure.ID = "oLBLCommercialExpenditure"
        oLBLCommercialExpenditure.Style.Add("font-size", "8pt")
        oLBLCommercialExpenditure.Style.Add("text-align", "left")
        oLBLCommercialExpenditure.Style.Add("font-weight", "normal")
        tblCell.Controls.Add(oLBLCommercialExpenditure)
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("padding-left", "5px")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell

        Dim lnkAltreSpese As New HyperLink
        lnkAltreSpese.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=AltreSpese&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkAltreSpese.Style.Add("text-decoration", "none")
        lnkAltreSpese.Style.Add("cursor", "pointer")

        lnkAltreSpese.Text = AltreSpese.ToString("N2")
        lnkAltreSpese.Style.Add("font-size", "8pt")
        lnkAltreSpese.Style.Add("text-align", "right")
        lnkAltreSpese.BackColor = Drawing.Color.Transparent

        tblCell.Controls.Add(lnkAltreSpese)

        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("text-align", "right")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell

        Dim lnkCumulExpenCosts As New HyperLink
        lnkCumulExpenCosts.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=CumulExpenCosts&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkCumulExpenCosts.Style.Add("text-decoration", "none")
        lnkCumulExpenCosts.Style.Add("cursor", "pointer")

        lnkCumulExpenCosts.Text = CumulExpenCosts.ToString("N2")
        lnkCumulExpenCosts.Style.Add("font-size", "8pt")
        lnkCumulExpenCosts.Style.Add("text-align", "right")

        tblCell.Controls.Add(lnkCumulExpenCosts)

        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblProfitLoss.Rows.Add(tblRow)
        ' ---------------------------------------------------------------------------------------------------------

        ' SECONDA RIGA
        tblRow = New TableRow
        tblCell = New TableCell
        Dim oLBLRentOfOutlets As New RadLabel
        oLBLRentOfOutlets.Text = "Rent of Outlets"
        oLBLRentOfOutlets.ID = "oLBLRentOfOutlets"
        oLBLRentOfOutlets.Style.Add("font-size", "8pt")
        oLBLRentOfOutlets.Style.Add("text-align", "left")
        oLBLRentOfOutlets.Style.Add("font-weight", "normal")
        tblCell.Controls.Add(oLBLRentOfOutlets)
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("padding-left", "5px")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim lnkTotalCostsOutle As New HyperLink
        lnkTotalCostsOutle.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=TotalCostsOutle&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkTotalCostsOutle.Style.Add("text-decoration", "none")
        lnkTotalCostsOutle.Style.Add("cursor", "pointer")

        lnkTotalCostsOutle.Text = TotalCostsOutle.ToString("N2")
        lnkTotalCostsOutle.Style.Add("font-size", "8pt")
        lnkTotalCostsOutle.Style.Add("text-align", "right")
        lnkTotalCostsOutle.BackColor = Drawing.Color.Transparent

        tblCell.Controls.Add(lnkTotalCostsOutle)

        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("text-align", "right")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim lnkCumulOutleCosts As New HyperLink
        lnkCumulOutleCosts.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=CumulOutleCosts&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkCumulOutleCosts.Style.Add("text-decoration", "none")
        lnkCumulOutleCosts.Style.Add("cursor", "pointer")

        lnkCumulOutleCosts.Text = CumulOutleCosts.ToString("N2")
        lnkCumulOutleCosts.Style.Add("font-size", "8pt")
        lnkCumulOutleCosts.Style.Add("text-align", "right")

        tblCell.Controls.Add(lnkCumulOutleCosts)

        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblProfitLoss.Rows.Add(tblRow)
        ' ---------------------------------------------------------------------------------------------------------

        ' TERZA RIGA
        tblRow = New TableRow
        tblCell = New TableCell
        Dim oLBLSalesPersonsSalary As New RadLabel
        oLBLSalesPersonsSalary.Text = "Sales persons salary"
        oLBLSalesPersonsSalary.ID = "oLBLSalesPersonsSalary"
        oLBLSalesPersonsSalary.Style.Add("font-size", "8pt")
        oLBLSalesPersonsSalary.Style.Add("text-align", "left")
        oLBLSalesPersonsSalary.Style.Add("font-weight", "normal")
        tblCell.Controls.Add(oLBLSalesPersonsSalary)
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("padding-left", "20px")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell

        Dim lnkCostsPersoPoint As New HyperLink
        lnkCostsPersoPoint.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=CostsPersoPoint&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkCostsPersoPoint.Style.Add("text-decoration", "none")
        lnkCostsPersoPoint.Style.Add("cursor", "pointer")

        lnkCostsPersoPoint.Text = CostsPersoPoint.ToString("N2")
        lnkCostsPersoPoint.Style.Add("font-size", "8pt")
        lnkCostsPersoPoint.Style.Add("text-align", "right")
        lnkCostsPersoPoint.BackColor = Drawing.Color.Transparent

        tblCell.Controls.Add(lnkCostsPersoPoint)

        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("text-align", "right")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim oLBLSalesPersonsSalaryValueCum As New RadLabel
        oLBLSalesPersonsSalaryValueCum.Text = "  "
        oLBLSalesPersonsSalaryValueCum.Style.Add("font-size", "8pt")
        oLBLSalesPersonsSalaryValueCum.Style.Add("text-align", "right")
        tblCell.Controls.Add(oLBLSalesPersonsSalaryValueCum)
        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblProfitLoss.Rows.Add(tblRow)
        ' ---------------------------------------------------------------------------------------------------------

        ' QUARTA RIGA
        tblRow = New TableRow
        tblCell = New TableCell
        Dim oLBLSupervisorSalary As New RadLabel
        oLBLSupervisorSalary.Text = "Supervisor salary"
        oLBLSupervisorSalary.ID = "oLBLSupervisorSalary"
        oLBLSupervisorSalary.Style.Add("font-size", "8pt")
        oLBLSupervisorSalary.Style.Add("text-align", "left")
        oLBLSupervisorSalary.Style.Add("font-weight", "normal")
        tblCell.Controls.Add(oLBLSupervisorSalary)
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("padding-left", "20px")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell

        Dim lnkCostsPersoStaff As New HyperLink
        lnkCostsPersoStaff.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=CostsPersoStaff&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkCostsPersoStaff.Style.Add("text-decoration", "none")
        lnkCostsPersoStaff.Style.Add("cursor", "pointer")

        lnkCostsPersoStaff.Text = CostsPersoStaff.ToString("N2")
        lnkCostsPersoStaff.Style.Add("font-size", "8pt")
        lnkCostsPersoStaff.Style.Add("text-align", "right")
        lnkCostsPersoStaff.BackColor = Drawing.Color.Transparent

        tblCell.Controls.Add(lnkCostsPersoStaff)

        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("text-align", "right")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim oLBLSupervisorSalaryValueCum As New RadLabel
        oLBLSupervisorSalaryValueCum.Text = "  "
        oLBLSupervisorSalaryValueCum.Style.Add("font-size", "8pt")
        oLBLSupervisorSalaryValueCum.Style.Add("text-align", "right")
        tblCell.Controls.Add(oLBLSupervisorSalaryValueCum)
        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblProfitLoss.Rows.Add(tblRow)
        ' ---------------------------------------------------------------------------------------------------------

        ' QUINTA RIGA
        tblRow = New TableRow
        tblCell = New TableCell
        Dim oLBLSalary As New RadLabel
        oLBLSalary.Text = "Salaries"
        oLBLSalary.ID = "oLBLSalary"
        oLBLSalary.Style.Add("font-size", "8pt")
        oLBLSalary.Style.Add("text-align", "left")
        oLBLSalary.Style.Add("font-weight", "normal")
        tblCell.Controls.Add(oLBLSalary)
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("padding-left", "5px")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim oLBLSalaryValue As New RadLabel
        oLBLSalaryValue.Text = Salari.ToString("N2")
        oLBLSalaryValue.Style.Add("font-size", "8pt")
        oLBLSalaryValue.Style.Add("text-align", "right")
        oLBLSalaryValue.BackColor = Drawing.Color.Transparent
        tblCell.Controls.Add(oLBLSalaryValue)
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("text-align", "right")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim oLBLSupervisorValueCum As New RadLabel
        oLBLSupervisorValueCum.Text = CumulPersoCosts.ToString("N2")
        oLBLSupervisorValueCum.Style.Add("font-size", "8pt")
        oLBLSupervisorValueCum.Style.Add("text-align", "right")
        tblCell.Controls.Add(oLBLSupervisorValueCum)
        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblProfitLoss.Rows.Add(tblRow)
        ' ---------------------------------------------------------------------------------------------------------

        ' INSERISCO LA RIGA COMMERCIAL COSTS
        tblRow = New TableRow
        tblCell = New TableCell
        Dim oLBLLogisticsCosts As New RadLabel
        oLBLLogisticsCosts.Text = "COMMERCIAL COSTS"
        oLBLLogisticsCosts.ID = "oLBLLogisticsCosts"
        oLBLLogisticsCosts.Style.Add("font-size", "8pt")
        oLBLLogisticsCosts.Style.Add("text-align", "left")
        oLBLLogisticsCosts.Style.Add("font-weight", "bold")
        tblCell.Controls.Add(oLBLLogisticsCosts)
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("padding-left", "5px")
        tblCell.Style.Add("background", "#e7e7e7")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell

        Dim lnkCostoCommerciale As New HyperLink
        lnkCostoCommerciale.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=CostoCommerciale&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkCostoCommerciale.Style.Add("text-decoration", "none")
        lnkCostoCommerciale.Style.Add("cursor", "pointer")

        lnkCostoCommerciale.Text = Nn(GetVariableDefineValue("CostoCommerciale", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
        lnkCostoCommerciale.Style.Add("font-size", "8pt")
        lnkCostoCommerciale.Style.Add("text-align", "right")

        tblCell.Controls.Add(lnkCostoCommerciale)

        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("background", "#e7e7e7")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell

        Dim lnkCostoCommeCumul As New HyperLink
        lnkCostoCommeCumul.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=CostoCommeCumul&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkCostoCommeCumul.Style.Add("text-decoration", "none")
        lnkCostoCommeCumul.Style.Add("cursor", "pointer")

        lnkCostoCommeCumul.Text = Nn(GetVariableDefineValue("CostoCommeCumul", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
        lnkCostoCommeCumul.Style.Add("font-size", "8pt")
        lnkCostoCommeCumul.Style.Add("text-align", "right")

        tblCell.Controls.Add(lnkCostoCommeCumul)

        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("background", "#e7e7e7")
        tblRow.Cells.Add(tblCell)

        tblProfitLoss.Rows.Add(tblRow)
        ' ---------------------------------------------------------------------------------------------------------

        m_CostoCommerciale = Nn(GetVariableDefineValue("CostoCommerciale", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame")))
        m_CostoCommercialeCum = Nn(GetVariableDefineValue("CostoCommeCumul", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame")))

    End Sub

    Private Sub OperatingProfit()
        Dim tblRow As TableRow
        Dim tblCell As TableCell

        ' INSERISCO LA RIGA OPERATING PROFIT
        tblRow = New TableRow
        tblCell = New TableCell
        Dim oLblOperatingProfitCosts As New RadLabel
        oLblOperatingProfitCosts.Text = "OPERATING PROFIT"
        oLblOperatingProfitCosts.ID = "oLblOperatingProfitCosts"
        oLblOperatingProfitCosts.Style.Add("font-size", "8pt")
        oLblOperatingProfitCosts.Style.Add("text-align", "left")
        oLblOperatingProfitCosts.Style.Add("font-weight", "bold")
        tblCell.Controls.Add(oLblOperatingProfitCosts)
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("padding-left", "5px")
        tblCell.Style.Add("background", "#e7e7e7")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell

        Dim lnkOperativeMargin As New HyperLink
        lnkOperativeMargin.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=OperativeMargin&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkOperativeMargin.Style.Add("text-decoration", "none")
        lnkOperativeMargin.Style.Add("cursor", "pointer")

        lnkOperativeMargin.Text = Nn(GetVariableDefineValue("OperativeMargin", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
        lnkOperativeMargin.Style.Add("font-size", "8pt")
        lnkOperativeMargin.Style.Add("text-align", "right")

        tblCell.Controls.Add(lnkOperativeMargin)

        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("background", "#e7e7e7")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell

        Dim lnkOperaMargiCumul As New HyperLink
        lnkOperaMargiCumul.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=OperaMargiCumul&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkOperaMargiCumul.Style.Add("text-decoration", "none")
        lnkOperaMargiCumul.Style.Add("cursor", "pointer")

        lnkOperaMargiCumul.Text = Nn(GetVariableDefineValue("OperaMargiCumul", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
        lnkOperaMargiCumul.Style.Add("font-size", "8pt")
        lnkOperaMargiCumul.Style.Add("text-align", "right")
        tblCell.Controls.Add(lnkOperaMargiCumul)

        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("background", "#e7e7e7")
        tblRow.Cells.Add(tblCell)

        tblProfitLoss.Rows.Add(tblRow)
        ' ---------------------------------------------------------------------------------------------------------
    End Sub

    Private Sub Finance()
        Dim tblRow As TableRow
        Dim tblCell As TableCell

        ' PERIODO
        Dim DebtsWrittOff As Double = GetVariableDefineValue("DebtsWrittOff", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))
        Dim TotalNegatInter As Double = GetVariableDefineValue("TotalNegatInter", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))
        Dim TotalPositInter As Double = GetVariableDefineValue("TotalPositInter", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))

        ' CUMULATO
        Dim CumulDebtsOff As Double = GetVariableDefineValue("CumulDebtsOff", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))
        Dim CumulNegatInter As Double = GetVariableDefineValue("CumulNegatInter", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))
        Dim CumulPositInter As Double = GetVariableDefineValue("CumulPositInter", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))

        ' RIGHE DI DETTAGLIO
        ' PRIMA RIGA
        tblRow = New TableRow
        tblCell = New TableCell
        Dim oLBLExceptionalItem_LossOnSale As New RadLabel
        oLBLExceptionalItem_LossOnSale.Text = "Exceptional Item - Loss on Sale (Machines)"
        oLBLExceptionalItem_LossOnSale.ID = "oLBLExceptionalItem_LossOnSale"
        oLBLExceptionalItem_LossOnSale.Style.Add("font-size", "8pt")
        oLBLExceptionalItem_LossOnSale.Style.Add("text-align", "left")
        oLBLExceptionalItem_LossOnSale.Style.Add("font-weight", "normal")
        tblCell.Controls.Add(oLBLExceptionalItem_LossOnSale)
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("padding-left", "5px")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim lnkDebtsWrittOff As New HyperLink
        lnkDebtsWrittOff.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=DebtsWrittOff&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkDebtsWrittOff.Style.Add("text-decoration", "none")
        lnkDebtsWrittOff.Style.Add("cursor", "pointer")

        lnkDebtsWrittOff.Text = DebtsWrittOff.ToString("N2")
        lnkDebtsWrittOff.Style.Add("font-size", "8pt")
        lnkDebtsWrittOff.Style.Add("text-align", "right")
        lnkDebtsWrittOff.BackColor = Drawing.Color.Transparent

        tblCell.Controls.Add(lnkDebtsWrittOff)

        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("text-align", "right")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim lnkCumulDebtsOff As New HyperLink
        lnkCumulDebtsOff.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=CumulDebtsOff&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkCumulDebtsOff.Style.Add("text-decoration", "none")
        lnkCumulDebtsOff.Style.Add("cursor", "pointer")

        lnkCumulDebtsOff.Text = CumulDebtsOff.ToString("N2")
        lnkCumulDebtsOff.Style.Add("font-size", "8pt")
        lnkCumulDebtsOff.Style.Add("text-align", "right")

        tblCell.Controls.Add(lnkCumulDebtsOff)

        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblProfitLoss.Rows.Add(tblRow)
        ' ---------------------------------------------------------------------------------------------------------

        ' SECONDA RIGA
        tblRow = New TableRow
        tblCell = New TableCell
        Dim oLBLLoanInterest As New RadLabel
        oLBLLoanInterest.Text = "Loan interest"
        oLBLLoanInterest.ID = "oLBLLoanInterest"
        oLBLLoanInterest.Style.Add("font-size", "8pt")
        oLBLLoanInterest.Style.Add("text-align", "left")
        oLBLLoanInterest.Style.Add("font-weight", "normal")
        tblCell.Controls.Add(oLBLLoanInterest)
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("padding-left", "5px")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim lnkTotalNegatInter As New HyperLink
        lnkTotalNegatInter.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=TotalNegatInter&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkTotalNegatInter.Style.Add("text-decoration", "none")
        lnkTotalNegatInter.Style.Add("cursor", "pointer")

        lnkTotalNegatInter.Text = TotalNegatInter.ToString("N2")
        lnkTotalNegatInter.Style.Add("font-size", "8pt")
        lnkTotalNegatInter.Style.Add("text-align", "right")
        lnkTotalNegatInter.BackColor = Drawing.Color.Transparent
        tblCell.Controls.Add(lnkTotalNegatInter)

        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("text-align", "right")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim oLBLLoanInterestValueCum As New RadLabel
        oLBLLoanInterestValueCum.Text = CumulNegatInter.ToString("N2")
        Dim lnkCumulNegatInter As New HyperLink
        lnkCumulNegatInter.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=CumulNegatInter&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkCumulNegatInter.Style.Add("text-decoration", "none")
        lnkCumulNegatInter.Style.Add("cursor", "pointer")

        lnkCumulNegatInter.Text = CumulNegatInter.ToString("N2")
        lnkCumulNegatInter.Style.Add("font-size", "8pt")
        lnkCumulNegatInter.Style.Add("text-align", "right")

        tblCell.Controls.Add(lnkCumulNegatInter)

        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblProfitLoss.Rows.Add(tblRow)
        ' ---------------------------------------------------------------------------------------------------------

        ' TERZA RIGA
        tblRow = New TableRow
        tblCell = New TableCell
        Dim oLBLInterestReceived As New RadLabel
        oLBLInterestReceived.Text = "Interest received"
        oLBLInterestReceived.ID = "oLBLInterestReceived"
        oLBLInterestReceived.Style.Add("font-size", "8pt")
        oLBLInterestReceived.Style.Add("text-align", "left")
        oLBLInterestReceived.Style.Add("font-weight", "normal")
        tblCell.Controls.Add(oLBLInterestReceived)
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("padding-left", "5px")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim lnkTotalPositInter As New HyperLink
        lnkTotalPositInter.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=TotalPositInter&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkTotalPositInter.Style.Add("text-decoration", "none")
        lnkTotalPositInter.Style.Add("cursor", "pointer")

        lnkTotalPositInter.Text = TotalPositInter.ToString("N2")
        lnkTotalPositInter.Style.Add("font-size", "8pt")
        lnkTotalPositInter.Style.Add("text-align", "right")
        lnkTotalPositInter.BackColor = Drawing.Color.Transparent
        tblCell.Controls.Add(lnkTotalPositInter)

        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("text-align", "right")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim lnkCumulPositInter As New HyperLink
        lnkCumulPositInter.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=CumulPositInter&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkCumulPositInter.Style.Add("text-decoration", "none")
        lnkCumulPositInter.Style.Add("cursor", "pointer")

        lnkCumulPositInter.Text = CumulPositInter.ToString("N2")
        lnkCumulPositInter.Style.Add("font-size", "8pt")
        lnkCumulPositInter.Style.Add("text-align", "right")

        tblCell.Controls.Add(lnkCumulPositInter)

        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblProfitLoss.Rows.Add(tblRow)
        ' ---------------------------------------------------------------------------------------------------------

        ' INSERISCO LA RIGA OPERATING PROFIT
        tblRow = New TableRow
        tblCell = New TableCell
        Dim oLBLFinance As New RadLabel
        oLBLFinance.Text = "FINANCE"
        oLBLFinance.ID = "oLBLFinance"
        oLBLFinance.Style.Add("font-size", "8pt")
        oLBLFinance.Style.Add("text-align", "left")
        oLBLFinance.Style.Add("font-weight", "bold")
        tblCell.Controls.Add(oLBLFinance)
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("padding-left", "5px")
        tblCell.Style.Add("background", "#e7e7e7")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim lnkCostoFinanza As New HyperLink
        lnkCostoFinanza.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=ProduBasicRequi&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkCostoFinanza.Style.Add("text-decoration", "none")
        lnkCostoFinanza.Style.Add("cursor", "pointer")

        lnkCostoFinanza.Text = Nn(GetVariableDefineValue("CostoFinanza", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
        lnkCostoFinanza.Style.Add("font-size", "8pt")
        lnkCostoFinanza.Style.Add("text-align", "right")

        tblCell.Controls.Add(lnkCostoFinanza)

        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("background", "#e7e7e7")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim lnkCostoFinanCumul As New HyperLink
        lnkCostoFinanCumul.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=ProduBasicRequi&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkCostoFinanCumul.Style.Add("text-decoration", "none")
        lnkCostoFinanCumul.Style.Add("cursor", "pointer")

        lnkCostoFinanCumul.Text = Nn(GetVariableDefineValue("CostoFinanCumul", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
        lnkCostoFinanCumul.Style.Add("font-size", "8pt")
        lnkCostoFinanCumul.Style.Add("text-align", "right")

        tblCell.Controls.Add(lnkCostoFinanCumul)

        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("background", "#e7e7e7")
        tblRow.Cells.Add(tblCell)

        tblProfitLoss.Rows.Add(tblRow)
        ' ---------------------------------------------------------------------------------------------------------
    End Sub

    Private Sub TotalCosts()
        Dim tblRow As TableRow
        Dim tblCell As TableCell

        'Dim CumulDirectCost As Double = GetVariableDefineValue("CumulDirecCosts", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))
        'Dim CumulCostPurch As Double = GetVariableDefineValue("CumulCostPurch", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))
        'Dim CumulFixedCosts As Double = GetVariableDefineValue("CumulFixedCosts", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))
        'Dim CumulSinkiPlant As Double = GetVariableDefineValue("CumulSinkiPlant", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))
        'Dim CumulSinkiVehic As Double = GetVariableDefineValue("CumulSinkiVehic", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))
        'Dim CumulOutleCosts As Double = GetVariableDefineValue("CumulOutleCosts", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))
        'Dim CumulPersoCosts As Double = GetVariableDefineValue("CumulPersoCosts", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))
        'Dim CumulNegatInter As Double = GetVariableDefineValue("CumulNegatInter", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))
        'Dim CumulExpenCosts As Double = GetVariableDefineValue("CumulExpenCosts", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))
        'Dim CumulWarehCosts As Double = GetVariableDefineValue("CumulWarehCosts", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))

        ' RIGA VUOTA
        tblRow = New TableRow
        tblCell = New TableCell
        Dim oLBLRowEmpty As New RadLabel
        oLBLRowEmpty.Text = " "
        oLBLRowEmpty.Style.Add("font-size", "8pt")
        oLBLRowEmpty.Style.Add("text-align", "left")
        oLBLRowEmpty.Style.Add("font-weight", "normal")
        tblCell.Controls.Add(oLBLRowEmpty)
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("padding-left", "5px")
        tblCell.ColumnSpan = 3
        tblRow.Cells.Add(tblCell)
        tblProfitLoss.Rows.Add(tblRow)

        '--------------------------------------------------------------------------
        ' 06/12/2016 Omissione della riga su consiglio di Fulvio via Skype...
        '--------------------------------------------------------------------------

        ' INSERISCO LA RIGA TOTAL COSTS
        'tblRow = New TableRow
        'tblCell = New TableCell
        'Dim oLBLTotalCosts As New RadLabel
        'oLBLTotalCosts.Text = "TOTAL COSTS"
        'oLBLTotalCosts.Style.Add("font-size", "8pt")
        'oLBLTotalCosts.Style.Add("text-align", "left")
        'oLBLTotalCosts.Style.Add("font-weight", "bold")
        'tblCell.Controls.Add(oLBLTotalCosts)
        'tblCell.BorderStyle = BorderStyle.None
        'tblCell.Style.Add("padding-left", "5px")
        'tblCell.Style.Add("background", "#e7e7e7")
        'tblRow.Cells.Add(tblCell)

        'tblCell = New TableCell
        'Dim oLBLTotalCostsValue As New RadLabel
        'oLBLTotalCostsValue.Text = Nn(GetVariableDefineValue("CostoVenduto", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent)).ToString("N2")
        'oLBLTotalCostsValue.Style.Add("font-size", "8pt")
        'oLBLTotalCostsValue.Style.Add("text-align", "right")
        'tblCell.Controls.Add(oLBLTotalCostsValue)
        'tblCell.Style.Add("text-align", "right")
        'tblCell.BorderStyle = BorderStyle.None
        'tblCell.Style.Add("background", "#e7e7e7")
        'tblRow.Cells.Add(tblCell)

        'tblCell = New TableCell
        'Dim oLBLTotalCostsValueCum As New RadLabel
        'oLBLTotalCostsValueCum.Text = Nn(GetVariableDefineValue("CostoVenduCumul", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent)).ToString("N2")
        'oLBLTotalCostsValueCum.Style.Add("font-size", "8pt")
        'oLBLTotalCostsValueCum.Style.Add("text-align", "right")
        'tblCell.Controls.Add(oLBLTotalCostsValueCum)
        'tblCell.Style.Add("text-align", "right")
        'tblCell.BorderStyle = BorderStyle.None
        'tblCell.Style.Add("background", "#e7e7e7")
        'tblRow.Cells.Add(tblCell)

        'tblProfitLoss.Rows.Add(tblRow)
        ' ---------------------------------------------------------------------------------------------------------

        ' RIGA VUOTA
        tblRow = New TableRow
        tblCell = New TableCell
        Dim oLBLRowEmpty1 As New RadLabel
        oLBLRowEmpty1.Text = " "
        oLBLRowEmpty1.Style.Add("font-size", "8pt")
        oLBLRowEmpty1.Style.Add("text-align", "left")
        oLBLRowEmpty1.Style.Add("font-weight", "normal")
        tblCell.Controls.Add(oLBLRowEmpty1)
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("padding-left", "5px")
        tblCell.ColumnSpan = 3
        tblRow.Cells.Add(tblCell)
        tblProfitLoss.Rows.Add(tblRow)
    End Sub

    Private Sub Tax()
        Dim tblRow As TableRow
        Dim tblCell As TableCell

        ' INSERISCO LA RIGA PROFIT BEFORE TAX
        tblRow = New TableRow
        tblCell = New TableCell
        Dim lblProfitBeforeTax As New RadLabel
        lblProfitBeforeTax.Text = "PROFIT BEFORE TAX"
        lblProfitBeforeTax.ID = "lblProfitBeforeTax"
        lblProfitBeforeTax.Style.Add("font-size", "8pt")
        lblProfitBeforeTax.Style.Add("text-align", "left")
        lblProfitBeforeTax.Style.Add("font-weight", "bold")
        tblCell.Controls.Add(lblProfitBeforeTax)
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("padding-left", "5px")
        tblCell.Style.Add("background", "#e7e7e7")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell

        Dim lnkProfiBeforTax As New HyperLink
        lnkProfiBeforTax.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=ProfiBeforTax&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkProfiBeforTax.Style.Add("text-decoration", "none")
        lnkProfiBeforTax.Style.Add("cursor", "pointer")

        lnkProfiBeforTax.Text = Nn(GetVariableDefineValue("ProfiBeforTax", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
        lnkProfiBeforTax.Style.Add("font-size", "8pt")
        lnkProfiBeforTax.Style.Add("text-align", "right")

        tblCell.Controls.Add(lnkProfiBeforTax)

        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("background", "#e7e7e7")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim lnkCumulBeforTax As New HyperLink
        lnkCumulBeforTax.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=CumulBeforTax&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkCumulBeforTax.Style.Add("text-decoration", "none")
        lnkCumulBeforTax.Style.Add("cursor", "pointer")

        lnkCumulBeforTax.Text = Nn(GetVariableDefineValue("CumulBeforTax", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
        lnkCumulBeforTax.Style.Add("font-size", "8pt")
        lnkCumulBeforTax.Style.Add("text-align", "right")

        tblCell.Controls.Add(lnkCumulBeforTax)

        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("background", "#e7e7e7")
        tblRow.Cells.Add(tblCell)

        tblProfitLoss.Rows.Add(tblRow)
        ' ---------------------------------------------------------------------------------------------------------

        ' SECONDA RIGA
        tblRow = New TableRow
        tblCell = New TableCell
        Dim oLBLTax As New RadLabel
        oLBLTax.Text = "Tax"
        oLBLTax.ID = "oLBLTax"
        oLBLTax.Style.Add("font-size", "8pt")
        oLBLTax.Style.Add("text-align", "left")
        oLBLTax.Style.Add("font-weight", "normal")
        tblCell.Controls.Add(oLBLTax)
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("padding-left", "20px")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim oLBLTaxValue As New RadLabel
        oLBLTaxValue.Text = " "
        oLBLTaxValue.Style.Add("font-size", "8pt")
        oLBLTaxValue.Style.Add("text-align", "right")
        oLBLTaxValue.BackColor = Drawing.Color.Transparent
        tblCell.Controls.Add(oLBLTaxValue)
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("text-align", "right")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim lnkTaxOfPerio As New HyperLink
        lnkTaxOfPerio.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=TaxOfPerio&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
        lnkTaxOfPerio.Style.Add("text-decoration", "none")
        lnkTaxOfPerio.Style.Add("cursor", "pointer")

        lnkTaxOfPerio.Text = Nn(GetVariableDefineValue("TaxOfPerio", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
        lnkTaxOfPerio.Style.Add("font-size", "8pt")
        lnkTaxOfPerio.Style.Add("text-align", "right")

        tblCell.Controls.Add(lnkTaxOfPerio)

        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblRow.Cells.Add(tblCell)

        tblProfitLoss.Rows.Add(tblRow)
        ' ---------------------------------------------------------------------------------------------------------

        ' INSERISCO LA RIGA PROFIT AFTER TAX
        tblRow = New TableRow
        tblCell = New TableCell
        Dim oLBLProfitAfterTAX As New RadLabel
        oLBLProfitAfterTAX.Text = "PROFIT AFTER TAX"
        oLBLProfitAfterTAX.ID = "oLBLProfitAfterTAX"
        oLBLProfitAfterTAX.Style.Add("font-size", "8pt")
        oLBLProfitAfterTAX.Style.Add("text-align", "left")
        oLBLProfitAfterTAX.Style.Add("font-weight", "bold")
        tblCell.Controls.Add(oLBLProfitAfterTAX)
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("padding-left", "5px")
        tblCell.Style.Add("background", "#e7e7e7")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim oLBLProfitAfterTAXValue As New RadLabel
        oLBLProfitAfterTAXValue.Text = " "
        oLBLProfitAfterTAXValue.Style.Add("font-size", "8pt")
        oLBLProfitAfterTAXValue.Style.Add("text-align", "right")
        tblCell.Controls.Add(oLBLProfitAfterTAXValue)
        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("background", "#e7e7e7")
        tblRow.Cells.Add(tblCell)

        tblCell = New TableCell
        Dim oLBLProfitAfterTAXValueCum As New RadLabel
        oLBLProfitAfterTAXValueCum.Text = Nn(Nn(GetVariableDefineValue("CumulBeforTax", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2") _
                                        - Nn(GetVariableDefineValue("TaxOfPerio", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")).ToString("N2")
        oLBLProfitAfterTAXValueCum.Style.Add("font-size", "8pt")
        oLBLProfitAfterTAXValueCum.Style.Add("text-align", "right")
        tblCell.Controls.Add(oLBLProfitAfterTAXValueCum)
        tblCell.Style.Add("text-align", "right")
        tblCell.BorderStyle = BorderStyle.None
        tblCell.Style.Add("background", "#e7e7e7")
        tblRow.Cells.Add(tblCell)

        tblProfitLoss.Rows.Add(tblRow)
        ' ---------------------------------------------------------------------------------------------------------
    End Sub

    Private Sub Finance_Player_Profit_Loss_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        If Not Page.IsPostBack Then
            If Session("IDRole") = "P" Then
                m_SiteMaster.ReloadPeriodsCalculate()
            End If
            LoadData()

            btnTranslate.Visible = Session("IDRole").Contains("D")

        End If
        ' Controllo tutti gli oggetti presenti
        Dim oTranslation As New TranslationObjectForm(Me, g_DAL, Nz(Session("LanguageActive")))

    End Sub

    Private Sub btnTranslate_Click(sender As Object, e As EventArgs) Handles btnTranslate.Click
        Session("PageOrigin") = Me.ToString().Substring(4, Me.ToString().Substring(4).Length - 5) + ".aspx"
        Response.Redirect("PageTranslation.aspx")
        Response.End()
    End Sub

#Region "PANNELLO HELP"

    Private Sub LoadHelp()
        lblHelp.Text = "<p>Total  direct costs<br>
Direct  costs are quantity produced x the cost per unit (see the Journal).</p>
                    <p>Total raw  material costs<br>
                      The cost of  raw materials refers to what was purchased in the period. The cost of local and  imported raw materials is displayed in the Journal.</p>
                    <p>Total cost  of production<br>
                      It is the pure  direct cost of production.<br>
  &nbsp; <br>
                      Bought-in  finished good<br>
                      It is the  cost of finished goods bought-in.</p>
                    <p>Total fixed  costs<br>
                      Fixed costs  are related to the number of machines in the period. The total amount of fixed  costs is 250,000 + 15,000 euro per machine.</p>
                    <p>Machine  depreciation<br>
                      Owned  machines depreciate by 1/12 of the purchase value each period</p>
                    <p>Leased  machine<br>
                      Leasing for  the existing machines. </p>
                    <p>Total  transport costs<br>
                      The total  transport cost includes the depreciation cost and the real cost of transport. &nbsp;The cost of internal transport is 0.3 per  space unit and the external &nbsp;cost is 0.75  per space unit.</p>
                    <p>Vehicles  depreciation<br>
                      Vehicles  depreciate by 1/12 of the purchase value each period</p>
                    <p>Warehouse  costs<br>
                      The  warehouse cost is related to the stock of the finished goods. The cost is  300,000 euro per period + 2 euros per space unit.</p>
                    <p>Loss on  sale<br>
                      When  selling owned machines, the company registers a loss on sale of machines. The  loss is 50% of the current value of the machines (present net value = cost of  purchase &amp;#8211; total depreciation).<br>
                      When  selling machines the income will be received as cash in the following period.</p>
                    <p>Rent<br>
                      Rent per  period is calculated by the cost of rent shown in the News X the total number  of outlets at the end of the previous period + the new validated outlets.</p>
                    <p>Sales  person cost<br>
                      It is the  cost of sales people. It includes the people hired in the period and the cost  that the company has to pay to the employees that have been laid off in the  period.</p>
                    <p>Staff cost<br>
                      It is the  cost of staff. It includes the people hired in the period and &nbsp;the cost that the company has to pay to the  employees that have bee laid off in the period.</p>
                    <p>Salaries<br>
                      It is the  total salaries for the employees</p>
                    <p>Loans  interest<br>
                      Interests  on loans are calculated on the base of 2% per period. The percentage can be  reduced according to the environmental expenditures level. &nbsp;Interests are calculated on the loans at the  end of the previous period.</p>
                    <p>Altre spese<br>
                      Other  expenditures include all the discretional expenditures such as:<br>
  &nbsp;Promotions, Advertising, Training, Automation  technology, Customer service , Environmental factors.<br>
  &nbsp;<br>
                      Total costs<br>
                      It is the  aggregate of total costs for the period.</p>
                    <p>Profit  before tax<br>
                      It is the  difference between revenues and total costs.</p>
                    <p>Tax for the  period<br>
                      Tax is 50%  of the total profit for the year. If the profit is negative, tax is not due but  the loss cannot be used to balance profits for the following year(s).</p>
                    <p>Profit  after tax<br>
                  Profit  after tax is the profit before tax; taxation for the year. It is only  calculated at the end of the year.</p>"
    End Sub

#End Region

End Class
