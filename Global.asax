﻿<%@ Application Language="VB" %>

<%@ Import Namespace="System.Web.Optimization" %>
<%@ Import Namespace="System.Web.Routing" %>
<%@ Import Namespace="APS" %>

<script RunAt="server">

    Sub Application_Start(sender As Object, e As EventArgs)
        RouteConfig.RegisterRoutes(RouteTable.Routes)
        BundleConfig.RegisterBundles(BundleTable.Bundles)
    End Sub

    Sub Session_end(sender As Object, e As EventArgs)
        If Not g_DAL Is Nothing Then SQLConnClose(g_DAL.GetConnObject)
        g_DAL = Nothing
        Session.Remove("SessionData")
        Session.Remove("IDTeam")
        Session.Remove("IDGame")
        Session.Remove("IDPeriod")
        Session.Remove("CurrentStep")
    End Sub

</script>
