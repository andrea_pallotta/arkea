﻿Imports System.Data
Imports APS
Imports APPCore.Utility
Imports DALC4NET
Imports Telerik.Web.UI

Partial Class Marketing_Player_Decision
    Inherits System.Web.UI.Page

    Private m_Variable As List(Of String)

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster

    Private Sub EnableSave()
        If Nni(Session("IDPeriod")) = HandleGetMaxPeriodValid(Session("IDGame")) Then
            btnSave.Enabled = True
        Else
            If Session("IDRole") = "P" Then
                btnSave.Enabled = False

            ElseIf Session("IDRole") = "D" Then
                btnSave.Enabled = True
            End If
        End If
    End Sub

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        tblPlayerDecision.Rows.Clear()
        HandleLoadPlayerDecision()
        Message.Visible = False
        EnableSave()

        pnlMain.Update()
    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        tblPlayerDecision.Rows.Clear()
        HandleLoadPlayerDecision()
        Message.Visible = False
        pnlMain.Update()
    End Sub

    Private Sub Marketing_Player_Decision_Init(sender As Object, e As EventArgs) Handles Me.Init
        m_SessionData = Session("SessionData")

        If IsNothing(m_SessionData) Then
            PageRedirect("~/Account/Login.aspx", "", "")
        End If

        m_SiteMaster = Me.Master

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oCBOPeriod As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler m_SiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler m_SiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(m_SessionData.Utente.Games.IDGame) ' Ultimo periodo
        oCBOPeriod.SelectedValue = Session("IDPeriod")
        m_SiteMaster.PeriodChange = Session("IDPeriod")

        Session("NameTeam") = GetTeamName(Session("IDTeam"))
        oCBOTeam.SelectedValue = Session("IDTeam")
        m_SiteMaster.TeamChange = Session("IDTeam")

        ' Aggiungo le variabili da gestire nella lista
        m_Variable = New List(Of String)
        m_Variable.Add("SalesPriceRequi")
        m_Variable.Add("AdverRequi")
        m_Variable.Add("PriceMarkeRequi")

        m_Variable.Add("InterMarkeRequi")

        m_Variable.Add("TermsPaymeWhole")
        m_Variable.Add("SalesPriceAllow")
        m_Variable.Add("AdverAllow")

        m_Variable.Add("InterPriceAllow")

        HandleLoadPlayerDecision()

        ' Gestione del pannello delle decisioni concesse
        modalHelp.OpenerElementID = lnkHelp.ClientID
        modalHelp.Modal = False
        modalHelp.VisibleTitlebar = True

        LoadHelp()

    End Sub

    Private Sub HandleLoadPlayerDecision()
        Dim bAggiungiHeader As Boolean = False

        Dim oDTDecisionsPlayer As DataTable
        Dim oDTDecisionsPlayerValue As DataTable

        Dim sHeader As String = ""

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Dim sNomeVariabile As String
        Dim sTraduzione As String
        Dim sTraduzioneExternalObject As String

        Dim bRigaVisibile As Boolean = False

        Dim iContaHeader As Integer = 0

        ' Carico la tabella delle intestazioni da usare
        oDTDecisionsPlayer = HandleLoadGameDecisionPlayer(m_SessionData.Utente.Games.IDGame, eVariablesCateogry.Marketing)

        ' Recupero le informazioni necessarie per costruirmi l'header della tabella HTML
        Dim oDTHeader As DataTable = oDTDecisionsPlayer

        ' Per ogni riga di intestazione inizio a costruire la tabella
        For Each oRowHeader As DataRow In oDTHeader.Rows
            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            If Nz(oRowHeader("VariableLabelGroup")) = "0" Or Nz(oRowHeader("VariableLabelGroup")) = "" Then
                If iContaHeader = 0 Then
                    Dim lblRetailDecisions As New RadLabel
                    lblRetailDecisions.ID = "lblRetailDecisions"
                    lblRetailDecisions.Text = "Retail decisions"
                    lblRetailDecisions.ForeColor = Drawing.Color.White
                    tblHeaderCell.Controls.Add(lblRetailDecisions)
                    bAggiungiHeader = True

                ElseIf iContaHeader = 2 Then
                    Dim lblWholesalersDecisions As New RadLabel
                    lblWholesalersDecisions.ID = "lblWholesalersDecisions"
                    lblWholesalersDecisions.Text = "Wholesalers decisions"
                    lblWholesalersDecisions.ForeColor = Drawing.Color.White
                    tblHeaderCell.Controls.Add(lblWholesalersDecisions)
                    bAggiungiHeader = True

                End If

            Else
                Dim lblVariableLabelGroup As New RadLabel
                lblVariableLabelGroup.ID = "lblVariableLabelGroup_" & Nz(oRowHeader("VariableLabelGroup"))
                lblVariableLabelGroup.Text = Nz(oRowHeader("VariableLabelGroup"))
                lblVariableLabelGroup.ForeColor = Drawing.Color.White

                tblHeaderCell.Controls.Add(lblVariableLabelGroup)

                bAggiungiHeader = True

            End If

            iContaHeader += 1

            tblHeaderCell.ColumnSpan = 3
            tblHeaderCell.BorderStyle = BorderStyle.None

            tblHeaderRow.Cells.Add(tblHeaderCell)
            tblHeaderRow.BorderStyle = BorderStyle.None
            tblHeaderRow.Style.Add("margin-top", "10px")

            ' Aggiungo la l'header solo se ho del testo di identificazione del gruppo
            'If tblHeaderCell.Text <> "" AndAlso sHeader <> Nz(oRowHeader("VariableLabelGroup")) Then tblPlayerDecision.Rows.Add(tblHeaderRow)
            If bAggiungiHeader Then
                tblPlayerDecision.Rows.Add(tblHeaderRow)
                bAggiungiHeader = False
            End If

            ' Carico i valori della variabile, e controllo eventualmente se è una variabile dipendente da qualche lista
            oDTDecisionsPlayerValue = HandleLoadGamePeriodDecisionPlayerValue(m_SiteMaster.PeriodGetCurrent, Nni(oRowHeader("ID")))

            ' Ciclo sui valori delle varibili trovate
            For Each oRowVariableValue As DataRow In oDTDecisionsPlayerValue.Rows
                sTraduzione = GetVariableTranslation(Session("IDGame"), Nni(oRowVariableValue("IDVariable")), Nz(Session("LanguageActive")))
                If Nz(oRowVariableValue("VariableValue")).ToUpper = "[LIST OF ITEMS]" Then ' Lista di ITEMS/Product
                    ' Carico i valori presenti nella tabella Decisions_Players_value_List
                    Dim oDTValoriPLayer As DataTable = HandleGetValueDecisionPlayer(Nni(oRowVariableValue("ID")), Nni(Session("IDTeam")), m_SiteMaster.PeriodGetCurrent)

                    If oDTValoriPLayer.Rows.Count > 0 Then
                        For Each oROWValore As DataRow In oDTValoriPLayer.Rows
                            bRigaVisibile = False
                            ' Recupero la traduzione corretta dell'item 
                            sTraduzioneExternalObject = GetItemTranslation(Nni(oROWValore("IDItem")), Nz(Session("LanguageActive")))

                            'sNomeVariabile = "var_" & Nz(Session("NameTeam") & "_" & Nni(oRowHeader("Id")) & "_" & Nz(oROWValore("VariableName"))).Replace(" ", "")
                            sNomeVariabile = Nz(oROWValore("VariableName"))
                            ' Etichetta della variabile
                            tblRow = New TableRow
                            tblCell = New TableCell

                            ' Recupero la traduzione in lingua
                            tblCell.Text = sTraduzione & " - " & sTraduzioneExternalObject
                            tblCell.BorderStyle = BorderStyle.None
                            tblCell.Style.Add("width", "80%")
                            tblRow.Cells.Add(tblCell)

                            ' Creo i controlli necessari ed eventualmente li popolo
                            ' Carico i valori esistenti, altrimenti li creo
                            Dim oTxtBox As New TextBox
                            tblCell = New TableCell
                            oTxtBox.Text = Nz(oROWValore("Value"))
                            oTxtBox.ID = Nz(sNomeVariabile)
                            oTxtBox.Attributes.Add("runat", "server")
                            oTxtBox.Style.Add("text-align", "right")
                            oTxtBox.ClientIDMode = ClientIDMode.Static
                            oTxtBox.EnableViewState = True
                            oTxtBox.TextMode = TextBoxMode.Number
                            ' Controllo se la decisione deve essere abilitata in un determinato periodo oppure se devo solo disabilitarla
                            If Nni(oRowHeader("EnabledFromPeriod"), 0) > 0 Then
                                oTxtBox.Enabled = Nni(Session("IDPeriod")) >= (Nni(oRowHeader("EnabledFromPeriod"), 0))
                                bRigaVisibile = Nni(Session("IDPeriod")) >= (Nni(oRowHeader("EnabledFromPeriod"), 0))

                            Else
                                oTxtBox.Enabled = Nb(oRowHeader("Attiva"))
                                bRigaVisibile = Nb(oRowHeader("Attiva"))

                            End If

                            ' Aggiungo il textbox appena creato alla cella
                            tblCell.Controls.Add(oTxtBox)
                            tblCell.Style.Add("text-align", "right")
                            tblCell.BorderStyle = BorderStyle.None
                            tblRow.Cells.Add(tblCell)

                            If bRigaVisibile Then
                                tblPlayerDecision.Rows.Add(tblRow)
                            End If
                        Next

                    End If

                Else
                    Dim oDTValoriPlayer As DataTable = HandleLoadGamePeriodDecisionPlayerValue(m_SiteMaster.PeriodGetCurrent, Nni(oRowVariableValue("IDDecisionPlayer")))
                    For Each oRowData As DataRow In oDTValoriPlayer.Rows
                        If Nni(oRowData("IDPlayer")) = Nni(Session("IDTeam")) Then
                            sNomeVariabile = "var_" & Nz(oRowData("IDPlayer") & "_" & Nz(oRowData("VariableName"))).Replace(" ", "")
                            ' Controllo la presenza di un eventuale textbox con lo stesso ID
                            ' Se presente esco, altrimenti lo creo correttamente
                            Dim oTXT As TextBox = TryCast(FindControlRecursive(Page, sNomeVariabile), TextBox)
                            If oTXT Is Nothing Then
                                ' Etichetta della variabile
                                tblRow = New TableRow
                                tblCell = New TableCell
                                tblCell.Text = sTraduzione
                                tblCell.BorderStyle = BorderStyle.None
                                tblRow.Cells.Add(tblCell)

                                ' Creo i controlli necessari ed eventualmente li popolo
                                ' Carico i valori esistenti, altrimenti li creo
                                Dim oTxtBox As New TextBox
                                tblCell = New TableCell

                                ' Recupero il valore secco della decisione del player nel periodo
                                Dim sValore As String = HandleLoadGamePeriodDecisionPlayerValueSingle(m_SiteMaster.PeriodGetCurrent, Nni(Session("IDTeam")), Nni(oRowData("IDDecisionPlayer")))
                                oTxtBox.Text = sValore
                                oTxtBox.ID = Nz(sNomeVariabile)
                                oTxtBox.Attributes.Add("runat", "server")
                                oTxtBox.Style.Add("text-align", "right")
                                oTxtBox.ClientIDMode = ClientIDMode.Static
                                oTxtBox.EnableViewState = True
                                oTxtBox.TextMode = TextBoxMode.Number
                                ' Controllo se la decisione deve essere abilitata in un determinato periodo oppure se devo solo disabilitarla
                                If Nni(oRowHeader("EnabledFromPeriod"), 0) > 0 Then
                                    oTxtBox.Enabled = Nni(Session("IDPeriod")) >= (Nni(oRowHeader("EnabledFromPeriod"), 0))
                                    bRigaVisibile = Nni(Session("IDPeriod")) >= (Nni(oRowHeader("EnabledFromPeriod"), 0))

                                Else
                                    oTxtBox.Enabled = Nb(oRowHeader("Attiva"))
                                    bRigaVisibile = Nb(oRowHeader("Attiva"))

                                End If

                                ' Aggiungo il textbox appena creato alla cella
                                tblCell.Controls.Add(oTxtBox)
                                tblCell.Style.Add("text-align", "right")
                                tblCell.BorderStyle = BorderStyle.None
                                tblRow.Cells.Add(tblCell)

                                If bRigaVisibile Then
                                    tblPlayerDecision.Rows.Add(tblRow)
                                End If
                            End If
                        End If
                    Next
                End If
            Next
            sHeader = Nz(oRowHeader("VariableLabelGroup"))
        Next

    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim oDTDecisionsPlayer As DataTable = HandleLoadGameDecisionPlayer(m_SessionData.Utente.Games.IDGame, eVariablesCateogry.Marketing)
        Dim sNomeTextBox As String
        Dim sNomeTeam As String = Nz(m_SiteMaster.TeamNameGet).Replace(" ", "")
        Dim sSQL As String
        Dim oDTDecisionPlayerValue As DataTable
        Dim oTXT As TextBox
        Dim oDAL As New DBHelper

        Message.Visible = False

        ' Controllo che effettivamente possa salvare e non sia scaduto il termine delle decisioni
        If Not IsPossibleToSaveDataPeriod(Session("IDGame"), Session("IDPeriod")) Then
            MessageText.Text = "Time over for this period"
            Message.Visible = True

            pnlMain.Update()

            btnSave.Enabled = False

            Exit Sub
        End If

        g_DAL = New DBHelper

        pnlMain.Update()

        For Each oRow As DataRow In oDTDecisionsPlayer.Rows
            ' Controllo se la varibile recuperata è di tipo lista altrimenti passo alle variabili con inserimento valore secco
            sSQL = "SELECT VariableValue FROM Decisions_Players_Value WHERE IDDecisionPlayer = " & Nni(oRow("ID"))
            Dim sVariableType As String = Nz(oDAL.ExecuteScalar(sSQL))

            sSQL = "SELECT ID FROM Decisions_Players_Value WHERE IDDecisionPlayer = " & Nni(oRow("ID"))
            Dim iIDDecisionPlayerValue As Integer = Nni(oDAL.ExecuteScalar(sSQL))

            If sVariableType.ToUpper.Contains("LIST OF") Then
                ' Recupero i dati della variabile interessata per cercare anche il Textbox che contiene i valori da salvare
                sSQL = "SELECT * FROM Decisions_Players_Value_List WHERE IDDecisionValue =" & iIDDecisionPlayerValue & " AND IDPlayer = " & Session("IDTeam")
                Dim oDTDecisionsValueList As DataTable = oDAL.ExecuteDataTable(sSQL)

                For Each oRowValueList As DataRow In oDTDecisionsValueList.Rows
                    sNomeTextBox = Nz(oRowValueList("VariableName"))

                    oTXT = TryCast(FindControlRecursive(Page, sNomeTextBox), TextBox)
                    If oTXT IsNot Nothing AndAlso oTXT.Text <> "" Then
                        ' Vado a salvare i valori corretti nella tabella di riferimento
                        sSQL = "SELECT * FROM Decisions_Players_Value_List WHERE IDPlayer = " & Nni(Session("IDTeam")) _
                             & " And IDDecisionValue = " & iIDDecisionPlayerValue & " And VariableName = '" & Nz(oRowValueList("VariableName")) & "' "
                        oDTDecisionPlayerValue = oDAL.ExecuteDataTable(sSQL)
                        If oDTDecisionPlayerValue.Rows.Count > 0 Then ' Ci sono dati vado in update
                            sSQL = "UPDATE Decisions_Players_Value_List SET Value = '" & oTXT.Text & "' " _
                                 & "WHERE IDPlayer = " & Nni(Session("IDTeam")) & " AND IDDecisionValue = " & iIDDecisionPlayerValue & " " _
                                 & "AND IDPeriod = " & Nni(Session("IDPeriod")) & " AND VariableName = '" & Nz(oRowValueList("VariableName")) & "' "
                            oDAL.ExecuteNonQuery(sSQL)

                            If Nni(Session("IDTeamM&U")) > 0 Then
                                If Nni(Session("IDTeam")) = Nni(Session("IDTeamMaster")) Then
                                    sSQL = "UPDATE Decisions_Players_Value_List SET Value = '" & oTXT.Text & "' " _
                                         & "WHERE IDPlayer = " & Nni(Session("IDTeamM&U")) & " AND IDDecisionValue = " & iIDDecisionPlayerValue & " " _
                                         & "AND IDPeriod = " & Nni(Session("IDPeriod")) & " AND VariableName = '" & Nz(oRowValueList("VariableName")) & "' "
                                    oDAL.ExecuteNonQuery(sSQL)
                                End If
                            End If
                        End If
                    End If
                Next

            Else
                ' Recupero tutti i campi che non necessitano di items o oggetti esterni
                ' E' fallita la ricerca, provo a ricercare il textbox nelle variabili a uso singolo, senza gestione del list of
                sSQL = "SELECT * FROM Decisions_Players_Value WHERE IDDecisionPlayer = " & Nni(oRow("ID"))
                Dim oDTDataValue As DataTable = oDAL.ExecuteDataTable(sSQL)

                For Each oRowDataValue As DataRow In oDTDataValue.Rows
                    sNomeTextBox = "var_" & Nz(oRowDataValue("IDPlayer") & "_" & Nz(oRow("VariableName")))

                    oTXT = TryCast(FindControlRecursive(Page, sNomeTextBox), TextBox)
                    If oTXT IsNot Nothing AndAlso oTXT.Text <> "" Then
                        sSQL = "UPDATE Decisions_Players_Value SET VariableValue = '" & oTXT.Text & "' WHERE IDDecisionPlayer = " & Nni(oRow("ID")) & " AND IDPlayer = " & Nni(Session("IDTeam")) _
                             & " AND ISNULL(IDPeriod, " & Nni(Session("IDPeriod")) & ") = " & Nni(Session("IDPeriod"))
                        oDAL.ExecuteNonQuery(sSQL)

                        If Nni(Session("IDTeamM&U")) > 0 Then
                            If Nni(Session("IDTeam")) = Nni(Session("IDTeamMaster")) Then
                                sSQL = "UPDATE Decisions_Players_Value SET VariableValue = '" & oTXT.Text & "' WHERE IDDecisionPlayer = " & Nni(oRow("ID")) & " AND IDPlayer = " & Nni(Session("IDTeamM&U")) _
                                     & " AND ISNULL(IDPeriod, " & Nni(Session("IDPeriod")) & ") = " & Nni(Session("IDPeriod"))
                                oDAL.ExecuteNonQuery(sSQL)
                            End If
                        End If
                    End If
                Next

            End If

        Next

        SQLConnClose(oDAL.GetConnObject)

        oDAL = Nothing

        MessageText.Text = "Save completed"
        Message.Visible = True
    End Sub

    Private Sub btnTranslate_Click(sender As Object, e As EventArgs) Handles btnTranslate.Click
        Session("PageOrigin") = Me.ToString().Substring(4, Me.ToString().Substring(4).Length - 5) + ".aspx"
        Response.Redirect("PageTranslation.aspx")
        Response.End()
    End Sub

    Private Sub btnManageDecisions_Click(sender As Object, e As EventArgs) Handles btnManageDecisions.Click
        Session("PageOrigin") = Me.ToString().Substring(4, Me.ToString().Substring(4).Length - 5) + ".aspx"
        Response.Redirect("PageManageDecisions.aspx")
        Response.End()
    End Sub

    Private Sub Marketing_Player_Decision_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        ' Controllo tutti gli oggetti presenti
        Dim oTranslation As New TranslationObjectForm(Me, g_DAL, Nz(Session("LanguageActive")))

        btnTranslate.Visible = Session("IDRole").Contains("D")
        btnManageDecisions.Visible = Session("IDRole").Contains("D")

        EnableSave()
    End Sub

#Region "PANNELLO HELP"

    Private Sub LoadHelp()
        lblHelp.Text = "<p><strong>Retail Market Price</strong><br>
The selling  price per unit must be decided for each of the 3 products. <br>
These must  be in whole numbers. &nbsp;&nbsp;Goods cannot be  sold below the cost of production. The upper limit will &nbsp;be determined by market forces. In no period  can a price be more than &nbsp;250% higher  than in the previous period. </p>
                    <p><strong>Advertising</strong><br>
  &nbsp;The amount spent on store publicity will  impact on your company image.<br>
                      The effect  of investment is immediate and cumulative.</p>
                    <p><strong>Wholesalers</strong><br>
                      The  wholesale market is completely separate from the retail market and success  criteria are different.&nbsp; Each period the  wholesale market establishes quantities, minimum relative quality acceptable  and maximum price considered.<br>
                      You have  the opportunity to enter the Wholesale market or not.&nbsp; Your success in the wholesale market reflects  price, product quality, and the terms of payment.&nbsp; There are no transport charges involved, the  wholesaler is responsible for collecting from the point of manufacture.<br>
                      The  wholesale market is supplied from current production.&nbsp; It cannot be supplied from stock, from  earlier production runs, nor can bought-in stock be sold on to the wholesale  market.&nbsp; Goods for the wholesale market  have priority over retail sales and are taken from production before the retail  market.<br>
  <strong>Price to the Wholesalers</strong><br>
                      The  relevant prices. The price must be equal to or lower than that stated &nbsp;by wholesaler. <br>
                      Price  cannot be varied by more than 20% of your previous stated price.<br>
                      <strong>Quantity&nbsp; offered to the Wholesalers.</strong><br>
                      The maximum  no. of units you are prepared to supply in each product category. <br>
                      You may  offer less than the full amount required but you cannot offer &nbsp;more than the amount required.<br>
                      The quality  must be equal to or higher than that stated by wholesaler. <br>
  <strong>Terms of Payment</strong><br>
                      The payment  terms- the number of months credit you are prepared to offer &nbsp;the wholesaler, 0 - 3 months.</p>"
    End Sub

#End Region

End Class
