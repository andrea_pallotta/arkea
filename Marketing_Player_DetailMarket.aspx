﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Marketing_Player_DetailMarket.aspx.vb" Inherits="Marketing_Player_DetailMarket" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="cntMain" ContentPlaceHolderID="MainContent" runat="Server" ClientIDMode="Static">

    <asp:UpdatePanel ID="pnlMain" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <h2>
                <asp:Label ID="lblWelcome" runat="server" Text="Marketing"></asp:Label>
            </h2>

            <div class="clr"></div>

            <div class="divTable">
                <div class="divTableRow">
                    <div class="divTableCell">
                        <h3>
                            <asp:Label ID="lblTitolo" runat="server" Text="Retail market"></asp:Label>
                        </h3>
                    </div>
                    <div class="divTableCell" style="text-align: right; font-size: 8pt">
                        <telerik:RadLinkButton ID="lnkHelp" runat="server" Text="Help" EnableAjaxSkinRendering="true" ToolTip="Show help"></telerik:RadLinkButton>
                    </div>
                </div>

                <div class="divTableCell">
                    <telerik:RadComboBox ID="cboItems" runat="server" Width="50%" EmptyMessage="< Select item... >"
                        RenderMode="Lightweight" RenderingMode="Full" AutoPostBack="true">
                    </telerik:RadComboBox>
                </div>

                <div class="row">
                </div>
            </div>

            <div class="row" style="margin-top: 20px;">
                <asp:Table ClientIDMode="Static" ID="tblPlayerRetailMarket" CssClass="tableMarketRetail" runat="server">
                </asp:Table>
            </div>

            <div class="row">

                <telerik:RadHtmlChart ID="grfDemandSales" runat="server" Width="100%">
                    <ChartTitle Text="Retail market demand and sales"></ChartTitle>

                    <PlotArea>
                        <Series>
                            <telerik:ColumnSeries Name="Demand" DataFieldY="MarketDemand">
                                <TooltipsAppearance DataFormatString="{0:2P}" BackgroundColor="White" Color="Blue" />
                                <LabelsAppearance Visible="false" Position="Center" RotationAngle="-90" Color="#FFE038" />
                                <Appearance FillStyle-BackgroundColor="#2359FC" />
                            </telerik:ColumnSeries>

                            <telerik:LineSeries Name="Sales" DataFieldY="GlobaMarkeSales">
                                <Appearance FillStyle-BackgroundColor="#B67C00" />
                                <LineAppearance LineStyle="Smooth" Width="3px" />
                                <TooltipsAppearance DataFormatString="{0:2N}" BackgroundColor="White" Color="#B67C00" />
                                <LabelsAppearance Visible="false" />
                            </telerik:LineSeries>
                        </Series>

                        <XAxis>
                        </XAxis>

                        <YAxis>
                            <LabelsAppearance DataFormatString="{0}" />
                        </YAxis>

                    </PlotArea>

                    <Legend>
                        <Appearance Visible="true" Position="Top" />
                    </Legend>
                </telerik:RadHtmlChart>
            </div>

            <div class="row" style="min-height: 30px;">
            </div>

            <div class="row">
                <asp:Table ClientIDMode="Static" ID="tblPlayerRetailMarketShare" runat="server" Style="width: 50%">
                </asp:Table>
            </div>

            <div class="row">
                <telerik:RadHtmlChart ID="grfFabricsQuote" runat="server" Width="100%">

                    <Legend>
                        <Appearance Position="Left" Visible="true">
                        </Appearance>
                    </Legend>

                    <PlotArea>
                        <Series>
                            <telerik:PieSeries StartAngle="0">

                                <LabelsAppearance Position="OutsideEnd" DataFormatString="{0} %">
                                </LabelsAppearance>

                                <TooltipsAppearance Color="White" DataFormatString="{0:N2} %">
                                </TooltipsAppearance>


                            </telerik:PieSeries>
                        </Series>
                    </PlotArea>
                </telerik:RadHtmlChart>

            </div>

            <telerik:RadWindow RenderMode="Lightweight" ID="modalHelp" runat="server" Width="520px" Height="450px" CenterIfModal="false"
                Style="z-index: 100001;" BorderStyle="None" Behaviors="Close, Move" Title="Help">
                <ContentTemplate>
                    <div style="padding: 10px; text-align: left;">
                        <div id="divTableMaster" runat="server" style="margin-bottom: 10px;">
                            <asp:Label runat="server" ID="lblHelp" Visible="true">

                            </asp:Label>
                        </div>
                    </div>

                </ContentTemplate>
            </telerik:RadWindow>

            <div class="row">
                <div class="divTableCell" style="text-align: right;">
                    <telerik:RadButton ID="btnTranslate" runat="server" Text="Translate" EnableAjaxSkinRendering="true" ToolTip=""></telerik:RadButton>
                </div>
            </div>

            <asp:UpdatePanel ID="pnlMessage" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="row">
                        <telerik:RadLabel runat="server" ID="lblMessage" Visible="false"></telerik:RadLabel>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>


        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
