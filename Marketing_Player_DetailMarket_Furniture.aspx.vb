﻿Imports System.Data
Imports APS.APSUtility
Imports APPCore.Utility
Imports DALC4NET
Imports Telerik.Web.UI

Partial Class Marketing_Player_DetailMarket_Furniture
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster
    Private m_IDItem As Integer = 3

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)

        LoadRetailMarket()
        lblMessage.Visible = False

        pnlMain.Update()
    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)

        LoadRetailMarket()
        lblMessage.Visible = False
        pnlMain.Update()
    End Sub

    Private Sub LoadRetailMarket()
        Dim sSQL As String

        Dim oDTVariableCalculateValue As DataTable
        Dim oDTPlayers As DataTable = LoadTeamsGame(Session("IDGame"))

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Dim dTotProduSold As Double
        Dim dTotRevenMarke As Double
        Dim dDomandaTotale As Double

        Try
            grfDemandSales.Visible = True
            grfFurnitureQuote.Visible = True

            tblPlayerRetailMarket.Rows.Clear()
            tblPlayerRetailMarketShare.Rows.Clear()

            sSQL = "SELECT * FROM Variables_Calculate_Define V " _
                 & "INNER JOIN Variables_Calculate_Value C ON V.Id = C.IDVariable " _
                 & "WHERE C.IDPeriod = " & m_SiteMaster.PeriodGetCurrent
            If Session("IDRole") = "P" Then
                sSQL &= " AND ISNULL(C.Simulation, 0) = 0 "
            End If
            oDTVariableCalculateValue = g_DAL.ExecuteDataTable(sSQL)

            If oDTVariableCalculateValue.Rows.Count = 0 Then
                grfDemandSales.Visible = False
                grfFurnitureQuote.Visible = False
                lblTitolo.Text &= "<br/>" & "<br/>" & "No data available for this period"

            Else
                lblTitolo.Text = "Retail market - furniture"


                ' Costruisco la riga Header
                tblHeaderRow = New TableHeaderRow
                tblHeaderCell = New TableHeaderCell

                ' Prima cella della prima riga dell'Header vuota
                tblHeaderCell.Text = ""
                tblHeaderCell.BorderStyle = BorderStyle.None
                tblHeaderRow.Cells.Add(tblHeaderCell)

                ' Cella del prezzo di vendita
                tblHeaderCell = New TableHeaderCell
                tblHeaderCell.Text = "Price (€)"
                tblHeaderCell.BorderStyle = BorderStyle.None
                tblHeaderRow.Cells.Add(tblHeaderCell)

                ' Cella della quantità
                tblHeaderCell = New TableHeaderCell
                tblHeaderCell.Text = "Q.ty"
                tblHeaderCell.BorderStyle = BorderStyle.None
                tblHeaderRow.Cells.Add(tblHeaderCell)

                ' Cella del prezzodi vendita
                tblHeaderCell = New TableHeaderCell
                tblHeaderCell.Text = "Value (€)"
                tblHeaderCell.BorderStyle = BorderStyle.None
                tblHeaderRow.Cells.Add(tblHeaderCell)

                tblHeaderRow.BorderStyle = BorderStyle.None
                tblHeaderRow.Style.Add("margin-top", "10px")

                tblPlayerRetailMarket.Rows.Add(tblHeaderRow)

                ' Carico il totale della domanda
                Dim oDRTempTotale As DataRow = oDTVariableCalculateValue.Select("VariableName = 'MarketDemand' AND IDItem = " & m_IDItem).First
                dDomandaTotale = Nn(oDRTempTotale("Value"))

                ' Controllo la presenza delle variabili nella tabella delle variabili del gioco
                VerifiyVariableExists("SalesPriceAllow", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)
                VerifiyVariableExists("TotalProduSold", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)
                VerifiyVariableExists("ProduRevenMarke", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)
                VerifiyVariableExists("MarketDemand", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)

                ' Inizio a ciclare sui player per recuperare i valori che mi servono al popolamento della tabella
                For Each oRowP As DataRow In oDTPlayers.Rows
                    ' Etichetta della variabile
                    tblRow = New TableRow

                    ' Setto il nome del player/team
                    tblCell = New TableCell
                    tblCell.Text = oRowP("TeamName")
                    tblCell.BorderStyle = BorderStyle.None
                    tblRow.Cells.Add(tblCell)

                    ' Setto il valore del prezzo
                    tblCell = New TableCell
                    Dim oLinkSalesPriceAllow As New HyperLink
                    oLinkSalesPriceAllow.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=SalesPriceAllow&II=" & m_IDItem & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                    oLinkSalesPriceAllow.Style.Add("text-decoration", "none")
                    oLinkSalesPriceAllow.Style.Add("cursor", "pointer")

                    Dim oDRTemp As DataRow = oDTVariableCalculateValue.Select("VariableName = 'SalesPriceAllow' AND IDPlayer = " & oRowP("ID") & " AND IDItem = " & m_IDItem).First
                    oLinkSalesPriceAllow.Text = Nn(oDRTemp("Value")).ToString("C2")
                    tblCell.Controls.Add(oLinkSalesPriceAllow)

                    tblCell.BorderStyle = BorderStyle.None
                    tblRow.Cells.Add(tblCell)

                    ' Setto il valore della quantità
                    tblCell = New TableCell
                    Dim oLinkTotalProduSold As New HyperLink
                    oLinkTotalProduSold.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=TotalProduSold&II=" & m_IDItem & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                    oLinkTotalProduSold.Style.Add("text-decoration", "none")
                    oLinkTotalProduSold.Style.Add("cursor", "pointer")

                    Dim oDRTempQty As DataRow = oDTVariableCalculateValue.Select("VariableName = 'TotalProduSold' AND IDPlayer = " & oRowP("ID") & " AND IDItem = " & m_IDItem).First
                    oLinkTotalProduSold.Text = Nn(oDRTempQty("Value")).ToString("N0")
                    tblCell.Controls.Add(oLinkTotalProduSold)

                    tblCell.BorderStyle = BorderStyle.None
                    tblRow.Cells.Add(tblCell)
                    dTotProduSold += Nn(oDRTempQty("Value"))

                    ' Setto il valore del valore 
                    tblCell = New TableCell
                    Dim oLinkProduRevenMarke As New HyperLink
                    oLinkProduRevenMarke.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=ProduRevenMarke&II=" & m_IDItem & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                    oLinkProduRevenMarke.Style.Add("text-decoration", "none")
                    oLinkProduRevenMarke.Style.Add("cursor", "pointer")

                    Dim oDRTempValue As DataRow = oDTVariableCalculateValue.Select("VariableName = 'ProduRevenMarke' AND IDPlayer = " & oRowP("ID") & " AND IDItem = " & m_IDItem).First
                    oLinkProduRevenMarke.Text = Nn(oDRTempValue("Value")).ToString("C2")
                    tblCell.Controls.Add(oLinkProduRevenMarke)

                    tblCell.BorderStyle = BorderStyle.None
                    tblRow.Cells.Add(tblCell)
                    dTotRevenMarke += Nn(oDRTempValue("Value"))

                    tblPlayerRetailMarket.Rows.Add(tblRow)
                Next
                ' Riga vuota
                tblRow = New TableRow
                tblCell = New TableCell
                tblCell.ColumnSpan = 4
                tblCell.Text = "<hr/>"
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)
                tblPlayerRetailMarket.Rows.Add(tblRow)

                ' Aggiungo le righe di riepilogo 
                tblRow = New TableRow
                tblCell = New TableCell
                tblCell.Text = "Total sales"
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                tblCell.Text = ""
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                Dim oLinkTotaleProduSold As New HyperLink
                oLinkTotaleProduSold.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=TotalProduSold&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                oLinkTotaleProduSold.Style.Add("text-decoration", "none")
                oLinkTotaleProduSold.Style.Add("cursor", "pointer")

                oLinkTotaleProduSold.Text = dTotProduSold.ToString("N0")
                tblCell.Controls.Add(oLinkTotaleProduSold)

                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                tblCell.Text = dTotRevenMarke.ToString("C2")
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)

                tblPlayerRetailMarket.Rows.Add(tblRow)
                ' -------------------------------------------------
                tblRow = New TableRow
                tblCell = New TableCell
                tblCell.Text = "Total demand"
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                tblCell.Text = ""
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                Dim oLinkDomandaTotale As New HyperLink
                oLinkDomandaTotale.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=MarketDemand&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                oLinkDomandaTotale.Style.Add("text-decoration", "none")
                oLinkDomandaTotale.Style.Add("cursor", "pointer")

                oLinkDomandaTotale.Text = dDomandaTotale.ToString("N0")
                tblCell.Controls.Add(oLinkDomandaTotale)

                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                tblCell.Text = ""
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)

                tblPlayerRetailMarket.Rows.Add(tblRow)
                ' -------------------------------------------------
                tblRow = New TableRow
                tblCell = New TableCell
                tblCell.Text = "Unsatisfied demand"
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                tblCell.Text = ""
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                tblCell.Text = (dDomandaTotale - dTotProduSold).ToString("N0")
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                tblCell.Text = ""
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)

                tblPlayerRetailMarket.Rows.Add(tblRow)

                LoadRetailMarketShares

                ' Popolo i grafici
                LoadGraphFabricQuote()
                LoadGraphDemandSales()

            End If

        Catch ex As Exception
            lblMessage.Text = ex.Message
            lblMessage.Visible = True
        End Try
    End Sub

    Private Sub LoadRetailMarketShares()
        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Dim oDTPlayers As DataTable = LoadTeamsGame(Session("IDGame"))
        Dim sSQL As String
        Dim oDTVariableCalculateValue As DataTable

        Try
            sSQL = "SELECT * FROM Variables_Calculate_Define V " _
                 & "INNER JOIN Variables_Calculate_Value C ON V.Id = C.IDVariable " _
                 & "WHERE C.IDPeriod = " & m_SiteMaster.PeriodGetCurrent & " AND V.VariableName = 'MarketDemand' "
            If Session("IDRole") = "P" Then
                sSQL &= " AND ISNULL(C.Simulation, 0) = 0 "
            End If
            oDTVariableCalculateValue = g_DAL.ExecuteDataTable(sSQL)

            If oDTVariableCalculateValue.Rows.Count = 0 Then
                lblTitolo.Text &= "<br/>" & "<br/>" & "No data available for this period"

            Else
                ' Costruisco la riga Header
                tblHeaderRow = New TableHeaderRow
                tblHeaderCell = New TableHeaderCell

                ' Prima cella della prima riga dell'Header vuota
                Dim oLBLTitleGrid As New RadLabel
                oLBLTitleGrid.ID = "lblGridTitle"
                oLBLTitleGrid.Text = "Furniture shares"
                oLBLTitleGrid.ForeColor = Drawing.Color.White
                tblHeaderCell.Controls.Add(oLBLTitleGrid)
                tblHeaderCell.Style.Add("background", "#525252")
                tblHeaderCell.ColumnSpan = 2

                tblHeaderRow.Cells.Add(tblHeaderCell)
                tblHeaderRow.BorderStyle = BorderStyle.None
                tblHeaderRow.Style.Add("margin-top", "10px")

                tblPlayerRetailMarketShare.Rows.Add(tblHeaderRow)

                tblRow = New TableRow
                tblCell = New TableCell
                Dim oLBLShareInVolume As New RadLabel
                oLBLShareInVolume.ID = "lblShareInVolume"
                oLBLShareInVolume.Text = "Share in volume"
                oLBLShareInVolume.Style.Add("font-size", "9pt")
                oLBLShareInVolume.Style.Add("text-align", "left")
                oLBLShareInVolume.Style.Add("font-weight", "bold")
                oLBLShareInVolume.Style.Add("color", "#ffffff")
                tblCell.Controls.Add(oLBLShareInVolume)
                tblCell.BorderStyle = BorderStyle.None
                tblCell.Style.Add("padding-left", "20px")
                tblCell.Style.Add("background", "#3e6c86")
                tblCell.Style.Add("text-align", "right")
                tblCell.ColumnSpan = 2
                tblRow.Cells.Add(tblCell)

                tblPlayerRetailMarketShare.Rows.Add(tblRow)

                ' Inizio a ciclare sui player per recuperare i valori che mi servono al popolamento della tabella
                For Each oRowP As DataRow In oDTPlayers.Rows
                    ' Etichetta della variabile
                    tblRow = New TableRow
                    tblCell = New TableCell

                    ' Setto il nome del player/team
                    Dim oLBLTeamName As New RadLabel
                    oLBLTeamName.ID = "lblTeamName" & oRowP("TeamName")
                    oLBLTeamName.Text = oRowP("TeamName")
                    oLBLTeamName.Style.Add("font-size", "9pt")
                    oLBLTeamName.Style.Add("text-align", "left")
                    oLBLTeamName.Style.Add("font-weight", "normal")
                    tblCell.Controls.Add(oLBLTeamName)
                    tblCell.Style.Add("padding-left", "5px")
                    tblRow.Cells.Add(tblCell)

                    ' Setto il valore del prezzo
                    tblCell = New TableCell
                    tblCell.Style.Add("font-size", "9pt")
                    tblCell.Style.Add("text-align", "right")
                    tblCell.BorderStyle = BorderStyle.None
                    tblCell.Style.Add("text-align", "right")
                    tblCell.BackColor = Drawing.Color.Transparent

                    Dim oLinkMarketShare As New HyperLink
                    oLinkMarketShare.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=MarketShare&II=" & m_IDItem & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                    oLinkMarketShare.Style.Add("text-decoration", "none")
                    oLinkMarketShare.Style.Add("cursor", "pointer")
                    oLinkMarketShare.Text = Nn(GetVariableDefineValue("MarketShare", oRowP("ID"), m_IDItem, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2") & " %"

                    tblCell.Controls.Add(oLinkMarketShare)
                    tblCell.BorderStyle = BorderStyle.None
                    tblRow.Cells.Add(tblCell)

                    tblPlayerRetailMarketShare.Rows.Add(tblRow)
                Next
            End If

        Catch ex As Exception
            lblMessage.Text = ex.Message
            lblMessage.Visible = True
        End Try
    End Sub

    Private Sub Marketing_Player_DetailMarket_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        m_SessionData = Session("SessionData")

        m_SiteMaster = Me.Master

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oCBOPeriod As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler m_SiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler m_SiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(m_SessionData.Utente.Games.IDGame) ' Ultimo periodo
        oCBOPeriod.SelectedValue = Session("IDPeriodValid")
        m_SiteMaster.PeriodChange = Session("IDPeriodValid")

        Session("NameTeam") = GetTeamName(Session("IDTeam"))
        oCBOTeam.SelectedValue = Session("IDTeam")
        m_SiteMaster.TeamChange = Session("IDTeam")

        ' Gestione del pannello delle decisioni concesse
        modalHelp.OpenerElementID = lnkHelp.ClientID
        modalHelp.Modal = False
        modalHelp.VisibleTitlebar = True

        LoadHelp()

        ' Recupero l'IDFabrics per questo game
        Dim sSQL As String
        sSQL = "SELECT IDVariable FROM vItems WHERE IDGame = " & Session("IDGame") & " AND VariableName = 'Furniture' "
        m_IDItem = Nni(g_DAL.ExecuteScalar(sSQL))

    End Sub

    Private Sub LoadGraphFabricQuote()
        Dim sSQL As String
        Dim oDTTableGraph As DataTable

        sSQL = "SELECT DISTINCT TeamName, Value FROM Variables_Calculate_Define V " _
             & "INNER JOIN Variables_Calculate_Value C ON V.Id = C.IDVariable " _
             & "INNER JOIN BGOL_Teams T ON C.IDPlayer = T.ID " _
             & "INNER JOIN BGOL_Players_Teams_Games PLT ON T.ID = PLT.IdTeam " _
             & "WHERE VariableName = 'MarketShare' AND IDItem = " & m_IDItem & " AND C.IDPeriod = " & m_SiteMaster.PeriodGetCurrent
        If Session("IDRole") = "P" Then
            sSQL &= " AND ISNULL(C.Simulation, 0) = 0 "
        End If
        oDTTableGraph = g_DAL.ExecuteDataTable(sSQL)

        grfFurnitureQuote.DataSource = oDTTableGraph
        grfFurnitureQuote.DataBind()

        Dim pieSeries1 As New PieSeries

        For Each oRow As DataRow In oDTTableGraph.Rows
            Dim oPieSerieItem As New PieSeriesItem
            oPieSerieItem.Y = Nn(oRow("Value")).ToString("N2")
            oPieSerieItem.Name = oRow("TeamName")
            pieSeries1.SeriesItems.Add(oPieSerieItem)
        Next

        pieSeries1.LabelsAppearance.Visible = True
        pieSeries1.TooltipsAppearance.Color = System.Drawing.Color.White
        pieSeries1.TooltipsAppearance.DataFormatString = "N2"
        pieSeries1.NameField = "Value"
        pieSeries1.DataFieldY = "TeamName"
        grfFurnitureQuote.ChartTitle.Text = "Furniture quote"
        grfFurnitureQuote.PlotArea.Series.Add(pieSeries1)

    End Sub

    Private Sub LoadGraphDemandSales()
        Dim sSQL As String
        Dim oDTTableResult As DataTable
        Dim oDTTableGraph As New DataTable
        Dim dMarketDemand As Double
        Dim dGlobaMarkeSales As Double
        Dim sPeriod As String = ""
        Dim iContaRighe As Integer = 1

        ' Preparo il datatable che ospiterà i dati
        oDTTableGraph.Columns.Add("MarketDemand", GetType(Double))
        oDTTableGraph.Columns.Add("GlobaMarkeSales", GetType(Double))
        oDTTableGraph.Columns.Add("Period", GetType(String))

        sSQL = "SELECT VariableName, Value, C.IDItem AS Item, P.Descrizione AS Period " _
             & "FROM Variables_Calculate_Define V " _
             & "INNER JOIN Variables_Calculate_Value C ON V.Id = C.IDVariable " _
             & "INNER JOIN BGOL_Periods P ON C.IDPeriod = P.Id " _
             & "WHERE (VariableName = 'MarketDemand' OR VariableName = 'GlobaMarkeSales') AND IDItem = " & m_IDItem & " AND C.IDPeriod <= " & m_SiteMaster.PeriodGetCurrent & " "
        If Session("IDRole") = "P" Then
            sSQL &= " AND ISNULL(C.Simulation, 0) = 0 "
        End If
        sSQL &= "ORDER BY C.IDPeriod "
        oDTTableResult = g_DAL.ExecuteDataTable(sSQL)

        For Each oRow As DataRow In oDTTableResult.Rows
            If Nz(oRow("VariableName")) = "MarketDemand" Then dMarketDemand = Nn(oRow("Value"))

            If Nz(oRow("VariableName")) = "GlobaMarkeSales" Then dGlobaMarkeSales = Nn(oRow("Value"))

            If sPeriod <> Nz(oRow("Period")) Then sPeriod = Nz(oRow("Period"))

            If iContaRighe = 2 Then
                oDTTableGraph.Rows.Add(dMarketDemand.ToString("N2"), dGlobaMarkeSales.ToString("N2"), sPeriod)
                sPeriod = ""
                iContaRighe = 1
            Else
                iContaRighe += 1
            End If
        Next

        grfDemandSales.DataSource = oDTTableGraph
        grfDemandSales.DataBind()

        grfDemandSales.PlotArea.XAxis.DataLabelsField = "Period"
        grfDemandSales.PlotArea.XAxis.LabelsAppearance.TextStyle.Bold = True

        grfDemandSales.PlotArea.XAxis.LabelsAppearance.RotationAngle = 45
        grfDemandSales.PlotArea.XAxis.EnableBaseUnitStepAuto = True
    End Sub

    Private Sub Marketing_Player_DetailMarket_Furniture_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        If Not Page.IsPostBack Then
            If Session("IDRole") = "P" Then
                m_SiteMaster.ReloadPeriodsCalculate()
            End If
            LoadRetailMarket()
        End If
    End Sub

#Region "PANNELLO HELP"

    Private Sub LoadHelp()
        lblHelp.Text = "Retail market: prices and sales</strong><br>
                      <strong>Here you can find the retail prices set by each  company, the quantities &nbsp;sold and total  revenues.</strong></p>
                    <p><strong>Market shares</strong><br>
                        <strong>Here you can find market shares achieved by  each company based on quantities &nbsp;sold in  the total retail market.</strong></p>
                    <p><strong>Total sales</strong><br>
                        <strong>Total sales are the aggregate of the sales of  each company in the market &nbsp;place.</strong><br>
                        <strong>&nbsp;</strong><br>
                        <strong>Total demand</strong><br>
                        <strong>Market demand is affected by the economic  climate, the general level of &nbsp;market  prices, the total amount of marketing investments and the average &nbsp;quality of products offered.</strong></p>
                    <p><strong>Unsatisfied demand </strong><br>
                        <strong>It is the gap between market demand and total  sales in the retail market.</strong> </p>"
    End Sub

#End Region

End Class
