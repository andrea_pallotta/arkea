﻿Imports System.Data
Imports APS.APSUtility
Imports APPCore.Utility
Imports Telerik.Web.UI

Partial Class Marketing_Player_DetailMarket_ImmagineGraph
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadDataPage()

        Message.Visible = False
        pnlMain.Update()
    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadDataPage()

        Message.Visible = False
        pnlMain.Update()
    End Sub

    Private Sub LoadDataPage()
        lblTitolo.Text = "Advertising and environmental"

        LoadAdvertisingLevel()

        LoadGreenLevel()
    End Sub

    Private Sub LoadAdvertisingLevel()
        Dim sSQL As String

        Dim oDTVariableCalculateValue As DataTable
        Dim oDTPlayers As DataTable = LoadTeamsGame(Session("IDGame"))
        Dim oDRPlayers As DataRow()

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Dim dMedia As Double

        Try
            ' Controllo la presenza delle variabili nella tabella delle variabili del gioco
            VerifiyVariableExists("AdverQualiProce", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)

            sSQL = "SELECT * FROM Variables_Calculate_Define V " _
                 & "INNER JOIN Variables_Calculate_Value C ON V.Id = C.IDVariable " _
                 & "WHERE C.IDPeriod = " & Session("IDPeriod") & " AND V.IDGame = " & Session("IDGame")
            If Session("IDRole") = "P" Then
                sSQL &= " AND ISNULL(C.Simulation, 0) = 0 "
            End If

            oDTVariableCalculateValue = g_DAL.ExecuteDataTable(sSQL)

            If oDTVariableCalculateValue.Rows.Count > 0 Then
                ' Calcolo la media
                sSQL = "SELECT SUM(CAST(REPLACE(Value, ',', '.') AS Decimal(18, 9))) / COUNT(C.ID) AS Media " _
                 & "FROM Variables_Calculate_Define V " _
                 & "INNER JOIN Variables_Calculate_Value C ON V.Id = C.IDVariable " _
                 & "WHERE V.VariableName = 'AdverQualiProce' " & " AND V.IDGame = " & Session("IDGame") & " AND C.IDPeriod = " & Session("IDPeriod")
                If Session("IDRole") = "P" Then
                    sSQL &= " AND ISNULL(C.Simulation, 0) = 0 "
                End If
                dMedia = Nn(g_DAL.ExecuteScalar(sSQL))

                ' Costruisco la riga Header
                tblHeaderRow = New TableHeaderRow
                tblHeaderCell = New TableHeaderCell

                ' Prima cella della prima riga dell'Header vuota
                tblHeaderCell.Text = "Advertising level"
                tblHeaderCell.BorderStyle = BorderStyle.None
                tblHeaderCell.ColumnSpan = "2"
                tblHeaderRow.Cells.Add(tblHeaderCell)

                tblHeaderRow.BorderStyle = BorderStyle.None
                tblHeaderRow.Style.Add("margin-top", "10px")

                tblAdvertisingLevel.Rows.Add(tblHeaderRow)

                If Session("IDRole").Contains("P") Then
                    oDRPlayers = oDTPlayers.Select("ID = " & Nni(Session("IDTeam")))
                Else
                    oDRPlayers = oDTPlayers.Select("")
                End If

                ' Inizio a ciclare sui player per recuperare i valori che mi servono al popolamento della tabella
                For Each oRowP As DataRow In oDRPlayers
                    ' Etichetta della variabile
                    tblRow = New TableRow

                    ' Setto il nome del player/team
                    tblCell = New TableCell
                    tblCell.Text = oRowP("TeamName")
                    tblCell.BorderStyle = BorderStyle.None
                    tblRow.Cells.Add(tblCell)

                    ' Setto il valore della variabile
                    tblCell = New TableCell

                    ' Creazione dell'oggetto linked server per rimandare alla pagina dei grafici
                    Dim oLinkAdverQualiProce As New HyperLink
                    oLinkAdverQualiProce.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=AdverQualiProce&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                    oLinkAdverQualiProce.Style.Add("text-decoration", "none")
                    oLinkAdverQualiProce.Style.Add("cursor", "pointer")

                    Dim oDRTemp As DataRow = oDTVariableCalculateValue.Select("VariableName = 'AdverQualiProce' AND IDPlayer = " & Nni(oRowP("ID"))).First
                    oLinkAdverQualiProce.Text = Nn(oDRTemp("Value")).ToString("N2")
                    tblCell.Controls.Add(oLinkAdverQualiProce)

                    tblCell.BorderStyle = BorderStyle.None
                    tblRow.Cells.Add(tblCell)

                    tblAdvertisingLevel.Rows.Add(tblRow)
                Next
                ' Metto il valore della media
                tblRow = New TableRow
                tblCell = New TableCell
                tblCell.Text = "Media"
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                tblCell.Text = Nn(GetVariableState("AbsolAdverMarke", 0, m_SiteMaster.PeriodGetCurrent, "", Session("IDGame"))).ToString("N2")
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)
                tblAdvertisingLevel.Rows.Add(tblRow)

                ' Riga vuota
                tblRow = New TableRow
                tblCell = New TableCell
                tblCell.ColumnSpan = 4
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)
                tblAdvertisingLevel.Rows.Add(tblRow)

                SQLConnClose(g_DAL.GetConnObject)

                LoadGraphAdvertisingLevel()

                LoadAdverAllow()

                grfRicordoPubblicitario.Visible = True

            Else
                grfRicordoPubblicitario.Visible = False
                lblTitolo.Text = "Advertising and environmental" & "<br/>" & "<br/>" & "No data available for this period"
            End If

        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub LoadAdverAllow()
        Dim sSQL As String

        Dim oDTVariableCalculateValue As DataTable
        Dim oDTPlayers As DataTable = LoadTeamsGame(Session("IDGame"))
        Dim oDRPlayers As DataRow()

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Dim iPeriodo As Integer
        Dim dMedia As Double

        Try
            ' Controllo la presenza delle variabili nella tabella delle variabili del gioco
            VerifiyVariableExists("AdverAllow", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)

            ' Calcolo la media
            sSQL = "SELECT SUM(CAST(REPLACE(Value, ',', '.') AS Decimal(18, 9))) / COUNT(C.ID) AS Media " _
                 & "FROM Variables_Calculate_Define V " _
                 & "INNER JOIN Variables_Calculate_Value C ON V.Id = C.IDVariable " _
                 & "WHERE V.VariableName = 'AdverAllow' AND V.IDGame = " & Session("IDGame") & " AND C.IDPeriod = " & Session("IDPeriod")
            If Session("IDRole") = "P" Then
                sSQL &= " AND ISNULL(C.Simulation, 0) = 0 "
            End If
            dMedia = Nn(g_DAL.ExecuteScalar(sSQL))

            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header vuota
            tblHeaderCell.Text = "Advertising investments"
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.ColumnSpan = "2"
            tblHeaderRow.Cells.Add(tblHeaderCell)

            tblHeaderRow.BorderStyle = BorderStyle.None
            tblHeaderRow.Style.Add("margin-top", "10px")

            tblGreenLevel.Rows.Add(tblHeaderRow)

            iPeriodo = m_SiteMaster.PeriodGetPrev

            sSQL = "SELECT * FROM Variables_Calculate_Define V " _
                 & "INNER JOIN Variables_Calculate_Value C ON V.Id = C.IDVariable " _
                 & "WHERE C.IDPeriod = " & Session("IDPeriod") & " AND V.IDGame = " & Session("IDGame") & " AND C.IDPeriod = " & Session("IDPeriod")
            If Session("IDRole") = "P" Then
                sSQL &= " AND ISNULL(C.Simulation, 0) = 0 "
            End If
            oDTVariableCalculateValue = g_DAL.ExecuteDataTable(sSQL)

            If oDTVariableCalculateValue.Rows.Count > 0 Then
                If Session("IDRole").Contains("P") Then
                    oDRPlayers = oDTPlayers.Select("ID = " & Nni(Session("IDTeam")))
                Else
                    oDRPlayers = oDTPlayers.Select("")
                End If

                ' Inizio a ciclare sui player per recuperare i valori che mi servono al popolamento della tabella
                For Each oRowP As DataRow In oDRPlayers
                    ' Etichetta della variabile
                    tblRow = New TableRow

                    ' Setto il nome del player/team
                    tblCell = New TableCell
                    tblCell.Text = oRowP("TeamName")
                    tblCell.BorderStyle = BorderStyle.None
                    tblRow.Cells.Add(tblCell)

                    ' Setto il valore della variabile
                    tblCell = New TableCell
                    Dim oLinkAdverAllow As New HyperLink
                    oLinkAdverAllow.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=AdverAllow&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                    oLinkAdverAllow.Style.Add("text-decoration", "none")
                    oLinkAdverAllow.Style.Add("cursor", "pointer")

                    Dim oDRTemp As DataRow = oDTVariableCalculateValue.Select("VariableName = 'AdverAllow' AND IDPlayer = " & Nni(oRowP("ID"))).First
                    oLinkAdverAllow.Text = Nn(oDRTemp("Value")).ToString("N2")
                    tblCell.Controls.Add(oLinkAdverAllow)

                    tblCell.BorderStyle = BorderStyle.None
                    tblRow.Cells.Add(tblCell)

                    tblGreenLevel.Rows.Add(tblRow)
                Next
                ' Metto il valore della media
                tblRow = New TableRow
                tblCell = New TableCell
                tblCell.Text = "Media"
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                tblCell.Text = dMedia.ToString("N2")
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)
                tblGreenLevel.Rows.Add(tblRow)

                ' Riga vuota
                tblRow = New TableRow
                tblCell = New TableCell
                tblCell.ColumnSpan = 4
                'tblCell.Text = "<hr/>"
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)
                tblGreenLevel.Rows.Add(tblRow)

                SQLConnClose(g_DAL.GetConnObject)
            Else
                lblTitolo.Text = "Advertising and environmental" & "<br/>" & "<br/>" & "No data available for this period"
            End If

        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub Marketing_Player_DetailMarket_ImmagineGraph_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        m_SessionData = Session("SessionData")

        m_SiteMaster = Me.Master

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oCBOPeriod As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler m_SiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler m_SiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(m_SessionData.Utente.Games.IDGame) ' Ultimo periodo
        oCBOPeriod.SelectedValue = Session("IDPeriodValid")
        m_SiteMaster.PeriodChange = Session("IDPeriodValid")

        Session("NameTeam") = GetTeamName(Session("IDTeam"))
        oCBOTeam.SelectedValue = Session("IDTeam")
        m_SiteMaster.TeamChange = Session("IDTeam")

        ' Gestione del pannello delle decisioni concesse
        modalHelp.OpenerElementID = lnkHelp.ClientID
        modalHelp.Modal = False
        modalHelp.VisibleTitlebar = True

        LoadHelp()
    End Sub

    Private Sub LoadGraphAdvertisingLevel()
        Dim sSQL As String
        Dim oDTTableResult As DataTable
        Dim oDTTableGraph As New DataTable
        Dim dAdverQualiProce As Double
        Dim dAdverQualiProceMedia As Double
        Dim sPeriod As String

        ' Preparo il datatable che ospiterà i dati
        oDTTableGraph.Columns.Add("AdverQualiProce", GetType(Double))
        oDTTableGraph.Columns.Add("AdverQualiProceMedia", GetType(Double))
        oDTTableGraph.Columns.Add("Period", GetType(String))

        sSQL = "SELECT P.Id AS IDPeriod, P.Descrizione AS Period, C.Value, VSV.Value AS Media " _
             & "FROM Variables_Calculate_Define V " _
             & "INNER JOIN Variables_Calculate_Value C ON V.Id = C.IDVariable " _
             & "INNER JOIN BGOL_Periods P ON C.IDPeriod = P.Id " _
             & "INNER JOIN Variables_State_Value VSV ON P.ID = VSV.IDPeriod " _
             & "INNER JOIN Variables_State VS ON VSV.IDVariable = VS.ID " _
             & "WHERE VariableName = 'AdverQualiProce' AND VS.Name = 'AbsolAdverMarke' AND VS.IDGame = " & Session("IDGame") & " " _
             & "AND C.IDPlayer = " & Nni(Session("IDTeam")) & " "

        If Session("IDRole") = "P" Then
            sSQL &= " AND ISNULL(VSV.Simulation, 0) = 0 "
        End If

        sSQL &= "GROUP BY P.ID, P.NumStep, P.Descrizione, VSV.Value, C.Value " _
              & "ORDER BY P.NumStep ASC "

        oDTTableResult = g_DAL.ExecuteDataTable(sSQL)

        For Each oRow As DataRow In oDTTableResult.Rows
            dAdverQualiProce = Nn(oRow("Value"))
            dAdverQualiProceMedia = Nn(oRow("Media"))
            sPeriod = Nz(oRow("Period"))

            oDTTableGraph.Rows.Add(dAdverQualiProce.ToString("N2"), dAdverQualiProceMedia.ToString("N2"), sPeriod)
        Next

        grfRicordoPubblicitario.DataSource = oDTTableGraph
        grfRicordoPubblicitario.DataBind()

        grfRicordoPubblicitario.PlotArea.XAxis.DataLabelsField = "Period"
        grfRicordoPubblicitario.PlotArea.XAxis.LabelsAppearance.TextStyle.Bold = False
        grfRicordoPubblicitario.PlotArea.XAxis.LabelsAppearance.TextStyle.FontSize = 8.5

        ' Mostro solo le etichette raggruppate, per cui eseguo uno step ogni x period
        'grfRicordoPubblicitario.PlotArea.XAxis.LabelsAppearance.Step = HandleLoadItemsData(Session("IDGame")).Rows.Count
        'grfRicordoPubblicitario.PlotArea.XAxis.LabelsAppearance.RotationAngle = 45
        grfRicordoPubblicitario.PlotArea.XAxis.EnableBaseUnitStepAuto = True

    End Sub

    Private Sub LoadGraphGreenLevel()
        Dim sSQL As String
        Dim oDTTableResult As DataTable
        Dim oDTTableGraph As New DataTable
        Dim dGreenQualiProceProce As Double
        Dim dGreenQualiProceMedia As Double
        Dim sPeriod As String
        ' Preparo il datatable che ospiterà i dati
        oDTTableGraph.Columns.Add("GreenQualiProce", GetType(Double))
        oDTTableGraph.Columns.Add("GreenQualiProceMedia", GetType(Double))
        oDTTableGraph.Columns.Add("Period", GetType(String))

        sSQL = "SELECT P.Id AS IDPeriod, P.Descrizione AS Period, C.Value, VSV.Value AS Media " _
             & "FROM Variables_Calculate_Define V " _
             & "INNER JOIN Variables_Calculate_Value C ON V.Id = C.IDVariable " _
             & "INNER JOIN BGOL_Periods P ON C.IDPeriod = P.Id " _
             & "INNER JOIN Variables_State_Value VSV ON P.ID = VSV.IDPeriod " _
             & "INNER JOIN Variables_State VS ON VSV.IDVariable = VS.ID " _
             & "WHERE VariableName = 'GreenQualiProce' AND VS.Name = 'AbsolQualiGreen' AND VS.IDGame = " & Session("IDGame") & " " _
             & "AND C.IDPlayer = " & Nni(Session("IDTeam")) & " "

        If Session("IDRole") = "P" Then
            sSQL &= " AND ISNULL(C.Simulation, 0) = 0 "
        End If
        sSQL &= "GROUP BY P.ID, P.NumStep, P.Descrizione, VSV.Value, C.Value "
        sSQL &= "ORDER BY P.NumStep ASC "
        oDTTableResult = g_DAL.ExecuteDataTable(sSQL)

        For Each oRow As DataRow In oDTTableResult.Rows
            dGreenQualiProceProce = Nn(oRow("Value"))
            dGreenQualiProceMedia = Nn(oRow("Media"))
            sPeriod = Nz(oRow("Period"))

            oDTTableGraph.Rows.Add(dGreenQualiProceProce.ToString("N2"), dGreenQualiProceMedia.ToString("N2"), sPeriod)
        Next

        grfGreenLevel.DataSource = oDTTableGraph
        grfGreenLevel.DataBind()

        grfGreenLevel.PlotArea.XAxis.DataLabelsField = "Period"
        grfGreenLevel.PlotArea.XAxis.LabelsAppearance.TextStyle.Bold = False
        grfGreenLevel.PlotArea.XAxis.LabelsAppearance.TextStyle.FontSize = 8.5

        ' Mostro solo le etichette raggruppate, per cui eseguo uno step ogni x period
        grfGreenLevel.PlotArea.XAxis.EnableBaseUnitStepAuto = True

    End Sub

    Private Sub LoadGreenLevel()
        Dim sSQL As String

        Dim oDTVariableCalculateValue As DataTable
        Dim oDTPlayers As DataTable = LoadTeamsGame(Session("IDGame"))
        Dim oDRPlayers As DataRow()

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Dim dMedia As Double

        Try
            ' Controllo la presenza delle variabili nella tabella delle variabili del gioco
            VerifiyVariableExists("GreenQualiProce", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)

            sSQL = "SELECT * FROM Variables_Calculate_Define V " _
                 & "INNER JOIN Variables_Calculate_Value C ON V.Id = C.IDVariable " _
                 & "WHERE C.IDPeriod = " & Session("IDPeriod") & " AND V.IDGame = " & Session("IDGame")
            If Session("IDRole") = "P" Then
                sSQL &= " AND ISNULL(C.Simulation, 0) = 0 "
            End If
            oDTVariableCalculateValue = g_DAL.ExecuteDataTable(sSQL)

            If oDTVariableCalculateValue.Rows.Count > 0 Then
                ' Calcolo la media
                sSQL = "SELECT SUM(CAST(REPLACE(Value, ',', '.') AS Decimal(18, 9))) / COUNT(C.ID) AS Media " _
                 & "FROM Variables_Calculate_Define V " _
                 & "INNER JOIN Variables_Calculate_Value C ON V.Id = C.IDVariable " _
                 & "WHERE V.VariableName = 'GreenQualiProce' " & " AND V.IDGame = " & Session("IDGame") & " AND C.IDPeriod = " & Session("IDPeriod")
                If Session("IDRole") = "P" Then
                    sSQL &= " AND ISNULL(C.Simulation, 0) = 0 "
                End If
                dMedia = Nn(g_DAL.ExecuteScalar(sSQL))

                ' Costruisco la riga Header
                tblHeaderRow = New TableHeaderRow
                tblHeaderCell = New TableHeaderCell

                ' Prima cella della prima riga dell'Header vuota
                tblHeaderCell.Text = "Green image"
                tblHeaderCell.BorderStyle = BorderStyle.None
                tblHeaderCell.ColumnSpan = "2"
                tblHeaderRow.Cells.Add(tblHeaderCell)

                tblHeaderRow.BorderStyle = BorderStyle.None
                tblHeaderRow.Style.Add("margin-top", "10px")

                tblGreenQualiProce.Rows.Add(tblHeaderRow)

                If Session("IDRole").Contains("P") Then
                    oDRPlayers = oDTPlayers.Select("ID = " & Nni(Session("IDTeam")))
                Else
                    oDRPlayers = oDTPlayers.Select("")
                End If

                ' Inizio a ciclare sui player per recuperare i valori che mi servono al popolamento della tabella
                For Each oRowP As DataRow In oDRPlayers
                    ' Etichetta della variabile
                    tblRow = New TableRow

                    ' Setto il nome del player/team
                    tblCell = New TableCell
                    tblCell.Text = oRowP("TeamName")
                    tblCell.BorderStyle = BorderStyle.None
                    tblRow.Cells.Add(tblCell)

                    ' Setto il valore della variabile
                    tblCell = New TableCell

                    ' Creazione dell'oggetto linked server per rimandare alla pagina dei grafici
                    Dim oLinkGreenQualiProce As New HyperLink
                    oLinkGreenQualiProce.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=GreenQualiProce&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                    oLinkGreenQualiProce.Style.Add("text-decoration", "none")
                    oLinkGreenQualiProce.Style.Add("cursor", "pointer")

                    Dim oDRTemp As DataRow = oDTVariableCalculateValue.Select("VariableName = 'GreenQualiProce' AND IDPlayer = " & Nni(oRowP("ID"))).First
                    oLinkGreenQualiProce.Text = Nn(oDRTemp("Value")).ToString("N2")
                    tblCell.Controls.Add(oLinkGreenQualiProce)
                    tblCell.BorderStyle = BorderStyle.None
                    tblRow.Cells.Add(tblCell)

                    tblGreenQualiProce.Rows.Add(tblRow)
                Next

                ' Metto il valore della media
                tblRow = New TableRow
                tblCell = New TableCell
                tblCell.Text = "Media"
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                tblCell.Text = Nn(GetVariableState("AbsolQualiGreen", 0, m_SiteMaster.PeriodGetCurrent, "", Session("IDGame"))).ToString("N2")
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)
                tblGreenQualiProce.Rows.Add(tblRow)

                ' Riga vuota
                tblRow = New TableRow
                tblCell = New TableCell
                tblCell.ColumnSpan = 4
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)
                tblGreenQualiProce.Rows.Add(tblRow)

                SQLConnClose(g_DAL.GetConnObject)

                LoadGraphGreenLevel()

                LoadGreenAllow()

                grfGreenLevel.Visible = True

            Else
                grfGreenLevel.Visible = False
                lblTitolo.Text = "Advertising and environmental" & "<br/>" & "<br/>" & "No data available for this period"
            End If

        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub LoadGreenAllow()
        Dim sSQL As String

        Dim oDTVariableCalculateValue As DataTable
        Dim oDTPlayers As DataTable = LoadTeamsGame(Session("IDGame"))
        Dim oDRPlayers As DataRow()

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Dim iPeriodo As Integer
        Dim dMedia As Double

        Try
            ' Controllo la presenza delle variabili nella tabella delle variabili del gioco
            VerifiyVariableExists("GreenAllow", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)

            ' Calcolo la media
            sSQL = "SELECT SUM(CAST(REPLACE(Value, ',', '.') AS Decimal(18, 9))) / COUNT(C.ID) AS Media " _
                 & "FROM Variables_Calculate_Define V " _
                 & "INNER JOIN Variables_Calculate_Value C ON V.Id = C.IDVariable " _
                 & "WHERE V.VariableName = 'GreenAllow' AND V.IDGame = " & Session("IDGame") & " AND C.IDPeriod = " & Session("IDPeriod")
            If Session("IDRole") = "P" Then
                sSQL &= " AND ISNULL(C.Simulation, 0) = 0 "
            End If
            dMedia = Nn(g_DAL.ExecuteScalar(sSQL))

            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header vuota
            tblHeaderCell.Text = "Green investments"
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.ColumnSpan = "2"
            tblHeaderRow.Cells.Add(tblHeaderCell)

            tblHeaderRow.BorderStyle = BorderStyle.None
            tblHeaderRow.Style.Add("margin-top", "10px")

            tblGreenAllow.Rows.Add(tblHeaderRow)

            iPeriodo = m_SiteMaster.PeriodGetPrev

            sSQL = "SELECT * FROM Variables_Calculate_Define V " _
                 & "INNER JOIN Variables_Calculate_Value C ON V.Id = C.IDVariable " _
                 & "WHERE C.IDPeriod = " & Session("IDPeriod")
            If Session("IDRole") = "P" Then
                sSQL &= " AND ISNULL(C.Simulation, 0) = 0 "
            End If
            oDTVariableCalculateValue = g_DAL.ExecuteDataTable(sSQL)

            If oDTVariableCalculateValue.Rows.Count > 0 Then
                If Session("IDRole").Contains("P") Then
                    oDRPlayers = oDTPlayers.Select("ID = " & Nni(Session("IDTeam")))
                Else
                    oDRPlayers = oDTPlayers.Select("")
                End If

                ' Inizio a ciclare sui player per recuperare i valori che mi servono al popolamento della tabella
                For Each oRowP As DataRow In oDRPlayers
                    ' Etichetta della variabile
                    tblRow = New TableRow

                    ' Setto il nome del player/team
                    tblCell = New TableCell
                    tblCell.Text = oRowP("TeamName")
                    tblCell.BorderStyle = BorderStyle.None
                    tblRow.Cells.Add(tblCell)

                    ' Setto il valore della variabile
                    tblCell = New TableCell

                    ' Creazione dell'oggetto linked server per rimandare alla pagina dei grafici
                    Dim oLinkGreenAllow As New HyperLink
                    oLinkGreenAllow.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=GreenAllow&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                    oLinkGreenAllow.Style.Add("text-decoration", "none")
                    oLinkGreenAllow.Style.Add("cursor", "pointer")

                    Dim oDRTemp As DataRow = oDTVariableCalculateValue.Select("VariableName = 'GreenAllow' AND IDPlayer = " & Nni(oRowP("ID"))).First
                    oLinkGreenAllow.Text = Nn(oDRTemp("Value")).ToString("N2")
                    tblCell.Controls.Add(oLinkGreenAllow)

                    tblCell.BorderStyle = BorderStyle.None
                    tblRow.Cells.Add(tblCell)

                    tblGreenAllow.Rows.Add(tblRow)
                Next
                ' Metto il valore della media
                tblRow = New TableRow
                tblCell = New TableCell
                tblCell.Text = "Media"
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                tblCell.Text = dMedia.ToString("N2")
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)
                tblGreenAllow.Rows.Add(tblRow)

                ' Riga vuota
                tblRow = New TableRow
                tblCell = New TableCell
                tblCell.ColumnSpan = 4
                'tblCell.Text = "<hr/>"
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)
                tblGreenAllow.Rows.Add(tblRow)

                SQLConnClose(g_DAL.GetConnObject)

            Else
                lblTitolo.Text = "Advertising and environmental" & "<br/>" & "<br/>" & "No data available for this period"
            End If

        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub Marketing_Player_DetailMarket_ImmagineGraph_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        If Not Page.IsPostBack Then
            If Session("IDRole") = "P" Then
                m_SiteMaster.ReloadPeriodsCalculate()
            End If
            LoadDataPage()

            btnTranslate.Visible = Session("IDRole").Contains("D")
        End If
        ' Controllo tutti gli oggetti presenti
        Dim oTranslation As New TranslationObjectForm(Me, g_DAL, Nz(Session("LanguageActive")))

    End Sub

    Private Sub btnTranslate_Click(sender As Object, e As EventArgs) Handles btnTranslate.Click
        Session("PageOrigin") = Me.ToString().Substring(4, Me.ToString().Substring(4).Length - 5) + ".aspx"
        Response.Redirect("PageTranslation.aspx")
        Response.End()
    End Sub

#Region "PANNELLO HELP"

    Private Sub LoadHelp()
        lblHelp.Text = "<p><strong>Advertising &nbsp;level </strong><br>
                        <strong>This is the awareness level in the market of  your company based on your previous and current period advertising  expenditures.&nbsp; It is a value comparing  your company performance with your competitors.</strong><br>
                        <strong>Because it is relative to investments made by  your competitors and increases &nbsp;your  company image, you have to invest more than your competitors. Poor investments  at the beginning may lead to a need for higher efforts to &nbsp;recover a poor market perception. </strong><strong>The standard market level shows the base from where each team is perceived by customers. The perception is calculated on the base of the variance of previous and present investments which are monitored and weighted by the market actors.</strong></p>
                        <p>&nbsp;</p>
                        <p><strong>Advertising &nbsp;investment</strong><br>
                        <strong>It compares your company advertising investment  to the average of your competitors.</strong></p>
                        <p><strong>Environmental &nbsp;image </strong><br>
                        <strong>This is the perceived level in the market of  your previous environmental &nbsp;investments  and current period investments. &nbsp;&nbsp;It is a  value comparing your company level to that of your competitors. Because it is  relative to investments made by your competitors and increases &nbsp;your company image, you have to invest more  than your competitors. Poor &nbsp;investments  at the beginning may lead to a need for higher efforts to &nbsp;recover a poor market perception. </strong><strong>The standard market level shows the base from where each team is perceived by customers. The perception is calculated on the base of the variance of previous and present investments which are monitored and weighted by the market actors.</strong></p>
                        <p><strong>.</strong></p>
                        <p><strong>Environmental investment</strong><br>
                        <strong>It compares your company environmental  investments to the average of your competitors.</strong> </p>"
    End Sub

#End Region

End Class
