﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Marketing_Player_Positioning.aspx.vb" Inherits="Marketing_Player_Positioning" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="cntMain" ContentPlaceHolderID="MainContent" runat="Server" ClientIDMode="Static">

    <asp:UpdatePanel ID="pnlMain" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <h2>
                <asp:Label ID="lblWelcome" runat="server" Text="Marketing"></asp:Label>
            </h2>

            <div class="clr"></div>

            <div class="divTable">
                <div class="divTableRow">
                    <div class="divTableCell">
                        <h3>
                            <asp:Label ID="lblTitolo" runat="server" Text="Positioning"></asp:Label>
                        </h3>
                    </div>
                    <div class="divTableCell" style="text-align: right; font-size: 8pt">
                        <telerik:RadLinkButton ID="lnkHelp" runat="server" Text="Help" EnableAjaxSkinRendering="true" ToolTip="Show help"></telerik:RadLinkButton>
                    </div>
                </div>
            </div>

            <div id="divCompetitivenessLevel" class="row" runat="server">
                <table>
                    <tr>
                        <td style="width: 80%;">
                            <telerik:RadHtmlChart ID="grfComeptitivenessLevel" runat="server" Width="100%">
                                <ChartTitle Text="Competitiveness level"></ChartTitle>

                            </telerik:RadHtmlChart>
                        </td>

                        <td style="width: 20%;">
                            <asp:Table ClientIDMode="Static" ID="tblCompetitivenessLevel" CssClass="tableGraph" runat="server">
                            </asp:Table>
                        </td>
                    </tr>

                </table>
            </div>

            <div class="clr"></div>

            <div id="divAttractivenessLevel" class="row" runat="server">
                <table>
                    <tr>
                        <td style="width: 80%;">
                            <telerik:RadHtmlChart ID="grfAttractivenessLevel" runat="server" Width="100%">
                                <ChartTitle Text="Attractiveness level"></ChartTitle>

                                <PlotArea>
                                    <Series>
                                        <telerik:ColumnSeries Name="Value" DataFieldY="IndexOfAttraction">
                                            <TooltipsAppearance DataFormatString="{0:2N}" BackgroundColor="White" Color="Blue" />
                                            <LabelsAppearance Visible="false" Position="Center" RotationAngle="-90" Color="#FFE038" />
                                            <Appearance FillStyle-BackgroundColor="DarkOrange" />
                                        </telerik:ColumnSeries>
                                    </Series>

                                    <XAxis>
                                    </XAxis>

                                    <YAxis>
                                        <LabelsAppearance DataFormatString="{0}" />
                                    </YAxis>

                                </PlotArea>

                                <Legend>
                                    <Appearance Visible="true" Position="Bottom" />
                                </Legend>
                            </telerik:RadHtmlChart>
                        </td>

                        <td style="width: 20%;">
                            <asp:Table ClientIDMode="Static" ID="tblAttractivenessLevel" CssClass="tableGraph" runat="server">
                            </asp:Table>
                        </td>
                    </tr>

                </table>
            </div>

            <div class="row">
                <asp:PlaceHolder runat="server" ID="Message" Visible="false">
                    <p class="text-danger">
                        <asp:Literal runat="server" ID="MessageText" />
                    </p>
                </asp:PlaceHolder>
            </div>

            <div class="clr"></div>

            <div class="row">
                <div class="divTableCell" style="text-align: right;">
                    <telerik:RadButton ID="btnTranslate" runat="server" Text="Translate" EnableAjaxSkinRendering="true" ToolTip=""></telerik:RadButton>
                </div>
            </div>

            <telerik:RadWindow RenderMode="Lightweight" ID="modalHelp" runat="server" Width="520px" Height="450px" CenterIfModal="false"
                Style="z-index: 100001;" BorderStyle="None" Behaviors="Close, Move" Title="Help">
                <ContentTemplate>
                    <div style="padding: 10px; text-align: left;">
                        <div id="divTableMaster" runat="server" style="margin-bottom: 10px;">
                            <asp:Label runat="server" ID="lblHelp" Visible="true">

                            </asp:Label>
                        </div>
                    </div>
                </ContentTemplate>
            </telerik:RadWindow>

        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
