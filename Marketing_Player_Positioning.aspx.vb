﻿Imports System.Data
Imports APS.APSUtility
Imports APPCore.Utility
Imports Telerik.Web.UI

Partial Class Marketing_Player_Positioning
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadDataPage()

        Message.Visible = False
        pnlMain.Update()
    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadDataPage()

        Message.Visible = False
        pnlMain.Update()
    End Sub

    Private Sub LoadDataPage()
        tblCompetitivenessLevel.Rows.Clear()

        LoadDataGraphCompetitivenessLevel()
        LoadDataGraphAttrativenessLevel()
    End Sub

    Private Sub Marketing_Player_Positioning_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        m_SessionData = Session("SessionData")

        m_SiteMaster = Me.Master

        m_SiteMaster.LoadPeriodsValid = Session("IDRole") = "P"

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oCBOPeriod As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler m_SiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler m_SiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(m_SessionData.Utente.Games.IDGame) ' Ultimo periodo
        oCBOPeriod.SelectedValue = Session("IDPeriodValid")
        m_SiteMaster.PeriodChange = Session("IDPeriodValid")

        Session("NameTeam") = GetTeamName(Session("IDTeam"))
        oCBOTeam.SelectedValue = Session("IDTeam")
        m_SiteMaster.TeamChange = Session("IDTeam")

        ' Gestione del pannello delle decisioni concesse
        modalHelp.OpenerElementID = lnkHelp.ClientID
        modalHelp.Modal = False
        modalHelp.VisibleTitlebar = True

        LoadHelp()
    End Sub

    Private Sub LoadDataGraphCompetitivenessLevel()
        Dim sSQL As String
        Dim oDTTableResult As DataTable
        Dim oDTItems As DataTable = HandleGetItemsPeriodVariables(Session("IDGame"), Nz(Session("LanguageActive")))
        Dim sItemName As String = ""
        Dim sIDPeriod As String = ""

        grfComeptitivenessLevel.PlotArea.Series.Clear()
        grfComeptitivenessLevel.PlotArea.XAxis.Items.Clear()

        sSQL = "SELECT P.Descrizione AS Period, C.Value, V.VariableName, I.VariableLabel, I.IDVariable, C.IDItem, C.IDPeriod, I.Translation, I.IDLanguage " _
             & "FROM Variables_Calculate_Define V " _
             & "INNER JOIN Variables_Calculate_Value C ON V.Id = C.IDVariable " _
             & "INNER JOIN BGOL_Periods P ON C.IDPeriod = P.Id " _
             & "INNER JOIN vItems I ON C.IDItem = I.IDVariable " _
             & "WHERE LTRIM(RTRIM(V.VariableName)) = 'IndexOfCompetition' AND C.IDPlayer = " & Nni(Session("IDTeam")) & " AND C.IDPeriod <= " & m_SiteMaster.PeriodGetCurrent
        If Session("IDRole").Contains("P") Then
            sSQL &= " AND ISNULL(C.Simulation, 0) = 0 "
        End If
        oDTTableResult = g_DAL.ExecuteDataTable(sSQL)

        ' Recupero tutti i periodi generati fino adesso
        sSQL = "SELECT * FROM BGOL_Games_Periods BGP " _
             & "INNER JOIN BGOL_Periods BP ON BGP.IDPeriodo = BP.Id " _
             & "WHERE BGP.IDGame = " & Session("IDGame") & " AND BP.NumStep <= " & Session("CurrentStep")
        If Session("IDRole").Contains("P") Then
            sSQL &= " AND ISNULL(BGP.Simulation, 0) = 0 "
        End If
        sSQL &= " ORDER BY BP.NumStep "
        Dim oDTPeriodi As DataTable = g_DAL.ExecuteDataTable(sSQL)

        Dim oSeriesData As LineSeries
        For Each oRowItm As DataRow In oDTItems.Rows
            oSeriesData = New LineSeries
            For Each oRowPeriod As DataRow In oDTPeriodi.Rows
                sItemName = oRowItm("VariableName").ToString

                Dim oDRValue As DataRow

                oDRValue = oDTTableResult.Select("IDperiod = " & oRowPeriod("IdPeriodo") & " AND IDItem = " & oRowItm("IdVariable")).FirstOrDefault

                If Not IsNothing(oDRValue) Then
                    Dim oItem As New SeriesItem
                    oItem.YValue = Nn(oDRValue("Value")).ToString("N2")
                    oItem.TooltipValue = Nz(oRowItm("VariableName"))
                    oSeriesData.Items.Add(oItem)
                    sIDPeriod &= Nz(oRowPeriod("IDPeriodo")) & ";"
                End If
            Next

            If sIDPeriod.EndsWith(";") Then sIDPeriod.TrimEnd(";")

            grfComeptitivenessLevel.PlotArea.Series.Add(oSeriesData)
            oSeriesData.Name = sItemName
            oSeriesData.VisibleInLegend = True
            oSeriesData.LabelsAppearance.Visible = True
        Next

        For Each oRowPeriod As DataRow In oDTPeriodi.Rows
            If sIDPeriod.Contains(oRowPeriod("IdPeriodo")) Then
                Dim newAxisItem As New AxisItem(Nz(oRowPeriod("Descrizione")))
                grfComeptitivenessLevel.PlotArea.XAxis.Items.Add(newAxisItem)
            End If
        Next

        grfComeptitivenessLevel.PlotArea.XAxis.MinorGridLines.Visible = False
        grfComeptitivenessLevel.PlotArea.YAxis.MinorGridLines.Visible = False
        grfComeptitivenessLevel.Legend.Appearance.Visible = True
        grfComeptitivenessLevel.Legend.Appearance.Position = HtmlChart.ChartLegendPosition.Top

        LoadDataCompetitivenessLevel()
    End Sub

    Private Sub LoadDataCompetitivenessLevel()
        Dim sSQL As String
        Dim oDTTableResult As DataTable

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Try
            ' Controllo la presenza delle variabili nella tabella delle variabili del gioco
            VerifiyVariableExists("IndexMarkeOffer", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)

            sSQL = "SELECT P.Descrizione AS Period, C.Value, V.VariableName, I.VariableLabel, I.IDVariable, C.IDItem, C.IDPeriod " _
             & "FROM Variables_Calculate_Define V " _
             & "INNER JOIN Variables_Calculate_Value C ON V.Id = C.IDVariable " _
             & "INNER JOIN BGOL_Periods P ON C.IDPeriod = P.Id " _
             & "INNER JOIN vItems I ON C.IDItem = I.IDVariable " _
             & "WHERE LTRIM(RTRIM(V.VariableName)) = 'IndexOfCompetition' AND C.IDPlayer = " & Nni(Session("IDTeam")) & " " _
             & "AND C.IDPeriod = " & Session("IDPeriod")
            oDTTableResult = g_DAL.ExecuteDataTable(sSQL)

            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header vuota
            tblHeaderCell.Text = "Index of Competitiveness"
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.ColumnSpan = "2"
            tblHeaderRow.Cells.Add(tblHeaderCell)

            tblHeaderRow.BorderStyle = BorderStyle.None
            tblHeaderRow.Style.Add("margin-top", "10px")

            tblCompetitivenessLevel.Rows.Add(tblHeaderRow)

            For Each oRow As DataRow In oDTTableResult.Rows
                tblRow = New TableRow
                tblCell = New TableCell
                tblCell.Text = Nz(oRow("VariableLabel"))
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell

                ' Creazione dell'oggetto linked server per rimandare alla pagina dei grafici
                Dim oLinkProduRevenMarke As New HyperLink
                oLinkProduRevenMarke.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=IndexOfCompetition&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                oLinkProduRevenMarke.Style.Add("text-decoration", "none")
                oLinkProduRevenMarke.Style.Add("cursor", "pointer")

                oLinkProduRevenMarke.Text = Nn(oRow("Value")).ToString("N2")
                tblCell.BorderStyle = BorderStyle.None
                tblCell.Controls.Add(oLinkProduRevenMarke)
                tblRow.Cells.Add(tblCell)
                tblCompetitivenessLevel.Rows.Add(tblRow)

            Next

            SQLConnClose(g_DAL.GetConnObject)
        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub LoadDataGraphAttrativenessLevel()
        Dim sSQL As String
        Dim oDTTableResult As DataTable
        Dim oDTTableGraph As New DataTable
        Dim dIndexOfAttraction As Double
        Dim sPeriod As String

        ' Preparo il datatable che ospiterà i dati
        oDTTableGraph.Columns.Add("IndexOfAttraction", GetType(Double))
        oDTTableGraph.Columns.Add("Period", GetType(String))

        sSQL = "SELECT C.Value, P.Descrizione AS Period " _
             & "FROM Variables_Calculate_Define V " _
             & "INNER JOIN Variables_Calculate_Value C ON V.Id = C.IDVariable " _
             & "INNER JOIN BGOL_Periods P ON C.IDPeriod = P.Id " _
             & "WHERE VariableName = 'IndexOfAttraction' AND C.IDPlayer = " & Nni(Session("IDTeam")) & " AND P.IDGame = " & Session("IDGame") _
             & " AND C.IDPeriod <= " & m_SiteMaster.PeriodGetCurrent
        If Session("IDRole").Contains("P") Then
            sSQL &= " AND ISNULL(C.Simulation, 0) = 0 "
        End If
        oDTTableResult = g_DAL.ExecuteDataTable(sSQL)

        For Each oRow As DataRow In oDTTableResult.Rows
            dIndexOfAttraction = Nn(oRow("Value"))
            sPeriod = Nz(oRow("Period"))

            oDTTableGraph.Rows.Add(dIndexOfAttraction, sPeriod)
        Next

        grfAttractivenessLevel.DataSource = oDTTableGraph
        grfAttractivenessLevel.DataBind()

        grfAttractivenessLevel.PlotArea.XAxis.DataLabelsField = "Period"
        grfAttractivenessLevel.PlotArea.XAxis.LabelsAppearance.TextStyle.Bold = False
        grfAttractivenessLevel.PlotArea.XAxis.LabelsAppearance.TextStyle.FontSize = 9
        grfAttractivenessLevel.PlotArea.XAxis.EnableBaseUnitStepAuto = True

        LoadDataAttrativenessLevel()
    End Sub

    Private Sub LoadDataAttrativenessLevel()
        Dim sSQL As String
        Dim oDTTableResult As DataTable

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Try
            ' Controllo la presenza delle variabili nella tabella delle variabili del gioco
            VerifiyVariableExists("IndexOfAttraction", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)

            sSQL = "SELECT P.Descrizione AS Period, C.Value, V.VariableName, C.IDPeriod " _
             & "FROM Variables_Calculate_Define V " _
             & "INNER JOIN Variables_Calculate_Value C ON V.Id = C.IDVariable " _
             & "INNER JOIN BGOL_Periods P ON C.IDPeriod = P.Id " _
             & "WHERE LTRIM(RTRIM(V.VariableName)) = 'IndexOfAttraction' AND C.IDPlayer = " & Nni(Session("IDTeam")) & " " _
             & "AND C.IDPeriod = " & Session("IDPeriod")
            If Session("IDRole").Contains("P") Then
                sSQL &= " AND ISNULL(C.Simulation, 0) = 0 "
            End If
            oDTTableResult = g_DAL.ExecuteDataTable(sSQL)

            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header vuota
            tblHeaderCell.Text = "Index of attraction"
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.ColumnSpan = "2"
            tblHeaderRow.Cells.Add(tblHeaderCell)

            tblHeaderRow.BorderStyle = BorderStyle.None
            tblHeaderRow.Style.Add("margin-top", "10px")

            tblAttractivenessLevel.Rows.Add(tblHeaderRow)

            For Each oRow As DataRow In oDTTableResult.Rows
                tblRow = New TableRow
                tblCell = New TableCell
                tblCell.Text = "Index"
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell

                ' Creazione dell'oggetto linked server per rimandare alla pagina dei grafici
                Dim oLinkIndexOfAttraction As New HyperLink
                oLinkIndexOfAttraction.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=IndexOfAttraction&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                oLinkIndexOfAttraction.Style.Add("text-decoration", "none")
                oLinkIndexOfAttraction.Style.Add("cursor", "pointer")
                oLinkIndexOfAttraction.Text = Nn(oRow("Value")).ToString("N2")
                tblCell.Controls.Add(oLinkIndexOfAttraction)

                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)
                tblAttractivenessLevel.Rows.Add(tblRow)

            Next

            SQLConnClose(g_DAL.GetConnObject)
        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub Marketing_Player_Positioning_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        If Not Page.IsPostBack Then
            If Session("IDRole") = "P" Then
                m_SiteMaster.ReloadPeriodsCalculate()
            End If
            LoadDataPage()

            btnTranslate.Visible = Session("IDRole").Contains("D")

        End If
        ' Controllo tutti gli oggetti presenti
        Dim oTranslation As New TranslationObjectForm(Me, g_DAL, Nz(Session("LanguageActive")))

    End Sub

    Private Sub btnTranslate_Click(sender As Object, e As EventArgs) Handles btnTranslate.Click
        Session("PageOrigin") = Me.ToString().Substring(4, Me.ToString().Substring(4).Length - 5) + ".aspx"
        Response.Redirect("PageTranslation.aspx")
        Response.End()
    End Sub

#Region "PANNELLO HELP"

    Private Sub LoadHelp()
        lblHelp.Text = "<p><strong>Index of Competitiveness</strong><br>
                        <strong>The competitive index is based on the perceived  price (price + promotions), &nbsp;the quality  of the products offered to the market and your attractiveness.</strong></p>
                        <p><strong>Index of Attraction</strong><br>
                        <strong>Attractiveness is a reflection of your capacity  to attract customers to your outlets through an effective site location,  
                        advertising, brand image, &nbsp;customer  service level and environmental sensitivities.</strong></p>"
    End Sub

#End Region


End Class
