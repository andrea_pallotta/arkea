﻿Imports System.Data
Imports APS.APSUtility
Imports APPCore.Utility
Imports DALC4NET
Imports Telerik.Web.UI

Partial Class Marketing_Player_Wholesalers
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster
    Private m_IDPeriodoMax As Integer

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        Message.Visible = False
        LoadData()
        UpdatePanelMain.Update()
    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        Message.Visible = False
        LoadData()
        UpdatePanelMain.Update()
    End Sub

    Private Sub Distribution_Player_Wholesalers_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        m_SessionData = Session("SessionData")

        m_SiteMaster = Me.Master

        m_SiteMaster.LoadPeriodsValid = Session("IDRole") = "P"

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oCBOPeriod As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOPeriodValid As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriodValid"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler m_SiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler m_SiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(Session("IDGame")) ' Ultimo periodo
        oCBOPeriod.SelectedValue = Session("IDPeriodValid")
        m_SiteMaster.PeriodChange = Session("IDPeriodValid")

        Session("NameTeam") = GetTeamName(Session("IDTeam"))
        oCBOTeam.SelectedValue = Session("IDTeam")
        m_SiteMaster.TeamChange = Session("IDTeam")

        ' Gestione del pannello delle decisioni concesse
        modalHelp.OpenerElementID = lnkHelp.ClientID
        modalHelp.Modal = False
        modalHelp.VisibleTitlebar = True

        LoadHelp()
    End Sub

    Private Sub LoadData()
        tblWholesalersMarket.Rows.Clear()
        tblWholesalersTeam.Rows.Clear()

        LoadWholesalersMarket()
        LoadWholesalersTeam()
    End Sub

    Private Sub LoadWholesalersMarket()
        Dim oDTItems As DataTable = HandleGetItemsPeriodVariables(Session("IDGame"), Nz(Session("LanguageActive")))

        Dim iTotNumPlayers As Integer = HandleLoadPlayersGame(Session("IDGame")).Rows.Count

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Dim sNomeVariabileLista As String

        Try
            VerifiyVariableExists("LowesQualiAccep", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)
            VerifiyVariableExists("HighePriceAccep", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)
            VerifiyVariableExists("QuantRequiInter", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)
            VerifiyVariableExists("GlobaInterSales", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)

            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header vuota
            tblHeaderCell.Text = ""
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderRow.Cells.Add(tblHeaderCell)

            ' Recupero gli Items per la costruzione delle colonne
            For Each oRowItm As DataRow In oDTItems.Rows
                ' Cella del prezzo di vendita
                tblHeaderCell = New TableHeaderCell
                tblHeaderCell.Text = Nz(oRowItm("VariableName"))
                tblHeaderCell.BorderStyle = BorderStyle.None
                tblHeaderRow.Cells.Add(tblHeaderCell)
            Next

            tblHeaderRow.BorderStyle = BorderStyle.None
            tblHeaderRow.Style.Add("margin-top", "10px")

            tblWholesalersMarket.Rows.Add(tblHeaderRow)

            ' Aggiungo le righe di dettaglio
            ' Riga Minumum quality
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblMinimumQuality As New RadLabel
            lblMinimumQuality.ID = "lblMinimumQuality"
            lblMinimumQuality.Text = "Minimum quality"
            lblMinimumQuality.Style.Add("color", "darkred")
            tblCell.Controls.Add(lblMinimumQuality)
            tblCell.Style.Add("width", "40%")
            tblRow.Cells.Add(tblCell)

            For Each oRowItm As DataRow In oDTItems.Rows
                sNomeVariabileLista = "LowesQualiAccep_" & oRowItm("VariableNameDefault")
                tblCell = New TableCell
                ' Creazione dell'oggetto linked server per rimandare alla pagina dei grafici
                Dim oLinkLowesQualiAccep As New HyperLink
                oLinkLowesQualiAccep.ID = sNomeVariabileLista
                oLinkLowesQualiAccep.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=LowesQualiAccep&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                oLinkLowesQualiAccep.Style.Add("text-decoration", "none")
                oLinkLowesQualiAccep.Style.Add("cursor", "pointer")

                oLinkLowesQualiAccep.Text = Nn(GetVariableBoss("LowesQualiAccep", 0, Session("IDPeriod"), sNomeVariabileLista, Session("IDGame"))).ToString("N1") & " %"
                tblCell.Controls.Add(oLinkLowesQualiAccep)
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)
            Next
            tblWholesalersMarket.Rows.Add(tblRow)

            ' Riga Maximum price
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblMaximumPrice As New RadLabel
            lblMaximumPrice.ID = "lblMaximumPrice"
            lblMaximumPrice.Text = "Maximum price"
            lblMaximumPrice.Style.Add("color", "darkred")
            tblCell.Controls.Add(lblMaximumPrice)
            tblCell.Style.Add("width", "20%")
            tblRow.Cells.Add(tblCell)

            For Each oRowItm As DataRow In oDTItems.Rows
                sNomeVariabileLista = "HighePriceAccep_" & oRowItm("VariableNameDefault")
                tblCell = New TableCell
                ' Creazione dell'oggetto linked server per rimandare alla pagina dei grafici
                Dim oLinkHighePriceAccep As New HyperLink
                oLinkHighePriceAccep.ID = sNomeVariabileLista
                oLinkHighePriceAccep.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=HighePriceAccep&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                oLinkHighePriceAccep.Style.Add("text-decoration", "none")
                oLinkHighePriceAccep.Style.Add("cursor", "pointer")
                oLinkHighePriceAccep.Text = Nn(GetVariableBoss("HighePriceAccep", 0, Session("IDPeriod"), sNomeVariabileLista, Session("IDGame"))).ToString("C0")
                tblCell.Controls.Add(oLinkHighePriceAccep)
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)
            Next
            tblWholesalersMarket.Rows.Add(tblRow)

            ' Riga Requested quantity
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblRequestedQuantity As New RadLabel
            lblRequestedQuantity.ID = "lblRequestedQuantity"
            lblRequestedQuantity.Text = "Requested quantity"
            lblRequestedQuantity.Style.Add("color", "darkred")
            tblCell.Controls.Add(lblRequestedQuantity)
            tblCell.Style.Add("width", "20%")
            tblRow.Cells.Add(tblCell)

            For Each oRowItm As DataRow In oDTItems.Rows
                sNomeVariabileLista = "QuantRequiInter_" & oRowItm("VariableNameDefault")
                tblCell = New TableCell
                Dim oLinkQuantRequiInter As New HyperLink
                oLinkQuantRequiInter.ID = sNomeVariabileLista
                oLinkQuantRequiInter.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=QuantRequiInter&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                oLinkQuantRequiInter.Style.Add("text-decoration", "none")
                oLinkQuantRequiInter.Style.Add("cursor", "pointer")
                oLinkQuantRequiInter.Text = Nn(GetVariableBoss("QuantRequiInter", 0, Session("IDPeriod"), sNomeVariabileLista, Session("IDGame"))).ToString("N0")
                tblCell.Controls.Add(oLinkQuantRequiInter)
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)
            Next
            tblWholesalersMarket.Rows.Add(tblRow)

            ' Riga Gobal intersales
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblGlobalInterSales As New RadLabel
            lblGlobalInterSales.ID = "lblGlobalInterSales"
            lblGlobalInterSales.Text = "Market sales"
            lblGlobalInterSales.Style.Add("color", "darkred")
            tblCell.Controls.Add(lblGlobalInterSales)
            tblCell.Style.Add("width", "20%")
            tblRow.Cells.Add(tblCell)

            For Each oRowItm As DataRow In oDTItems.Rows
                tblCell = New TableCell
                Dim oLinkGlobaInterSales As New HyperLink

                oLinkGlobaInterSales.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=GlobaInterSales&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                oLinkGlobaInterSales.Style.Add("text-decoration", "none")
                oLinkGlobaInterSales.Style.Add("cursor", "pointer")
                oLinkGlobaInterSales.Text = Nn(GetVariableDefineValue("GlobaInterSales", 0, oRowItm("IDVariable"), 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N0")
                tblCell.Controls.Add(oLinkGlobaInterSales)

                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)
            Next
            tblWholesalersMarket.Rows.Add(tblRow)

        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub LoadWholesalersTeam()
        Dim oDTItems As DataTable = HandleGetItemsPeriodVariables(Session("IDGame"), Nz(Session("LanguageActive")))

        Dim iTotNumPlayers As Integer = HandleLoadPlayersGame(Session("IDGame")).Rows.Count

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Dim sNomeVariabileLista As String

        Try
            VerifiyVariableExists("InterMarkeRequi", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)
            VerifiyVariableExists("SalesInterMarke", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)
            VerifiyVariableExists("ProduRevenInter", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)
            VerifiyVariableExists("GlobaInterSales", Session("IDGame"), 16, "", 0, 0, False, False, False, 0)

            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header vuota
            tblHeaderCell.Text = m_SiteMaster.TeamNameGet
            tblHeaderCell.ForeColor = Drawing.Color.White
            tblHeaderCell.Style.Add("background", "#525252")
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderRow.Cells.Add(tblHeaderCell)

            ' Recupero gli Items per la costruzione delle colonne
            For Each oRowItm As DataRow In oDTItems.Rows
                ' Cella del prezzo di vendita
                tblHeaderCell = New TableHeaderCell
                tblHeaderCell.Text = Nz(oRowItm("VariableName"))
                tblHeaderCell.BorderStyle = BorderStyle.None
                tblHeaderRow.Cells.Add(tblHeaderCell)
            Next

            tblHeaderRow.BorderStyle = BorderStyle.None
            tblHeaderRow.Style.Add("margin-top", "10px")

            tblWholesalersTeam.Rows.Add(tblHeaderRow)

            ' Aggiungo le righe di dettaglio
            ' Riga Team offer
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblOffer As New RadLabel
            lblOffer.ID = "lblOffer"
            lblOffer.Text = "Offer (units)"
            lblOffer.Style.Add("color", "darkred")
            tblCell.Controls.Add(lblOffer)
            tblCell.Style.Add("width", "40%")
            tblRow.Cells.Add(tblCell)

            For Each oRowItm As DataRow In oDTItems.Rows
                sNomeVariabileLista = "var_" & m_SiteMaster.TeamNameGet & "_InterMarkeRequi_" & oRowItm("VariableNameDefault")
                tblCell = New TableCell

                ' Creazione dell'oggetto linked server per rimandare alla pagina dei grafici
                Dim oLinkInterMarkeRequi As New HyperLink
                oLinkInterMarkeRequi.ID = sNomeVariabileLista
                oLinkInterMarkeRequi.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=InterMarkeRequi&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                oLinkInterMarkeRequi.Style.Add("text-decoration", "none")
                oLinkInterMarkeRequi.Style.Add("cursor", "pointer")

                oLinkInterMarkeRequi.Text = Nn(GetDecisionPlayers("InterMarkeRequi", Session("IDTeam"), Session("IDPeriod"), sNomeVariabileLista, Session("IDGame"))).ToString("N0")
                tblCell.Controls.Add(oLinkInterMarkeRequi)

                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)
            Next
            tblWholesalersTeam.Rows.Add(tblRow)

            ' Riga Sales
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLBLSales As New RadLabel
            oLBLSales.ID = "lblSales"
            oLBLSales.Text = "Sales (units)"
            oLBLSales.Style.Add("color", "darkred")
            tblCell.Controls.Add(oLBLSales)
            tblCell.Style.Add("width", "20%")
            tblRow.Cells.Add(tblCell)

            For Each oRowItm As DataRow In oDTItems.Rows
                tblCell = New TableCell

                ' Creazione dell'oggetto linked server per rimandare alla pagina dei grafici
                Dim oLinkSalesInterMarke As New HyperLink
                oLinkSalesInterMarke.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=SalesInterMarke&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                oLinkSalesInterMarke.Style.Add("text-decoration", "none")
                oLinkSalesInterMarke.Style.Add("cursor", "pointer")

                oLinkSalesInterMarke.Text = Nn(GetVariableDefineValue("SalesInterMarke", Session("IDTeam"), oRowItm("IDVariable"), 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N0")
                tblCell.Controls.Add(oLinkSalesInterMarke)
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)
            Next
            tblWholesalersTeam.Rows.Add(tblRow)

            ' Riga sales value
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLBLSalesValue As New RadLabel
            oLBLSalesValue.ID = "lblSalesValue"
            oLBLSalesValue.Text = "Sales (value)"
            oLBLSalesValue.Style.Add("color", "darkred")
            tblCell.Controls.Add(oLBLSalesValue)
            tblCell.Style.Add("width", "20%")
            tblRow.Cells.Add(tblCell)

            For Each oRowItm As DataRow In oDTItems.Rows
                tblCell = New TableCell
                ' Creazione dell'oggetto linked server per rimandare alla pagina dei grafici
                Dim oLinkProduRevenInter As New HyperLink
                oLinkProduRevenInter.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=ProduRevenInter&II=" & 0 & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                oLinkProduRevenInter.Style.Add("text-decoration", "none")
                oLinkProduRevenInter.Style.Add("cursor", "pointer")

                oLinkProduRevenInter.Text = Nn(GetVariableDefineValue("ProduRevenInter", Session("IDTeam"), oRowItm("IDVariable"), 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("C0")
                tblCell.Controls.Add(oLinkProduRevenInter)
                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)
            Next
            tblWholesalersTeam.Rows.Add(tblRow)

            ' Riga Share
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLBLShare As New RadLabel
            oLBLShare.ID = "lblShare"
            oLBLShare.Text = "Share"
            oLBLShare.Style.Add("color", "darkred")
            tblCell.Controls.Add(oLBLShare)
            tblCell.Style.Add("width", "20%")
            tblRow.Cells.Add(tblCell)

            Dim dGlobaInterSales As Double
            Dim dSalesInterMarke As Double

            For Each oRowItm As DataRow In oDTItems.Rows
                tblCell = New TableCell
                dGlobaInterSales = Nn(GetVariableDefineValue("GlobaInterSales", 0, oRowItm("IDVariable"), 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N0")
                dSalesInterMarke = Nn(GetVariableDefineValue("SalesInterMarke", Session("IDTeam"), oRowItm("IDVariable"), 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N0")

                If dGlobaInterSales > 0 Then
                    tblCell.Text = (dSalesInterMarke / dGlobaInterSales).ToString("P1")
                Else
                    tblCell.Text = 0.ToString("P1")
                End If

                tblCell.BorderStyle = BorderStyle.None
                tblRow.Cells.Add(tblCell)
            Next
            tblWholesalersTeam.Rows.Add(tblRow)

        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub Distribution_Player_Wholesalers_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        If Not Page.IsPostBack Then
            If Session("IDRole") = "P" Then
                m_SiteMaster.ReloadPeriodsCalculate()
            End If
            LoadData()

            btnTranslate.Visible = Session("IDRole").Contains("D")
        End If

        ' Controllo tutti gli oggetti presenti
        Dim oTranslation As New TranslationObjectForm(Me, g_DAL, Nz(Session("LanguageActive")))

    End Sub

    Private Sub btnTranslate_Click(sender As Object, e As EventArgs) Handles btnTranslate.Click
        Session("PageOrigin") = Me.ToString().Substring(4, Me.ToString().Substring(4).Length - 5) + ".aspx"
        Response.Redirect("PageTranslation.aspx")
        Response.End()
    End Sub

#Region "PANNELLO HELP"

    Private Sub LoadHelp()
        lblHelp.Text = "<p><strong>Wholesalers market</strong><br>
                        This is the  information for market demand and sales in the wholesalers market. If the  market demand is not satisfied, the excess quantity will &nbsp;be lost.<br>
                        <strong>Minimum quality</strong><br>
                        This was  the minimum quality accepted in the period by the wholesalers.<br>
                        <strong>Maximum price</strong><br>
                        This was  the maximum price accepted in the period by the wholesalers.<br>
                        <strong>Requested quantity</strong><br>
                        This is the  amount requested per product to the whole market.<br>
                        <strong>Market sales</strong><br>
                        The total  sales in the wholesalers market.<br>
                        <strong>Wholesalers offer</strong><br>
                        This is  what your company offered in the period to the wholesalers and the quantity  sold.<br>
                        <strong>Offer (units)</strong><br>
                        The  quantity offered is based on the terms offered by the company; it can be  restricted by an inability to match the expectations set by the &nbsp;wholesalers.<br>
                        <strong>Sales (units)</strong><br>
                        This is the  quantity sold to the wholesalers.<br>
                        <strong>Sales  (value)</strong><br>
                        This is the  revenues per product in the wholesalers market</p>"
    End Sub

#End Region

End Class
