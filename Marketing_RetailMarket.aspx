﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Marketing_RetailMarket.aspx.vb" Inherits="Marketing_RetailMarket" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="cntMain" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:UpdatePanel ID="pnlMain" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <h2>
                <asp:Label ID="lblWelcome" runat="server" Text="Marketing"></asp:Label>
            </h2>

            <div class="clr"></div>

            <h3>
                <asp:Label ID="lblTitolo" runat="server" Text="Retail market"></asp:Label>
            </h3>

            <div class="divTable" id="divTableMasterMarketing" runat="server">
                <asp:Table ClientIDMode="Static" ID="tblMarketingMaster" runat="server">
                </asp:Table>
            </div>
            
            <div class="clr"></div>

            <div class="divTable">
                <telerik:RadHtmlChart ID="grfDemandSales" runat="server" Width="100%">
                    <ChartTitle Text="Retail market demand and sales"></ChartTitle>

                    <PlotArea>
                        <Series>
                            <telerik:ColumnSeries Name="Demand" DataFieldY="GlobalMarketDemand">
                                <TooltipsAppearance DataFormatString="{0:n}" BackgroundColor="White" Color="Blue"/>
                                <LabelsAppearance Visible="true" DataField="Item" Position="Center" RotationAngle="-90" Color="#FFE038"/>
                                <Appearance FillStyle-BackgroundColor="#2359FC" />
                            </telerik:ColumnSeries>

                            <telerik:LineSeries Name="Sales" DataFieldY="TotalOrderAlloc">
                                <Appearance FillStyle-BackgroundColor="#B67C00"/>
                                <LineAppearance LineStyle="Smooth" Width="3px" />
                                <TooltipsAppearance DataFormatString="{0:n}" BackgroundColor="White" Color="#B67C00"/>
                                <LabelsAppearance Visible="false" />
                            </telerik:LineSeries>
                        </Series>

                        <XAxis>
                        </XAxis>

                        <YAxis>
                            <LabelsAppearance DataFormatString="{0}" />
                        </YAxis>

                    </PlotArea>

                    <Legend>
                        <Appearance Visible="true" Position="Top" />
                    </Legend>
                </telerik:RadHtmlChart>

                <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="pnlMain">
                    <ProgressTemplate>
                        <asp:Panel ID="Panel1" CssClass="overlay" runat="server">
                            <asp:Panel ID="Panel2" CssClass="loader" runat="server">
                                <img alt="" src="Images/Loading.gif" />
                            </asp:Panel>
                        </asp:Panel>
                    </ProgressTemplate>
                </asp:UpdateProgress>

            </div>

            <asp:PlaceHolder runat="server" ID="Message" Visible="false">
                <p class="text-danger">
                    <asp:Literal runat="server" ID="MessageText" />
                </p>
            </asp:PlaceHolder>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

