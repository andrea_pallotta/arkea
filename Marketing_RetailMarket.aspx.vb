﻿Imports System.Data
Imports APS
Imports APPCore.Utility
Imports DALC4NET
Imports Telerik.Web.UI
Imports System.Drawing

Partial Class Marketing_RetailMarket
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData
    Private m_Separator As String = "."

    Private m_IDPeriodoMax As Integer

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        HandleLoadDataMarketing()
        pnlMain.Update()
    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        HandleLoadDataMarketing()
        pnlMain.Update()
    End Sub

    Private Sub HandleLoadDataMarketing()
        Dim oDTResults As DataTable
        Dim sSQL As String

        Try
            divTableMasterMarketing.Controls.Clear()

            Dim oSiteMaster As SiteMaster = Me.Master

            If m_IDPeriodoMax > 1 AndAlso Nni(oSiteMaster.PeriodGetPrev) >= 1 Then
                If Session("IDRole") <> "P" Then
                    sSQL = "SELECT * FROM vResults_Marketing_Boss WHERE IDGame = " & m_SessionData.Utente.Games.IDGame
                Else
                    sSQL = "SELECT * FROM vResults_Marketing_Player WHERE IDGame = " & m_SessionData.Utente.Games.IDGame & " " _
                         & " AND IDPlayer = " & Nni(Session("IDTeam"))
                End If
                sSQL &= " AND IDCategory = 3 " _
                     & "AND IDPeriod <= " & oSiteMaster.PeriodGetPrev & " " _
                     & "AND VariableName IN ('SalesPriceAllow','FirstOrderAlloc') " _
                     & "ORDER BY TeamName, Variable, Item"

                oDTResults = g_DAL.ExecuteDataTable(sSQL)

                ' Converto i valori dei risultati da varchar a numerici con con decimali
                For Each oRow As DataRow In oDTResults.Rows
                    oRow("ValueDef") = Math.Round(Nn(oRow("Value")), 2).ToString("N2")
                Next

                Dim oPivot As New Pivot(oDTResults)
                Dim dtPivot As DataTable = oPivot.PivotData("TeamName", "ValueDef", AggregateFunction.Sum, "Variable", "Item")

                Dim oGridViewMkt As New GridView
                AddHandler oGridViewMkt.RowCreated, AddressOf grdPivot_RowCreated
                AddHandler oGridViewMkt.RowDataBound, AddressOf grdPivot_RowDataBound
                oGridViewMkt.DataSource = dtPivot
                oGridViewMkt.DataBind()

                divTableMasterMarketing.Controls.Add(oGridViewMkt)

                SQLConnClose(g_DAL.GetConnObject)

                ' Per ogni cella sistemo il formato, se di tipo numerico faccio l'allineamento a destra, altrimenti a sinistra
                For Each oRow As GridViewRow In oGridViewMkt.Rows
                    For Each oCell As TableCell In oRow.Cells
                        If IsNumeric(oCell.Text) Then
                            oCell.HorizontalAlign = HorizontalAlign.Right
                        Else
                            oCell.HorizontalAlign = HorizontalAlign.Left
                        End If
                    Next
                Next

                ' Aggiungo una nuova riga per mettere i totali delle quantità
                'Dim tblRow As TableRow
                'Dim tblCell As TableCell

                'tblRow = New TableRow
                'tblCell = New TableCell

                ' Popolo il grafico

                Dim oDTGlobalMarket As DataTable
                Dim oDTGlobalMarketSales As DataTable

                ' Richiesta
                sSQL = "SELECT V.Variable, V.VariableName, V.IDPeriod, SUM(CONVERT(Decimal,REPLACE(V.Value, ',', '.'))) AS GlobalMarketDemand, P.Descrizione, V.Item " _
                     & "FROM vResults_Marketing_Boss V " _
                     & "LEFT JOIN BGOL_Periods P ON V.IDPeriod = P.Id " _
                     & "WHERE V.IDGame = " & m_SessionData.Utente.Games.IDGame & " " _
                     & "AND V.IDPeriod <= " & oSiteMaster.PeriodGetPrev & " And V.VariableName = 'MarketDemand' " _
                     & "GROUP BY V.Variable, V.VariableName, V.IDPeriod, P.Descrizione, V.Item "
                oDTGlobalMarket = g_DAL.ExecuteDataTable(sSQL)

                ' Venduto
                sSQL = "SELECT V.Variable, V.VariableName, V.IDPeriod, SUM(CONVERT(Decimal,REPLACE(V.Value, ',', '.'))) AS TotalOrderAlloc, P.Descrizione, V.Item " _
                     & "FROM vResults_Marketing_Boss V " _
                     & "LEFT JOIN BGOL_Periods P ON V.IDPeriod = P.Id " _
                     & "WHERE V.IDGame = " & m_SessionData.Utente.Games.IDGame & " " _
                     & "AND V.IDPeriod <= " & oSiteMaster.PeriodGetPrev & " And V.VariableName = 'TotalOrderAlloc' " _
                     & "GROUP BY V.Variable, V.VariableName, V.IDPeriod, P.Descrizione, V.Item "
                oDTGlobalMarketSales = g_DAL.ExecuteDataTable(sSQL)

                ' Aggiungo la colonna del venduto al Datatable appena creato
                oDTGlobalMarket.Columns.Add("TotalOrderAlloc", GetType(Decimal))

                ' Per ogni riga presente nel datatable vado a recuperare il valore nel datatable del venduto
                For Each oRow As DataRow In oDTGlobalMarket.Rows
                    Dim oDRRowSales As DataRow = oDTGlobalMarketSales.Select("IdPeriod = " & Nni(oRow("IDPeriod")) & " AND Item = '" & Nz(oRow("Item")) & "' ").FirstOrDefault
                    oRow("TotalOrderAlloc") = Nn(oDRRowSales("TotalOrderAlloc"))
                Next

                grfDemandSales.DataSource = oDTGlobalMarket
                grfDemandSales.DataBind()

                grfDemandSales.PlotArea.XAxis.DataLabelsField = "Descrizione"
                grfDemandSales.PlotArea.XAxis.LabelsAppearance.TextStyle.Bold = True

                ' Mostro solo le etichette raggruppate, per cui eseguo uno step ogni x items
                grfDemandSales.PlotArea.XAxis.LabelsAppearance.Step = LoadItemsGame(m_SessionData.Utente.Games.IDGame, Nz(Session("LanguageActive"))).Rows.Count
                grfDemandSales.PlotArea.XAxis.LabelsAppearance.RotationAngle = 45
                grfDemandSales.PlotArea.XAxis.EnableBaseUnitStepAuto = True

                SQLConnClose(g_DAL.GetConnObject)

            End If

        Catch ex As Exception
            MessageText.Text = ex.Message & "<br/>" & g_MessaggioErrore & "<br/> "
            Message.Visible = True
        End Try

    End Sub

    Private Sub Marketing_RetailMarket_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsNothing(m_SessionData) Then
            PageRedirect("~/Account/Login.aspx", "", "")
        End If
    End Sub

    Protected Sub grdPivot_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.Header Then
            MergeHeader(DirectCast(sender, GridView), e.Row, 2)
        End If
    End Sub

    Protected Sub grdPivot_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            If e.Row.DataItemIndex >= 0 Then
                e.Row.Cells(0).BackColor = System.Drawing.ColorTranslator.FromHtml("#55A2DF")
                e.Row.Cells(0).ForeColor = System.Drawing.ColorTranslator.FromHtml("#fff")
            End If
        End If
    End Sub

    Private Sub MergeHeader(ByVal gv As GridView, ByVal row As GridViewRow, ByVal PivotLevel As Integer)
        Dim iCount As Integer = 1
        Dim iContaColonneHeader = 0

        For iCount = 1 To PivotLevel
            Dim oGridViewRow As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
            Dim Header = (row.Cells.Cast(Of TableCell)().[Select](Function(x) GetHeaderText(x.Text, iCount, PivotLevel))).GroupBy(Function(x) x)

            For Each v In Header
                Dim cell As New TableHeaderCell()
                If iContaColonneHeader > 0 Then
                    cell.Text = v.Key.Substring(v.Key.LastIndexOf(m_Separator) + 1)
                Else
                    cell.Text = ""
                End If

                cell.ColumnSpan = v.Count()

                oGridViewRow.Cells.Add(cell)
                iContaColonneHeader += 1
            Next
            gv.Controls(0).Controls.AddAt(row.RowIndex, oGridViewRow)
        Next
        row.Visible = False
    End Sub

    Private Function GetHeaderText(ByVal s As String, ByVal i As Integer, ByVal PivotLevel As Integer) As String
        If Not s.Contains(m_Separator) AndAlso i <> PivotLevel Then
            Return String.Empty
        Else
            Dim Index As Integer = NthIndexOf(s, m_Separator, i)
            If Index = -1 Then
                Return s
            End If
            Return s.Substring(0, Index)
        End If
    End Function

    Private Function NthIndexOf(ByVal str As String, ByVal SubString As String, ByVal n As Integer) As Integer
        Dim x As Integer = -1
        For i As Integer = 0 To n - 1
            x = str.IndexOf(SubString, x + 1)
            If x = -1 Then
                Return x
            End If
        Next
        Return x
    End Function

    Private Sub Marketing_RetailMarket_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        m_SessionData = Session("SessionData")

        Dim sSQL As String

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oSiteMaster As SiteMaster = Me.Master
        Dim oCBOPeriod As RadComboBox = DirectCast(oSiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(oSiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler oSiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler oSiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        m_IDPeriodoMax = HandleGetMaxPeriodValid(m_SessionData.Utente.Games.IDGame) ' Ultimo periodo di gioco
        Dim iIDPeriodoMaxGame As Integer = HandleGetMaxPeriodGame(m_SessionData.Utente.Games.IDGame) ' Periodo massimo del game

        If iIDPeriodoMaxGame = m_IDPeriodoMax OrElse m_IDPeriodoMax = 1 Then
            ' Controllo di avere dei risultati nel caso sia nell'ultimo periodo
            ' altrimenti vuol dire che lo sto ancora giocando e non posso mostrare alcun risultato

            sSQL = "SELECT TOP 1 * FROM vResults_Sales_Boss WHERE IDGame = " & m_SessionData.Utente.Games.IDGame & " " _
                 & "AND IDPeriod = " & m_IDPeriodoMax & " "
            Dim oDAL As New DBHelper
            Dim oDTResult As DataTable = oDAL.ExecuteDataTable(sSQL)
            SQLConnClose(oDAL.GetConnObject)
            If oDTResult.Rows.Count > 0 Then
                oCBOPeriod.SelectedValue = m_IDPeriodoMax
                oSiteMaster.PeriodChange = m_IDPeriodoMax
            Else
                oCBOPeriod.SelectedValue = m_IDPeriodoMax - 1
                oSiteMaster.PeriodChange = m_IDPeriodoMax - 1
            End If
        Else
            oCBOPeriod.SelectedValue = m_IDPeriodoMax - 1
            oSiteMaster.PeriodChange = m_IDPeriodoMax - 1
        End If

        HandleLoadDataMarketing()
    End Sub
End Class
