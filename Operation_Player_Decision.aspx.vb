﻿Imports System.Data
Imports APS
Imports APPCore.Utility
Imports DALC4NET
Imports Telerik.Web.UI

Partial Class Operation_Player_Decision
    Inherits System.Web.UI.Page

    Private m_Variable As List(Of String)

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster

    Private Sub EnableSave()
        If Nni(Session("IDPeriod")) = HandleGetMaxPeriodValid(Session("IDGame")) Then
            btnSave.Enabled = True
        Else
            If Session("IDRole") = "P" Then
                btnSave.Enabled = False

            ElseIf Session("IDRole") = "D" Then
                btnSave.Enabled = True
            End If
        End If
    End Sub

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        tblPlayerDecision.Rows.Clear()
        HandleLoadPlayerDecision()
        Message.Visible = False
        EnableSave()

        pnlMain.Update()
    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        tblPlayerDecision.Rows.Clear()
        HandleLoadPlayerDecision()
        Message.Visible = False
        If Nni(Session("IDPeriod")) = HandleGetMaxPeriodValid(Session("IDGame")) Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If
        pnlMain.Update()
    End Sub

    Private Sub Operation_Player_Decision_Init(sender As Object, e As EventArgs) Handles Me.Init
        m_SessionData = Session("SessionData")

        If IsNothing(m_SessionData) Then
            PageRedirect("~/Account/Login.aspx", "", "")
        End If

        m_SiteMaster = Me.Master

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oCBOPeriod As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler m_SiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler m_SiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(m_SessionData.Utente.Games.IDGame) ' Ultimo periodo
        oCBOPeriod.SelectedValue = Session("IDPeriodValid")
        m_SiteMaster.PeriodChange = Session("IDPeriodValid")

        Session("NameTeam") = GetTeamName(Session("IDTeam"))
        oCBOTeam.SelectedValue = Session("IDTeam")
        m_SiteMaster.TeamChange = Session("IDTeam")

        ' Aggiungo le variabili da gestire nella lista
        m_Variable = New List(Of String)

        m_Variable.Add("PurchEuropRequi")
        m_Variable.Add("PurchNICRequi")
        m_Variable.Add("ExtraRequi")

        m_Variable.Add("ProduBasicRequi")

        m_Variable.Add("LeaseRequi")
        m_Variable.Add("PlantRequi")
        m_Variable.Add("TechnAutomRequi")

        ' Controllo la presenza di tutte le decisioni per questo player
        'If Session("CurrentStep") <= 1 Then
        '    CheckDecisionsPlayerExistsDatabase(m_Variable, Session("IDGame"), m_SiteMaster.PeriodGetCurrent, eVariablesCateogry.Operations, Nni(Session("IDTeam")))
        'End If

        HandleLoadPlayerDecision()

        ' Gestione del pannello delle decisioni concesse
        modalHelp.OpenerElementID = lnkHelp.ClientID
        modalHelp.Modal = False
        modalHelp.VisibleTitlebar = True

        LoadHelp()
    End Sub

    Private Sub HandleLoadPlayerDecision()
        Dim bAggiungiHeader As Boolean = False

        Dim oDTDecisionsPlayer As DataTable
        Dim oDTDecisionsPlayerValue As DataTable

        Dim sHeader As String = ""

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Dim sNomeVariabile As String
        Dim sTraduzione As String
        Dim sTraduzioneExternalObject As String

        Dim bRigaVisibile As Boolean = False

        ' Carico la tabella delle intestazioni da usare
        oDTDecisionsPlayer = HandleLoadGameDecisionPlayer(m_SessionData.Utente.Games.IDGame, eVariablesCateogry.Operations)

        ' Recupero le informazioni necessarie per costruirmi l'header della tabella HTML
        Dim oDTHeader As DataTable = oDTDecisionsPlayer

        ' Per ogni riga di intestazione inizio a costruire la tabella
        For Each oRowHeader As DataRow In oDTHeader.Rows
            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell
            If Nz(oRowHeader("VariableLabelGroup")) = "0" Or Nz(oRowHeader("VariableLabelGroup")) = "" Then
                Dim lblOperationsDecisions As New RadLabel
                lblOperationsDecisions.ID = "lblOperationsDecisions"
                lblOperationsDecisions.Text = "Operation Decisions"
                lblOperationsDecisions.ForeColor = Drawing.Color.White
                tblHeaderCell.Controls.Add(lblOperationsDecisions)
                bAggiungiHeader = True

            Else
                If sHeader <> Nz(oRowHeader("VariableLabelGroup")) Then
                    Dim lblVariableLabelGroup As New RadLabel
                    lblVariableLabelGroup.ID = "lblVariableLabelGroup_" & Nz(oRowHeader("VariableLabelGroup"))
                    lblVariableLabelGroup.Text = Nz(oRowHeader("VariableLabelGroup"))
                    lblVariableLabelGroup.ForeColor = Drawing.Color.White
                    tblHeaderCell.Controls.Add(lblVariableLabelGroup)
                    sHeader = Nz(oRowHeader("VariableLabelGroup"))
                    bAggiungiHeader = True
                Else
                    bAggiungiHeader = False
                End If

            End If

            tblHeaderCell.ColumnSpan = 3
            tblHeaderCell.BorderStyle = BorderStyle.None

            tblHeaderRow.Cells.Add(tblHeaderCell)
            tblHeaderRow.BorderStyle = BorderStyle.None
            tblHeaderRow.Style.Add("margin-top", "10px")

            ' Aggiungo la l'header solo se ho del testo di identificazione del gruppo
            'If tblHeaderCell.Text <> "" AndAlso sHeader <> Nz(oRowHeader("VariableLabelGroup")) Then tblPlayerDecision.Rows.Add(tblHeaderRow)
            If bAggiungiHeader Then
                tblPlayerDecision.Rows.Add(tblHeaderRow)
                bAggiungiHeader = False
            End If

            ' Carico i valori della variabile, e controllo eventualmente se è una variabile dipendente da qualche lista
            oDTDecisionsPlayerValue = HandleLoadGamePeriodDecisionPlayerValue(m_SiteMaster.PeriodGetCurrent, Nni(oRowHeader("ID")))

            ' Ciclo sui valori delle varibili trovate
            For Each oRowVariableValue As DataRow In oDTDecisionsPlayerValue.Rows
                sTraduzione = GetVariableTranslation(Session("IDGame"), Nni(oRowVariableValue("IDVariable")), Nz(Session("LanguageActive")))
                If Nz(oRowVariableValue("VariableValue")).ToUpper = "[LIST OF ITEMS]" Then ' Lista di ITEMS/Product
                    ' Carico i valori presenti nella tabella Decisions_Players_value_List
                    Dim oDTValoriPLayer As DataTable = HandleGetValueDecisionPlayer(Nni(oRowVariableValue("ID")), Nni(Session("IDTeam")), m_SiteMaster.PeriodGetCurrent)

                    If oDTValoriPLayer.Rows.Count > 0 Then
                        For Each oROWValore As DataRow In oDTValoriPLayer.Rows
                            bRigaVisibile = False

                            ' Recupero la traduzione corretta dell'item 
                            sTraduzioneExternalObject = GetItemTranslation(Nni(oROWValore("IDItem")), Nz(Session("LanguageActive")))

                            'sNomeVariabile = "var_" & Nz(Session("NameTeam") & "_" & Nni(oRowHeader("Id")) & "_" & Nz(oROWValore("VariableName"))).Replace(" ", "")
                            sNomeVariabile = Nz(oROWValore("VariableName"))
                            ' Etichetta della variabile
                            tblRow = New TableRow
                            tblCell = New TableCell

                            ' Recupero la traduzione in lingua
                            tblCell.Text = sTraduzione & " - " & sTraduzioneExternalObject
                            tblCell.BorderStyle = BorderStyle.None
                            tblRow.Cells.Add(tblCell)

                            ' Creo i controlli necessari ed eventualmente li popolo
                            ' Carico i valori esistenti, altrimenti li creo
                            Dim oTxtBox As New TextBox
                            tblCell = New TableCell
                            oTxtBox.Text = Nz(oROWValore("Value"))
                            oTxtBox.ID = Nz(sNomeVariabile)
                            oTxtBox.Attributes.Add("runat", "server")
                            oTxtBox.Style.Add("text-align", "right")
                            oTxtBox.ClientIDMode = ClientIDMode.Static
                            oTxtBox.EnableViewState = True
                            oTxtBox.TextMode = TextBoxMode.Number
                            oTxtBox.Attributes.Add("type", "number")
                            ' Controllo se la decisione deve essere abilitata in un determinato periodo oppure se devo solo disabilitarla
                            If Nni(oRowHeader("EnabledFromPeriod"), 0) > 0 Then
                                oTxtBox.Enabled = Nni(Session("IDPeriod")) >= (Nni(oRowHeader("EnabledFromPeriod"), 0))
                                bRigaVisibile = Nni(Session("IDPeriod")) >= (Nni(oRowHeader("EnabledFromPeriod"), 0))

                            Else
                                oTxtBox.Enabled = Nb(oRowHeader("Attiva"))
                                bRigaVisibile = Nb(oRowHeader("Attiva"))
                            End If

                            ' Aggiungo il textbox appena creato alla cella
                            tblCell.Controls.Add(oTxtBox)
                            tblCell.Style.Add("text-align", "right")
                            tblCell.BorderStyle = BorderStyle.None
                            tblRow.Cells.Add(tblCell)

                            If bRigaVisibile Then
                                tblPlayerDecision.Rows.Add(tblRow)
                            End If
                        Next

                    End If

                Else
                    Dim oDTValoriPlayer As DataTable = HandleLoadGamePeriodDecisionPlayerValue(m_SiteMaster.PeriodGetCurrent, Nni(oRowVariableValue("IDDecisionPlayer")))
                    For Each oRowData As DataRow In oDTValoriPlayer.Rows
                        If Nni(oRowData("IDPlayer")) = Nni(Session("IDTeam")) Then
                            sNomeVariabile = "var_" & Nz(oRowData("IDPlayer") & "_" & Nz(oRowData("VariableName"))).Replace(" ", "")
                            ' Controllo la presenza di un eventuale textbox con lo stesso ID
                            ' Se presente esco, altrimenti lo creo correttamente
                            Dim oTXT As TextBox = TryCast(FindControlRecursive(Page, sNomeVariabile), TextBox)
                            If oTXT Is Nothing Then
                                ' Etichetta della variabile
                                tblRow = New TableRow
                                tblCell = New TableCell
                                tblCell.Text = sTraduzione
                                tblCell.BorderStyle = BorderStyle.None
                                tblRow.Cells.Add(tblCell)

                                ' Creo i controlli necessari ed eventualmente li popolo
                                ' Carico i valori esistenti, altrimenti li creo
                                Dim oTxtBox As New TextBox
                                tblCell = New TableCell

                                ' Recupero il valore secco della decisione del player nel periodo
                                Dim sValore As String = HandleLoadGamePeriodDecisionPlayerValueSingle(m_SiteMaster.PeriodGetCurrent, Nni(Session("IDTeam")), Nni(oRowData("IDDecisionPlayer")))
                                oTxtBox.Text = sValore
                                oTxtBox.ID = Nz(sNomeVariabile)
                                oTxtBox.Attributes.Add("runat", "server")
                                oTxtBox.Style.Add("text-align", "right")
                                oTxtBox.ClientIDMode = ClientIDMode.Static
                                oTxtBox.EnableViewState = True
                                oTxtBox.TextMode = TextBoxMode.Number
                                oTxtBox.Attributes.Add("type", "number")
                                ' Controllo se la decisione deve essere abilitata in un determinato periodo oppure se devo solo disabilitarla
                                If Nni(oRowHeader("EnabledFromPeriod"), 0) > 0 Then
                                    oTxtBox.Enabled = Nni(Session("IDPeriod")) >= (Nni(oRowHeader("EnabledFromPeriod"), 0))
                                    bRigaVisibile = Nni(Session("IDPeriod")) >= (Nni(oRowHeader("EnabledFromPeriod"), 0))

                                Else
                                    oTxtBox.Enabled = Nb(oRowHeader("Attiva"))
                                    bRigaVisibile = Nb(oRowHeader("Attiva"))

                                End If

                                ' Aggiungo il textbox appena creato alla cella
                                tblCell.Controls.Add(oTxtBox)
                                tblCell.Style.Add("text-align", "right")
                                tblCell.BorderStyle = BorderStyle.None
                                tblRow.Cells.Add(tblCell)

                                If bRigaVisibile Then
                                    tblPlayerDecision.Rows.Add(tblRow)
                                End If
                            End If
                        End If
                    Next
                End If
            Next
            sHeader = Nz(oRowHeader("VariableLabelGroup"))
        Next


    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim oDTDecisionsPlayer As DataTable = HandleLoadGameDecisionPlayer(m_SessionData.Utente.Games.IDGame, eVariablesCateogry.Operations)
        Dim sNomeTextBox As String
        Dim sNomeTeam As String = Nz(m_SiteMaster.TeamNameGet).Replace(" ", "")
        Dim sSQL As String
        Dim oDTDecisionPlayerValue As DataTable
        Dim oTXT As TextBox
        Dim oDAL As New DBHelper

        Message.Visible = False

        ' Controllo che effettivamente possa salvare e non sia scaduto il termine delle decisioni
        If Not IsPossibleToSaveDataPeriod(Session("IDGame"), Session("IDPeriod")) Then
            MessageText.Text = "Time over for this period"
            Message.Visible = True

            pnlMain.Update()

            btnSave.Enabled = False

            Exit Sub
        End If

        g_DAL = New DBHelper

        pnlMain.Update()
        For Each oRow As DataRow In oDTDecisionsPlayer.Rows
            ' Controllo se la varibile recuperata è di tipo lista altrimenti passo alle variabili con inserimento valore secco
            sSQL = "SELECT VariableValue FROM Decisions_Players_Value WHERE IDDecisionPlayer = " & Nni(oRow("ID"))
            Dim sVariableType As String = Nz(oDAL.ExecuteScalar(sSQL))

            sSQL = "SELECT ID FROM Decisions_Players_Value WHERE IDDecisionPlayer = " & Nni(oRow("ID"))
            Dim iIDDecisionPlayerValue As Integer = Nni(oDAL.ExecuteScalar(sSQL))

            If sVariableType.ToUpper.Contains("LIST OF") Then
                ' Recupero i dati della variabile interessata per cercare anche il Textbox che contiene i valori da salvare
                sSQL = "SELECT * FROM Decisions_Players_Value_List WHERE IDDecisionValue =" & iIDDecisionPlayerValue & " AND IDPlayer = " & Session("IDTeam")
                Dim oDTDecisionsValueList As DataTable = oDAL.ExecuteDataTable(sSQL)

                For Each oRowValueList As DataRow In oDTDecisionsValueList.Rows
                    sNomeTextBox = Nz(oRowValueList("VariableName"))

                    oTXT = TryCast(FindControlRecursive(Page, sNomeTextBox), TextBox)
                    If oTXT IsNot Nothing AndAlso oTXT.Text <> "" Then
                        ' Vado a salvare i valori corretti nella tabella di riferimento
                        sSQL = "SELECT * FROM Decisions_Players_Value_List WHERE IDPlayer = " & Nni(Session("IDTeam")) _
                             & " And IDDecisionValue = " & iIDDecisionPlayerValue & " And VariableName = '" & Nz(oRowValueList("VariableName")) & "' "
                        oDTDecisionPlayerValue = oDAL.ExecuteDataTable(sSQL)
                        If oDTDecisionPlayerValue.Rows.Count > 0 Then ' Ci sono dati vado in update
                            sSQL = "UPDATE Decisions_Players_Value_List SET Value = '" & oTXT.Text & "' " _
                                 & "WHERE IDPlayer = " & Nni(Session("IDTeam")) & " AND IDDecisionValue = " & iIDDecisionPlayerValue & " " _
                                 & "AND IDPeriod = " & Nni(Session("IDPeriod")) & " AND VariableName = '" & Nz(oRowValueList("VariableName")) & "' "
                            oDAL.ExecuteNonQuery(sSQL)

                            If Nni(Session("IDTeamM&U")) > 0 Then
                                If Nni(Session("IDTeam")) = Nni(Session("IDTeamMaster")) Then
                                    sSQL = "UPDATE Decisions_Players_Value_List SET Value = '" & oTXT.Text & "' " _
                                         & "WHERE IDPlayer = " & Nni(Session("IDTeamM&U")) & " AND IDDecisionValue = " & iIDDecisionPlayerValue & " " _
                                         & "AND IDPeriod = " & Nni(Session("IDPeriod")) & " AND VariableName = '" & Nz(oRowValueList("VariableName")) & "' "
                                    oDAL.ExecuteNonQuery(sSQL)
                                End If
                            End If
                        End If
                    End If
                Next

            Else
                ' Recupero tutti i campi che non necessitano di items o oggetti esterni
                ' E' fallita la ricerca, provo a ricercare il textbox nelle variabili a uso singolo, senza gestione del list of
                sSQL = "SELECT * FROM Decisions_Players_Value WHERE IDDecisionPlayer = " & Nni(oRow("ID")) & " AND ISNULL(IDPeriod, " & m_SiteMaster.PeriodGetCurrent & ") = " & m_SiteMaster.PeriodGetCurrent
                Dim oDTDataValue As DataTable = oDAL.ExecuteDataTable(sSQL)

                For Each oRowDataValue As DataRow In oDTDataValue.Rows
                    sNomeTextBox = "var_" & Nz(oRowDataValue("IDPlayer") & "_" & Nz(oRow("VariableName")))

                    oTXT = TryCast(FindControlRecursive(Page, sNomeTextBox), TextBox)
                    If oTXT IsNot Nothing AndAlso oTXT.Text <> "" Then
                        sSQL = "UPDATE Decisions_Players_Value SET VariableValue = '" & oTXT.Text & "' WHERE IDDecisionPlayer = " & Nni(oRow("ID")) & " AND IDPlayer = " & Nni(Session("IDTeam")) _
                             & " AND ISNULL(IDPeriod, " & Nni(Session("IDPeriod")) & ") = " & Nni(Session("IDPeriod"))
                        oDAL.ExecuteNonQuery(sSQL)

                        If Nni(Session("IDTeamM&U")) > 0 Then
                            If Nni(Session("IDTeam")) = Nni(Session("IDTeamMaster")) Then
                                sSQL = "UPDATE Decisions_Players_Value SET VariableValue = '" & oTXT.Text & "' WHERE IDDecisionPlayer = " & Nni(oRow("ID")) & " AND IDPlayer = " & Nni(Session("IDTeamM&U")) _
                                     & " AND ISNULL(IDPeriod, " & Nni(Session("IDPeriod")) & ") = " & Nni(Session("IDPeriod"))
                                oDAL.ExecuteNonQuery(sSQL)
                            End If
                        End If
                    End If
                Next

            End If

        Next

        SQLConnClose(oDAL.GetConnObject)

        oDAL = Nothing

        MessageText.Text = "Save completed"
        Message.Visible = True
    End Sub

    Private Sub Operation_Player_Decision_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete

        btnTranslate.Visible = Session("IDRole").Contains("D")
        btnManageDecisions.Visible = Session("IDRole").Contains("D")

        ' Controllo tutti gli oggetti presenti
        Dim oTranslation As New TranslationObjectForm(Me, g_DAL, Nz(Session("LanguageActive")))

        EnableSave()
    End Sub

    Private Sub btnTranslate_Click(sender As Object, e As EventArgs) Handles btnTranslate.Click
        Session("PageOrigin") = Me.ToString().Substring(4, Me.ToString().Substring(4).Length - 5) + ".aspx"
        Response.Redirect("PageTranslation.aspx")
        Response.End()
    End Sub

    Private Sub btnManageDecisions_Click(sender As Object, e As EventArgs) Handles btnManageDecisions.Click
        Session("PageOrigin") = Me.ToString().Substring(4, Me.ToString().Substring(4).Length - 5) + ".aspx"
        Response.Redirect("PageManageDecisions.aspx")
        Response.End()
    End Sub

#Region "PANNELLO HELP"

    Private Sub LoadHelp()
        lblHelp.Text = "<p><strong>Purchase of raw materials</strong><br>
                        <strong>Raw materials for each product may be procured  from local suppliers or from importers. </strong><br>
                        <strong>Local supplies are available immediately.</strong><br>
                        <strong>Imported supplies can be of lower and variable  quality but cost less.&nbsp; There is one  month delay with imported supplies, i.e. only 2/3 of the total ordered is  available to use in the current quarter, the remainder will arrive next  quarter, but are not guaranteed at the same quality.</strong><br>
                        <strong>The stock of raw materials is evaluated on a  LIFO basis. </strong><br>
                        <strong>Raw materials do not incur warehouse costs. </strong><br>
                        <strong>The decision must be rounded to the nearest  hundred.</strong></p>
                        <p><strong>&nbsp;</strong></p>
                        <p><strong>Purchase of finished goods&lt;/strong&gt;</strong><br>
                        <strong>Their quality, the price at which they may be  bought, and any discounts for quantity purchases, are shown in The News for the  period. </strong><br>
                        <strong>Bought-in finished goods are variable in  quality. </strong><br>
                        <strong>The decision must be rounded to the nearest  hundred.</strong></p>
                        <p><strong>Production Levels</strong><br>
                        <strong>The number of finished goods to be produced  and/or purchased from external suppliers has to be decided. &nbsp;The production hours required to make the  products differ with each product category. The production hours required  reduce as a consequence of your investment in automation technology. See under  Automation Technology. </strong><br>
                        <strong>Production levels are constrained by raw  material supplies and the maximum number of hours of production capacity  available. The maximum capacity is shown in the Management Report under  Machines (and, of course, in the Operations Office). The cost of production  includes labour and is equal for all companies. Inflation increases the cost of  production. </strong></p>
                        <p><strong>Investment in New Production Capacity</strong><br>
                        <strong>Manufacturing capacity can be bought or leased:</strong><br>
                        <strong>A new machine with a capacity of 1500 production  hours in the first quarter will cost 250,000E. New machines do not come on line  until the quarter after purchase. Bought machines suffer a depreciation of 1/12  each quarter in terms of both value and production capacity based on original  values. &nbsp;Leased machines may be used  immediately and suffer 1/12 depreciation each quarter in terms of production  capacity based on original values. Costs are shown in the News.</strong></p>
                        <p><strong>Sale of Production Capacity</strong><br>
                        <strong>If it becomes necessary or desirable to reduce  production capacity, machines may be sold at 50% of current depreciated value,  not exceeding the capacity of owned machines. The oldest machines are sold  first. It is not possible to sell machines which will enter in the last quarter  of life. Cash realized from the sale of plant is not received until the  beginning of the quarter following sale. &nbsp;To reduce the number of machines type a  negative figure. &nbsp;When machines have  ended their 3-year-life they are no longer available for use (it is not  necessary to put a negative figure !)</strong></p>
                        <p><strong>Automation technology investment</strong><br>
                        <strong>Investment in AT will raise the overall quality  of your products. It will also improve the quality of process by reducing the  production time needed, thereby having the effect of increasing your production  capacity. </strong><br>
                        <strong>The benefits are as follows: &nbsp;The quality of your product is enhanced.</strong><br>
                        <strong>The production hours required is reduced,  thereby increasing your production capacity.</strong><br>
                        <strong>The effect of investment is immediate and  cumulative.</strong> </p>
                        <p>&nbsp;</p>"
    End Sub

#End Region

End Class
