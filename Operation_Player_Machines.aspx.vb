﻿Imports System.Data
Imports APS.APSUtility
Imports APPCore.Utility
Imports DALC4NET
Imports Telerik.Web.UI

Partial Class Operation_Player_Machines
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster
    Private m_IDPeriodoMax As Integer

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        Message.Visible = False
        LoadData()
        UpdatePanelMain.Update()
    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        Message.Visible = False
        LoadData()
        UpdatePanelMain.Update()
    End Sub

    Private Sub Operation_Player_Machines_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        m_SessionData = Session("SessionData")

        m_SiteMaster = Me.Master

        m_SiteMaster.LoadPeriodsValid = Session("IDRole") = "P"

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oCBOPeriod As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOPeriodValid As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriodValid"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler m_SiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler m_SiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(Session("IDGame")) ' Ultimo periodo
        oCBOPeriod.SelectedValue = Session("IDPeriodValid")
        m_SiteMaster.PeriodChange = Session("IDPeriodValid")

        Session("NameTeam") = GetTeamName(Session("IDTeam"))
        oCBOTeam.SelectedValue = Session("IDTeam")
        m_SiteMaster.TeamChange = Session("IDTeam")

        ' Gestione del pannello delle decisioni concesse
        modalHelp.OpenerElementID = lnkHelp.ClientID
        modalHelp.Modal = False
        modalHelp.VisibleTitlebar = True

        LoadHelp()
    End Sub

    Private Sub LoadData()
        LoadDataProduction()
        tblMachines.Rows.Clear()
        tblTimeOfProduction.Rows.Clear()
        tblProductionCapacity.Rows.Clear()

        LoadDataMachines()
        LoadDataTimeOfProduction()
        LoadDataProductionCapacity()
    End Sub

    Private Sub LoadDataProduction()
        Dim LoansRequi, ActuaMaximLoans, LoansLevel, BankLevel, LoansAllowVar, PaymeToCover, DisinAllowPerio, CA1, CA2, CA3 As Double

        '/* CALCOLO PRESTITI DEL PERIODO */
        LoansRequi = GetDecisionPlayers("LoansRequi", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "", Session("IDGame"))
        ActuaMaximLoans = GetVariableState("ActuaMaximLoans", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "", Session("IDGame"))
        LoansLevel = GetVariableState("LoansLevel", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "", Session("IDGame"))
        BankLevel = GetVariableState("BankLevel", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "", Session("IDGame"))
        If (LoansRequi >= 0) Then
            LoansAllowVar = Math.Max(0, Math.Min(LoansRequi, ActuaMaximLoans - LoansLevel))
        Else
            LoansAllowVar = Math.Max(Math.Max(-BankLevel, LoansRequi), -LoansLevel)
        End If


        '/* CALCOLO DISINVESTIMENTI DEL PERIODO  */
        Dim TotalPointPerso As Double = GetVariableState("TotalPointPerso", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "", Session("IDGame"))
        Dim NewPersoPoint As Double = GetDecisionPlayers("NewPersoPoint", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "", Session("IDGame"))
        Dim NewCostPoint As Double = GetVariableDefineValue("NewCostPoint", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetNext, Session("IDGame"))
        Dim TotalStaffPerso As Double = GetVariableState("TotalStaffPerso", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "", Session("IDGame"))
        Dim NewPersoStaff As Double = GetDecisionPlayers("NewPersoStaff", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "", Session("IDGame"))
        Dim NewCostStaff As Double = GetVariableDefineValue("NewCostStaff", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetNext, Session("IDGame"))

        DisinAllowPerio = -Math.Min(0, Math.Max(-0.2 * TotalPointPerso, NewPersoPoint) * NewCostPoint * 2) _
                        - Math.Min(0, Math.Max(-0.2 * TotalStaffPerso, NewPersoStaff) * NewCostStaff * 2)

        '/* CALCOLO PAGAMENTI ANTICIPATI DA COPRIRE  */
        Dim TotalCentrStore As Double = GetVariableState("TotalCentrStore", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "", Session("IDGame"))
        Dim RentPerioCentr As Double = GetVariableDefineValue("RentPerioCentr", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetNext, Session("IDGame"))
        Dim TotalPerifStore As Double = GetVariableState("TotalPerifStore", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "", Session("IDGame"))
        Dim RentPerioPerif As Double = GetVariableDefineValue("RentPerioPerif", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetNext, Session("IDGame"))
        Dim GlobaActuaLease As Double = GetVariableDefineValue("GlobaActuaLease", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetNext, Session("IDGame"))
        Dim NewLeaseCost As Double = GetVariableDefineValue("NewLeaseCost", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetNext, Session("IDGame"))

        Dim TaxPayment As Double = GetVariableDefineValue("TaxPayment", Session("IDTeam"), 0, 0, m_SiteMaster.PeriodGetNext, Session("IDGame"))

        PaymeToCover = TotalCentrStore * RentPerioCentr _
                     + TotalPerifStore * RentPerioPerif _
                     + TotalPointPerso * NewCostPoint _
                     + TotalStaffPerso * NewCostStaff _
                     + GlobaActuaLease * NewLeaseCost

        CA1 = BankLevel + LoansAllowVar + DisinAllowPerio - PaymeToCover - TaxPayment

        Dim LeaseCashFinan, PerifCashFinan, CentrStoreFinan, VehicCashFinan, PersoStaffFinan, PersoPointFinan, PlantCashFinan, AutomFinan, MktgFinan As Double

        Dim LeaseRequi As Double = GetDecisionPlayers("LeaseRequi", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "", Session("IDGame"))
        Dim DelayPaymeLease As Double = GetDecisionPlayers("DelayPaymeLease", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "", Session("IDGame"))
        Dim RicarTermiMese As Double = GetVariableBoss("RicarTermiMese", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "", Session("IDGame"))
        Dim DiritRitarPagam As Double = GetVariableBoss("DiritRitarPagam", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "", Session("IDGame"))
        Dim TechnAutomRequi As Double = GetDecisionPlayers("TechnAutomRequi", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "", Session("IDGame"))
        Dim ForwaPaymeAutom As Double = GetDecisionPlayers("ForwaPaymeAutom", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "", Session("IDGame"))
        Dim DiscoTermiMese As Double = GetVariableBoss("DiscoTermiMese", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "", Session("IDGame"))
        Dim DiritScontPagam As Double = GetVariableBoss("DiritScontPagam", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "", Session("IDGame"))
        Dim TrainServiRequi As Double = GetDecisionPlayers("TrainServiRequi", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "", Session("IDGame"))
        Dim AdverRequi As Double = GetDecisionPlayers("AdverRequi", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "", Session("IDGame"))
        Dim ForwaPaymeMktg As Double = GetDecisionPlayers("ForwaPaymeMktg", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "", Session("IDGame"))
        Dim PlantRequi As Double = GetDecisionPlayers("PlantRequi", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "", Session("IDGame"))
        Dim VehicRequi As Double = GetDecisionPlayers("VehicRequi", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "", Session("IDGame"))
        Dim NewVehicCost As Double = GetVariableBoss("NewVehicCost", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "", Session("IDGame"))
        Dim PerifStoreRequi As Double = GetVariableBoss("PerifStoreRequi", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "", Session("IDGame"))
        Dim CentrStoreRequi As Double = GetDecisionPlayers("CentrStoreRequi", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "", Session("IDGame"))

        LeaseCashFinan = LeaseCashFinan + LeaseRequi * NewLeaseCost * ((3 - DelayPaymeLease) / 3) * ((100 + DelayPaymeLease * RicarTermiMese * DiritRitarPagam) / 100)

        AutomFinan = TechnAutomRequi * ((100 - ForwaPaymeAutom * DiscoTermiMese * DiritScontPagam) / 100) * ((0 + ForwaPaymeAutom) / 3)

        MktgFinan = (TrainServiRequi + AdverRequi) * ((100 - ForwaPaymeMktg * DiscoTermiMese * DiritScontPagam) / 100) * ((0 + ForwaPaymeMktg) / 3)

        PlantCashFinan = PlantRequi

        VehicCashFinan = VehicRequi * NewVehicCost

        If PerifStoreRequi > 0 Then
            PerifCashFinan += PerifStoreRequi * RentPerioPerif
        End If

        If CentrStoreRequi > 0 Then
            CentrStoreFinan += CentrStoreRequi * RentPerioCentr
        End If

        If NewPersoStaff > 0 Then
            PersoStaffFinan += NewPersoStaff * NewCostStaff
        End If

        If NewPersoPoint > 0 Then
            PersoPointFinan += NewPersoPoint * NewCostPoint
        End If

        Dim ProduCashFinan, PurchNicFinan, PurchEuropFinan, PromoToFinan, ExtraProduFinan As Double

        ProduCashFinan = 0
        PurchNicFinan = 0
        PurchEuropFinan = 0
        PromoToFinan = 0
        ExtraProduFinan = 0

        ' Ciclo sui prodotti per calcolare i dati corretti
        Dim oDTItems As DataTable = LoadItemsGame(Session("IDGame"), Nz(Session("LanguageActive")))
        Dim oDTPlayers As DataTable = LoadTeamsGame(Session("IDGame"))
        Dim ProduBasicRequi As Double
        Dim ProduUnitCost As Double
        Dim DelayPaymeProdu As Double
        Dim PurchNICRequi As Double
        Dim RawNICCost As Double
        Dim PurchEuropRequi As Double
        Dim RawEuropCost As Double
        Dim DelayPaymeEurop As Double
        Dim MarkeExpenRequi As Double
        Dim DelayPaymePromo As Double
        Dim ForwaPaymeExtra As Double
        Dim ExtraRequi As Double
        Dim ThresDiscoExtra As Double
        Dim ExtraProduCost As Double
        Dim DiscoLevelExtra As Double

        For Each oRowItm As DataRow In oDTItems.Rows
            ProduBasicRequi = GetDecisionPlayers("ProduBasicRequi", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "var_" & GetTeamName(Session("IDTeam")) & "_ProduBasicRequi_" & oRowItm("VariableNameDefault"), Session("IDGame"))
            ProduUnitCost = GetVariableBoss("ProduUnitCost", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "ProduUnitCost_" & Nz(oRowItm("VariableNameDefault")), Session("IDGame"))
            DelayPaymeProdu = GetDecisionPlayers("DelayPaymeProdu", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "", Session("IDGame"))

            PurchNICRequi = GetDecisionPlayers("PurchNICRequi", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "var_" & GetTeamName(Session("IDTeam")) & "_PurchNICRequi_" & oRowItm("VariableNameDefault"), Session("IDGame"))
            RawNICCost = GetVariableBoss("RawNICCost", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "RawNICCost_" & Nz(oRowItm("VariableNameDefault")), Session("IDGame"))

            PurchEuropRequi = GetDecisionPlayers("PurchEuropRequi", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "var_" & GetTeamName(Session("IDTeam")) & "_PurchEuropRequi_" & oRowItm("VariableNameDefault"), Session("IDGame"))
            RawEuropCost = GetVariableBoss("RawEuropCost", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "RawEuropCost_" & Nz(oRowItm("VariableNameDefault")), Session("IDGame"))
            DelayPaymeEurop = GetDecisionPlayers("DelayPaymeEurop", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "", Session("IDGame"))

            MarkeExpenRequi = GetDecisionPlayers("MarkeExpenRequi", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "var_" & GetTeamName(Session("IDTeam")) & "_MarkeExpenRequi_" & oRowItm("VariableNameDefault"), Session("IDGame"))
            DelayPaymePromo = GetDecisionPlayers("DelayPaymePromo", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "", Session("IDGame"))
            ForwaPaymeExtra = GetDecisionPlayers("ForwaPaymeExtra", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "", Session("IDGame"))
            ExtraRequi = GetDecisionPlayers("ExtraRequi", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "", Session("IDGame"))
            ThresDiscoExtra = GetVariableBoss("ThresDiscoExtra", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "ThresDiscoExtra_" & Nz(oRowItm("VariableNameDefault")), Session("IDGame"))
            ExtraProduCost = GetVariableBoss("ExtraProduCost", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "ExtraProduCost_" & Nz(oRowItm("VariableNameDefault")), Session("IDGame"))
            DiscoLevelExtra = GetVariableBoss("DiscoLevelExtra", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "", Session("IDGame"))

            ProduCashFinan += ProduBasicRequi * ProduUnitCost * ((3 - DelayPaymeProdu) / 3) * ((100 + DelayPaymeProdu * RicarTermiMese * DiritRitarPagam) / 100)
            PurchNicFinan += (PurchNICRequi * RawNICCost) * (2 / 3)
            PurchEuropFinan += (PurchEuropRequi * RawEuropCost) * ((2 - DelayPaymeEurop) / 3) * ((100 + DelayPaymeEurop * RicarTermiMese * DiritRitarPagam) / 100)
            PromoToFinan += MarkeExpenRequi * ((3 - DelayPaymePromo) / 3) * ((100 + DelayPaymePromo * RicarTermiMese * DiritRitarPagam) / 100)
            ExtraProduFinan += ((100 - ForwaPaymeExtra * DiscoTermiMese * DiritScontPagam) / 100) * ((0 + ForwaPaymeExtra) / 3) _
                             * (Math.Min(ExtraRequi, ThresDiscoExtra) * ExtraProduCost + (Math.Max(0, ExtraRequi - ThresDiscoExtra) *
                             ExtraProduCost * (100 - DiscoLevelExtra) / 100))
        Next

        CA2 = ProduCashFinan + LeaseCashFinan + PerifCashFinan + PurchNicFinan + PurchEuropFinan + CentrStoreFinan _
            + VehicCashFinan + PersoStaffFinan + PersoPointFinan + PromoToFinan + MktgFinan + AutomFinan + ExtraProduFinan

        If (CA1 >= CA2 Or CA2 = 0) Then
            CA3 = 100
        Else
            CA3 = Math.Max(0, Math.Min(CA1 / CA2 * 100, 100))
        End If
        Session("CA3") = (Math.Floor(CA3 * 10) / 10).ToString("n1") + "%"

        Dim PR1, PR2, PR3, CP1, CP2, CP3, CP4 As Double

        ' Capacità produttiva da nuovi leasing (decisione)
        CP1 = Math.Round(LeaseRequi * CA3 / 100, 0) * 6000 * (3 / 12)

        ' Capacità produttiva da macchine di proprietà acquistate
        CP2 = GetVariableState("PlantPerAge", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "var_" & GetTeamName(Session("IDTeam")) & "_PlantPerAge_1", Session("IDGame"), 1) * 6000 * (3 / 12) _
            + GetVariableState("PlantPerAge", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "var_" & GetTeamName(Session("IDTeam")) & "_PlantPerAge_2", Session("IDGame"), 2) * 5500 * (3 / 12) _
            + GetVariableState("PlantPerAge", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "var_" & GetTeamName(Session("IDTeam")) & "_PlantPerAge_3", Session("IDGame"), 3) * 5000 * (3 / 12) _
            + GetVariableState("PlantPerAge", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "var_" & GetTeamName(Session("IDTeam")) & "_PlantPerAge_4", Session("IDGame"), 4) * 4500 * (3 / 12) _
            + GetVariableState("PlantPerAge", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "var_" & GetTeamName(Session("IDTeam")) & "_PlantPerAge_5", Session("IDGame"), 5) * 4000 * (3 / 12) _
            + GetVariableState("PlantPerAge", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "var_" & GetTeamName(Session("IDTeam")) & "_PlantPerAge_6", Session("IDGame"), 6) * 3500 * (3 / 12) _
            + GetVariableState("PlantPerAge", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "var_" & GetTeamName(Session("IDTeam")) & "_PlantPerAge_7", Session("IDGame"), 7) * 3000 * (3 / 12) _
            + GetVariableState("PlantPerAge", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "var_" & GetTeamName(Session("IDTeam")) & "_PlantPerAge_8", Session("IDGame"), 8) * 2500 * (3 / 12) _
            + GetVariableState("PlantPerAge", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "var_" & GetTeamName(Session("IDTeam")) & "_PlantPerAge_9", Session("IDGame"), 9) * 2000 * (3 / 12) _
            + GetVariableState("PlantPerAge", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "var_" & GetTeamName(Session("IDTeam")) & "_PlantPerAge_10", Session("IDGame"), 10) * 1500 * (3 / 12) _
            + GetVariableState("PlantPerAge", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "var_" & GetTeamName(Session("IDTeam")) & "_PlantPerAge_11", Session("IDGame"), 11) * 1000 * (3 / 12) _
            + GetVariableState("PlantPerAge", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "var_" & GetTeamName(Session("IDTeam")) & "_PlantPerAge_12", Session("IDGame"), 12) * 500 * (3 / 12)

        'Capacità produttiva da leasing attivi 
        CP3 = GetVariableState("LeasePerAge", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "var_" & GetTeamName(Session("IDTeam")) & "_LeasePerAge_1", Session("IDGame"), 1) * 5500 * (3 / 12) _
            + GetVariableState("LeasePerAge", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "var_" & GetTeamName(Session("IDTeam")) & "_LeasePerAge_2", Session("IDGame"), 2) * 5000 * (3 / 12) _
            + GetVariableState("LeasePerAge", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "var_" & GetTeamName(Session("IDTeam")) & "_LeasePerAge_3", Session("IDGame"), 3) * 4500 * (3 / 12)

        'Capacità produttiva da macchine di proprietà dismesse
        Dim Rimanenti, Macchinari, Dismessi As Double

        Rimanenti = -Math.Min(0, Nn(GetDecisionPlayers("PlantRequi", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "", Session("IDGame"))))

        CP4 = 0
        Dim iIndex As Integer
        For iIndex = 11 To 1 Step -1
            Macchinari = GetVariableDefineValue("PlantRequi", Session("IDTeam"), 0, iIndex, m_SiteMaster.PeriodGetNext, Session("IDGame"))
            Dismessi = Math.Min(Macchinari, Rimanenti)
            Rimanenti -= Dismessi
            CP4 -= Dismessi * (1500 - (iIndex) * 125)
        Next iIndex

        PR1 = CP1 + CP2 + CP3 + CP4
        Session("CP1") = CP1.ToString("n0")
        Session("CP2") = CP2.ToString("n0")
        Session("CP3") = CP3.ToString("n0")
        Session("CP4") = CP4.ToString("n0")
        Session("PR1") = PR1.ToString("n0")

        For Each oRowItm As DataRow In oDTItems.Rows
            PR2 = PR2 + Nn(GetVariableBoss("HoursPerItem", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "var_" & m_SiteMaster.TeamNameGet & "_HoursPerItem_" & oRowItm("VariableNameDefault"), Session("IDGame"))) _
                * Math.Min(Nn(GetDecisionPlayers("ProduBasicRequi", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "var_" & m_SiteMaster.TeamNameGet & "_ProduBasicRequi_" & oRowItm("VariableNameDefault"), Session("IDGame"))) * CA3 / 100,
                          Nn(GetDecisionPlayers("PurchEuropRequi", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "var_" & m_SiteMaster.TeamNameGet & "_PurchEuropRequi_" & oRowItm("VariableNameDefault"), Session("IDGame"))) * CA3 / 100 _
                + Nn(GetDecisionPlayers("PurchNICRequi", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "var_" & m_SiteMaster.TeamNameGet & "_PurchNICRequi_" & oRowItm("VariableNameDefault"), Session("IDGame"))) * (2 / 3) * CA3 / 100 _
                + Nn(GetVariableState("StockRawMater", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "var_" & m_SiteMaster.TeamNameGet & "_StockRawMater_" & oRowItm("VariableNameDefault"), Session("IDGame")))) _
                + Nn(GetVariableState("PurchDelivNIC", Session("IDTeam"), m_SiteMaster.PeriodGetNext, "var_" & m_SiteMaster.TeamNameGet & "_PurchDelivNIC_" & oRowItm("VariableNameDefault"), Session("IDGame")))
        Next


        Session("PR2") = PR2.ToString("n0")

        If (PR1 = 0) Then
            PR3 = 100
        Else
            PR3 = Math.Min(100, PR2 / PR1 * 100)
        End If

        ' MODIFICA: ARROTONDA PER DIFETTO, PER EVITARE CHE UN VALORE >99,5 SIA VIUSALIZZATO COME 100
        Session("PR3") = (Math.Floor(PR3 * 10) / 10).ToString("n1")
    End Sub

    Private Sub LoadDataMachines()
        Dim oDTAge As DataTable = HandleLoadAgeData(Session("IDGame"))
        Dim iNumQuarter As Integer = 0

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Dim iTotVehicles As Integer
        Dim dMoltiplicatore As Integer = 6000

        Try
            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header vuota
            Dim oLBLTitleGrid As New RadLabel
            oLBLTitleGrid.ID = "lblGridTitle"
            oLBLTitleGrid.Text = "Machines" & "<br/>" & "(operative machines in the previous quarter)"
            oLBLTitleGrid.ForeColor = Drawing.Color.White
            tblHeaderCell.Controls.Add(oLBLTitleGrid)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.ColumnSpan = 4
            tblHeaderCell.Style.Add("background", "#525252")

            tblHeaderRow.BorderStyle = BorderStyle.None
            tblHeaderRow.Style.Add("margin-top", "10px")
            tblHeaderRow.Cells.Add(tblHeaderCell)
            tblMachines.Rows.Add(tblHeaderRow)

            ' Prima riga di intestazione
            tblCell = New TableCell
            tblRow = New TableRow

            Dim oLBLAgeQuarters As New RadLabel
            oLBLAgeQuarters.ID = "lblAgeQuarters"
            oLBLAgeQuarters.Text = "Age (3 months)"
            oLBLAgeQuarters.ForeColor = Drawing.Color.White
            tblCell.Controls.Add(oLBLAgeQuarters)
            tblCell.Style.Add("width", "25%")
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("background", "#3e6c86")
            tblCell.Style.Add("padding-left", "5px")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLBLOperativeOwnedMachines As New RadLabel
            oLBLOperativeOwnedMachines.ID = "lblOperativeOwnedMachines"
            oLBLOperativeOwnedMachines.Text = "Operative owned machines"
            oLBLOperativeOwnedMachines.ForeColor = Drawing.Color.White
            tblCell.Controls.Add(oLBLOperativeOwnedMachines)
            tblCell.Style.Add("width", "25%")
            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("background", "#3e6c86")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLBLLeasedMachines As New RadLabel
            oLBLLeasedMachines.ID = "lblLeasedMachines"
            oLBLLeasedMachines.Text = "Leased machines"
            oLBLLeasedMachines.ForeColor = Drawing.Color.White
            tblCell.Controls.Add(oLBLLeasedMachines)
            tblCell.Style.Add("width", "25%")
            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("background", "#3e6c86")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLBLCapacityProduction As New RadLabel
            oLBLCapacityProduction.ID = "lblCapacityProduction"
            oLBLCapacityProduction.Text = "Production capacity"
            oLBLCapacityProduction.ForeColor = Drawing.Color.White
            tblCell.Controls.Add(oLBLCapacityProduction)
            tblCell.Style.Add("width", "25%")
            tblCell.Style.Add("text-align", "right")
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("background", "#3e6c86")
            tblRow.Cells.Add(tblCell)

            tblMachines.Rows.Add(tblRow)

            Dim iIDQuarter As Integer = 1

            For Each oRowAge As DataRow In oDTAge.Rows
                ' Righe dati
                Dim oLBLQuarter As New RadLabel

                oLBLQuarter.ID = "Q" & iNumQuarter

                tblRow = New TableRow
                tblCell = New TableCell
                iNumQuarter += 3

                ' Conto gli anni
                Dim iTotaleQuarter As Double = iNumQuarter / 12

                Select Case iTotaleQuarter
                    Case 1
                        oLBLQuarter.Text = "(1 year)" & " " & iNumQuarter

                    Case 2
                        oLBLQuarter.Text = "(2 years)" & " " & iNumQuarter

                    Case 3
                        oLBLQuarter.Text = "(3 years)" & " " & iNumQuarter

                    Case 4
                        oLBLQuarter.Text = "(4 years)" & " " & iNumQuarter

                    Case 5
                        oLBLQuarter.Text = "(5 years)" & " " & iNumQuarter

                    Case 6
                        oLBLQuarter.Text = "(6 years)" & " " & iNumQuarter

                    Case 7
                        oLBLQuarter.Text = "(7 years)" & " " & iNumQuarter

                    Case 8
                        oLBLQuarter.Text = "(8 years)" & " " & iNumQuarter

                    Case 9
                        oLBLQuarter.Text = "(9 years)" & " " & iNumQuarter

                    Case 10
                        oLBLQuarter.Text = "(10 years)" & " " & iNumQuarter

                    Case Else
                        oLBLQuarter.Text = iNumQuarter
                End Select
                oLBLQuarter.ForeColor = Drawing.Color.Brown
                oLBLQuarter.Style.Add("font-size", "8pt")
                tblCell.Controls.Add(oLBLQuarter)
                tblCell.BorderStyle = BorderStyle.None
                tblCell.Style.Add("padding-left", "5px")
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                Dim oLBLTotalPlant As New RadLabel
                oLBLTotalPlant.Text = Nn(GetVariableDefineValue("TotalPlant", Session("IDTeam"), 0, oRowAge("AgeDescription"), Session("IDPeriod"), Session("IDGame"))).ToString("N0")
                tblCell.Controls.Add(oLBLTotalPlant)
                tblCell.BorderStyle = BorderStyle.None
                tblCell.Style.Add("text-align", "right")
                tblRow.Cells.Add(tblCell)

                iTotVehicles += Nni(oLBLTotalPlant.Text)

                tblCell = New TableCell
                If iNumQuarter <= 12 Then
                    Dim oLBLLeasedMachinesValue As New RadLabel
                    oLBLLeasedMachinesValue.Text = Nn(GetVariableDefineValue("TotalLease", Session("IDTeam"), 0, oRowAge("AgeDescription"), Session("IDPeriod"), Session("IDGame"))).ToString("N0")
                    tblCell.Controls.Add(oLBLLeasedMachinesValue)
                    tblCell.BorderStyle = BorderStyle.None
                    tblCell.Style.Add("text-align", "right")
                Else
                    tblCell.Text = ""
                    tblCell.BorderStyle = BorderStyle.None
                    tblCell.Style.Add("text-align", "right")
                End If
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                Dim oLBLProductionCapacityValue As New RadLabel
                If iNumQuarter <= 12 Then
                    oLBLProductionCapacityValue.Text = (Nn(GetVariableDefineValue("TotalPlant", Session("IDTeam"), 0, oRowAge("AgeDescription"), Session("IDPeriod"), Session("IDGame")) _
                                        + Nn(GetVariableDefineValue("TotalLease", Session("IDTeam"), 0, oRowAge("AgeDescription"), Session("IDPeriod"), Session("IDGame")))) _
                                        * dMoltiplicatore * 3 / 12).ToString("N0") & "h"
                Else
                    oLBLProductionCapacityValue.Text = (Nn(GetVariableDefineValue("TotalPlant", Session("IDTeam"), 0, oRowAge("AgeDescription"), Session("IDPeriod"), Session("IDGame"))) * dMoltiplicatore * 3 / 12).ToString("N0")
                End If
                tblCell.Controls.Add(oLBLProductionCapacityValue)
                tblCell.BorderStyle = BorderStyle.None
                tblCell.Style.Add("text-align", "right")
                tblRow.Cells.Add(tblCell)

                dMoltiplicatore -= 500

                tblMachines.Rows.Add(tblRow)
            Next

            ' Riga di riepilogo
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLBLTotVehicles As New RadLabel
            oLBLTotVehicles.Text = "Total"
            oLBLTotVehicles.Style.Add("font-size", "8pt")
            oLBLTotVehicles.Style.Add("text-align", "left")
            oLBLTotVehicles.Style.Add("font-weight", "bold")
            oLBLTotVehicles.Style.Add("color", "#ffffff")
            tblCell.Controls.Add(oLBLTotVehicles)
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("padding-left", "5px")
            tblCell.Style.Add("background", "#3e6c86")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLBLTotVehiclesNum As New RadLabel
            oLBLTotVehiclesNum.Text = iTotVehicles.ToString("N0")
            oLBLTotVehiclesNum.Style.Add("font-size", "8pt")
            oLBLTotVehiclesNum.Style.Add("text-align", "right")
            oLBLTotVehiclesNum.Style.Add("font-weight", "bold")
            oLBLTotVehiclesNum.Style.Add("color", "#ffffff")
            tblCell.Controls.Add(oLBLTotVehiclesNum)
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("padding-left", "20px")
            tblCell.Style.Add("background", "#3e6c86")
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            Dim oLBLLeasedMachinesTot As New RadLabel
            oLBLLeasedMachinesTot.Text = ""
            oLBLLeasedMachinesTot.Style.Add("font-size", "8pt")
            oLBLLeasedMachinesTot.Style.Add("text-align", "right")
            oLBLLeasedMachinesTot.Style.Add("font-weight", "bold")
            oLBLLeasedMachinesTot.Style.Add("color", "#ffffff")
            tblCell.Controls.Add(oLBLLeasedMachinesTot)
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("padding-left", "20px")
            tblCell.Style.Add("background", "#3e6c86")
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell

            Dim lnkTotalCapacProdu As New HyperLink
            lnkTotalCapacProdu.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=TotalCapacProdu&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            lnkTotalCapacProdu.Style.Add("text-decoration", "none")
            lnkTotalCapacProdu.Style.Add("cursor", "pointer")
            lnkTotalCapacProdu.Style.Add("color", "white")

            lnkTotalCapacProdu.Text = Nn(GetVariableDefineValue("TotalCapacProdu", Session("IDTeam"), 0, 0, Session("IDPeriod"), Session("IDGame"))).ToString("N0")

            tblCell.Controls.Add(lnkTotalCapacProdu)

            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("padding-left", "20px")
            tblCell.Style.Add("background", "#3e6c86")
            tblCell.Style.Add("text-align", "right")

            tblRow.Cells.Add(tblCell)

            tblMachines.Rows.Add(tblRow)
        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub LoadDataTimeOfProduction()
        Dim oDTItems As DataTable = HandleGetItemsPeriodVariables(Session("IDGame"), Nz(Session("LanguageActive")))

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Try
            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header vuota
            Dim oLBLTitleGrid As New RadLabel
            oLBLTitleGrid.ID = "lblGridTitleTimeOfProduction"
            oLBLTitleGrid.Text = "Time of production (hours)"
            oLBLTitleGrid.ForeColor = Drawing.Color.White
            tblHeaderCell.Controls.Add(oLBLTitleGrid)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.ColumnSpan = 2
            tblHeaderCell.Style.Add("background", "#525252")

            tblHeaderRow.BorderStyle = BorderStyle.None
            tblHeaderRow.Style.Add("margin-top", "10px")
            tblHeaderRow.Cells.Add(tblHeaderCell)
            tblTimeOfProduction.Rows.Add(tblHeaderRow)

            ' Ciclo sugli item per la costruzione delle righe
            For Each oRowItm As DataRow In oDTItems.Rows
                tblRow = New TableRow
                tblCell = New TableCell
                Dim oLBLItemName As New RadLabel
                oLBLItemName.ID = "lblItem_" & oRowItm("VariableName")
                oLBLItemName.Text = oRowItm("VariableName")
                oLBLItemName.Style.Add("color", "darkred")
                tblCell.Controls.Add(oLBLItemName)
                tblCell.Style.Add("width", "75%")
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell

                Dim lnkHoursPerItem As New HyperLink
                lnkHoursPerItem.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=HoursPerItem&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                lnkHoursPerItem.Style.Add("text-decoration", "none")
                lnkHoursPerItem.Style.Add("cursor", "pointer")

                lnkHoursPerItem.Text = Nn(GetVariableBoss("HoursPerItem", 0, m_SiteMaster.PeriodGetCurrent, "var_" & m_SiteMaster.TeamNameGet & "_HoursPerItem_" & oRowItm("VariableNameDefault"), Session("IDGame"))).ToString("N2") & "h"
                tblCell.Controls.Add(lnkHoursPerItem)
                tblCell.Style.Add("text-align", "right")

                tblRow.Cells.Add(tblCell)

                tblTimeOfProduction.Rows.Add(tblRow)
            Next
        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub LoadDataProductionCapacity()
        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Try
            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header vuota
            Dim oLBLTitleGrid As New RadLabel
            oLBLTitleGrid.ID = "lblGridTitleProductionCapacity"
            oLBLTitleGrid.Text = "Production capacity for the current decisions"
            oLBLTitleGrid.ForeColor = Drawing.Color.White
            tblHeaderCell.Controls.Add(oLBLTitleGrid)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.ColumnSpan = 2
            tblHeaderCell.Style.Add("background", "#525252")

            tblHeaderRow.BorderStyle = BorderStyle.None
            tblHeaderRow.Style.Add("margin-top", "10px")
            tblHeaderRow.Cells.Add(tblHeaderCell)
            tblProductionCapacity.Rows.Add(tblHeaderRow)

            ' Ciclo sugli item per la costruzione delle righe

            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLBLHourlyGlobalCapacity As New RadLabel
            oLBLHourlyGlobalCapacity.ID = "lblHourlyGlobalCapacity"
            oLBLHourlyGlobalCapacity.Text = "Hourly global capacity "
            oLBLHourlyGlobalCapacity.Style.Add("color", "darkred")
            tblCell.Controls.Add(oLBLHourlyGlobalCapacity)
            tblCell.Style.Add("width", "75%")
            tblRow.Cells.Add(tblCell)

            tblCell = New TableCell
            tblCell.Text = Nn(Session("PR1")).ToString("N2")
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)

            tblProductionCapacity.Rows.Add(tblRow)

        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub Operation_Player_Machines_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        If Not Page.IsPostBack Then
            If Session("IDRole") = "P" Then
                m_SiteMaster.ReloadPeriodsCalculate()
            End If
            LoadData()

            btnTranslate.Visible = Session("IDRole").Contains("D")
        End If

        ' Controllo tutti gli oggetti presenti
        Dim oTranslation As New TranslationObjectForm(Me, g_DAL, Nz(Session("LanguageActive")))

    End Sub

    Private Sub btnTranslate_Click(sender As Object, e As EventArgs) Handles btnTranslate.Click
        Session("PageOrigin") = Me.ToString().Substring(4, Me.ToString().Substring(4).Length - 5) + ".aspx"
        Response.Redirect("PageTranslation.aspx")
        Response.End()
    End Sub

#Region "PANNELLO HELP"

    Private Sub LoadHelp()
        lblHelp.Text = "<p><strong>Hourly global capacity </strong><br>
                          <strong>It shows the hourly capacity of production for  the next quarter</strong></p>
                        <p><strong>Production capacity: next quarter</strong><br>
                            <strong>It shows the production capacity of the next quarter  (the one in which the decisions are taken).</strong></p>
                        <p><strong>&nbsp;</strong></p>
                        <p><strong>Time of production per unit</strong><br>
                            <strong>Each of the three product lines needs different  production times. The time needed is reduced by investments in Automation Technology. </strong></p>
                        <p><strong>Age</strong><br>
                            <strong>It shows the number of months of the existing  machines.</strong><br>
                            <strong>Owned machines</strong><br>
                            <strong>The owned machines are divided up according to  their age Owned machines start to produce in the following period. After 12 &nbsp;periods the machine will automatically go out  of the production process.</strong></p>
                        <p><strong>Production capacity</strong><br>
                            <strong>It is the total number of hours based on the  age of the machines</strong></p>
                        <p><strong>Production capacity available. </strong><br>
                            <strong>The production capacity available is calculated  on the existing machines + new leased machines + incoming owned machines (ie.  those ordered in the previous period). This calculation does not include the  loss of production capacity from the sales of existing owned machines.</strong> </p>
                      <p>&nbsp;</p>"
    End Sub

#End Region

End Class
