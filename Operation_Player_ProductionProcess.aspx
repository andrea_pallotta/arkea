﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Operation_Player_ProductionProcess.aspx.vb" Inherits="Operation_Player_ProductionProcess" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="cntMain" ContentPlaceHolderID="MainContent" runat="Server" ClientIDMode="Static">

    <asp:UpdatePanel ID="UpdatePanelMain" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <h2>
                <asp:Label ID="lblWelcome" runat="server" Text="Operation"></asp:Label>
            </h2>

            <div class="clr"></div>

            <div class="divTable">
                <div class="divTableRow">
                    <div class="divTableCell">
                        <h3>
                            <asp:Label ID="lblTitolo" runat="server" Text="Production run"></asp:Label>
                        </h3>
                    </div>
                    <div class="divTableCell" style="text-align: right; font-size: 8pt">
                        <telerik:RadLinkButton ID="lnkHelp" runat="server" Text="Help" EnableAjaxSkinRendering="true" ToolTip="Show help"></telerik:RadLinkButton>
                    </div>
                </div>
            </div>


            <div class="row" style="vertical-align: top">
                <asp:Table ClientIDMode="Static" ID="tblProductions" runat="server" Style="width: 70%; float: left; padding-right: 10px;">
                </asp:Table>

                <asp:Label ID="lblCommento" runat="server" Style="float: right; width: 25%; padding-left: 10px;">
                </asp:Label>
            </div>

            <div class="clr"></div>

            <div class="row" style="min-height: 50px;"></div>

            <div class="row">
                <telerik:RadHtmlChart ID="grfProduction" runat="server" Width="100%">
                    <ChartTitle Text="Average market expenditure"></ChartTitle>
                </telerik:RadHtmlChart>
            </div>

            <div class="clr"></div>

            <div class="row">
                <asp:PlaceHolder runat="server" ID="Message" Visible="false">
                    <p class="text-danger">
                        <asp:Literal runat="server" ID="MessageText" />
                    </p>
                </asp:PlaceHolder>
            </div>

            <telerik:RadWindow RenderMode="Lightweight" ID="modalHelp" runat="server" Width="520px" Height="450px" CenterIfModal="false"
                Style="z-index: 100001;" BorderStyle="None" Behaviors="Close, Move" Title="Help">
                <ContentTemplate>
                    <div style="padding: 10px; text-align: left;">
                        <div id="divTableMaster" runat="server" style="margin-bottom: 10px;">
                            <asp:Label runat="server" ID="lblHelp" Visible="true">

                            </asp:Label>
                        </div>
                    </div>
                </ContentTemplate>
            </telerik:RadWindow>

            <div class="clr"></div>

            <div class="row">
                <div class="divTableCell" style="text-align: right;">
                    <telerik:RadButton ID="btnTranslate" runat="server" Text="Translate" EnableAjaxSkinRendering="true" ToolTip=""></telerik:RadButton>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
