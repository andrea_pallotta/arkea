﻿Imports System.Data
Imports APS.APSUtility
Imports APPCore.Utility
Imports DALC4NET
Imports Telerik.Web.UI

Partial Class Operation_Player_ProductionProcess
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster
    Private m_IDPeriodoMax As Integer

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        Message.Visible = False
        LoadData()
        UpdatePanelMain.Update()
    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        Message.Visible = False
        LoadData()
        UpdatePanelMain.Update()
    End Sub

    Private Sub Operation_Player_ProductionProcess_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        m_SessionData = Session("SessionData")

        m_SiteMaster = Me.Master

        m_SiteMaster.LoadPeriodsValid = Session("IDRole") = "P"

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oCBOPeriod As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOPeriodValid As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriodValid"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler m_SiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler m_SiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(Session("IDGame")) ' Ultimo periodo
        oCBOPeriod.SelectedValue = Session("IDPeriodValid")
        m_SiteMaster.PeriodChange = Session("IDPeriodValid")

        Session("NameTeam") = GetTeamName(Session("IDTeam"))
        oCBOTeam.SelectedValue = Session("IDTeam")
        m_SiteMaster.TeamChange = Session("IDTeam")

        ' Gestione del pannello delle decisioni concesse
        modalHelp.OpenerElementID = lnkHelp.ClientID
        modalHelp.Modal = False
        modalHelp.VisibleTitlebar = True

        LoadHelp()
    End Sub
    Private Sub LoadData()
        tblProductions.Rows.Clear()

        LoadDataProduction()
    End Sub

    Private Sub LoadDataProduction()
        Dim oDTItems As DataTable = HandleGetItemsPeriodVariables(Session("IDGame"), Nz(Session("LanguageActive")))

        Dim iTotNumPlayers As Integer = HandleLoadPlayersGame(Session("IDGame")).Rows.Count

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Dim sNomeVariabileLista As String

        Try
            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header vuota
            tblHeaderCell.Text = ""
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.Style.Add("background", "#ffffff")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            ' Recupero gli Items per la costruzione delle colonne
            For Each oRowItm As DataRow In oDTItems.Rows
                ' Cella del prezzo di vendita
                tblHeaderCell = New TableHeaderCell
                tblHeaderCell.Text = Nz(oRowItm("VariableName"))
                tblHeaderCell.BorderStyle = BorderStyle.None
                tblHeaderRow.Cells.Add(tblHeaderCell)
            Next
            tblHeaderRow.BorderStyle = BorderStyle.None
            tblHeaderRow.Style.Add("margin-top", "10px")
            tblProductions.Rows.Add(tblHeaderRow)

            ' Aggiungo le righe di dettaglio
            ' Riga Production planned
            tblRow = New TableRow
            tblCell = New TableCell

            Dim oLBLProductionPLanned As New RadLabel
            oLBLProductionPLanned.ID = "lblProductionPLanned"
            oLBLProductionPLanned.Text = "Production planned"
            oLBLProductionPLanned.Style.Add("color", "darkred")
            tblCell.Controls.Add(oLBLProductionPLanned)
            tblCell.Style.Add("width", "30%")
            tblRow.Cells.Add(tblCell)

            For Each oRowItm As DataRow In oDTItems.Rows
                tblCell = New TableCell
                sNomeVariabileLista = "var_" & m_SiteMaster.TeamNameGet & "_ProduBasicRequi_" & oRowItm("VariableNameDefault")

                Dim lnkProduBasicRequi As New HyperLink
                lnkProduBasicRequi.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=ProduBasicRequi&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                lnkProduBasicRequi.Style.Add("text-decoration", "none")
                lnkProduBasicRequi.Style.Add("cursor", "pointer")

                lnkProduBasicRequi.Text = Nn(GetDecisionPlayers("ProduBasicRequi", Session("IDTeam"), Session("IDPeriod"), sNomeVariabileLista, Session("IDGame"))).ToString("N0")

                tblCell.Controls.Add(lnkProduBasicRequi)

                tblCell.BorderStyle = BorderStyle.None
                tblCell.Style.Add("text-align", "right")

                tblRow.Cells.Add(tblCell)
            Next
            tblProductions.Rows.Add(tblRow)

            ' Riga Production allowed
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLBLProductionAllowed As New RadLabel
            oLBLProductionAllowed.ID = "lblProductionAllowed"
            oLBLProductionAllowed.Text = "Production allowed"
            oLBLProductionAllowed.Style.Add("color", "darkred")
            tblCell.Controls.Add(oLBLProductionAllowed)
            tblRow.Cells.Add(tblCell)

            For Each oRowItm As DataRow In oDTItems.Rows
                tblCell = New TableCell
                Dim lnkProduAllow As New HyperLink
                lnkProduAllow.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=ProduBasicRequi&II=" & oRowItm("IDVariable") & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                lnkProduAllow.Style.Add("text-decoration", "none")
                lnkProduAllow.Style.Add("cursor", "pointer")

                lnkProduAllow.Text = Nn(GetVariableDefineValue("ProduAllow", Session("IDTeam"), oRowItm("IDVariable"), 0, Session("IDPeriod"), Session("IDGame"))).ToString("N0")

                tblCell.Controls.Add(lnkProduAllow)

                tblCell.BorderStyle = BorderStyle.None
                tblCell.Style.Add("text-align", "right")
                tblRow.Cells.Add(tblCell)
            Next
            tblProductions.Rows.Add(tblRow)

            ' Riga Actual production
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLBLActualProduction As New RadLabel
            oLBLActualProduction.ID = "lblActualProduction"
            oLBLActualProduction.Text = "Actual production"
            oLBLActualProduction.Style.Add("color", "darkred")
            tblCell.Controls.Add(oLBLActualProduction)
            tblRow.Cells.Add(tblCell)

            For Each oRowItm As DataRow In oDTItems.Rows
                tblCell = New TableCell
                Dim lnkProduReque As New HyperLink
                lnkProduReque.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=ProduReque&II=" & oRowItm("IDVariable") & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                lnkProduReque.Style.Add("text-decoration", "none")
                lnkProduReque.Style.Add("cursor", "pointer")

                lnkProduReque.Text = Nn(GetVariableDefineValue("ProduReque", Session("IDTeam"), oRowItm("IDVariable"), 0, Session("IDPeriod"), Session("IDGame"))).ToString("N0")

                tblCell.Controls.Add(lnkProduReque)

                tblCell.BorderStyle = BorderStyle.None
                tblCell.Style.Add("text-align", "right")
                tblRow.Cells.Add(tblCell)
            Next
            tblProductions.Rows.Add(tblRow)

            ' Aggiungo il commento
            Dim TotalHoursRequi As Double = Nn(GetVariableDefineValue("TotalHoursRequi", Session("IDTeam"), 0, 0, Session("IDPeriod"), Session("IDGame"))).ToString("N0")
            Dim PerceProduUsed As Double = Nn(GetVariableDefineValue("PerceProduUsed", Session("IDTeam"), 0, 0, Session("IDPeriod"), Session("IDGame"))).ToString("N0")
            Dim TotalCapacProdu As Double = Nn(GetVariableDefineValue("TotalCapacProdu", Session("IDTeam"), 0, 0, Session("IDPeriod"), Session("IDGame"))).ToString("N0")
            Dim TotalProd As Double = 0

            If (TotalCapacProdu) = 0 Then
                TotalProd = 0
            Else
                TotalProd = TotalHoursRequi / (TotalCapacProdu) * 100
            End If

            Session("TotalProd") = TotalProd.ToString("N2")

            Select Case Session("LanguageActive")
                Case "ITA"
                    lblCommento.Text = "<br>La tua richiesta per la produzione è stata l'" & Session("TotalProd") & "% della propria capacità. "
                    lblCommento.Text &= "(Quando è superiore a 100%, la produzione viene ridotta al limite)."

                Case "ENG"
                    lblCommento.Text = "<br>Your request for the production was "
                    lblCommento.Text &= Session("TotalProd")
                    lblCommento.Text &= " per cent of your own capacity. "
                    lblCommento.Text &= "(When higher than 100 the "
                    lblCommento.Text &= "production is reduced to the limit)."

                Case = "FRA"
                    lblCommento.Text = "Votre demande de production était de " & Session("TotalProd") & "% pour cent de votre propre capacité. " _
                                     & "(Lorsque plus de 100 la production est réduite à la limite)."

                Case = "DEU"
                    lblCommento.Text = "Ihre Anfrage für die Produktion betrug " & Session("TotalProd") & "%  Prozent Ihrer eigenen Kapazität. " _
                                     & "(Bei mehr als 100 wird die Produktion bis an die Grenze reduziert)."
            End Select

            LoadDataGraph()
        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub LoadDataGraph()
        Dim sSQL As String
        Dim oDTItems As DataTable = HandleGetItemsPeriodVariables(Session("IDGame"), Nz(Session("LanguageActive")))
        Dim sItemName As String
        Dim dValue As Double = 0
        Dim iTotNumPlayers As Integer = HandleLoadPlayersGame(Session("IDGame")).Rows.Count

        grfProduction.PlotArea.Series.Clear()
        grfProduction.PlotArea.XAxis.Items.Clear()

        ' Controllo lo step, se è quello che devo giocare mostro i dati al precedente, altrimenti quello corrente

        ' Recupero tutti i periodi generati fino adesso
        sSQL = "SELECT * FROM BGOL_Games_Periods BGP " _
             & "INNER JOIN BGOL_Periods BP ON BGP.IDPeriodo = BP.Id "

        If Session("StepToPlay") = Session("CurrentStep") Then
            sSQL &= "WHERE BGP.IDGame = " & Session("IDGame") & " AND BP.NumStep < " & Session("CurrentStep")
        Else
            sSQL &= "WHERE BGP.IDGame = " & Session("IDGame") & " AND BP.NumStep <= " & Session("CurrentStep")
        End If
        sSQL &= " ORDER BY BP.NumStep "
        Dim oDTPeriodi As DataTable = g_DAL.ExecuteDataTable(sSQL)

        Dim oSeriesData As LineSeries
        For Each oRowItm As DataRow In oDTItems.Rows
            sItemName = oRowItm("VariableName").ToString

            oSeriesData = New LineSeries
            For Each oRowPeriod As DataRow In oDTPeriodi.Rows

                dValue = Nn(GetVariableDefineValue("ProduReque", Session("IDTeam"), oRowItm("IDVariable"), 0, oRowPeriod("IDPeriodo"), Session("IDGame"))).ToString("N0")

                If Not IsNothing(dValue) Then
                    Dim oItem As New SeriesItem
                    oItem.YValue = dValue.ToString("N2")
                    oItem.TooltipValue = Nz(oRowItm("VariableName"))
                    oSeriesData.Items.Add(oItem)
                End If
            Next
            grfProduction.PlotArea.Series.Add(oSeriesData)
            oSeriesData.Name = sItemName
            oSeriesData.VisibleInLegend = True
            oSeriesData.LabelsAppearance.Visible = True
        Next

        For Each oRowPeriod As DataRow In oDTPeriodi.Rows
            Dim newAxisItem As New AxisItem(Nz(oRowPeriod("Descrizione")))
            grfProduction.PlotArea.XAxis.Items.Add(newAxisItem)
        Next

        grfProduction.PlotArea.XAxis.MinorGridLines.Visible = False
        grfProduction.PlotArea.YAxis.MinorGridLines.Visible = False
        grfProduction.Legend.Appearance.Visible = True
        grfProduction.Legend.Appearance.Position = HtmlChart.ChartLegendPosition.Top

    End Sub

    Private Sub Operation_Player_ProductionProcess_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        If Not Page.IsPostBack Then
            If Session("IDRole") = "P" Then
                m_SiteMaster.ReloadPeriodsCalculate()
            End If
            LoadData()

            btnTranslate.Visible = Session("IDRole").Contains("D")
        End If

        ' Controllo tutti gli oggetti presenti
        Dim oTranslation As New TranslationObjectForm(Me, g_DAL, Nz(Session("LanguageActive")))

    End Sub

    Private Sub btnTranslate_Click(sender As Object, e As EventArgs) Handles btnTranslate.Click
        Session("PageOrigin") = Me.ToString().Substring(4, Me.ToString().Substring(4).Length - 5) + ".aspx"
        Response.Redirect("PageTranslation.aspx")
        Response.End()
    End Sub

#Region "PANNELLO HELP"

    Private Sub LoadHelp()
        lblHelp.Text = "<p><strong>Production planned</strong><br>
                      <strong>It is the production level decided by the  company</strong></p>
                    <p><strong>&nbsp;</strong></p>
                    <p><strong>Production allowed</strong><br>
                        <strong>This is the production that the system allows  you to produce within the cash available</strong></p>
                    <p><strong>&nbsp;</strong></p>
                    <p><strong>Actual production</strong><br>
                        <strong>This is the production that the system allows  you to produce with the raw materials and production capacity available</strong> </p>
                  <p>&nbsp;</p>"
    End Sub

#End Region

End Class
