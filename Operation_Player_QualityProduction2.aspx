﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Operation_Player_QualityProduction2.aspx.vb" Inherits="Operation_Player_QualityProduction2" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="cntMain" ContentPlaceHolderID="MainContent" runat="Server" ClientIDMode="Static">

    <asp:UpdatePanel ID="UpdatePanelMain" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <h2>
                <asp:Label ID="lblWelcome" runat="server" Text="Operation"></asp:Label>
            </h2>

            <div class="clr"></div>

            <div class="divTable">
                <div class="divTableRow">
                    <div class="divTableCell">
                        <h3>
                            <asp:Label ID="lblTitolo" runat="server" Text="Quality of production"></asp:Label>
                        </h3>
                    </div>
                    <div class="divTableCell" style="text-align: right; font-size: 8pt">
                        <telerik:RadLinkButton ID="lnkHelp" runat="server" Text="Help" EnableAjaxSkinRendering="true" ToolTip="Show help"></telerik:RadLinkButton>
                    </div>
                </div>
            </div>

            <div class="row">
                <telerik:RadHtmlChart ID="grfQualityProduction" runat="server" Style="width: 75%; float: left;">
                    <ChartTitle Text="Production quality"></ChartTitle>
                    <PlotArea>
                        <Series>
                            <telerik:ColumnSeries Name="Production" DataFieldY="Production">
                                <TooltipsAppearance DataFormatString="{0:2N}" BackgroundColor="White" Color="Blue" />
                                <LabelsAppearance Visible="false" Position="Center" RotationAngle="-90" Color="#FFE038" />
                                <Appearance FillStyle-BackgroundColor="#2359FC" />
                            </telerik:ColumnSeries>

                            <telerik:LineSeries Name="Average" DataFieldY="Average">
                                <Appearance FillStyle-BackgroundColor="#B67C00" />
                                <LineAppearance LineStyle="Smooth" Width="3px" />
                                <TooltipsAppearance DataFormatString="{0:2P}" BackgroundColor="White" Color="#B67C00" />
                                <LabelsAppearance Visible="false" />
                            </telerik:LineSeries>
                        </Series>

                        <XAxis>
                        </XAxis>

                        <YAxis>
                            <LabelsAppearance DataFormatString="{0}" />
                        </YAxis>

                    </PlotArea>

                    <Legend>
                        <Appearance Visible="true" Position="Top" />
                    </Legend>
                </telerik:RadHtmlChart>

                <asp:Table ClientIDMode="Static" ID="tblQualityTeam" runat="server" Style="float: right; width: 25%; padding-left: 10px; padding-top: 50px">
                </asp:Table>
            </div>

            <div class="clr"></div>

            <div class="row" style="min-height: 50px;"></div>

            <div class="row" style="vertical-align: top">
                <asp:Table ClientIDMode="Static" ID="tblQualityProductions" runat="server">
                </asp:Table>
            </div>

            <div class="clr"></div>

            <div class="row">
                <asp:PlaceHolder runat="server" ID="Message" Visible="false">
                    <p class="text-danger">
                        <asp:Literal runat="server" ID="MessageText" />
                    </p>
                </asp:PlaceHolder>
            </div>

            <telerik:RadWindow RenderMode="Lightweight" ID="modalHelp" runat="server" Width="520px" Height="450px" CenterIfModal="false"
                Style="z-index: 100001;" BorderStyle="None" Behaviors="Close, Move" Title="Help">
                <ContentTemplate>
                    <div style="padding: 10px; text-align: left;">
                        <div id="divTableMaster" runat="server" style="margin-bottom: 10px;">
                            <asp:Label runat="server" ID="lblHelp" Visible="true">

                            </asp:Label>
                        </div>
                    </div>
                </ContentTemplate>
            </telerik:RadWindow>

            <div class="clr"></div>

            <div class="row">
                <div class="divTableCell" style="text-align: right;">
                    <telerik:RadButton ID="btnTranslate" runat="server" Text="Translate" EnableAjaxSkinRendering="true" ToolTip=""></telerik:RadButton>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
