﻿Imports System.Data
Imports APS.APSUtility
Imports APPCore.Utility
Imports DALC4NET
Imports Telerik.Web.UI

Partial Class Operation_Player_QualityProduction2
    Inherits System.Web.UI.Page
    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster
    Private m_IDPeriodoMax As Integer

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        Message.Visible = False
        LoadData()
        UpdatePanelMain.Update()
    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        Message.Visible = False
        LoadData()
        UpdatePanelMain.Update()
    End Sub

    Private Sub Operation_Player_QualityProduction2_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        m_SessionData = Session("SessionData")

        m_SiteMaster = Me.Master

        m_SiteMaster.LoadPeriodsValid = Session("IDRole") = "P"

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oCBOPeriod As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOPeriodValid As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriodValid"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler m_SiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler m_SiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(Session("IDGame")) ' Ultimo periodo
        oCBOPeriod.SelectedValue = Session("IDPeriodValid")
        m_SiteMaster.PeriodChange = Session("IDPeriodValid")

        Session("NameTeam") = GetTeamName(Session("IDTeam"))
        oCBOTeam.SelectedValue = Session("IDTeam")
        m_SiteMaster.TeamChange = Session("IDTeam")

        ' Gestione del pannello delle decisioni concesse
        modalHelp.OpenerElementID = lnkHelp.ClientID
        modalHelp.Modal = False
        modalHelp.VisibleTitlebar = True

        LoadHelp()
    End Sub

    Private Sub LoadData()
        tblQualityProductions.Rows.Clear()
        tblQualityTeam.Rows.Clear()
        grfQualityProduction.Visible = False

        LoadDataGraph()
        LoadDataTable()
        LoadDataQualityTeam()
    End Sub

    Private Sub LoadDataGraph()
        Dim sSQL As String
        Dim oDTTableResult As DataTable
        Dim oDTTableGraph As New DataTable
        Dim dProduction As Double
        Dim dAverage As Double
        Dim sPeriod As String = ""
        Dim iContaRighe As Integer = 1

        '' Preparo il datatable che ospiterà i dati
        oDTTableGraph.Columns.Add("Production", GetType(Double))
        oDTTableGraph.Columns.Add("Average", GetType(Double))
        oDTTableGraph.Columns.Add("Period", GetType(String))

        sSQL = "SELECT VariableName, Value, P.Descrizione AS Period, C.IDPeriod " _
             & "FROM Variables_Calculate_Define V " _
             & "INNER JOIN Variables_Calculate_Value C ON V.Id = C.IDVariable " _
             & "INNER JOIN BGOL_Periods P ON C.IDPeriod = P.Id " _
             & "WHERE (VariableName = 'ProduQualiProce') AND IDPlayer = " & Session("IDTeam") _
             & " AND V.IDGame = " & Session("IDGame")
        If Session("IDRole") = "P" Then
            sSQL &= " AND ISNULL(C.Simulation, 0) = 0 "
        End If
        sSQL &= "ORDER BY C.IDPeriod "

        oDTTableResult = g_DAL.ExecuteDataTable(sSQL)

        For Each oRow As DataRow In oDTTableResult.Rows
            If Nz(oRow("VariableName")) = "ProduQualiProce" Then dProduction = Nn(oRow("Value"))

            If sPeriod <> Nz(oRow("Period")) Then sPeriod = Nz(oRow("Period"))

            sSQL = "SELECT SUM(CAST(REPLACE(Value, ',', '.') AS Decimal(18, 9))) / COUNT(C.ID) AS Media " _
                 & "FROM Variables_Calculate_Define V " _
                 & "INNER JOIN Variables_Calculate_Value C ON V.Id = C.IDVariable " _
                 & "WHERE LTRIM(RTRIM(V.VariableName)) = 'ProduQualiProce' AND IDPeriod = " & Nni(oRow("IdPeriod")) _
                 & " AND V.IDGame = " & Session("IDGame")
            dAverage = g_DAL.ExecuteScalar(sSQL)

            oDTTableGraph.Rows.Add(dProduction.ToString("N2"), dAverage.ToString("N2"), sPeriod)

        Next

        grfQualityProduction.DataSource = oDTTableGraph
        grfQualityProduction.DataBind()

        grfQualityProduction.PlotArea.XAxis.DataLabelsField = "Period"
        grfQualityProduction.PlotArea.XAxis.LabelsAppearance.TextStyle.Bold = True

        grfQualityProduction.PlotArea.XAxis.LabelsAppearance.RotationAngle = 45
        grfQualityProduction.PlotArea.XAxis.EnableBaseUnitStepAuto = True

        If oDTTableResult.Rows.Count > 0 Then
            grfQualityProduction.Visible = True
            lblTitolo.Text = "Quality of production"
        Else
            lblTitolo.Text &= "<br/><br/> No data available for this period"
        End If
    End Sub

    Private Sub LoadDataTable()
        Dim oDTItems As DataTable = HandleGetItemsPeriodVariables(Session("IDGame"), Nz(Session("LanguageActive")))

        Dim iTotNumPlayers As Integer = HandleLoadPlayersGame(Session("IDGame")).Rows.Count

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Try
            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header vuota
            tblCell = New TableCell
            Dim oLBLTextHeader As New RadLabel
            oLBLTextHeader.ID = "lblTextHeader"
            oLBLTextHeader.Text = "Quality of production"
            oLBLTextHeader.ForeColor = Drawing.Color.WhiteSmoke

            tblHeaderCell.Controls.Add(oLBLTextHeader)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.Style.Add("background", "#3e6c86")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            ' Recupero gli Items per la costruzione delle colonne
            For Each oRowItm As DataRow In oDTItems.Rows
                ' Cella del prezzo di vendita
                tblHeaderCell = New TableHeaderCell
                tblHeaderCell.Text = Nz(oRowItm("VariableName"))
                tblHeaderCell.BorderStyle = BorderStyle.None
                tblHeaderRow.Cells.Add(tblHeaderCell)
            Next
            tblHeaderRow.BorderStyle = BorderStyle.None
            tblHeaderRow.Style.Add("margin-top", "10px")
            tblQualityProductions.Rows.Add(tblHeaderRow)

            ' Aggiungo le righe di dettaglio
            ' Riga Quality of raw materials used
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLBLQualityRawMaterials As New RadLabel
            oLBLQualityRawMaterials.ID = "lblQualityRawMaterials"
            oLBLQualityRawMaterials.Text = "Quality of raw materials used"
            oLBLQualityRawMaterials.Style.Add("color", "darkred")
            tblCell.Controls.Add(oLBLQualityRawMaterials)
            tblCell.Style.Add("width", "30%")
            tblRow.Cells.Add(tblCell)
            For Each oRowItm As DataRow In oDTItems.Rows
                ' RawQualiUsed
                tblCell = New TableCell

                Dim lnkRawQualiUsed As New HyperLink
                lnkRawQualiUsed.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=RawQualiUsed&II=" & oRowItm("IDVariable") & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                lnkRawQualiUsed.Style.Add("text-decoration", "none")
                lnkRawQualiUsed.Style.Add("cursor", "pointer")

                lnkRawQualiUsed.Text = Nn(GetVariableDefineValue("RawQualiUsed", m_SiteMaster.TeamIDGet, oRowItm("IDVariable"), 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N1")
                tblCell.Controls.Add(lnkRawQualiUsed)

                tblCell.BorderStyle = BorderStyle.None
                tblCell.Style.Add("width", "12,5%")
                tblCell.Style.Add("font-weight", "bold")
                tblCell.Style.Add("text-align", "right")
                tblRow.Cells.Add(tblCell)
            Next
            tblQualityProductions.Rows.Add(tblRow)

            ' Riga Quality of production process
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLBLQualityProductionProcess As New RadLabel
            oLBLQualityProductionProcess.ID = "lblQualityProductionProcess"
            oLBLQualityProductionProcess.Text = "Quality of production process"
            oLBLQualityProductionProcess.Style.Add("color", "darkred")
            tblCell.Controls.Add(oLBLQualityProductionProcess)
            tblRow.Cells.Add(tblCell)
            For Each oRowItm As DataRow In oDTItems.Rows
                ' ProduQualiProce
                tblCell = New TableCell

                Dim lnkProduQualiProce As New HyperLink
                lnkProduQualiProce.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=ProduQualiProce&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                lnkProduQualiProce.Style.Add("text-decoration", "none")
                lnkProduQualiProce.Style.Add("cursor", "pointer")

                lnkProduQualiProce.Text = Nn(GetVariableDefineValue("ProduQualiProce", m_SiteMaster.TeamIDGet, 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N1")
                tblCell.Controls.Add(lnkProduQualiProce)

                tblCell.BorderStyle = BorderStyle.None
                tblCell.Style.Add("text-align", "right")
                tblRow.Cells.Add(tblCell)
            Next
            tblQualityProductions.Rows.Add(tblRow)

            ' Riga Quality of finished goods
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLBLQualityFinishedGoods As New RadLabel
            oLBLQualityFinishedGoods.ID = "lblQualityFinishedGoods"
            oLBLQualityFinishedGoods.Text = "Quality of finished goods"
            oLBLQualityFinishedGoods.Style.Add("color", "darkred")
            tblCell.Controls.Add(oLBLQualityFinishedGoods)
            tblCell.Style.Add("font-weight", "bold")
            tblRow.Cells.Add(tblCell)
            For Each oRowItm As DataRow In oDTItems.Rows
                ' ProduTotalQuali
                tblCell = New TableCell
                tblCell.Text = Nn(GetVariableDefineValue("ProduTotalQuali", m_SiteMaster.TeamIDGet, oRowItm("IDVariable"), 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N1")
                tblCell.BorderStyle = BorderStyle.None
                tblCell.Style.Add("text-align", "right")
                tblCell.Style.Add("font-weight", "bold")
                tblRow.Cells.Add(tblCell)
            Next
            tblQualityProductions.Rows.Add(tblRow)

        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try

    End Sub

    Private Sub LoadDataQualityTeam()
        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Try
            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header 
            tblCell = New TableCell
            Dim oLBLTextHeaderQualityTeam As New RadLabel
            oLBLTextHeaderQualityTeam.ID = "lblTextHeaderQualityTeam"
            oLBLTextHeaderQualityTeam.Text = "Quality of production"
            oLBLTextHeaderQualityTeam.Style.Add("color", "#ffffff")
            tblHeaderCell.Controls.Add(oLBLTextHeaderQualityTeam)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.Style.Add("background", "#3e6c86")
            tblHeaderCell.ColumnSpan = 2
            tblHeaderRow.Cells.Add(tblHeaderCell)
            tblQualityTeam.Rows.Add(tblHeaderRow)

            ' PRIMA RIGA DATI
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLBLQualityTeam As New RadLabel
            oLBLQualityTeam.ID = "lblQualityTeam"
            oLBLQualityTeam.Text = m_SiteMaster.TeamNameGet
            oLBLQualityTeam.Style.Add("color", "darkred")
            tblCell.Controls.Add(oLBLQualityTeam)
            tblCell.Style.Add("width", "80%")
            tblRow.Cells.Add(tblCell)

            ' ProduQualiProce
            tblCell = New TableCell

            Dim lnkProduQualiProceTeam As New HyperLink
            lnkProduQualiProceTeam.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=ProduQualiProce&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            lnkProduQualiProceTeam.Style.Add("text-decoration", "none")
            lnkProduQualiProceTeam.Style.Add("cursor", "pointer")

            lnkProduQualiProceTeam.Text = Nn(GetVariableDefineValue("ProduQualiProce", Session("IDTeam"), 0, 0, Session("IDPeriod"), Session("IDGame"))).ToString("N1") & "%"
            tblCell.Controls.Add(lnkProduQualiProceTeam)

            tblCell.Style.Add("width", "20%")
            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)
            tblQualityTeam.Rows.Add(tblRow)

            ' SECONDA RIGA DATI
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLBLAverageText As New RadLabel
            oLBLAverageText.ID = "lbloLBLAverageText"
            oLBLAverageText.Text = "Average"
            tblCell.Controls.Add(oLBLAverageText)
            tblRow.Cells.Add(tblCell)

            ' AbsolQualiProce
            tblCell = New TableCell

            Dim lnkAbsolQualiProce As New HyperLink
            lnkAbsolQualiProce.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=AbsolQualiProce&II=0&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
            lnkAbsolQualiProce.Style.Add("text-decoration", "none")
            lnkAbsolQualiProce.Style.Add("cursor", "pointer")

            lnkAbsolQualiProce.Text = Nn(GetVariableState("AbsolQualiProce", Session("IDTeam"), Session("IDPeriod"), "", Session("IDGame"))).ToString("N1") & "%"
            tblCell.Controls.Add(lnkAbsolQualiProce)

            tblCell.Style.Add("text-align", "right")
            tblRow.Cells.Add(tblCell)
            tblQualityTeam.Rows.Add(tblRow)

        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub Operation_Player_QualityProduction2_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        If Not Page.IsPostBack Then
            If Session("IDRole") = "P" Then
                m_SiteMaster.ReloadPeriodsCalculate()
            End If
            LoadData()

            btnTranslate.Visible = Session("IDRole").Contains("D")

        End If
        ' Controllo tutti gli oggetti presenti
        Dim oTranslation As New TranslationObjectForm(Me, g_DAL, Nz(Session("LanguageActive")))

    End Sub

    Private Sub btnTranslate_Click(sender As Object, e As EventArgs) Handles btnTranslate.Click
        Session("PageOrigin") = Me.ToString().Substring(4, Me.ToString().Substring(4).Length - 5) + ".aspx"
        Response.Redirect("PageTranslation.aspx")
        Response.End()
    End Sub

#Region "PANNELLO HELP"

    Private Sub LoadHelp()
        lblHelp.Text = "<p><strong>Quality</strong><br>
                      <strong>Compare this level to the minimum level  required by the wholesalers to see if you match their threshold. The quality of  products produced in-house for retail will increase customer competitiveness.</strong></p>
                    <p><strong>Quality of raw materials used</strong><br>
                        <strong>The quality of raw materials used in production  is based on a compounded average of raw materials from local, importers and  stock. The raw materials used are calculated with a FIFO method in order to prevent any possible deterioration of the materials. This, despite the legal fiscal law which force the company to account on a LIFO base. </strong></p>
                    <p><strong>Quality of production process</strong><br>
                        <strong>The quality of the production process reflects  the total amount of investments in automation technology. </strong><br>
                        <strong>It is a value comparing your company level to  that of your competitors&nbsp; Because it is  relative to investments made by your competitors and increases your company  level, you must invest more than your competitors. Poor investments at the  beginning may lead to a need for higher efforts to recover a poor market  quality perception. &nbsp;The quality of the  production process changes in the same period in which the investment is made.</strong> <strong>The standard market level shows the base from where each team is perceived by customers. The teams' perception is calculated on the base of the variance of previous and present investments which are monitored and weighted by the market actors.</strong></p>
                    <p><strong>&nbsp;</strong></p>
                    <p><strong>Quality of finished goods</strong><br>
                        <strong>It is determined by the quality of raw  materials used and the quality &nbsp;of the  production process.</strong> </p>
                    <p>&nbsp;</p>"
    End Sub

#End Region

End Class
