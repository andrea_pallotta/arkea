﻿Imports System.Data
Imports APS.APSUtility
Imports APPCore.Utility
Imports DALC4NET
Imports Telerik.Web.UI

Partial Class Operation_Player_StockRawMaterial
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster
    Private m_IDPeriodoMax As Integer

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        Message.Visible = False
        LoadData()
        UpdatePanelMain.Update()
    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        Message.Visible = False
        LoadData()
        UpdatePanelMain.Update()
    End Sub

    Private Sub Operation_Player_StockRawMaterials_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        m_SessionData = Session("SessionData")

        m_SiteMaster = Me.Master

        m_SiteMaster.LoadPeriodsValid = Session("IDRole") = "P"

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oCBOPeriod As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOPeriodValid As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriodValid"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler m_SiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler m_SiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(Session("IDGame")) ' Ultimo periodo
        oCBOPeriod.SelectedValue = Session("IDPeriodValid")
        m_SiteMaster.PeriodChange = Session("IDPeriodValid")

        Session("NameTeam") = GetTeamName(Session("IDTeam"))
        oCBOTeam.SelectedValue = Session("IDTeam")
        m_SiteMaster.TeamChange = Session("IDTeam")

        ' Gestione del pannello delle decisioni concesse
        modalHelp.OpenerElementID = lnkHelp.ClientID
        modalHelp.Modal = False
        modalHelp.VisibleTitlebar = True

        LoadHelp()
    End Sub

    Private Sub LoadData()
        tblStockRawMaterials.Rows.Clear()
        tblUnitRawMaterials.Rows.Clear()

        LoadDataStockRawMaterials()
        LoadDataUnitRawMaterials()
    End Sub

    Private Sub LoadDataStockRawMaterials()
        Dim oDTItems As DataTable = HandleGetItemsPeriodVariables(Session("IDGame"), Nz(Session("LanguageActive")))

        Dim iTotNumPlayers As Integer = HandleLoadPlayersGame(Session("IDGame")).Rows.Count

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Dim sNomeVariabileLista As String

        Try
            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header vuota
            tblCell = New TableCell
            Dim oLBLTextHeader As New RadLabel
            oLBLTextHeader.ID = "lblTextHeader"
            oLBLTextHeader.Text = "Stock of Raw materials"
            oLBLTextHeader.Style.Add("color", "#ffffff")
            tblCell.Controls.Add(oLBLTextHeader)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.Style.Add("background", "#3e6c86")
            tblHeaderCell.Style.Add("color", "#ffffff")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            ' Recupero gli Items per la costruzione delle colonne
            For Each oRowItm As DataRow In oDTItems.Rows
                ' Cella del prezzo di vendita
                tblHeaderCell = New TableHeaderCell
                tblHeaderCell.Text = Nz(oRowItm("VariableName"))
                tblHeaderCell.BorderStyle = BorderStyle.None
                tblHeaderCell.ColumnSpan = 2
                tblHeaderRow.Cells.Add(tblHeaderCell)
            Next

            tblHeaderRow.BorderStyle = BorderStyle.None
            tblHeaderRow.Style.Add("margin-top", "10px")

            tblStockRawMaterials.Rows.Add(tblHeaderRow)

            ' Aggiungo le righe di dettaglio
            ' Riga Beginning stock
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLBLBeginningStock As New RadLabel
            oLBLBeginningStock.ID = "lblBeginningStock"
            oLBLBeginningStock.Text = "Beginning stock"
            oLBLBeginningStock.Style.Add("color", "darkred")
            tblCell.Controls.Add(oLBLBeginningStock)
            tblCell.Style.Add("width", "30%")
            tblRow.Cells.Add(tblCell)

            For Each oRowItm As DataRow In oDTItems.Rows
                ' StockRawMater
                sNomeVariabileLista = "var_" & m_SiteMaster.TeamNameGet & "_StockRawMater_" & oRowItm("VariableName")
                tblCell = New TableCell

                Dim lnkStockRawMater As New HyperLink
                lnkStockRawMater.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=StockRawMater&II=" & oRowItm("IDVariable") & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                lnkStockRawMater.Style.Add("text-decoration", "none")
                lnkStockRawMater.Style.Add("cursor", "pointer")

                lnkStockRawMater.Text = Nn(GetVariableState("StockRawMater", Session("IDTeam"), m_SiteMaster.PeriodGetCurrent, sNomeVariabileLista, Session("IDGame"))).ToString("N0")

                tblCell.Controls.Add(lnkStockRawMater)

                tblCell.BorderStyle = BorderStyle.None

                tblRow.Cells.Add(tblCell)

                ' RawStockQuali
                tblCell = New TableCell
                Dim lnkRawStockQuali As New HyperLink

                sNomeVariabileLista = "var_" & m_SiteMaster.TeamNameGet & "_RawStockQuali_" & oRowItm("VariableName")
                lnkRawStockQuali.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=StockRawMater&II=" & oRowItm("IDVariable") & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                lnkRawStockQuali.Style.Add("text-decoration", "none")
                lnkRawStockQuali.Style.Add("cursor", "pointer")

                lnkRawStockQuali.Text = Nn(GetVariableState("RawStockQuali", Session("IDTeam"), m_SiteMaster.PeriodGetCurrent, sNomeVariabileLista, Session("IDGame"))).ToString("N1") & "%"

                tblCell.Controls.Add(lnkRawStockQuali)

                tblCell.BorderStyle = BorderStyle.None

                tblRow.Cells.Add(tblCell)
            Next
            tblStockRawMaterials.Rows.Add(tblRow)

            ' Riga Purchase of local raw materials
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLBLPurchaseOfRawMaterials As New RadLabel
            oLBLPurchaseOfRawMaterials.ID = "lblPurchaseOfRawMaterials"
            oLBLPurchaseOfRawMaterials.Text = "Purchase of raw materials"
            oLBLPurchaseOfRawMaterials.Style.Add("color", "darkred")
            tblCell.Controls.Add(oLBLPurchaseOfRawMaterials)
            tblRow.Cells.Add(tblCell)
            For Each oRowItm As DataRow In oDTItems.Rows
                ' PurchAllowEurop
                tblCell = New TableCell
                Dim lnkPurchAllowEurop As New HyperLink

                lnkPurchAllowEurop.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=PurchAllowEurop&II=" & oRowItm("IDVariable") & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                lnkPurchAllowEurop.Style.Add("text-decoration", "none")
                lnkPurchAllowEurop.Style.Add("cursor", "pointer")

                lnkPurchAllowEurop.Text = Nn(GetVariableDefineValue("PurchAllowEurop", Session("IDTeam"), oRowItm("IDVariable"), 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N0")

                tblCell.Controls.Add(lnkPurchAllowEurop)

                tblCell.BorderStyle = BorderStyle.None

                tblRow.Cells.Add(tblCell)

                ' RawEuropQuali
                tblCell = New TableCell
                Dim lnkRawEuropQuali As New HyperLink

                sNomeVariabileLista = "RawEuropQuali_" & oRowItm("VariableName")

                lnkRawEuropQuali.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=RawEuropQuali&II=" & oRowItm("IDVariable") & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                lnkRawEuropQuali.Style.Add("text-decoration", "none")
                lnkRawEuropQuali.Style.Add("cursor", "pointer")

                lnkRawEuropQuali.Text = Nn(GetVariableBoss("RawEuropQuali", Session("IDTeam"), m_SiteMaster.PeriodGetCurrent, sNomeVariabileLista, Session("IDGame"))).ToString("N1") & " %"

                tblCell.Controls.Add(lnkRawEuropQuali)
                tblCell.BorderStyle = BorderStyle.None

                tblRow.Cells.Add(tblCell)
            Next
            tblStockRawMaterials.Rows.Add(tblRow)

            ' Riga Purchase of imported raw materials
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLBLPurchaseOfImportedRawMaterials As New RadLabel
            oLBLPurchaseOfImportedRawMaterials.ID = "lblPurchaseOfImportedRawMaterials"
            oLBLPurchaseOfImportedRawMaterials.Text = "Purchase of imported raw materials"
            oLBLPurchaseOfImportedRawMaterials.Style.Add("color", "darkred")
            tblCell.Controls.Add(oLBLPurchaseOfImportedRawMaterials)
            tblRow.Cells.Add(tblCell)
            For Each oRowItm As DataRow In oDTItems.Rows
                ' PurchAllowNIC
                tblCell = New TableCell
                Dim lnkPurchAllowNIC As New HyperLink

                lnkPurchAllowNIC.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=PurchAllowNIC&II=" & oRowItm("IDVariable") & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                lnkPurchAllowNIC.Style.Add("text-decoration", "none")
                lnkPurchAllowNIC.Style.Add("cursor", "pointer")

                lnkPurchAllowNIC.Text = Nn(GetVariableDefineValue("PurchAllowNIC", Session("IDTeam"), oRowItm("IDVariable"), 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N0")

                tblCell.Controls.Add(lnkPurchAllowNIC)
                tblCell.BorderStyle = BorderStyle.None

                tblRow.Cells.Add(tblCell)

                ' RawNICQuali
                tblCell = New TableCell
                Dim lnkRawNICQuali As New HyperLink
                sNomeVariabileLista = "RawNICQuali_" & oRowItm("VariableName")

                lnkRawNICQuali.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=RawEuropQuali&II=" & oRowItm("IDVariable") & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                lnkRawNICQuali.Style.Add("text-decoration", "none")
                lnkRawNICQuali.Style.Add("cursor", "pointer")

                lnkRawNICQuali.Text = Nn(GetVariableBoss("RawNICQuali", Session("IDTeam"), m_SiteMaster.PeriodGetCurrent, sNomeVariabileLista, Session("IDGame"))).ToString("N0")

                tblCell.Controls.Add(lnkRawNICQuali)
                tblCell.BorderStyle = BorderStyle.None

                tblRow.Cells.Add(tblCell)
            Next
            tblStockRawMaterials.Rows.Add(tblRow)

            ' Riga Pending of import raw materials (1/3)
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLBLPendingImport As New RadLabel
            oLBLPendingImport.ID = "lblPendingImport"
            oLBLPendingImport.Text = "Pending of import raw materials (1/3)"
            oLBLPendingImport.Style.Add("color", "darkred")
            tblCell.Controls.Add(oLBLPendingImport)
            tblRow.Cells.Add(tblCell)
            For Each oRowItm As DataRow In oDTItems.Rows
                ' PortfPurchNIC
                tblCell = New TableCell
                Dim lnkPortfPurchNIC As New HyperLink
                lnkPortfPurchNIC.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=PortfPurchNIC&II=" & oRowItm("IDVariable") & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                lnkPortfPurchNIC.Style.Add("text-decoration", "none")
                lnkPortfPurchNIC.Style.Add("cursor", "pointer")

                lnkPortfPurchNIC.Text = Nn(GetVariableDefineValue("PortfPurchNIC", Session("IDTeam"), oRowItm("IDVariable"), 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N0") * -1

                tblCell.Controls.Add(lnkPortfPurchNIC)
                tblCell.BorderStyle = BorderStyle.None

                tblRow.Cells.Add(tblCell)

                '  
                tblCell = New TableCell
                tblCell.Text = ""
                tblCell.BorderStyle = BorderStyle.None
                tblCell.Style.Add("text-align", "right")
                tblCell.Style.Add("padding-right", "5px")
                tblRow.Cells.Add(tblCell)
            Next
            tblStockRawMaterials.Rows.Add(tblRow)

            ' Riga Outstanding imports
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLBLOutstandingImports As New RadLabel
            oLBLOutstandingImports.ID = "lblOutstandingImports"
            oLBLOutstandingImports.Text = "Outstanding imports"
            oLBLOutstandingImports.Style.Add("color", "darkred")
            tblCell.Controls.Add(oLBLOutstandingImports)
            tblRow.Cells.Add(tblCell)
            For Each oRowItm As DataRow In oDTItems.Rows
                ' PortfDelivNIC
                tblCell = New TableCell
                Dim lnkPortfDelivNIC As New HyperLink
                lnkPortfDelivNIC.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=PortfDelivNIC&II=" & oRowItm("IDVariable") & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                lnkPortfDelivNIC.Style.Add("text-decoration", "none")
                lnkPortfDelivNIC.Style.Add("cursor", "pointer")

                lnkPortfDelivNIC.Text = Nn(GetVariableDefineValue("PortfDelivNIC", Session("IDTeam"), oRowItm("IDVariable"), 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N0")

                tblCell.Controls.Add(lnkPortfDelivNIC)
                tblCell.BorderStyle = BorderStyle.None

                tblRow.Cells.Add(tblCell)

                ' RawNICQuali
                tblCell = New TableCell
                sNomeVariabileLista = "RawNICQuali_" & oRowItm("VariableName")

                Dim lnkRawNICQuali As New HyperLink
                lnkRawNICQuali.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=RawNICQuali&II=" & oRowItm("IDVariable") & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                lnkRawNICQuali.Style.Add("text-decoration", "none")
                lnkRawNICQuali.Style.Add("cursor", "pointer")

                lnkRawNICQuali.Text = Nn(GetVariableBoss("RawNICQuali", Session("IDTeam"), m_SiteMaster.PeriodGetCurrent, sNomeVariabileLista, Session("IDGame"))).ToString("N1") & " %"

                tblCell.Controls.Add(lnkRawNICQuali)
                tblCell.BorderStyle = BorderStyle.None

                tblRow.Cells.Add(tblCell)
            Next
            tblStockRawMaterials.Rows.Add(tblRow)

            ' Riga Raw materials used in production
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLBLRawMaterialUsed As New RadLabel
            oLBLRawMaterialUsed.ID = "lblRawMaterialUsed"
            oLBLRawMaterialUsed.Text = "Raw materials used in production"
            oLBLRawMaterialUsed.Style.Add("color", "darkred")
            tblCell.Controls.Add(oLBLRawMaterialUsed)
            tblRow.Cells.Add(tblCell)
            For Each oRowItm As DataRow In oDTItems.Rows
                ' ProduReque
                tblCell = New TableCell
                Dim lnkProduReque As New HyperLink
                lnkProduReque.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=ProduReque&II=" & oRowItm("IDVariable") & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                lnkProduReque.Style.Add("text-decoration", "none")
                lnkProduReque.Style.Add("cursor", "pointer")

                lnkProduReque.Text = Nn(GetVariableDefineValue("ProduReque", Session("IDTeam"), oRowItm("IDVariable"), 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N0") * -1

                tblCell.Controls.Add(lnkProduReque)
                tblCell.BorderStyle = BorderStyle.None

                tblRow.Cells.Add(tblCell)

                '  
                tblCell = New TableCell
                tblCell.Text = ""
                tblCell.BorderStyle = BorderStyle.None
                tblCell.Style.Add("text-align", "right")
                tblCell.Style.Add("padding-right", "5px")
                tblRow.Cells.Add(tblCell)
            Next
            tblStockRawMaterials.Rows.Add(tblRow)

            ' Riga Stock raw net
            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLBLEndStock As New RadLabel
            oLBLEndStock.ID = "lblEndStock"
            oLBLEndStock.Text = "End stock"
            oLBLEndStock.Style.Add("color", "darkred")
            tblCell.Controls.Add(oLBLEndStock)
            tblRow.Cells.Add(tblCell)
            For Each oRowItm As DataRow In oDTItems.Rows
                ' StockRawNext
                tblCell = New TableCell

                Dim lnkStockRawNext As New HyperLink
                lnkStockRawNext.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=StockRawNext&II=" & oRowItm("IDVariable") & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                lnkStockRawNext.Style.Add("text-decoration", "none")
                lnkStockRawNext.Style.Add("cursor", "pointer")

                lnkStockRawNext.Text = Nn(GetVariableDefineValue("StockRawNext", Session("IDTeam"), oRowItm("IDVariable"), 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N0")

                tblCell.Controls.Add(lnkStockRawNext)
                tblCell.BorderStyle = BorderStyle.None

                tblRow.Cells.Add(tblCell)

                ' RawStockQuali
                tblCell = New TableCell
                sNomeVariabileLista = "var_" & m_SiteMaster.TeamNameGet & "_RawStockQuali_" & oRowItm("VariableName")

                Dim lnkRawStockQuali As New HyperLink
                lnkRawStockQuali.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=RawStockQuali&II=" & oRowItm("IDVariable") & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                lnkRawStockQuali.Style.Add("text-decoration", "none")
                lnkRawStockQuali.Style.Add("cursor", "pointer")

                lnkRawStockQuali.Text = Nn(GetVariableState("RawStockQuali", Session("IDTeam"), m_SiteMaster.PeriodGetCurrent, sNomeVariabileLista, Session("IDGame"))).ToString("N1") & "%"

                tblCell.Controls.Add(lnkRawStockQuali)
                tblCell.BorderStyle = BorderStyle.None

                tblRow.Cells.Add(tblCell)
            Next
            tblStockRawMaterials.Rows.Add(tblRow)

        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub LoadDataUnitRawMaterials()
        Dim oDTItems As DataTable = HandleGetItemsPeriodVariables(Session("IDGame"), Nz(Session("LanguageActive")))

        Dim iTotNumPlayers As Integer = HandleLoadPlayersGame(Session("IDGame")).Rows.Count

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Dim sNomeVariabileLista As String

        Try
            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header vuota
            tblCell = New TableCell
            Dim oLBLTextHeader As New RadLabel
            oLBLTextHeader.ID = "lblTextHeaderDataUnitRawMaterials"
            oLBLTextHeader.Text = "Unit value of raw materials"
            oLBLTextHeader.Style.Add("color", "#ffffff")
            tblCell.Controls.Add(oLBLTextHeader)
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("background", "#3e6c86")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            ' Recupero gli Items per la costruzione delle colonne
            For Each oRowItm As DataRow In oDTItems.Rows
                ' Cella del prezzo di vendita
                tblHeaderCell = New TableHeaderCell
                tblHeaderCell.Text = Nz(oRowItm("VariableName"))
                tblHeaderCell.BorderStyle = BorderStyle.None
                tblHeaderRow.Cells.Add(tblHeaderCell)
            Next

            tblHeaderRow.BorderStyle = BorderStyle.None
            tblHeaderRow.Style.Add("margin-top", "10px")

            tblUnitRawMaterials.Rows.Add(tblHeaderRow)

            ' Aggiungo le righe di dettaglio

            tblRow = New TableRow
            tblCell = New TableCell
            Dim oLBLUnitValue As New RadLabel
            oLBLUnitValue.ID = "lblUnitValue"
            oLBLUnitValue.Text = "Unit value"
            oLBLUnitValue.Style.Add("color", "darkred")
            tblCell.Controls.Add(oLBLUnitValue)
            tblCell.Style.Add("width", "30%")
            tblRow.Cells.Add(tblCell)

            For Each oRowItm As DataRow In oDTItems.Rows
                ' ValueRawMater
                tblCell = New TableCell
                sNomeVariabileLista = "var_" & m_SiteMaster.TeamNameGet & "_ValueRawMater_" & oRowItm("VariableName")

                Dim lnkValueRawMater As New HyperLink
                lnkValueRawMater.Attributes.Add("onclick", "window.open('DataChart.aspx?VN=ValueRawMater&II=" & oRowItm("IDVariable") & "&IA=0&TP=" & lblTitolo.Text & "',null,'height=800, width=1024,status= no, resizable= no, scrollbars=no, toolbar=no,location=no,menubar=no ');")
                lnkValueRawMater.Style.Add("text-decoration", "none")
                lnkValueRawMater.Style.Add("cursor", "pointer")

                lnkValueRawMater.Text = Nn(GetVariableState("ValueRawMater", Session("IDTeam"), m_SiteMaster.PeriodGetCurrent, sNomeVariabileLista, Session("IDGame"))).ToString("C2")

                tblCell.Controls.Add(lnkValueRawMater)
                tblCell.BorderStyle = BorderStyle.None

                tblRow.Cells.Add(tblCell)
            Next
            tblUnitRawMaterials.Rows.Add(tblRow)

        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub Operation_Player_StockRawMaterial_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        If Not Page.IsPostBack Then
            If Session("IDRole") = "P" Then
                m_SiteMaster.ReloadPeriodsCalculate()
            End If
            LoadData()

            btnTranslate.Visible = Session("IDRole").Contains("D")

        End If

        ' Controllo tutti gli oggetti presenti
        Dim oTranslation As New TranslationObjectForm(Me, g_DAL, Nz(Session("LanguageActive")))

    End Sub

    Private Sub btnTranslate_Click(sender As Object, e As EventArgs) Handles btnTranslate.Click
        Session("PageOrigin") = Me.ToString().Substring(4, Me.ToString().Substring(4).Length - 5) + ".aspx"
        Response.Redirect("PageTranslation.aspx")
        Response.End()
    End Sub

#Region "PANNELLO HELP"

    Private Sub LoadHelp()
        lblHelp.Text = "<p><strong>Stock of raw materials</strong><br>
                      <strong>The stock existing at the beginning of the  period</strong></p>
                    <p><strong>Purchase of local raw materials</strong><br>
                        <strong>Raw material purchased on the local market</strong></p>
                    <p><strong>Purchase of imported raw materials</strong><br>
                        <strong>Raw material purchased from importers. 2/3 of  those purchased are delivered &nbsp;in the  current period. The remaining 1/3 will be pending and delivered in the  following period at the quality level of the goods of the following period</strong></p>
                    <p><strong>Pending of import raw materials (1/3</strong><br>
                        <strong>This is the outstanding delivery of raw  materials purchased from importers in the current period. They will be  delivered in the next period with the next period quality</strong></p>
                    <p><strong>Outstanding imports</strong><br>
                        <strong>This is the pending delivery of raw materials  purchased from importers &nbsp;in the previous  period. They are delivered in this period with the current period quality.</strong> </p>
                  <p>&nbsp;</p>"
    End Sub

#End Region

End Class
