﻿Imports System.Data
Imports APS
Imports APPCore.Utility
Imports DALC4NET
Imports Telerik.Web.UI

Partial Class Operation_Player_StoreMP
    Inherits System.Web.UI.Page

    Private m_Variable As List(Of String)

    Private m_SiteMaster As SiteMaster

    Private m_SessionData As MU_SessionData
    Private m_Separator As String = "."

    Private m_IDPeriodoMax As Integer

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadDataMagazzinoMP()
        UpdatePanelMain.Update()
    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadDataMagazzinoMP()
        UpdatePanelMain.Update()
    End Sub

    Private Sub LoadDataInitialPeriod(IDPeriodo As Integer)
        Dim sSQL As String = ""
        Dim oDAL As New DBHelper
        Dim sNomeVariabili As String = ""
        Dim oDTResults As DataTable

        Try
            ' Rimuovo i controlli dal DIV di gestione della tabella Pivot
            If divTableMaster.Controls.Count > 0 Then
                For Each oControl As Control In divTableMaster.Controls
                    divTableMaster.Controls.Remove(oControl)
                Next
            End If

            ' Recupero la lista delle variabili da caricare
            For iRiga As Integer = 0 To m_Variable.Count - 1
                sNomeVariabili &= "'" & m_Variable(iRiga) & "',"
            Next

            ' Elimino l'ultima virgola presente
            sNomeVariabili = sNomeVariabili.Substring(0, sNomeVariabili.Length - 1)

            ' Non sono player, per cui carico i dati per ogni player presente in anagrafica
            If Session("IDRole").Contains("B") Or Session("IDRole").Contains("D") Or Session("IDRole").Contains("T") Then
                Dim oDTPlayers As DataTable = LoadPlayersIDTeam(Nni(Session("IDGame")))
                For Each oRowPlayer As DataRow In oDTPlayers.Rows
                    sSQL = "SELECT VS.Name, VS.VariableLabel, " _
                         & "ISNULL(VSV.Value, '0') AS StateValue, VSV.IDAge AS StateIDAge, VSV.IDItem AS StateIDItem, VSV.IDPlayer AS StateIDPlayer, VSV.IDPeriod AS StateIDPeriod, VSV.Id AS StateID, " _
                         & "VSVL.VariableName, VSVL.ID AS StateValueID, VSVL.IDAge AS StateValueIDAge, VSVL.IDItem AS StatevalueIDItem, VSVL.IDPlayer AS StateValueIDPlayer, VSVL.IDPeriod AS StateValueIDPeriod, " _
                         & "ISNULL(VSVL.Value, '0') AS StateValueValue, I.VariableName AS Item, A.VariableName AS Age, 0.0 AS ValueDef " _
                         & "FROM Variables_State VS " _
                         & "LEFT JOIN Variables_State_Value VSV ON VS.Id = VSV.IDVariable " _
                         & "LEFT JOIN Variables_State_Value_List VSVL ON VSV.Id = VSVL.IDVariableState " _
                         & "LEFT JOIN Items_Period_Variables AS I ON VSVL.IDItem = I.Id " _
                         & "LEFT JOIN Items_Period_Variables AS A ON VSVL.IDAge = A.Id " _
                         & "WHERE VS.Name IN (" & sNomeVariabili & ") AND VS.IDGame = " & Nni(Session("IDGame")) & " " _
                         & "AND ISNULL(VSVL.IDPeriod, " & IDPeriodo & ") = " & IDPeriodo & " AND VSVL.IDPlayer = " & Nni(oRowPlayer("IDTeam")) & " " _
                         & "ORDER BY VS.ID, VSVL.IDPlayer "
                    oDTResults = oDAL.ExecuteDataTable(sSQL)

                    ' Converto i valori dei risultati da varchar a numerici con con decimali
                    For Each oRow As DataRow In oDTResults.Rows
                        If Nz(oRow("StateValueValue")) = "" Then
                            oRow("StateValueValue") = "0"
                        End If
                        oRow("ValueDef") = Math.Round(Nn(oRow("StateValueValue"), "0"), 2)
                    Next

                    Dim oPivot As New Pivot(oDTResults)
                    Dim dtPivot As DataTable = oPivot.PivotData("VariableLabel", "ValueDef", AggregateFunction.Sum, "Item")

                    Dim oGridView As New GridView
                    oGridView.ID = Nz(oRowPlayer("TeamName"))
                    AddHandler oGridView.RowCreated, AddressOf grdPivot_RowCreated
                    AddHandler oGridView.RowDataBound, AddressOf grdPivot_RowDataBound
                    oGridView.DataSource = dtPivot
                    oGridView.DataBind()

                    Dim oLabel As New Label
                    oLabel.Text = Nz(oRowPlayer("TeamName"))
                    divTableMaster.Controls.Add(oLabel)
                    divTableMaster.Controls.Add(oGridView)

                    Dim ControlRow As New Literal()
                    ControlRow.Text = "<hr/> <br/>"

                    divTableMaster.Controls.Add(ControlRow)

                    oPivot = Nothing
                Next

            ElseIf Session("IDRole").Contains("P") Then
                sSQL = "Select VS.Name, VS.VariableLabel, " _
                     & "ISNULL(VSV.Value, '0') AS StateValue, VSV.IDAge AS StateIDAge, VSV.IDItem AS StateIDItem, VSV.IDPlayer AS StateIDPlayer, VSV.IDPeriod AS StateIDPeriod, VSV.Id AS StateID, " _
                     & "VSVL.VariableName, VSVL.ID AS StateValueID, VSVL.IDAge AS StateValueIDAge, VSVL.IDItem AS StatevalueIDItem, VSVL.IDPlayer AS StateValueIDPlayer, VSVL.IDPeriod AS StateValueIDPeriod, " _
                     & "ISNULL(VSVL.Value, '0') AS StateValueValue, I.VariableName AS Item, A.VariableName AS Age, 0.0 AS ValueDef " _
                     & "FROM Variables_State VS " _
                     & "LEFT JOIN Variables_State_Value VSV ON VS.Id = VSV.IDVariable " _
                     & "LEFT JOIN Variables_State_Value_List VSVL ON VSV.Id = VSVL.IDVariableState " _
                     & "LEFT JOIN Items_Period_Variables AS I ON VSVL.IDItem = I.Id " _
                     & "LEFT JOIN Items_Period_Variables AS A ON VSVL.IDAge = A.Id " _
                     & "WHERE VS.Name IN (" & sNomeVariabili & ") AND VS.IDGame = " & Nni(Session("IDGame")) & " " _
                     & "AND ISNULL(VSVL.IDPeriod, " & IDPeriodo & ") = " & IDPeriodo & " AND VSVL.IDPlayer = " & Nni(Session("IDTeam")) & " " _
                     & "ORDER BY VS.ID, VSVL.IDPlayer "
                oDTResults = oDAL.ExecuteDataTable(sSQL)

                ' Converto i valori dei risultati da varchar a numerici con con decimali
                For Each oRow As DataRow In oDTResults.Rows
                    If Nz(oRow("StateValueValue")) = "" Then
                        oRow("StateValueValue") = "0"
                    End If
                    oRow("ValueDef") = Math.Round(Nn(oRow("StateValueValue"), "0"), 2)
                Next

                Dim oPivot As New Pivot(oDTResults)
                Dim dtPivot As DataTable = oPivot.PivotData("VariableLabel", "ValueDef", AggregateFunction.Sum, "Item")

                Dim oGridView As New GridView
                AddHandler oGridView.RowCreated, AddressOf grdPivot_RowCreated
                AddHandler oGridView.RowDataBound, AddressOf grdPivot_RowDataBound
                oGridView.DataSource = dtPivot
                oGridView.DataBind()

                divTableMaster.Controls.Add(oGridView)

                oPivot = Nothing
            End If

            SQLConnClose(g_DAL.GetConnObject)
        Catch ex As Exception
            MessageText.Text = ex.Message & "<br/>" & g_MessaggioErrore & "<br/> "
            Message.Visible = True
        End Try
    End Sub

    Protected Sub grdPivot_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.Header Then
            MergeHeader(DirectCast(sender, GridView), e.Row, 1)
        End If
    End Sub

    Protected Sub grdPivot_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            If e.Row.DataItemIndex >= 0 Then
                e.Row.Cells(0).BackColor = System.Drawing.ColorTranslator.FromHtml("#55A2DF")
                e.Row.Cells(0).ForeColor = System.Drawing.ColorTranslator.FromHtml("#fff")
            End If
        End If
    End Sub

    Private Sub MergeHeader(ByVal gv As GridView, ByVal row As GridViewRow, ByVal PivotLevel As Integer)
        Dim iCount As Integer = 1
        Dim iContaColonneHeader = 0

        For iCount = 1 To PivotLevel
            Dim oGridViewRow As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
            Dim Header = (row.Cells.Cast(Of TableCell)().[Select](Function(x) GetHeaderText(x.Text, iCount, PivotLevel))).GroupBy(Function(x) x)

            For Each v In Header
                Dim cell As New TableHeaderCell()
                If iContaColonneHeader > 0 Then
                    cell.Text = v.Key.Substring(v.Key.LastIndexOf(m_Separator) + 1)
                Else
                    cell.Text = ""
                End If

                cell.ColumnSpan = v.Count()

                oGridViewRow.Cells.Add(cell)
                iContaColonneHeader += 1
            Next
            gv.Controls(0).Controls.AddAt(row.RowIndex, oGridViewRow)
        Next
        row.Visible = False
    End Sub

    Private Function GetHeaderText(ByVal s As String, ByVal i As Integer, ByVal PivotLevel As Integer) As String
        If Not s.Contains(m_Separator) AndAlso i <> PivotLevel Then
            Return String.Empty
        Else
            Dim Index As Integer = NthIndexOf(s, m_Separator, i)
            If Index = -1 Then
                Return s
            End If
            Return s.Substring(0, Index)
        End If
    End Function

    ''' <summary>
    ''' Returns the nth occurance of the SubString from string str
    ''' </summary>
    ''' <param name="str">source string</param>
    ''' <param name="SubString">SubString whose nth occurance to be found</param>
    ''' <param name="n">n</param>
    ''' <returns>Index of nth occurance of SubString if found else -1</returns>
    Private Function NthIndexOf(ByVal str As String, ByVal SubString As String, ByVal n As Integer) As Integer
        Dim x As Integer = -1
        For i As Integer = 0 To n - 1
            x = str.IndexOf(SubString, x + 1)
            If x = -1 Then
                Return x
            End If
        Next
        Return x
    End Function

    Private Sub LoadDataMagazzinoMP()
        Try
            Message.Visible = False
            ' Controllo se sono developer/deisgner o boss e se sono al periodo 1
            ' nel caso mostro solo i dati del primo periodo come valori iniziali, altrimenti quelli del periodo precedente calcolati.
            If Session("IDRole").Contains("B") Or Session("IDRole").Contains("D") Or Session("IDRole").Contains("T") Then
                If Session("IDPeriod") <= 1 Then ' sono all'inizio mostro i valori decisionali inseriti 
                    LoadDataInitialPeriod(Session("IDPeriod"))

                Else ' carico i valori riferiti al periodo precedente
                    LoadDataInitialPeriod(Session("IDPeriod") - 1)
                End If

            ElseIf Session("IDRole").Contains("P") Then
                If Session("IDPeriod") > 1 Then ' Sono all'inizio e sono player, non vederò una mazza di niente, non posso vedere i valori decisionali
                    LoadDataInitialPeriod(Session("IDPeriod") - 1)

                Else ' carico i valori riferiti al periodo precedente
                    MessageText.Text = "There aren't value to load"
                    Message.Visible = True
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Operation_Player_StoreMP_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        m_SessionData = Session("SessionData")

        m_SiteMaster = Me.Master

        m_SiteMaster.LoadPeriodsValid = Session("IDRole") = "P"

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oCBOPeriod As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOPeriodValid As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriodValid"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler m_SiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler m_SiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(Session("IDGame")) ' Ultimo periodo
        oCBOPeriod.SelectedValue = Session("IDPeriodValid")
        m_SiteMaster.PeriodChange = Session("IDPeriodValid")

        Session("NameTeam") = GetTeamName(Session("IDTeam"))
        oCBOTeam.SelectedValue = Session("IDTeam")
        m_SiteMaster.TeamChange = Session("IDTeam")

        ' Carico la lista delle variabili
        ' Aggiungo le variabili da gestire nella lista
        m_Variable = New List(Of String)

        m_Variable.Add("StockRawMater")
        m_Variable.Add("RawStockQuali")
        m_Variable.Add("PurchAllowEurop")
        m_Variable.Add("RawEuropQuali")

        m_Variable.Add("PurchAllowNIC")
        m_Variable.Add("RawNICQuali")

        m_Variable.Add("PortfPurchNIC")

        m_Variable.Add("PortfDelivNIC")

        m_Variable.Add("ProduReque")

        m_Variable.Add("RawStockQuali")

        m_Variable.Add("RawStockQuali")

        m_Variable.Add("StockRawNext")

        m_Variable.Add("ValueRawMater")

        m_Variable.Add("PurchDelivNIC")


    End Sub

    Private Sub Operation_Player_StoreMP_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        If Not Page.IsPostBack Then
            If Session("IDRole") = "P" Then
                m_SiteMaster.HandleReloadPeriods(HandleGetMaxPeriodValid(Session("IDGame")))
            End If
            ' Carico i dati da mostrare
            LoadDataMagazzinoMP()

            btnTranslate.Visible = Session("IDRole").Contains("D")

        End If
        ' Controllo tutti gli oggetti presenti
        Dim oTranslation As New TranslationObjectForm(Me, g_DAL, Nz(Session("LanguageActive")))

    End Sub

    Private Sub btnTranslate_Click(sender As Object, e As EventArgs) Handles btnTranslate.Click
        Session("PageOrigin") = Me.ToString().Substring(4, Me.ToString().Substring(4).Length - 5) + ".aspx"
        Response.Redirect("PageTranslation.aspx")
        Response.End()
    End Sub

End Class
