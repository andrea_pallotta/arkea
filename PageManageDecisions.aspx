﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="PageManageDecisions.aspx.vb" Inherits="PageManageDecisions" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="cntMain" ContentPlaceHolderID="MainContent" runat="Server" ClientIDMode="Static">

    <asp:UpdatePanel ID="UpdatePanelMain" runat="server" UpdateMode="Conditional">
        <Triggers>
            <asp:PostBackTrigger ControlID="btnHideManage" />
            <asp:PostBackTrigger ControlID="btnSaveDecision" />
        </Triggers>

        <ContentTemplate>
            <h2>
                <asp:Label ID="lblWelcome" runat="server" Text=""></asp:Label>
            </h2>

            <div class="clr"></div>

            <h3>
                <asp:Label ID="lblTitolo" runat="server" Text="Manage decisions"></asp:Label>
            </h3>

            <div class="row">
                <asp:Panel ID="pnlTable" runat="server">
                    <telerik:RadGrid ID="grdDecisions" runat="server" RenderMode="Lightweight" AllowPaging="True" AllowSorting="True" EnableLinqExpressions="false"
                        OnNeedDataSource="grdDecisions_NeedDataSource" Width="670px" AllowFilteringByColumn="True" Culture="it-IT" GroupPanelPosition="Top">
                        <MasterTableView AutoGenerateColumns="false" TableLayout="Fixed" CssClass="RadGrid_WebBlue"
                            DataKeyNames="Id, IDGame, VariableName, VariableLabel, Attiva, EnabledFromPeriod">

                            <Columns>
                                <telerik:GridButtonColumn CommandName="Modify" Text="Edit" UniqueName="EditColumn"
                                    ButtonType="ImageButton" ImageUrl="~/Content/GridTelerik/Edit.gif">
                                    <HeaderStyle Width="20px" />
                                </telerik:GridButtonColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="VariableName" HeaderText="Variable name" UniqueName="VariableName"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                    <HeaderStyle Width="75px" />
                                </telerik:GridBoundColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="VariableLabel" HeaderText="Variable label" UniqueName="VariableLabel"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                    <HeaderStyle Width="75px" />
                                </telerik:GridBoundColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="Attiva" HeaderText="Enabled" UniqueName="Attiva"
                                    AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="false" AllowFiltering="false">
                                    <HeaderStyle Width="75px" />
                                </telerik:GridBoundColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="PeriodToActivated" HeaderText="Period to activate" UniqueName="PeriodToActivated"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false" AllowFiltering="false">
                                    <HeaderStyle Width="100px" />
                                </telerik:GridBoundColumn>
                            </Columns>

                        </MasterTableView>
                    </telerik:RadGrid>

                </asp:Panel>
            </div>

            <div class="clr"></div>

            <div class="row">
                <input id="hdnPanelDecision" type="hidden" name="hddClick" runat="server" />
                <ajaxToolkit:ModalPopupExtender ID="mpeMain" runat="server" PopupControlID="pnlDecision" TargetControlID="hdnPanelDecision"
                    BackgroundCssClass="modalBackground" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>

                <asp:UpdatePanel ID="pnlDecisionUpdate" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel runat="server" ID="pnlDecision" CssClass="modalPopup" Style="display: none;">
                            <div class="pnlheader">
                                <asp:Label ID="lblPanelTitle" runat="server" Text="Manage decision"></asp:Label>
                            </div>

                            <div class="pnlbody">
                                <!-- GESTIONE DELLA TABELLA CON DIV -->
                                <div class="divTable">
                                    <div class="divTableBody">
                                        <div class="divTableRow">
                                            <div class="divTableCell" style="width: 200px;">Decision</div>
                                            <div class="divTableCell" style="width: 580px;">
                                                <telerik:RadTextBox ID="txtDecision" runat="server" RenderMode="Lightweight" ClientIDMode="Static"
                                                    TextMode="SingleLine" Resize="None" ReadOnly="true">
                                                </telerik:RadTextBox>
                                            </div>
                                        </div>

                                        <div class="divTableRow">
                                            <div class="divTableCell" style="width: 200px;">VariableLabel</div>
                                            <div class="divTableCell" style="width: 580px;">
                                                <telerik:RadTextBox ID="txtVariableLabel" runat="server" RenderMode="Lightweight" ClientIDMode="Static"
                                                    TextMode="SingleLine" Resize="None" ReadOnly="true">
                                                </telerik:RadTextBox>
                                            </div>
                                        </div>

                                        <div class="divTableRow">
                                            <div class="divTableCell" style="width: 200px;">Enabled</div>
                                            <div class="divTableCell" style="width: 580px;">
                                               <telerik:RadCheckBox ID="chkEnabled" runat="server" AutoPostBack="false" RenderMode="Lightweight">
                                               </telerik:RadCheckBox>
                                            </div>
                                        </div>
                                        
                                        <div class="divTableRow">
                                            <div class="divTableCell" style="width: 200px;">Period to activate</div>
                                            <div class="divTableCell" style="width: 580px;">
                                               <telerik:RadComboBox ID="cboPeriod" runat="server" Width="290px" EmptyMessage="< Select period... >"
                                                    RenderMode="Lightweight" RenderingMode="Full" ZIndex="1000000" ClientIDMode="Static">
                                                </telerik:RadComboBox>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="pnlfooter">
                                <telerik:RadButton ID="btnHideManage" runat="server" Text="Close" Width="80px"
                                    RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                                </telerik:RadButton>

                                <telerik:RadButton ID="btnSaveDecision" runat="server" Text="Save" Width="80px"
                                    RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                                </telerik:RadButton>
                            </div>

                            <div style="text-align: center;">
                                <asp:Label runat="server" ID="txtMessageDecision" Visible="false"></asp:Label>
                            </div>

                        </asp:Panel>

                    </ContentTemplate>

                </asp:UpdatePanel>
            </div>

            <div class="row" style="min-height: 20px;"></div>

            <div class="row">
                <asp:PlaceHolder runat="server" ID="Message" Visible="false">
                    <p class="text-danger">
                        <asp:Literal runat="server" ID="MessageText" />
                    </p>
                </asp:PlaceHolder>
            </div>

            <div class="clr"></div>

            <div class="row">
                <div class="divTableCell" style="text-align: right;">
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
