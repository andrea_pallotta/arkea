﻿
Imports System.Data
Imports Telerik.Web.UI
Imports APS
Imports APPCore.Utility

Partial Class PageManageDecisions
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster

    Protected Sub grdDecisions_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs)
        grdDecisions.DataSource = Nothing
        grdDecisions.DataSource = LoadDecisions()
    End Sub

    Private Function LoadDecisions() As DataTable
        Dim oDTDecisions As New DataTable

        Select Case Nz(Session("PageOrigin")).ToString.ToUpper
            Case "DISTRIBUTION_PLAYER_DECISION.ASPX"
                oDTDecisions = HandleLoadGameDecisionPlayer(Session("IDGame"), eVariablesCateogry.Distribution)

            Case "FINANCE_CONTROL_PLAYER_DECISION.ASPX"
                oDTDecisions = HandleLoadGameDecisionPlayer(m_SessionData.Utente.Games.IDGame, eVariablesCateogry.Finance_Control)

            Case "MARKETING_PLAYER_DECISION.ASPX"
                oDTDecisions = HandleLoadGameDecisionPlayer(m_SessionData.Utente.Games.IDGame, eVariablesCateogry.Marketing)

            Case "OPERATION_PLAYER_DECISION.ASPX"
                oDTDecisions = HandleLoadGameDecisionPlayer(m_SessionData.Utente.Games.IDGame, eVariablesCateogry.Operations)
        End Select

        Return oDTDecisions

    End Function

    Private Sub ClearPanel()
        txtDecision.Text = ""
        txtMessageDecision.Text = ""
        txtVariableLabel.Text = ""
        cboPeriod.SelectedValue = -1
        chkEnabled.Checked = False
    End Sub

    Private Sub PageManageDecisions_Init(sender As Object, e As EventArgs) Handles Me.Init
        m_SessionData = Session("SessionData")

    End Sub

    Private Sub grdDecisions_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles grdDecisions.ItemCommand
        If e.CommandName.ToUpper <> "PAGE" AndAlso e.CommandName.ToUpper <> "SORT" AndAlso e.CommandName.ToUpper <> "FILTER" Then
            Session("DecisionSelected") = DirectCast(e.Item, GridDataItem)

            If e.CommandName.ToUpper = "MODIFY" Then
                ClearPanel()
                mpeMain.Show()
                UpdatePanelMain.Update()
            End If

        End If
    End Sub

    Private Sub pnlDecisionUpdate_PreRender(sender As Object, e As EventArgs) Handles pnlDecisionUpdate.PreRender
        Dim oDecision As GridDataItem = DirectCast(Session("DecisionSelected"), GridDataItem)

        ' Procedo con il caricamento dei dati da inserire nel panello
        ' Recupero l'id della variabile selezionata
        If Not oDecision Is Nothing Then
            ' Controllo che sia stato selezionato effettivamente un item, altrimenti sono nel caricamento della pagina
            lblPanelTitle.Text = "Manage decision - " & oDecision.GetDataKeyValue("VariableName").ToString

            Session("IDDecision") = oDecision.GetDataKeyValue("Id")

            txtDecision.Text = Nz(oDecision.GetDataKeyValue("VariableName"))
            txtVariableLabel.Text = Nz(oDecision.GetDataKeyValue("VariableLabel"))

            chkEnabled.Checked = Nb(oDecision.GetDataKeyValue("Attiva"))

            cboPeriod.SelectedValue = Nni(oDecision.GetDataKeyValue("EnabledFromPeriod"))
        End If
    End Sub

    Private Sub LoadPeriods()
        Dim oDTPeriods As DataTable

        Try
            cboPeriod.Items.Clear()
            oDTPeriods = LoadPeriodGame(Nni(Session("IDGame")), True)
            cboPeriod.DataSource = oDTPeriods
            cboPeriod.DataValueField = "Id"
            cboPeriod.DataTextField = "Descrizione"
            cboPeriod.DataBind()

            cboPeriod.Items.Insert(0, New RadComboBoxItem("< Select period... >", String.Empty))

            cboPeriod.SelectedValue = 0
        Catch ex As Exception
            MessageText.Text = "PageManageDecisions.aspx -> LoadPeriods " & ControlChars.NewLine & ex.Message
        End Try
    End Sub

    Private Sub PageManageDecisions_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadPeriods()
        End If
    End Sub

    Private Sub btnSaveDecision_Click(sender As Object, e As EventArgs) Handles btnSaveDecision.Click
        ' Procedo con il salvataggio dell'informazione relativa alla decisione
        Dim sSQL As String = ""

        ' Controllo se andare in modifica o inserimento
        If Nni(Session("IDDecision")) > 0 Then ' Modifica
            If Nz(cboPeriod.SelectedValue) <> "" Then
                sSQL = "UPDATE Decisions_Players SET " _
                     & "Enabled = " & Boolean_To_Bit(chkEnabled.Checked) & ", EnabledFromPeriod = " & Nni(cboPeriod.SelectedValue) _
                     & " WHERE ID = " & Nni(Session("IDDecision"))

            Else
                sSQL = "UPDATE Decisions_Players SET " _
                     & "Enabled = " & Boolean_To_Bit(chkEnabled.Checked) & ", EnabledFromPeriod = NULL " _
                     & " WHERE ID = " & Nni(Session("IDDecision"))
            End If

        End If
        g_DAL.ExecuteNonQuery(sSQL)

        ClearPanel()

        grdDecisions.Rebind()

        UpdatePanelMain.Update()

        mpeMain.Hide()
    End Sub

End Class
