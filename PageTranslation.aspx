﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="PageTranslation.aspx.vb" Inherits="PageTranslation" ValidateRequest="false"  %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="cntMain" ContentPlaceHolderID="MainContent" runat="Server" ClientIDMode="Static">

    <asp:UpdatePanel ID="UpdatePanelMain" runat="server" UpdateMode="Conditional">
        <Triggers>
            <asp:PostBackTrigger ControlID="btnHideTranslation" />
        </Triggers>

        <ContentTemplate>
            <h2>
                <asp:Label ID="lblWelcome" runat="server" Text=""></asp:Label>
            </h2>

            <div class="clr"></div>

            <h3>
                <asp:Label ID="lblTitolo" runat="server" Text="Traduzione pagina"></asp:Label>
            </h3>

            <div class="row">
                <asp:Panel ID="pnlTable" runat="server">
                    <telerik:RadGrid ID="grdFormsObject" runat="server" RenderMode="Lightweight" AllowPaging="True" AllowSorting="True" EnableLinqExpressions="false"
                        OnNeedDataSource="grdFormsObject_NeedDataSource" Width="670px" AllowFilteringByColumn="True" Culture="it-IT" GroupPanelPosition="Top">
                        <MasterTableView AutoGenerateColumns="false" TableLayout="Fixed" CssClass="RadGrid_WebBlue"
                            DataKeyNames="Id, FormName, ObjectID, ObjectName, ObjectDescription">

                            <Columns>
                                <telerik:GridButtonColumn CommandName="Modify" Text="Edit" UniqueName="EditColumn"
                                    ButtonType="ImageButton" ImageUrl="~/Content/GridTelerik/Edit.gif">
                                    <HeaderStyle Width="20px" />
                                </telerik:GridButtonColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="ObjectName" HeaderText="Object name" UniqueName="ObjectName"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                    <HeaderStyle Width="75px" />
                                </telerik:GridBoundColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="ObjectDescription" HeaderText="Description" UniqueName="ObjectDescription"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                    <HeaderStyle Width="75px" />
                                </telerik:GridBoundColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridButtonColumn CommandName="Translate" Text="Translate" UniqueName="Translate"
                                    ButtonType="ImageButton" ImageUrl="Images/Translate.ico" Resizable="false">
                                    <HeaderStyle Width="20px" />
                                </telerik:GridButtonColumn>
                            </Columns>

                        </MasterTableView>
                    </telerik:RadGrid>

                </asp:Panel>
            </div>

            <div class="clr"></div>

            <div class="row">
                <input id="hdnPanelObject" type="hidden" name="hddClick" runat="server" />
                <ajaxToolkit:ModalPopupExtender ID="mpeMain" runat="server" PopupControlID="pnlNewObject" TargetControlID="hdnPanelObject"
                    BackgroundCssClass="modalBackground" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>

                <asp:UpdatePanel ID="pnlObjectDetailUpdate" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel runat="server" ID="pnlNewObject" CssClass="modalPopup" Style="display: none;">
                            <div class="pnlheader">
                                <asp:Label ID="lblPanelTitle" runat="server" Text="Manage object form"></asp:Label>
                            </div>

                            <div class="pnlbody">
                                <!-- GESTIONE DELLA TABELLA CON DIV -->
                                <div class="divTable">
                                    <div class="divTableBody">
                                        <div class="divTableRow">
                                            <div class="divTableCell" style="width: 200px;">Object description</div>
                                            <div class="divTableCell" style="width: 580px;">
                                                <telerik:RadTextBox ID="txtObjectDescription" runat="server" RenderMode="Lightweight" ClientIDMode="Static"
                                                    TextMode="MultiLine" Height="100px" Resize="None">
                                                </telerik:RadTextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="pnlfooter">
                                <telerik:RadButton ID="btnHide" runat="server" Text="Close" Width="80px"
                                    RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                                </telerik:RadButton>

                                <telerik:RadButton ID="btnOK" runat="server" Text="Save" Width="80px"
                                    RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                                </telerik:RadButton>
                            </div>

                            <div style="text-align: center;">
                                <asp:Label runat="server" ID="txtMessageVariable" Visible="false"></asp:Label>
                            </div>

                        </asp:Panel>

                    </ContentTemplate>

                </asp:UpdatePanel>
            </div>

            <div class="clr"></div>

            <div class="row">
                <input id="hdnPanelTranslations" type="hidden" name="hddclickTranslation" runat="server" />
                <ajaxToolkit:ModalPopupExtender ID="mpeTranslate" runat="server" PopupControlID="pnlTranslate" TargetControlID="hdnPanelTranslations"
                    BackgroundCssClass="modalBackground" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>

                <asp:UpdatePanel ID="pnlVariablesTranslate" runat="server" UpdateMode="Conditional">

                    <ContentTemplate>
                        <asp:Panel runat="server" ID="pnlTranslate" CssClass="modalPopup" Style="display: none;">
                            <div class="pnlheader">
                                <asp:Label ID="lblTitleTranslation" runat="server" Text="Manage translation variable"></asp:Label>
                            </div>

                            <div class="pnlbody">
                                <!-- GESTIONE DELLA TABELLA CON DIV -->
                                <div class="divTable">
                                    <div class="divTableBody">
                                        <div class="divTableRow">
                                            <div class="divTableCell" style="width: 90%">
                                                <asp:Label ID="lblObjectName" runat="server" Text="Object name" Width="100px"></asp:Label>
                                                <telerik:RadTextBox ID="txtObjectNameTranslate" runat="server" RenderMode="Lightweight" Width="370px" ClientIDMode="Static"></telerik:RadTextBox>
                                            </div>
                                        </div>

                                        <div class="divTableRow">
                                            <div class="divTableCell" style="width: 90%">
                                                <asp:Label ID="lblSelectLanguage" runat="server" Text="Select language" Width="100px"></asp:Label>
                                                <telerik:RadComboBox ID="cboLanguage" runat="server" Width="370px" EmptyMessage="< Select language...>"
                                                    RenderMode="Lightweight" RenderingMode="Full" ZIndex="1000000" ClientIDMode="Static">
                                                </telerik:RadComboBox>
                                            </div>
                                        </div>

                                        <div class="divTableRow">
                                            <div class="divTableCell" style="width: 90%">
                                                <asp:Label ID="lblObjectTranslation" runat="server" Text="Object translation" Width="100px"></asp:Label>
                                                <telerik:RadTextBox ID="txtObjectTranslation" runat="server" RenderMode="Lightweight" Width="370px" ClientIDMode="Static"
                                                    TextMode="MultiLine" Height="100px" Resize="None">
                                                </telerik:RadTextBox>
                                            </div>
                                        </div>

                                        <div class="divTableRow">
                                            <div class="divTableCell" style="width: 90%">
                                                <asp:Label ID="lblTranslationAvailable" runat="server" Text="Translations available" Width="250px"></asp:Label>
                                                <asp:Panel ID="pnlGridTranslation" runat="server">
                                                    <telerik:RadGrid ID="grdTranslation" runat="server" RenderMode="Lightweight" AllowPaging="True" AllowSorting="True"
                                                        CellSpacing="0" GridLines="None" OnNeedDataSource="grdTranslations_NeedDataSource" Width="480px" PageSize="30">
                                                        <MasterTableView AutoGenerateColumns="false" TableLayout="Fixed" CssClass="RadGrid_WebBlue"
                                                            DataKeyNames="ID, IDLanguage, IDWebForm, ObjectName, ObjectDescription, ObejctTranslation, IDObjectWebForm"
                                                            NoMasterRecordsText="No translations available">

                                                            <Columns>
                                                                <telerik:GridButtonColumn CommandName="Modify" Text="Edit" UniqueName="EditColumnTranslation"
                                                                    ButtonType="ImageButton" ImageUrl="~/Content/GridTelerik/Edit.gif">
                                                                    <HeaderStyle Width="20px" />
                                                                </telerik:GridButtonColumn>
                                                            </Columns>

                                                            <Columns>
                                                                <telerik:GridBoundColumn DataField="ObejctTranslation" HeaderText="Translation" UniqueName="Translation">
                                                                    <HeaderStyle Width="230px" />
                                                                </telerik:GridBoundColumn>
                                                            </Columns>

                                                            <Columns>
                                                                <telerik:GridBoundColumn DataField="Code" HeaderText="Lang." UniqueName="Code">
                                                                    <HeaderStyle Width="80px" />
                                                                </telerik:GridBoundColumn>
                                                            </Columns>

                                                        </MasterTableView>
                                                    </telerik:RadGrid>

                                                    <asp:PlaceHolder runat="server" ID="MessageTranslation" Visible="false">
                                                        <p class="text-danger">
                                                            <asp:Literal runat="server" ID="txtMessageTranslation" />
                                                        </p>
                                                    </asp:PlaceHolder>

                                                </asp:Panel>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="pnlfooter">
                                <telerik:RadButton ID="btnHideTranslation" runat="server" Text="Close" Width="80px"
                                    RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                                </telerik:RadButton>

                                <telerik:RadButton ID="btnSaveTranslation" runat="server" Text="Save" Width="80px"
                                    RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                                </telerik:RadButton>
                            </div>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>

            <div class="row" style="min-height: 20px;"></div>

            <div class="row">
                <asp:PlaceHolder runat="server" ID="Message" Visible="false">
                    <p class="text-danger">
                        <asp:Literal runat="server" ID="MessageText" />
                    </p>
                </asp:PlaceHolder>
            </div>

            <div class="clr"></div>

            <div class="row">
                <div class="divTableCell" style="text-align: right;">
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>


