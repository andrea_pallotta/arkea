﻿
Imports System.Data
Imports Telerik.Web.UI
Imports APS
Imports APPCore.Utility

Partial Class PageTranslation
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster

    Private Sub ClearPanelTranslations()
        cboLanguage.SelectedValue = -1
        txtObjectTranslation.Text = ""
    End Sub

    Private Sub ClearPanel()
        txtObjectDescription.Text = ""
    End Sub

    Protected Sub grdFormsObject_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs)
        grdFormsObject.DataSource = LoadDataWebForms()
    End Sub

    Protected Sub grdTranslations_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs)
        grdTranslation.DataSource = LoadDataTranslation()
    End Sub

    Private Sub LoadLanguages()
        Dim sSQL As String
        Dim oDTLanguages As DataTable

        sSQL = "SELECT -1 AS ID, '' AS Language UNION ALL SELECT ID, Language FROM Languages ORDER BY ID"
        oDTLanguages = g_DAL.ExecuteDataTable(sSQL)

        cboLanguage.DataSource = oDTLanguages
        cboLanguage.DataValueField = "ID"
        cboLanguage.DataTextField = "Language"
        cboLanguage.DataBind()

        cboLanguage.SelectedValue = -1

    End Sub

    Private Sub PageTranslation_Init(sender As Object, e As EventArgs) Handles Me.Init
        m_SessionData = Session("SessionData")
    End Sub

    Private Sub PageTranslation_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsNothing(m_SessionData) Then
            PageRedirect("ErrorTimeout.aspx", "", "")
        End If

        If Not Page.IsPostBack Then
            LoadLanguages()
            lblTitolo.Text &= " - " & Session("PageOrigin")
        End If
    End Sub

    Private Function LoadDataTranslation() As DataTable
        Dim sSQL As String

        If Not IsNothing(Session("IDWebForm")) AndAlso Nni(Session("IDWebForm")) > 0 Then
            sSQL = "SELECT WF.ID, WFT.IDWebForm, WFT.IDLanguage, WF.ObjectName, WF.ObjectDescription, WFT.ObejctTranslation, L.Code, WFT.ID AS IDObjectWebForm " _
                 & "FROM WebForms WF " _
                 & "LEFT JOIN WebForms_Translation WFT ON WF.Id = WFT.IDWebForm " _
                 & "LEFT JOIN Languages L ON WFT.IDLanguage = L.ID " _
                 & "WHERE WFT.IDWebForm = " & Nni(Session("IDWebForm")) _
                 & " AND ISNULL(WFT.IDGame, " & Nni(Session("IDGame")) & ") = " & Nni(Session("IDGame"))
            Return g_DAL.ExecuteDataTable(sSQL)

        Else
            Return Nothing
        End If
    End Function

    Private Function LoadDataWebForms() As DataTable
        Dim sSQL As String

        sSQL = "SELECT * " _
             & "FROM WebForms WHERE FormName = '" & Session("PageOrigin") & "' AND ISNULL(CONVERT(Varchar(Max), ObjectDescription), '') <> '' "

        Return g_DAL.ExecuteDataTable(sSQL)
    End Function

    Private Sub grdFormsObject_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles grdFormsObject.ItemCommand
        If e.CommandName.ToUpper <> "PAGE" AndAlso e.CommandName.ToUpper <> "SORT" AndAlso e.CommandName.ToUpper <> "FILTER" Then
            Session("ItemSelected") = DirectCast(e.Item, GridDataItem)

            If e.CommandName.ToUpper = "MODIFY" Then
                ClearPanel()
                mpeMain.Show()
                UpdatePanelMain.Update()

            ElseIf e.CommandName.ToUpper = "TRANSLATE" Then
                ClearPanelTranslations()
                mpeTranslate.Show()
                UpdatePanelMain.Update()

            End If

        End If
    End Sub

    Private Sub pnlObjectDetailUpdate_PreRender(sender As Object, e As EventArgs) Handles pnlObjectDetailUpdate.PreRender
        Dim oItem As GridDataItem = DirectCast(Session("ItemSelected"), GridDataItem)

        ' Procedo con il caricamento dei dati da inserire nel panello
        ' Recupero l'id della variabile selezionata
        If Not oItem Is Nothing AndAlso Not lblPanelTitle Is Nothing Then
            ' Controllo che sia stato selezionato effettivamente un item, altrimenti sono nel caricamento della pagina
            lblPanelTitle.Text = "Manage object form - " & oItem.GetDataKeyValue("ObjectName").ToString

            Session("IDObject") = oItem.GetDataKeyValue("Id")

            txtObjectDescription.Text = Nz(oItem.GetDataKeyValue("ObjectDescription"))
        End If

    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        ' Procedo con il savataggio delle informazioni
        Dim sSQL As String = ""

        ' Controllo se andare in modifica o inserimento
        If Nni(Session("IDObject")) > 0 Then ' Modifica
            sSQL = "UPDATE WebForms SET " _
                 & "ObjectDescription = '" & HttpContext.Current.Server.HtmlEncode(CStrSql(txtObjectDescription.Text)) & "' " _
                 & "WHERE ID = " & Nni(Session("IDObject"))

        End If
        g_DAL.ExecuteNonQuery(sSQL)

        ClearPanel()

        grdFormsObject.DataSource = Nothing
        grdFormsObject.Rebind()

        txtMessageVariable.Text = "Update completed"
        txtMessageVariable.Visible = True

        UpdatePanelMain.Update()

        mpeMain.Hide()

    End Sub

    Private Sub pnlTranslate_PreRender(sender As Object, e As EventArgs) Handles pnlTranslate.PreRender
        Dim oItem As GridDataItem = DirectCast(Session("ItemSelected"), GridDataItem)
        Dim oItemTranslation As GridDataItem = DirectCast(Session("ItemTranslationSelected"), GridDataItem)

        ' Prima carico i dati della variabile selezionata, poi controllo se ci sono delle traduzioni già inserite
        If Not oItem Is Nothing Then
            Session("IDWebForm") = oItem.GetDataKeyValue("Id")
            If Not IsNothing(oItemTranslation) Then
                Session("IDObjectWebForm") = oItemTranslation.GetDataKeyValue("IDObjectWebForm")
            Else
                Session("IDObjectWebForm") = 0
            End If
            lblPanelTitle.Text = "Manage translation object - " & oItem.GetDataKeyValue("ObjectName").ToString

            ' Carico tutti i dati della traduzione
            txtObjectNameTranslate.Text = Nz(oItem.GetDataKeyValue("ObjectName"))
            txtObjectNameTranslate.ReadOnly = True
        End If

        MessageTranslation.Visible = False

        grdTranslation.DataSource = Nothing
        grdTranslation.Rebind()

    End Sub

    Private Sub btnSaveTranslation_Click(sender As Object, e As EventArgs) Handles btnSaveTranslation.Click
        ' Procedo con il savataggio delle informazioni
        Dim sSQL As String

        If Nz(cboLanguage.SelectedValue) = "" OrElse Nni(cboLanguage.SelectedValue) <= 0 Then
            txtMessageTranslation.Text = "Please select a language"
            MessageTranslation.Visible = True
            mpeTranslate.Show()
            UpdatePanelMain.Update()

            Exit Sub
        End If

        ' Controllo se andare in modifica o inserimento
        ' Controllo l'esistenza dell'oggetto, per la lingua di traduzione nel database
        sSQL = "SELECT ID FROM WebForms_Translation WHERE IDWebForm = " & Nni(Session("IDWebForm")) & " AND IDLanguage = " & Nni(cboLanguage.SelectedValue)
        Dim iIDFind As Integer = g_DAL.ExecuteScalar(sSQL)

        If Nni(Session("IDObjectWebForm")) > 0 AndAlso iIDFind > 0 Then ' Modifica
            sSQL = "UPDATE WebForms_Translation Set ObejctTranslation = '" & HttpContext.Current.Server.HtmlEncode(txtObjectTranslation.Text) & "' " _
                 & "WHERE ID = " & Nni(Session("IDObjectWebForm"))
        Else ' Inserimento
            sSQL = "INSERT INTO WebForms_Translation (IDWebForm, IDLanguage, ObejctTranslation, IDGame) VALUES (" _
                 & "" & Nni(Session("IDWebForm")) & ", " & Nni(cboLanguage.SelectedValue) & ", '" & HttpContext.Current.Server.HtmlEncode(txtObjectTranslation.Text) & "', " _
                 & Nni(Session("IDGame")) & ") "
        End If
        g_DAL.ExecuteNonQuery(sSQL)

        If Nni(Session("IDObjectWebForm"), 0) = 0 Then ' Sono in inserimento, procedo con il caricamento dell'ultimo id inserito
            sSQL = "SELECT MAX(ID) FROM Variables_Translations "
            Session("IDObjectWebForm") = Nni(g_DAL.ExecuteScalar(sSQL))
        End If

        grdTranslation.DataSource = Nothing
        grdTranslation.Rebind()

        txtMessageTranslation.Text = "Update completed"
        MessageTranslation.Visible = True

        ClearPanelTranslations()

        pnlVariablesTranslate.Update()

        mpeTranslate.Show()
        pnlVariablesTranslate.Update()
        UpdatePanelMain.Update()
    End Sub

    Private Sub grdTranslation_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles grdTranslation.ItemCommand
        If e.CommandName.ToUpper <> "PAGE" Then
            If e.CommandName.ToUpper = "MODIFY" Then
                Dim oItem As GridDataItem = DirectCast(e.Item, GridDataItem)
                Session("ItemTranslationSelected") = DirectCast(e.Item, GridDataItem)

                ' Procedo con il caricamento dei dati da inserire nel panello
                ' Recupero l'id della variabile selezionata
                If Not oItem Is Nothing Then
                    txtObjectTranslation.Text = HttpContext.Current.Server.HtmlDecode(oItem.GetDataKeyValue("ObejctTranslation"))
                    cboLanguage.SelectedValue = Nni(oItem.GetDataKeyValue("IDLanguage"))
                End If

                mpeTranslate.Show()
                UpdatePanelMain.Update()

            End If
        End If
    End Sub

End Class
