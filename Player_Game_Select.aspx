﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Player_Game_Select.aspx.vb" Inherits="Player_Game_Select" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="cntMain" ContentPlaceHolderID="MainContent" runat="Server" ClientIDMode="Static">

    <asp:UpdatePanel ID="pnlMain" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <h2>
                <asp:Label ID="lblWelcome" runat="server" Text=""></asp:Label>
            </h2>

            <div class="clr"></div>

            <div class="divTable">
                <div class="divTableRow">
                    <h3>
                        <asp:Label ID="lblTitolo" runat="server" Text="Select game"></asp:Label>
                    </h3>
                </div>
            </div>


            <div class="row">

                <div class="row" style="padding-bottom: 5px;">
                    <asp:Panel ID="pnlTable" runat="server">
                        <telerik:RadGrid ID="grdGames" runat="server" RenderMode="Lightweight" AllowPaging="True" AllowSorting="True" EnableLinqExpressions="false"
                            OnNeedDataSource="grdGames_NeedDataSource" Width="670px" AllowFilteringByColumn="True" Culture="it-IT" GroupPanelPosition="Top" PageSize="50">
                            <MasterTableView AutoGenerateColumns="false" TableLayout="Fixed" CssClass="RadGrid_WebBlue"
                                DataKeyNames="ID, IDRole">

                                <Columns>
                                    <telerik:GridButtonColumn CommandName="Select" Text="Select" UniqueName="SelectColumn"
                                        ButtonType="ImageButton" ImageUrl="Images/Conferma_Verde.png">
                                        <HeaderStyle Width="20px" />
                                    </telerik:GridButtonColumn>
                                </Columns>

                                <Columns>
                                    <telerik:GridBoundColumn DataField="Gioco" HeaderText="Game" UniqueName="Gioco"
                                        FilterControlWidth="120px" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                        <HeaderStyle Width="110px" />
                                    </telerik:GridBoundColumn>
                                </Columns>

                                <Columns>
                                    <telerik:GridBoundColumn DataField="Modello" HeaderText="Model" UniqueName="Modello"
                                        FilterControlWidth="130px" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                        <HeaderStyle Width="120px" />
                                    </telerik:GridBoundColumn>
                                </Columns>

                                <Columns>
                                    <telerik:GridBoundColumn DataField="DataInzioFormat" HeaderText="Data inizio" UniqueName="DataInzioFormat"
                                        FilterControlWidth="90px" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                        <HeaderStyle Width="80px" />
                                    </telerik:GridBoundColumn>
                                </Columns>

                                <Columns>
                                    <telerik:GridBoundColumn DataField="DataFineFormat" HeaderText="Def. value" UniqueName="DataFineFormat"
                                        FilterControlWidth="90px" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                        <HeaderStyle Width="80px" />
                                    </telerik:GridBoundColumn>
                                </Columns>

                                <Columns>
                                    <telerik:GridBoundColumn DataField="TeamName" HeaderText="Team name" UniqueName="TeamName"
                                        FilterControlWidth="130px" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                        <HeaderStyle Width="120px" />
                                    </telerik:GridBoundColumn>
                                </Columns>

                                <Columns>
                                    <telerik:GridBoundColumn DataField="Ruolo" HeaderText="Role" UniqueName="Ruolo"
                                        FilterControlWidth="130px" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                        <HeaderStyle Width="100px" />
                                    </telerik:GridBoundColumn>
                                </Columns>

                                <%--COLONNE NASCOSTE--%>
                                <Columns>
                                    <telerik:GridBoundColumn DataField="ID" HeaderText="ID" UniqueName="ID" Visible="false" />
                                </Columns>

                                <Columns>
                                    <telerik:GridBoundColumn DataField="IDRole" HeaderText="IDRole" UniqueName="IDRole" Visible="false" />
                                </Columns>

                                <Columns>
                                    <telerik:GridBoundColumn DataField="DataInizio" HeaderText="DataInizio" UniqueName="DataInizio" Visible="false" />
                                </Columns>

                                <Columns>
                                    <telerik:GridBoundColumn DataField="DataFine" HeaderText="DataFine" UniqueName="DataFine" Visible="false" />
                                </Columns>

                                <Columns>
                                    <telerik:GridBoundColumn DataField="IDPlayer" HeaderText="IDPlayer" UniqueName="IDPlayer" Visible="false" />
                                </Columns>

                            </MasterTableView>
                        </telerik:RadGrid>

                    </asp:Panel>
                </div>

                <asp:PlaceHolder runat="server" ID="Message" Visible="false">
                    <p class="text-danger">
                        <asp:Literal runat="server" ID="MessageText" />
                    </p>
                </asp:PlaceHolder>
            </div>

            <telerik:RadWindow RenderMode="Lightweight" ID="modalHelp" runat="server" Width="520px" Height="450px" CenterIfModal="false"
                Style="z-index: 100001;" BorderStyle="None" Behaviors="Close, Move" Title="Help">
                <ContentTemplate>
                    <div style="padding: 10px; text-align: left;">
                        <div id="divTableMaster" runat="server" style="margin-bottom: 10px;">
                            <asp:Label runat="server" ID="lblHelp" Visible="true">

                            </asp:Label>
                        </div>
                    </div>

                </ContentTemplate>
            </telerik:RadWindow>

        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
