﻿
Imports Telerik.Web.UI
Imports System.Data
Imports APS
Imports APPCore.Utility
Imports DALC4NET

Partial Class Player_Game_Select
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster

    Private Function LoadDataGames() As DataTable
        Dim sSQL As String
        Dim oDT As DataTable

        sSQL = "SELECT G.ID, G.Descrizione AS Gioco, G.DataInizio, G.DataFine, PG.IDPlayer, T.TeamName, R.Descrizione AS Ruolo, M.Descrizione AS Modello, " _
             & "CASE WHEN ISNULL(G.DataInizio, '') <> '' THEN RIGHT(G.DataInizio, 2) + '/' + SUBSTRING(G.DataInizio, 5, 2) + '/' + LEFT(G.DataInizio, 4) ELSE '' END AS DataInzioFormat, " _
             & "CASE WHEN ISNULL(G.DataFine, '') <> '' THEN RIGHT(G.DataFine, 2) + '/' + SUBSTRING(G.DataFine, 5, 2) + '/' + LEFT(G.DataFine, 4) ELSE '' END AS DataFineFormat, " _
             & "PG.IDRole " _
             & "FROM BGOL_Games G " _
             & "INNER JOIN BGOL_Players_Games PG ON G.Id = PG.IDGame " _
             & "INNER JOIN BGOL_Players_Teams_Games PTG ON PG.IDGame = PTG.IdGame AND PG.IDPlayer = PTG.IdPlayer " _
             & "INNER JOIN BGOL_Teams T ON PTG.IdTeam = T.Id " _
             & "INNER JOIN BGOL_Roles R ON PG.IDRole = R.Id " _
             & "INNER JOIN BGOL_Models M ON G.IdModel = M.Id " _
             & "WHERE PG.IDPlayer = " & m_SessionData.Utente.ID
        oDT = g_DAL.ExecuteDataTable(sSQL)

        Return oDT
    End Function

    Protected Sub grdGames_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs)
        grdGames.DataSource = LoadDataGames()
    End Sub

    Private Sub Player_Game_Select_Init(sender As Object, e As EventArgs) Handles Me.Init
        m_SessionData = Session("SessionData")
    End Sub

    Private Sub Player_Game_Select_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsNothing(m_SessionData) Then
            PageRedirect("ErrorTimeout.aspx", "", "")
        End If

        If Not Page.IsPostBack Then
            m_SiteMaster = Me.Master
            LoadDataGames()
            m_SiteMaster.LoadMenuSidebarRight(False)
            m_SiteMaster.SetComboPeriodTeamInvisible()
            m_SiteMaster.PageTitle = "Selection game"
        End If
    End Sub

    Private Sub grdGames_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles grdGames.ItemCommand
        If e.CommandName.ToUpper <> "PAGE" AndAlso e.CommandName.ToUpper <> "SORT" AndAlso e.CommandName.ToUpper <> "FILTER" Then
            Dim oItem As GridDataItem = DirectCast(e.Item, GridDataItem)

            Dim sSQL As String

            If e.CommandName.ToUpper = "SELECT" Then
                m_SessionData.Utente.Games.IDGame = oItem.GetDataKeyValue("ID")

                Session("SessionData") = m_SessionData
                Session("IDGame") = m_SessionData.Utente.Games.IDGame
                Session("IDPlayer") = m_SessionData.Utente.ID
                Session("IDTeam") = GetIDTeam(oItem.GetDataKeyValue("ID"), Session("IDPlayer"))
                Session("IDPeriodValid") = HandleGetMaxPeriodValid(Nni(Session("IDGame")))
                Session("IDRole") = oItem.GetDataKeyValue("IDRole")
                Session("IDRole") = oItem.GetDataKeyValue("IDRole")
                sSQL = "SELECT TOP 1 DBV.Value " _
                     & "FROM Decisions_Developer D " _
                     & "LEFT JOIN Decisions_Developer_Value DBV ON D.Id = DBV.IDDecisionDeveloper " _
                     & "WHERE D.VariableName = 'PeriodLength' AND IDGame = " & Session("IDGame")
                Session("LunghezzaPeriodo") = Nni(g_DAL.ExecuteScalar(sSQL))

                ' Carico tutte le informazioni legate ai game del player appena collegato
                Session("SessionData") = m_SessionData
                PageRedirect("Default.aspx", "", "")
            End If
        End If
    End Sub

End Class
