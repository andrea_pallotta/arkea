﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Players_Decision_Results.aspx.vb" Inherits="Players_Decision_Results" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="cntMain" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:UpdatePanel runat="server" ID="UpdatePanelMain" UpdateMode="Conditional">
        <ContentTemplate>
            <h2>
                <asp:Label ID="lblWelcome" runat="server" Text="Players" />
            </h2>

            <div class="clr"></div>

            <div id="subTitle">
                <div id="subLeft" style="float: left; width: 20%;">
                    <h3>
                        <asp:Label ID="lblTitle" runat="server" Text="Player results"></asp:Label>
                    </h3>
                </div>

                <div id="subRight" style="float: right; width: 80%;">
                    <div id="divRight" style="margin-top: 10px; margin-bottom: 5px; text-align: right;">
                    </div>
                </div>
            </div>

            <div class="clr"></div>

            <div class="divTable">
                <div class="divTableHeading">
                    <div class="divTableRow">
                        <div class="divTableCell">
                            <telerik:RadTabStrip ID="tsMain" runat="server" RenderMode="Lightweight" MultiPageID="rmpMain">
                                <Tabs>
                                    <telerik:RadTab Text="Sales" Width="200px"></telerik:RadTab>
                                    <telerik:RadTab Text="Marketing" Width="200px"></telerik:RadTab>
                                </Tabs>
                            </telerik:RadTabStrip>
                        </div>

                    </div>
                </div>

                <div class="divTableRow">
                    <div class="divTableCell">
                        <telerik:RadMultiPage runat="server" ID="rmpMain" SelectedIndex="0">
                            <telerik:RadPageView runat="server" ID="rpvSales">
                                <div>
                                    <asp:Image runat="server" ID="imgHeaderSales" ImageUrl="Images/Sales1.jpg" />
                                </div>

                                <h4>Retail sales</h4>
                                <div id="divTableMasterSales" runat="server">
                                </div>
                            </telerik:RadPageView>

                            <telerik:RadPageView runat="server" ID="rpvMarketing">
                                <div>
                                    <asp:Image runat="server" ID="Image1" ImageUrl="Images/Marketing.jpg" />
                                </div>
                                <h4>Goods available</h4>
                                <div id="divTableMasterMarketing" runat="server">
                                </div>
                            </telerik:RadPageView>
                        </telerik:RadMultiPage>
                    </div>

                </div>

                <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanelMain">
                    <ProgressTemplate>
                        <asp:Panel ID="Panel1" CssClass="overlay" runat="server">
                            <asp:Panel ID="Panel2" CssClass="loader" runat="server">
                                <img alt="" src="Images/Loading.gif" />
                            </asp:Panel>
                        </asp:Panel>
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </div>
            <asp:PlaceHolder runat="server" ID="Message" Visible="false">
                <p class="text-danger">
                    <asp:Literal runat="server" ID="MessageText" />
                </p>
            </asp:PlaceHolder>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

