﻿Imports System.Data
Imports APS
Imports APPCore.Utility
Imports DALC4NET
Imports Telerik.Web.UI

Partial Class Players_Decision_Results
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData
    Private m_Separator As String = "."

    Private m_IDPeriodoMax As Integer

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        HandleLoadDataMarketing()
        HandleLoadDataSales()
        UpdatePanelMain.Update()
    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        HandleLoadDataMarketing()
        HandleLoadDataSales()
        UpdatePanelMain.Update()
    End Sub

    Private Sub HandleLoadDataSales()
        Dim oDTResults As DataTable
        Dim sSQL As String

        g_DAL = New DBHelper
        Try
            divTableMasterSales.Controls.Clear()

            Dim oSiteMaster As SiteMaster = Me.Master

            If m_IDPeriodoMax > 1 AndAlso Nni(oSiteMaster.PeriodGetPrev) >= 1 Then
                ' Controllo il periodo di elaborazione
                If Session("IDRole") <> "P" Then
                    sSQL = "SELECT * FROM vResults_Sales_Boss WHERE IDGame = " & m_SessionData.Utente.Games.IDGame
                Else
                    sSQL = "SELECT * FROM vResults_Sales_Player WHERE IDGame = " & m_SessionData.Utente.Games.IDGame & " " _
                     & " AND IDPlayer = " & Nni(Session("IDTeam"))
                End If
                sSQL &= " AND IDCategory = 2 " _
                 & "AND IDPeriod = " & oSiteMaster.PeriodGetPrev & " " _
                 & "ORDER BY TeamName, Variable, Item "

                oDTResults = g_DAL.ExecuteDataTable(sSQL)

                ' Converto i valori dei risultati da varchar a numerici con con decimali
                For Each oRow As DataRow In oDTResults.Rows
                    oRow("ValueDef") = Math.Round(Nn(oRow("Value"), "0"), 2)
                Next

                Dim oPivot As New Pivot(oDTResults)
                Dim dtPivot As DataTable = oPivot.PivotData("Variable", "ValueDef", AggregateFunction.Sum, "TeamName", "Item")

                Dim oGridViewSales As New GridView
                AddHandler oGridViewSales.RowCreated, AddressOf grdPivot_RowCreated
                AddHandler oGridViewSales.RowDataBound, AddressOf grdPivot_RowDataBound
                oGridViewSales.DataSource = dtPivot
                oGridViewSales.DataBind()

                divTableMasterSales.Controls.Add(oGridViewSales)

                oPivot = Nothing

                SQLConnClose(g_DAL.GetConnObject)

            End If

        Catch ex As Exception
            MessageText.Text = ex.Message & "<br/>" & g_MessaggioErrore & "<br/> "
            Message.Visible = True
        End Try

    End Sub

    Private Sub HandleLoadDataMarketing()
        Dim oDTResults As DataTable
        Dim sSQL As String

        Try
            divTableMasterMarketing.Controls.Clear()

            Dim oSiteMaster As SiteMaster = Me.Master

            If m_IDPeriodoMax > 1 AndAlso Nni(oSiteMaster.PeriodGetPrev) >= 1 Then
                If Session("IDRole") <> "P" Then
                    sSQL = "SELECT * FROM vResults_Marketing_Boss WHERE IDGame = " & m_SessionData.Utente.Games.IDGame
                Else
                    sSQL = "SELECT * FROM vResults_Marketing_Player WHERE IDGame = " & m_SessionData.Utente.Games.IDGame & " " _
                         & " AND IDPlayer = " & Nni(Session("IDTeam"))
                End If
                sSQL &= " AND IDCategory = 3 " _
                     & "AND IDPeriod = " & oSiteMaster.PeriodGetPrev & " " _
                     & "ORDER BY TeamName, Variable, Item"

                oDTResults = g_DAL.ExecuteDataTable(sSQL)

                ' Converto i valori dei risultati da varchar a numerici con con decimali
                For Each oRow As DataRow In oDTResults.Rows
                    oRow("ValueDef") = Math.Round(Nn(oRow("Value")), 2)
                Next

                Dim oPivot As New Pivot(oDTResults)
                Dim dtPivot As DataTable = oPivot.PivotData("Variable", "ValueDef", AggregateFunction.Sum, "TeamName", "Item")

                Dim oGridViewMkt As New GridView
                AddHandler oGridViewMkt.RowCreated, AddressOf grdPivot_RowCreated
                AddHandler oGridViewMkt.RowDataBound, AddressOf grdPivot_RowDataBound
                oGridViewMkt.DataSource = dtPivot
                oGridViewMkt.DataBind()

                divTableMasterMarketing.Controls.Add(oGridViewMkt)

                SQLConnClose(g_DAL.GetConnObject)
            End If

        Catch ex As Exception
            MessageText.Text = ex.Message & "<br/>" & g_MessaggioErrore & "<br/> "
            Message.Visible = True
        End Try

    End Sub

    Protected Sub grdPivot_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.Header Then
            MergeHeader(DirectCast(sender, GridView), e.Row, 2)
        End If
    End Sub

    Protected Sub grdPivot_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            If e.Row.DataItemIndex >= 0 Then
                e.Row.Cells(0).BackColor = System.Drawing.ColorTranslator.FromHtml("#55A2DF")
                e.Row.Cells(0).ForeColor = System.Drawing.ColorTranslator.FromHtml("#fff")
            End If
        End If
    End Sub

    Private Sub MergeHeader(ByVal gv As GridView, ByVal row As GridViewRow, ByVal PivotLevel As Integer)
        Dim iCount As Integer = 1
        Dim iContaColonneHeader = 0

        For iCount = 1 To PivotLevel
            Dim oGridViewRow As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
            Dim Header = (row.Cells.Cast(Of TableCell)().[Select](Function(x) GetHeaderText(x.Text, iCount, PivotLevel))).GroupBy(Function(x) x)

            For Each v In Header
                Dim cell As New TableHeaderCell()
                If iContaColonneHeader > 0 Then
                    cell.Text = v.Key.Substring(v.Key.LastIndexOf(m_Separator) + 1)
                Else
                    cell.Text = ""
                End If

                cell.ColumnSpan = v.Count()

                oGridViewRow.Cells.Add(cell)
                iContaColonneHeader += 1
            Next
            gv.Controls(0).Controls.AddAt(row.RowIndex, oGridViewRow)
        Next
        row.Visible = False
    End Sub

    Private Function GetHeaderText(ByVal s As String, ByVal i As Integer, ByVal PivotLevel As Integer) As String
        If Not s.Contains(m_Separator) AndAlso i <> PivotLevel Then
            Return String.Empty
        Else
            Dim Index As Integer = NthIndexOf(s, m_Separator, i)
            If Index = -1 Then
                Return s
            End If
            Return s.Substring(0, Index)
        End If
    End Function

    ''' <summary>
    ''' Returns the nth occurance of the SubString from string str
    ''' </summary>
    ''' <param name="str">source string</param>
    ''' <param name="SubString">SubString whose nth occurance to be found</param>
    ''' <param name="n">n</param>
    ''' <returns>Index of nth occurance of SubString if found else -1</returns>
    Private Function NthIndexOf(ByVal str As String, ByVal SubString As String, ByVal n As Integer) As Integer
        Dim x As Integer = -1
        For i As Integer = 0 To n - 1
            x = str.IndexOf(SubString, x + 1)
            If x = -1 Then
                Return x
            End If
        Next
        Return x
    End Function

    Private Sub Players_Decision_Results_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsNothing(m_SessionData) Then
            PageRedirect("ErrorTimeout.aspx", "", "")
        End If
    End Sub

    Private Sub Players_Decision_Results_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        m_SessionData = Session("SessionData")

        Dim sSQL As String

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oSiteMaster As SiteMaster = Me.Master
        Dim oCBOPeriod As RadComboBox = DirectCast(oSiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(oSiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler oSiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler oSiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        m_IDPeriodoMax = HandleGetMaxPeriodValid(m_SessionData.Utente.Games.IDGame) ' Ultimo periodo di gioco
        Dim iIDPeriodoMaxGame As Integer = HandleGetMaxPeriodGame(m_SessionData.Utente.Games.IDGame) ' Periodo massimo del game

        If iIDPeriodoMaxGame = m_IDPeriodoMax OrElse m_IDPeriodoMax = 1 Then
            ' Controllo di avere dei risultati nel caso sia nell'ultimo periodo
            ' altrimenti vuol dire che lo sto ancora giocando e non posso mostrare alcun risultato

            sSQL = "SELECT TOP 1 * FROM vResults_Sales_Boss WHERE IDGame = " & m_SessionData.Utente.Games.IDGame & " " _
                 & "AND IDPeriod = " & m_IDPeriodoMax & " "
            Dim oDAL As New DBHelper
            Dim oDTResult As DataTable = oDAL.ExecuteDataTable(sSQL)
            SQLConnClose(oDAL.GetConnObject)
            If oDTResult.Rows.Count > 0 Then
                oCBOPeriod.SelectedValue = m_IDPeriodoMax
                oSiteMaster.PeriodChange = m_IDPeriodoMax
            Else
                oCBOPeriod.SelectedValue = m_IDPeriodoMax - 1
                oSiteMaster.PeriodChange = m_IDPeriodoMax - 1
            End If
        Else
            oCBOPeriod.SelectedValue = m_IDPeriodoMax - 1
            oSiteMaster.PeriodChange = m_IDPeriodoMax - 1
        End If

        HandleLoadDataSales()
        HandleLoadDataMarketing()
    End Sub

End Class
