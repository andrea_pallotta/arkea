﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Players_Decisions.aspx.vb" Inherits="Players_Decisions_Demo" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="cntMain" ContentPlaceHolderID="MainContent" runat="Server" ClientIDMode="Static">
    
    <asp:UpdatePanel ID="pnlMain" runat="server" UpdateMode="Conditional" >
        <ContentTemplate>
            <h2>
                <asp:Label ID="lblWelcome" runat="server" Text="Player decision"></asp:Label>
            </h2>

            <div class="clr"></div>

            <h3>
                <asp:Label ID="lblTitolo" runat="server" Text="Player decision"></asp:Label>
            </h3>

            <div class="row">
                <asp:Table ClientIDMode="Static" ID="tblPlayerDecision" runat="server">
                </asp:Table>

                <div id="divConfirmButton" style="margin-top: 10px; margin-bottom: 5px; text-align: center;">
                    <telerik:RadButton ID="btnSave" runat="server" Text="Confirm" Width="60%"
                        RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                    </telerik:RadButton>
                </div>

                <asp:PlaceHolder runat="server" ID="Message" Visible="false">
                    <p class="text-danger">
                        <asp:Literal runat="server" ID="MessageText" />
                    </p>
                </asp:PlaceHolder>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>