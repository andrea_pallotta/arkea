﻿Imports System.Data
Imports APS
Imports APPCore.Utility
Imports DALC4NET
Imports Telerik.Web.UI

Partial Class Players_Decisions_Demo
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        tblPlayerDecision.Rows.Clear()
        LoadDataPlayerDecisions()
        Message.Visible = False

        If Nni(Session("IDPeriod")) = HandleGetMaxPeriodValid(Session("IDGame")) Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        pnlMain.Update()
    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        tblPlayerDecision.Rows.Clear()
        LoadDataPlayerDecisions()
        Message.Visible = False
        pnlMain.Update()
    End Sub

    Private Sub Players_Decisions_Init(sender As Object, e As EventArgs) Handles Me.Init
        m_SessionData = Session("SessionData")

        m_SiteMaster = Me.Master

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oCBOPeriod As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler m_SiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler m_SiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(m_SessionData.Utente.Games.IDGame) ' Ultimo periodo
        oCBOPeriod.SelectedValue = Session("IDPeriodValid")
        m_SiteMaster.PeriodChange = Session("IDPeriodValid")

        Session("IDTeam") = GetIDTeam(m_SessionData.Utente.Games.IDGame, m_SessionData.Utente.ID)
        Session("NameTeam") = GetTeamName(Session("IDTeam"))
        oCBOTeam.SelectedValue = Session("IDTeam")
        m_SiteMaster.TeamChange = Session("IDTeam")

        LoadDataPlayerDecisions()

    End Sub

    Private Sub LoadDataPlayerDecisions()
        Dim oDTDecisionsPlayer As DataTable
        Dim oDTDecisionsPlayerValue As DataTable

        Dim sHeader As String = ""

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Dim sNomeVariabile As String
        Dim sTraduzione As String
        Dim sTraduzioneExternalObject As String

        ' Carico la tabella delle intestazioni da usare
        oDTDecisionsPlayer = HandleLoadGameDecisionPlayer(m_SessionData.Utente.Games.IDGame, 0)

        ' Recupero le informazioni necessarie per costruirmi l'header della tabella HTML
        Dim oDTHeader As DataTable = oDTDecisionsPlayer

        ' Per ogni riga di intestazione inizio a costruire la tabella
        For Each oRowHeader As DataRow In oDTHeader.Rows
            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            If Nz(oRowHeader("VariableGroupLabel")) = "" Then
                tblHeaderCell.Text = "Decision player"
            Else
                tblHeaderCell.Text = Nz(oRowHeader("VariableGroupLabel"))
            End If

            tblHeaderCell.ColumnSpan = 3
            tblHeaderCell.BorderStyle = BorderStyle.None

            tblHeaderRow.Cells.Add(tblHeaderCell)
            tblHeaderRow.BorderStyle = BorderStyle.None
            tblHeaderRow.Style.Add("margin-top", "10px")

            ' Aggiungo la l'header solo se ho del testo di identificazione del gruppo
            If tblHeaderCell.Text <> "" AndAlso sHeader <> Nz(oRowHeader("VariableGroupLabel")) Then tblPlayerDecision.Rows.Add(tblHeaderRow)

            ' Carico i valori della variabile, e controllo eventualmente se è una variabile dipendente da qualche lista
            oDTDecisionsPlayerValue = HandleLoadGamePeriodDecisionPlayerValue(m_SiteMaster.PeriodGetCurrent, Nni(oRowHeader("ID")))

            ' Ciclo sui valori delle varibili trovate
            For Each oRowVariableValue As DataRow In oDTDecisionsPlayerValue.Rows
                sTraduzione = GetVariableTranslation(Session("IDGame"), Nni(oRowVariableValue("IDVariable")), Nz(Session("LanguageActive")))
                If Nz(oRowVariableValue("VariableValue")).ToUpper = "[LIST OF ITEMS]" Then ' Lista di ITEMS/Product
                    ' Carico i valori presenti nella tabella Decisions_Players_value_List
                    Dim oDTValoriPLayer As DataTable = HandleGetValueDecisionPlayer(Nni(oRowVariableValue("ID")), Nni(Session("IDTeam")), m_SiteMaster.PeriodGetCurrent)

                    If oDTValoriPLayer.Rows.Count > 0 Then
                        For Each oROWValore As DataRow In oDTValoriPLayer.Rows
                            ' Recupero la traduzione corretta dell'item 
                            sTraduzioneExternalObject = GetItemTranslation(Nni(oROWValore("IDItem")), Nz(Session("LanguageActive")))

                            'sNomeVariabile = "var_" & Nz(Session("NameTeam") & "_" & Nni(oRowHeader("Id")) & "_" & Nz(oROWValore("VariableName"))).Replace(" ", "")
                            sNomeVariabile = Nz(oROWValore("VariableName"))
                            ' Etichetta della variabile
                            tblRow = New TableRow
                            tblCell = New TableCell

                            ' Recupero la traduzione in lingua
                            tblCell.Text = sTraduzione & " - " & sTraduzioneExternalObject
                            tblCell.BorderStyle = BorderStyle.None
                            tblRow.Cells.Add(tblCell)

                            ' Creo i controlli necessari ed eventualmente li popolo
                            ' Carico i valori esistenti, altrimenti li creo
                            Dim oTxtBox As New TextBox
                            tblCell = New TableCell
                            oTxtBox.Text = Nz(oROWValore("Value"))
                            oTxtBox.ID = Nz(sNomeVariabile)
                            oTxtBox.Attributes.Add("runat", "server")
                            oTxtBox.Style.Add("text-align", "right")
                            oTxtBox.ClientIDMode = ClientIDMode.Static
                            oTxtBox.EnableViewState = True

                            ' Aggiungo il textbox appena creato alla cella
                            tblCell.Controls.Add(oTxtBox)
                            tblCell.Style.Add("text-align", "right")
                            tblCell.BorderStyle = BorderStyle.None
                            tblRow.Cells.Add(tblCell)
                            tblPlayerDecision.Rows.Add(tblRow)
                        Next

                    End If

                Else
                    Dim oDTValoriPlayer As DataTable = HandleLoadGamePeriodDecisionPlayerValue(m_SiteMaster.PeriodGetCurrent, Nni(oRowVariableValue("IDDecisionPlayer")))
                    For Each oRowData As DataRow In oDTValoriPlayer.Rows
                        'If Nni(oRowData("IDPlayer")) = Nni(Session("IDTeam")) Then
                        sNomeVariabile = "var_" & Nz(oRowData("IDPlayer") & "_" & Nz(oRowData("VariableName"))).Replace(" ", "")
                        ' Controllo la presenza di un eventuale textbox con lo stesso ID
                        ' Se presente esco, altrimenti lo creo correttamente
                        Dim oTXT As TextBox = TryCast(FindControlRecursive(Page, sNomeVariabile), TextBox)
                        If oTXT Is Nothing Then
                            ' Etichetta della variabile
                            tblRow = New TableRow
                            tblCell = New TableCell
                            tblCell.Text = sTraduzione
                            tblCell.BorderStyle = BorderStyle.None
                            tblRow.Cells.Add(tblCell)

                            ' Creo i controlli necessari ed eventualmente li popolo
                            ' Carico i valori esistenti, altrimenti li creo
                            Dim oTxtBox As New TextBox
                            tblCell = New TableCell

                            ' Recupero il valore secco della decisione del player nel periodo
                            Dim sValore As String = HandleLoadGamePeriodDecisionPlayerValueSingle(m_SiteMaster.PeriodGetCurrent, Nni(Session("IDTeam")), Nni(oRowData("IDDecisionPlayer")))
                            oTxtBox.Text = sValore
                            oTxtBox.ID = Nz(sNomeVariabile)
                            oTxtBox.Attributes.Add("runat", "server")
                            oTxtBox.Style.Add("text-align", "right")
                            oTxtBox.ClientIDMode = ClientIDMode.Static
                            oTxtBox.EnableViewState = True

                            ' Aggiungo il textbox appena creato alla cella
                            tblCell.Controls.Add(oTxtBox)
                            tblCell.Style.Add("text-align", "right")
                            tblCell.BorderStyle = BorderStyle.None
                            tblRow.Cells.Add(tblCell)
                            tblPlayerDecision.Rows.Add(tblRow)
                            'End If
                        End If
                    Next
                End If
            Next
            sHeader = Nz(oRowHeader("VariableGroupLabel"))
        Next
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim oDTDecisionsPlayer As DataTable = HandleLoadGameDecisionPlayer(m_SessionData.Utente.Games.IDGame, 0)
        Dim sNomeTextBox As String
        Dim sNomeTeam As String = Nz(m_SiteMaster.TeamNameGet).Replace(" ", "")
        Dim sSQL As String
        Dim oDTDecisionPlayerValue As DataTable
        Dim oTXT As TextBox

        Message.Visible = False

        g_DAL = New DBHelper

        pnlMain.Update()
        For Each oRow As DataRow In oDTDecisionsPlayer.Rows
            ' Controllo se la varibile recuperata è di tipo lista altrimenti passo alle variabili con inserimento valore secco
            sSQL = "SELECT VariableValue FROM Decisions_Players_Value WHERE IDDecisionPlayer = " & Nni(oRow("ID"))
            Dim sVariableType As String = Nz(g_DAL.ExecuteScalar(sSQL))

            sSQL = "SELECT ID FROM Decisions_Players_Value WHERE IDDecisionPlayer = " & Nni(oRow("ID"))
            Dim iIDDecisionPlayerValue As Integer = Nni(g_DAL.ExecuteScalar(sSQL))

            If sVariableType.ToUpper.Contains("LIST OF") Then
                ' Recupero i dati della variabile interessata per cercare anche il Textbox che contiene i valori da salvare
                sSQL = "SELECT * FROM Decisions_Players_Value_List WHERE IDDecisionValue =" & iIDDecisionPlayerValue & " AND IDPlayer = " & Session("IDTeam")
                Dim oDTDecisionsValueList As DataTable = g_DAL.ExecuteDataTable(sSQL)

                For Each oRowValueList As DataRow In oDTDecisionsValueList.Rows
                    sNomeTextBox = Nz(oRowValueList("VariableName"))

                    oTXT = TryCast(FindControlRecursive(Page, sNomeTextBox), TextBox)
                    If oTXT IsNot Nothing AndAlso oTXT.Text <> "" Then
                        ' Vado a salvare i valori corretti nella tabella di riferimento
                        sSQL = "SELECT * FROM Decisions_Players_Value_List WHERE IDPlayer = " & Nni(Session("IDTeam")) _
                             & " And IDDecisionValue = " & iIDDecisionPlayerValue & " And VariableName = '" & Nz(oRowValueList("VariableName")) & "' "
                        oDTDecisionPlayerValue = g_DAL.ExecuteDataTable(sSQL)
                        If oDTDecisionPlayerValue.Rows.Count > 0 Then ' Ci sono dati vado in update
                            sSQL = "UPDATE Decisions_Players_Value_List SET Value = '" & oTXT.Text & "' " _
                                 & "WHERE IDPlayer = " & Nni(Session("IDTeam")) & " AND IDDecisionValue = " & iIDDecisionPlayerValue & " " _
                                 & "AND IDPeriod = " & m_SiteMaster.PeriodGetCurrent & " AND VariableName = '" & Nz(oRowValueList("VariableName")) & "' "
                            g_DAL.ExecuteNonQuery(sSQL)
                        End If
                    End If
                Next

            Else
                ' Recupero tutti i campi che non necessitano di items o oggetti esterni
                ' E' fallita la ricerca, provo a ricercare il textbox nelle variabili a uso singolo, senza gestione del list of
                sSQL = "SELECT * FROM Decisions_Players_Value WHERE IDDecisionPlayer = " & Nni(oRow("ID"))
                Dim oDTDataValue As DataTable = g_DAL.ExecuteDataTable(sSQL)

                For Each oRowDataValue As DataRow In oDTDataValue.Rows
                    sNomeTextBox = "var_" & Nz(oRowDataValue("IDPlayer") & "_" & Nz(oRow("VariableName")))

                    oTXT = TryCast(FindControlRecursive(Page, sNomeTextBox), TextBox)
                    If oTXT IsNot Nothing AndAlso oTXT.Text <> "" Then
                        sSQL = "UPDATE Decisions_Players_Value SET VariableValue = '" & oTXT.Text & "' WHERE IDDecisionPlayer = " & Nni(oRow("ID")) & " AND  IDPlayer = " & Nni(Session("IDTeam"))
                        g_DAL.ExecuteNonQuery(sSQL)
                    End If
                Next

            End If

        Next

        SQLConnClose(g_DAL.GetConnObject)

        MessageText.Text = "Save completed"
        Message.Visible = True

    End Sub

    Private Sub Players_Decisions_Demo_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsNothing(m_SessionData) Then
            PageRedirect("ErrorTimeout.aspx", "", "")
        End If
    End Sub

End Class
