﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Ranking_FinancialIndicator.aspx.vb" Inherits="Ranking_FinancialIndicator" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="cntMain" ContentPlaceHolderID="MainContent" runat="Server" ClientIDMode="Static">

    <asp:UpdatePanel ID="UpdatePanelMain" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <h2>
                <asp:Label ID="lblWelcome" runat="server" Text="Ranking"></asp:Label>
            </h2>

            <div class="clr"></div>

            <div class="divTable">
                <div class="divTableRow">
                    <div class="divTableCell">
                        <h3>
                            <asp:Label ID="lblTitolo" runat="server" Text="Financial indicator"></asp:Label>
                        </h3>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <asp:Table ClientIDMode="Static" ID="tblIndicatorCurrent" runat="server" Width="682px">
                </asp:Table>
            </div>

            <div class="clr"></div>

            <div class="row">
                <asp:Table ClientIDMode="Static" ID="tblIndicatorLast" runat="server">
                </asp:Table>
            </div>

            <div class="row" style="min-height: 20px;"></div>

            <div class="row">
                <asp:PlaceHolder runat="server" ID="Message" Visible="false">
                    <p class="text-danger">
                        <asp:Literal runat="server" ID="MessageText" />
                    </p>
                </asp:PlaceHolder>
            </div>
        
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
