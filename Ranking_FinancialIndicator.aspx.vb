﻿
Imports APS.APSUtility
Imports APPCore.Utility
Imports Telerik.Web.UI
Imports System.Data

Partial Class Ranking_FinancialIndicator
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadData()
        Message.Visible = False

        UpdatePanelMain.Update()
    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadData()

        Message.Visible = False
        UpdatePanelMain.Update()
    End Sub

    Private Sub LoadData()
        tblIndicatorCurrent.Rows.Clear()
        tblIndicatorLast.Rows.Clear()

        IndicatorCurrent()
        IndicatorLastQuarter()
    End Sub

    Private Sub IndicatorCurrent()
        Dim oDTPlayers As New DataTable
        Dim oDTItems As New DataTable

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Dim dColWidth As Double

        Dim iContaItem As Integer
        Dim MarketShare As Double

        Try
            oDTItems = LoadItemsGame(Session("IDGame"), Nz(Session("LanguageActive")))
            iContaItem = oDTItems.Rows.Count

            ' Carico la tabella di tutti i players
            oDTPlayers = LoadTeamsGame(Session("IDGame"))

            ' Setto la larghezza della colonna in base al numero di giocatori
            ' 682 è la lunghezza massima della griglia in questo ambito
            dColWidth = 682 / (oDTPlayers.Rows.Count + 2) ' +1 è la cella header vuota ad inzio tabella

            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header vuota
            tblHeaderCell.Text = ""
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.Style.Add("background", "Transparent")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            ' Costruisco l'header
            For Each drPlayer As DataRow In oDTPlayers.Rows
                tblHeaderCell = New TableHeaderCell
                Dim oLBLTeamName As New RadLabel
                oLBLTeamName.ID = "lbl" & drPlayer("TeamName")
                ' se il nome del team contiene _ allora lo splitto così che possa metterlo su più righe
                If Nz(drPlayer("TeamName")).Contains("_") Then
                    Dim sNomeTeam() As String = Nz(drPlayer("TeamName")).Split("_")
                    For iSplit As Integer = 0 To sNomeTeam.Length - 1
                        oLBLTeamName.Text &= sNomeTeam(iSplit) & "<br/>"
                    Next
                Else
                    oLBLTeamName.Text = Nz(drPlayer("TeamName"))
                End If

                oLBLTeamName.Style.Add("color", Nz(drPlayer("Color")))
                tblHeaderCell.Controls.Add(oLBLTeamName)
                tblHeaderCell.Width = dColWidth
                tblHeaderCell.Font.Size = 8
                tblHeaderRow.Cells.Add(tblHeaderCell)
            Next
            tblIndicatorCurrent.Rows.Add(tblHeaderRow)

            ' Costruisco le righe 
            ' FATTURATO
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblFatturatoDesc As New RadLabel
            lblFatturatoDesc.Text = "Fatturato"
            tblCell.Controls.Add(lblFatturatoDesc)
            tblRow.Cells.Add(tblCell)

            For Each drPlayer As DataRow In oDTPlayers.Rows
                tblCell = New TableCell
                Dim lblFatturato As New RadLabel
                lblFatturato.ID = "lblFatturato" & drPlayer("TeamName")
                lblFatturato.Text = Nn(GetVariableDefineValue("FattuTotalCumul", drPlayer("ID"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
                tblCell.Controls.Add(lblFatturato)
                tblRow.Cells.Add(tblCell)
            Next
            tblIndicatorCurrent.Rows.Add(tblRow)

            ' QUOTA DI MERCATO
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblQuotaMercatoDesc As New RadLabel
            lblQuotaMercatoDesc.Text = "Quota di mercato"
            tblCell.Controls.Add(lblQuotaMercatoDesc)
            tblRow.Cells.Add(tblCell)

            For Each drPlayer As DataRow In oDTPlayers.Rows
                ' Ciclo sugli item per calcolare la quota di mercato player/item
                For Each drItem As DataRow In oDTItems.Rows
                    MarketShare += Nn(GetVariableDefineValue("MarketShare", drPlayer("ID"), drItem("IDVariable"), 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
                Next
                MarketShare = MarketShare / iContaItem

                tblCell = New TableCell
                Dim lblMarketShare As New RadLabel
                lblMarketShare.ID = "lblMarketShare" & drPlayer("TeamName")
                lblMarketShare.Text = MarketShare.ToString("N2")
                tblCell.Controls.Add(lblMarketShare)
                tblRow.Cells.Add(tblCell)
            Next
            tblIndicatorCurrent.Rows.Add(tblRow)

            ' TURNOVER
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblTurnoverDesc As New RadLabel
            lblTurnoverDesc.Text = "Turnover"
            tblCell.Controls.Add(lblTurnoverDesc)
            tblRow.Cells.Add(tblCell)

            For Each drPlayer As DataRow In oDTPlayers.Rows
                tblCell = New TableCell
                Dim lblTurnOver As New RadLabel
                lblTurnOver.ID = "lblTurnOver" & drPlayer("TeamName")
                lblTurnOver.Text = Nn(GetVariableDefineValue("FattuTotalCumul", drPlayer("ID"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2") _
                                 / Nn(GetVariableDefineValue("CapitInvesNetto", drPlayer("ID"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
                tblCell.Controls.Add(lblTurnOver)
                tblRow.Cells.Add(tblCell)
            Next
            tblIndicatorCurrent.Rows.Add(tblRow)

            ' IndexRONA
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblRONADesc As New RadLabel
            lblRONADesc.Text = "RONA"
            tblCell.Controls.Add(lblRONADesc)
            tblRow.Cells.Add(tblCell)

            For Each drPlayer As DataRow In oDTPlayers.Rows
                tblCell = New TableCell
                Dim lblRONA As New RadLabel
                lblRONA.ID = "lblRONA" & drPlayer("TeamName")
                lblRONA.Text = Nn(GetVariableDefineValue("IndexRONA", drPlayer("ID"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
                tblCell.Controls.Add(lblRONA)
                tblRow.Cells.Add(tblCell)
            Next
            tblIndicatorCurrent.Rows.Add(tblRow)

            ' ROE
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblROEDesc As New RadLabel
            lblROEDesc.Text = "ROE"
            tblCell.Controls.Add(lblROEDesc)
            tblRow.Cells.Add(tblCell)

            For Each drPlayer As DataRow In oDTPlayers.Rows
                tblCell = New TableCell
                Dim lblROE As New RadLabel
                lblROE.ID = "lblROE" & drPlayer("TeamName")
                lblROE.Text = Nn(GetVariableDefineValue("IndexROE", drPlayer("ID"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
                tblCell.Controls.Add(lblROE)
                tblRow.Cells.Add(tblCell)
            Next
            tblIndicatorCurrent.Rows.Add(tblRow)

            ' EBITDA
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblEBITDADesc As New RadLabel
            lblEBITDADesc.Text = "EBITDA %"
            tblCell.Controls.Add(lblEBITDADesc)
            tblRow.Cells.Add(tblCell)

            For Each drPlayer As DataRow In oDTPlayers.Rows
                tblCell = New TableCell
                Dim lblEBITDA As New RadLabel
                lblEBITDA.ID = "lblEBITDA" & drPlayer("TeamName")
                lblEBITDA.Text = Nn(GetVariableDefineValue("EBITDAperce", drPlayer("ID"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
                tblCell.Controls.Add(lblEBITDA)
                tblRow.Cells.Add(tblCell)
            Next
            tblIndicatorCurrent.Rows.Add(tblRow)

            ' INDEBITAMENTO
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblRappoIndebDesc As New RadLabel
            lblRappoIndebDesc.Text = "Rapporto di indebitamento"
            tblCell.Controls.Add(lblRappoIndebDesc)
            tblRow.Cells.Add(tblCell)

            For Each drPlayer As DataRow In oDTPlayers.Rows
                tblCell = New TableCell
                Dim lblIndebitamento As New RadLabel
                lblIndebitamento.ID = "lblIndebitamento" & drPlayer("TeamName")
                lblIndebitamento.Text = Nn(GetVariableDefineValue("RappoIndeb", drPlayer("ID"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
                tblCell.Controls.Add(lblIndebitamento)
                tblRow.Cells.Add(tblCell)
            Next
            tblIndicatorCurrent.Rows.Add(tblRow)

            ' DEBITI A LUNGO/EBITDA
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblDebitiLungoDesc As New RadLabel
            lblDebitiLungoDesc.Text = "Debiti a lungo / EBITDA"
            tblCell.Controls.Add(lblDebitiLungoDesc)
            tblRow.Cells.Add(tblCell)

            For Each drPlayer As DataRow In oDTPlayers.Rows
                tblCell = New TableCell
                Dim lblDebitiLungo As New RadLabel
                lblDebitiLungo.ID = "lblDebitiLungo" & drPlayer("TeamName")
                lblDebitiLungo.Text = Nn(GetVariableDefineValue("LoansEndPerio", drPlayer("ID"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2") _
                                    / Nn(GetVariableDefineValue("EBITDAperce", drPlayer("ID"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2") _
                                    / 100 * Nn(GetVariableDefineValue("CumulRevenTotal", drPlayer("ID"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
                tblCell.Controls.Add(lblDebitiLungo)
                tblRow.Cells.Add(tblCell)
            Next
            tblIndicatorCurrent.Rows.Add(tblRow)

            ' INDICE DI DISPONIBILITA
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblIndiceDisponibilitaDesc As New RadLabel
            lblIndiceDisponibilitaDesc.Text = "Debiti a lungo / EBITDA"
            tblCell.Controls.Add(lblIndiceDisponibilitaDesc)
            tblRow.Cells.Add(tblCell)

            For Each drPlayer As DataRow In oDTPlayers.Rows
                tblCell = New TableCell
                Dim lblIndiceDisponibilita As New RadLabel
                lblIndiceDisponibilita.ID = "lblIndiceDisponibilita" & drPlayer("TeamName")
                lblIndiceDisponibilita.Text = Nn(GetVariableDefineValue("IndicDispo", drPlayer("ID"), 0, 0, m_SiteMaster.PeriodGetCurrent, Session("IDGame"))).ToString("N2")
                tblCell.Controls.Add(lblIndiceDisponibilita)
                tblRow.Cells.Add(tblCell)
            Next
            tblIndicatorCurrent.Rows.Add(tblRow)

        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub IndicatorLastQuarter()
        Dim oDTPlayers As New DataTable
        Dim oDTItems As New DataTable

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Dim dColWidth As Double

        Dim iContaItem As Integer
        Dim MarketShare As Double

        Dim iIDPeriodLastQuarter As Integer

        Try
            oDTItems = LoadItemsGame(Session("IDGame"), Nz(Session("LanguageActive")))
            iContaItem = oDTItems.Rows.Count

            iIDPeriodLastQuarter = m_SiteMaster.PeriodGetLastQuarter

            ' Carico la tabella di tutti i players
            oDTPlayers = LoadTeamsGame(Session("IDGame"))

            ' Setto la larghezza della colonna in base al numero di giocatori
            ' 682 è la lunghezza massima della griglia in questo ambito
            dColWidth = 682 / (oDTPlayers.Rows.Count + 2) ' +1 è la cella header vuota ad inzio tabella

            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header vuota
            tblHeaderCell.Text = ""
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.Style.Add("background", "Transparent")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            ' Costruisco l'header
            For Each drPlayer As DataRow In oDTPlayers.Rows
                tblHeaderCell = New TableHeaderCell
                Dim oLBLTeamName As New RadLabel
                oLBLTeamName.ID = "lbl" & drPlayer("TeamName")
                ' se il nome del team contiene _ allora lo splitto così che possa metterlo su più righe
                If Nz(drPlayer("TeamName")).Contains("_") Then
                    Dim sNomeTeam() As String = Nz(drPlayer("TeamName")).Split("_")
                    For iSplit As Integer = 0 To sNomeTeam.Length - 1
                        oLBLTeamName.Text &= sNomeTeam(iSplit) & "<br/>"
                    Next
                Else
                    oLBLTeamName.Text = Nz(drPlayer("TeamName"))
                End If

                oLBLTeamName.Style.Add("color", Nz(drPlayer("Color")))
                tblHeaderCell.Controls.Add(oLBLTeamName)
                tblHeaderCell.Width = dColWidth
                tblHeaderCell.Font.Size = 8
                tblHeaderRow.Cells.Add(tblHeaderCell)
            Next
            tblIndicatorCurrent.Rows.Add(tblHeaderRow)

            ' Costruisco le righe 
            ' FATTURATO
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblFatturatoDesc As New RadLabel
            lblFatturatoDesc.Text = "Fatturato"
            tblCell.Controls.Add(lblFatturatoDesc)
            tblRow.Cells.Add(tblCell)

            For Each drPlayer As DataRow In oDTPlayers.Rows
                tblCell = New TableCell
                Dim lblFatturato As New RadLabel
                lblFatturato.ID = "lblFatturato" & drPlayer("TeamName")
                lblFatturato.Text = Nn(GetVariableDefineValue("FattuTotalCumul", drPlayer("ID"), 0, 0, iIDPeriodLastQuarter, Session("IDGame"))).ToString("N2")
                tblCell.Controls.Add(lblFatturato)
                tblRow.Cells.Add(tblCell)
            Next
            tblIndicatorCurrent.Rows.Add(tblRow)

            ' QUOTA DI MERCATO
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblQuotaMercatoDesc As New RadLabel
            lblQuotaMercatoDesc.Text = "Quota di mercato"
            tblCell.Controls.Add(lblQuotaMercatoDesc)
            tblRow.Cells.Add(tblCell)

            For Each drPlayer As DataRow In oDTPlayers.Rows
                ' Ciclo sugli item per calcolare la quota di mercato player/item
                For Each drItem As DataRow In oDTItems.Rows
                    MarketShare += Nn(GetVariableDefineValue("MarketShare", drPlayer("ID"), drItem("IDVariable"), 0, iIDPeriodLastQuarter, Session("IDGame"))).ToString("N2")
                Next
                MarketShare = MarketShare / iContaItem

                tblCell = New TableCell
                Dim lblMarketShare As New RadLabel
                lblMarketShare.ID = "lblMarketShare" & drPlayer("TeamName")
                lblMarketShare.Text = MarketShare.ToString("N2")
                tblCell.Controls.Add(lblMarketShare)
                tblRow.Cells.Add(tblCell)
            Next
            tblIndicatorCurrent.Rows.Add(tblRow)

            ' TURNOVER
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblTurnoverDesc As New RadLabel
            lblTurnoverDesc.Text = "Turnover"
            tblCell.Controls.Add(lblTurnoverDesc)
            tblRow.Cells.Add(tblCell)

            For Each drPlayer As DataRow In oDTPlayers.Rows
                tblCell = New TableCell
                Dim lblTurnOver As New RadLabel
                lblTurnOver.ID = "lblTurnOver" & drPlayer("TeamName")
                lblTurnOver.Text = Nn(GetVariableDefineValue("FattuTotalCumul", drPlayer("ID"), 0, 0, iIDPeriodLastQuarter, Session("IDGame"))).ToString("N2") _
                                 / Nn(GetVariableDefineValue("CapitInvesNetto", drPlayer("ID"), 0, 0, iIDPeriodLastQuarter, Session("IDGame"))).ToString("N2")
                tblCell.Controls.Add(lblTurnOver)
                tblRow.Cells.Add(tblCell)
            Next
            tblIndicatorCurrent.Rows.Add(tblRow)

            ' IndexRONA
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblRONADesc As New RadLabel
            lblRONADesc.Text = "RONA"
            tblCell.Controls.Add(lblRONADesc)
            tblRow.Cells.Add(tblCell)

            For Each drPlayer As DataRow In oDTPlayers.Rows
                tblCell = New TableCell
                Dim lblRONA As New RadLabel
                lblRONA.ID = "lblRONA" & drPlayer("TeamName")
                lblRONA.Text = Nn(GetVariableDefineValue("IndexRONA", drPlayer("ID"), 0, 0, iIDPeriodLastQuarter, Session("IDGame"))).ToString("N2")
                tblCell.Controls.Add(lblRONA)
                tblRow.Cells.Add(tblCell)
            Next
            tblIndicatorCurrent.Rows.Add(tblRow)

            ' ROE
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblROEDesc As New RadLabel
            lblROEDesc.Text = "ROE"
            tblCell.Controls.Add(lblROEDesc)
            tblRow.Cells.Add(tblCell)

            For Each drPlayer As DataRow In oDTPlayers.Rows
                tblCell = New TableCell
                Dim lblROE As New RadLabel
                lblROE.ID = "lblROE" & drPlayer("TeamName")
                lblROE.Text = Nn(GetVariableDefineValue("IndexROE", drPlayer("ID"), 0, 0, iIDPeriodLastQuarter, Session("IDGame"))).ToString("N2")
                tblCell.Controls.Add(lblROE)
                tblRow.Cells.Add(tblCell)
            Next
            tblIndicatorCurrent.Rows.Add(tblRow)

            ' EBITDA
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblEBITDADesc As New RadLabel
            lblEBITDADesc.Text = "EBITDA %"
            tblCell.Controls.Add(lblEBITDADesc)
            tblRow.Cells.Add(tblCell)

            For Each drPlayer As DataRow In oDTPlayers.Rows
                tblCell = New TableCell
                Dim lblEBITDA As New RadLabel
                lblEBITDA.ID = "lblEBITDA" & drPlayer("TeamName")
                lblEBITDA.Text = Nn(GetVariableDefineValue("EBITDAperce", drPlayer("ID"), 0, 0, iIDPeriodLastQuarter, Session("IDGame"))).ToString("N2")
                tblCell.Controls.Add(lblEBITDA)
                tblRow.Cells.Add(tblCell)
            Next
            tblIndicatorCurrent.Rows.Add(tblRow)

            ' INDEBITAMENTO
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblRappoIndebDesc As New RadLabel
            lblRappoIndebDesc.Text = "Rapporto di indebitamento"
            tblCell.Controls.Add(lblRappoIndebDesc)
            tblRow.Cells.Add(tblCell)

            For Each drPlayer As DataRow In oDTPlayers.Rows
                tblCell = New TableCell
                Dim lblIndebitamento As New RadLabel
                lblIndebitamento.ID = "lblIndebitamento" & drPlayer("TeamName")
                lblIndebitamento.Text = Nn(GetVariableDefineValue("RappoIndeb", drPlayer("ID"), 0, 0, iIDPeriodLastQuarter, Session("IDGame"))).ToString("N2")
                tblCell.Controls.Add(lblIndebitamento)
                tblRow.Cells.Add(tblCell)
            Next
            tblIndicatorCurrent.Rows.Add(tblRow)

            ' DEBITI A LUNGO/EBITDA
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblDebitiLungoDesc As New RadLabel
            lblDebitiLungoDesc.Text = "Debiti a lungo / EBITDA"
            tblCell.Controls.Add(lblDebitiLungoDesc)
            tblRow.Cells.Add(tblCell)

            For Each drPlayer As DataRow In oDTPlayers.Rows
                tblCell = New TableCell
                Dim lblDebitiLungo As New RadLabel
                lblDebitiLungo.ID = "lblDebitiLungo" & drPlayer("TeamName")
                lblDebitiLungo.Text = Nn(GetVariableDefineValue("LoansEndPerio", drPlayer("ID"), 0, 0, iIDPeriodLastQuarter, Session("IDGame"))).ToString("N2") _
                                    / Nn(GetVariableDefineValue("EBITDAperce", drPlayer("ID"), 0, 0, iIDPeriodLastQuarter, Session("IDGame"))).ToString("N2") _
                                    / 100 * Nn(GetVariableDefineValue("CumulRevenTotal", drPlayer("ID"), 0, 0, iIDPeriodLastQuarter, Session("IDGame"))).ToString("N2")
                tblCell.Controls.Add(lblDebitiLungo)
                tblRow.Cells.Add(tblCell)
            Next
            tblIndicatorCurrent.Rows.Add(tblRow)

            ' INDICE DI DISPONIBILITA
            tblRow = New TableRow
            tblCell = New TableCell
            Dim lblIndiceDisponibilitaDesc As New RadLabel
            lblIndiceDisponibilitaDesc.Text = "Debiti a lungo / EBITDA"
            tblCell.Controls.Add(lblIndiceDisponibilitaDesc)
            tblRow.Cells.Add(tblCell)

            For Each drPlayer As DataRow In oDTPlayers.Rows
                tblCell = New TableCell
                Dim lblIndiceDisponibilita As New RadLabel
                lblIndiceDisponibilita.ID = "lblIndiceDisponibilita" & drPlayer("TeamName")
                lblIndiceDisponibilita.Text = Nn(GetVariableDefineValue("IndicDispo", drPlayer("ID"), 0, 0, iIDPeriodLastQuarter, Session("IDGame"))).ToString("N2")
                tblCell.Controls.Add(lblIndiceDisponibilita)
                tblRow.Cells.Add(tblCell)
            Next
            tblIndicatorCurrent.Rows.Add(tblRow)

        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub Ranking_FinancialIndicator_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        m_SessionData = Session("SessionData")

        m_SiteMaster = Me.Master

        m_SiteMaster.LoadPeriodsValid = Session("IDRole") = "P"

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oCBOPeriod As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOPeriodValid As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriodValid"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler m_SiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler m_SiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(Session("IDGame")) ' Ultimo periodo
        oCBOPeriod.SelectedValue = Session("IDPeriodValid")
        m_SiteMaster.PeriodChange = Session("IDPeriodValid")

        Session("NameTeam") = GetTeamName(Session("IDTeam"))
        oCBOTeam.SelectedValue = Session("IDTeam")
        m_SiteMaster.TeamChange = Session("IDTeam")

    End Sub

    Private Sub Ranking_FinancialIndicator_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        If Not Page.IsPostBack Then
            If Session("IDRole") = "P" Then
                m_SiteMaster.HandleReloadPeriods(HandleGetMaxPeriodValid(Session("IDGame")))
            End If
            LoadData()
        End If
    End Sub

End Class
