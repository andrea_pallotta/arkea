﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Ranking_Financial_Indicator.aspx.vb" Inherits="Ranking_Financial_Indicator" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>


<html lang="it">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Ranking - ARKEA</title>

    <link href="Content/Site.css" rel="stylesheet" type="text/css" />
    <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />

    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />

</head>

<body>
    <form id="frmMain" runat="server">

        <asp:ScriptManager runat="server" AsyncPostBackTimeout="200000" EnableScriptGlobalization="true">
            <Scripts>
                <%--To learn more about bundling scripts in ScriptManager see http://go.microsoft.com/fwlink/?LinkID=301884 --%>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="jquery" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="respond" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>

        <div>
            <asp:SqlDataSource runat="server" ID="dsTeam"></asp:SqlDataSource>

            <asp:UpdatePanel ID="UpdatePanelMain" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <h2>
                        <asp:Label ID="lblWelcome" runat="server" Text="Ranking"></asp:Label>
                    </h2>

                    <div class="clr"></div>

                    <div class="divTable">
                        <div class="divTableRow">
                            <div class="divTableCell">
                                <h3>
                                    <asp:Label ID="lblTitolo" runat="server" Text="Indicatori finanziari"></asp:Label>
                                </h3>
                            </div>
                        </div>
                    </div>

                    <div class="divTableRow" style="margin-bottom: 10px;">

                        <div class="divTableCell">
                            <telerik:RadComboBox ID="cboPeriod" runat="server" Width="100%" EmptyMessage="< Select period...>"
                                RenderMode="Lightweight" RenderingMode="Full" AutoPostBack="true" Visible="true">
                            </telerik:RadComboBox>
                        </div>

                    </div>

                    <div class="clr"></div>

                    <div class="divTableRow" style="min-height: 20px;" />

                    <div class="divTableRow">
                        <asp:PlaceHolder runat="server" ID="phIndicatorCurrent" >
                            <asp:Table ClientIDMode="Static" ID="tblIndicatorCurrent" runat="server" CssClass="tableRanking">

                            </asp:Table>
                        </asp:PlaceHolder>
                    </div>
                    
                    <div class="divTableRow" style="min-height: 20px;" />

                    <div class="divTableRow">
                        <asp:PlaceHolder runat="server" ID="phIndicatorLastQuarter" >
                            <asp:Table ClientIDMode="Static" ID="tblIndicatorLast" runat="server" CssClass="tableRanking">
                            </asp:Table>
                        </asp:PlaceHolder>
                    </div>

                    <div class="divTableRow" style="min-height: 20px;" />

                    <div class="row">
                        <asp:PlaceHolder runat="server" ID="Message" Visible="false">
                            <p class="text-danger">
                                <asp:Literal runat="server" ID="MessageText" />
                            </p>
                        </asp:PlaceHolder>
                    </div>

                </ContentTemplate>
            </asp:UpdatePanel>

        </div>
    </form>
</body>
</html>
