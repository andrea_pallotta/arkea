﻿
Imports APS.APSUtility
Imports APPCore.Utility
Imports Telerik.Web.UI
Imports System.Data

Partial Class Ranking_Financial_Indicator
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData

    Friend Function GetPeriodLastQuarter() As Integer
        ' Devo recuperare l'id del periodo precedente altrimenti mi carica valori sbagliati, sono al periodo 2, carico i valori del periodo 1
        Dim sSQL As String
        sSQL = "SELECT NumStep - 4 FROM [BGOL_Periods] WHERE ID = " & Nni(cboPeriod.SelectedValue) & " AND IDGame = " & Session("IDGame")
        Dim iStep As Integer = Nni(g_DAL.ExecuteScalar(sSQL))

        If iStep = 0 Then
            Return 1
        Else
            sSQL = "SELECT ID FROM BGOL_Periods WHERE NumStep = " & iStep & " AND IDGame = " & Session("IDGame")
            Return Nni(g_DAL.ExecuteScalar(sSQL))
        End If
    End Function

    Public Sub LoadPeriods()
        Dim oDTPeriods As DataTable

        Try
            cboPeriod.Items.Clear()
            oDTPeriods = LoadPeriodGame(Nni(Session("IDGame")))
            cboPeriod.DataSource = oDTPeriods
            cboPeriod.DataValueField = "Id"
            cboPeriod.DataTextField = "Descrizione"
            cboPeriod.DataBind()

            ReloaadPeriodsNoSimulation()

            If cboPeriod.SelectedValue <> "" Then
                Session("IDPeriod") = Nni(cboPeriod.SelectedValue)
                Session("NamePeriod") = Nz(cboPeriod.Text)
            Else
                cboPeriod.SelectedIndex = 0
                Session("IDPeriod") = Nni(cboPeriod.SelectedValue)
                Session("NamePeriod") = Nz(cboPeriod.Text)
            End If

        Catch ex As Exception
            Throw New ApplicationException("Ranking_Financial_Indicator.aspx -> LoadPeriods", ex)
        End Try
    End Sub

    Private Sub ReloaadPeriodsNoSimulation()
        Dim sSQL As String = "SELECT IDPeriodo FROM BGOL_Games_Periods WHERE IDGame = " & Session("IDGame") & " AND Simulation = 1 "
        Dim iIDPeriodToRemove As Integer = g_DAL.ExecuteScalar(sSQL)

        If iIDPeriodToRemove > 0 AndAlso Session("IDRole").Contains("P") Then
            For Each oItem As RadComboBoxItem In cboPeriod.Items
                If oItem.Value = iIDPeriodToRemove Then
                    cboPeriod.Items.Remove(oItem)
                    cboPeriod.SelectedIndex = 0
                    Exit For
                End If
            Next
        End If
    End Sub

    Private Sub LoadData()
        MessageText.Text = ""
        Message.Visible = False

        tblIndicatorCurrent.Controls.Clear()
        tblIndicatorLast.Controls.Clear()

        tblIndicatorCurrent.Rows.Clear()
        tblIndicatorLast.Rows.Clear()

        IndicatorCurrent()
        IndicatorLastQuarter()

        ' Se le tabelle dati sono vuote provo a caricare il periodo precedente
        If tblIndicatorCurrent.Rows.Count <= 1 AndAlso tblIndicatorLast.Rows.Count <= 1 Then
            MessageText.Text = "No data available for this period"
            Message.Visible = True
        End If
    End Sub

    Private Sub IndicatorCurrent()
        Dim oDTPlayers As New DataTable
        Dim oDTItems As New DataTable

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Dim iContaItem As Integer
        Dim MarketShare As Double

        Try
            oDTItems = LoadItemsGame(Session("IDGame"), Nz(Session("LanguageActive")))
            iContaItem = oDTItems.Rows.Count

            ' Carico la tabella di tutti i players
            oDTPlayers = LoadTeamsGame(Session("IDGame"))

            ' Setto la larghezza della colonna in base al numero di giocatori

            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header vuota
            tblHeaderCell.Text = ""
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.Style.Add("background", "Transparent")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            If Nz(cboPeriod.Text) <> "" Then
                ' Costruisco l'header
                For Each drPlayer As DataRow In oDTPlayers.Rows
                    tblHeaderCell = New TableHeaderCell
                    Dim oLBLTeamName As New RadLabel
                    oLBLTeamName.ID = "lblCurrent" & drPlayer("TeamName")
                    ' se il nome del team contiene _ allora lo splitto così che possa metterlo su più righe
                    If Nz(drPlayer("TeamName")).Contains("_") Then
                        Dim sNomeTeam() As String = Nz(drPlayer("TeamName")).Split("_")
                        For iSplit As Integer = 0 To sNomeTeam.Length - 1
                            oLBLTeamName.Text &= sNomeTeam(iSplit) & "<br/>"
                        Next
                    Else
                        oLBLTeamName.Text = Nz(drPlayer("TeamName"))
                    End If

                    oLBLTeamName.Style.Add("color", Nz(drPlayer("Color")))
                    tblHeaderCell.Controls.Add(oLBLTeamName)
                    tblHeaderCell.Font.Size = 8
                    tblHeaderRow.Cells.Add(tblHeaderCell)
                Next
                tblIndicatorCurrent.Rows.Add(tblHeaderRow)

                ' Costruisco le righe 
                ' FATTURATO
                tblRow = New TableRow
                tblCell = New TableCell
                Dim lblFatturatoDesc As New RadLabel
                lblFatturatoDesc.Text = "Fatturato"
                tblCell.Controls.Add(lblFatturatoDesc)
                tblRow.Cells.Add(tblCell)

                For Each drPlayer As DataRow In oDTPlayers.Rows
                    tblCell = New TableCell
                    Dim lblFatturato As New RadLabel
                    lblFatturato.ID = "lblFatturatoCurrent" & drPlayer("TeamName")
                    lblFatturato.Text = Nn(GetVariableDefineValue("FattuTotalCumul", drPlayer("ID"), 0, 0, Nni(cboPeriod.SelectedValue), Session("IDGame"))).ToString("N2")
                    tblCell.Controls.Add(lblFatturato)
                    tblRow.Cells.Add(tblCell)
                Next
                tblIndicatorCurrent.Rows.Add(tblRow)

                ' QUOTA DI MERCATO
                tblRow = New TableRow
                tblCell = New TableCell
                Dim lblQuotaMercatoDesc As New RadLabel
                lblQuotaMercatoDesc.Text = "Quota di mercato"
                tblCell.Controls.Add(lblQuotaMercatoDesc)
                tblRow.Cells.Add(tblCell)

                For Each drPlayer As DataRow In oDTPlayers.Rows
                    ' Ciclo sugli item per calcolare la quota di mercato player/item
                    For Each drItem As DataRow In oDTItems.Rows
                        MarketShare += Nn(GetVariableDefineValue("MarketShare", drPlayer("ID"), drItem("IDVariable"), 0, Nni(cboPeriod.SelectedValue), Session("IDGame"))).ToString("N2")
                    Next
                    MarketShare = MarketShare / iContaItem

                    tblCell = New TableCell
                    Dim lblMarketShare As New RadLabel
                    lblMarketShare.ID = "lblMarketShareCurrent" & drPlayer("TeamName")
                    lblMarketShare.Text = MarketShare.ToString("N2")
                    tblCell.Controls.Add(lblMarketShare)
                    tblRow.Cells.Add(tblCell)
                Next
                tblIndicatorCurrent.Rows.Add(tblRow)

                ' TURNOVER
                tblRow = New TableRow
                tblCell = New TableCell
                Dim lblTurnoverDesc As New RadLabel
                lblTurnoverDesc.Text = "Turnover"
                tblCell.Controls.Add(lblTurnoverDesc)
                tblRow.Cells.Add(tblCell)

                For Each drPlayer As DataRow In oDTPlayers.Rows
                    tblCell = New TableCell
                    Dim lblTurnOver As New RadLabel
                    lblTurnOver.ID = "lblTurnOverCurrent" & drPlayer("TeamName")
                    lblTurnOver.Text = Nn(Nn(GetVariableDefineValue("FattuTotalCumul", drPlayer("ID"), 0, 0, Nni(cboPeriod.SelectedValue), Session("IDGame"))).ToString("N2") _
                                     / Nn(GetVariableDefineValue("CapitInvesNetto", drPlayer("ID"), 0, 0, Nni(cboPeriod.SelectedValue), Session("IDGame"))).ToString("N2")).ToString("N2")
                    tblCell.Controls.Add(lblTurnOver)
                    tblRow.Cells.Add(tblCell)
                Next
                tblIndicatorCurrent.Rows.Add(tblRow)

                ' IndexRONA
                tblRow = New TableRow
                tblCell = New TableCell
                Dim lblRONADesc As New RadLabel
                lblRONADesc.Text = "RONA"
                tblCell.Controls.Add(lblRONADesc)
                tblRow.Cells.Add(tblCell)

                For Each drPlayer As DataRow In oDTPlayers.Rows
                    tblCell = New TableCell
                    Dim lblRONA As New RadLabel
                    lblRONA.ID = "lblRONACurrent" & drPlayer("TeamName")
                    lblRONA.Text = Nn(GetVariableDefineValue("IndexRONA", drPlayer("ID"), 0, 0, Nni(cboPeriod.SelectedValue), Session("IDGame"))).ToString("N2")
                    tblCell.Controls.Add(lblRONA)
                    tblRow.Cells.Add(tblCell)
                Next
                tblIndicatorCurrent.Rows.Add(tblRow)

                ' ROE
                tblRow = New TableRow
                tblCell = New TableCell
                Dim lblROEDesc As New RadLabel
                lblROEDesc.Text = "ROE"
                tblCell.Controls.Add(lblROEDesc)
                tblRow.Cells.Add(tblCell)

                For Each drPlayer As DataRow In oDTPlayers.Rows
                    tblCell = New TableCell
                    Dim lblROE As New RadLabel
                    lblROE.ID = "lblROECurrent" & drPlayer("TeamName")
                    lblROE.Text = Nn(GetVariableDefineValue("IndexROE", drPlayer("ID"), 0, 0, Nni(cboPeriod.SelectedValue), Session("IDGame"))).ToString("N2")
                    tblCell.Controls.Add(lblROE)
                    tblRow.Cells.Add(tblCell)
                Next
                tblIndicatorCurrent.Rows.Add(tblRow)

                ' EBITDA
                tblRow = New TableRow
                tblCell = New TableCell
                Dim lblEBITDADesc As New RadLabel
                lblEBITDADesc.Text = "EBITDA %"
                tblCell.Controls.Add(lblEBITDADesc)
                tblRow.Cells.Add(tblCell)

                For Each drPlayer As DataRow In oDTPlayers.Rows
                    tblCell = New TableCell
                    Dim lblEBITDA As New RadLabel
                    lblEBITDA.ID = "lblEBITDACurrent" & drPlayer("TeamName")
                    lblEBITDA.Text = Nn(GetVariableDefineValue("EBITDAperce", drPlayer("ID"), 0, 0, Nni(cboPeriod.SelectedValue), Session("IDGame"))).ToString("N2")
                    tblCell.Controls.Add(lblEBITDA)
                    tblRow.Cells.Add(tblCell)
                Next
                tblIndicatorCurrent.Rows.Add(tblRow)

                ' INDEBITAMENTO
                tblRow = New TableRow
                tblCell = New TableCell
                Dim lblRappoIndebDesc As New RadLabel
                lblRappoIndebDesc.Text = "Rapporto di indebitamento"
                tblCell.Controls.Add(lblRappoIndebDesc)
                tblRow.Cells.Add(tblCell)

                For Each drPlayer As DataRow In oDTPlayers.Rows
                    tblCell = New TableCell
                    Dim lblIndebitamento As New RadLabel
                    lblIndebitamento.ID = "lblIndebitamentoCurrent" & drPlayer("TeamName")
                    lblIndebitamento.Text = Nn(GetVariableDefineValue("RappoIndeb", drPlayer("ID"), 0, 0, Nni(cboPeriod.SelectedValue), Session("IDGame"))).ToString("N2")
                    tblCell.Controls.Add(lblIndebitamento)
                    tblRow.Cells.Add(tblCell)
                Next
                tblIndicatorCurrent.Rows.Add(tblRow)

                ' DEBITI A LUNGO/EBITDA
                tblRow = New TableRow
                tblCell = New TableCell
                Dim lblDebitiLungoDesc As New RadLabel
                lblDebitiLungoDesc.Text = "Debiti a lungo / EBITDA"
                tblCell.Controls.Add(lblDebitiLungoDesc)
                tblRow.Cells.Add(tblCell)

                For Each drPlayer As DataRow In oDTPlayers.Rows
                    tblCell = New TableCell
                    Dim lblDebitiLungo As New RadLabel
                    lblDebitiLungo.ID = "lblDebitiLungoCurrent" & drPlayer("TeamName")
                    lblDebitiLungo.Text = Nn(GetVariableDefineValue("EBITDAperce", drPlayer("ID"), 0, 0, Nni(cboPeriod.SelectedValue), Session("IDGame"))).ToString("N2")
                    tblCell.Controls.Add(lblDebitiLungo)
                    tblRow.Cells.Add(tblCell)
                Next
                tblIndicatorCurrent.Rows.Add(tblRow)

                ' INDICE DI DISPONIBILITA
                tblRow = New TableRow
                tblCell = New TableCell
                Dim lblIndiceDisponibilitaDesc As New RadLabel
                lblIndiceDisponibilitaDesc.Text = "Indice disponibilità"
                tblCell.Controls.Add(lblIndiceDisponibilitaDesc)
                tblRow.Cells.Add(tblCell)

                For Each drPlayer As DataRow In oDTPlayers.Rows
                    tblCell = New TableCell
                    Dim lblIndiceDisponibilita As New RadLabel
                    lblIndiceDisponibilita.ID = "lblIndiceDisponibilitaCurrent" & drPlayer("TeamName")
                    lblIndiceDisponibilita.Text = Nn(GetVariableDefineValue("IndicDispo", drPlayer("ID"), 0, 0, Nni(cboPeriod.SelectedValue), Session("IDGame"))).ToString("N2")
                    tblCell.Controls.Add(lblIndiceDisponibilita)
                    tblRow.Cells.Add(tblCell)
                Next
                tblIndicatorCurrent.Rows.Add(tblRow)

            End If

        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub IndicatorLastQuarter()
        Dim oDTPlayers As New DataTable
        Dim oDTItems As New DataTable

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Dim iContaItem As Integer
        Dim MarketShare As Double

        Dim iIDPeriodLastQuarter As Integer

        Try
            oDTItems = LoadItemsGame(Session("IDGame"), Nz(Session("LanguageActive")))
            iContaItem = oDTItems.Rows.Count

            ' Carico la tabella di tutti i players
            oDTPlayers = LoadTeamsGame(Session("IDGame"))

            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            ' Prima cella della prima riga dell'Header vuota
            tblHeaderCell.Text = ""
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.Style.Add("background", "Transparent")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            If Nz(cboPeriod.Text) <> "" Then
                iIDPeriodLastQuarter = GetPeriodLastQuarter()

                ' Costruisco l'header
                For Each drPlayer As DataRow In oDTPlayers.Rows
                    tblHeaderCell = New TableHeaderCell
                    Dim oLBLTeamName As New RadLabel
                    oLBLTeamName.ID = "lbl" & drPlayer("TeamName")
                    ' se il nome del team contiene _ allora lo splitto così che possa metterlo su più righe
                    If Nz(drPlayer("TeamName")).Contains("_") Then
                        Dim sNomeTeam() As String = Nz(drPlayer("TeamName")).Split("_")
                        For iSplit As Integer = 0 To sNomeTeam.Length - 1
                            oLBLTeamName.Text &= sNomeTeam(iSplit) & "<br/>"
                        Next
                    Else
                        oLBLTeamName.Text = Nz(drPlayer("TeamName"))
                    End If

                    oLBLTeamName.Style.Add("color", Nz(drPlayer("Color")))
                    tblHeaderCell.Controls.Add(oLBLTeamName)

                    tblHeaderCell.Font.Size = 8
                    tblHeaderRow.Cells.Add(tblHeaderCell)
                Next
                tblIndicatorLast.Rows.Add(tblHeaderRow)

                ' Costruisco le righe 
                ' FATTURATO
                tblRow = New TableRow
                tblCell = New TableCell
                Dim lblFatturatoDesc As New RadLabel
                lblFatturatoDesc.Text = "Fatturato"
                tblCell.Controls.Add(lblFatturatoDesc)
                tblRow.Cells.Add(tblCell)

                For Each drPlayer As DataRow In oDTPlayers.Rows
                    tblCell = New TableCell
                    Dim lblFatturato As New RadLabel
                    lblFatturato.ID = "lblFatturato" & drPlayer("TeamName")
                    lblFatturato.Text = Nn(GetVariableDefineValue("FattuTotalCumul", drPlayer("ID"), 0, 0, iIDPeriodLastQuarter, Session("IDGame"))).ToString("N2")
                    tblCell.Controls.Add(lblFatturato)
                    tblRow.Cells.Add(tblCell)
                Next
                tblIndicatorLast.Rows.Add(tblRow)

                ' QUOTA DI MERCATO
                tblRow = New TableRow
                tblCell = New TableCell
                Dim lblQuotaMercatoDesc As New RadLabel
                lblQuotaMercatoDesc.Text = "Quota di mercato"
                tblCell.Controls.Add(lblQuotaMercatoDesc)
                tblRow.Cells.Add(tblCell)

                For Each drPlayer As DataRow In oDTPlayers.Rows
                    ' Ciclo sugli item per calcolare la quota di mercato player/item
                    For Each drItem As DataRow In oDTItems.Rows
                        MarketShare += Nn(GetVariableDefineValue("MarketShare", drPlayer("ID"), drItem("IDVariable"), 0, iIDPeriodLastQuarter, Session("IDGame"))).ToString("N2")
                    Next
                    MarketShare = MarketShare / iContaItem

                    tblCell = New TableCell
                    Dim lblMarketShare As New RadLabel
                    lblMarketShare.ID = "lblMarketShare" & drPlayer("TeamName")
                    lblMarketShare.Text = MarketShare.ToString("N2")
                    tblCell.Controls.Add(lblMarketShare)
                    tblRow.Cells.Add(tblCell)
                Next
                tblIndicatorLast.Rows.Add(tblRow)

                ' TURNOVER
                tblRow = New TableRow
                tblCell = New TableCell
                Dim lblTurnoverDesc As New RadLabel
                lblTurnoverDesc.Text = "Turnover"
                tblCell.Controls.Add(lblTurnoverDesc)
                tblRow.Cells.Add(tblCell)

                For Each drPlayer As DataRow In oDTPlayers.Rows
                    tblCell = New TableCell
                    Dim lblTurnOver As New RadLabel
                    lblTurnOver.ID = "lblTurnOver" & drPlayer("TeamName")
                    lblTurnOver.Text = Nn(Nn(GetVariableDefineValue("FattuTotalCumul", drPlayer("ID"), 0, 0, iIDPeriodLastQuarter, Session("IDGame"))) _
                                     / Nn(GetVariableDefineValue("CapitInvesNetto", drPlayer("ID"), 0, 0, iIDPeriodLastQuarter, Session("IDGame")))).ToString("N2")
                    tblCell.Controls.Add(lblTurnOver)
                    tblRow.Cells.Add(tblCell)
                Next
                tblIndicatorLast.Rows.Add(tblRow)

                ' IndexRONA
                tblRow = New TableRow
                tblCell = New TableCell
                Dim lblRONADesc As New RadLabel
                lblRONADesc.Text = "RONA"
                tblCell.Controls.Add(lblRONADesc)
                tblRow.Cells.Add(tblCell)

                For Each drPlayer As DataRow In oDTPlayers.Rows
                    tblCell = New TableCell
                    Dim lblRONA As New RadLabel
                    lblRONA.ID = "lblRONA" & drPlayer("TeamName")
                    lblRONA.Text = Nn(GetVariableDefineValue("IndexRONA", drPlayer("ID"), 0, 0, iIDPeriodLastQuarter, Session("IDGame"))).ToString("N2")
                    tblCell.Controls.Add(lblRONA)
                    tblRow.Cells.Add(tblCell)
                Next
                tblIndicatorLast.Rows.Add(tblRow)

                ' ROE
                tblRow = New TableRow
                tblCell = New TableCell
                Dim lblROEDesc As New RadLabel
                lblROEDesc.Text = "ROE"
                tblCell.Controls.Add(lblROEDesc)
                tblRow.Cells.Add(tblCell)

                For Each drPlayer As DataRow In oDTPlayers.Rows
                    tblCell = New TableCell
                    Dim lblROE As New RadLabel
                    lblROE.ID = "lblROE" & drPlayer("TeamName")
                    lblROE.Text = Nn(GetVariableDefineValue("IndexROE", drPlayer("ID"), 0, 0, iIDPeriodLastQuarter, Session("IDGame"))).ToString("N2")
                    tblCell.Controls.Add(lblROE)
                    tblRow.Cells.Add(tblCell)
                Next
                tblIndicatorLast.Rows.Add(tblRow)

                ' EBITDA
                tblRow = New TableRow
                tblCell = New TableCell
                Dim lblEBITDADesc As New RadLabel
                lblEBITDADesc.Text = "EBITDA %"
                tblCell.Controls.Add(lblEBITDADesc)
                tblRow.Cells.Add(tblCell)

                For Each drPlayer As DataRow In oDTPlayers.Rows
                    tblCell = New TableCell
                    Dim lblEBITDA As New RadLabel
                    lblEBITDA.ID = "lblEBITDA" & drPlayer("TeamName")
                    lblEBITDA.Text = Nn(GetVariableDefineValue("EBITDAperce", drPlayer("ID"), 0, 0, iIDPeriodLastQuarter, Session("IDGame"))).ToString("N2")
                    tblCell.Controls.Add(lblEBITDA)
                    tblRow.Cells.Add(tblCell)
                Next
                tblIndicatorLast.Rows.Add(tblRow)

                ' INDEBITAMENTO
                tblRow = New TableRow
                tblCell = New TableCell
                Dim lblRappoIndebDesc As New RadLabel
                lblRappoIndebDesc.Text = "Rapporto di indebitamento"
                tblCell.Controls.Add(lblRappoIndebDesc)
                tblRow.Cells.Add(tblCell)

                For Each drPlayer As DataRow In oDTPlayers.Rows
                    tblCell = New TableCell
                    Dim lblIndebitamento As New RadLabel
                    lblIndebitamento.ID = "lblIndebitamento" & drPlayer("TeamName")
                    lblIndebitamento.Text = Nn(GetVariableDefineValue("RappoIndeb", drPlayer("ID"), 0, 0, iIDPeriodLastQuarter, Session("IDGame"))).ToString("N2")
                    tblCell.Controls.Add(lblIndebitamento)
                    tblRow.Cells.Add(tblCell)
                Next
                tblIndicatorLast.Rows.Add(tblRow)

                ' DEBITI A LUNGO/EBITDA
                tblRow = New TableRow
                tblCell = New TableCell
                Dim lblDebitiLungoDesc As New RadLabel
                lblDebitiLungoDesc.Text = "Debiti a lungo / EBITDA"
                tblCell.Controls.Add(lblDebitiLungoDesc)
                tblRow.Cells.Add(tblCell)

                For Each drPlayer As DataRow In oDTPlayers.Rows
                    tblCell = New TableCell
                    Dim lblDebitiLungo As New RadLabel
                    lblDebitiLungo.ID = "lblDebitiLungo" & drPlayer("TeamName")
                    lblDebitiLungo.Text = Nn(GetVariableDefineValue("EBITDAperce", drPlayer("ID"), 0, 0, iIDPeriodLastQuarter, Session("IDGame"))).ToString("N2")
                    tblCell.Controls.Add(lblDebitiLungo)
                    tblRow.Cells.Add(tblCell)
                Next
                tblIndicatorLast.Rows.Add(tblRow)

                ' INDICE DI DISPONIBILITA
                tblRow = New TableRow
                tblCell = New TableCell
                Dim lblIndiceDisponibilitaDesc As New RadLabel
                lblIndiceDisponibilitaDesc.Text = "Indice disponibilità"
                tblCell.Controls.Add(lblIndiceDisponibilitaDesc)
                tblRow.Cells.Add(tblCell)

                For Each drPlayer As DataRow In oDTPlayers.Rows
                    tblCell = New TableCell
                    Dim lblIndiceDisponibilita As New RadLabel
                    lblIndiceDisponibilita.ID = "lblIndiceDisponibilita" & drPlayer("TeamName")
                    lblIndiceDisponibilita.Text = Nn(GetVariableDefineValue("IndicDispo", drPlayer("ID"), 0, 0, iIDPeriodLastQuarter, Session("IDGame"))).ToString("N2")
                    tblCell.Controls.Add(lblIndiceDisponibilita)
                    tblRow.Cells.Add(tblCell)
                Next
                tblIndicatorLast.Rows.Add(tblRow)

            End If

        Catch ex As Exception
            MessageText.Text = ex.Message
            Message.Visible = True
        End Try
    End Sub

    Private Sub Ranking_Financial_Indicator_Init(sender As Object, e As EventArgs) Handles Me.Init
        m_SessionData = Session("SessionData")

        LoadData()

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(Session("IDGame")) ' Ultimo periodo
        Session("NameTeam") = GetTeamName(Session("IDTeam"))
    End Sub

    Private Sub cboPeriod_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles cboPeriod.SelectedIndexChanged
        LoadData()
    End Sub

    Private Sub Ranking_Financial_Indicator_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadPeriods()
        End If
    End Sub
End Class
