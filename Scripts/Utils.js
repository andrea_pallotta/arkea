﻿function isDigit(evt, txt) {
    var charCode = (evt.which) ? evt.which : event.keyCode

    var c = String.fromCharCode(charCode);

    if (txt.indexOf(c) > 0 && charCode == 46) {
        return false;
    }
    else if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }

    return true;
}