﻿Imports System.IO
Imports DALC4NET
Imports System.Data
Imports APS
Imports APPCore.Utility
Imports Telerik.Web.UI

Partial Public Class SiteMaster
    Inherits MasterPage

    Private m_SessionData As MU_SessionData

    Private m_LoadMenuSideBar As Boolean = True

    Private m_LoadPeriodValid As Boolean

    Public Event cboPeriodSelectedIndexChange(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
    Public Event cboTeamSelectedIndexChange(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)

    Private m_PageTitle As String

    Public ReadOnly Property UpdatePanelTeamPeriod() As UpdatePanel
        Get
            Return pnlMainUpdate
        End Get
    End Property

    Public ReadOnly Property UpdatePanelMenu() As UpdatePanel
        Get
            Return updMenuPanel
        End Get
    End Property

    WriteOnly Property PageTitle As String
        Set(value As String)
            m_PageTitle = value
        End Set
    End Property

    WriteOnly Property LoadPeriodsValid As Boolean
        Set(value As Boolean)
            m_LoadPeriodValid = value
        End Set
    End Property

    Public WriteOnly Property PeriodChange As Integer
        Set(value As Integer)
            cboPeriod.SelectedValue = value
            If cboPeriodValid.Visible Then
                cboPeriodValid.SelectedValue = value
            End If
            pnlMainUpdate.Update()
        End Set
    End Property

    Public WriteOnly Property TeamChange As Integer
        Set(value As Integer)
            cboTeam.SelectedValue = value
            pnlMainUpdate.Update()
        End Set
    End Property

    Public ReadOnly Property PeriodGetPrev As Integer
        Get
            ' Devo recuperare l'id del periodo precedente altrimenti mi carica valori sbagliati, sono al periodo 2, carico i valori del periodo 1
            Dim sSQL As String
            sSQL = "SELECT NumStep - 1 FROM [BGOL_Periods] WHERE ID = " & Nni(cboPeriod.SelectedValue) & " AND IDGame = " & Session("IDGame")
            Dim iStep As Integer = Nni(g_DAL.ExecuteScalar(sSQL))

            If iStep = 0 Then
                Return 1
            Else
                sSQL = "SELECT ID FROM BGOL_Periods WHERE NumStep = " & iStep & " AND IDGame = " & Session("IDGame")
                Return Nni(g_DAL.ExecuteScalar(sSQL))
            End If
        End Get
    End Property

    Public ReadOnly Property PeriodGetMeno2 As Integer
        Get
            ' Devo recuperare l'id del periodo precedente altrimenti mi carica valori sbagliati, sono al periodo 2, carico i valori del periodo 1
            Dim sSQL As String
            sSQL = "SELECT NumStep - 2 FROM [BGOL_Periods] WHERE ID = " & Nni(cboPeriod.SelectedValue) & " AND IDGame = " & Session("IDGame")
            Dim iStep As Integer = Nni(g_DAL.ExecuteScalar(sSQL))

            If iStep = 0 Then
                Return 1
            Else
                sSQL = "SELECT ID FROM BGOL_Periods WHERE NumStep = " & iStep & " AND IDGame = " & Session("IDGame")
                Return Nni(g_DAL.ExecuteScalar(sSQL))
            End If
        End Get
    End Property

    Public ReadOnly Property PeriodGetLastQuarter As Integer
        Get
            ' Devo recuperare l'id del periodo precedente altrimenti mi carica valori sbagliati, sono al periodo 2, carico i valori del periodo 1
            Dim sSQL As String
            sSQL = "SELECT NumStep - 4 FROM [BGOL_Periods] WHERE ID = " & Nni(cboPeriod.SelectedValue) & " AND IDGame = " & Session("IDGame")
            Dim iStep As Integer = Nni(g_DAL.ExecuteScalar(sSQL))

            If iStep = 0 Then
                Return 1
            Else
                sSQL = "SELECT ID FROM BGOL_Periods WHERE NumStep = " & iStep & " AND IDGame = " & Session("IDGame")
                Return Nni(g_DAL.ExecuteScalar(sSQL))
            End If
        End Get
    End Property

    Public ReadOnly Property PeriodGetNext As Integer
        Get
            Dim iMaxStep As Integer

            ' Devo recuperare l'id del periodo successivo, altrimenti mi carica valori sbagliati, sono al periodo 2, carico i valori del periodo 3
            Dim sSQL As String
            sSQL = "SELECT NumStep + 1 FROM [BGOL_Periods] WHERE ID = " & Nni(cboPeriod.SelectedValue) & " AND IDGame = " & Session("IDGame")
            Dim iStep As Integer = Nni(g_DAL.ExecuteScalar(sSQL))

            sSQL = "SELECT MAX(NumStep) FROM [BGOL_Periods] WHERE IDGame = " & Session("IDGame")
            iMaxStep = Nni(g_DAL.ExecuteScalar(sSQL))

            If iStep = 0 Then
                Return 1
            ElseIf iMaxStep = iStep Then
                sSQL = "SELECT ID FROM BGOL_Periods WHERE NumStep = " & iStep & " AND IDGame = " & Session("IDGame")
                Return Nni(g_DAL.ExecuteScalar(sSQL))
            Else
                sSQL = "SELECT ID FROM BGOL_Periods WHERE NumStep = " & iStep & " AND IDGame = " & Session("IDGame")
                Return Nni(g_DAL.ExecuteScalar(sSQL))
            End If
        End Get
    End Property

    Public ReadOnly Property PeriodGetCurrent As Integer
        Get
            Return Nni(cboPeriod.SelectedValue)
        End Get
    End Property

    Public ReadOnly Property TeamIDGet As Integer
        Get
            Return Nni(cboTeam.SelectedValue)
        End Get
    End Property

    Public ReadOnly Property TeamNameGet As String
        Get
            Return Nz(GetTeamName(Session("IDTeam")))
        End Get
    End Property

    Public Sub SetComboPeriodTeamInvisible()
        cboPeriod.Visible = False
        cboTeam.Visible = False
    End Sub

    Public Sub SetComboTeamDisabled()
        cboTeam.Enabled = False
    End Sub

    Public Sub LoadMenuSidebarRight(Visible As Boolean)
        m_LoadMenuSideBar = Visible
    End Sub

    ''' <summary>
    ''' Permette di cambiare il percorso delle immagini presenti nella pagina, questo è necessario nel caso in cui le pagine non siano all'interno della stessa root
    ''' </summary>
    Public Sub SetImageInvisible()
        ' Setto le immagini dei pulsanti a seconda della pagina chiamante
        Dim oWebPage As Page = DirectCast(Session("PageOrigin"), Page)
        Dim sNomePaginaOrigine As String = oWebPage.ToString().Substring(4, oWebPage.ToString().Substring(4).Length - 5) + ".aspx"
        Select Case sNomePaginaOrigine.ToUpper
            Case "ACCOUNT_LOGIN.ASPX"
                imgTutorAdministration.Visible = False
                imgChat.Visible = False
                imgFaq.Visible = False
                imgManual.Visible = False
                imgMaterials.Visible = False
                imgTutorMessage.Visible = False
                lblTutorAdministrator.Visible = False
            Case Else
                imgTutorAdministration.Visible = True
                imgChat.Visible = True
                imgFaq.Visible = True
                imgManual.Visible = True
                imgMaterials.Visible = True
                imgTutorMessage.Visible = True
                lblTutorAdministrator.Visible = True

        End Select
    End Sub

    ''' <summary>
    ''' Caricamento dello step corrente
    ''' </summary>
    Private Sub GetCurrentStep()
        Dim sSQL As String = "SELECT NumStep FROM BGOL_Periods WHERE IDGame = " & Session("IDGame") & " AND ID = " & Session("IDPeriod")
        Session("CurrentStep") = Nni(g_DAL.ExecuteScalar(sSQL))
    End Sub

    Private Sub GetStepToPlay()
        Dim sSQL As String = "SELECT NumStep FROM BGOL_Periods WHERE ID = (SELECT MAX(IDPeriodo) FROM BGOL_Games_Periods WHERE IDGame = " & Session("IDGame") & ") "
        Session("StepToPlay") = Nni(g_DAL.ExecuteScalar(sSQL))
    End Sub

    ''' <summary>
    ''' Caricamento del menù laterale sx, voci del game
    ''' </summary>
    Private Sub HandleLoadMenuSideBar()
        Dim oDTMenuMaster As DataTable
        Dim sIDParent As String
        Dim sSQL As String
        Dim sMenuDescrizione As String

        Try
            ' Procedo con il caricamento delle voci di menù master/principali
            ' oDTMenuMaster = oDAL.ExecuteDataTable("sp_Menu_Master_Load", CommandType.StoredProcedure)
            sSQL = "SELECT MM.Id AS MenuID, MM.Description AS  MenuDescription, MM.OrderNum AS MenuOrderNum, " _
                 & "MM.Visible AS MenuVisible, MM.RefPage AS MenuPage, " _
                 & "MMT.Translation, MMT.IDLanguage " _
                 & "FROM Menu_Master MM " _
                 & "LEFT JOIN Menu_Master_Translation MMT ON MM.ID = MMT.IDMenu " _
                 & "WHERE MM.Visible = 1 AND MM.IDGame = " & Nni(Session("IDGame")) _
                 & " ORDER BY MM.OrderNum "
            oDTMenuMaster = g_DAL.ExecuteDataTable(sSQL)

            For Each oRow As DataRow In oDTMenuMaster.Rows
                sIDParent = "P" & Nz(oRow("MenuID"))
                ' Recupero l'eventuale traduzione del menu
                Dim iIDLanguage As Integer
                sSQL = "SELECT ID FROM Languages WHERE Code = '" & Nz(Session("LanguageActive")) & "' "
                iIDLanguage = Nni(g_DAL.ExecuteScalar(sSQL))

                Dim drTranslations() As DataRow = oDTMenuMaster.Select("IDLanguage = " & iIDLanguage & " AND MenuID = " & Nni(oRow("MenuID")))

                If drTranslations.Length > 0 Then
                    sMenuDescrizione = Nz(drTranslations(0)("Translation"))
                Else
                    sMenuDescrizione = Nz(oRow("MenuDescription"))
                End If

                mnuSideBar.AddParent(sIDParent, sMenuDescrizione, Nz(oRow("MenuPage")))
                If Nz(oRow("MenuDescription")) = "Tutor" Then
                    Dim oCTRLHide As Control = mnuSideBar.FindControl(sIDParent)
                    If Not oCTRLHide Is Nothing Then
                        oCTRLHide.Visible = Not Session("IDRole").Contains("P")
                    End If
                Else
                    Dim oCTRLHide As Control = mnuSideBar.FindControl(sIDParent)
                    If Not oCTRLHide Is Nothing Then
                        oCTRLHide.Visible = Nb(oRow("MenuVisible"))
                    End If
                End If
                If Nb(oRow("MenuVisible")) Then
                    HandleLoadMenuChild(sIDParent, Nni(oRow("MenuID")), g_DAL)
                End If
            Next

            SQLConnClose(g_DAL.GetConnObject)

        Catch ex As Exception
            Throw New ApplicationException("LoadMenuSidebar -> HandleLoadMenuSideBar", ex)
        End Try
    End Sub

    Private Sub HandleLoadMenuChild(IDParent As String, IDMaster As Integer, oDAL As DBHelper)
        Dim oDTMenuDetail As DataTable
        Dim sChildID As String
        Dim sSQL As String
        Dim sMenuDescrizione As String

        Try
            ' Procedo con il caricamento delle voci di menù master/principali
            sSQL = "SELECT MD.Id AS MenuChildID, MD.Description AS MenuChildDescription, MD.OrderNum AS MenuChildOrderNum, " _
                 & "MD.Visible AS MenuChildVisible, MD.RefPage AS MenuChildPage, MD.IsVisibleTo, MDT.IDLanguage, MDT.Translation, OpenAsPopup " _
                 & "FROM Menu_Detail MD " _
                 & "LEFT JOIN Menu_Detail_Translation MDT ON MD.Id = MDT.IDMenu " _
                 & "WHERE MD.IdMenuMaster = " & IDMaster & " " _
                 & "ORDER BY MD.OrderNum "
            oDTMenuDetail = oDAL.ExecuteDataTable(sSQL)

            For Each oRow As DataRow In oDTMenuDetail.Rows
                sChildID = "C" & Nz(oRow("MenuChildID"))
                ' Recupero l'eventuale traduzione del menu
                Dim iIDLanguage As Integer
                sSQL = "SELECT ID FROM Languages WHERE Code = '" & Nz(Session("LanguageActive")) & "' "
                iIDLanguage = Nni(g_DAL.ExecuteScalar(sSQL))

                Dim drTranslations() As DataRow = oDTMenuDetail.Select("IDLanguage = " & iIDLanguage & "AND MenuChildID = " & Nni(oRow("MenuChildID")))

                If drTranslations.Length > 0 Then
                    sMenuDescrizione = Nz(drTranslations(0)("Translation"))
                Else
                    sMenuDescrizione = Nz(oRow("MenuChildDescription"))
                End If

                If Nb(oRow("MenuChildVisible")) AndAlso Nz(oRow("IsVisibleTo")).Contains(Session("IDRole")) Then
                    If Nb(oRow("OpenAsPopup")) = True Then
                        mnuSideBar.AddChildAt(sChildID, IDParent, sMenuDescrizione, Nz(oRow("MenuChildPage")), Nothing, Nothing, "#")
                    Else
                        mnuSideBar.AddChildAt(sChildID, IDParent, sMenuDescrizione, Nz(oRow("MenuChildPage")), Nothing, Nothing)
                    End If

                End If
            Next

        Catch ex As Exception
            Throw New ApplicationException("LoadMenuSidebar -> HandleLoadMenuChild", ex)
        End Try
    End Sub

    Public Sub LoadTeams()
        Dim sSQL As String
        Dim bMeUIsPlayer As Boolean

        Try
            cboTeam.ClearSelection()
            cboTeam.Items.Clear()
            cboTeam.DataSource = Nothing
            cboTeam.DataSource = LoadTeamsGame(Nni(Session("IDGame")))
            cboTeam.DataValueField = "ID"
            cboTeam.DataTextField = "TeamName"
            cboTeam.DataBind()

            If Session("IDRole").Contains("P") Then
                cboTeam.SelectedValue = GetIDTeam(Nni(Session("IDGame")), Nni(Session("IDPlayer")))
            Else
                cboTeam.SelectedIndex = 0
            End If

            If Nz(cboTeam.SelectedValue) <> "" AndAlso Nni(cboTeam.SelectedValue) <> 0 AndAlso Nz(Session("IDTeam")) = "" Then
                Session("IDTeam") = Nni(cboTeam.SelectedValue)
                Session("NameTeam") = Nz(cboTeam.Text)
            Else
                If Nz(Session("IDTeam")) <> "" Then
                    cboTeam.SelectedValue = Nni(Session("IDTeam"))
                Else
                    cboTeam.SelectedIndex = 0
                End If
            End If

            If Session("IDRole").Contains("P") Then
                cboTeam.Enabled = False
            Else
                cboTeam.Enabled = True
            End If

            ' Controllo se nei player c'è anche M&U, se non c'è setto una variabile di sessione a false
            bMeUIsPlayer = False
            For Each oItem As RadComboBoxItem In cboTeam.Items
                If oItem.Text.Contains("M&U") Then
                    bMeUIsPlayer = True
                    Exit For
                End If
            Next

            ' Se M&U non è player, allora mi recupero i suoi dati e quelli del primo player del game, 
            ' mi serve per fare i salvataggi durante le decisioni del gioco
            If Not bMeUIsPlayer Then
                sSQL = "SELECT ID FROM BGOL_Teams WHERE TeamName = 'M&U' "
                Session("IDTeamM&U") = Nni(g_DAL.ExecuteScalar(sSQL))
                sSQL = "SELECT TeamName FROM BGOL_Teams WHERE ID = " & Session("IDTeamM&U")
                Session("TeamNameM&U") = Nz(g_DAL.ExecuteScalar(sSQL))

                sSQL = "SELECT MIN(IDTeam) FROM BGOL_Players_Teams_Games WHERE IDGame = " & Session("IDGame") & " AND IDTeam <> " & Session("IDTeamM&U")
                Session("IDTeamMaster") = Nni(g_DAL.ExecuteScalar(sSQL))
                sSQL = "SELECT TeamName FROM BGOL_Teams WHERE ID = " & Session("IDTeamMaster")
                Session("TeamNameMaster") = Nz(g_DAL.ExecuteScalar(sSQL))

            Else
                Session("IDTeamM&U") = 0

            End If

        Catch ex As Exception
            Throw New ApplicationException("SiteMaster -> HandleLoadTeams", ex)
        End Try
    End Sub

    Public Sub LoadPeriods()
        Dim oDTPeriods As DataTable

        Try
            cboPeriod.Items.Clear()
            oDTPeriods = LoadPeriodGame(Nni(Session("IDGame")))
            cboPeriod.DataSource = oDTPeriods
            cboPeriod.DataValueField = "Id"
            cboPeriod.DataTextField = "Descrizione"
            cboPeriod.DataBind()

            ReloaadPeriodsNoSimulation()

            If cboPeriod.SelectedValue <> "" Then
                Session("IDPeriod") = Nni(cboPeriod.SelectedValue)
                Session("NamePeriod") = Nz(cboPeriod.Text)
            Else
                cboPeriod.SelectedIndex = 0
                Session("IDPeriod") = Nni(cboPeriod.SelectedValue)
                Session("NamePeriod") = Nz(cboPeriod.Text)
            End If
            GetCurrentStep()
            GetStepToPlay()

        Catch ex As Exception
            Throw New ApplicationException("SiteMaster ->LoadPeriods", ex)
        End Try
    End Sub

    Public Sub HandleReloadPeriods(IDPeriodToRemove As Integer)
        For Each oItem As RadComboBoxItem In cboPeriod.Items
            If oItem.Value = IDPeriodToRemove Then
                ' Controllo se il periodo da rimuovere è simulato
                Dim sSQL As String = "SELECT ISNULL(Simulation, 0) FROM BGOL_Games_Periods WHERE IDPeriodo = " & IDPeriodToRemove
                If Nb(g_DAL.ExecuteScalar(sSQL)) = True Then
                    cboPeriod.Items.Remove(oItem)
                    cboPeriod.SelectedIndex = 0
                    Session("IDPeriod") = cboPeriod.SelectedValue
                End If
                Exit For
            End If
        Next
        SQLConnClose(g_DAL.GetConnObject)
    End Sub

    Public Sub ReloadPeriodsCalculate()
        Dim iIDPeriodValid As Integer
        Dim sSQL As String

        ' Recupero l'ultimo IDPeriodo Calcolato
        sSQL = "SELECT MAX(IDPeriod) FROM Variables_Calculate_Define V " _
             & "INNER JOIN Variables_Calculate_Value C ON V.Id = C.IDVariable " _
             & "WHERE V.IDGame = " & Session("IDGame")
        iIDPeriodValid = Nni(g_DAL.ExecuteScalar(sSQL))

        For Each oItem As RadComboBoxItem In cboPeriod.Items
            If oItem.Value > iIDPeriodValid Then
                cboPeriod.Items.Remove(oItem)
                cboPeriod.SelectedIndex = 0
                Session("IDPeriod") = cboPeriod.SelectedValue
                Exit For
            End If
        Next

        SQLConnClose(g_DAL.GetConnObject)
    End Sub

    Private Sub ReloaadPeriodsNoSimulation()
        Dim sSQL As String = "SELECT IDPeriodo FROM BGOL_Games_Periods WHERE IDGame = " & Session("IDGame") & " AND Simulation = 1 "
        Dim iIDPeriodToRemove As Integer = g_DAL.ExecuteScalar(sSQL)

        If iIDPeriodToRemove > 0 AndAlso Session("IDRole").Contains("P") Then
            For Each oItem As RadComboBoxItem In cboPeriod.Items
                If oItem.Value = iIDPeriodToRemove Then
                    cboPeriod.Items.Remove(oItem)
                    cboPeriod.SelectedIndex = 0
                    Session("IDPeriod") = cboPeriod.SelectedValue
                    Exit For
                End If
            Next
        End If

        SQLConnClose(g_DAL.GetConnObject)
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs)
        Dim oDAL As New DBHelper

        If IsNothing(m_SessionData) Then
            PageRedirect("~/Account/Login.aspx", "", "")
        End If

        m_SessionData = Session("SessionData")

        If Not Page.IsPostBack Then

            If m_LoadMenuSideBar Then
                ' Carico il menù laterale sx
                HandleLoadMenuSideBar()

                LoadTeams()
                LoadPeriods()
            End If

            If m_SessionData.Utente.SuperUser Then
                aTutorAdministration.HRef = "Tutor_Administration.aspx"
            Else
                aTutorAdministration.HRef = ""
            End If

            cboTeam.Enabled = Session("IDRole") <> "P"

            If m_PageTitle <> "" Then
                lblGameTitle.Text = ""
            Else
                If Not IsNothing(Session("IDGame")) Then
                    Dim sSQL As String = "SELECT Descrizione FROM BGOL_Games WHERE ID = " & Nni(Session("IDGame"))
                    lblGameTitle.Text = Nz(oDAL.ExecuteScalar(sSQL))

                    SQLConnClose(oDAL.GetConnObject)
                    oDAL = Nothing

                End If

            End If

        End If
    End Sub

    Private Sub cboPeriod_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles cboPeriod.SelectedIndexChanged
        Session("IDPeriod") = Nni(cboPeriod.SelectedValue)
        Session("NamePeriod") = Nz(cboPeriod.Text)
        GetCurrentStep()
        GetStepToPlay()
        RaiseEvent cboPeriodSelectedIndexChange(Me, e)
    End Sub

    Private Sub cboTeam_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles cboTeam.SelectedIndexChanged
        Dim sSQL As String

        Session("IDTeam") = Nni(cboTeam.SelectedValue)
        Session("NameTeam") = Nz(cboTeam.Text)

        Dim oDAL As New DBHelper

        ' Con l'ID del Team recupero l'ID player
        sSQL = "SELECT IDPlayer FROM BGOL_Players_Teams_Games WHERE IDGame = " & m_SessionData.Utente.Games.IDGame & " AND IDTeam = " & Session("IDTeam")
        Session("IDPlayer") = Nni(oDAL.ExecuteScalar(sSQL))

        RaiseEvent cboTeamSelectedIndexChange(Me, e)

        SQLConnClose(oDAL.GetConnObject)
        oDAL = Nothing

    End Sub

    Private Sub SiteMaster_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Select Case Nz(Session("LanguageActive"))
                Case "ITA"
                    lblYourGames.Text = "<a href='Player_Game_Select.aspx'>I tuoi giochi</a>"
                    lblWelcomePage.Text = "<a href='default.aspx'>Benvenuto</a>"

            End Select
        End If

        Session("IDTeam") = cboTeam.SelectedValue
    End Sub

End Class
