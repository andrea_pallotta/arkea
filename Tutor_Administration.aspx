﻿<%@ Import Namespace="APPCore.Utility" %>

<%@ Page Title="" Language="VB" MasterPageFile="Site.master" AutoEventWireup="false" CodeFile="Tutor_Administration.aspx.vb" Inherits="Tutor_TutorAdministration" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="cntMain" ContentPlaceHolderID="MainContent" runat="Server">
    <link href="Content/StyleTutor.css" rel="stylesheet" type="text/css" />

    <asp:UpdatePanel ID="pnlMainUpdate" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
                <script type="text/javascript">
                    function RowDblClick(sender, eventArgs) {
                        sender.get_masterTableView().editItem(eventArgs.get_itemIndexHierarchical());
                    }

                    function onPopUpShowing(sender, args) {
                        args.get_popUp().className += " popUpEditForm";
                    }
                </script>
            </telerik:RadCodeBlock>

            <h2>
                <asp:Label ID="lblWelcome" runat="server" Text="Tutor administration"></asp:Label>
            </h2>

            <div class="clr">
                <h3>Game parameters
                </h3>
            </div>

            <div class="row">
                <telerik:RadAjaxManager ID="ramMain" runat="server">
                    <AjaxSettings>
                        <telerik:AjaxSetting AjaxControlID="grdDecisions">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="grdDecisions" LoadingPanelID="ralpMain"></telerik:AjaxUpdatedControl>
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                    </AjaxSettings>
                </telerik:RadAjaxManager>

                <telerik:RadAjaxLoadingPanel ID="ralpMain" runat="server" Skin="Default"></telerik:RadAjaxLoadingPanel>

                <div class="col-md-12">
                    <telerik:RadGrid RenderMode="Lightweight" ID="grdDecisions" runat="server" AllowPaging="True" ShowFooter="true"
                        AllowSorting="True" AutoGenerateColumns="False" ShowStatusBar="true" MasterTableView-EditMode="PopUp"
                        OnNeedDataSource="grdDecisions_NeedDataSource" OnUpdateCommand="grdDecisions_UpdateCommand"
                        OnInsertCommand="grdDecisions_InsertCommand" OnDeleteCommand="grdDecisions_DeleteCommand" AllowAutomaticUpdates="true">
                        <MasterTableView Width="100%" CommandItemDisplay="Top" DataKeyNames="IDDecision,IDType">
                            <EditFormSettings InsertCaption="Add new item" CaptionFormatString="Edit Item: {0}"
                                CaptionDataField="IDDecision" PopUpSettings-Modal="true" EditFormType="Template">
                                <EditColumn UniqueName="EditCommandColumn1"></EditColumn>
                                <FormTemplate>
                                    <table id="tableMain" border="0" style="border-collapse: collapse; width: 100%;">
                                        <tr class="EditFormHeader">
                                            <td colspan="2">
                                                <b>Decision detail</b>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <table id="Table3" style="border-collapse: collapse; width: 250px;" border="0" class="module">
                                                    <tr>
                                                        <td class="title" style="font-weight: bold;" colspan="2">Define decision:</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Decision:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtDecision" runat="server" Text='<%# Bind("Decision")%>' TabIndex="1">
                                                            </asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Data type:
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="cboDecisionType" runat="server"
                                                                TabIndex="2" AppendDataBoundItems="true">
                                                                <asp:ListItem Selected="True" Text="<... Choose data type ...>" Value="">
                                                                </asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Value:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtDecisionValue" runat="server" Text='<%# Bind("Value")%>' TabIndex="3">
                                                            </asp:TextBox>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>
                                                        </td>

                                                        <td>
                                                            <asp:CheckBox ID="chkPlayer" runat="server" Text="Player" Checked='<%# Bind("Player")%>'></asp:CheckBox>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>
                                                        </td>

                                                        <td>
                                                            <asp:CheckBox ID="chkItem" runat="server" Text="Item" Checked='<%# Bind("Item")%>'></asp:CheckBox>
                                                        </td>
                                                    </tr>

                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </table>
                                    <table style="width: 100%">
                                        <tr>
                                            <td style="text-align: right;" colspan="2">
                                                <asp:Button ID="Button1" Text='<%# Iif (TypeOf Container is GridEditFormInsertItem, "Insert", "Update") %>'
                                                    runat="server" CommandName='<%# Iif (TypeOf Container is GridEditFormInsertItem, "PerformInsert", "Update") %>'></asp:Button>&nbsp;  
                                <asp:Button ID="Button2" Text="Cancel" runat="server" CausesValidation="False" CommandName="Cancel"></asp:Button>
                                            </td>
                                        </tr>
                                    </table>
                                </FormTemplate>

                                <PopUpSettings Modal="True"></PopUpSettings>
                            </EditFormSettings>
                            <Columns>
                                <telerik:GridEditCommandColumn UniqueName="EditCommandColumn">
                                </telerik:GridEditCommandColumn>
                                <telerik:GridBoundColumn UniqueName="Decision" HeaderText="Decision" DataField="Decision">
                                    <HeaderStyle Width="80px"></HeaderStyle>
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn UniqueName="TypeDecision" HeaderText="Type decision" DataField="TypeDecision">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn UniqueName="Value" HeaderText="Value" DataField="Value">
                                </telerik:GridBoundColumn>

                                <telerik:GridBoundColumn UniqueName="IDType" HeaderText="IDType" DataField="IDType" Visible="false">
                                </telerik:GridBoundColumn>

                                <telerik:GridBoundColumn UniqueName="Item" HeaderText="Item" DataField="Item" Visible="false">
                                </telerik:GridBoundColumn>

                                <telerik:GridBoundColumn UniqueName="Player" HeaderText="Player" DataField="Player" Visible="false">
                                </telerik:GridBoundColumn>

                                <telerik:GridButtonColumn UniqueName="DeleteColumn" Text="Delete" CommandName="Delete" ButtonType="ImageButton"
                                    ConfirmText="This action will delete the selected decision. Are you sure?" ImageUrl="Images/Delete16.png">
                                </telerik:GridButtonColumn>


                            </Columns>
                        </MasterTableView>
                        <ClientSettings>
                            <ClientEvents OnRowDblClick="RowDblClick" OnPopUpShowing="onPopUpShowing" />
                        </ClientSettings>
                    </telerik:RadGrid>
                    <asp:SqlDataSource ID="sdsDecisionType" runat="server" ProviderName="System.Data.SqlClient"></asp:SqlDataSource>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

