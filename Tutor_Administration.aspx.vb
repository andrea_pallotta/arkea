﻿Imports APS
Imports APPCore.Utility
Imports Telerik.Web.UI
Imports System.Data
Imports DALC4NET

Partial Class Tutor_TutorAdministration
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData

    Public ReadOnly Property Decisions() As DataTable
        Get
            Dim oSessionDecisions As Object = Session("Decisions")
            If Not oSessionDecisions Is Nothing Then
                Return CType(oSessionDecisions, DataTable)
            End If

            Dim oDTDecisions As DataTable = New DataTable
            oDTDecisions = GetDataTableDecisions()
            Session("Decisions") = oDTDecisions
            Return oDTDecisions
        End Get
    End Property

    Public Function GetDataTableDecisions() As DataTable
        Dim oParamColl As New DBParameterCollection
        Dim oParamUtente As New DBParameter("@IDGame", m_SessionData.Utente.Games.IDGame, DbType.Int16)
        Dim oDTGames As DataTable

        Try
            g_DAL = New DBHelper()
            oParamColl.Add(oParamUtente)
            oDTGames = g_DAL.ExecuteDataTable("sp_Decisions_Game_Load", oParamColl, CommandType.StoredProcedure)

            g_DAL.GetConnObject.Close()
            g_DAL.GetConnObject.Dispose()

            Return oDTGames
        Catch ex As Exception
            Throw New ApplicationException("TutorAdministration -> GetDataTableDecisions", ex)
        End Try

    End Function

    Private Sub Tutor_TutorAdministration_Load(sender As Object, e As EventArgs) Handles Me.Load

        m_SessionData = Session("SessionData")

        If IsNothing(m_SessionData) Then
            PageRedirect("ErrorTimeout.aspx", "", "")
        End If

        If Not Page.IsPostBack Then
            sdsDecisionType.ConnectionString = g_ConnString
            sdsDecisionType.SelectCommand = "SELECT ID, TypeDescription FROM Variables_Type "
            sdsDecisionType.DataBind()
        End If
    End Sub

    Protected Sub grdDecisions_NeedDataSource(ByVal source As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles grdDecisions.NeedDataSource
        grdDecisions.DataSource = Decisions
        Me.Decisions.PrimaryKey = New DataColumn() {Me.Decisions.Columns("IDDecision")}
    End Sub

    Protected Sub grdDecisions_DeleteCommand(ByVal source As Object, ByVal e As GridCommandEventArgs)
        Dim iID As Integer = Nni((CType(e.Item, GridDataItem)).OwnerTableView.DataKeyValues(e.Item.ItemIndex)("IDDecision").ToString)
        Dim dtDecisions As DataTable = Me.Decisions
        If Not (dtDecisions.Rows.Find(iID) Is Nothing) Then
            dtDecisions.Rows.Find(iID).Delete()
            dtDecisions.AcceptChanges()

            ' Il datasource deriva da un stored multi table devo procedere anche all'aggiornamento manuale del database
            Dim sSQL As String = "DELETE FROM Decisions WHERE ID = " & iID
            g_DAL = New DBHelper
            g_DAL.ExecuteNonQuery(sSQL, CommandType.Text)

            sSQL = "DELETE FROM Decisions_Value WHERE IDDecision = " & iID
            g_DAL.ExecuteNonQuery(sSQL, CommandType.Text)

            grdDecisions.Rebind()
            g_DAL = Nothing
        End If
    End Sub

    Protected Sub grdDecisions_InsertCommand(ByVal source As Object, ByVal e As GridCommandEventArgs)
        Dim oEditedItem As GridEditableItem = CType(e.Item, GridEditableItem)
        Dim sDecision As String
        Dim iIDType As Integer
        Dim sValue As String
        Dim iIDDecision As Integer
        Dim bItem As Boolean
        Dim bPlayer As Boolean

        ' Prepare new row to add it in the DataSource
        Dim oDRnewRow As DataRow = Me.Decisions.NewRow

        'Insert new values
        Dim newValues As Hashtable = New Hashtable

        newValues("Decision") = CType(oEditedItem.FindControl("txtDecision"), TextBox).Text
        newValues("IDType") = CType(oEditedItem.FindControl("cboDecisionType"), DropDownList).SelectedValue
        newValues("TypeDecision") = CType(oEditedItem.FindControl("cboDecisionType"), DropDownList).SelectedItem.Text
        newValues("Value") = CType(oEditedItem.FindControl("txtDecisionValue"), TextBox).Text
        newValues("Item") = CType(oEditedItem.FindControl("chkItem"), CheckBox).Checked
        newValues("Player") = CType(oEditedItem.FindControl("chkPlayer"), CheckBox).Checked

        sDecision = Nz(newValues("Decision"))
        iIDType = Nni(newValues("IDType"))
        sValue = Nz(newValues("Value"))
        bItem = Nb(newValues("Item"))
        bPlayer = Nb(newValues("Player"))

        ' make sure that unique primary key value is generated for the inserted row 
        If Me.Decisions.Rows.Count > 0 Then
            newValues("IDDecision") = (CType(Me.Decisions.Rows((Me.Decisions.Rows.Count - 1))("IDDecision"), Integer) + 1)
        Else
            newValues("IDDecision") = 1
        End If

        iIDDecision = newValues("IDDecision")

        Try
            For Each entry As DictionaryEntry In newValues
                oDRnewRow(CType(entry.Key, String)) = entry.Value
            Next
            Me.Decisions.Rows.Add(oDRnewRow)
            Me.Decisions.AcceptChanges()

            ' Il datasource deriva da un stored multi table devo procedere anche all'aggiornamento manuale del database
            Dim sSQL As String = "INSERT INTO Decisions (Decision, IDType, IDGame, Item, Player) VALUES (" _
                               & "'" & sDecision & "', " & iIDType & ", " & m_SessionData.Utente.Games.IDGame & ", " & Boolean_To_Bit(bItem) & ", " & Boolean_To_Bit(bPlayer) & ") "
            g_DAL = New DBHelper
            g_DAL.ExecuteNonQuery(sSQL, CommandType.Text)

            HandleUpdateDecisionValue(iIDDecision, sValue)

            grdDecisions.Rebind()

        Catch ex As Exception
            Dim lblError As Label = New Label()
            lblError.Text = "Unable To insert Decision. Reason: " + ex.Message
            lblError.ForeColor = System.Drawing.Color.Red
            grdDecisions.Controls.Add(lblError)
            e.Canceled = True
        End Try
    End Sub

    Protected Sub grdDecisions_UpdateCommand(ByVal source As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs)
        Dim oEditedItem As GridEditableItem = CType(e.Item, GridEditableItem)

        Dim sDecision As String
        Dim iIDType As Integer
        Dim sValue As String
        Dim iIDDecision As Integer = oEditedItem.OwnerTableView.DataKeyValues(oEditedItem.ItemIndex)("IDDecision")
        Dim bItem As Boolean
        Dim bPlayer As Boolean

        'Locate the changed row in the DataSource
        Dim changedRows As DataRow() = Me.Decisions.Select("IDDecision = " & iIDDecision)

        If (Not changedRows.Length = 1) Then
            e.Canceled = True
            Return
        End If

        'Update new values
        Dim newValues As Hashtable = New Hashtable

        newValues("Decision") = CType(oEditedItem.FindControl("txtDecision"), TextBox).Text
        newValues("IDType") = CType(oEditedItem.FindControl("cboDecisionType"), DropDownList).SelectedValue
        newValues("TypeDecision") = CType(oEditedItem.FindControl("cboDecisionType"), DropDownList).SelectedItem.Text
        newValues("Value") = CType(oEditedItem.FindControl("txtDecisionValue"), TextBox).Text
        newValues("Item") = CType(oEditedItem.FindControl("chkItem"), CheckBox).Checked
        newValues("Player") = CType(oEditedItem.FindControl("chkPlayer"), CheckBox).Checked

        sDecision = Nz(newValues("Decision"))
        iIDType = Nni(newValues("IDType"))
        sValue = Nz(newValues("Value"))
        bItem = Nb(newValues("Item"))
        bPlayer = Nb(newValues("Player"))

        changedRows(0).BeginEdit()
        Try
            Dim entry As DictionaryEntry
            For Each entry In newValues
                changedRows(0)(CType(entry.Key, String)) = entry.Value
            Next
            changedRows(0).EndEdit()
            Me.Decisions.AcceptChanges()
        Catch ex As Exception
            changedRows(0).CancelEdit()

            Dim lblError As Label = New Label()
            lblError.Text = "Unable to update Decision. Reason: " + ex.Message
            lblError.ForeColor = System.Drawing.Color.Red
            grdDecisions.Controls.Add(lblError)

            e.Canceled = True
        End Try

        ' Il datasource deriva da un stored multi table devo procedere anche all'aggiornamento manuale del database
        Dim sSQL As String = "UPDATE Decisions SET Decision =  '" & sDecision & "', " _
                           & "IDType = " & iIDType & ", IDGame = " & m_SessionData.Utente.Games.IDGame & ", " _
                           & "Item = " & Boolean_To_Bit(bItem) & ", " _
                           & "Player = " & Boolean_To_Bit(bPlayer) & " " _
                           & "WHERE ID = " & iIDDecision
        g_DAL = New DBHelper
        g_DAL.ExecuteNonQuery(sSQL, CommandType.Text)

        HandleUpdateDecisionValue(iIDDecision, sValue)

        grdDecisions.Rebind()
    End Sub

    Private Sub HandleUpdateDecisionValue(IDDecision As Integer, Valore As String)
        Dim sSQL As String
        ' Recupero l'ID del valore da aggiornare
        Dim iIDDecisionValue As Integer
        sSQL = "SELECT ID FROM Decisions_Value WHERE IDDecision = " & IDDecision
        g_DAL = New DBHelper
        iIDDecisionValue = Nni(g_DAL.ExecuteScalar(sSQL, CommandType.Text))

        If iIDDecisionValue > 0 Then
            sSQL = "UPDATE Decisions_Value SET Value = '" & Valore & "' WHERE ID = " & iIDDecisionValue
        Else
            sSQL = "INSERT INTO Decisions_Value (Value, IDDecision) VALUES ('" & Valore & "', " & IDDecision & ") "
        End If
        g_DAL.ExecuteNonQuery(sSQL, CommandType.Text)
        g_DAL = Nothing
    End Sub

    Private Sub grdDecisions_ItemDataBound(sender As Object, e As GridItemEventArgs) Handles grdDecisions.ItemDataBound
        If (TypeOf e.Item Is GridEditableItem AndAlso e.Item.IsInEditMode) Then

            Dim oItem As GridEditableItem = e.Item

            ' Access/Modify the edit item template settings here
            Dim oCBODecisionType As DropDownList = oItem.FindControl("cboDecisionType")
            oCBODecisionType.DataSource = HandleLoadComboBox_Variables_DataType()
            oCBODecisionType.DataTextField = "TypeDescription"
            oCBODecisionType.DataValueField = "ID"
            oCBODecisionType.DataBind()
            If Not DataBinder.Eval(oItem.DataItem, "IDType").ToString = "" Then
                oCBODecisionType.SelectedValue = Nni(DataBinder.Eval(oItem.DataItem, "IDType"))
            End If

        ElseIf (TypeOf e.Item Is GridDataItem AndAlso Not e.Item.IsInEditMode AndAlso Page.IsPostBack) Then

        End If
    End Sub

End Class
