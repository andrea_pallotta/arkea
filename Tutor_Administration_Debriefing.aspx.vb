﻿
Imports System.Data
Imports APPCore.Utility
Imports APS.APSUtility
Imports Telerik.Charting
Imports Telerik.Web.UI

Partial Class Tutor_Administration_Debriefing
    Inherits System.Web.UI.Page

    Private Sub Tutor_Administration_FlowChart_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadDataChartType()
            LoadTeams()
            LoadPeriods()
            LoadItems()
            LoadDataVariables()
        End If
    End Sub

    Private Sub LoadDataChartType()
        cboChartType.DataSource = GetValuesChartType()
        cboChartType.DataValueField = "Key"
        cboChartType.DataTextField = "Value"
        cboChartType.DataBind()

        cboChartType.SelectedValue = 3
    End Sub


    Public Shared Function GetValuesChartType() As Dictionary(Of Int16, String)
        Dim enumType As Type = GetType(HtmlChart.SeriesType)

        If Not enumType.IsEnum Then
            Throw New ArgumentException("Type '" + enumType.Name + "' is not an enum")
        End If

        Dim fields As Reflection.FieldInfo() = enumType.GetFields()

        Dim oTipoGrafico As New Dictionary(Of Int16, String)

        For Each field As Reflection.FieldInfo In fields
            If field.IsLiteral Then
                Dim value As Object = field.GetValue(enumType)
                Select Case DirectCast(value, HtmlChart.SeriesType)
                    Case HtmlChart.SeriesType.Line
                        oTipoGrafico.Add(Nni(HtmlChart.SeriesType.Line), DirectCast(value, HtmlChart.SeriesType).ToString)

                    Case HtmlChart.SeriesType.Bar
                        oTipoGrafico.Add(Nni(HtmlChart.SeriesType.Bar), DirectCast(value, HtmlChart.SeriesType).ToString)

                    Case HtmlChart.SeriesType.Area
                        oTipoGrafico.Add(Nni(HtmlChart.SeriesType.Area), DirectCast(value, HtmlChart.SeriesType).ToString)

                    Case HtmlChart.SeriesType.Column
                        oTipoGrafico.Add(Nni(HtmlChart.SeriesType.Column), DirectCast(value, HtmlChart.SeriesType).ToString)

                        'Case HtmlChart.SeriesType.Donut
                        '    oTipoGrafico.Add(Nni(HtmlChart.SeriesType.Donut), DirectCast(value, HtmlChart.SeriesType).ToString)

                        'Case HtmlChart.SeriesType.RangeColumn
                        '    oTipoGrafico.Add(Nni(HtmlChart.SeriesType.RangeColumn), DirectCast(value, HtmlChart.SeriesType).ToString)

                        'Case HtmlChart.SeriesType.Waterfall
                        '    oTipoGrafico.Add(Nni(HtmlChart.SeriesType.Waterfall), DirectCast(value, HtmlChart.SeriesType).ToString)

                End Select

            End If
        Next

        Return oTipoGrafico
    End Function

    Private Sub LoadPeriods()
        Dim oDTPeriods As DataTable

        Try
            cboPeriod.Items.Clear()
            oDTPeriods = LoadPeriodGameValid(Nni(Session("IDGame")), Session("CurrentStep"))
            cboPeriod.DataSource = oDTPeriods
            cboPeriod.DataValueField = "Id"
            cboPeriod.DataTextField = "Descrizione"
            cboPeriod.DataBind()

            ReloaadPeriodsNoSimulation()

            If cboPeriod.SelectedValue <> "" Then
                Session("IDPeriod") = Nni(cboPeriod.SelectedValue)
                Session("NamePeriod") = Nz(cboPeriod.Text)
            Else
                cboPeriod.SelectedIndex = 0
                Session("IDPeriod") = Nni(cboPeriod.SelectedValue)
                Session("NamePeriod") = Nz(cboPeriod.Text)
            End If

        Catch ex As Exception
            Throw New ApplicationException("LoadPeriods -> LoadPeriods", ex)
        End Try
    End Sub

    Private Sub ReloaadPeriodsNoSimulation()
        Dim sSQL As String = "SELECT IDPeriodo FROM BGOL_Games_Periods WHERE IDGame = " & Session("IDGame") & " AND Simulation = 1 "
        Dim iIDPeriodToRemove As Integer = g_DAL.ExecuteScalar(sSQL)

        If iIDPeriodToRemove > 0 AndAlso Session("IDRole").Contains("P") Then
            For Each oItem As RadComboBoxItem In cboPeriod.Items
                If oItem.Value = iIDPeriodToRemove Then
                    cboPeriod.Items.Remove(oItem)
                    cboPeriod.SelectedIndex = 0
                    Exit For
                End If
            Next
        End If
    End Sub

    Private Sub LoadTeams()
        Dim oDTTeams As DataTable

        cboPlayer.Items.Clear()
        cboPlayer.DataSource = Nothing
        oDTTeams = LoadTeamsGame(Nni(Session("IDGame")))
        cboPlayer.DataSource = oDTTeams
        cboPlayer.DataValueField = "Id"
        cboPlayer.DataTextField = "TeamName"
        cboPlayer.DataBind()

        'If Session("IDRole").Contains("P") Then
        '    cboPlayer.Enabled = False
        '    Session("IDTeamDebriefing") = Session("IDTeam")
        'Else
        '    cboPlayer.Enabled = True
        'End If

        cboPlayer.Items.Insert(0, New RadComboBoxItem("All players", String.Empty))

        cboPlayer.SelectedIndex = 0

    End Sub

    Private Sub LoadItems()
        Dim oDTItems As DataTable

        cboItems.Items.Clear()
        cboItems.DataSource = Nothing
        oDTItems = LoadItemsGame(Session("IDgame"), Nz(Session("LanguageActive")))
        cboItems.DataSource = oDTItems
        cboItems.DataValueField = "IdVariable"
        cboItems.DataTextField = "VariableName"
        cboItems.DataBind()

        cboItems.Items.Insert(0, New RadComboBoxItem("All items", String.Empty))

        cboItems.SelectedIndex = 0
    End Sub

    Private Sub LoadDataVariables()
        Dim oDTVariables As DataTable
        Dim sSQL As String = "SELECT * FROM Variables_Debriefing ORDER BY OrderNum"
        cboVariables.Items.Clear()
        cboVariables.DataSource = Nothing
        oDTVariables = g_DAL.ExecuteDataTable(sSQL)
        cboVariables.DataSource = oDTVariables
        cboVariables.DataValueField = "VariableName"
        cboVariables.DataTextField = "Description"
        cboVariables.DataBind()

        cboVariables.SelectedIndex = -1
    End Sub

    Private Sub LoadDataChart()
        Dim chartSeries As HtmlChart.SeriesType
        Dim oGenericSeries As SeriesBase

        Dim sSQL As String
        Dim sSeriesName As String = ""
        Dim sIDPeriod As String = ""

        Dim dtDataGraph As DataTable
        Dim dtPlayer As DataTable = LoadTeamsGame(Nni(Session("IDGame")))
        Dim dtItems As DataTable = LoadItemsGame(Session("IDgame"), Nz(Session("LanguageActive")))

        Dim oDRItems As DataRow()
        Dim oDRPlayers As DataRow()

        ' Imposto la serie 
        chartSeries = cboChartType.SelectedValue

        ' Recupero tutti i periodi generati fino adesso
        sSQL = "SELECT * FROM BGOL_Games_Periods BGP " _
                 & "INNER JOIN BGOL_Periods BP ON BGP.IDPeriodo = BP.Id " _
                 & "WHERE BGP.IDGame = " & Session("IDGame") & " AND BP.NumStep <= " & Session("CurrentStep")
        If Session("IDRole").Contains("P") Then
            sSQL &= " AND ISNULL(BGP.Simulation, 0) = 0 "
        End If
        sSQL &= " ORDER BY BP.NumStep "
        Dim oDTPeriodi As DataTable = g_DAL.ExecuteDataTable(sSQL)

        grfGeneric.PlotArea.Series.Clear()
        grfGeneric.PlotArea.XAxis.Items.Clear()

        If Session("IDRole").Contains("P") Then
            'cboPlayer.SelectedValue = Session("IDTeam")
            'cboPlayer.Enabled = False
        End If

        ' Carico tutte le variaibli calcolate interessate
        sSQL = "SELECT * FROM Variables_Calculate_Define V " _
             & "LEFT JOIN Variables_Calculate_Value C ON V.Id = C.IDVariable " _
             & "WHERE LTRIM(RTRIM(V.VariableName)) = '" & Nz(cboVariables.SelectedValue) & "' AND IDGame = " & Nni(Session("IDGame"))
        dtDataGraph = g_DAL.ExecuteDataTable(sSQL)

        If Nb(Session("UseItems")) AndAlso Nz(cboVariables.SelectedValue) <> "" Then
            If Nni(Session("IDTeamDebriefing")) > 0 Then ' ho selezionato un player specifico
                If cboItems.SelectedValue <> "" AndAlso cboItems.SelectedValue > 0 Then
                    oDRItems = dtItems.Select("IDVariable = " & cboItems.SelectedValue)
                Else
                    oDRItems = dtItems.Select("")
                End If

                For Each oRowItm As DataRow In oDRItems
                    sSeriesName = oRowItm("VariableName").ToString

                    Select Case chartSeries
                        Case HtmlChart.SeriesType.Line
                            oGenericSeries = New LineSeries

                        Case HtmlChart.SeriesType.Bar
                            oGenericSeries = New BarSeries

                        Case HtmlChart.SeriesType.Area
                            oGenericSeries = New AreaSeries

                        Case HtmlChart.SeriesType.Column
                            oGenericSeries = New ColumnSeries

                        Case HtmlChart.SeriesType.Donut
                            oGenericSeries = New DonutSeries

                        Case HtmlChart.SeriesType.RangeColumn
                            oGenericSeries = New RangeColumnSeries

                        Case HtmlChart.SeriesType.Waterfall
                            oGenericSeries = New WaterfallSeries

                    End Select

                    For Each oRowPeriod As DataRow In oDTPeriodi.Rows

                        Dim oDRValue As DataRow

                        oDRValue = dtDataGraph.Select("IDPeriod = " & oRowPeriod("IdPeriodo") & " AND IDItem = " & oRowItm("IDVariable") _
                                                      & " AND VariableName = '" & cboVariables.SelectedValue & "' AND IDPlayer = " & Nni(Session("IDTeamDebriefing"))).FirstOrDefault

                        If Not IsNothing(oDRValue) Then
                            Dim oItem As New SeriesItem
                            oItem.YValue = Nn(oDRValue("Value")).ToString("N2")
                            oItem.TooltipValue = Nz(oRowItm("VariableName"))
                            oGenericSeries.Items.Add(oItem)
                            sIDPeriod &= Nz(oRowPeriod("IDPeriodo")) & ";"
                        End If

                    Next
                    grfGeneric.PlotArea.Series.Add(oGenericSeries)

                    If sIDPeriod.EndsWith(";") Then sIDPeriod = sIDPeriod.TrimEnd(";")

                    ' Grafico generico
                    oGenericSeries.Name = sSeriesName
                    oGenericSeries.VisibleInLegend = True
                Next
                For Each oRowPeriod As DataRow In oDTPeriodi.Rows
                    If sIDPeriod.Contains(oRowPeriod("IdPeriodo")) Then
                        Dim newAxisItem As New AxisItem(Nz(oRowPeriod("Descrizione")))
                        grfGeneric.PlotArea.XAxis.Items.Add(newAxisItem)
                    End If
                Next
            Else
                ' Ho selezionato tutti i player, ma uso un solo item
                If Nni(Session("IDTeamDebriefing")) > 0 Then
                    oDRPlayers = dtPlayer.Select("IDTeam = " & Nni(Session("IDTeamDebriefing")))
                Else
                    oDRPlayers = dtPlayer.Select("")
                End If

                For Each oRowPlayer As DataRow In oDRPlayers
                    sSeriesName = oRowPlayer("TeamName").ToString

                    Select Case chartSeries
                        Case HtmlChart.SeriesType.Line
                            oGenericSeries = New LineSeries

                        Case HtmlChart.SeriesType.Bar
                            oGenericSeries = New BarSeries

                        Case HtmlChart.SeriesType.Area
                            oGenericSeries = New AreaSeries

                        Case HtmlChart.SeriesType.Column
                            oGenericSeries = New ColumnSeries

                        Case HtmlChart.SeriesType.Donut
                            oGenericSeries = New DonutSeries

                        Case HtmlChart.SeriesType.RangeColumn
                            oGenericSeries = New RangeColumnSeries

                        Case HtmlChart.SeriesType.Waterfall
                            oGenericSeries = New WaterfallSeries

                    End Select

                    For Each oRowPeriod As DataRow In oDTPeriodi.Rows

                        Dim oDRValue As DataRow

                        oDRValue = dtDataGraph.Select("IDPeriod = " & oRowPeriod("IdPeriodo") & " AND VariableName = '" & cboVariables.SelectedValue _
                                                      & "' AND IDPlayer = " & Nni(oRowPlayer("ID")) & " AND IDItem = " & Nni(cboItems.SelectedValue)).FirstOrDefault

                        If Not IsNothing(oDRValue) Then
                            Dim oItem As New SeriesItem
                            oItem.YValue = Nn(oDRValue("Value")).ToString("N2")
                            oItem.TooltipValue = Nz(oRowPlayer("ID"))
                            oGenericSeries.Items.Add(oItem)
                            sIDPeriod &= Nz(oRowPeriod("IDPeriodo")) & ";"
                        End If

                    Next
                    grfGeneric.PlotArea.Series.Add(oGenericSeries)

                    If sIDPeriod.EndsWith(";") Then sIDPeriod = sIDPeriod.TrimEnd(";")

                    ' Grafico generico
                    oGenericSeries.Name = sSeriesName
                    oGenericSeries.VisibleInLegend = True
                Next

                For Each oRowPeriod As DataRow In oDTPeriodi.Rows
                    If sIDPeriod.Contains(oRowPeriod("IdPeriodo")) Then
                        Dim newAxisItem As New AxisItem(Nz(oRowPeriod("Descrizione")))
                        grfGeneric.PlotArea.XAxis.Items.Add(newAxisItem)
                    End If
                Next
            End If

        Else ' non uso gli items, ma solo i players
            If Nni(Session("IDTeamDebriefing")) > 0 AndAlso Nz(cboVariables.SelectedValue) <> "" Then
                If Nni(Session("IDTeamDebriefing")) > 0 Then ' ho selezionato un player specifico
                    If Nni(Session("IDTeamDebriefing")) > 0 Then
                        oDRPlayers = dtPlayer.Select("ID = " & Nni(Session("IDTeamDebriefing")))
                    Else
                        oDRPlayers = dtPlayer.Select("")
                    End If

                    For Each oRowPlayer As DataRow In oDRPlayers
                        sSeriesName = oRowPlayer("TeamName").ToString

                        Select Case chartSeries
                            Case HtmlChart.SeriesType.Line
                                oGenericSeries = New LineSeries

                            Case HtmlChart.SeriesType.Bar
                                oGenericSeries = New BarSeries

                            Case HtmlChart.SeriesType.Area
                                oGenericSeries = New AreaSeries

                            Case HtmlChart.SeriesType.Column
                                oGenericSeries = New ColumnSeries

                            Case HtmlChart.SeriesType.Donut
                                oGenericSeries = New DonutSeries

                            Case HtmlChart.SeriesType.RangeColumn
                                oGenericSeries = New RangeColumnSeries

                            Case HtmlChart.SeriesType.Waterfall
                                oGenericSeries = New WaterfallSeries

                        End Select

                        For Each oRowPeriod As DataRow In oDTPeriodi.Rows

                            Dim oDRValue As DataRow

                            oDRValue = dtDataGraph.Select("IDPeriod = " & oRowPeriod("IdPeriodo") _
                                                          & " AND VariableName = '" & cboVariables.SelectedValue & "' AND IDPlayer = " & Nni(Session("IDTeamDebriefing"))).FirstOrDefault

                            If Not IsNothing(oDRValue) Then
                                Dim newAxisItem As New AxisItem(Nz(oRowPeriod("Descrizione")))
                                grfGeneric.PlotArea.XAxis.Items.Add(newAxisItem)

                                Dim oItem As New SeriesItem
                                oItem.YValue = Nn(oDRValue("Value")).ToString("N2")
                                oItem.TooltipValue = Nz(oRowPlayer("TeamName"))
                                oGenericSeries.Items.Add(oItem)
                            End If

                        Next
                        grfGeneric.PlotArea.Series.Add(oGenericSeries)

                        If sIDPeriod.EndsWith(";") Then sIDPeriod = sIDPeriod.TrimEnd(";")

                        ' Grafico generico
                        oGenericSeries.Name = sSeriesName
                        oGenericSeries.VisibleInLegend = True
                    Next

                Else
                    ' Ho selezionato tutti i player, ma uso un solo item
                    If cboPlayer.SelectedValue <> "" AndAlso cboPlayer.SelectedValue > 0 Then
                        oDRPlayers = dtPlayer.Select("IDTeam = " & cboPlayer.SelectedValue)
                    Else
                        oDRPlayers = dtPlayer.Select("")
                    End If

                    For Each oRowPlayer As DataRow In oDRPlayers
                        sSeriesName = oRowPlayer("TeamName").ToString

                        Select Case chartSeries
                            Case HtmlChart.SeriesType.Line
                                oGenericSeries = New LineSeries

                            Case HtmlChart.SeriesType.Bar
                                oGenericSeries = New BarSeries

                            Case HtmlChart.SeriesType.Area
                                oGenericSeries = New AreaSeries

                            Case HtmlChart.SeriesType.Column
                                oGenericSeries = New ColumnSeries

                            Case HtmlChart.SeriesType.RangeBar
                                oGenericSeries = New RangeBarSeries

                            Case HtmlChart.SeriesType.RangeColumn
                                oGenericSeries = New RangeColumnSeries

                            Case HtmlChart.SeriesType.Waterfall
                                oGenericSeries = New WaterfallSeries

                        End Select

                        For Each oRowPeriod As DataRow In oDTPeriodi.Rows

                            Dim oDRValue As DataRow

                            oDRValue = dtDataGraph.Select("IDPeriod = " & oRowPeriod("IdPeriodo") & " AND VariableName = '" & cboVariables.SelectedValue _
                                                          & "' AND IDPlayer = " & Nni(oRowPlayer("ID"))).FirstOrDefault

                            If Not IsNothing(oDRValue) Then
                                Dim newAxisItem As New AxisItem(Nz(oRowPeriod("Descrizione")))
                                grfGeneric.PlotArea.XAxis.Items.Add(newAxisItem)

                                Dim oItem As New SeriesItem
                                oItem.YValue = Nn(oDRValue("Value")).ToString("N2")
                                oItem.TooltipValue = Nz(oRowPlayer("ID"))
                                oGenericSeries.Items.Add(oItem)
                            End If

                        Next
                        grfGeneric.PlotArea.Series.Add(oGenericSeries)

                        If sIDPeriod.EndsWith(";") Then sIDPeriod = sIDPeriod.TrimEnd(";")

                        ' Grafico generico
                        oGenericSeries.Name = sSeriesName
                        oGenericSeries.VisibleInLegend = True
                    Next
                End If
            Else
                ' Ho scelto tutti i player
                ' Ho selezionato tutti i player, ma uso un solo item
                If Nni(Session("IDTeamDebriefing")) > 0 Then
                    oDRPlayers = dtPlayer.Select("IDTeam = " & cboPlayer.SelectedValue)
                Else
                    oDRPlayers = dtPlayer.Select("")
                End If

                For Each oRowPlayer As DataRow In oDRPlayers
                    sSeriesName = oRowPlayer("TeamName").ToString

                    Select Case chartSeries
                        Case HtmlChart.SeriesType.Line
                            oGenericSeries = New LineSeries

                        Case HtmlChart.SeriesType.Bar
                            oGenericSeries = New BarSeries

                        Case HtmlChart.SeriesType.Area
                            oGenericSeries = New AreaSeries

                        Case HtmlChart.SeriesType.Column
                            oGenericSeries = New ColumnSeries

                        Case HtmlChart.SeriesType.Donut
                            oGenericSeries = New DonutSeries

                        Case HtmlChart.SeriesType.RangeColumn
                            oGenericSeries = New RangeColumnSeries

                        Case HtmlChart.SeriesType.Waterfall
                            oGenericSeries = New WaterfallSeries

                    End Select

                    For Each oRowPeriod As DataRow In oDTPeriodi.Rows
                        Dim oDRValue As DataRow

                        oDRValue = dtDataGraph.Select("IDPeriod = " & oRowPeriod("IdPeriodo") & " AND VariableName = '" & cboVariables.SelectedValue _
                                                      & "' AND IDPlayer = " & Nni(oRowPlayer("ID"))).FirstOrDefault

                        If Not IsNothing(oDRValue) Then
                            Dim oItem As New SeriesItem
                            oItem.YValue = Nn(oDRValue("Value")).ToString("N2")
                            oItem.TooltipValue = Nz(oRowPlayer("ID"))
                            oGenericSeries.Items.Add(oItem)

                            sIDPeriod &= Nz(oRowPeriod("IDPeriodo")) & ";"
                        End If

                    Next
                    grfGeneric.PlotArea.Series.Add(oGenericSeries)

                    If sIDPeriod.EndsWith(";") Then sIDPeriod = sIDPeriod.TrimEnd(";")

                    ' Grafico generico
                    oGenericSeries.Name = sSeriesName
                    oGenericSeries.VisibleInLegend = True
                Next

                For Each oRowPeriod As DataRow In oDTPeriodi.Rows
                    If sIDPeriod.Contains(oRowPeriod("IdPeriodo")) Then
                        Dim newAxisItem As New AxisItem(Nz(oRowPeriod("Descrizione")))
                        grfGeneric.PlotArea.XAxis.Items.Add(newAxisItem)
                    End If
                Next
            End If

        End If

        ' Grafico generico
        grfGeneric.PlotArea.XAxis.MinorGridLines.Visible = False
        grfGeneric.PlotArea.YAxis.MinorGridLines.Visible = False
        grfGeneric.Legend.Appearance.Visible = True
        grfGeneric.Legend.Appearance.Position = HtmlChart.ChartLegendPosition.Top

        grfGeneric.PlotArea.CommonTooltipsAppearance.DataFormatString = "N"
        grfGeneric.PlotArea.YAxis.LabelsAppearance.DataFormatString = "N"
    End Sub

    Private Sub cboPeriod_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles cboPeriod.SelectedIndexChanged
        LoadDataChart()
    End Sub

    Private Sub cboPlayer_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles cboPlayer.SelectedIndexChanged
        If Nz(cboPlayer.SelectedValue) <> "" Then
            Session("IDTeamDebriefing") = Nni(cboPlayer.SelectedValue)
        Else
            Session("IDTeamDebriefing") = 0
            cboItems.SelectedIndex = 1
        End If

        LoadDataChart()
    End Sub

    Private Sub cboVariables_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles cboVariables.SelectedIndexChanged
        Dim sSQL As String
        Dim bUseItems As Boolean

        If Nz(cboVariables.SelectedValue) <> "" AndAlso Nz(cboVariables.SelectedValue) <> "< Select data...>" Then
            ' Controllo se la variabile gestisce anche gli item, nel caso mi posiziono su tutti i player e sul primo item
            sSQL = "SELECT UseItems FROM Variables_Debriefing WHERE VariableName = '" & cboVariables.SelectedValue & "' "
            bUseItems = Nb(g_DAL.ExecuteScalar(sSQL))

            ' Abilito/disabilito la combobox degli items
            cboItems.Enabled = bUseItems

            If bUseItems Then
                ' Seleziono solo il primo item
                cboItems.SelectedIndex = 1
                ' Seleziono tutti i players
                cboPlayer.SelectedIndex = -1
            End If

            Session("UseItems") = bUseItems

            ' Controllo anche se cicla sui player
            sSQL = "SELECT C.IDPlayer FROM Variables_Calculate_Define V " _
                 & "LEFT JOIN Variables_Calculate_Value C ON V.Id = C.IDVariable " _
                 & "WHERE LTRIM(RTRIM(V.VariableName)) = '" & Nz(cboVariables.SelectedValue) & "' AND IDGame = " & Nni(Session("IDGame"))
            Session("UsePlayer") = Nb(g_DAL.ExecuteScalar(sSQL)) > 0

            LoadDataChart()
        End If
    End Sub

    Private Sub cboItems_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles cboItems.SelectedIndexChanged
        If Nz(cboItems.SelectedValue) = "" Then
            cboPlayer.SelectedIndex = 1
            Session("IDTeamDebriefing") = Nni(cboPlayer.SelectedValue)
        End If
        LoadDataChart()
    End Sub

    Private Sub cboChartType_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles cboChartType.SelectedIndexChanged
        LoadDataChart()
    End Sub

End Class

