﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" CodeFile="Tutor_Administration_FlowChart.aspx.vb" Inherits="Tutor_Administration_FlowChart" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html lang="it">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Flowchart - ARKEA</title>

    <link href="Content/Site.css" rel="stylesheet" type="text/css" />
    <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />

    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />

    <script type="text/javascript">
        (function (global, undefined) {
            var jsonTextBox, diagram;

            function diagramLoad(sender) {
                diagram = sender.get_kendoWidget();
            }

            function exportToImage() {
                diagram.exportImage().done(function (data) {
                    kendo.saveAs({
                        dataURI: data,
                        fileName: "diagram.png"
                    });
                });
            }

            function exportToPDF() {
                diagram.saveAsPDF();
            }

            function exportToSVG() {
                diagram.exportSVG().done(function (data) {
                    kendo.saveAs({
                        dataURI: data,
                        fileName: "diagram.svg"
                    });
                });

            }

            global.diagramLoad = diagramLoad;
            global.exportToImage = exportToImage;
            global.exportToPDF = exportToPDF;
            global.exportToSVG = exportToSVG;

        })(window);
    </script>

</head>

<body>
    <form id="frmMain" runat="server">
        <asp:ScriptManager runat="server" AsyncPostBackTimeout="200000" EnableScriptGlobalization="true">
            <Scripts>
                <%--To learn more about bundling scripts in ScriptManager see http://go.microsoft.com/fwlink/?LinkID=301884 --%>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="jquery" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="respond" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>

        <div>
            <asp:UpdatePanel ID="pnlMain" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <h2>
                        <asp:Label ID="lblTitolo" runat="server" Text="Flowchart"></asp:Label>
                    </h2>

                    <div class="divTableRow" style="min-height: 10px;">

                        <div class="divTableCell">
                            <telerik:RadComboBox ID="cboPeriod" runat="server" Width="100%" EmptyMessage="< Select period...>"
                                RenderMode="Lightweight" RenderingMode="Full" AutoPostBack="true" Visible="true">
                            </telerik:RadComboBox>
                        </div>

                        <div class="divTableCell">
                            <telerik:RadComboBox ID="cboPlayer" runat="server" Width="100%" EmptyMessage="< Select player...>"
                                RenderMode="Lightweight" RenderingMode="Full" AutoPostBack="true" Visible="true">
                            </telerik:RadComboBox>
                        </div>

                    </div>

                    <div class="clr"></div>

                    <div style="width: 98%; margin: 0 auto">

                        <telerik:RadDiagram ID="dgrMain" runat="server" Width="98%" Height="720px">
                            <ClientEvents OnLoad="diagramLoad" />

                            <PdfSettings FileName="diagram.pdf" Title="Diagram Exported to PDF" />

                            <ShapesCollection>
                            </ShapesCollection>
                        </telerik:RadDiagram>

                    </div>

                    <div class="clr"></div>

                    <div class="divTableRow" style="min-height: 10px;">
                        <div class="divTableCell">
                            <telerik:RadButton ID="btnExportPDF" runat="server" Text="Export PDF" Width="100%"
                                RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static"
                                OnClientClicked="exportToPDF">
                            </telerik:RadButton>
                        </div>
                    </div>

                    <div class="divTableRow" style="min-height: 10px;" />
                </ContentTemplate>
            </asp:UpdatePanel>

        </div>
    </form>
</body>
</html>
