﻿
Imports System.Data
Imports APPCore.Utility
Imports APS.APSUtility
Imports Telerik.Web.UI

Partial Class Tutor_Administration_FlowChart
    Inherits System.Web.UI.Page

    Enum TipoVariabile
        DecisionBoss
        DecisionDeveloper
        VariableState
        DecisionPlayer
        Calculate
    End Enum

    Private Sub Tutor_Administration_FlowChart_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadPlayers()
            LoadPeriods()
            LoadDataVariable()
        End If
    End Sub

    Private Sub LoadDataVariable()
        Dim dtItems As DataTable = LoadItemsGame(Session("IDGame"), Nz(Session("LanguageActive")))
        Dim sTableReturn As String
        Dim xPositionDefault As Double = 25
        Dim yPositionDefault As Double = 20

        dgrMain.ShapesCollection.Clear()

        Dim oShapeStockFinisGoods As New Telerik.Web.UI.DiagramShape
        sTableReturn = LoadTableDataItem(dtItems, "StockFinisGoods", TipoVariabile.VariableState, "Stock of finished goods")
        oShapeStockFinisGoods.Type = "Rectangle"
        oShapeStockFinisGoods.Id = "StockFinisGoods"
        oShapeStockFinisGoods.Source = sTableReturn
        oShapeStockFinisGoods.ContentSettings.Html = sTableReturn
        oShapeStockFinisGoods.Width = 210
        oShapeStockFinisGoods.X = xPositionDefault
        oShapeStockFinisGoods.Y = 20
        oShapeStockFinisGoods.FillSettings.Color = "White"
        dgrMain.ShapesCollection.Add(oShapeStockFinisGoods)

        Dim oShapeTotalOfferCusto As New Telerik.Web.UI.DiagramShape
        sTableReturn = LoadTableDataItem(dtItems, "TotalOfferCusto", TipoVariabile.Calculate, "Total available to retail")
        oShapeTotalOfferCusto.Type = "Rectangle"
        oShapeTotalOfferCusto.Id = "TotalOfferCusto"
        oShapeTotalOfferCusto.Source = sTableReturn
        oShapeTotalOfferCusto.ContentSettings.Html = sTableReturn
        oShapeTotalOfferCusto.Width = 210
        oShapeTotalOfferCusto.X = oShapeStockFinisGoods.Width + 60
        oShapeTotalOfferCusto.Y = yPositionDefault
        oShapeTotalOfferCusto.FillSettings.Color = "White"
        dgrMain.ShapesCollection.Add(oShapeTotalOfferCusto)

        Dim oShapeExtraAllow As New Telerik.Web.UI.DiagramShape
        sTableReturn = LoadTableDataItem(dtItems, "ExtraAllow", TipoVariabile.Calculate, "Bought-In")
        oShapeExtraAllow.Type = "Rectangle"
        oShapeExtraAllow.Id = "ExtraAllow"
        oShapeExtraAllow.Source = sTableReturn
        oShapeExtraAllow.ContentSettings.Html = sTableReturn
        oShapeExtraAllow.Width = 210
        oShapeExtraAllow.X = xPositionDefault
        oShapeExtraAllow.Y = oShapeStockFinisGoods.Height + 50
        oShapeExtraAllow.FillSettings.Color = "White"
        dgrMain.ShapesCollection.Add(oShapeExtraAllow)

        Dim oShapeMaximSellaGoods As New Telerik.Web.UI.DiagramShape
        sTableReturn = LoadTableDataItem(dtItems, "MaximSellaGoods", TipoVariabile.Calculate, "Maximum sales to retail")
        oShapeMaximSellaGoods.Type = "Rectangle"
        oShapeMaximSellaGoods.Id = "MaximSellaGoods"
        oShapeMaximSellaGoods.Source = sTableReturn
        oShapeMaximSellaGoods.ContentSettings.Html = sTableReturn
        oShapeMaximSellaGoods.Width = 210
        oShapeMaximSellaGoods.X = oShapeTotalOfferCusto.X
        oShapeMaximSellaGoods.Y = oShapeExtraAllow.Y + 10
        oShapeMaximSellaGoods.FillSettings.Color = "White"
        dgrMain.ShapesCollection.Add(oShapeMaximSellaGoods)

        Dim oShapeTotalProduSold As New Telerik.Web.UI.DiagramShape
        sTableReturn = LoadTableDataItem(dtItems, "TotalProduSold", TipoVariabile.Calculate, "Actual sales to retail")
        oShapeTotalProduSold.Type = "Rectangle"
        oShapeTotalProduSold.Id = "TotalProduSold"
        oShapeTotalProduSold.Source = sTableReturn
        oShapeTotalProduSold.ContentSettings.Html = sTableReturn
        oShapeTotalProduSold.Width = 210
        oShapeTotalProduSold.X = oShapeStockFinisGoods.X
        oShapeTotalProduSold.Y = oShapeExtraAllow.Y + oShapeExtraAllow.Height + 50
        oShapeTotalProduSold.FillSettings.Color = "White"
        dgrMain.ShapesCollection.Add(oShapeTotalProduSold)

        Dim oShapeGlobaMarkeSales As New Telerik.Web.UI.DiagramShape
        sTableReturn = LoadTableDataItem(dtItems, "GlobaMarkeSales", TipoVariabile.Calculate, "Total market sales")
        oShapeGlobaMarkeSales.Type = "Rectangle"
        oShapeGlobaMarkeSales.Id = "GlobaMarkeSales"
        oShapeGlobaMarkeSales.Source = sTableReturn
        oShapeGlobaMarkeSales.ContentSettings.Html = sTableReturn
        oShapeGlobaMarkeSales.Width = 210
        oShapeGlobaMarkeSales.X = oShapeStockFinisGoods.X
        oShapeGlobaMarkeSales.Y = oShapeTotalProduSold.Y + oShapeTotalProduSold.Height + 50
        oShapeGlobaMarkeSales.FillSettings.Color = "White"
        dgrMain.ShapesCollection.Add(oShapeGlobaMarkeSales)

        Dim oShapeMarketShare As New Telerik.Web.UI.DiagramShape
        sTableReturn = LoadTableDataItem(dtItems, "MarketShare", TipoVariabile.Calculate, "Market share")
        oShapeMarketShare.Type = "Rectangle"
        oShapeMarketShare.Id = "MarketShare"
        oShapeMarketShare.Source = sTableReturn
        oShapeMarketShare.ContentSettings.Html = sTableReturn
        oShapeMarketShare.Width = 210
        oShapeMarketShare.X = oShapeStockFinisGoods.X
        oShapeMarketShare.Y = oShapeGlobaMarkeSales.Y + oShapeGlobaMarkeSales.Height + 50
        oShapeMarketShare.FillSettings.Color = "White"
        dgrMain.ShapesCollection.Add(oShapeMarketShare)

        Dim oShapeIndexOfCompetition As New Telerik.Web.UI.DiagramShape
        sTableReturn = LoadTableDataItem(dtItems, "IndexOfCompetition", TipoVariabile.Calculate, "Competitive index")
        oShapeIndexOfCompetition.Type = "Rectangle"
        oShapeIndexOfCompetition.Id = "IndexOfCompetition"
        oShapeIndexOfCompetition.Source = sTableReturn
        oShapeIndexOfCompetition.ContentSettings.Html = sTableReturn
        oShapeIndexOfCompetition.Width = 210
        oShapeIndexOfCompetition.X = oShapeMaximSellaGoods.X
        oShapeIndexOfCompetition.Y = oShapeTotalProduSold.Y
        oShapeIndexOfCompetition.FillSettings.Color = "White"
        dgrMain.ShapesCollection.Add(oShapeIndexOfCompetition)

        Dim oShapeMarketDemand As New Telerik.Web.UI.DiagramShape
        sTableReturn = LoadTableDataItem(dtItems, "MarketDemand", TipoVariabile.Calculate, "Total market demand")
        oShapeMarketDemand.Type = "Rectangle"
        oShapeMarketDemand.Id = "MarketDemand"
        oShapeMarketDemand.Source = sTableReturn
        oShapeMarketDemand.ContentSettings.Html = sTableReturn
        oShapeMarketDemand.Width = 210
        oShapeMarketDemand.X = oShapeMaximSellaGoods.X
        oShapeMarketDemand.Y = oShapeMarketShare.Y
        oShapeMarketDemand.FillSettings.Color = "White"
        dgrMain.ShapesCollection.Add(oShapeMarketDemand)

        Dim oShapeIndexOfAttraction As New Telerik.Web.UI.DiagramShape
        sTableReturn = LoadDataIndexOfAttraction()
        oShapeIndexOfAttraction.Type = "Rectangle"
        oShapeIndexOfAttraction.Id = "DataIndexOfAttraction"
        oShapeIndexOfAttraction.Source = sTableReturn
        oShapeIndexOfAttraction.ContentSettings.Html = sTableReturn
        oShapeIndexOfAttraction.Width = 210
        oShapeIndexOfAttraction.X = oShapeGlobaMarkeSales.X + oShapeGlobaMarkeSales.Width + 35
        oShapeIndexOfAttraction.Y = oShapeGlobaMarkeSales.Y
        oShapeIndexOfAttraction.FillSettings.Color = "White"
        dgrMain.ShapesCollection.Add(oShapeIndexOfAttraction)

        Dim oShapeTotalVolumSpace As New Telerik.Web.UI.DiagramShape
        sTableReturn = LoadDataSpace()
        oShapeTotalVolumSpace.Type = "Rectangle"
        oShapeTotalVolumSpace.Id = "DataSpace"
        oShapeTotalVolumSpace.Source = sTableReturn
        oShapeTotalVolumSpace.ContentSettings.Html = sTableReturn
        oShapeTotalVolumSpace.Width = 210
        oShapeTotalVolumSpace.X = oShapeTotalOfferCusto.X + oShapeTotalOfferCusto.Width + 50
        oShapeTotalVolumSpace.Y = oShapeTotalOfferCusto.Y + (oShapeTotalOfferCusto.Height - 20)
        oShapeTotalVolumSpace.FillSettings.Color = "White"
        dgrMain.ShapesCollection.Add(oShapeTotalVolumSpace)

        Dim oShapeTotalDataManger As New Telerik.Web.UI.DiagramShape
        sTableReturn = LoadDataManager()
        oShapeTotalDataManger.Type = "Rectangle"
        oShapeTotalDataManger.Id = "DataManager"
        oShapeTotalDataManger.Source = sTableReturn
        oShapeTotalDataManger.ContentSettings.Html = sTableReturn
        oShapeTotalDataManger.Width = 210
        oShapeTotalDataManger.X = oShapeTotalVolumSpace.X
        oShapeTotalDataManger.Y = oShapeTotalVolumSpace.Y + oShapeTotalOfferCusto.Height + 20
        oShapeTotalDataManger.FillSettings.Color = "White"
        dgrMain.ShapesCollection.Add(oShapeTotalDataManger)

        Dim oShapeStore As New Telerik.Web.UI.DiagramShape
        sTableReturn = LoadDataStore()
        oShapeStore.Type = "Rectangle"
        oShapeStore.Id = "DataStore"
        oShapeStore.Source = sTableReturn
        oShapeStore.ContentSettings.Html = sTableReturn
        oShapeStore.Width = 210
        oShapeStore.X = oShapeTotalDataManger.X
        oShapeStore.Y = oShapeTotalDataManger.Y + oShapeTotalDataManger.Height + 20
        oShapeStore.FillSettings.Color = "White"
        dgrMain.ShapesCollection.Add(oShapeStore)

        Dim oShapeQualiGoodsOffer As New Telerik.Web.UI.DiagramShape
        sTableReturn = LoadTableDataItem(dtItems, "QualiGoodsOffer", TipoVariabile.Calculate, "Quality of the offered goods")
        oShapeQualiGoodsOffer.Type = "Rectangle"
        oShapeQualiGoodsOffer.Id = "QualiGoodsOffer"
        oShapeQualiGoodsOffer.Source = sTableReturn
        oShapeQualiGoodsOffer.ContentSettings.Html = sTableReturn
        oShapeQualiGoodsOffer.Width = 210
        oShapeQualiGoodsOffer.X = oShapeTotalVolumSpace.X + oShapeTotalVolumSpace.Width + 70
        oShapeQualiGoodsOffer.Y = oShapeIndexOfCompetition.Y
        oShapeQualiGoodsOffer.FillSettings.Color = "White"
        dgrMain.ShapesCollection.Add(oShapeQualiGoodsOffer)

        Dim oShapeExtraProduQuali As New Telerik.Web.UI.DiagramShape
        sTableReturn = LoadTableDataItem(dtItems, "ExtraProduQuali", TipoVariabile.DecisionBoss, "Bought-in (quality)")
        oShapeExtraProduQuali.Type = "Rectangle"
        oShapeExtraProduQuali.Id = "ExtraProduQuali"
        oShapeExtraProduQuali.Source = sTableReturn
        oShapeExtraProduQuali.ContentSettings.Html = sTableReturn
        oShapeExtraProduQuali.Width = 210
        oShapeExtraProduQuali.X = oShapeQualiGoodsOffer.X
        oShapeExtraProduQuali.Y = oShapeTotalOfferCusto.Y
        oShapeExtraProduQuali.FillSettings.Color = "White"
        dgrMain.ShapesCollection.Add(oShapeExtraProduQuali)

        Dim oShapeCompoMarkeQuali As New Telerik.Web.UI.DiagramShape
        sTableReturn = LoadTableDataItem(dtItems, "CompoMarkeQuali", TipoVariabile.Calculate, "Average Mkt Q")
        oShapeCompoMarkeQuali.Type = "Rectangle"
        oShapeCompoMarkeQuali.Id = "CompoMarkeQuali"
        oShapeCompoMarkeQuali.Source = sTableReturn
        oShapeCompoMarkeQuali.ContentSettings.Html = sTableReturn
        oShapeCompoMarkeQuali.Width = 210
        oShapeCompoMarkeQuali.X = oShapeExtraProduQuali.X + oShapeExtraProduQuali.Width + 30
        oShapeCompoMarkeQuali.Y = oShapeExtraProduQuali.Y
        oShapeCompoMarkeQuali.FillSettings.Color = "White"
        dgrMain.ShapesCollection.Add(oShapeCompoMarkeQuali)

        Dim oShapeDataQuality As New Telerik.Web.UI.DiagramShape
        sTableReturn = LoadDataQuality()
        oShapeDataQuality.Type = "Rectangle"
        oShapeDataQuality.Id = "DataQuality"
        oShapeDataQuality.Source = sTableReturn
        oShapeDataQuality.ContentSettings.Html = sTableReturn
        oShapeDataQuality.Width = 230
        oShapeDataQuality.X = oShapeCompoMarkeQuali.X - 10
        oShapeDataQuality.Y = oShapeCompoMarkeQuali.Y + oShapeCompoMarkeQuali.Height + 60
        oShapeDataQuality.FillSettings.Color = "White"
        dgrMain.ShapesCollection.Add(oShapeDataQuality)

        Dim oShapeCompoDiscoPrice As New Telerik.Web.UI.DiagramShape
        sTableReturn = LoadTableDataItem(dtItems, "CompoDiscoPrice", TipoVariabile.Calculate, "Average market perceived price")
        oShapeCompoDiscoPrice.Type = "Rectangle"
        oShapeCompoDiscoPrice.Id = "CompoDiscoPrice"
        oShapeCompoDiscoPrice.Source = sTableReturn
        oShapeCompoDiscoPrice.ContentSettings.Html = sTableReturn
        oShapeCompoDiscoPrice.Width = 230
        oShapeCompoDiscoPrice.X = oShapeDataQuality.X - 30
        oShapeCompoDiscoPrice.Y = oShapeDataQuality.Y + oShapeDataQuality.Height + 130
        oShapeCompoDiscoPrice.FillSettings.Color = "White"
        dgrMain.ShapesCollection.Add(oShapeCompoDiscoPrice)

        Dim oShapeDiscoPriceOffer As New Telerik.Web.UI.DiagramShape
        sTableReturn = LoadTableDataItem(dtItems, "DiscoPriceOffer", TipoVariabile.Calculate, "Your perceived price (price promotion)")
        oShapeDiscoPriceOffer.Type = "Rectangle"
        oShapeDiscoPriceOffer.Id = "DiscoPriceOffer"
        oShapeDiscoPriceOffer.Source = sTableReturn
        oShapeDiscoPriceOffer.ContentSettings.Html = sTableReturn
        oShapeDiscoPriceOffer.Width = 230
        oShapeDiscoPriceOffer.X = oShapeCompoDiscoPrice.X
        oShapeDiscoPriceOffer.Y = oShapeMarketDemand.Y
        oShapeDiscoPriceOffer.FillSettings.Color = "White"
        dgrMain.ShapesCollection.Add(oShapeDiscoPriceOffer)

        dgrMain.ConnectionDefaultsSettings.EndCap = Diagram.ConnectionEndCap.ArrowEnd

        GetConnections()

        pnlMain.Update()
    End Sub

    Private Sub GetConnections()
        Dim oConnector As New DiagramConnection

        oConnector.FromSettings.ShapeId = "StockFinisGoods"
        oConnector.ToSettings.ShapeId = "TotalOfferCusto"
        dgrMain.ConnectionsCollection.Add(oConnector)

        oConnector = New DiagramConnection
        oConnector.FromSettings.ShapeId = "ExtraAllow"
        oConnector.ToSettings.ShapeId = "TotalOfferCusto"
        dgrMain.ConnectionsCollection.Add(oConnector)

        oConnector = New DiagramConnection
        oConnector.FromSettings.ShapeId = "TotalOfferCusto"
        oConnector.ToSettings.ShapeId = "MaximSellaGoods"
        dgrMain.ConnectionsCollection.Add(oConnector)

        oConnector = New DiagramConnection
        oConnector.FromSettings.ShapeId = "MaximSellaGoods"
        oConnector.ToSettings.ShapeId = "TotalProduSold"
        dgrMain.ConnectionsCollection.Add(oConnector)

        oConnector = New DiagramConnection
        oConnector.FromSettings.ShapeId = "TotalProduSold"
        oConnector.ToSettings.ShapeId = "MarketShare"
        dgrMain.ConnectionsCollection.Add(oConnector)

        oConnector = New DiagramConnection
        oConnector.FromSettings.ShapeId = "GlobaMarkeSales"
        oConnector.ToSettings.ShapeId = "MarketShare"
        dgrMain.ConnectionsCollection.Add(oConnector)

        oConnector = New DiagramConnection
        oConnector.FromSettings.ShapeId = "IndexOfCompetition"
        oConnector.ToSettings.ShapeId = "TotalProduSold"
        dgrMain.ConnectionsCollection.Add(oConnector)

        oConnector = New DiagramConnection
        oConnector.FromSettings.ShapeId = "MarketDemand"
        oConnector.ToSettings.ShapeId = "TotalProduSold"
        dgrMain.ConnectionsCollection.Add(oConnector)

        oConnector = New DiagramConnection
        oConnector.FromSettings.ShapeId = "DataIndexOfAttraction"
        oConnector.ToSettings.ShapeId = "IndexOfCompetition"
        dgrMain.ConnectionsCollection.Add(oConnector)

        oConnector = New DiagramConnection
        oConnector.FromSettings.ShapeId = "DataSpace"
        oConnector.ToSettings.ShapeId = "MaximSellaGoods"
        dgrMain.ConnectionsCollection.Add(oConnector)

        oConnector = New DiagramConnection
        oConnector.FromSettings.ShapeId = "DataStore"
        oConnector.ToSettings.ShapeId = "DataIndexOfAttraction"
        dgrMain.ConnectionsCollection.Add(oConnector)

        oConnector = New DiagramConnection
        oConnector.FromSettings.ShapeId = "DataManager"
        oConnector.ToSettings.ShapeId = "DataSpace"
        dgrMain.ConnectionsCollection.Add(oConnector)

        oConnector = New DiagramConnection
        oConnector.FromSettings.ShapeId = "DataManager"
        oConnector.ToSettings.ShapeId = "MaximSellaGoods"
        dgrMain.ConnectionsCollection.Add(oConnector)

        oConnector = New DiagramConnection
        oConnector.FromSettings.ShapeId = "QualiGoodsOffer"
        oConnector.ToSettings.ShapeId = "IndexOfCompetition"
        dgrMain.ConnectionsCollection.Add(oConnector)

        oConnector = New DiagramConnection
        oConnector.FromSettings.ShapeId = "QualiGoodsOffer"
        oConnector.ToSettings.ShapeId = "MarketDemand"
        dgrMain.ConnectionsCollection.Add(oConnector)

        oConnector = New DiagramConnection
        oConnector.FromSettings.ShapeId = "ExtraProduQuali"
        oConnector.ToSettings.ShapeId = "QualiGoodsOffer"
        dgrMain.ConnectionsCollection.Add(oConnector)

        oConnector = New DiagramConnection
        oConnector.FromSettings.ShapeId = "DataQuality"
        oConnector.ToSettings.ShapeId = "DataIndexOfAttraction"
        dgrMain.ConnectionsCollection.Add(oConnector)

        oConnector = New DiagramConnection
        oConnector.FromSettings.ShapeId = "CompoDiscoPrice"
        oConnector.ToSettings.ShapeId = "IndexOfCompetition"
        dgrMain.ConnectionsCollection.Add(oConnector)

        oConnector = New DiagramConnection
        oConnector.FromSettings.ShapeId = "CompoDiscoPrice"
        oConnector.ToSettings.ShapeId = "MarketDemand"
        dgrMain.ConnectionsCollection.Add(oConnector)

        oConnector = New DiagramConnection
        oConnector.FromSettings.ShapeId = "CompoDiscoPrice"
        oConnector.ToSettings.ShapeId = "DiscoPriceOffer"
        dgrMain.ConnectionsCollection.Add(oConnector)

    End Sub

    Private Function LoadTableDataItem(DataItems As DataTable, NomeVariabile As String, TipoCalcolo As TipoVariabile, Titolo As String) As String
        Dim sNomeVariabileLista As String
        Dim sTable As String
        Dim dValore As Double
        Dim iIDGame As Integer = Nni(Session("IDGame"))
        Dim iIDPeriod As Integer = Nni(cboPeriod.SelectedValue)

        Dim sNomeItem As String = ""
        Dim sNomeTeam As String = ""

        sTable = "<table class=|tableFlowchart|>
                    <tr>
                        <th colspan=|2| style=|color:#fff;vertical-align:middle; font-size: 9px|>
                             " & Titolo & " 
                        </th>
                    </tr>"
        For Each drItem As DataRow In DataItems.Rows
            sNomeItem = Nz(drItem("VariableName"))
            sNomeTeam = cboPlayer.Text
            sNomeVariabileLista = "var_" & sNomeTeam & "_" & NomeVariabile & "_" & sNomeItem
            Select Case TipoCalcolo
                Case TipoVariabile.DecisionBoss
                    If NomeVariabile.ToUpper = "EXTRAPRODUQUALI" Then
                        sNomeVariabileLista = NomeVariabile & "_" & sNomeItem
                    End If
                    dValore = GetVariableBoss(NomeVariabile, 0, iIDPeriod, sNomeVariabileLista, iIDGame)

                Case TipoVariabile.DecisionDeveloper

                Case TipoVariabile.VariableState
                    dValore = GetVariableState(NomeVariabile, Nni(cboPlayer.SelectedValue), iIDPeriod, sNomeVariabileLista, iIDGame, 0, drItem("IDVariable"))

                Case TipoVariabile.DecisionPlayer

                Case TipoVariabile.Calculate
                    Select Case NomeVariabile.ToUpper
                        Case "GLOBAMARKESALES", "MARKETDEMAND", "COMPOMARKEQUALI", "COMPODISCOPRICE"
                            dValore = GetVariableDefineValue(NomeVariabile, 0, drItem("IDVariable"), 0, iIDPeriod, iIDGame)

                        Case Else
                            dValore = GetVariableDefineValue(NomeVariabile, Nni(cboPlayer.SelectedValue), drItem("IDVariable"), 0, iIDPeriod, iIDGame)
                    End Select

            End Select


            sTable &= "<tr>
                        <td style=|width:140px; font-size: 9px|> " &
                           sNomeItem & "
                        </td>"

            sTable &= "<td style=|width:100px; text-align: right; font-size: 9px|>                             
                            " & dValore.ToString("N2") & "
                        </td>
                    </tr>"
        Next

        sTable &= "</table>"
        sTable = sTable.Replace("|", "\""")
        sTable = sTable.Replace("\", "")

        Return sTable
    End Function

    Private Function LoadDataSpace() As String
        Dim sTable As String
        Dim dValore As Double
        Dim iIDGame As Integer = Nni(Session("IDGame"))
        Dim iIDPeriod As Integer = Nni(cboPeriod.SelectedValue)

        Dim sNomeTeam As String = ""

        sTable = "<table class=|tableFlowchart|>"

        dValore = GetVariableDefineValue("TotalVolumSpace", Nni(cboPlayer.SelectedValue), 0, 0, iIDPeriod, iIDGame)
        sTable &= "<tr>
                        <td style=|width:140px; font-size: 9px|> 
                            Space required
                        </td>"
        sTable &= "<td style=|width:100px; text-align: right; font-size: 9px|>                             
                            " & dValore.ToString("N2") & "
                        </td>
                    </tr>"

        dValore = GetVariableDefineValue("MaximSpaceAvail", Nni(cboPlayer.SelectedValue), 0, 0, iIDPeriod, iIDGame)
        sTable &= "<tr>
                        <td style=|width:140px; font-size: 9px|> 
                            Space available
                        </td>"
        sTable &= "<td style=|width:100px; text-align: right; font-size: 9px|>                             
                            " & dValore.ToString("N2") & "
                        </td>
                    </tr>"

        sTable &= "</table>"
        sTable = sTable.Replace("|", "\""")
        sTable = sTable.Replace("\", "")

        Return sTable
    End Function

    Private Function LoadDataManager() As String
        Dim sTable As String
        Dim dValore As Double
        Dim iIDGame As Integer = Nni(Session("IDGame"))
        Dim iIDPeriod As Integer = Nni(cboPeriod.SelectedValue)

        Dim sNomeTeam As String = ""

        sTable = "<table class=|tableFlowchart|>"

        dValore = GetVariableDefineValue("CurreSuperStaff", Nni(cboPlayer.SelectedValue), 0, 0, iIDPeriod, iIDGame)
        sTable &= "<tr>
                        <td style=|width:140px; font-size: 9px|> 
                            Total manager
                        </td>"
        sTable &= "<td style=|width:100px; text-align: right; font-size: 9px|>                             
                            " & dValore.ToString("N2") & "
                        </td>
                    </tr>"

        dValore = GetVariableDefineValue("CurreSalesPeopl", Nni(cboPlayer.SelectedValue), 0, 0, iIDPeriod, iIDGame)
        sTable &= "<tr>
                        <td style=|width:140px; font-size: 9px|> 
                            Total sales persons
                        </td>"
        sTable &= "<td style=|width:100px; text-align: right; font-size: 9px|>                             
                            " & dValore.ToString("N2") & "
                        </td>
                    </tr>"

        dValore = GetVariableDefineValue("TableEffecStaff", Nni(cboPlayer.SelectedValue), 0, 0, iIDPeriod, iIDGame)
        sTable &= "<tr>
                        <td style=|width:140px; font-size: 9px|> 
                            Manager effectiveness
                        </td>"
        sTable &= "<td style=|width:100px; text-align: right; font-size: 9px|>                             
                            " & dValore.ToString("N2") & "
                        </td>
                    </tr>"

        dValore = GetVariableDefineValue("TableEffecPoint", Nni(cboPlayer.SelectedValue), 0, 0, iIDPeriod, iIDGame)
        sTable &= "<tr>
                        <td style=|width:140px; font-size: 9px|> 
                            Sales effectiveness
                        </td>"
        sTable &= "<td style=|width:100px; text-align: right; font-size: 9px|>                             
                            " & dValore.ToString("N2") & "
                        </td>
                    </tr>"

        sTable &= "</table>"
        sTable = sTable.Replace("|", "\""")
        sTable = sTable.Replace("\", "")

        Return sTable
    End Function

    Private Function LoadDataStore() As String
        Dim sTable As String
        Dim dValore As Double
        Dim iIDGame As Integer = Nni(Session("IDGame"))
        Dim iIDPeriod As Integer = Nni(cboPeriod.SelectedValue)

        Dim sNomeTeam As String = ""

        sTable = "<table class=|tableFlowchart|>"

        dValore = GetVariableDefineValue("CurreOutleCentr", Nni(cboPlayer.SelectedValue), 0, 0, iIDPeriod, iIDGame)
        sTable &= "<tr>
                        <td style=|width:140px; font-size: 9px|> 
                            Central stores
                        </td>"
        sTable &= "<td style=|width:100px; text-align: right; font-size: 9px|>                             
                            " & dValore.ToString("N2") & "
                        </td>
                    </tr>"

        sTable &= "</table>"
        sTable = sTable.Replace("|", "\""")
        sTable = sTable.Replace("\", "")

        Return sTable
    End Function

    Private Function LoadDataIndexOfAttraction() As String
        Dim sTable As String
        Dim dValore As Double
        Dim iIDGame As Integer = Nni(Session("IDGame"))
        Dim iIDPeriod As Integer = Nni(cboPeriod.SelectedValue)

        Dim sNomeTeam As String = ""

        sTable = "<table class=|tableFlowchart|>"

        dValore = GetVariableDefineValue("IndexOfAttraction", Nni(cboPlayer.SelectedValue), 0, 0, iIDPeriod, iIDGame)
        sTable &= "<tr>
                        <td style=|width:140px; font-size: 9px|> 
                            Index of attraction
                        </td>"
        sTable &= "<td style=|width:100px; text-align: right; font-size: 9px|>                             
                            " & dValore.ToString("N2") & "
                        </td>
                    </tr>"

        sTable &= "</table>"
        sTable = sTable.Replace("|", "\""")
        sTable = sTable.Replace("\", "")

        Return sTable
    End Function

    Private Function LoadDataQuality() As String
        Dim sTable As String
        Dim dValore As Double
        Dim iIDGame As Integer = Nni(Session("IDGame"))
        Dim iIDPeriod As Integer = Nni(cboPeriod.SelectedValue)

        sTable = "<table class=|tableFlowchart|>
                    <tr>
                        <th style=|background-color:#fff; color:#fff;vertical-align:middle; font-size: 9px; border: none|>     
                        </th>

                        <th style=|color:#fff;vertical-align:middle; font-size: 9px|>
                             Company
                        </th>

                        <th style=|color:#fff;vertical-align:middle; font-size: 9px|>
                             Market 
                        </th>
                    </tr>"

        sTable &= "<tr>"
        sTable &= "<td style=|width:120px; font-size: 9px|> Quality of advertising </td>"

        dValore = GetVariableDefineValue("AdverQualiProce", Nni(cboPlayer.SelectedValue), 0, 0, iIDPeriod, iIDGame)
        sTable &= "<td style=|width:60px; font-size: 9px; text-align: right;|> " & dValore.ToString("N2") & " </td>"

        dValore = GetVariableState("AbsolAdverMarke", 0, iIDPeriod, "", iIDGame, 0, 0)
        sTable &= "<td style=|width:60px; font-size: 9px; text-align: right;|> " & dValore.ToString("N2") & " </td>"
        sTable &= "</tr>"

        sTable &= "<tr>"
        sTable &= "<td style=|font-size: 9px|> Quality of enviroment </td>"

        dValore = GetVariableDefineValue("GreenQualiProce", Nni(cboPlayer.SelectedValue), 0, 0, iIDPeriod, iIDGame)
        sTable &= "<td style=|font-size: 9px; text-align: right;|> " & dValore.ToString("N2") & " </td>"

        dValore = GetVariableState("AbsolQualiGreen", 0, iIDPeriod, "", iIDGame, 0, 0)
        sTable &= "<td style=|font-size: 9px; text-align: right;|> " & dValore.ToString("N2") & " </td>"
        sTable &= "</tr>"

        sTable &= "<tr>"
        sTable &= "<td style=|font-size: 9px|> Quality of customer service </td>"

        dValore = GetVariableDefineValue("TrainQualiProce", Nni(cboPlayer.SelectedValue), 0, 0, iIDPeriod, iIDGame)
        sTable &= "<td style=|font-size: 9px; text-align: right;|> " & dValore.ToString("N2") & " </td>"

        dValore = GetVariableState("AbsolQualiTrain", 0, iIDPeriod, "", iIDGame, 0, 0)
        sTable &= "<td style=|font-size: 9px; text-align: right;|> " & dValore.ToString("N2") & " </td>"
        sTable &= "</tr>"

        sTable &= "</table>"
        sTable = sTable.Replace("|", "\""")
        sTable = sTable.Replace("\", "")

        Return sTable
    End Function

    Private Sub LoadPeriods()
        Dim oDTPeriods As DataTable

        Try
            cboPeriod.Items.Clear()
            oDTPeriods = LoadPeriodGameValid(Nni(Session("IDGame")), Session("CurrentStep"))
            cboPeriod.DataSource = oDTPeriods
            cboPeriod.DataValueField = "Id"
            cboPeriod.DataTextField = "Descrizione"
            cboPeriod.DataBind()

            ReloaadPeriodsNoSimulation()

            If cboPeriod.SelectedValue <> "" Then
                Session("IDPeriod") = Nni(cboPeriod.SelectedValue)
                Session("NamePeriod") = Nz(cboPeriod.Text)
            Else
                cboPeriod.SelectedIndex = 0
                Session("IDPeriod") = Nni(cboPeriod.SelectedValue)
                Session("NamePeriod") = Nz(cboPeriod.Text)
            End If

        Catch ex As Exception
            Throw New ApplicationException("LoadPeriods -> LoadPeriods", ex)
        End Try
    End Sub

    Private Sub LoadPlayers()
        Dim dtPLayers As DataTable

        Try
            cboPlayer.Items.Clear()
            dtPLayers = LoadTeamsGame(Session("IDGame"))
            cboPlayer.DataSource = dtPLayers
            cboPlayer.DataValueField = "ID"
            cboPlayer.DataTextField = "TeamName"
            cboPlayer.DataBind()

            cboPlayer.SelectedValue = Nni(Session("IDTeam"))

            cboPlayer.Enabled = Nz(Session("IDRole")).ToUpper <> "P"

        Catch ex As Exception
            Throw New ApplicationException("LoadPlayers -> LoadPeriods", ex)
        End Try
    End Sub

    Private Sub ReloaadPeriodsNoSimulation()
        Dim sSQL As String = "SELECT IDPeriodo FROM BGOL_Games_Periods WHERE IDGame = " & Session("IDGame") & " AND Simulation = 1 "
        Dim iIDPeriodToRemove As Integer = g_DAL.ExecuteScalar(sSQL)

        If iIDPeriodToRemove > 0 AndAlso Session("IDRole").Contains("P") Then
            For Each oItem As RadComboBoxItem In cboPeriod.Items
                If oItem.Value = iIDPeriodToRemove Then
                    cboPeriod.Items.Remove(oItem)
                    cboPeriod.SelectedIndex = 0
                    Exit For
                End If
            Next
        End If
    End Sub

    Private Sub cboPeriod_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles cboPeriod.SelectedIndexChanged
        If Nz(cboPeriod.SelectedValue) <> "" Then
            LoadDataVariable()
        End If
    End Sub

    Private Sub cboPlayer_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles cboPlayer.SelectedIndexChanged
        If Nz(cboPlayer.SelectedValue) <> "" Then
            LoadDataVariable()
        End If
    End Sub

End Class
