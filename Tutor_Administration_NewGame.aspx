﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Tutor_Administration_NewGame.aspx.vb" Inherits="Tutor_Administration_NewGame" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="cntMain" ContentPlaceHolderID="MainContent" runat="Server">

    <asp:UpdatePanel runat="server" ID="pnlMain" UpdateMode="Conditional">
        <ContentTemplate>
            <h2>
                <asp:Label ID="lblWelcome" runat="server" Text="Tutor administration" />
            </h2>

            <div class="clr"></div>

            <div id="subTitle">
                <div id="subLeft" style="float: left; width: 30%;">
                    <h3>
                        <asp:Label ID="lblTitle" runat="server" Text="Create a new game"></asp:Label>
                    </h3>
                </div>

                <div id="subRight" style="float: right; padding-right: 5px;">
                    <div id="divNewGame" style="margin-top: 10px; margin-bottom: 5px; text-align: right;">
                        <telerik:RadButton ID="btnNewGame" runat="server" Text="New game"
                            RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                        </telerik:RadButton>
                    </div>
                </div>
            </div>

            <div class="clr"></div>

            <div class="row" style="padding-bottom: 5px;">
                <asp:Panel ID="pnlTableGames" runat="server">
                    <telerik:RadGrid ID="grdGames" runat="server" RenderMode="Lightweight" AllowPaging="True" AllowSorting="True" EnableLinqExpressions="false"
                        OnNeedDataSource="grdGames_NeedDataSource" Width="670px" AllowFilteringByColumn="True" Culture="it-IT" GroupPanelPosition="Top" PageSize="50">
                        <MasterTableView AutoGenerateColumns="false" TableLayout="Fixed" CssClass="RadGrid_WebBlue"
                            DataKeyNames="ID,Descrizione">

                            <Columns>
                                <telerik:GridButtonColumn CommandName="Modify" Text="Edit" UniqueName="EditColumn"
                                    ButtonType="ImageButton" ImageUrl="~/Content/GridTelerik/Edit.gif">
                                    <HeaderStyle Width="20px" />
                                </telerik:GridButtonColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="Descrizione" HeaderText="Game" UniqueName="Descrizione"
                                    FilterControlWidth="130px" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                    <HeaderStyle Width="130px" />
                                </telerik:GridBoundColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="Modello" HeaderText="Modello" UniqueName="Modello"
                                    FilterControlWidth="90px" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                    <HeaderStyle Width="90px" />
                                </telerik:GridBoundColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="DefaultPage" HeaderText="Def. page" UniqueName="DefaultPage"
                                    FilterControlWidth="110px" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                    <HeaderStyle Width="110px" />
                                </telerik:GridBoundColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridButtonColumn CommandName="Delete" Text="Delete" UniqueName="Delete" ConfirmText="Are you sure you want to delete de selected item?"
                                    ButtonType="ImageButton" ImageUrl="Images/Delete16.png" Resizable="false">
                                    <HeaderStyle Width="20px" />
                                </telerik:GridButtonColumn>
                            </Columns>

                            <%--COLONNE NASCOSTE--%>
                            <Columns>
                                <telerik:GridBoundColumn DataField="ID" HeaderText="ID" UniqueName="ID" Visible="false" />
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="DataInizio" HeaderText="Data inizio" UniqueName="DataInizio" Visible="false"
                                    FilterControlWidth="90px" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                    <HeaderStyle Width="90px" />
                                </telerik:GridBoundColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="DataFine" HeaderText="Data fine" UniqueName="DataFine" Visible="false"
                                    FilterControlWidth="90px" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                    <HeaderStyle Width="90px" />
                                </telerik:GridBoundColumn>
                            </Columns>

                        </MasterTableView>
                    </telerik:RadGrid>

                </asp:Panel>
            </div>

            <div class="divTableRow">
                <asp:PlaceHolder runat="server" ID="Message" Visible="false">
                    <p class="text-danger">
                        <asp:Literal runat="server" ID="MessageText" />
                    </p>
                </asp:PlaceHolder>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="clr"></div>

    <div class="divTableRow">
        <asp:UpdatePanel ID="pnlGamesDetailUpdate" runat="server" UpdateMode="Conditional">
            <ContentTemplate>

                <input id="hdnPanelGames" type="hidden" name="hddClick" runat="server" />
                <ajaxToolkit:ModalPopupExtender ID="mpeMain" runat="server" PopupControlID="pnlNewGame" TargetControlID="hdnPanelGames"
                    BackgroundCssClass="modalBackground" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>

                <asp:Panel runat="server" ID="pnlNewGame" CssClass="modalPopup">
                    <div class="pnlheader">
                        <asp:Label ID="lblPanelTitle" runat="server" Text="Manage game" />
                    </div>

                    <div class="pnlbody">
                        <!-- GESTIONE DELLA TABELLA CON DIV -->
                        <asp:UpdatePanel runat="server" ID="updDataDetail">
                            <ContentTemplate>
                                <div class="divTable">
                                    <div class="divTableBody">
                                        <div class="divTableRow">
                                            <div class="divTableCell" style="width: 250px;">
                                                <telerik:RadLabel ID="lblDescription" runat="server" Text="Descrizione game" />
                                            </div>
                                            <div class="divTableCell" style="width: 530px;">
                                                <telerik:RadTextBox ID="txtDescription" runat="server" RenderMode="Lightweight" ClientIDMode="Static" Width="120px" ValidateRequestMode="Enabled" />
                                            </div>
                                        </div>

                                        <div class="divTableRow">
                                            <div class="divTableCell">
                                                <telerik:RadLabel ID="lblModello" runat="server" Text="Modello da usare"></telerik:RadLabel>
                                            </div>
                                            <div class="divTableCell">
                                                <telerik:RadComboBox ID="cboModello" runat="server" EmptyMessage="< Select model >"
                                                    RenderMode="Lightweight" RenderingMode="Full" ZIndex="1000000" ClientIDMode="Static" Width="120px" ValidateRequestMode="Enabled">
                                                </telerik:RadComboBox>
                                            </div>
                                        </div>

                                        <div class="divTableRow">
                                            <div class="divTableCell">
                                                <telerik:RadLabel ID="lblGame" runat="server" Text="Gioco da usare"></telerik:RadLabel>
                                            </div>
                                            <div class="divTableCell">
                                                <telerik:RadComboBox ID="cboGames" runat="server" EmptyMessage="< Select game >"
                                                    RenderMode="Lightweight" RenderingMode="Full" ZIndex="1000000" ClientIDMode="Static" Width="120px" ValidateRequestMode="Enabled">
                                                </telerik:RadComboBox>
                                            </div>
                                        </div>

                                        <div class="divTableRow">
                                            <div class="divTableCell">
                                                <telerik:RadLabel ID="lblDateStart" runat="server" Text="Data inizio"></telerik:RadLabel>
                                            </div>
                                            <div class="divTableCell">
                                                <telerik:RadTextBox ID="txtDateStart" EmptyMessage="" ToolTip="Start date game" runat="server" Width="90px" />
                                            </div>
                                        </div>

                                        <div class="divTableRow">
                                            <div class="divTableCell">
                                                <telerik:RadLabel ID="lblDateStop" runat="server" Text="Data fine"></telerik:RadLabel>
                                            </div>
                                            <div class="divTableCell">
                                                <telerik:RadTextBox ID="txtDateStop" EmptyMessage="" ToolTip="Finish date game" runat="server" Width="90px" />
                                            </div>
                                        </div>

                                        <div class="divTableRow">
                                            <div class="divTableCell">
                                                <telerik:RadLabel ID="lblDefaultPage" runat="server" Text="Default page"></telerik:RadLabel>
                                            </div>
                                            <div class="divTableCell">
                                                <telerik:RadTextBox ID="txtDefaultPage" runat="server" RenderMode="Lightweight" ClientIDMode="Static" Width="120px" />
                                            </div>
                                        </div>

                                        <div class="divTableRow">
                                            <div class="divTableCell">
                                                <telerik:RadLabel ID="lblNumeroPlayers" runat="server" Text="Num. players"></telerik:RadLabel>
                                            </div>
                                            <div class="divTableCell">
                                                <telerik:RadTextBox ID="txtNumPlayers" runat="server" RenderMode="Lightweight" ClientIDMode="Static" Width="120px" />
                                            </div>
                                        </div>

                                        <div class="divTableRow">
                                            <div class="divTableCell">
                                                <telerik:RadLabel ID="lblPrimoPeriodo" runat="server" Text="Genera solo per il primo periodo"></telerik:RadLabel>
                                            </div>
                                            <div class="divTableCell">
                                                <telerik:RadCheckBox ID="chkPrimoPeriodo" runat="server" Text="" AutoPostBack="true" RenderMode="Lightweight"></telerik:RadCheckBox>
                                            </div>
                                        </div>

                                        <div class="divTableRow">
                                            <div class="divTableCell">
                                                <telerik:RadLabel ID="lblFinoAlPeriodo" runat="server" Text="Genera fino a questo periodo"></telerik:RadLabel>
                                            </div>
                                            <div class="divTableCell">
                                                <telerik:RadCheckBox ID="chkPeriodoFinoAl" runat="server" Text="" AutoPostBack="true" RenderMode="Lightweight"></telerik:RadCheckBox>
                                            </div>
                                        </div>

                                        <div class="divTableRow">
                                            <div class="divTableCell">
                                                <telerik:RadLabel ID="lblPeriodo" runat="server" Text="Periodo"></telerik:RadLabel>
                                            </div>
                                            <div class="divTableCell">
                                                <telerik:RadComboBox ID="cboPeriodoFinoAl" runat="server" EmptyMessage="< Select period >" Enabled="false"
                                                    RenderMode="Lightweight" RenderingMode="Full" ZIndex="1000000" ClientIDMode="Static" Width="120px" ValidateRequestMode="Enabled">
                                                </telerik:RadComboBox>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="row">
                                    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="pnlGamesDetailUpdate" ClientIDMode="Static">
                                        <ProgressTemplate>
                                            <asp:Panel ID="Panel1" CssClass="overlay" runat="server">
                                                <asp:Panel ID="Panel2" CssClass="loader" runat="server">
                                                    <img alt="" src="Images/Loading.gif" />
                                                </asp:Panel>
                                            </asp:Panel>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                </div>

                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>

                    <div class="pnlfooter">
                        <telerik:RadButton ID="btnHide" runat="server" Text="Close" Width="80px"
                            RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                        </telerik:RadButton>

                        <telerik:RadButton ID="btnGenerate" runat="server" Text="Generate" Width="80px"
                            RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                        </telerik:RadButton>

                        <div style="text-align: center;">
                            <asp:Label runat="server" ID="txtMessageGame" Visible="false"></asp:Label>
                        </div>
                    </div>

                    <telerik:RadInputManager RenderMode="Lightweight" ID="RadInputManager1" runat="server">
                        <telerik:TextBoxSetting BehaviorID="TextBoxBehavior1" EmptyMessage="Type here" Validation-IsRequired="true">
                            <TargetControls>
                                <telerik:TargetInput ControlID="txtDescription"></telerik:TargetInput>
                            </TargetControls>
                        </telerik:TextBoxSetting>

                        <telerik:DateInputSetting BehaviorID="DateInputBehavior1" Validation-IsRequired="true" DateFormat="dd/MM/yyyy">
                            <TargetControls>
                                <telerik:TargetInput ControlID="txtDateStart"></telerik:TargetInput>
                            </TargetControls>
                        </telerik:DateInputSetting>

                        <telerik:DateInputSetting BehaviorID="DateInputBehavior2" Validation-IsRequired="true" DateFormat="dd/MM/yyyy">
                            <TargetControls>
                                <telerik:TargetInput ControlID="txtDateStop"></telerik:TargetInput>
                            </TargetControls>
                        </telerik:DateInputSetting>

                        <telerik:DateInputSetting BehaviorID="ComboInputBehavior1" Validation-IsRequired="true">
                            <TargetControls>
                                <telerik:TargetInput ControlID="ComboInputBehavior1"></telerik:TargetInput>
                            </TargetControls>
                        </telerik:DateInputSetting>

                        <telerik:DateInputSetting BehaviorID="ComboInputBehavior2" Validation-IsRequired="true">
                            <TargetControls>
                                <telerik:TargetInput ControlID="cboModello"></telerik:TargetInput>
                            </TargetControls>
                        </telerik:DateInputSetting>

                    </telerik:RadInputManager>

                </asp:Panel>

            </ContentTemplate>

        </asp:UpdatePanel>
    </div>

</asp:Content>
