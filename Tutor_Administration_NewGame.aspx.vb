﻿Imports Telerik.Web.UI
Imports System.Data
Imports APS
Imports APPCore.Utility
Imports DALC4NET

Partial Class Tutor_Administration_NewGame
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster

    Private Sub ClearPanel()
        txtDescription.Text = ""
        txtDateStart.Text = String.Format("{0:dd/MM/yyyy}", Now.Date.AddDays(1))
        txtDateStop.Text = String.Format("{0:dd/MM/yyyy}", Now.Date.AddDays(60))

        cboModello.SelectedIndex = -1
        cboGames.SelectedIndex = -1

        txtDefaultPage.Text = ""

        txtNumPlayers.Text = 0

        Session("GameSelected") = Nothing

        txtMessageGame.Visible = False
    End Sub

    Private Function HandleLoadDataGridGames() As DataTable
        Dim sSQL As String
        Dim oDT As DataTable

        sSQL = "SELECT G.Id, G.Descrizione, G.DataInizio, G.DataFine, G.DefaultPage, M.Descrizione AS Modello " _
             & "FROM BGOL_Games G " _
             & "INNER JOIN BGOL_Models M ON G.IdModel = M.Id "
        oDT = g_DAL.ExecuteDataTable(sSQL)

        Return oDT
    End Function

    Protected Sub grdGames_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs)
        grdGames.DataSource = HandleLoadDataGridGames()
    End Sub

    Private Sub Tutor_Administration_NewGame_Init(sender As Object, e As EventArgs) Handles Me.Init
        m_SessionData = Session("SessionData")
    End Sub

    Private Sub Tutor_Administration_NewGame(sender As Object, e As EventArgs) Handles Me.Load
        If IsNothing(m_SessionData) Then
            PageRedirect("ErrorTimeout.aspx", "", "")
        End If

        If Not Page.IsPostBack Then
            LoadDataComboModelli()
            LoadDataComboGames()
        End If

    End Sub

    Private Sub grdGames_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles grdGames.ItemCommand
        If e.CommandName.ToUpper <> "PAGE" AndAlso e.CommandName.ToUpper <> "SORT" AndAlso e.CommandName.ToUpper <> "FILTER" Then
            Session("GameSelected") = DirectCast(e.Item, GridDataItem)

            If e.CommandName.ToUpper = "MODIFY" Then
                Dim oPNL As UpdatePanel = TryCast(FindControlRecursive(Page, "pnlGamesDetailUpdate"), UpdatePanel)
                oPNL.Update()
                mpeMain.Show()

            ElseIf e.CommandName.ToUpper = "DELETE" Then
                Session("GameSelected") = DirectCast(e.Item, GridDataItem)
                Dim oItem As GridDataItem = DirectCast(Session("GameSelected"), GridDataItem)

                ' Cancello gli eventuali dati del vecchio game
                Dim oprmID As New DBParameter("@IDGameDelete", Nni(oItem.GetDataKeyValue("ID")), DbType.Int16)
                Dim oprmCollection As New DBParameterCollection()
                oprmCollection.Add(oprmID)
                g_DAL.ExecuteNonQuery("sp_Game_Delete", oprmCollection, CommandType.StoredProcedure)

                grdGames.DataSource = Nothing
                grdGames.Rebind()

            End If
        End If
    End Sub

    Private Sub pnlNewGame_PreRender(sender As Object, e As EventArgs) Handles pnlNewGame.PreRender
        If Not IsNothing(Session("GameSelected")) Then
            Dim oItem As GridDataItem = DirectCast(Session("GameSelected"), GridDataItem)
            Dim oDT As DataTable = LoadDataGame()

            ' Procedo con il caricamento dei dati da inserire nel panello
            ' Recupero l'id del modello
            If Not oItem Is Nothing Then
                Dim iID As Integer = oItem.GetDataKeyValue("ID")
                Dim oDRData As DataRow = oDT.Select("ID = " & iID).FirstOrDefault
                If Not oDRData Is Nothing Then
                    txtDescription.Text = Nz(oDRData("Descrizione"))
                    txtDateStart.Text = ANSIToDate(Nz(oDRData("DataInizio")))
                    txtDateStop.Text = ANSIToDate(Nz(oDRData("DataFine")))

                    cboModello.SelectedValue = Nni(oDRData("IDModel"))

                    txtDefaultPage.Text = Nz(oDRData("DefaultPage"))

                    cboGames.SelectedValue = Session("IDGame")
                End If
            End If
        End If
    End Sub

    Private Function LoadDataGame() As DataTable
        Dim sSQL As String
        Dim oDT As DataTable

        sSQL = "SELECT * FROM BGOL_Games "
        oDT = g_DAL.ExecuteDataTable(sSQL)

        Return oDT
    End Function

    Private Sub LoadDataComboModelli()
        Dim sSQL As String
        Dim oDTModelli As DataTable

        sSQL = "SELECT -1 AS ID, '' AS Modello UNION ALL SELECT ID, Descrizione FROM BGOL_Models ORDER BY Modello "
        oDTModelli = g_DAL.ExecuteDataTable(sSQL)

        cboModello.DataSource = oDTModelli
        cboModello.DataValueField = "ID"
        cboModello.DataTextField = "Modello"
        cboModello.DataBind()

        cboModello.SelectedValue = -1

    End Sub

    Private Sub LoadDataComboGames()
        Dim sSQL As String
        Dim oDTGames As DataTable

        sSQL = "SELECT -1 AS ID, '' AS Game UNION ALL SELECT ID, Descrizione FROM BGOL_Games ORDER BY Game "
        oDTGames = g_DAL.ExecuteDataTable(sSQL)

        cboGames.DataSource = oDTGames
        cboGames.DataValueField = "ID"
        cboGames.DataTextField = "Game"
        cboGames.DataBind()

        cboGames.SelectedValue = -1

    End Sub

    Private Sub btnNewGame_Click(sender As Object, e As EventArgs) Handles btnNewGame.Click
        ClearPanel()
        mpeMain.Show()
    End Sub

    Private Sub btnGenerate_Click(sender As Object, e As EventArgs) Handles btnGenerate.Click
        Dim sSQL As String
        Dim iIDGameNew As Integer
        Dim iIDGame As Integer
        Dim sTeamName As String
        Dim sPlayer As String
        Dim iIDPlayerMeU As Integer
        Dim iIDTeamMeU As Integer
        Dim iIDPeriodPrimo As Integer
        Dim iIDPeriodPrimoGameNew As Integer
        Dim dtItems As DataTable

        Dim iIDTeamMaster As Integer
        Dim iIDPlayerMaster As Integer
        Dim sTeamNameMaster As String

        Try
            txtMessageGame.Visible = False

            ' Setto le variabili per l'identificazione dei game, vecchio e poi nuovo
            iIDGame = Nni(cboGames.SelectedValue)

            ' Per il game di origine recupero l'ID del primo periodo se flaggato il chek
            If chkPrimoPeriodo.Checked Then
                sSQL = "SELECT MIN(ID) FROM BGOL_Periods WHERE IDGame = " & iIDGame
                iIDPeriodPrimo = Nni(g_DAL.ExecuteScalar(sSQL))

            ElseIf chkPeriodoFinoAl.Checked Then
                sSQL = "SELECT MIN(ID) FROM BGOL_Periods WHERE IDGame = " & iIDGame
                iIDPeriodPrimo = Nni(g_DAL.ExecuteScalar(sSQL))

            End If

            ' Recupero i dati di gioco di M&U
            sSQL = "SELECT ID FROM BGOL_Teams WHERE TeamName = 'M&U' "
            iIDTeamMeU = Nni(g_DAL.ExecuteScalar(sSQL))

            ' Recupero l'id del player per il gioco selezionato
            sSQL = "SELECT MIN(IDPlayer) FROM BGOL_Players_Teams_Games WHERE IDTeam = " & iIDTeamMeU & " AND IDGame = " & iIDGame
            iIDPlayerMeU = Nni(g_DAL.ExecuteScalar(sSQL))

            ' Recupero i dati del primo giocatore del game di origine
            ' Recupero i dati di del primo player, generalmente è quello usato come master
            sSQL = "SELECT MIN(BPTG.IDPLayer) AS IDPlayer , MIN(BPTG.IdTeam) AS IDTeam " _
                 & "FROM BGOL_Players_Teams_Games BPTG " _
                 & "INNER JOIN BGOL_Players_Games BTG ON BPTG.IdGame = BTG.IDGame " _
                 & "AND BPTG.IdPlayer = BTG.IDPlayer " _
                 & "AND BPTG.IdTeam = BTG.IDTeam " _
                 & "WHERE BPTG.IDGame = " & iIDGame & " AND BPTG.IDTeam <> " & iIDTeamMeU & " AND BTG.IDRole = 'P' "
            Dim dtData As DataTable = g_DAL.ExecuteDataTable(sSQL)
            For Each drData As DataRow In dtData.Rows
                iIDTeamMaster = Nni(drData("IDTeam"))
                iIDPlayerMaster = Nni(drData("IDPlayer"))
            Next
            sSQL = "SELECT TeamName FROM BGOL_Teams WHERE ID = " & iIDTeamMaster
            sTeamNameMaster = Nz(g_DAL.ExecuteScalar(sSQL))

            ' Procedo con i controlli
            If txtDescription.Text = "" Then
                txtMessageGame.Text = "Description game required"
                txtMessageGame.Visible = True
                Exit Try
            End If

            If Nni(cboModello.SelectedValue) = 0 Then
                txtMessageGame.Text = "Model game required"
                txtMessageGame.Visible = True
                Exit Try
            End If

            If Nni(cboGames.SelectedValue) = 0 Then
                txtMessageGame.Text = "Game required"
                txtMessageGame.Visible = True
                Exit Try
            End If

            If Nz(txtDateStart.Text) <> "" Then
                If Not IsDate(txtDateStart.Text) Then
                    txtMessageGame.Text = "Insert a valid date start"
                    txtMessageGame.Visible = True
                    Exit Try
                End If
            End If

            If Nz(txtDateStop.Text) <> "" Then
                If Not IsDate(txtDateStop.Text) Then
                    txtMessageGame.Text = "Insert a valid date finish"
                    txtMessageGame.Visible = True
                    Exit Try
                End If
            End If

            ' Controllo se devo generare anche fino al..
            If chkPeriodoFinoAl.Checked Then
                If Nni(cboPeriodoFinoAl.SelectedValue) = 0 Then
                    txtMessageGame.Text = "Select end period to generate"
                    txtMessageGame.Visible = True
                    Exit Try
                End If
            End If

            txtMessageGame.Text = "... wait ..."
            txtMessageGame.Visible = True
            pnlGamesDetailUpdate.Update()

            ' Prima di tutto procedo con la generazione del nuovo gioco
            Dim sDataInizio As String = ""
            Dim sDataFine As String = ""

            If txtDateStart.Text <> "" Then
                sDataInizio = DateToANSI(txtDateStart.Text)
            End If

            If txtDateStop.Text <> "" Then
                sDataFine = DateToANSI(txtDateStop.Text)
            End If

            ' Inserisco i dati del nuovo game

            ' Procedo all'eventuale cancellazione del dato creato sbagliato
            Dim iIDGameDelete As Integer
            sSQL = "SELECT ID FROM BGOL_Games WHERE Descrizione = '" & CStrSql(txtDescription.Text) & "' "
            iIDGameDelete = Nni(g_DAL.ExecuteScalar(sSQL))
            sSQL = "DELETE FROM BGOL_Games WHERE ID = " & iIDGameDelete
            g_DAL.ExecuteNonQuery(sSQL)

            sSQL = "INSERT INTO BGOL_Games (Descrizione, IDModel, DataInizio, DataFine, DefaultPage) VALUES (" _
                 & "'" & CStrSql(txtDescription.Text) & "', " & Nni(cboModello.SelectedValue) & ", '" & sDataInizio & "', '" & sDataFine & "', '" & CStrSql(txtDefaultPage.Text) & "') "
            g_DAL.ExecuteNonQuery(sSQL)

            ' Recupero l'ID del game appena creato
            sSQL = "SELECT MAX(ID) FROM BGOL_Games "
            iIDGameNew = g_DAL.ExecuteScalar(sSQL)

            ' Dal vecchio game recupero i periodi e li trasporto nel nuovo
            ' Cancello eventuali periodi inseriti in modo errato
            sSQL = "DELETE FROM BGOL_Periods WHERE IDGame = " & iIDGameDelete
            g_DAL.ExecuteNonQuery(sSQL)
            sSQL = "DELETE FROM BGOL_Games_Periods WHERE IDGame = " & iIDGameDelete
            g_DAL.ExecuteNonQuery(sSQL)

            ' Inserisco il nuovo periodo nella tabella per il gioco
            sSQL = "INSERT INTO BGOL_Periods (Descrizione, DateStart, DateEnd, NumStep, IDGame, IsVisible, DateHourEndEffective) " _
                 & "SELECT Descrizione, DateStart, DateEnd, NumStep, " & iIDGameNew & ", IsVisible, DateHourEndEffective FROM BGOL_Periods WHERE IDGame = " & iIDGame
            g_DAL.ExecuteNonQuery(sSQL)

            ' Recupero l'ID del nuovo periodo del game appena creato
            sSQL = "SELECT MIN(ID) FROM BGOL_Periods WHERE IDGame = " & iIDGameNew
            iIDPeriodPrimoGameNew = Nni(g_DAL.ExecuteScalar(sSQL))

            ' Genero i teams da associare ai player generici
            If Nni(txtNumPlayers.Text) > 0 Then
                sSQL = "DELETE FROM BGOL_Players_Teams_Games WHERE IDGame = " & iIDGameDelete
                g_DAL.ExecuteNonQuery(sSQL)

                sSQL = "DELETE FROM BGOL_Players_Games WHERE IDGame = " & iIDGameDelete
                g_DAL.ExecuteNonQuery(sSQL)

                For iPlayer As Integer = 1 To txtNumPlayers.Text
                    ' Genero il nome del team e del player
                    sTeamName = "TeamName_" & CStrSql(txtDescription.Text).Replace(" ", "") & "_" & iPlayer
                    sPlayer = "Player_" & iPlayer & "@" & CStrSql(txtDescription.Text).Replace(" ", "") & ".com"

                    sSQL = "DELETE FROM BGOL_Teams WHERE TeamName = '" & CStrSql(sTeamName) & "' "
                    g_DAL.ExecuteNonQuery(sSQL)

                    sSQL = "DELETE FROM BGOL_Players WHERE Email = '" & CStrSql(sPlayer) & "' "
                    g_DAL.ExecuteNonQuery(sSQL)

                    ' TEAM
                    sSQL = "INSERT INTO BGOL_Teams (TeamName) VALUES (" _
                         & "'" & CStrSql(sTeamName) & "') "
                    g_DAL.ExecuteNonQuery(sSQL)
                    ' Recupero l'ID del team
                    sSQL = "SELECT MAX(ID) FROM BGOL_Teams "
                    Dim iIDTeam As Integer = Nni(g_DAL.ExecuteScalar(sSQL))

                    ' PLAYER
                    sSQL = "INSERT INTO BGOL_Players (Nome, Cognome, Username, Email, Attivo, SuperUser, DefaultLanguage) VALUES (" _
                        & "'Player_" & iPlayer & "', 'Player_" & iPlayer & "', 'Player_" & iPlayer & "', '" & CStrSql(sPlayer) & "', 1, 0, 'ITA') "
                    g_DAL.ExecuteNonQuery(sSQL)
                    ' Recupero l'ID del player
                    sSQL = "SELECT MAX(ID) FROM BGOL_Players "
                    Dim iIDPlayer As Integer = Nni(g_DAL.ExecuteScalar(sSQL))

                    ' Inserisco la coppia Team/Player nella tabella che li associa al game appena creato
                    ' Procedo con l'inserimento del player/team nel database

                    sSQL = "INSERT INTO BGOL_Players_Teams_Games (IDPlayer, IDTeam, IDGame) VALUES (" _
                         & iIDPlayer & ", " & iIDTeam & ", " & iIDGameNew & ") "
                    g_DAL.ExecuteNonQuery(sSQL)

                    sSQL = "INSERT INTO BGOL_Players_Games (IDPlayer, IDGame, IDTeam, IDRole, SettingVariables) VALUES (" _
                         & iIDPlayer & ", " & iIDGameNew & ", " & iIDTeam & ", 'P', 1) "
                    g_DAL.ExecuteNonQuery(sSQL)
                Next
            End If

            ' Aggiungo anche M&U alla tabella dei players/games
            sSQL = "INSERT INTO BGOL_Players_Teams_Games (IDPlayer, IDTeam, IDGame) VALUES (" _
                 & iIDPlayerMeU & ", " & iIDTeamMeU & ", " & iIDGameNew & ") "
            g_DAL.ExecuteNonQuery(sSQL)

            ' Sistemo il player nella tabella BGOL_Players_Games
            sSQL = "INSERT INTO BGOL_Players_Games (IDPlayer, IDGame, IDTeam, IDRole, SettingVariables) VALUES (" _
                 & iIDPlayerMeU & ", " & iIDGameNew & ", " & iIDTeamMeU & ", 'D', 1) "
            g_DAL.ExecuteNonQuery(sSQL)

            ' Gestione dei menu
            LoadMenuItem(iIDGame, iIDGameNew)

            ' Sistemazione delle traduzioni dei menu
            LoadMenuTranslation(iIDGame, iIDGameNew)

            ' Decisioni del BOSS
            LoadDataDecisionsBoss(iIDGame, iIDGameNew, iIDPeriodPrimo, iIDPeriodPrimoGameNew, iIDGameDelete, iIDTeamMaster)

            ' Decisioni Developer
            LoadDataDecisionsDeveloper(iIDGame, iIDGameNew, iIDPeriodPrimo, iIDPeriodPrimoGameNew, iIDGameDelete, iIDTeamMaster)

            ' Variabili di stato
            LoadDataVariablesState(iIDGame, iIDGameNew, iIDPeriodPrimo, iIDPeriodPrimoGameNew, iIDGameDelete, iIDTeamMaster)

            ' Decisioni del player
            LoadDataDecisionsPlayer(iIDGame, iIDGameNew, iIDPeriodPrimo, iIDPeriodPrimoGameNew, iIDGameDelete, iIDTeamMaster)

            ' Aggiorno la tabella delle variabili del gioco
            LoadDataVariables(iIDGame, iIDGameNew, iIDGameDelete)

            ' Aggiorno la tabella delle variabili da calcolare
            LoadDataVariablesCalculateDefine(iIDGame, iIDGameNew, iIDTeamMeU, iIDPeriodPrimoGameNew, iIDPeriodPrimo)

            ' Inserisco il periodo 1 nella tabella BGOL_Games_Periods
            sSQL = "DELETE FROM BGOL_Games_Periods WHERE IDGame = " & iIDGameDelete
            g_DAL.ExecuteNonQuery(sSQL)
            sSQL = "INSERT INTO BGOL_Games_Periods (IDGame, IDPeriodo) VALUES (" & iIDGameNew & ", " & iIDPeriodPrimoGameNew & ") "
            g_DAL.ExecuteNonQuery(sSQL)

            If chkPeriodoFinoAl.Checked Then
                Dim iIDPeriodoGameNew As Integer

                ' Recupero tutti i periodi escludendo il primo che ho già creato
                sSQL = "SELECT * FROM BGOL_Periods WHERE IDGame = " & iIDGame & " AND ID > " & iIDPeriodPrimo & " AND ID <= " & Nni(cboPeriodoFinoAl.SelectedValue)
                Dim oDTPeriods As DataTable = g_DAL.ExecuteDataTable(sSQL)

                For Each drPeriod As DataRow In oDTPeriods.Rows
                    ' Recupero il periodo uguale a quello del game di origine, baso la ricerca sul numero di step
                    sSQL = "SELECT ID FROM BGOL_Periods WHERE NumStep = " & Nni(drPeriod("NumStep")) & " AND IDGame = " & iIDGameNew
                    iIDPeriodoGameNew = Nni(g_DAL.ExecuteScalar(sSQL))

                    ' Decisioni del BOSS
                    LoadDataDecisionsBoss(iIDGame, iIDGameNew, drPeriod("ID"), iIDPeriodoGameNew, iIDTeamMeU, "")

                    ' Decisioni Developer
                    LoadDataDecisionsDeveloper(iIDGame, iIDGameNew, drPeriod("ID"), iIDPeriodoGameNew, iIDTeamMeU, "")

                    ' Variabili di stato
                    LoadDataVariablesState(iIDGame, iIDGameNew, drPeriod("ID"), iIDPeriodoGameNew, iIDTeamMeU, "")

                    ' Decisioni del player
                    LoadDataDecisionsPlayer(iIDGame, iIDGameNew, drPeriod("ID"), iIDPeriodoGameNew, iIDTeamMeU, "")

                    ' Aggiorno la tabella delle variabili calcolate
                    LoadDataVariableCalculateDefine(iIDGame, iIDGameNew, drPeriod("ID"), iIDPeriodoGameNew, iIDTeamMeU, "")

                    ' Inserisco il nuovo periodo nella tabella dei periodi di gioco
                    sSQL = "INSERT INTO BGOL_Games_Periods (IDGame, IDPeriodo) VALUES (" & iIDGameNew & ", " & iIDPeriodoGameNew & ") "
                    g_DAL.ExecuteNonQuery(sSQL)
                Next

                ' Cancello le variabili calcolate dell'ultimo periodo generato
                sSQL = "DELETE FROM Variables_Calculate_Value WHERE IDPeriod = " & iIDPeriodoGameNew
                g_DAL.ExecuteNonQuery(sSQL)

            End If

            ' Sistemo gli AGE
            ' Cancello eventuali age del vecchio game generato
            sSQL = "DELETE FROM Age WHERE IDGame = " & iIDGameDelete
            g_DAL.ExecuteNonQuery(sSQL)

            sSQL = "INSERT INTO Age (ID, IDGame, AgeDescription, AgeLabel) " _
                 & "SELECT ID, " & iIDGameNew & ", AgeDescription, AgeLabel FROM Age WHERE IDGame = " & iIDGame
            g_DAL.ExecuteNonQuery(sSQL)

            ' Sistemazione degli ITEMS
            LoadDataItems(iIDGame, iIDGameNew, iIDGameDelete, iIDPeriodPrimoGameNew)

            ' Caricamento della tabella Variables_Group
            LoadVariablesGroup(iIDGame, iIDGameNew)

            ' Sistemazione Decisons_BossTitle e SubTitle
            LoadDecisionsBossTitle_SubTitle(iIDGame, iIDGameNew, iIDGameDelete)

            ' Sistemazione degli items nel caso abbia scelto la generazione di più periodi
            ' Carico gli tems del nuovo gioco
            If chkPeriodoFinoAl.Checked Then
                Dim dtItemsOld As DataTable = LoadItemsGame(iIDGame, "")
                Dim drItemOld As DataRow

                dtItems = LoadItemsGame(iIDGameNew, "")
                For Each drItem As DataRow In dtItems.Rows
                    ' Recupero l'id dell'items da sostituire, quello preso dal gioco di origine della copia
                    drItemOld = dtItemsOld.Select("VariableName = '" & Nz(drItem("VariableName")) & "'").FirstOrDefault

                    ' Allineo i dati relativi agli items
                    ' VARIABLES_STATE_VALUE_LIST
                    sSQL = "UPDATE VSVL SET IDItem = " & Nni(drItem("IDVariable")) _
                         & "FROM Variables_State VS " _
                         & "INNER JOIN Variables_State_Value VSV ON VS.ID = VSV.IDVariable " _
                         & "INNER JOIN Variables_State_Value_List VSVL ON VSV.Id = VSVL.IDVariableState " _
                         & "WHERE IDGame = " & iIDGameNew & " AND VSVL.IDItem = " & Nni(drItemOld("IDVariable"))
                    g_DAL.ExecuteNonQuery(sSQL)

                    ' VARIABLES_STATE_VALUE
                    sSQL = "UPDATE VSV SET IDItem = " & Nni(drItem("IDVariable")) _
                         & "FROM Variables_State VS " _
                         & "INNER JOIN Variables_State_Value VSV ON VS.ID = VSV.IDVariable " _
                         & "WHERE IDGame = " & iIDGameNew & " AND VSV.IDItem = " & Nni(drItemOld("IDVariable"))
                    g_DAL.ExecuteNonQuery(sSQL)

                    ' DECISIONS_DEVELOPER_VALUE
                    sSQL = "UPDATE DBV SET IDItem = " & Nni(drItem("IDVariable")) _
                         & "FROM Decisions_Developer D " _
                         & "INNER JOIN Decisions_Developer_Value DBV ON D.Id = DBV.IDDecisionDeveloper " _
                         & "WHERE IDGame = " & iIDGameNew & " AND DBV.IDItem = " & Nni(drItemOld("IDVariable"))
                    g_DAL.ExecuteNonQuery(sSQL)

                    ' DECISIONS_PLAYERS_VALUE_LIST
                    sSQL = "UPDATE DPVL SET IDItem = " & Nni(drItem("IDVariable")) _
                         & "FROM Decisions_Players D " _
                         & "INNER JOIN Decisions_Players_Value DPV ON D.Id = DPV.IDDecisionPlayer " _
                         & "INNER JOIN Decisions_Players_Value_List DPVL ON DPV.ID = DPVL.IDDecisionValue " _
                         & "WHERE IDGame = " & iIDGameNew & " AND DPVL.IDItem = " & Nni(drItemOld("IDVariable"))
                    g_DAL.ExecuteNonQuery(sSQL)

                    ' VARIABLES_CALCULATE_VALUE
                    sSQL = "UPDATE C SET IDItem = " & Nni(drItem("IDVariable")) _
                         & "FROM Variables_Calculate_Define V " _
                         & "INNER JOIN Variables_Calculate_Value C ON V.Id = C.IDVariable " _
                         & "WHERE IDGame = " & iIDGameNew & " AND C.IDItem = " & Nni(drItemOld("IDVariable"))
                    g_DAL.ExecuteNonQuery(sSQL)
                Next
            End If

            txtMessageGame.Text = "Generation complete"
            txtMessageGame.Visible = True

            pnlMain.Update()
            mpeMain.Hide()

            UpdateProgress1.Visible = False
            grdGames.Rebind()
            pnlMain.Update()

        Catch ex As Exception
            txtMessageGame.Text = "Error btnGenerate_Click -> " & ex.Message
            txtMessageGame.Visible = True
        End Try
        mpeMain.Show()
    End Sub

    Private Sub LoadDataVariablesCalculateDefine(IDGameOrigin As Integer, IDGameDestination As Integer, IDTeamMeU As Integer, IDPeriodoGameNew As Integer, IDPeriodGameOrigin As Integer)
        Dim sSQL As String
        Dim iIDVariableCalculateDefine As Integer
        Dim iIDTeamMaster As Integer
        Dim iIDPlayerMaster As Integer
        Dim sTeamNameMaster As String
        Dim sTeamName As String
        Dim iIDTeamNew As Integer

        Try
            ' Recupero i dati di del primo player, generalmente è quello usato come master
            sSQL = "SELECT MIN(IDPLayer) AS IDPlayer, MIN(IDTeam) AS IDTeam FROM BGOL_Players_Teams_Games WHERE IDGame = " & IDGameOrigin & " AND IDTeam <> " & IDTeamMeU
            Dim dtData As DataTable = g_DAL.ExecuteDataTable(sSQL)
            For Each drData As DataRow In dtData.Rows
                iIDTeamMaster = Nni(drData("IDTeam"))
                iIDPlayerMaster = Nni(drData("IDPlayer"))
            Next
            sSQL = "SELECT TeamName FROM BGOL_Teams WHERE ID = " & iIDTeamMaster
            sTeamNameMaster = Nz(g_DAL.ExecuteScalar(sSQL))

            sSQL = "INSERT INTO Variables_Calculate_Define (VariableName, IDGame, IDCategory, VariableLabel) " _
                 & "SELECT VariableName, " & IDGameDestination & ", IDCategory, VariableLabel " _
                 & "FROM Variables_Calculate_Define V " _
                 & "WHERE IDGame = " & IDGameOrigin
            g_DAL.ExecuteNonQuery(sSQL)

            ' Inserisco le variabili calcolate del primo periodo
            sSQL = "SELECT * FROM Variables_Calculate_Define WHERE IDGame = " & IDGameOrigin
            Dim dtVariableCalculateDefine As DataTable = g_DAL.ExecuteDataTable(sSQL)
            dtVariableCalculateDefine.PrimaryKey = New DataColumn() {dtVariableCalculateDefine.Columns("ID"), dtVariableCalculateDefine.Columns("VariableName"),
                                                                     dtVariableCalculateDefine.Columns("IDGame")}

            For Each drVariableCalculateDefine As DataRow In dtVariableCalculateDefine.Rows
                ' Recupero l'ID della variabile nel gioco nuovo
                sSQL = "SELECT ID FROM Variables_Calculate_Define WHERE IDGame = " & IDGameDestination & " AND VariableName = '" & Nz(drVariableCalculateDefine("VariableName")) & "' "
                iIDVariableCalculateDefine = Nni(g_DAL.ExecuteScalar(sSQL))

                sSQL = "SELECT MIN(ID) AS ID, IDVariable, IDPlayer, IDPeriod, IDItem, IDAge, Value FROM Variables_Calculate_Value WHERE IDVariable = " & drVariableCalculateDefine("ID") _
                     & " AND ISNULL(IDPeriod, " & IDPeriodGameOrigin & ") = " & IDPeriodGameOrigin & " AND (ISNULL(IDPlayer, " & iIDTeamMaster & ") = " & iIDTeamMaster _
                     & " OR ISNULL(IDPlayer, 0) = 0) " _
                     & "GROUP BY IDVariable, IDPlayer, IDPeriod, IDItem, IDAge, Value "
                Dim dtVariables_Calculate_Value As DataTable = g_DAL.ExecuteDataTable(sSQL)
                dtVariables_Calculate_Value.PrimaryKey = New DataColumn() {dtVariables_Calculate_Value.Columns("IDVariable"),
                                                                           dtVariables_Calculate_Value.Columns("IDPlayer"), dtVariables_Calculate_Value.Columns("IDPeriod"),
                                                                           dtVariables_Calculate_Value.Columns("IDItem"), dtVariables_Calculate_Value.Columns("IDAge")}

                For Each drVariables_Calculate_Value As DataRow In dtVariables_Calculate_Value.Rows
                    ' COntrollo che sia una variabile con il player valorizzato
                    If Nni(drVariables_Calculate_Value("IDPlayer")) > 0 Then
                        ' Inserisco i valori necessari per tutti i player del nuovo game
                        If Nni(drVariables_Calculate_Value("IDPlayer")) = iIDTeamMaster Then
                            For iPlayer As Integer = 1 To txtNumPlayers.Text
                                sTeamName = "TeamName_" & CStrSql(txtDescription.Text).Replace(" ", "") & "_" & iPlayer
                                sSQL = "Select ID FROM BGOL_Teams WHERE TeamName = '" & CStrSql(sTeamName) & "' "
                                iIDTeamNew = Nni(g_DAL.ExecuteScalar(sSQL))
                                ' Inserisco la variabile nella tabella value
                                sSQL = "INSERT INTO Variables_Calculate_Value (IDVariable, IDPeriod, IDPlayer, Value, IDItem, IDAge) " _
                                     & "SELECT " & iIDVariableCalculateDefine & ", " & IDPeriodoGameNew & ", " & iIDTeamNew & ", Value, IDItem, IDAge " _
                                     & "FROM Variables_Calculate_Value " _
                                     & "WHERE ID = " & Nni(drVariables_Calculate_Value("Id"))
                                g_DAL.ExecuteNonQuery(sSQL)
                            Next
                        End If
                    Else
                        ' Non ho player associati, procedo con il normale inserimento della riga calcolata
                        sSQL = "INSERT INTO Variables_Calculate_Value (IDVariable, IDPeriod, IDPlayer, Value, IDItem, IDAge) " _
                             & "SELECT " & iIDVariableCalculateDefine & ", " & IDPeriodoGameNew & ", IDPlayer, Value, IDItem, IDAge " _
                             & "FROM Variables_Calculate_Value " _
                             & "WHERE ID = " & Nni(drVariables_Calculate_Value("Id"))
                        g_DAL.ExecuteNonQuery(sSQL)
                    End If
                Next

            Next
        Catch ex As Exception
            txtMessageGame.Text = "Error LoadDataVariablesCalculateDefine -> " & ex.Message
            txtMessageGame.Visible = True
        End Try
    End Sub

    Private Sub LoadDecisionsBossTitle_SubTitle(IDGame As Integer, IDGameNew As Integer, IDGameDelete As Integer)
        Dim sSQL As String
        Dim oDTTitoli As DataTable
        Dim oDTSottoTitoli As DataTable
        Dim iIDTitolo As Integer
        Dim iIDTitoloNew As Integer

        Try
            ' Cancello i vecchi dati generati
            sSQL = "DELETE FROM Decisions_Boss_SubTitle WHERE IDTitle IN (SELECT ID FROM Decisions_Boss_Title WHERE IDGame = " & IDGameDelete & ") "
            g_DAL.ExecuteNonQuery(sSQL)

            sSQL = "DELETE FROM Decisions_Boss_Title WHERE IDGame = " & IDGameDelete
            g_DAL.ExecuteNonQuery(sSQL)

            ' Recupero i titoli del game da cui genero
            sSQL = "SELECT ID, Title, NumOrder FROM Decisions_Boss_Title WHERE IDGame = " & IDGame
            oDTTitoli = g_DAL.ExecuteDataTable(sSQL)

            ' Ciclo si titoli e inizio ad inserire titoli e sotto titoli
            For Each drTitolo As DataRow In oDTTitoli.Rows
                iIDTitolo = drTitolo("ID")
                ' Inserisco il titolo nel nuovo game
                sSQL = "INSERT INTO Decisions_Boss_Title (IDGame, Title, NumOrder) VALUES (" _
                     & IDGameNew & ", " & "'" & drTitolo("Title") & "', " & drTitolo("NumOrder") & ") "
                g_DAL.ExecuteNonQuery(sSQL)

                ' REcupero l'ID del titolo appena inserito
                sSQL = "SELECT MAX(ID) FROM Decisions_Boss_Title WHERE IDGame = " & IDGameNew
                iIDTitoloNew = g_DAL.ExecuteScalar(sSQL)

                ' Carico la tabella dei sotto titoli presenti e procedo con il caricamento della nuova e 
                ' l'aggiornamento della tabella delle decisioni del boss
                sSQL = "SELECT * FROM Decisions_Boss_SubTitle WHERE IDTitle = " & iIDTitolo
                oDTSottoTitoli = g_DAL.ExecuteDataTable(sSQL)

                For Each drSottoTitolo As DataRow In oDTSottoTitoli.Rows
                    ' Inserisco il nuovo sottotitolo, recupero l'ID e aggiorno le decisioni del boss
                    sSQL = "INSERT INTO Decisions_Boss_SubTitle (IDTitle, SubTitle) " _
                         & "SELECT " & iIDTitoloNew & ", SubTitle FROM Decisions_Boss_SubTitle WHERE ID = " & drSottoTitolo("ID")
                    g_DAL.ExecuteNonQuery(sSQL)

                    ' REcupero l'Id appena creato
                    sSQL = "SELECT MAX(ID) FROM Decisions_Boss_SubTitle "
                    Dim iIDSottoTitoloNew As Integer = g_DAL.ExecuteScalar(sSQL)

                    ' Aggiorno i dati della tabella Decisions_Boss con i nuovi sottotitoli
                    sSQL = "UPDATE Decisions_Boss " _
                         & "SET IDSubTitle = " & iIDSottoTitoloNew _
                         & " WHERE ID IN (SELECT D.ID FROM Decisions_Boss D " _
                         & "WHERE IDGame = " & IDGameNew & " AND IDSubTitle = " & drSottoTitolo("ID") & ") "
                    g_DAL.ExecuteNonQuery(sSQL)
                Next
            Next
        Catch ex As Exception
            txtMessageGame.Text = "Error LoadDecisionsBossTitle_SubTitle -> " & ex.Message
            txtMessageGame.Visible = True
        End Try
    End Sub

    Private Sub LoadDataDecisionsBoss(IDGame As Integer, IDGameNew As Integer, IDPeriodoPrimo As Integer, IDPeriodoPrimoGameNew As Integer, IDGameDelete As Integer, IDTeamMaster As Integer)
        Dim sSQL As String = ""
        Dim sTeamName As String
        Dim oDTTeams As DataTable = APSUtility.LoadTeamsGame(IDGame)
        Dim iIDPlayerMeU As Integer

        Try
            ' Cancello gli eventuali dati del vecchio game
            Dim oprmID As New DBParameter("@IDGame", IDGameDelete, DbType.Int16)
            Dim oprmCollection As New DBParameterCollection()
            oprmCollection.Add(oprmID)
            g_DAL.ExecuteNonQuery("sp_Boss_Decisions_Delete", oprmCollection, CommandType.StoredProcedure)

            ' Recupero l'id del player per il gioco selezionato
            sSQL = "SELECT MIN(IDPlayer) FROM BGOL_Players_Teams_Games WHERE IDTeam = " & IDTeamMaster & " AND IDGame = " & IDGame
            iIDPlayerMeU = Nni(g_DAL.ExecuteScalar(sSQL))

            ' Procedo con la generazione dei titoli ed eventuali sottotitoli
            sSQL = "SELECT * FROM Decisions_Boss_Title WHERE IDGame = " & IDGame
            Dim oDTTitle As DataTable = g_DAL.ExecuteDataTable(sSQL)

            ' Avvio l'inserimento delle decisioni del boss
            ' Recupero tutte le decisioni del game originale
            For Each drTitle As DataRow In oDTTitle.Rows
                ' Con il titolo in oggetto recupero i relativi sottotitoli
                sSQL = "SELECT * FROM Decisions_Boss_SubTitle WHERE IDTitle = " & Nni(drTitle("ID"))
                Dim oDTSubTitle As DataTable = g_DAL.ExecuteDataTable(sSQL)

                For Each drSubTitle As DataRow In oDTSubTitle.Rows
                    sSQL = "SELECT D.Id, D.IDDataType, D.Decision, D.DecisionLabel, D.IDGame, D.IDSubTitle FROM Decisions_Boss D WHERE IDGame = " & IDGame & " AND IDSubTitle = " & Nni(drSubTitle("ID"))
                    Dim oDTDecisionBoss As DataTable = g_DAL.ExecuteDataTable(sSQL)
                    oDTDecisionBoss.PrimaryKey = New DataColumn() {oDTDecisionBoss.Columns("ID"), oDTDecisionBoss.Columns("Decision"), oDTDecisionBoss.Columns("IDGame")}

                    For Each drDecisionsBoss As DataRow In oDTDecisionBoss.Rows

                        sSQL = "INSERT INTO Decisions_Boss (IDDataType, Decision, DecisionLabel, IDGame, IDSubTitle) " _
                             & "SELECT D.IDDataType, D.Decision, D.DecisionLabel, " & IDGameNew & ", D.IDSubTitle FROM Decisions_Boss D WHERE ID = " & Nni(drDecisionsBoss("ID"))
                        g_DAL.ExecuteNonQuery(sSQL)

                        ' Recupero l'ID dell'ultima decisione inserita
                        Dim iIDDecisionBoss As Integer
                        sSQL = "SELECT MAX(ID) FROM Decisions_Boss WHERE IDGame = " & IDGameNew
                        iIDDecisionBoss = g_DAL.ExecuteScalar(sSQL)

                        ' Recupero i dati della variabile originale da per inserirli nel nuovo game
                        sSQL = "SELECT DBV.Id, DBV.IDDecisionBoss, DBV.IDPeriod, DBV.Simulation, DBV.Value FROM Decisions_Boss_Value DBV WHERE DBV.IDDecisionBoss = " & Nni(drDecisionsBoss("ID"))
                        ' Controllo che sia stato selezionato il primo periodo e basta
                        sSQL &= " AND ISNULL(DBV.IDPeriod, " & IDPeriodoPrimo & ") = " & IDPeriodoPrimo

                        Dim oDTDecisionBossValue As DataTable = g_DAL.ExecuteDataTable(sSQL)
                        oDTDecisionBossValue.PrimaryKey = New DataColumn() {oDTDecisionBossValue.Columns("IDDecisionBoss"),
                                                                            oDTDecisionBossValue.Columns("ID")}

                        For Each drDecisionBossValue As DataRow In oDTDecisionBossValue.Rows
                            sSQL = "INSERT INTO Decisions_Boss_Value (IDDecisionBoss, IDPeriod, Simulation, Value) " _
                                 & "SELECT " & iIDDecisionBoss & ", " & IDPeriodoPrimoGameNew & ", Simulation, Value FROM Decisions_Boss_Value WHERE IDDecisionBoss = " & Nni(drDecisionsBoss("ID")) _
                                 & " AND ISNULL(IDPeriod, " & IDPeriodoPrimo & ") = " & IDPeriodoPrimo
                            g_DAL.ExecuteNonQuery(sSQL)

                            ' Recupero l'ID dell'ultima decisione inserita
                            Dim iIDDecisionBossValue As Integer
                            sSQL = "SELECT MAX(ID) FROM Decisions_Boss_Value "
                            iIDDecisionBossValue = Nni(g_DAL.ExecuteScalar(sSQL))

                            ' Controllo che sia una lista di valori, se si recupero i dati direttamente dalla tabella Decisions_Boss_Value_List
                            If Nz(drDecisionBossValue("Value")).ToUpper.Contains("LIST OF") Then
                                ' Recupero tutti i dati della variabile da salvare
                                sSQL = "SELECT DBVL.Id, DBVL.IdDecisionBossValue, DBVL.IDPeriod, DBVL.Simulation, DBVL.Value, DBVL.VariableName " _
                                     & "FROM Decisions_Boss_Value_List DBVL WHERE DBVL.IdDecisionBossValue = " & Nni(drDecisionBossValue("Id")) _
                                     & "AND ISNULL(IDPeriod, " & IDPeriodoPrimo & ") = " & IDPeriodoPrimo
                                Dim oDTDecisionsBossValueList As DataTable = g_DAL.ExecuteDataTable(sSQL)
                                oDTDecisionsBossValueList.PrimaryKey = New DataColumn() {oDTDecisionsBossValueList.Columns("IdDecisionBossValue"),
                                                                                         oDTDecisionsBossValueList.Columns("VariableName"),
                                                                                         oDTDecisionsBossValueList.Columns("Id")}

                                ' Ciclo sulle righe e procedo all'inserimento
                                ' controllo se contiene valori di tipo player, inizia per var_, sono casi anomali, ma possibili
                                For Each drDecisionBossValueList As DataRow In oDTDecisionsBossValueList.Rows
                                    If Nz(drDecisionBossValueList("VariableName")).ToUpper.Contains("VAR_") Then
                                        If Nz(drDecisionBossValueList("VariableName")).ToUpper.Contains("M&U") Then
                                            ' Prendo solo quelli di M&U, dovrebbero essere quelli con i dati corretti
                                            For iPlayer As Integer = 1 To txtNumPlayers.Text
                                                ' Genero il nome del team e del player
                                                sTeamName = "TeamName_" & CStrSql(txtDescription.Text).Replace(" ", "") & "_" & iPlayer
                                                sSQL = "INSERT INTO Decisions_Boss_Value_List (IdDecisionBossValue, IDPeriod, Simulation, Value, VariableName) " _
                                                     & "SELECT " & iIDDecisionBossValue & ", " & IDPeriodoPrimoGameNew & " , Simulation, Value, REPLACE(VariableName, 'M&U', '" & CStrSql(sTeamName) & "') " _
                                                     & "FROM Decisions_Boss_Value_List DBVL " _
                                                     & "WHERE Id = " & Nni(drDecisionBossValueList("Id")) & " AND VariableName LIKE 'var_M&U%' AND IDPeriod = " & IDPeriodoPrimo
                                                g_DAL.ExecuteNonQuery(sSQL)
                                            Next
                                            ' Inserisco anche i dati di M&U
                                            sSQL = "INSERT INTO Decisions_Boss_Value_List (IdDecisionBossValue, IDPeriod, Simulation, Value, VariableName) " _
                                                 & "SELECT " & iIDDecisionBossValue & ", " & IDPeriodoPrimoGameNew & ", Simulation, Value, VariableName " _
                                                 & "FROM Decisions_Boss_Value_List DBVL " _
                                                 & "WHERE Id = " & Nni(drDecisionBossValueList("Id")) & " AND VariableName LIKE 'var_M&U%' AND IDPeriod = " & IDPeriodoPrimo
                                            g_DAL.ExecuteNonQuery(sSQL)
                                        End If

                                    Else
                                        If Nz(drDecisionBossValueList("VariableName")).ToUpper.Contains("M&U") Then
                                            For iPlayer As Integer = 1 To txtNumPlayers.Text
                                                sTeamName = "TeamName_" & CStrSql(txtDescription.Text).Replace(" ", "") & "_" & iPlayer
                                                sSQL = "INSERT INTO Decisions_Boss_Value_List (IdDecisionBossValue, IDPeriod, Simulation, Value, VariableName) " _
                                                     & "SELECT " & iIDDecisionBossValue & ", " & IDPeriodoPrimoGameNew & ", Simulation, Value, REPLACE(VariableName, 'M&U', '" & CStrSql(sTeamName) & "') " _
                                                     & "FROM Decisions_Boss_Value_List DBVL " _
                                                     & "WHERE Id = " & Nni(drDecisionBossValueList("Id")) & " AND ISNULL(IDPeriod, " & IDPeriodoPrimo & ") = " & IDPeriodoPrimo
                                                g_DAL.ExecuteNonQuery(sSQL)
                                            Next
                                            ' Inserisco anche i dati di M&U
                                            sSQL = "INSERT INTO Decisions_Boss_Value_List (IdDecisionBossValue, IDPeriod, Simulation, Value, VariableName) " _
                                                 & "SELECT " & iIDDecisionBossValue & ", " & IDPeriodoPrimoGameNew & ", Simulation, Value, VariableName " _
                                                 & "FROM Decisions_Boss_Value_List DBVL " _
                                                 & "WHERE Id = " & Nni(drDecisionBossValueList("Id")) & " AND ISNULL(IDPeriod, " & IDPeriodoPrimo & ") = " & IDPeriodoPrimo
                                            g_DAL.ExecuteNonQuery(sSQL)
                                        Else

                                            ' Controllo che non contenga niente di diverso da M&U, nel caso siano dei player
                                            Dim bProcediUpdate As Boolean = True
                                            ' Controllo che non ci siano casi strani per cui il nome del player compaia a qualche parte nella variabile
                                            For Each drTeam As DataRow In oDTTeams.Rows
                                                If Nz(drDecisionBossValueList("VariableName")).ToUpper.Contains(Nz(drTeam("TeamName")).ToUpper) Then
                                                    bProcediUpdate = False
                                                    Exit For
                                                End If
                                            Next

                                            If bProcediUpdate Then
                                                sSQL = "INSERT INTO Decisions_Boss_Value_List (IdDecisionBossValue, IDPeriod, Simulation, Value, VariableName) " _
                                                     & "SELECT " & iIDDecisionBossValue & ", " & IDPeriodoPrimoGameNew & ", Simulation, Value, VariableName " _
                                                     & "FROM Decisions_Boss_Value_List DBVL " _
                                                     & "WHERE Id = " & Nni(drDecisionBossValueList("Id")) & " AND ISNULL(IDPeriod, " & IDPeriodoPrimo & ") = " & IDPeriodoPrimo
                                                g_DAL.ExecuteNonQuery(sSQL)
                                            End If

                                        End If
                                    End If
                                Next
                            End If
                        Next
                    Next
                Next

            Next

            ' Aggiorno eventuali errori durante la generazione
            sSQL = "UPDATE Decisions_Boss_Value SET IDPeriod = NULL WHERE UPPER(Value) LIKE '%LIST OF%' "
            g_DAL.ExecuteNonQuery(sSQL)

        Catch ex As Exception
            txtMessageGame.Text = "Error LoadDataDecisionsBoss -> " & ex.Message & "<br/>" & sSQL
            txtMessageGame.Visible = True
        End Try

    End Sub

    Private Sub LoadDataDecisionsBoss(IDGame As Integer, IDGameNew As Integer, IDPeriodo As Integer, IDPeriodoGameNew As Integer, IDTeamMeU As Integer, TeamNameMeU As String)
        Dim sSQL As String = ""
        Dim iIDTeamMaster As Integer
        Dim iIDPlayerMaster As Integer
        Dim sTeamNameMaster As String
        Dim sTeamName As String
        Dim iIDDecisionBossNew As Integer
        Dim iIDDecisionBossValueNew As Integer

        Try
            ' Recupero i dati di del primo player, generalmente è quello usato come master
            sSQL = "SELECT MIN(BPTG.IDPLayer) AS IDPlayer , MIN(BPTG.IdTeam) AS IDTeam " _
                 & "FROM BGOL_Players_Teams_Games BPTG " _
                 & "INNER JOIN BGOL_Players_Games BTG ON BPTG.IdGame = BTG.IDGame " _
                 & "AND BPTG.IdPlayer = BTG.IDPlayer " _
                 & "AND BPTG.IdTeam = BTG.IDTeam " _
                 & "WHERE BPTG.IDGame = " & IDGame & " AND BPTG.IDTeam <> " & IDTeamMeU & " AND BTG.IDRole = 'P' "
            Dim dtData As DataTable = g_DAL.ExecuteDataTable(sSQL)
            For Each drData As DataRow In dtData.Rows
                iIDTeamMaster = Nni(drData("IDTeam"))
                iIDPlayerMaster = Nni(drData("IDPlayer"))
            Next
            sSQL = "SELECT TeamName FROM BGOL_Teams WHERE ID = " & iIDTeamMaster
            sTeamNameMaster = Nz(g_DAL.ExecuteScalar(sSQL))

            ' Recupero i record della tabella Decisions_Boss_Value
            sSQL = "SELECT * FROM Decisions_Boss_Value DSV " _
                 & "INNER JOIN Decisions_Boss DB ON DSV.IDDecisionBoss = DB.Id " _
                 & "WHERE DB.IDGame = " & IDGame & " " _
                 & "AND ISNULL(DSV.IDPeriod, " & IDPeriodo & ") = " & IDPeriodo
            Dim dtDecisions_Boss_Value As DataTable = g_DAL.ExecuteDataTable(sSQL)

            dtDecisions_Boss_Value.PrimaryKey = New DataColumn() {dtDecisions_Boss_Value.Columns("IDDecisionBoss"),
                                                                  dtDecisions_Boss_Value.Columns("ID")}

            For Each drDecisions_Boss_Value As DataRow In dtDecisions_Boss_Value.Rows

                ' Recupero l'IDDecisionBoss del game di destinazione
                sSQL = "SELECT ID FROM Decisions_Boss WHERE Decision = '" & Nz(drDecisions_Boss_Value("Decision")) & "' AND IDGame = " & IDGameNew
                iIDDecisionBossNew = g_DAL.ExecuteScalar(sSQL)

                If Nz(drDecisions_Boss_Value("Value")).ToUpper.Contains("LIST OF") Then
                    ' Recupero la riga della lista di dettaglio
                    sSQL = "SELECT * FROM Decisions_Boss_Value_List " _
                         & "WHERE IdDecisionBossValue IN(" & drDecisions_Boss_Value("ID") & ") AND ISNULL(IDPeriod, " & IDPeriodo & ") = " & IDPeriodo
                    Dim dtDecisions_Boss_Value_List As DataTable = g_DAL.ExecuteDataTable(sSQL)
                    dtDecisions_Boss_Value_List.PrimaryKey = New DataColumn() {dtDecisions_Boss_Value_List.Columns("IdDecisionBossValue"),
                                                                               dtDecisions_Boss_Value_List.Columns("VariableName"),
                                                                               dtDecisions_Boss_Value_List.Columns("IDPeriod"),
                                                                               dtDecisions_Boss_Value_List.Columns("Id")}

                    For Each drDecisions_Boss_Value_List As DataRow In dtDecisions_Boss_Value_List.Rows
                        ' Recupero l'ID della variabile dal gioco nuovo per l'inserimento corretto
                        sSQL = "SELECT ID FROM Decisions_Boss_Value WHERE IdDecisionBoss = " & iIDDecisionBossNew
                        iIDDecisionBossValueNew = Nni(g_DAL.ExecuteScalar(sSQL))

                        ' Controllo che nel nome della variabile non sia contenuto il nome del team, nel caso devo generare per tutti i player
                        If Nz(drDecisions_Boss_Value_List("Value")).ToUpper.Contains(sTeamNameMaster) Then
                            For iPlayer As Integer = 1 To txtNumPlayers.Text
                                ' Genero il nome del team e del player
                                sTeamName = "TeamName_" & CStrSql(txtDescription.Text).Replace(" ", "") & "_" & iPlayer
                                sSQL = "INSERT INTO Decisions_Boss_Value_List (IdDecisionBossValue, IDPeriod, Simulation, Value, VariableName) " _
                                     & "SELECT " & iIDDecisionBossValueNew & ", " & IDPeriodoGameNew & " , Simulation, Value, " _
                                     & "REPLACE(VariableName, " & sTeamNameMaster & ", '" & CStrSql(sTeamName) & "') " _
                                     & "FROM Decisions_Boss_Value_List DBVL " _
                                     & "WHERE Id = " & Nni(drDecisions_Boss_Value_List("Id")) & " AND VariableName LIKE '%" & sTeamNameMaster & "%' AND IDPeriod = " & IDPeriodo
                                g_DAL.ExecuteNonQuery(sSQL)
                            Next
                        Else
                            sSQL = "INSERT INTO Decisions_Boss_Value_List (IdDecisionBossValue, IDPeriod, Simulation, Value, VariableName) " _
                                 & "SELECT " & iIDDecisionBossValueNew & ", " & IDPeriodoGameNew & " , Simulation, Value, VariableName " _
                                 & "FROM Decisions_Boss_Value_List DBVL " _
                                 & "WHERE Id = " & Nni(drDecisions_Boss_Value_List("Id")) & " AND IDPeriod = " & IDPeriodo
                            g_DAL.ExecuteNonQuery(sSQL)
                        End If
                    Next

                Else ' Valore secco da inserire 
                    sSQL = "INSERT INTO Decisions_Boss_Value (IDDecisionBoss, Value, IDPeriod) VALUES (" _
                         & iIDDecisionBossNew & ", '" & drDecisions_Boss_Value("Value") & "', " & IDPeriodoGameNew & ") "
                    g_DAL.ExecuteNonQuery(sSQL)
                End If
            Next

            ' Aggiorno eventuali errori durante la generazione
            sSQL = "UPDATE Decisions_Boss_Value SET IDPeriod = NULL WHERE UPPER(Value) LIKE '%LIST OF%' "
            g_DAL.ExecuteNonQuery(sSQL)

            ' Faccio pulizia nei nomi dei team appena inseriti
            ' Recupero tutti i nomi dei team del gioco originale
            Dim dtTeamOrigin As DataTable = LoadTeamsGame(IDGame)
            Dim dtTeamDest As DataTable = LoadTeamsGame(IDGameNew)
            Dim iContaTeam As Integer = 1
            Dim drTeamFind As DataRow
            Dim sNomeTeamOrigin As String

            For Each drTeam As DataRow In dtTeamOrigin.Rows
                sNomeTeamOrigin = Nz(drTeam("TeamName"))
                ' Recupero il rispettivo nome del team di destinazione, il collegamento è 1:1
                drTeamFind = dtTeamDest.Select("TeamName LIKE '%" & txtDescription.Text & "_" & iContaTeam & "%'").FirstOrDefault

                sSQL = "UPDATE Decisions_Boss_Value_List SET VariableName = REPLACE(VariableName, '" & sNomeTeamOrigin & "', '" & Nz(drTeamFind("TeamName")) & "') " _
                     & "WHERE ID IN (SELECT DBVL.ID FROM Decisions_Boss D LEFT JOIN Decisions_Boss_Value DBV ON D.Id = DBV.IDDecisionBoss " _
                     & "LEFT JOIN Decisions_Boss_Value_List DBVL ON DBV.id = DBVL.IdDecisionBossValue " _
                     & "WHERE IDGame = " & IDGameNew & ")"
                g_DAL.ExecuteNonQuery(sSQL)
                iContaTeam += 1
            Next

        Catch ex As Exception
            txtMessageGame.Text = "Error LoadDataDecisionsBoss -> " & ex.Message & "<br/>" & sSQL
            txtMessageGame.Visible = True
        End Try

    End Sub

    Private Sub LoadDataDecisionsDeveloper(IDGame As Integer, IDGameNew As Integer, IDPeriodoPrimo As Integer, IDPeriodoPrimoGameNew As Integer, IDGameDelete As Integer, IDTeamMaster As Integer)
        Dim sSQL As String = ""
        Dim sTeamName As String = ""
        Dim oDTTeams As DataTable = APSUtility.LoadTeamsGame(IDGame)

        Try
            ' Cancello gli eventuali dati del vecchio game
            Dim oprmID As New DBParameter("@IDGame", IDGameDelete, DbType.Int16)
            Dim oprmCollection As New DBParameterCollection()
            oprmCollection.Add(oprmID)
            g_DAL.ExecuteNonQuery("sp_Developer_Decisions_Delete", oprmCollection, CommandType.StoredProcedure)

            ' Avvio l'inserimento delle decisioni del developer
            ' Recupero tutte le decisioni 
            sSQL = "SELECT D.Id, D.IDDataType, D.IDGroup, D.IDVariable, D.VariableLabel, D.VariableLabelGroup, D.VariableName " _
                 & "FROM Decisions_Developer D WHERE IDGame = " & IDGame

            Dim oDTDecisionsDeveloper As DataTable = g_DAL.ExecuteDataTable(sSQL)
            oDTDecisionsDeveloper.PrimaryKey = New DataColumn() {oDTDecisionsDeveloper.Columns("IDGame"), oDTDecisionsDeveloper.Columns("ID"), oDTDecisionsDeveloper.Columns("VariableName")}

            For Each drDecisionsDeveloper As DataRow In oDTDecisionsDeveloper.Rows
                'If Nz(drDecisionsDeveloper("VariableName")).ToUpper = "SELLIPERSOCOST" Then
                '    MessageText.Text = "Eccomi... "
                'End If

                ' Inserico la variabile nel db
                sSQL = "INSERT INTO Decisions_Developer (IDDataType, IDGroup, IDVariable, VariableLabel, VariableLabelGroup, VariableName, IDGame) " _
                     & "SELECT D.IDDataType, D.IDGroup, D.IDVariable, D.VariableLabel, D.VariableLabelGroup, D.VariableName, " & IDGameNew & " " _
                     & "FROM Decisions_Developer D " _
                     & "WHERE D.ID = " & Nni(drDecisionsDeveloper("ID"))
                g_DAL.ExecuteNonQuery(sSQL)

                ' Recupero l'ultimo ID inserito
                sSQL = "SELECT MAX(ID) FROM Decisions_Developer "
                Dim iIDDecisionDeveloper As Integer = Nni(g_DAL.ExecuteScalar(sSQL))

                ' Vado a recuperare tutti i dati nella tabella contenente i valori
                sSQL = "SELECT DBV.Id, DBV.IDDecisionDeveloper, DBV.IDItem, DBV.IDPlayer, DBV.IDPeriod, DBV.Simulation, DBV.Value " _
                     & "FROM Decisions_Developer_Value DBV " _
                     & "WHERE DBV.IDDecisionDeveloper = " & Nni(drDecisionsDeveloper("ID")) _
                     & " AND ISNULL(IDPeriod, " & IDPeriodoPrimo & ") = " & IDPeriodoPrimo

                Dim oDTDecisionsDeveloperValue As DataTable = g_DAL.ExecuteDataTable(sSQL)
                oDTDecisionsDeveloperValue.PrimaryKey = New DataColumn() {oDTDecisionsDeveloperValue.Columns("ID"), oDTDecisionsDeveloperValue.Columns("IDDecisionDeveloper"),
                                                                          oDTDecisionsDeveloperValue.Columns("IDPeriod"), oDTDecisionsDeveloperValue.Columns("Value")}

                For Each drDecisionsDeveloperValue As DataRow In oDTDecisionsDeveloperValue.Rows
                    If Nni(drDecisionsDeveloperValue("IDPlayer")) > 0 Then
                        If Nni(drDecisionsDeveloperValue("IDPlayer")) = IDTeamMaster Then
                            For iPlayer As Integer = 1 To txtNumPlayers.Text
                                sTeamName = "TeamName_" & CStrSql(txtDescription.Text).Replace(" ", "") & "_" & iPlayer
                                ' Recupero l'ID del team da inserire
                                Dim iIDTeamNew As Integer
                                sSQL = "SELECT ID FROM BGOL_Teams WHERE TeamName = '" & sTeamName & "' "
                                iIDTeamNew = Nni(g_DAL.ExecuteScalar(sSQL))

                                sSQL = "INSERT INTO Decisions_Developer_Value (IDDecisionDeveloper, IDItem, IDPlayer, IDPeriod, Simulation, Value) " _
                                     & "SELECT " & iIDDecisionDeveloper & ", DBV.IDItem, " & iIDTeamNew & ", " & IDPeriodoPrimoGameNew & ", DBV.Simulation, DBV.Value " _
                                     & "FROM Decisions_Developer_Value DBV " _
                                     & "WHERE DBV.ID = " & Nni(drDecisionsDeveloperValue("Id"))
                                g_DAL.ExecuteNonQuery(sSQL)
                            Next

                        Else
                            If Nz(drDecisionsDeveloper("VariableName")).ToUpper <> "SELLIPERSOCOST" Then ' Variabile particolare, può creare del casino, per cui la escludo dal controllo e inserimento
                                sSQL = "INSERT INTO Decisions_Developer_Value (IDDecisionDeveloper, IDItem, IDPlayer, IDPeriod, Simulation, Value) " _
                                 & "SELECT " & iIDDecisionDeveloper & ", DBV.IDItem, " & Nni(drDecisionsDeveloperValue("IDPlayer")) & ", " & IDPeriodoPrimoGameNew & ", DBV.Simulation, DBV.Value " _
                                 & "FROM Decisions_Developer_Value DBV " _
                                 & "WHERE DBV.ID = " & Nni(drDecisionsDeveloperValue("Id"))
                                g_DAL.ExecuteNonQuery(sSQL)
                            End If
                        End If
                    Else
                        sSQL = "INSERT INTO Decisions_Developer_Value (IDDecisionDeveloper, IDItem, IDPlayer, IDPeriod, Simulation, Value) " _
                             & "SELECT " & iIDDecisionDeveloper & ", DBV.IDItem, DBV.IDPlayer, " & IDPeriodoPrimoGameNew & ", DBV.Simulation, DBV.Value " _
                             & "FROM Decisions_Developer_Value DBV " _
                             & "WHERE DBV.ID = " & Nni(drDecisionsDeveloperValue("Id"))
                        g_DAL.ExecuteNonQuery(sSQL)
                    End If
                Next
            Next

        Catch ex As Exception
            txtMessageGame.Text = "Error LoadDataDecisionsDeveloper -> " & ex.Message & "<br/>" & sSQL
            txtMessageGame.Visible = True
        End Try
    End Sub

    Private Sub LoadDataDecisionsDeveloper(IDGame As Integer, IDGameNew As Integer, IDPeriodo As Integer, IDPeriodoGameNew As Integer, IDTeamMeU As Integer, TeamNameMeU As String)
        Dim sSQL As String = ""
        Dim iIDTeamMaster As Integer
        Dim iIDPlayerMaster As Integer
        Dim sTeamNameMaster As String
        Dim sTeamName As String
        Dim iIDTeamNew As Integer
        Dim iIDDecisionDeveloperNew As Integer

        Try
            ' Recupero i dati di del primo player, generalmente è quello usato come master
            sSQL = "SELECT MIN(BPTG.IDPLayer) AS IDPlayer , MIN(BPTG.IdTeam) AS IDTeam " _
                 & "FROM BGOL_Players_Teams_Games BPTG " _
                 & "INNER JOIN BGOL_Players_Games BTG ON BPTG.IdGame = BTG.IDGame " _
                 & "AND BPTG.IdPlayer = BTG.IDPlayer " _
                 & "AND BPTG.IdTeam = BTG.IDTeam " _
                 & "WHERE BPTG.IDGame = " & IDGame & " AND BPTG.IDTeam <> " & IDTeamMeU & " AND BTG.IDRole = 'P' "
            Dim dtData As DataTable = g_DAL.ExecuteDataTable(sSQL)
            For Each drData As DataRow In dtData.Rows
                iIDTeamMaster = Nni(drData("IDTeam"))
                iIDPlayerMaster = Nni(drData("IDPlayer"))
            Next
            sSQL = "SELECT TeamName FROM BGOL_Teams WHERE ID = " & iIDTeamMaster
            sTeamNameMaster = Nz(g_DAL.ExecuteScalar(sSQL))

            sSQL = "SELECT * FROM Decisions_Developer_Value DDV " _
                 & "INNER JOIN Decisions_Developer DD ON DDV.IDDecisionDeveloper = DD.Id " _
                 & "WHERE DD.IDGame = " & IDGame & " AND ISNULL(DDV.IDPeriod, " & IDPeriodo & ") = " & IDPeriodo
            Dim dtDecisions_Developer_Value As DataTable = g_DAL.ExecuteDataTable(sSQL)

            For Each drDecisions_Developer_Value As DataRow In dtDecisions_Developer_Value.Rows
                ' Recupero l'ID della variabile developer per il nuovo gioco
                sSQL = "SELECT ID FROM Decisions_Developer WHERE IDGame = " & IDGameNew & " AND VariableName = '" & Nz(drDecisions_Developer_Value("VariableName")) & "' "
                iIDDecisionDeveloperNew = g_DAL.ExecuteScalar(sSQL)

                ' Controllo la presenza dell'IDPlayer, se presente allora ciclo su tutti i player del nuovo gioco, altrimenti 
                If Nni(drDecisions_Developer_Value("IDPlayer")) > 0 Then
                    If Nni(drDecisions_Developer_Value("IDPlayer")) = iIDTeamMaster Then
                        For iPlayer As Integer = 1 To txtNumPlayers.Text
                            sTeamName = "TeamName_" & CStrSql(txtDescription.Text).Replace(" ", "") & "_" & iPlayer
                            ' Recupero l'ID del team da inserire
                            sSQL = "SELECT ID FROM BGOL_Teams WHERE TeamName = '" & sTeamName & "' "
                            iIDTeamNew = Nni(g_DAL.ExecuteScalar(sSQL))

                            ' Inserisco i dati nella tabella
                            sSQL = "INSERT INTO Decisions_Developer_Value (IDDecisionDeveloper, IDPeriod, Value, IDItem, IDPlayer) VALUES (" _
                                 & iIDDecisionDeveloperNew & ", " & IDPeriodoGameNew & ", '" & Nz(drDecisions_Developer_Value("Value")) & "', " _
                                 & Nni(drDecisions_Developer_Value("IDItem")) & ", " & iIDTeamNew & ") "
                            g_DAL.ExecuteNonQuery(sSQL)
                        Next

                    End If
                Else
                    sSQL = "INSERT INTO Decisions_Developer_Value (IDDecisionDeveloper, IDPeriod, Value, IDItem) VALUES (" _
                         & iIDDecisionDeveloperNew & ", " & IDPeriodoGameNew & ", '" & Nz(drDecisions_Developer_Value("Value")) & "', " & Nni(drDecisions_Developer_Value("IDItem")) & ") "
                    g_DAL.ExecuteNonQuery(sSQL)
                End If
            Next

        Catch ex As Exception
            txtMessageGame.Text = "Error LoadDataDecisionsDeveloper -> " & ex.Message & "<br/>" & sSQL
            txtMessageGame.Visible = True
        End Try
    End Sub

    Private Sub LoadDataVariablesState(IDGame As Integer, IDGameNew As Integer, IDPeriodoPrimo As Integer, IDPeriodoPrimoGameNew As Integer, IDGameDelete As Integer, IDTeamMaster As Integer)
        Dim sSQL As String = ""
        Dim sTeamName As String = ""
        Dim sPlayer As String = ""
        Dim oDTTeams As DataTable = APSUtility.LoadTeamsGame(IDGame)
        Dim iIDTeamNew As Integer
        Dim iIDTeamMaster As Integer
        Dim iIDPlayerMaster As Integer
        Dim sTeamNameMaster As String

        Try
            ' Cancello gli eventuali dati del vecchio game
            Dim oprmID As New DBParameter("@IDGame", IDGameDelete, DbType.Int16)
            Dim oprmCollection As New DBParameterCollection()
            oprmCollection.Add(oprmID)
            g_DAL.ExecuteNonQuery("sp_VariableState_Decisions_Delete", oprmCollection, CommandType.StoredProcedure)

            ' Recupero i dati di del primo player, generalmente è quello usato come master
            sSQL = "SELECT MIN(BPTG.IDPLayer) AS IDPlayer , MIN(BPTG.IdTeam) AS IDTeam " _
                 & "FROM BGOL_Players_Teams_Games BPTG " _
                 & "INNER JOIN BGOL_Players_Games BTG ON BPTG.IdGame = BTG.IDGame " _
                 & "AND BPTG.IdPlayer = BTG.IDPlayer " _
                 & "AND BPTG.IdTeam = BTG.IDTeam " _
                 & "WHERE BPTG.IDGame = " & IDGame & " AND BPTG.IDTeam = " & IDTeamMaster & " AND BTG.IDRole = 'P' "
            Dim dtData As DataTable = g_DAL.ExecuteDataTable(sSQL)
            For Each drData As DataRow In dtData.Rows
                iIDTeamMaster = Nni(drData("IDTeam"))
                iIDPlayerMaster = Nni(drData("IDPlayer"))
            Next
            sSQL = "SELECT TeamName FROM BGOL_Teams WHERE ID = " & iIDTeamMaster
            sTeamNameMaster = Nz(g_DAL.ExecuteScalar(sSQL))

            ' Avvio l'inserimento delle variabili di stato
            sSQL = "SELECT VS.Id, VS.IDDataType, VS.IDGame, VS.IDGroup, VS.Name, VS.VariableLabel FROM Variables_State VS WHERE IDGame = " & IDGame
            g_DAL.ExecuteNonQuery(sSQL)
            Dim oDTVariablesState As DataTable = g_DAL.ExecuteDataTable(sSQL)
            oDTVariablesState.PrimaryKey = New DataColumn() {oDTVariablesState.Columns("Name"), oDTVariablesState.Columns("IDGame"), oDTVariablesState.Columns("IDAge")}

            For Each drVariablesState As DataRow In oDTVariablesState.Rows
                sSQL = "INSERT INTO Variables_State (IDDataType, IDGame, IDGroup, Name, VariableLabel) " _
                     & "SELECT VS.IDDataType, " & IDGameNew & ", VS.IDGroup, VS.Name, VS.VariableLabel FROM Variables_State VS " _
                     & "WHERE VS.ID = " & Nni(drVariablesState("ID"))
                g_DAL.ExecuteNonQuery(sSQL)

                sSQL = "SELECT MAX(ID) FROM Variables_State "
                Dim iIDVariableState As Integer = Nni(g_DAL.ExecuteScalar(sSQL))

                ' Recupero tutti i dati legati alla variabile in esame, prima sulla tabella VALUE
                sSQL = "SELECT VSV.Id, VSV.IDAge, VSV.IDItem, VSV.IDPeriod, VSV.IDPlayer, VSV.IDVariable, VSV.Simulation, VSV.Value " _
                     & "FROM Variables_State_Value VSV " _
                     & "WHERE VSV.IDVariable = " & Nni(drVariablesState("ID")) & " AND ISNULL(VSV.IDPeriod, " & IDPeriodoPrimo & ") = " & IDPeriodoPrimo _
                     & " AND ISNULL(IDPlayer, " & IDTeamMaster & ") = " & IDTeamMaster
                Dim oDTVariableStateValue As DataTable = g_DAL.ExecuteDataTable(sSQL)
                oDTVariableStateValue.PrimaryKey = New DataColumn() {oDTVariableStateValue.Columns("IDVariable"), oDTVariableStateValue.Columns("ID")}

                For Each drVariableStateValue As DataRow In oDTVariableStateValue.Rows

                    If Nz(drVariableStateValue("Value")).ToUpper.Contains("LIST OF") Then
                        ' Inserisco la variabile di stato valore
                        sSQL = "INSERT INTO Variables_State_Value (IDAge, IDItem, IDPeriod, IDPlayer, IDVariable, Simulation, Value) " _
                             & "SELECT VSV.IDAge, VSV.IDItem, " & IDPeriodoPrimoGameNew & " , VSV.IDPlayer, " & iIDVariableState & ", VSV.Simulation, VSV.Value " _
                             & "FROM Variables_State_Value VSV " _
                             & "WHERE VSV.ID = " & Nni(drVariableStateValue("Id"))
                        g_DAL.ExecuteNonQuery(sSQL)
                        'Recupero l'ultimo ID inserito
                        sSQL = "SELECT MAX(ID) FROM Variables_State_Value "
                        Dim iIDVariableStateValue As Integer = Nni(g_DAL.ExecuteScalar(sSQL))

                        ' Procedo con il caricamento dei dati relativi alla variabile di stato
                        sSQL = "SELECT VSVL.ID, VSVL.IDAge, VSVL.IDItem, VSVL.IDPeriod, VSVL.IDPlayer, VSVL.IDVariableState, VSVL.Simulation, VSVL.Value, VSVL.VariableName " _
                             & "FROM Variables_State_Value_List VSVL " _
                             & "WHERE VSVL.IDVariableState = " & Nni(drVariableStateValue("ID"))
                        If chkPrimoPeriodo.Checked Then
                            sSQL &= " AND ISNULL(VSVL.IDPeriod, " & IDPeriodoPrimo & ") = " & IDPeriodoPrimo
                        End If
                        Dim oDTVariableStateValueList As DataTable = g_DAL.ExecuteDataTable(sSQL)
                        oDTVariableStateValueList.PrimaryKey = New DataColumn() {oDTVariableStateValueList.Columns("IDPeriod"), oDTVariableStateValueList.Columns("VariableName"),
                                                                                 oDTVariableStateValueList.Columns("IDPlayer"), oDTVariableStateValueList.Columns("ID")}

                        For Each drVariableStateValueList As DataRow In oDTVariableStateValueList.Rows
                            ' Recupero i dati del teammaster e genero quelli dei giocatori
                            If Nni(drVariableStateValueList("IDPlayer")) = IDTeamMaster Then
                                For iPlayer As Integer = 1 To txtNumPlayers.Text
                                    sTeamName = "TeamName_" & CStrSql(txtDescription.Text).Replace(" ", "") & "_" & iPlayer
                                    sPlayer = "TeamName_" & CStrSql(txtDescription.Text).Replace(" ", "") & "_" & iPlayer
                                    sSQL = "SELECT ID FROM BGOL_Teams WHERE TeamName = '" & CStrSql(sTeamName) & "' "
                                    iIDTeamNew = Nni(g_DAL.ExecuteScalar(sSQL))
                                    sSQL = "INSERT INTO Variables_State_Value_List (IDAge, IDItem, IDPeriod, IDPlayer, IDVariableState, Simulation, Value, VariableName) " _
                                         & "SELECT VSVL.IDAge, VSVL.IDItem, " & IDPeriodoPrimoGameNew & ", " & iIDTeamNew & ", " & iIDVariableStateValue & ", VSVL.Simulation, VSVL.Value, " _
                                         & "REPLACE(VSVL.VariableName, '" & sTeamNameMaster & "', '" & CStrSql(sTeamName) & "') " _
                                         & "FROM Variables_State_Value_List VSVL " _
                                         & "WHERE VSVL.ID = " & Nni(drVariableStateValueList("Id"))
                                    g_DAL.ExecuteNonQuery(sSQL)
                                Next
                            End If
                        Next

                    Else
                        ' Non è una variabile di tipo lista per cui procedo con l'inserimento diretto
                        ' Recupero tutte le varibili che non contengono l'idplyer e tutte quelle con l'id player che mi interessa
                        sSQL = "SELECT VSV.Id, VSV.IDAge, VSV.IDItem, VSV.IDPeriod, VSV.IDPlayer, VSV.IDPlayer, VSV.IDVariable, VSV.Simulation, VSV.Value " _
                             & "FROM Variables_State_Value VSV " _
                             & "WHERE ISNULL(IDPlayer, " & IDTeamMaster & ") = " & IDTeamMaster & " AND IDVariable = " & Nni(drVariableStateValue("IDVariable")) _
                             & " AND ISNULL(IDPeriod, " & IDPeriodoPrimo & ") = " & IDPeriodoPrimo
                        Dim oDTData As DataTable = g_DAL.ExecuteDataTable(sSQL)
                        oDTData.PrimaryKey = New DataColumn() {oDTData.Columns("IDVariable"), oDTData.Columns("ID")}

                        For Each drData As DataRow In oDTData.Rows
                            For iPlayer As Integer = 1 To txtNumPlayers.Text
                                sTeamName = "TeamName_" & CStrSql(txtDescription.Text).Replace(" ", "") & "_" & iPlayer
                                sSQL = "SELECT ID FROM BGOL_Teams WHERE TeamName = '" & CStrSql(sTeamName) & "' "
                                iIDTeamNew = Nni(g_DAL.ExecuteScalar(sSQL))
                                sSQL = "INSERT INTO Variables_State_Value (IDAge, IDItem, IDPeriod, IDPlayer, IDVariable, Simulation, Value) " _
                                     & "SELECT VSV.IDAge, VSV.IDItem, " & IDPeriodoPrimoGameNew & ", " & iIDTeamNew & ", " & iIDVariableState & ", VSV.Simulation, VSV.Value " _
                                     & "FROM Variables_State_Value VSV " _
                                     & "WHERE VSV.ID = " & Nni(drData("ID"))
                                g_DAL.ExecuteScalar(sSQL)
                            Next
                            sSQL = "INSERT INTO Variables_State_Value (IDAge, IDItem, IDPeriod, IDPlayer, IDVariable, Simulation, Value) " _
                                 & "SELECT VSV.IDAge, VSV.IDItem, " & IDPeriodoPrimoGameNew & ", " & IDTeamMaster & ", " & iIDVariableState & ", VSV.Simulation, VSV.Value " _
                                 & "FROM Variables_State_Value VSV " _
                                 & "WHERE VSV.ID = " & Nni(drData("ID"))
                            g_DAL.ExecuteScalar(sSQL)
                        Next
                    End If
                Next
            Next

        Catch ex As Exception
            txtMessageGame.Text = "Error LoadDataVariablesState -> " & ex.Message & "<br/>" & sSQL
            txtMessageGame.Visible = True
        End Try
    End Sub

    Private Sub LoadDataVariablesState(IDGame As Integer, IDGameNew As Integer, IDPeriodo As Integer, IDPeriodoGameNew As Integer, IDTeamMeU As Integer, TeamNameMeU As String)
        Dim sSQL As String = ""
        Dim iIDTeamMaster As Integer
        Dim iIDPlayerMaster As Integer
        Dim sTeamNameMaster As String
        Dim sTeamName As String
        Dim iIDTeamNew As Integer
        Dim iIDVariableState As Integer
        Dim iIDVariableStateValue As Integer
        Dim sPlayer As String

        Try
            ' Recupero i dati di del primo player, generalmente è quello usato come master
            sSQL = "SELECT MIN(BPTG.IDPLayer) AS IDPlayer , MIN(BPTG.IdTeam) AS IDTeam " _
                 & "FROM BGOL_Players_Teams_Games BPTG " _
                 & "INNER JOIN BGOL_Players_Games BTG ON BPTG.IdGame = BTG.IDGame " _
                 & "AND BPTG.IdPlayer = BTG.IDPlayer " _
                 & "AND BPTG.IdTeam = BTG.IDTeam " _
                 & "WHERE BPTG.IDGame = " & IDGame & " AND BPTG.IDTeam <> " & IDTeamMeU & " AND BTG.IDRole = 'P' "
            Dim dtData As DataTable = g_DAL.ExecuteDataTable(sSQL)
            For Each drData As DataRow In dtData.Rows
                iIDTeamMaster = Nni(drData("IDTeam"))
                iIDPlayerMaster = Nni(drData("IDPlayer"))
            Next
            sSQL = "SELECT TeamName FROM BGOL_Teams WHERE ID = " & iIDTeamMaster
            sTeamNameMaster = Nz(g_DAL.ExecuteScalar(sSQL))

            ' Recupero le variabili che mi servono
            sSQL = "SELECT * FROM Variables_State_Value VSV " _
                 & "INNER JOIN Variables_State VS ON VSV.IDVariable = VS.Id " _
                 & "WHERE VS.IDGame = " & IDGame & " AND ISNULL(VSV.IDPeriod, " & IDPeriodo & ") = " & IDPeriodo
            '& " AND Name = 'StockRawMater' "
            Dim dtVariables_State_Value As DataTable = g_DAL.ExecuteDataTable(sSQL)
            dtVariables_State_Value.PrimaryKey = New DataColumn() {dtVariables_State_Value.Columns("IDVariable"), dtVariables_State_Value.Columns("ID")}

            For Each drVariables_State_Value As DataRow In dtVariables_State_Value.Rows
                ' Recupero l'ID della variabile di stato del game attuale
                sSQL = "SELECT ID FROM Variables_State WHERE IDGame = " & IDGameNew & " AND Name = '" & Nz(drVariables_State_Value("Name")) & "' "
                iIDVariableState = g_DAL.ExecuteScalar(sSQL)

                ' Recupero l'ID della variabile valore da usare
                sSQL = "SELECT ID FROM Variables_State_Value WHERE IDVariable = " & iIDVariableState
                iIDVariableStateValue = g_DAL.ExecuteScalar(sSQL)

                ' Controllo il tipo di variabile
                ' Controllo se di tipo LISTA
                If Nz(drVariables_State_Value("Value")).ToUpper.Contains("LIST OF") Then
                    ' Ciclo sui player del gioco per comporre anche il nome della variabile e inserirla
                    If Nni(drVariables_State_Value("IDPlayer")) = iIDTeamMaster Then
                        For iPlayer As Integer = 1 To txtNumPlayers.Text
                            sTeamName = "TeamName_" & CStrSql(txtDescription.Text).Replace(" ", "") & "_" & iPlayer
                            sPlayer = "TeamName_" & CStrSql(txtDescription.Text).Replace(" ", "") & "_" & iPlayer

                            sSQL = "SELECT ID FROM BGOL_Teams WHERE TeamName = '" & CStrSql(sTeamName) & "' "
                            iIDTeamNew = Nni(g_DAL.ExecuteScalar(sSQL))

                            sSQL = "INSERT INTO Variables_State_Value_List (IDAge, IDItem, IDPeriod, IDPlayer, IDVariableState, Simulation, Value, VariableName) " _
                                 & "SELECT VSVL.IDAge, VSVL.IDItem, " & IDPeriodoGameNew & ", " & iIDTeamNew & ", " & iIDVariableStateValue & ", VSVL.Simulation, VSVL.Value, " _
                                 & "REPLACE('" & sTeamNameMaster & "', '" & sTeamName & "') " _
                                 & "FROM Variables_State_Value_List VSVL " _
                                 & "WHERE VSVL.ID = " & Nni(drVariables_State_Value("Id"))
                            g_DAL.ExecuteNonQuery(sSQL)
                        Next
                    End If

                Else

                    ' For Each drData As DataRow In oDTData.Rows
                    If Nni(drVariables_State_Value("IDPlayer")) > 0 Then
                        If Nni(drVariables_State_Value("IDPlayer")) = iIDTeamMaster Then
                            For iPlayer As Integer = 1 To txtNumPlayers.Text
                                sTeamName = "TeamName_" & CStrSql(txtDescription.Text).Replace(" ", "") & "_" & iPlayer
                                sSQL = "SELECT ID FROM BGOL_Teams WHERE TeamName = '" & CStrSql(sTeamName) & "' "
                                iIDTeamNew = Nni(g_DAL.ExecuteScalar(sSQL))
                                sSQL = "INSERT INTO Variables_State_Value (IDAge, IDItem, IDPeriod, IDPlayer, IDVariable, Simulation, Value) " _
                                     & "SELECT VSV.IDAge, VSV.IDItem, " & IDPeriodoGameNew & ", " & iIDTeamNew & ", " & iIDVariableState & ", VSV.Simulation, VSV.Value " _
                                     & "FROM Variables_State_Value VSV " _
                                     & "WHERE VSV.ID = " & Nni(drVariables_State_Value("ID"))
                                g_DAL.ExecuteScalar(sSQL)
                            Next
                        End If
                    Else
                        sSQL = "INSERT INTO Variables_State_Value (IDAge, IDItem, IDPeriod, IDPlayer, IDVariable, Simulation, Value) " _
                             & "SELECT VSV.IDAge, VSV.IDItem, " & IDPeriodoGameNew & ", IDPlayer, " & iIDVariableState & ", VSV.Simulation, VSV.Value " _
                             & "FROM Variables_State_Value VSV " _
                             & "WHERE VSV.ID = " & Nni(drVariables_State_Value("ID"))
                        g_DAL.ExecuteScalar(sSQL)
                    End If

                End If
            Next
        Catch ex As Exception
            txtMessageGame.Text = "Error LoadDataVariablesState -> " & ex.Message & "<br/>" & sSQL
            txtMessageGame.Visible = True
        End Try
    End Sub

    Private Sub LoadDataDecisionsPlayer(IDGame As Integer, IDGameNew As Integer, IDPeriodoPrimo As Integer, IDPeriodoPrimoGameNew As Integer, IDGameDelete As Integer, IDTeamMaster As Integer)
        Dim sSQL As String = ""
        Dim sTeamName As String = ""
        Dim sPlayer As String = ""
        Dim oDTTeams As DataTable = APSUtility.LoadTeamsGame(IDGame)
        Dim iIDTeamNew As Integer
        Dim sNomeVariabile As String
        Dim sNomeItem As String

        Try
            ' Cancello gli eventuali dati del vecchio game
            Dim oprmID As New DBParameter("@IDGame", IDGameDelete, DbType.Int16)
            Dim oprmCollection As New DBParameterCollection()
            oprmCollection.Add(oprmID)
            g_DAL.ExecuteNonQuery("sp_Player_Decisions_Delete", oprmCollection, CommandType.StoredProcedure)

            ' Avvio l'inserimento delle decisioni del player
            sSQL = "SELECT D.Id, D.IDDataType, D.IDGame, D.IDVariable, D.VariableLabel, D.VariableLabelGroup, D.VariableName " _
                 & "FROM Decisions_Players D " _
                 & "WHERE D.IDGame = " & IDGame
            Dim oDTDecisionsPLayers As DataTable = g_DAL.ExecuteDataTable(sSQL)
            oDTDecisionsPLayers.PrimaryKey = New DataColumn() {oDTDecisionsPLayers.Columns("ID"), oDTDecisionsPLayers.Columns("VariableName"), oDTDecisionsPLayers.Columns("IDGame")}

            For Each drDecisionsPlayer As DataRow In oDTDecisionsPLayers.Rows
                ' MI recupero il numero di Step dell'origine per eventuali blocchi da inserire
                sSQL = "SELECT NumStep " _
                     & "FROM Decisions_Players D " _
                     & "LEFT JOIN BGOL_Periods P ON D.EnabledFromPeriod = P.Id " _
                     & "WHERE D.IDGame = " & IDGame & " AND D.ID = " & Nni(drDecisionsPlayer("Id"))
                Dim iNumStepOrigin As Integer = Nni(g_DAL.ExecuteScalar(sSQL))

                sSQL = "SELECT ID FROM BGOL_Periods WHERE IDGame = " & IDGameNew & "AND NumStep = " & iNumStepOrigin
                Dim iIDPeriodToLock As Integer = Nni(g_DAL.ExecuteScalar(sSQL))

                ' Inserisco la decisione nella tabella con il nuovo game
                sSQL = "INSERT INTO Decisions_Players (IDDataType, IDGame, IDVariable, VariableLabel, VariableLabelGroup, VariableName, Enabled, EnabledFromPeriod) " _
                     & "SELECT D.IDDataType, " & IDGameNew & ", IDVariable, VariableLabel, VariableLabelGroup, VariableName, Enabled, " & iIDPeriodToLock & " " _
                     & "FROM Decisions_Players D " _
                     & "WHERE D.ID = " & Nni(drDecisionsPlayer("Id"))
                g_DAL.ExecuteNonQuery(sSQL)

                ' Recupero l'ID appena inserito
                sSQL = "SELECT MAX(ID) FROM Decisions_Players "
                Dim iIDDecisionPlayer As Integer = g_DAL.ExecuteScalar(sSQL)

                ' Carico la tabella delle decision player value
                sSQL = "SELECT DPV.Id, DPV.IDDecisionPlayer, DPV.IDPeriod, DPV.IDPlayer, DPV.Simulation, DPV.VariableValue " _
                     & "FROM Decisions_Players_Value DPV " _
                     & "WHERE DPV.IDDecisionPlayer = " & Nni(drDecisionsPlayer("Id")) & " AND ISNULL(DPV.IDPeriod, " & IDPeriodoPrimo & ") = " & IDPeriodoPrimo
                Dim oDTDecisionsPlayersValue As DataTable = g_DAL.ExecuteDataTable(sSQL)
                oDTDecisionsPlayersValue.PrimaryKey = New DataColumn() {oDTDecisionsPlayersValue.Columns("IDDecisionPlayer"), oDTDecisionsPlayersValue.Columns("ID"),
                                                                        oDTDecisionsPlayersValue.Columns("variableValue")}

                For Each drDecisionPlayerValue As DataRow In oDTDecisionsPlayersValue.Rows
                    If Nz(drDecisionPlayerValue("VariableValue")).ToUpper.Contains("LIST OF") Then
                        ' Inserisco la variabile nella tabella value
                        sSQL = "INSERT INTO Decisions_Players_Value (IDDecisionPlayer, IDPeriod, IDPlayer, Simulation, VariableValue) " _
                             & "SELECT " & iIDDecisionPlayer & ", DPV.IDPeriod, DPV.IDPlayer, DPV.Simulation, DPV.VariableValue " _
                             & "FROM Decisions_Players_Value DPV " _
                             & "WHERE DPV.ID = " & Nni(drDecisionPlayerValue("Id"))
                        g_DAL.ExecuteNonQuery(sSQL)

                        ' Recuepero l'ID della variabile appena inserita
                        sSQL = "SELECT MAX(ID) FROM Decisions_Players_Value "
                        Dim iIDDecisionPlayerValue As Integer = Nni(g_DAL.ExecuteScalar(sSQL))

                        ' Recupero i dati relativi al player M&Ud
                        sSQL = "SELECT DPVL.Id, DPVL.IDAge, DPVL.IDDecisionValue, DPVL.IDItem, DPVL.IDPeriod, DPVL.IDPlayer, DPVL.Simulation, DPVL.Value, DPVL.VariableName " _
                             & "FROM Decisions_Players_Value_List DPVL " _
                             & "WHERE IDDecisionValue = " & Nni(drDecisionPlayerValue("ID")) & " AND IDPeriod = " & IDPeriodoPrimo & " AND IDPlayer = " & IDTeamMaster
                        Dim oDTDecisionPlayerValueList As DataTable = g_DAL.ExecuteDataTable(sSQL)
                        oDTDecisionPlayerValueList.PrimaryKey = New DataColumn() {oDTDecisionPlayerValueList.Columns("ID"), oDTDecisionPlayerValueList.Columns("IDDecisionValue"),
                                                                                  oDTDecisionPlayerValueList.Columns("IDPeriod"),
                                                                                  oDTDecisionPlayerValueList.Columns("IDPlayer"), oDTDecisionPlayerValueList.Columns("IDItem")}

                        For Each drDecisionPlayerValueList As DataRow In oDTDecisionPlayerValueList.Rows
                            ' Inserisco il valore per tutti player nella lista del nuovo game
                            ' Mi splitto il nome della variabile per caricare i dati correttamente:
                            Dim sVariableNameSplit As String() = Nz(drDecisionPlayerValueList("VariableName")).Split("_")
                            If sVariableNameSplit.Length > 0 Then
                                If sVariableNameSplit.Length >= 4 Then
                                    sNomeVariabile = sVariableNameSplit(sVariableNameSplit.Length - 2)
                                    sNomeItem = sVariableNameSplit(sVariableNameSplit.Length - 1)
                                Else
                                    sNomeVariabile = ""
                                    sNomeItem = ""
                                End If
                            Else
                                sNomeVariabile = ""
                                sNomeItem = ""
                            End If

                            For iPlayer As Integer = 1 To txtNumPlayers.Text
                                sTeamName = "TeamName_" & CStrSql(txtDescription.Text).Replace(" ", "") & "_" & iPlayer
                                sSQL = "SELECT ID FROM BGOL_Teams WHERE TeamName = '" & CStrSql(sTeamName) & "' "
                                iIDTeamNew = Nni(g_DAL.ExecuteScalar(sSQL))

                                If sNomeVariabile.Trim <> "" AndAlso sNomeItem.Trim <> "" Then
                                    sTeamName = "var_TeamName_" & CStrSql(txtDescription.Text).Replace(" ", "") & "_" & iPlayer & "_" & sNomeVariabile & "_" & sNomeItem
                                Else
                                    sTeamName = "TeamName_" & CStrSql(txtDescription.Text).Replace(" ", "") & "_" & iPlayer
                                End If
                                sSQL = "INSERT INTO Decisions_Players_Value_List (IDAge, IDDecisionValue, IDItem, IDPeriod, IDPlayer, Simulation, Value, VariableName) " _
                                     & "SELECT DPVL.IDAge," & iIDDecisionPlayerValue & ", DPVL.IDItem, " & IDPeriodoPrimoGameNew & ", " & iIDTeamNew & ", DPVL.Simulation, " _
                                     & "DPVL.Value, '" & CStrSql(sTeamName) & "' " _
                                     & "FROM Decisions_Players_Value_List DPVL " _
                                     & "WHERE DPVL.ID = " & drDecisionPlayerValueList("ID") & " AND ISNULL(IDPeriod, " & IDPeriodoPrimo & ") = " & IDPeriodoPrimo
                                g_DAL.ExecuteNonQuery(sSQL)
                            Next

                        Next

                    Else
                        ' Se il player presente è il TEAM Master procedo, altrimenti scarto
                        If Nni(drDecisionPlayerValue("IDPlayer")) = IDTeamMaster Then
                            For iPlayer As Integer = 1 To txtNumPlayers.Text
                                sTeamName = "TeamName_" & CStrSql(txtDescription.Text).Replace(" ", "") & "_" & iPlayer
                                sSQL = "SELECT ID FROM BGOL_Teams WHERE TeamName = '" & CStrSql(sTeamName) & "' "
                                iIDTeamNew = Nni(g_DAL.ExecuteScalar(sSQL))
                                ' Inserisco la variabile nella tabella value
                                sSQL = "INSERT INTO Decisions_Players_Value (IDDecisionPlayer, IDPeriod, IDPlayer, Simulation, VariableValue) " _
                                     & "SELECT " & iIDDecisionPlayer & ", " & IDPeriodoPrimoGameNew & ", " & iIDTeamNew & ", DPV.Simulation, DPV.VariableValue " _
                                     & "FROM Decisions_Players_Value DPV " _
                                     & "WHERE DPV.ID = " & Nni(drDecisionPlayerValue("Id"))
                                g_DAL.ExecuteNonQuery(sSQL)
                            Next

                        End If
                    End If
                Next
            Next

            ' Svuoto i periodi che contengono l'abilitazione = 0
            sSQL = "UPDATE Decisions_Players SET EnabledFromPeriod = NULL WHERE EnabledFromPeriod = 0 AND IDGame = " & IDGame
            g_DAL.ExecuteNonQuery(sSQL)

        Catch ex As Exception
            txtMessageGame.Text = "Error LoadDataDecisionsPlayer -> " & ex.Message & "<br/>" & sSQL
            txtMessageGame.Visible = True
        End Try
    End Sub

    Private Sub LoadDataDecisionsPlayer(IDGame As Integer, IDGameNew As Integer, IDPeriodo As Integer, IDPeriodoGameNew As Integer, IDTeamMeU As Integer, TeamNameMeU As String)
        Dim sSQL As String = ""
        Dim iIDTeamMaster As Integer
        Dim iIDPlayerMaster As Integer
        Dim sTeamNameMaster As String
        Dim sTeamName As String
        Dim iIDTeamNew As Integer
        Dim iIDDecisions_Player_Developer As Integer
        Dim iIDDecisions_Player_Developer_Value As Integer

        Try
            ' Recupero i dati di del primo player, generalmente è quello usato come master
            sSQL = "SELECT MIN(BPTG.IDPLayer) AS IDPlayer , MIN(BPTG.IdTeam) AS IDTeam " _
                 & "FROM BGOL_Players_Teams_Games BPTG " _
                 & "INNER JOIN BGOL_Players_Games BTG ON BPTG.IdGame = BTG.IDGame " _
                 & "AND BPTG.IdPlayer = BTG.IDPlayer " _
                 & "AND BPTG.IdTeam = BTG.IDTeam " _
                 & "WHERE BPTG.IDGame = " & IDGame & " AND BPTG.IDTeam <> " & IDTeamMeU & " AND BTG.IDRole = 'P' "
            Dim dtData As DataTable = g_DAL.ExecuteDataTable(sSQL)
            For Each drData As DataRow In dtData.Rows
                iIDTeamMaster = Nni(drData("IDTeam"))
                iIDPlayerMaster = Nni(drData("IDPlayer"))
            Next
            sSQL = "SELECT TeamName FROM BGOL_Teams WHERE ID = " & iIDTeamMaster
            sTeamNameMaster = Nz(g_DAL.ExecuteScalar(sSQL))

            ' Recupero le variabili che mi servono
            sSQL = "SELECT D.ID, D.IDGame, D.VariableName, D.VariableLabelGroup, D.VariableLabel, D.IDDataType, D.IDVariable, " _
                 & "DPV.ID AS IDDecisionPlayerValue, IDDecisionPlayer, IDPeriod, VariableValue, IDPlayer, Simulation " _
                 & "FROM Decisions_Players D " _
                 & "LEFT JOIN Decisions_Players_Value DPV ON D.Id = DPV.IDDecisionPlayer " _
                 & "WHERE D.IDGame = " & IDGame & " AND ISNULL(DPV.IDPeriod, " & IDPeriodo & ") = " & IDPeriodo
            Dim dtDecisions_Players_Value As DataTable = g_DAL.ExecuteDataTable(sSQL)

            For Each drDecisions_Players_Value As DataRow In dtDecisions_Players_Value.Rows
                ' Recupero l'ID della variabile decisions developer del game nuovo
                sSQL = "SELECT ID FROM Decisions_Players WHERE IDGame = " & IDGameNew & " AND VariableName = '" & Nz(drDecisions_Players_Value("VariableName")) & "' "
                iIDDecisions_Player_Developer = Nni(g_DAL.ExecuteScalar(sSQL))

                sSQL = "SELECT ID FROM Decisions_Players_Value WHERE IDDecisionPlayer = " & iIDDecisions_Player_Developer
                iIDDecisions_Player_Developer_Value = Nni(g_DAL.ExecuteScalar(sSQL))

                ' Controllo il tipo di variabile
                ' Controllo se di tipo LISTA
                If Nz(drDecisions_Players_Value("VariableValue")).ToUpper.Contains("LIST OF") Then
                    ' Recupero i dati 
                    sSQL = "SELECT DPVL.Id, DPVL.IDAge, DPVL.IDDecisionValue, DPVL.IDItem, DPVL.IDPeriod, DPVL.IDPlayer, DPVL.Simulation, DPVL.Value, DPVL.VariableName " _
                         & "FROM Decisions_Players_Value_List DPVL " _
                         & "WHERE IDDecisionValue = " & Nni(drDecisions_Players_Value("IDDecisionPlayerValue")) & " AND IDPeriod = " & IDPeriodo & " AND IDPlayer = " & iIDTeamMaster
                    Dim dtDecisionPlayerValueList As DataTable = g_DAL.ExecuteDataTable(sSQL)
                    dtDecisionPlayerValueList.PrimaryKey = New DataColumn() {dtDecisionPlayerValueList.Columns("ID"),
                                                                            dtDecisionPlayerValueList.Columns("IDDecisionValue"),
                                                                            dtDecisionPlayerValueList.Columns("IDPeriod"),
                                                                            dtDecisionPlayerValueList.Columns("IDPlayer"),
                                                                            dtDecisionPlayerValueList.Columns("IDItem")}

                    For Each drDecisions_players_value_List As DataRow In dtDecisionPlayerValueList.Rows
                        If Nni(drDecisions_players_value_List("IDPlayer")) = iIDTeamMaster Then
                            ' Inserisco il valore per tutti player nella lista del nuovo game
                            For iPlayer As Integer = 1 To txtNumPlayers.Text
                                sTeamName = "TeamName_" & CStrSql(txtDescription.Text).Replace(" ", "") & "_" & iPlayer
                                sSQL = "SELECT ID FROM BGOL_Teams WHERE TeamName = '" & CStrSql(sTeamName) & "' "
                                iIDTeamNew = Nni(g_DAL.ExecuteScalar(sSQL))
                                sSQL = "INSERT INTO Decisions_Players_Value_List (IDAge, IDDecisionValue, IDItem, IDPeriod, IDPlayer, Simulation, Value, VariableName) " _
                                     & "SELECT DPVL.IDAge," & iIDDecisions_Player_Developer_Value & ", DPVL.IDItem, " & IDPeriodoGameNew & ", " & iIDTeamNew & ", DPVL.Simulation, " _
                                     & "DPVL.Value, REPLACE(DPVL.VariableName, '" & sTeamNameMaster & "', '" & CStrSql(sTeamName) & "') " _
                                     & "FROM Decisions_Players_Value_List DPVL " _
                                     & "WHERE DPVL.ID = " & Nni(drDecisions_players_value_List("ID")) & " AND ISNULL(IDPeriod, " & IDPeriodo & ") = " & IDPeriodo
                                g_DAL.ExecuteNonQuery(sSQL)
                            Next
                        End If
                    Next
                Else
                    ' Se il player presente è M&U procedo, altrimenti scarto
                    If Nni(drDecisions_Players_Value("IDPlayer")) = iIDTeamMaster Then
                        For iPlayer As Integer = 1 To txtNumPlayers.Text
                            sTeamName = "TeamName_" & CStrSql(txtDescription.Text).Replace(" ", "") & "_" & iPlayer
                            sSQL = "Select ID FROM BGOL_Teams WHERE TeamName = '" & CStrSql(sTeamName) & "' "
                            iIDTeamNew = Nni(g_DAL.ExecuteScalar(sSQL))
                            ' Inserisco la variabile nella tabella value
                            sSQL = "INSERT INTO Decisions_Players_Value (IDDecisionPlayer, IDPeriod, IDPlayer, Simulation, VariableValue) " _
                                 & "SELECT " & iIDDecisions_Player_Developer & ", " & IDPeriodoGameNew & ", " & iIDTeamNew & ", DPV.Simulation, DPV.VariableValue " _
                                 & "FROM Decisions_Players_Value DPV " _
                                 & "WHERE DPV.ID = " & Nni(drDecisions_Players_Value("IDDecisionPlayerValue"))
                            g_DAL.ExecuteNonQuery(sSQL)
                        Next

                    End If
                End If
            Next
        Catch ex As Exception
            txtMessageGame.Text = "Error LoadDataDecisionsPlayer -> " & ex.Message & "<br/>" & sSQL
            txtMessageGame.Visible = True
        End Try

    End Sub

    Private Sub LoadDataVariableCalculateDefine(IDGame As Integer, IDGameNew As Integer, IDPeriodo As Integer, IDPeriodoGameNew As Integer, IDTeamMeU As Integer, TeamNameMeU As String)
        Dim sSQL As String = ""
        Dim oDTTeams As DataTable = APSUtility.LoadTeamsGame(IDGame)
        Dim iIDTeamMaster As Integer
        Dim iIDPlayerMaster As Integer
        Dim sTeamNameMaster As String
        Dim sTeamName As String
        Dim iIDTeamNew As Integer
        Dim iIDVariableCalculateDefine As Integer

        Try
            ' Recupero i dati di del primo player, generalmente è quello usato come master
            sSQL = "SELECT MIN(BPTG.IDPLayer) AS IDPlayer , MIN(BPTG.IdTeam) AS IDTeam " _
                 & "FROM BGOL_Players_Teams_Games BPTG " _
                 & "INNER JOIN BGOL_Players_Games BTG ON BPTG.IdGame = BTG.IDGame " _
                 & "AND BPTG.IdPlayer = BTG.IDPlayer " _
                 & "AND BPTG.IdTeam = BTG.IDTeam " _
                 & "WHERE BPTG.IDGame = " & IDGame & " AND BPTG.IDTeam <> " & IDTeamMeU & " AND BTG.IDRole = 'P' "
            Dim dtData As DataTable = g_DAL.ExecuteDataTable(sSQL)
            For Each drData As DataRow In dtData.Rows
                iIDTeamMaster = Nni(drData("IDTeam"))
                iIDPlayerMaster = Nni(drData("IDPlayer"))
            Next
            sSQL = "SELECT TeamName FROM BGOL_Teams WHERE ID = " & iIDTeamMaster
            sTeamNameMaster = Nz(g_DAL.ExecuteScalar(sSQL))

            ' Recpero la lista delle variabili calcolate
            sSQL = "SELECT V.Id, V.VariableName, V.IDGame, V.IDCategory, V.VariableLabel, " _
                 & "MIN(C.Id) AS IDVariableCalculateValue, C.IDVariable, C.IDPlayer, C.IDPeriod, C.IDItem, C.IDAge, C.Value, ISNULL(C.Simulation, 0) AS Simulation " _
                 & "FROM Variables_Calculate_Define V " _
                 & "LEFT JOIN Variables_Calculate_Value C ON V.Id = C.IDVariable " _
                 & "WHERE V.IDGame = " & IDGame & " AND IDPeriod = " & IDPeriodo _
                 & " AND (ISNULL(IDPlayer, " & iIDTeamMaster & ") = " & iIDTeamMaster _
                 & "OR ISNULL(IDPlayer, 0) = 0) " _
                 & "GROUP BY V.Id, V.VariableName, V.IDGame, V.IDCategory, V.VariableLabel, C.IDVariable, C.IDPlayer, C.IDPeriod, " _
                 & "C.IDItem, C.IDAge, C.Value, ISNULL(C.Simulation, 0) "
            Dim dtVariablesCalculate As DataTable = g_DAL.ExecuteDataTable(sSQL)
            dtVariablesCalculate.PrimaryKey = New DataColumn() {dtVariablesCalculate.Columns("ID"), dtVariablesCalculate.Columns("VariableName"),
                                                                dtVariablesCalculate.Columns("IDGame"), dtVariablesCalculate.Columns("IDPeriod"),
                                                                dtVariablesCalculate.Columns("IDPlayer"), dtVariablesCalculate.Columns("IDVariableCalculateValue")}

            For Each drVariableCalculate As DataRow In dtVariablesCalculate.Rows
                ' Recupero l'ID della variabile calcolata del gioco attuale
                sSQL = "SELECT ID FROM Variables_Calculate_Define WHERE IDGame = " & IDGameNew & " AND VariableName = '" & Nz(drVariableCalculate("VariableName")) & "' "
                iIDVariableCalculateDefine = Nni(g_DAL.ExecuteScalar(sSQL))

                ' COntrollo che sia una variabile con il player valorizzato
                If Nni(drVariableCalculate("IDPlayer")) > 0 Then
                    ' Inserisco i valori necessari per tutti i player del nuovo game
                    If Nni(drVariableCalculate("IDPlayer")) = iIDTeamMaster Then
                        For iPlayer As Integer = 1 To txtNumPlayers.Text
                            sTeamName = "TeamName_" & CStrSql(txtDescription.Text).Replace(" ", "") & "_" & iPlayer
                            sSQL = "SELECT ID FROM BGOL_Teams WHERE TeamName = '" & CStrSql(sTeamName) & "' "
                            iIDTeamNew = Nni(g_DAL.ExecuteScalar(sSQL))
                            ' Inserisco la variabile nella tabella value
                            sSQL = "INSERT INTO Variables_Calculate_Value (IDVariable, IDPeriod, IDPlayer, Value, IDItem, IDAge) " _
                                 & "SELECT " & iIDVariableCalculateDefine & ", " & IDPeriodoGameNew & ", " & iIDTeamNew & ", Value, IDItem, IDAge " _
                                 & "FROM Variables_Calculate_Value " _
                                 & "WHERE ID = " & Nni(drVariableCalculate("IDVariableCalculateValue"))
                            g_DAL.ExecuteNonQuery(sSQL)
                        Next
                    End If
                Else
                    ' Non ho player associati, procedo con il normale inserimento della riga calcolata
                    sSQL = "INSERT INTO Variables_Calculate_Value (IDVariable, IDPeriod, IDPlayer, Value, IDItem, IDAge) " _
                         & "SELECT " & iIDVariableCalculateDefine & ", " & IDPeriodoGameNew & ", IDPlayer, Value, IDItem, IDAge " _
                         & "FROM Variables_Calculate_Value " _
                         & "WHERE ID = " & Nni(drVariableCalculate("IDVariableCalculateValue"))
                    g_DAL.ExecuteNonQuery(sSQL)
                End If
            Next

        Catch ex As Exception
            txtMessageGame.Text = "Error LoadDataVariableCalculateDefine -> " & ex.Message & "<br/>" & sSQL
            txtMessageGame.Visible = True
        End Try
    End Sub

    Private Sub LoadDataVariables(IDGame As Integer, IDGameNew As Integer, IDGameDelete As Integer)
        Dim sSQL As String
        Dim iIDVariable As Integer

        Try
            ' Cancello gli eventuali dati del vecchio game
            Dim oprmID As New DBParameter("@IDGame", IDGameDelete, DbType.Int16)
            Dim oprmCollection As New DBParameterCollection()
            oprmCollection.Add(oprmID)
            g_DAL.ExecuteNonQuery("sp_Variables_Delete", oprmCollection, CommandType.StoredProcedure)

            sSQL = "SELECT V.DecisionBoss, V.DecisionPlayer, V.DecisionState, V.DefaultValue, V.Id, " _
                 & "V.IDCategory, V.IDDataType, V.IDDefinition, V.IDGame, V.IDGroup, V.ListOfItems, V.OrderVisibility, " _
                 & "V.VariableGroupLabel, V.VariableLabel, V.VariableName " _
                 & "FROM Variables V " _
                 & "WHERE V.IDGame = " & IDGame
            Dim oDTVariables As DataTable = g_DAL.ExecuteDataTable(sSQL)
            oDTVariables.PrimaryKey = New DataColumn() {oDTVariables.Columns("ID")}

            For Each drVariable As DataRow In oDTVariables.Rows
                ' Per ogni variabile procedo all'inserimento e alla relativa traduzione
                sSQL = "INSERT INTO Variables (DecisionBoss, DecisionPlayer, DecisionState, DefaultValue, " _
                     & "IDCategory, IDDataType, IDDefinition, IDGame, IDGroup, ListOfItems, OrderVisibility, " _
                     & "VariableGroupLabel, VariableLabel, VariableName) " _
                     & "SELECT V.DecisionBoss, V.DecisionPlayer, V.DecisionState, V.DefaultValue, " _
                     & "V.IDCategory, V.IDDataType, V.IDDefinition, " & IDGameNew & ", V.IDGroup, V.ListOfItems, V.OrderVisibility, " _
                     & "V.VariableGroupLabel, V.VariableLabel, V.VariableName " _
                     & "FROM Variables V " _
                     & "WHERE V.ID = " & Nni(drVariable("ID"))
                g_DAL.ExecuteNonQuery(sSQL)

                ' Recupero l'id appena inserito
                sSQL = "SELECT MAX(ID) FROM VARIABLES "
                iIDVariable = Nni(g_DAL.ExecuteScalar(sSQL))

                ' Inserisco i dati della vecchia variabile, come traduzione, in quella nuova
                sSQL = "INSERT INTO Variables_Translations (VT.IDLanguage, VT.IDVariable, VT.Translation) " _
                     & "SELECT VT.IDLanguage, " & iIDVariable & ", VT.Translation FROM Variables_Translations VT " _
                     & "WHERE VT.IDVariable = " & Nni(drVariable("ID"))
                g_DAL.ExecuteNonQuery(sSQL)
            Next

        Catch ex As Exception
            txtMessageGame.Text = "Error LoadDataVariables -> " & ex.Message
            txtMessageGame.Visible = True
        End Try
    End Sub

    Private Sub LoadDataItems(IDGame As Integer, IDGameNew As Integer, IDGameDelete As Integer, IDPeriod As Integer)
        Dim sSQL As String
        Dim oDTItemsOrigin As DataTable
        Dim oDTItemsDestination As DataTable

        Try
            ' Cancello gli eventuali dati del vecchio game
            Dim oprmID As New DBParameter("@IDGame", IDGameDelete, DbType.Int16)
            Dim oprmCollection As New DBParameterCollection()
            oprmCollection.Add(oprmID)
            g_DAL.ExecuteNonQuery("sp_Developer_Items_Delete", oprmCollection, CommandType.StoredProcedure)

            sSQL = "Select ID, Item_Title, IDGame, OrderNum FROM Items_Title WHERE IDGame = " & IDGame
            Dim oDTItemsTitle As DataTable = g_DAL.ExecuteDataTable(sSQL)
            For Each drItemsTitle As DataRow In oDTItemsTitle.Rows
                ' Procedo all'inserimento del nuovo item title
                sSQL = "INSERT INTO Items_Title (Item_Title, IDGame, OrderNum) " _
                     & "Select Item_Title, " & IDGameNew & ", OrderNum FROM Items_Title " _
                     & "WHERE ID = " & Nni(drItemsTitle("ID"))
                g_DAL.ExecuteNonQuery(sSQL)
                ' Recupero l'ultimo ID Inserito
                sSQL = "Select MAX(ID) FROM Items_Title "
                Dim iIDItemTitle As Integer = Nni(g_DAL.ExecuteScalar(sSQL))

                ' Inserisco i valori all'interno della tabella Items Sub Title
                sSQL = "INSERT INTO Items_SubTitle (IDItemTitle, SubItemTitle, OrderNum) " _
                     & "Select " & iIDItemTitle & ", SubItemTitle, OrderNum " _
                     & "FROM Items_SubTitle " _
                     & "WHERE IDItemTitle = " & Nni(drItemsTitle("ID"))
                g_DAL.ExecuteNonQuery(sSQL)
                ' Recupero l'ultimo ID inserito
                sSQL = "Select MAX(ID) FROM Items_SubTitle "
                Dim iIDItemSubtitle As Integer = Nni(g_DAL.ExecuteScalar(sSQL))

                ' Inserisco gli items nella tabella
                sSQL = "Select TOP 1 ID FROM Items_SubTitle WHERE IDItemTitle = " & Nni(drItemsTitle("ID"))
                Dim iIDSubItemOLD As Integer = Nni(g_DAL.ExecuteScalar(sSQL))
                sSQL = "INSERT INTO Items_Period_Variables (IDSubItem, IDPeriod, IDDataType, VariableLabel, VariableValue, VariableName, OrderNum) " _
                     & "Select " & iIDItemSubtitle & ", IDPeriod, IDDataType, VariableLabel, VariableValue, VariableName, OrderNum " _
                     & "FROM Items_Period_Variables " _
                     & "WHERE IDSubItem = " & iIDSubItemOLD
                g_DAL.ExecuteNonQuery(sSQL)

            Next

            oDTItemsOrigin = LoadItemsGame(IDGame, "")
            oDTItemsDestination = LoadItemsGame(IDGameNew, "")

            ' Aggiorno gli items già creati nel game
            For Each drItem As DataRow In oDTItemsOrigin.Rows
                ' CIclo sugli item e sulle tabelle da sistemare
                Dim drItems() As DataRow = oDTItemsDestination.Select("VariableName = '" & Nz(drItem("VariableName")) & "' ")
                If drItems.Length > 0 Then
                    ' Aggiorno la tabella Decisions_Developer_Value_List
                    sSQL = "UPDATE Decisions_Developer_Value_List SET IDItem = " & drItems(0)("IDVariable") _
                         & " WHERE IDPeriod >= " & IDPeriod & " AND IDItem = " & drItem("IDVariable")
                    g_DAL.ExecuteNonQuery(sSQL)

                    ' Aggiorno la tabella Decisions_Developer_Value
                    sSQL = "UPDATE Decisions_Developer_Value SET IDItem = " & drItems(0)("IDVariable") _
                         & " WHERE IDPeriod >= " & IDPeriod & " AND IDItem = " & drItem("IDVariable")
                    g_DAL.ExecuteNonQuery(sSQL)

                    ' Aggiorno la tabella Variables_State_Value_List
                    sSQL = "UPDATE Variables_State_Value_List SET IDItem = " & drItems(0)("IDVariable") _
                         & " WHERE IDPeriod >= " & IDPeriod & " AND IDItem = " & drItem("IDVariable")
                    g_DAL.ExecuteNonQuery(sSQL)

                    ' Aggiorno la tabella Variables_State_Value
                    sSQL = "UPDATE Variables_State_Value SET IDItem = " & drItems(0)("IDVariable") _
                         & " WHERE IDPeriod >= " & IDPeriod & " AND IDItem = " & drItem("IDVariable")
                    g_DAL.ExecuteNonQuery(sSQL)

                    ' Aggiorno la tabella Decisions_Players_Value_List
                    sSQL = "UPDATE Decisions_Players_Value_List SET IDItem = " & drItems(0)("IDVariable") _
                         & " WHERE IDPeriod >= " & IDPeriod & " AND IDItem = " & drItem("IDVariable")
                    g_DAL.ExecuteNonQuery(sSQL)
                End If
            Next
        Catch ex As Exception
            txtMessageGame.Text = "Error LoadDataItems -> " & ex.Message
            txtMessageGame.Visible = True
        End Try
    End Sub

    Private Sub LoadVariablesGroup(IDGame As Integer, IDGameNExt As Integer)
        Dim sSQL As String

        Try
            sSQL = "INSERT INTO Variables_Groups " _
                 & "SELECT ID, GroupDescription, " & IDGameNExt & " FROM Variables_Groups WHERE IDGame = " & IDGame
            g_DAL.ExecuteNonQuery(sSQL)
        Catch ex As Exception
            txtMessageGame.Text = "Error LoadVariablesGroup -> " & ex.Message
            txtMessageGame.Visible = True
        End Try
    End Sub

    Private Sub LoadMenuItem(IDGame As Integer, IDGameNew As Integer)
        Try
            Dim oprmIDGameOrigin As New DBParameter("@IDGameOrigin", IDGame, DbType.Int16)
            Dim oprmIDGameDestination As New DBParameter("@IDGameDestination", IDGameNew, DbType.Int16)

            Dim oprmCollection As New DBParameterCollection()
            oprmCollection.Add(oprmIDGameOrigin)
            oprmCollection.Add(oprmIDGameDestination)

            g_DAL.ExecuteNonQuery("sp_MenuGame_Create", oprmCollection, CommandType.StoredProcedure)

        Catch ex As Exception
            txtMessageGame.Text = "Error LoadMenuItem -> " & ex.Message
            txtMessageGame.Visible = True
        End Try
    End Sub

    Private Sub btnHide_Click(sender As Object, e As EventArgs) Handles btnHide.Click
        mpeMain.Hide()
        ClearPanel()
    End Sub

    Private Sub chkPeriodoFinoAl_Click(sender As Object, e As EventArgs) Handles chkPeriodoFinoAl.Click
        If chkPeriodoFinoAl.Checked Then
            cboPeriodoFinoAl.Enabled = True
            chkPrimoPeriodo.Checked = False

            ' Procedo con il caricamento dei periodi direttamente dal gioco sorgente
            Dim sSQL As String
            sSQL = "SELECT BGP.IDPeriodo, BP.Descrizione " _
                 & "FROM BGOL_Games_Periods BGP " _
                 & "INNER JOIN BGOL_Periods BP ON BGP.IDPeriodo = BP.Id " _
                 & "WHERE BGP.IDGame = " & Nni(cboGames.SelectedValue, 0) & " " _
                 & "ORDER BY BP.NumStep DESC "

            cboPeriodoFinoAl.DataSource = g_DAL.ExecuteDataTable(sSQL)
            cboPeriodoFinoAl.DataValueField = "IDPeriodo"
            cboPeriodoFinoAl.DataTextField = "Descrizione"
            cboPeriodoFinoAl.DataBind()

            cboPeriodoFinoAl.SelectedValue = -1
        End If
    End Sub

    Private Sub chkPrimoPeriodo_Click(sender As Object, e As EventArgs) Handles chkPrimoPeriodo.Click
        If chkPrimoPeriodo.Checked Then
            chkPeriodoFinoAl.Checked = False
            cboPeriodoFinoAl.Enabled = False
        End If
    End Sub

    Private Sub LoadMenuTranslation(IDGameOrigin As Integer, IDGameDestination As Integer)
        Dim sSQL As String
        Dim dtMenuMasterNew As DataTable
        Dim dtMenuMasterTranslation As DataTable
        Dim dtMenuDetailOriginTranslation As DataTable

        Try
            ' Recupero tutte le voci di menu del gioco nuovo
            sSQL = "SELECT * FROM Menu_Master WHERE IDGame = " & IDGameDestination
            dtMenuMasterNew = g_DAL.ExecuteDataTable(sSQL)
            ' Per ogni riga recupero la corrispondente tradotta nel game di origine e la inserisco nel nuovo game
            For Each drMenuMaster As DataRow In dtMenuMasterNew.Rows
                sSQL = "SELECT MM.ID, MM.Description, MMT.ID, MMT.IDLanguage, MMT.Translation FROM Menu_Master MM " _
                     & "LEFT JOIN Menu_Master_Translation MMT ON MM.ID = MMT.IDMenu " _
                     & "WHERE IDGame = " & IDGameOrigin & " AND MM.Description = '" & Nz(drMenuMaster("Description")) & "' "
                dtMenuMasterTranslation = g_DAL.ExecuteDataTable(sSQL)
                For Each drMenuMasterTranslation As DataRow In dtMenuMasterTranslation.Rows
                    sSQL = "INSERT INTO Menu_Master_Translation (IDMenu, IDLanguage, Translation) VALUES (" _
                         & Nni(drMenuMaster("ID")) & ", " & Nni(drMenuMasterTranslation("IDLanguage")) & ", '" & Nz(drMenuMasterTranslation("Translation")) & "') "
                    g_DAL.ExecuteNonQuery(sSQL)
                Next
            Next

            ' Faccio lo stesso giro per i sotto menu
            ' Carico tutti i menu di origine, poi recupero il corrispettivo in quello di destinazione ed eseguo l'inserimento in archivio
            sSQL = "SELECT MD.Id, MD.IdMenuMaster, MD.Description, MD.OrderNum, MD.RefPage, MD.Visible, MD.IsVisibleTo, " _
                 & "MDT.ID, MDT.IDLanguage, MDT.IDMenu, MDT.Translation " _
                 & "FROM Menu_Master MM " _
                 & "LEFT JOIN Menu_Detail MD ON MM.Id = MD.IdMenuMaster " _
                 & "LEFT JOIN Menu_Detail_Translation MDT ON MD.Id = MDT.IDMenu " _
                 & "WHERE MM.IDGame = " & IDGameDestination
            dtMenuDetailOriginTranslation = g_DAL.ExecuteDataTable(sSQL)
            For Each drMenuDetailOriginTranslation As DataRow In dtMenuDetailOriginTranslation.Rows
                sSQL = "SELECT DISTINCT MDT.ID, MDT.IDMenu, MDT.IDLanguage, MDT.Translation, MD.Description " _
                     & "FROM Menu_Detail MD " _
                     & "LEFT JOIN Menu_Detail_Translation MDT ON MD.Id = MDT.IDMenu " _
                     & "WHERE RefPage = '" & Nz(drMenuDetailOriginTranslation("RefPage")) & "' " _
                     & "AND IdMenuMaster IN(SELECT ID FROM Menu_Master WHERE IDGame = " & IDGameOrigin & ") " _
                     & "AND MD.Description = '" & Nz(drMenuDetailOriginTranslation("Description")) & "' "
                Dim dtTranslation As DataTable = g_DAL.ExecuteDataTable(sSQL)

                For Each drTranslation As DataRow In dtTranslation.Rows
                    sSQL = "INSERT INTO Menu_Detail_Translation (IDMenu, IDLanguage, Translation) VALUES (" _
                         & Nni(drMenuDetailOriginTranslation("ID")) & ", " & Nni(drTranslation("IDLanguage")) & ", '" _
                         & Nz(drTranslation("Translation")) & "') "
                    ' Inserisco il dato solo se ho la traduzione
                    If Nz(drTranslation("Translation")) <> "" Then
                        g_DAL.ExecuteNonQuery(sSQL)
                    End If
                Next

            Next

        Catch ex As Exception
            txtMessageGame.Text = "Error LoadMenuTranslation -> " & ex.Message
            txtMessageGame.Visible = True
        End Try
    End Sub

End Class
