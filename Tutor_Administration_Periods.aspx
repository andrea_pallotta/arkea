﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Tutor_Administration_Periods.aspx.vb" Inherits="Tutor_Administration_Periods" %>

<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="cntMain" ContentPlaceHolderID="MainContent" runat="Server">

    <link href="Content/GridTelerik.css" rel="stylesheet" type="text/css" />

    <telerik:RadWindowManager RenderMode="Lightweight" runat="server" ID="RadWindowManager1">
    </telerik:RadWindowManager>

    <asp:UpdatePanel runat="server" ID="pnlMain" UpdateMode="Conditional">
        <ContentTemplate>
            <h2>
                <asp:Label ID="lblWelcome" runat="server" Text="Tutor/Manage periods" />
            </h2>

            <div class="clr"></div>

            <div id="subTitle">
                <div id="subLeft" style="float: left; width: 30%;">
                    <h3>
                        <asp:Label ID="lblTitle" runat="server" Text="Manage game periods"></asp:Label>
                    </h3>
                </div>

                <div id="subRight" style="float: right; padding-right: 5px;">
                    <div id="divNewVariable" style="margin-top: 10px; margin-bottom: 5px; text-align: right;">
                        <telerik:RadButton ID="btnNewPeriod" runat="server" Text="New period"
                            RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                        </telerik:RadButton>
                    </div>
                </div>
            </div>

            <div class="clr"></div>

            <div class="row" style="padding-bottom: 5px;">
                <asp:Panel ID="pnlTable" runat="server">
                    <telerik:RadGrid ID="grdPeriods" runat="server" RenderMode="Lightweight" AllowPaging="True" AllowSorting="True" EnableLinqExpressions="false"
                        OnNeedDataSource="grdPeriods_NeedDataSource" Width="670px" AllowFilteringByColumn="True" Culture="it-IT" GroupPanelPosition="Top" PageSize="150">
                        <MasterTableView AutoGenerateColumns="false" TableLayout="Fixed" CssClass="RadGrid_WebBlue"
                            DataKeyNames="ID, Descrizione, DataInizio, DataFine, NumStep, DateHourEndEffective, DataFineEffettiva, DateStart, DateEnd">

                            <Columns>
                                <telerik:GridButtonColumn CommandName="Modify" Text="Edit" UniqueName="EditColumn"
                                    ButtonType="ImageButton" ImageUrl="~/Content/GridTelerik/Edit.gif">
                                    <HeaderStyle Width="30px" />
                                </telerik:GridButtonColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="Descrizione" HeaderText="Period" UniqueName="Descrizione"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                    <HeaderStyle Width="150px" />
                                </telerik:GridBoundColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="DataInizio" HeaderText="Date start" UniqueName="DataInizio"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                    <HeaderStyle Width="95px" />
                                </telerik:GridBoundColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="DataFine" HeaderText="Date end" UniqueName="Datafine"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                    <HeaderStyle Width="95px" />
                                </telerik:GridBoundColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="NumStep" HeaderText="Num. step" UniqueName="NumStep"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                    <HeaderStyle Width="95px" />
                                </telerik:GridBoundColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="DataFineEffettiva" HeaderText="Date time end eff." UniqueName="DataFineEffettiva"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                    <HeaderStyle Width="140px" />
                                </telerik:GridBoundColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridButtonColumn CommandName="Delete" Text="Delete" UniqueName="Delete" ConfirmText="Are you sure you want to delete de selected item?"
                                    ButtonType="ImageButton" ImageUrl="Images/Delete16.png" Resizable="false">
                                    <HeaderStyle Width="30px" />
                                </telerik:GridButtonColumn>
                            </Columns>

                        </MasterTableView>
                    </telerik:RadGrid>
                </asp:Panel>
            </div>

            <%--------------------------------------------------------------------------------------------------------------------------------------------------------------------%>
            <%-- PANNELLO DI GESTIONE DEI PERIODI --%>
            <%--------------------------------------------------------------------------------------------------------------------------------------------------------------------%>
            <div class="row">
                <input id="hdfPeriod" type="hidden" name="hddclickPeriod" runat="server" />
                <ajaxToolkit:ModalPopupExtender ID="mpePeriod" runat="server" PopupControlID="pnlPeriod" TargetControlID="hdfPeriod"
                    BackgroundCssClass="modalBackground" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>

                <asp:Panel runat="server" ID="pnlPeriod" CssClass="modalPopup" Style="display: none;">
                    <asp:UpdatePanel ID="pnlUpdatePeriod" runat="server" UpdateMode="Conditional">

                        <ContentTemplate>

                            <div class="pnlheader">
                                <asp:Label ID="lblPeriodTitle" runat="server" Text="Manage period"></asp:Label>
                            </div>

                            <div class="pnlbody">
                                <!-- GESTIONE DELLA TABELLA CON DIV -->
                                <div class="divTable">
                                    <div class="divTableBody">
                                        <div class="divTableRow">
                                            <div class="divTableCell" style="width: 200px;">
                                                <asp:Label ID="lblDescrizione" runat="server" Text="Period"></asp:Label>
                                            </div>
                                            <div class="divTableCell" style="width: 580px;">
                                                <telerik:RadTextBox ID="txtDescrizione" runat="server" RenderMode="Lightweight" ClientIDMode="Static" Resize="None">
                                                </telerik:RadTextBox>
                                            </div>
                                        </div>

                                        <div class="divTableRow">
                                            <div class="divTableCell" style="width: 200px;">
                                                <asp:Label ID="lblDataInizio" runat="server" Text="Date start"></asp:Label>
                                            </div>
                                            <div class="divTableCell" style="width: 580px;">
                                                <asp:TextBox ID="txtDataInizio" runat="server" RenderMode="Lightweight" ClientIDMode="Static" Resize="None">
                                                </asp:TextBox>
                                            </div>
                                            <ajaxToolkit:MaskedEditExtender TargetControlID="txtDataInizio"
                                                ID="meDataInizio"
                                                runat="server"
                                                Mask="99/99/9999"
                                                MessageValidatorTip="false"
                                                MaskType="Date" CultureName="it-IT"
                                                AcceptAMPM="false" />

                                            <ajaxToolkit:MaskedEditValidator ID="mevDataInizio" runat="server"
                                                ControlExtender="meDataInizio"
                                                ControlToValidate="txtDataInizio"
                                                InvalidValueMessage="Date is invalid"
                                                Display="Dynamic"
                                                TooltipMessage="" />
                                        </div>

                                        <div class="divTableRow">
                                            <div class="divTableCell" style="width: 200px;">
                                                <asp:Label ID="lblDataFine" runat="server" Text="Date end"></asp:Label>
                                            </div>
                                            <div class="divTableCell" style="width: 580px;">
                                                <asp:TextBox ID="txtDataFine" runat="server" RenderMode="Lightweight" ClientIDMode="Static" Resize="None">
                                                </asp:TextBox>
                                            </div>
                                            <ajaxToolkit:MaskedEditExtender TargetControlID="txtDataFine"
                                                ID="meDataFine"
                                                runat="server"
                                                Mask="99/99/9999"
                                                MessageValidatorTip="false"
                                                MaskType="Date" CultureName="it-IT"
                                                AcceptAMPM="false" />

                                            <ajaxToolkit:MaskedEditValidator ID="mevDataFine" runat="server"
                                                ControlExtender="meDataFine"
                                                ControlToValidate="txtDataFine"
                                                InvalidValueMessage="Date is invalid"
                                                Display="Dynamic"
                                                TooltipMessage="" />
                                        </div>

                                        <div class="divTableRow">
                                            <div class="divTableCell" style="width: 200px;">
                                                <asp:Label ID="lblNumStep" runat="server" Text="Num. step"></asp:Label>
                                            </div>
                                            <div class="divTableCell" style="width: 580px;">
                                                <telerik:RadTextBox ID="txtNumStep" runat="server" RenderMode="Lightweight" ClientIDMode="Static" Resize="None">
                                                </telerik:RadTextBox>
                                            </div>
                                        </div>

                                        <div class="divTableRow">
                                            <div class="divTableCell" style="width: 200px;">
                                                <asp:Label ID="lblDateHourEndEffective" runat="server" Text="Date hour end effective"></asp:Label>
                                            </div>
                                            <div class="divTableCell" style="width: 580px;">
                                                <asp:TextBox ID="txtDateHourEndEffective" runat="server" RenderMode="Lightweight" ClientIDMode="Static" Resize="None">
                                                </asp:TextBox>

                                            </div>
                                            <ajaxToolkit:MaskedEditExtender TargetControlID="txtDateHourEndEffective"
                                                ID="meDateHourEffective"
                                                runat="server"
                                                Mask="99/99/9999 99:99"
                                                MessageValidatorTip="false"
                                                MaskType="DateTime" CultureName="it-IT"
                                                AcceptAMPM="false" />

                                            <ajaxToolkit:MaskedEditValidator ID="mevDatehourEffective" runat="server"
                                                ControlExtender="meDateHourEffective"
                                                ControlToValidate="txtDateHourEndEffective"
                                                InvalidValueMessage="Date is invalid"
                                                Display="Dynamic"
                                                TooltipMessage="" />
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="pnlfooter">
                                <telerik:RadButton ID="btnPeriodClose" runat="server" Text="Close" Width="80px"
                                    RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                                </telerik:RadButton>

                                <telerik:RadButton ID="btnPeriodSave" runat="server" Text="Save" Width="80px"
                                    RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                                </telerik:RadButton>

                                <div class="row" style="min-height: 20px;"></div>

                                <div class="row">
                                    <asp:PlaceHolder runat="server" ID="MessagePeriod" Visible="false">
                                        <p class="text-danger">
                                            <asp:Literal runat="server" ID="MessagePeriodText" />
                                        </p>
                                    </asp:PlaceHolder>
                                </div>
                            </div>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>

            </div>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
