﻿Imports Telerik.Web.UI
Imports System.Data
Imports APS
Imports APPCore.Utility
Imports DALC4NET

Partial Class Tutor_Administration_Periods
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster

    Protected Sub grdPeriods_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs)
        grdPeriods.DataSource = LoadPeriods()
    End Sub

    Private Sub ClearPanelPeriod()
        txtDescrizione.Text = ""
        txtDataFine.Text = ""
        txtDataInizio.Text = ""
        txtDateHourEndEffective.Text = ""
        txtNumStep.Text = ""
    End Sub

    Private Sub btnNewPeriod_Click(sender As Object, e As EventArgs) Handles btnNewPeriod.Click
        Session.Remove("PeriodSelected")
        Session.Remove("PeriodID")
        ClearPanelPeriod()
        mpePeriod.Show()
    End Sub

    Private Sub Tutor_Administration_Periods_Load(sender As Object, e As EventArgs) Handles Me.Load
        m_SessionData = Session("SessionData")

        If IsNothing(m_SessionData) Then
            PageRedirect("~/Account/Login.aspx", "", "")
        End If

        If Not Page.IsPostBack Then
            ClearPanelPeriod()
            LoadPeriods()
        End If

    End Sub

    Private Function LoadPeriods() As DataTable
        Dim sSQL As String
        Dim oDAL As New DBHelper

        If IsNothing(m_SessionData) Then
            PageRedirect("~/Account/Login.aspx", "", "")
        End If

        sSQL = "SELECT ID, Descrizione, '' AS DataInizio, '' AS DataFine, DateStart, DateEnd, NumStep, DateHourEndEffective, '' AS DataFineEffettiva " _
             & "FROM BGOL_Periods " _
             & "WHERE IDGame = " & Session("IDGame") _
             & "ORDER BY NumStep ASC "

        Dim dtPeriods As DataTable = oDAL.ExecuteDataTable(sSQL)

        ' Ciclo sulla tabella dei periodi per la sistemazione dei valori da mostrare, le date e ore
        For Each drPeriod As DataRow In dtPeriods.Rows
            If Nz(drPeriod("DateStart")) <> "" Then
                drPeriod("DataInizio") = Nz(ANSIToDate(drPeriod("DateStart"))).Substring(0, 10)
            End If

            If Nz(drPeriod("DateEnd")) <> "" Then
                drPeriod("DataFine") = Nz(ANSIToDate(drPeriod("DateEnd"))).Substring(0, 10)
            End If

            If Nz(drPeriod("DateHourEndEffective")) <> "" Then
                drPeriod("DataFineEffettiva") = Nz(ANSIToDate(drPeriod("DateHourEndEffective"), True))
            End If

        Next
        dtPeriods.AcceptChanges()

        SQLConnClose(oDAL.GetConnObject)
        oDAL = Nothing

        Return dtPeriods

    End Function

    Private Sub pnlUpdatePeriod_PreRender(sender As Object, e As EventArgs) Handles pnlUpdatePeriod.PreRender
        Dim oPeriod As GridDataItem = DirectCast(Session("PeriodSelected"), GridDataItem)

        ' Procedo con il caricamento dei dati da inserire nel panello
        ' Recupero l'id della variabile selezionata
        If Not oPeriod Is Nothing Then
            lblPeriodTitle.Text = "Manage - " & oPeriod.GetDataKeyValue("Descrizione").ToString

            Session("PeriodID") = oPeriod.GetDataKeyValue("ID")

            txtDescrizione.Text = Nz(oPeriod.GetDataKeyValue("Descrizione"))
            txtDataInizio.Text = Nz(oPeriod.GetDataKeyValue("DataInizio"))
            txtDataFine.Text = Nz(oPeriod.GetDataKeyValue("DataFine"))
            txtNumStep.Text = Nni(oPeriod.GetDataKeyValue("NumStep"))
            txtDateHourEndEffective.Text = Nz(oPeriod.GetDataKeyValue("DateHourEndEffective"))

            ' Genero lo step ultimo da usa, resta comunque modificabile
            Dim sSQL As String = ""
        End If

    End Sub

    Private Sub btnPeriodClose_Click(sender As Object, e As EventArgs) Handles btnPeriodClose.Click
        ClearPanelPeriod()
        mpePeriod.Hide()
    End Sub

End Class
