﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Tutor_Administration_Players.aspx.vb" Inherits="Tutor_Administration_Players_Define" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="cntMain" ContentPlaceHolderID="MainContent" runat="Server" ClientIDMode="Static">
    <link href="Content/GridTelerik.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        function confirmDelete(arg) {
            if (arg) {
                __doPostBack("", "");
            }
        }
    </script>

    <telerik:RadWindowManager RenderMode="Lightweight" runat="server" ID="RadWindowManager1">
    </telerik:RadWindowManager>

    <asp:UpdatePanel runat="server" ID="pnlMain" UpdateMode="Conditional">
        <ContentTemplate>
            <h2>
                <asp:Label ID="lblWelcome" runat="server" Text="Tutor/Manage players" />
            </h2>

            <div class="clr"></div>

            <div id="subTitle">
                <div id="subLeft" style="float: left; width: 20%;">
                    <h3>
                        <asp:Label ID="lblTitle" runat="server" Text="Manage players"></asp:Label>
                    </h3>
                </div>

                <div id="subRight" style="float: right; padding-right: 5px;">
                    <div id="divNewVariable" style="margin-top: 10px; margin-bottom: 5px; text-align: right;">
                        <telerik:RadButton ID="btnEditPlayer" runat="server" Text="New player"
                            RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                        </telerik:RadButton>
                    </div>
                </div>
            </div>

            <div class="clr"></div>

            <div class="divTableRow">
                <div class="divTableCell">
                    <telerik:RadCheckBox ID="chkShowAllPlayers" runat="server" AutoPostBack="true" Text="Show all players">
                    </telerik:RadCheckBox>
                </div>
            </div>

            <div class="row" style="padding-bottom: 5px;">
                <asp:Panel ID="pnlTable" runat="server">
                    <telerik:RadGrid ID="grdPlayers" runat="server" RenderMode="Lightweight" AllowPaging="True" AllowSorting="True" EnableLinqExpressions="false"
                        OnNeedDataSource="grdPlayers_NeedDataSource" Width="670px" AllowFilteringByColumn="True" Culture="it-IT" GroupPanelPosition="Top" PageSize="150">
                        <MasterTableView AutoGenerateColumns="false" TableLayout="Fixed" CssClass="RadGrid_WebBlue"
                            DataKeyNames="Id">

                            <Columns>
                                <telerik:GridButtonColumn CommandName="Modify" Text="Edit" UniqueName="EditColumn"
                                    ButtonType="ImageButton" ImageUrl="~/Content/GridTelerik/Edit.gif">
                                    <HeaderStyle Width="20px" />
                                </telerik:GridButtonColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="Nome" HeaderText="Nome" UniqueName="Nome"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                    <HeaderStyle Width="75px" />
                                </telerik:GridBoundColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="Cognome" HeaderText="Cognome" UniqueName="Cognome"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                    <HeaderStyle Width="75px" />
                                </telerik:GridBoundColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="Username" HeaderText="Username" UniqueName="Username"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                    <HeaderStyle Width="75px" />
                                </telerik:GridBoundColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="Email" HeaderText="Email" UniqueName="Email"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                    <HeaderStyle Width="100px" />
                                </telerik:GridBoundColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridButtonColumn CommandName="Delete" Text="Delete" UniqueName="Delete" ConfirmText="Are you sure you want to delete de selected item?"
                                    ButtonType="ImageButton" ImageUrl="Images/Delete16.png" Resizable="false">
                                    <HeaderStyle Width="20px" />
                                </telerik:GridButtonColumn>
                            </Columns>

                            <%--COLONNE NASCOSTE--%>
                            <Columns>
                                <telerik:GridBoundColumn DataField="Id" HeaderText="Id" UniqueName="Id" Visible="false" />
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="Attivo" HeaderText="Attivo" UniqueName="Attivo" Visible="false" />
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="DefaultLanguage" HeaderText="DefaultLanguage" UniqueName="DefaultLanguage" Visible="false" />
                            </Columns>

                        </MasterTableView>
                    </telerik:RadGrid>

                </asp:Panel>
            </div>

        </ContentTemplate>

    </asp:UpdatePanel>

    <div class="clr"></div>

    <div class="row">

        <asp:UpdatePanel ID="pnlPlayerUpdate" runat="server" UpdateMode="Conditional">
            <ContentTemplate>

                <input id="hdnPanelVariable" type="hidden" name="hddClick" runat="server" />

                <ajaxToolkit:ModalPopupExtender ID="mpeMain" runat="server" PopupControlID="pnlPlayerEdit" TargetControlID="hdnPanelVariable"
                    BackgroundCssClass="modalBackground" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>

                <asp:Panel runat="server" ID="pnlPlayerEdit" CssClass="modalPopup">

                    <div class="pnlheader">
                        <asp:Label ID="lblPanelTitle" runat="server" Text="Manage player"></asp:Label>
                    </div>

                    <div class="pnlbody">
                        <!-- GESTIONE DELLA TABELLA CON DIV -->
                        <div class="divTable">
                            <div class="divTableBody">

                                <div class="divTableRow">
                                    <div class="divTableCell" style="width: 200px;">Nome</div>
                                    <div class="divTableCell" style="width: 580px;">
                                        <telerik:RadTextBox ID="txtNome" runat="server" RenderMode="Lightweight" ClientIDMode="Static"></telerik:RadTextBox>
                                    </div>
                                </div>

                                <div class="divTableRow">
                                    <div class="divTableCell">Cognome</div>
                                    <div class="divTableCell">
                                        <telerik:RadTextBox ID="txtCognome" runat="server" RenderMode="Lightweight" ClientIDMode="Static"></telerik:RadTextBox>
                                    </div>
                                </div>

                                <div class="divTableRow">
                                    <div class="divTableCell">Username</div>
                                    <div class="divTableCell">
                                        <telerik:RadTextBox ID="txtUsername" runat="server" RenderMode="Lightweight" ClientIDMode="Static"></telerik:RadTextBox>
                                    </div>
                                </div>

                                <div class="divTableRow">
                                    <div class="divTableCell">Email</div>
                                    <div class="divTableCell">
                                        <telerik:RadTextBox ID="txtEmail" runat="server" RenderMode="Lightweight" ClientIDMode="Static"></telerik:RadTextBox>
                                    </div>
                                </div>

                                <div class="divTableRow">
                                    <div class="divTableCell">Attivo</div>
                                    <div class="divTableCell">
                                        <telerik:RadCheckBox ID="chkAttivo" runat="server" Text="" AutoPostBack="false" RenderMode="Lightweight"></telerik:RadCheckBox>
                                    </div>
                                </div>

                                <div class="divTableRow">
                                    <div class="divTableCell">Default language</div>
                                    <div class="divTableCell">
                                        <telerik:RadComboBox ID="cboLanguage" runat="server" EmptyMessage="<Select language...>"
                                            RenderMode="Lightweight" RenderingMode="Full" ZIndex="1000000" ClientIDMode="Static">
                                        </telerik:RadComboBox>
                                    </div>
                                </div>

                                <div class="divTableRow">
                                    <div class="divTableCell">Team</div>
                                    <div class="divTableCell">
                                        <telerik:RadComboBox ID="cboTeam" runat="server" EmptyMessage="<Select team...>"
                                            RenderMode="Lightweight" RenderingMode="Full" ZIndex="1000000" ClientIDMode="Static">
                                        </telerik:RadComboBox>
                                        <telerik:RadButton ID="btnAddTeam" runat="server" Text="New..." Width="80px"
                                            RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static" Font-Size="Smaller">
                                        </telerik:RadButton>

                                        <telerik:RadTextBox ID="txtTeamName" runat="server" RenderMode="Lightweight" ClientIDMode="Static"></telerik:RadTextBox>
                                    </div>
                                </div>

                                <div class="divTableRow">
                                    <div class="divTableCell">Password</div>
                                    <div class="divTableCell">
                                        <telerik:RadTextBox ID="txtPassword" runat="server" RenderMode="Lightweight" ClientIDMode="Static"></telerik:RadTextBox>
                                    </div>
                                </div>

                                <div class="divTableRow">
                                    <div class="divTableCell">Role</div>
                                    <div class="divTableCell">
                                        <telerik:RadComboBox ID="cboRole" runat="server" EmptyMessage="<Select Role...>"
                                            RenderMode="Lightweight" RenderingMode="Full" ZIndex="1000000" ClientIDMode="Static">
                                        </telerik:RadComboBox>
                                    </div>
                                </div>

                                <div class="divTableRow">
                                    <div class="divTableCell">Game to participate</div>
                                    <div class="divTableCell">
                                        <telerik:RadComboBox ID="cboGames" runat="server" EmptyMessage="<Select game...>"
                                            RenderMode="Lightweight" RenderingMode="Full" ZIndex="1000000" ClientIDMode="Static">
                                        </telerik:RadComboBox>

                                        <telerik:RadButton ID="btnAddGame" runat="server" Text="Add..." Width="80px"
                                            RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static" Font-Size="Smaller">
                                        </telerik:RadButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div style="width: 100%; margin-left: 1%; margin-right: 1%">
                        <telerik:RadGrid ID="grdPlayersGames" runat="server" RenderMode="Lightweight" AllowPaging="True" AllowSorting="True" EnableLinqExpressions="false"
                            OnNeedDataSource="grdPlayersGames_NeedDataSource" Width="98%" AllowFilteringByColumn="False" Culture="it-IT" GroupPanelPosition="Top">
                            <MasterTableView AutoGenerateColumns="false" TableLayout="Fixed" CssClass="RadGrid_WebBlue" DataKeyNames="Id, IDGame, IDRole">

                                <Columns>
                                    <telerik:GridBoundColumn DataField="Game_name" HeaderText="Game" UniqueName="Game"
                                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                        <HeaderStyle Width="75px" />
                                    </telerik:GridBoundColumn>
                                </Columns>

                                <Columns>
                                    <telerik:GridBoundColumn DataField="Role_desc" HeaderText="Role" UniqueName="Role"
                                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                        <HeaderStyle Width="75px" />
                                    </telerik:GridBoundColumn>
                                </Columns>

                                <Columns>
                                    <telerik:GridButtonColumn CommandName="Delete" Text="Delete" UniqueName="Delete" ConfirmText="Are you sure you want to delete de selected item?"
                                        ButtonType="ImageButton" ImageUrl="Images/Delete16.png" Resizable="false">
                                        <HeaderStyle Width="20px" />
                                    </telerik:GridButtonColumn>
                                </Columns>

                            </MasterTableView>
                        </telerik:RadGrid>
                    </div>

                    <div class="pnlfooter">
                        <telerik:RadButton ID="btnEditPlayerClose" runat="server" Text="Close" Width="80px"
                            RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                        </telerik:RadButton>

                        <telerik:RadButton ID="btnEditPlayerSave" runat="server" Text="Save" Width="80px"
                            RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                        </telerik:RadButton>
                    </div>

                    <div style="text-align: center;">
                        <asp:Label runat="server" ID="txtMessageVariable" Visible="false"></asp:Label>
                    </div>

                </asp:Panel>

            </ContentTemplate>

        </asp:UpdatePanel>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>

                <input id="hdnAddTeam" type="hidden" name="hddClick" runat="server" />

                <ajaxToolkit:ModalPopupExtender ID="mpeAddTeam" runat="server" PopupControlID="pnlAddTeam"
                    BackgroundCssClass="modalBackground" DropShadow="true" TargetControlID="hdnAddTeam">
                </ajaxToolkit:ModalPopupExtender>

                <asp:Panel runat="server" ID="pnlAddTeam" CssClass="modalPopup">

                    <div class="pnlheader">
                        <asp:Label ID="Label1" runat="server" Text="Add Team"></asp:Label>
                    </div>

                    <div class="pnlbody">
                        <!-- GESTIONE DELLA TABELLA CON DIV -->
                        <div class="divTable">
                            <div class="divTableBody">
                                <div class="divTableRow">
                                    <div class="divTableCell" style="width: 200px;">Team name</div>
                                    <div class="divTableCell" style="width: 580px;">
                                        <telerik:RadTextBox ID="txtAddTeam" runat="server" RenderMode="Lightweight" ClientIDMode="Static"></telerik:RadTextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="pnlfooter">
                        <telerik:RadButton ID="btnAddTeamClose" runat="server" Text="Close" Width="80px"
                            RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                        </telerik:RadButton>

                        <telerik:RadButton ID="btnAddTeamOK" runat="server" Text="Add" Width="80px"
                            RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                        </telerik:RadButton>
                    </div>

                    <div style="text-align: center;">
                        <asp:Label runat="server" ID="Label2" Visible="false"></asp:Label>
                    </div>

                </asp:Panel>

            </ContentTemplate>

        </asp:UpdatePanel>

    </div>


</asp:Content>
