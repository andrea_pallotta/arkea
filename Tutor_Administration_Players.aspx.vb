﻿Imports Telerik.Web.UI
Imports System.Data
Imports APS
Imports APPCore.Utility
Imports DALC4NET

Partial Class Tutor_Administration_Players_Define
    Inherits System.Web.UI.Page

    Private _mSessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster

    Private Sub AddGame()
        Dim sSQL As String

        If Nz(cboRole.SelectedValue) <> "" AndAlso Nni(cboGames.SelectedValue) > 0 AndAlso Nni(cboTeam.SelectedValue) > 0 Then
            ' Controllo che non esista già l'associazione Team game/player game
            sSQL = "SELECT COUNT(ID) FROM BGOL_Players_Games WHERE IDGame = " & Nni(cboGames.SelectedValue) & " AND IDPlayer = " & Session("IDPlayer") & " " _
                 & "AND IDTeam = " & Nni(cboTeam.SelectedValue) & " AND IDRole = '" & Nz(cboRole.SelectedValue) & "' "

            If Nni(g_DAL.ExecuteScalar(sSQL)) = 0 Then
                sSQL = "INSERT INTO BGOL_Players_Games (IDPlayer, IDGame, IDTeam, IDRole, SettingVariables ) " _
                     & " VALUES (" _
                     & Nni(Session("IDPlayer")) & ", " _
                     & Nni(cboGames.SelectedValue) & ", " _
                     & Nni(cboTeam.SelectedValue) & ", " _
                     & "'" & CStrSql(cboRole.SelectedValue) & "', " _
                     & Boolean_To_Bit(True) _
                     & ")"
                g_DAL.ExecuteNonQuery(sSQL)
            End If

            sSQL = "SELECT COUNT(ID) FROM BGOL_Players_Teams_Games WHERE IDGame = " & Nni(cboGames.SelectedValue) & " AND IDPlayer = " & Session("IDPlayer") _
                 & " AND IDTeam = " & Nni(cboTeam.SelectedValue)
            If Nni(g_DAL.ExecuteScalar(sSQL)) = 0 Then
                sSQL = "INSERT INTO BGOL_Players_Teams_Games (IdPlayer, IdTeam, IdGame) " _
                     & " VALUES (" _
                     & Nni(Session("IDPlayer")) & ", " _
                     & Nni(cboTeam.SelectedValue) & ", " _
                     & Nni(cboGames.SelectedValue) _
                     & ")"
                g_DAL.ExecuteNonQuery(sSQL)
            End If

            grdPlayersGames.DataSource = Nothing
            grdPlayersGames.Rebind()

            Dim oPNL As UpdatePanel = TryCast(FindControlRecursive(Page, "pnlPlayerUpdate"), UpdatePanel)
            oPNL.Update()
            mpeMain.Show()

        End If

    End Sub

    Private Sub HandleLoadLanguages()
        Dim sSQL As String
        Dim oDT As DataTable

        sSQL = "SELECT '' AS Code, '' AS Language UNION ALL SELECT Code, Language FROM Languages ORDER BY Language"
        oDT = g_DAL.ExecuteDataTable(sSQL)

        cboLanguage.DataSource = oDT
        cboLanguage.DataValueField = "Code"
        cboLanguage.DataTextField = "Language"
        cboLanguage.DataBind()

        cboLanguage.SelectedValue = -1

    End Sub

    Private Sub HandleLoadTeams()
        Dim sSQL As String
        Dim oDT As DataTable

        sSQL = "SELECT -1 AS Id, '' AS TeamName UNION ALL SELECT Id, TeamName FROM BGOL_Teams ORDER BY Id"
        oDT = g_DAL.ExecuteDataTable(sSQL)

        cboTeam.DataSource = oDT
        cboTeam.DataValueField = "Id"
        cboTeam.DataTextField = "TeamName"
        cboTeam.DataBind()

        cboTeam.SelectedValue = -1

    End Sub

    Private Sub HandleLoadRoles()
        Dim sSQL As String
        Dim oDT As DataTable

        sSQL = "SELECT '' AS Id, '' AS Descrizione UNION ALL SELECT Id, Descrizione FROM BGOL_Roles ORDER BY Id"
        oDT = g_DAL.ExecuteDataTable(sSQL)

        cboRole.DataSource = oDT
        cboRole.DataValueField = "Id"
        cboRole.DataTextField = "Descrizione"
        cboRole.DataBind()

        cboRole.SelectedValue = -1

    End Sub

    Private Sub HandleLoadGames()
        Dim sSQL As String
        Dim oDT As DataTable

        cboGames.DataSource = Nothing
        If Not Session("IDPLayer") Is Nothing Then
            sSQL = "SELECT -1 AS Id, '' AS Descrizione UNION ALL SELECT Id, Descrizione FROM BGOL_Games " _
                 & "WHERE ID NOT IN(SELECT PG.IDGame " _
                 & "FROM BGOL_Players_Games PG " _
                 & "LEFT JOIN BGOL_Games G ON G.ID = PG.IDGame " _
                 & "LEFT JOIN BGOL_Roles R ON R.ID = PG.IDRole " _
                 & "WHERE IDPlayer = " & Session("IDPlayer") & ") "

        Else
            sSQL = "SELECT -1 AS Id, '' AS Descrizione UNION ALL SELECT Id, Descrizione FROM BGOL_Games "
        End If

        oDT = g_DAL.ExecuteDataTable(sSQL)

        cboGames.DataSource = oDT
        cboGames.DataValueField = "ID"
        cboGames.DataTextField = "Descrizione"
        cboGames.DataBind()

        cboGames.SelectedValue = -1

    End Sub

    Private Function HandleLoadPlayers() As DataTable
        Dim sSql As String

        If IsNothing(g_DAL) Then g_DAL = New DBHelper

        sSQL = "SELECT DISTINCT " _
             & "BP.Id, BP.Nome, BP.Cognome, BP.Username, BP.Email, BP.Attivo, BP.SuperUser, BP.DefaultLanguage " _
             & "FROM BGOL_Players BP " _
             & "INNER JOIN BGOL_Players_Games BPG ON BP.Id = BPG.IDPlayer "

        If _mSessionData.Utente.Ruolo.IDRuolo.Contains("P") Then
            sSQL &= "WHERE BP.ID = " & _mSessionData.Utente.ID
        Else
            ' Controllo che non sia selezionato il checked button di caricamento di tutti i player
            If chkShowAllPlayers.Checked Then
                sSQL &= "WHERE BPG.IDRole <> 'D' "
            Else
                sSql &= "WHERE BPG.IDGame = " & Nni(Session("IDGame")) & " AND BPG.IDRole <> 'D' "
            End If

        End If

        Return g_DAL.ExecuteDataTable(sSQL)

    End Function

    Private Function HandleLoadPlayersGames(PlayerId As Integer) As DataTable
        Dim sSQL As String

        sSQL = "SELECT top 1 Id, IDPlayer, IDGame, IDTeam, IDRole, SettingVariables " _
             & "FROM BGOL_Players_Games " _
             & "WHERE IDPlayer = " & Nni(PlayerId)

        Return g_DAL.ExecuteDataTable(sSQL)

    End Function

    Private Function HandleLoadPlayersGamesForGrid(PlayerId As Integer) As DataTable
        Dim sSQL As String

        sSQL = "SELECT PG.Id, IDGame, IDRole, G.descrizione as Game_name, R.descrizione as Role_desc " _
             & "FROM BGOL_Players_Games PG " _
             & "LEFT JOIN BGOL_Games G ON G.ID = PG.IDGame " _
             & "LEFT JOIN BGOL_Roles R ON R.ID = PG.IDRole " _
             & "WHERE IDPlayer = " & PlayerId
        Return g_DAL.ExecuteDataTable(sSQL)

    End Function

    Private Sub ClearPanel()
        If Not Session("IDPlayer") Is Nothing Then Session("IDPlayer") = 0

        txtNome.Text = ""
        txtCognome.Text = ""
        txtUsername.Text = ""
        txtEmail.Text = ""
        chkAttivo.Checked = False
        cboLanguage.SelectedValue = -1
        cboTeam.SelectedValue = -1
        txtPassword.Text = ""
        cboRole.SelectedValue = -1
        cboGames.SelectedValue = -1

        pnlPlayerUpdate.Update()
    End Sub

    Protected Sub grdPlayers_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs)
        grdPlayers.DataSource = HandleLoadPlayers()
    End Sub

    Protected Sub grdPlayersGames_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs)
        grdPlayersGames.DataSource = HandleLoadPlayersGamesForGrid(Session("IDPlayer"))
    End Sub

    Private Sub Tutor_Administration_Players_Define_Init(sender As Object, e As EventArgs) Handles Me.Init
        _mSessionData = Session("SessionData")
    End Sub

    Private Sub Tutor_Administration_Players_Define_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsNothing(_mSessionData) Then
            PageRedirect("ErrorTimeout.aspx", "", "")
        End If

        If Not Page.IsPostBack Then

            HandleLoadLanguages()
            HandleLoadTeams()
            HandleLoadRoles()
            HandleLoadGames()

            ClearPanel()

            chkShowAllPlayers.Visible = _mSessionData.Utente.Ruolo.IDRuolo <> "P"

        End If

    End Sub

    Private Sub grdPlayers_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles grdPlayers.ItemCommand
        If e.CommandName.ToUpper <> "PAGE" AndAlso e.CommandName.ToUpper <> "SORT" AndAlso e.CommandName.ToUpper <> "FILTER" Then
            Session("ItemSelected") = DirectCast(e.Item, GridDataItem)

            If e.CommandName.ToUpper = "MODIFY" Then
                ClearPanel()
                Dim oPNL As UpdatePanel = TryCast(FindControlRecursive(Page, "pnlPlayerUpdate"), UpdatePanel)
                oPNL.Update()
                mpeMain.Show()

            ElseIf e.CommandName.ToUpper = "DELETE" Then
                deletePlayer()
            End If

        End If
    End Sub

    Private Sub deletePlayer()

        Dim oDAL As New DBHelper
        Dim oItem As GridDataItem = DirectCast(Session("ItemSelected"), GridDataItem)
        Dim oDT As DataTable = HandleLoadPlayers()

        If Not oItem Is Nothing Then
            Dim iID As Integer = oItem.GetDataKeyValue("Id")

            Dim sSQL = "DELETE FROM BGOL_Players WHERE Id = " & iID
            oDAL.ExecuteNonQuery(sSQL)

            sSQL = "DELETE FROM BGOL_Players_Games WHERE IdPlayer = " & iID
            oDAL.ExecuteNonQuery(sSQL)

            sSQL = "DELETE BGOL_Players_Teams_Games WHERE IdPlayer = " & iID
            oDAL.ExecuteNonQuery(sSQL)

            sSQL = "DELETE FROM BGOL_Players_Password WHERE IdPlayer = " & iID
            oDAL.ExecuteNonQuery(sSQL)

            Session.Remove("IDPlayer")
            Session.Remove("ItemSelected")
        End If

    End Sub

    Private Sub btnAddTeam_Click(sender As Object, e As EventArgs) Handles btnAddTeam.Click
        hdnPanelVariable.Value = "AddTeam"
        txtAddTeam.Text = ""
        mpeAddTeam.Show()
    End Sub

    Private Sub btnAddTeamOK_Click(sender As Object, e As EventArgs) Handles btnAddTeamOK.Click

        mpeMain.Show()

        Dim item As RadComboBoxItem = cboTeam.FindItemByValue("N")
        If Not item Is Nothing Then
            item.Remove()
        End If

        item = New RadComboBoxItem(txtAddTeam.Text, "N")
        item.Selected = True
        cboTeam.Items.Insert(1, item)

        txtTeamName.Text = txtAddTeam.Text

        Dim oPNL As UpdatePanel = TryCast(FindControlRecursive(Page, "pnlPlayerUpdate"), UpdatePanel)
        oPNL.Update()

        hdnPanelVariable.Value = ""

        cboTeam.SelectedValue = "N"

    End Sub

    Private Sub btnAddTeamClose_Click(sender As Object, e As EventArgs) Handles btnAddTeamClose.Click
        hdnPanelVariable.Value = ""
        mpeMain.Show()
    End Sub

    Private Sub btnEditPlayer_Click(sender As Object, e As EventArgs) Handles btnEditPlayer.Click
        Session.Remove("IDPlayer")
        Session.Remove("ItemSelected")
        ClearPanel()

        ' Ricarico tutti i giochi
        HandleLoadGames()

        mpeMain.Show()
    End Sub

    Private Sub btnEditPlayerClose_Click(sender As Object, e As EventArgs) Handles btnEditPlayerClose.Click
        ClearPanel()
        mpeMain.Hide()

    End Sub

    Private Sub Tutor_Administration_Players_Define_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        If Not Session("IDPlayer") Is Nothing Then Session.Remove("IDPlayerIDPlayer")
        If Not Session("ItemSelected") Is Nothing Then Session.Remove("ItemSelected")
    End Sub

    Private Sub btnEditPlayerSave_Click(sender As Object, e As EventArgs) Handles btnEditPlayerSave.Click
        ' Procedo con il savataggio delle informazioni
        Dim sSql As String
        Dim oCryptoDecrypto As New CryptDecrypt
        Dim iIdTeam As Integer
        Dim sNomeOldTeam As String
        Dim iIdOldTeam As Integer

        '----------------- create BGOL_Teams -----------------
        If cboTeam.SelectedValue = "N" Then

            sSql = "INSERT INTO BGOL_Teams (TeamName) " _
                    & " VALUES (" _
                        & "'" & CStrSql(cboTeam.SelectedItem.Text) & "'" _
                    & ")"
            g_DAL.ExecuteNonQuery(sSql)

            sSql = "SELECT MAX(ID) FROM BGOL_Teams "
            iIdTeam = Nni(g_DAL.ExecuteScalar(sSql))
        Else
            iIdTeam = Nni(cboTeam.SelectedValue)
        End If

        '----------------- update BGOL_Players -----------------
        ' Controllo se andare in modifica o inserimento

        ' Recupero il nome del vecchio TEAM
        sSql = "SELECT BT.TeamName " _
             & "FROM BGOL_Players_Games BPG " _
             & "INNER JOIN BGOL_Teams BT ON BPG.IDTeam = BT.Id " _
             & "WHERE IdPlayer = " & Nni(Session("IDPlayer")) & " AND IDGame = " & Nni(cboGames.SelectedValue)
        sNomeOldTeam = Nz(g_DAL.ExecuteScalar(sSql))

        ' Recupero l'ID del vecchio TEAM
        sSql = "SELECT BT.ID " _
             & "FROM BGOL_Players_Games BPG " _
             & "INNER JOIN BGOL_Teams BT ON BPG.IDTeam = BT.Id " _
             & "WHERE IdPlayer = " & Nni(Session("IDPlayer")) & " AND IDGame = " & Nni(cboGames.SelectedValue)
        iIDOldTeam = Nni(g_DAL.ExecuteScalar(sSql))

        If Nni(Session("IDPlayer")) > 0 Then ' Modifica

            sSql = "UPDATE BGOL_Players Set " _
                        & "Nome = '" & CStrSql(txtNome.Text) & "', " _
                        & "Cognome = '" & CStrSql(txtCognome.Text) & "', " _
                        & "Username = '" & CStrSql(txtUsername.Text) & "', " _
                        & "Email = '" & CStrSql(txtEmail.Text) & "', " _
                        & "Attivo = '" & Boolean_To_Bit(chkAttivo.Checked) & "', " _
                        & "DefaultLanguage = '" & CStrSql(cboLanguage.SelectedValue) & "' " _
                    & "WHERE Id = " & Nni(Session("IDPlayer"))

            g_DAL.ExecuteNonQuery(sSql)

            '----------------- update BGOL_Players_Password -----------------

            If txtPassword.Text.Trim <> "" Then

                Dim dtExpiry As DateTime = DateTime.Now.AddDays(60)
                Dim scadenza As String = dtExpiry.Year.ToString() & dtExpiry.Month.ToString().PadLeft(2, "0") & dtExpiry.Day.ToString().PadLeft(2, "0")

                Dim iIDPassword As Integer = 0
                sSql = "SELECT ID FROM BGOL_Players_Password WHERE IDPlayer = " & Nni(Session("IDPlayer"))
                iIDPassword = Nni(g_DAL.ExecuteScalar(sSql))

                If iIDPassword > 0 Then
                    sSql = "UPDATE BGOL_Players_Password Set " _
                         & "Password = '" & CStrSql(oCryptoDecrypto.Encrypt(txtPassword.Text.Trim)) & "', " _
                         & "Scadenza = '" & CStrSql(scadenza) & "', " _
                         & "Attiva = " & Boolean_To_Bit(True) _
                         & "WHERE IdPlayer = " & Nni(Session("IDPlayer"))
                Else
                    sSql = "INSERT INTO BGOL_Players_Password (Password, Scadenza, Attiva, IDPlayer) VALUES (" _
                         & "'" & CStrSql(oCryptoDecrypto.Encrypt(txtPassword.Text.Trim)) & "', " _
                         & "'" & CStrSql(scadenza) & "', " _
                         & "" & Boolean_To_Bit(True) & ", " & Nni(Session("IDPlayer")) & ") "
                End If

                g_DAL.ExecuteNonQuery(sSql)

            End If

            '----------------- insert BGOL_Players_Games -----------------

            sSql = "UPDATE BGOL_Players_Games Set " _
                 & "IDTeam = " & Nni(iIdTeam) & ", " _
                 & "IDRole = '" & CStrSql(cboRole.SelectedValue) & "' " _
                 & "WHERE IdPlayer = " & Nni(Session("IDPlayer")) & " AND IDGame = " & Nni(cboGames.SelectedValue)

            g_DAL.ExecuteNonQuery(sSql)

            '----------------- update BGOL_Players_Teams_Games -----------------

            sSql = "UPDATE BGOL_Players_Teams_Games Set " _
                 & "IdTeam = " & Nni(iIdTeam) & " " _
                 & "WHERE IdPlayer = " & Nni(Session("IDPlayer")) & " AND IdGame = " & Nni(cboGames.SelectedValue)
            g_DAL.ExecuteNonQuery(sSql)

            'ClearPanel()
            mpeMain.Show()
            HandleLoadPlayers()
            txtMessageVariable.Text = "Update completed"
            txtMessageVariable.Visible = True

            ' Aggiorno tutte le variabili presenti nel sistema in cui è contenuto il nome del vecchio Team
            ' Variabili del boss
            ' Cambio i dati del team solo se il dato presente nella combobox è diverso da quello presente nella casella di testo
            If cboTeam.Text <> txtTeamName.Text Then
                ' Aggiorno anche il nome nuovo assegnato al team
                sSql = "UPDATE BGOL_Teams SET TeamName = '" & txtTeamName.Text & "' WHERE ID = " & Nni(cboTeam.SelectedValue)
                g_DAL.ExecuteNonQuery(sSql)

                sSql = "UPDATE Decisions_Boss_Value_List SET VariableName = REPLACE(VariableName, '" & sNomeOldTeam & "', '" & txtTeamName.Text & "') WHERE ID IN (" _
                     & "SELECT DBVL.Id " _
                     & "FROM Decisions_Boss D " _
                     & "LEFT JOIN Decisions_Boss_Value DBV ON D.Id = DBV.IDDecisionBoss " _
                     & "LEFT JOIN Decisions_Boss_Value_List DBVL ON DBV.id = DBVL.IdDecisionBossValue " _
                     & "WHERE IDGame = " & Nni(cboGames.SelectedValue) & " AND VariableName LIKE '%" & sNomeOldTeam & "%') "
                g_DAL.ExecuteNonQuery(sSql)

                ' Decisioni del developer
                sSql = "UPDATE Decisions_Developer_Value_List SET VariableName = REPLACE(VariableName, '" & sNomeOldTeam & "', '" & txtTeamName.Text & "') WHERE ID IN (" _
                     & "SELECT DBVL.Id " _
                     & "FROM Decisions_Developer D " _
                     & "LEFT JOIN Decisions_Developer_Value DBV ON D.Id = DBV.IDDecisionDeveloper " _
                     & "LEFT JOIN Decisions_Developer_Value_List DBVL ON DBV.id = DBVL.IDDecisionValue " _
                     & "WHERE IDGame = " & Nni(cboGames.SelectedValue) & " AND DBVL.VariableName LIKE '%" & sNomeOldTeam & "%') "
                g_DAL.ExecuteNonQuery(sSql)

                sSql = "UPDATE Decisions_Developer_Value SET IDPlayer = " & iIdTeam & " WHERE ID IN (" _
                     & "SELECT DBV.Id " _
                     & "FROM Decisions_Developer D " _
                     & "LEFT JOIN Decisions_Developer_Value DBV ON D.Id = DBV.IDDecisionDeveloper " _
                     & "WHERE IDGame = " & Nni(cboGames.SelectedValue) & " AND DBV.IDPlayer = " & iIdOldTeam & ") "
                g_DAL.ExecuteNonQuery(sSql)

                ' Varibili di stato
                sSql = "UPDATE Variables_State_Value_List SET VariableName = REPLACE(VariableName, '" & sNomeOldTeam & "', '" & txtTeamName.Text & "') WHERE ID IN (" _
                     & "SELECT VSVL.Id " _
                     & "FROM Variables_State VS " _
                     & "INNER JOIN Variables_State_Value VSV ON VS.ID = VSV.IDVariable " _
                     & "LEFT JOIN Variables_State_Value_List VSVL ON VSV.Id = VSVL.IDVariableState " _
                     & "WHERE IDGame = " & Nni(cboGames.SelectedValue) & " AND VSVL.VariableName LIKE '%" & sNomeOldTeam & "%') "
                g_DAL.ExecuteNonQuery(sSql)

                sSql = "UPDATE Variables_State_Value SET IDPlayer = " & iIdTeam & " WHERE ID IN (" _
                     & "SELECT VSV.Id " _
                     & "FROM Variables_State VS " _
                     & "INNER JOIN Variables_State_Value VSV ON VS.ID = VSV.IDVariable " _
                     & "WHERE IDGame = " & Nni(cboGames.SelectedValue) & " AND VSV.IDPlayer = " & iIdOldTeam & ") "
                g_DAL.ExecuteNonQuery(sSql)

                ' Decisioni del player
                sSql = "UPDATE Decisions_Players_Value_List SET VariableName = REPLACE(VariableName, '" & sNomeOldTeam & "', '" & txtTeamName.Text & "'), " _
                     & "IDPlayer = " & iIdTeam & " " _
                     & "WHERE ID IN (" _
                     & "SELECT DPVL.Id " _
                     & "FROM Decisions_Players D " _
                     & "LEFT JOIN Decisions_Players_Value DPV ON D.Id = DPV.IDDecisionPlayer " _
                     & "LEFT JOIN Decisions_Players_Value_List DPVL ON DPV.ID = DPVL.IDDecisionValue " _
                     & "WHERE IDGame = " & Nni(cboGames.SelectedValue) & " AND DPVL.VariableName LIKE '%" & sNomeOldTeam & "%') "
                g_DAL.ExecuteNonQuery(sSql)

                sSql = "UPDATE Variables_State_Value SET IDPlayer = " & iIdTeam & " WHERE ID IN (" _
                     & "SELECT DPV.Id " _
                     & "FROM Decisions_Players D " _
                     & "LEFT JOIN Decisions_Players_Value DPV ON D.Id = DPV.IDDecisionPlayer " _
                     & "WHERE IDGame = " & Nni(cboGames.SelectedValue) & " AND DPV.IDPlayer = " & iIdOldTeam & ") "
                g_DAL.ExecuteNonQuery(sSql)

                ' Variabili calcolate
                sSql = "UPDATE Variables_Calculate_Value SET IDPlayer = " & iIdTeam & " WHERE ID IN (" _
                     & "SELECT C.ID " _
                     & "FROM Variables_Calculate_Define V " _
                     & "LEFT JOIN Variables_Calculate_Value C ON V.Id = C.IDVariable " _
                     & "WHERE IDGame = " & Nni(cboGames.SelectedValue) & " AND IDPlayer = " & iIdOldTeam & ") "
                g_DAL.ExecuteNonQuery(sSql)

            End If

        Else

            '----------------- insert BGOL_Players -----------------

            sSql = "INSERT INTO BGOL_Players (Nome, Cognome, Username, Email, Attivo, DefaultLanguage) " _
                    & " VALUES (" _
                    & "'" & CStrSql(txtNome.Text) & "', " _
                    & "'" & CStrSql(txtCognome.Text) & "', " _
                    & "'" & CStrSql(txtUsername.Text) & "', " _
                    & "'" & CStrSql(txtEmail.Text) & "', " _
                    & Boolean_To_Bit(chkAttivo.Checked) & ", " _
                    & "'" & CStrSql(cboLanguage.SelectedValue) & "'" _
                    & ")"

            g_DAL.ExecuteNonQuery(sSql)

            '----------------- Sono in inserimento, procedo con il caricamento dell'ultimo id inserito '-----------------

            If Nni(Session("IDVariable"), 0) = 0 Then
                sSql = "SELECT MAX(ID) FROM BGOL_Players "
                Session("IDPlayer") = Nni(g_DAL.ExecuteScalar(sSql))
            End If

            '----------------- insert BGOL_Players_Teams_Games -----------------

            sSql = "INSERT INTO BGOL_Players_Teams_Games (IdPlayer, IdTeam, IdGame) " _
                    & " VALUES (" _
                        & Nni(Session("IDPlayer")) & ", " _
                        & Nni(iIdTeam) & ", " _
                        & Nni(cboGames.SelectedValue) _
                        & ")"

            g_DAL.ExecuteNonQuery(sSql)

            '----------------- insert BGOL_Players_Games -----------------

            sSql = "INSERT INTO BGOL_Players_Games (IDPlayer, IDGame, IDTeam, IDRole, SettingVariables ) " _
                    & " VALUES (" _
                        & Nni(Session("IDPlayer")) & ", " _
                        & Nni(cboGames.SelectedValue) & ", " _
                        & Nni(iIdTeam) & ", " _
                        & "'" & CStrSql(cboRole.SelectedValue) & "', " _
                        & Boolean_To_Bit(True) _
                    & ")"

            g_DAL.ExecuteNonQuery(sSql)

            mpeMain.Hide()

        End If

        grdPlayers.DataSource = Nothing
        grdPlayers.Rebind()

        grdPlayersGames.DataSource = Nothing
        grdPlayersGames.Rebind()

        If m_SiteMaster Is Nothing Then m_SiteMaster = Me.Master
        m_SiteMaster.LoadTeams()
        m_SiteMaster.UpdatePanelTeamPeriod.Update()

        pnlMain.Update()
    End Sub

    Private Sub pnlPlayerEdit_PreRender(sender As Object, e As EventArgs) Handles pnlPlayerEdit.PreRender

        If hdnPanelVariable.Value = "AddTeam" Or txtAddTeam.Text <> "" Then
            Return
        End If

        Dim oItem As GridDataItem = DirectCast(Session("ItemSelected"), GridDataItem)
        Dim oDT As DataTable = HandleLoadPlayers()

        ' Procedo con il caricamento dei dati da inserire nel panello
        ' Recupero l'id della variabile selezionata
        If Not oItem Is Nothing Then

            HandleLoadGames()

            Dim iID As Integer = oItem.GetDataKeyValue("Id")
            Dim oDRData As DataRow = oDT.Select("Id = " & iID).FirstOrDefault
            If Not oDRData Is Nothing Then
                txtNome.Text = Nz(oDRData("Nome"))
                txtCognome.Text = Nz(oDRData("Cognome"))
                txtUsername.Text = Nz(oDRData("Username"))
                txtEmail.Text = Nz(oDRData("Email"))
                chkAttivo.Checked = Nz(oDRData("Attivo"))
                cboLanguage.SelectedValue = Nz(oDRData("DefaultLanguage"))

                Session("IDPlayer") = iID

                Dim oDT2 As DataTable = HandleLoadPlayersGames(Session("IDPlayer"))
                Dim oDRData2 As DataRow = oDT2.Select("IDPlayer = " & Nni(Session("IDPlayer"))).FirstOrDefault

                If Not oDRData2 Is Nothing Then
                    cboTeam.SelectedValue = Nni(oDRData2("IDTeam"))
                    cboRole.SelectedValue = Nz(oDRData2("IDRole"))
                    cboGames.SelectedValue = Nni(oDRData2("IDGame"))
                End If

                grdPlayersGames.DataSource = HandleLoadPlayersGamesForGrid(Session("IDPlayer"))
                grdPlayersGames.Rebind()

                ' Carico anche il textBox del nome team, nel caso sia un player e voglia modificarli
                txtTeamName.Text = GetTeamName(Nni(oDRData2("IDTeam")))

                If _mSessionData.Utente.Ruolo.IDRuolo.Contains("P") Then
                    cboTeam.Enabled = False
                    txtTeamName.Enabled = True
                    txtTeamName.Visible = True
                    btnAddGame.Enabled = False
                    btnAddTeam.Enabled = False
                    cboRole.Enabled = False
                    cboGames.Enabled = False
                    grdPlayersGames.Enabled = False

                Else
                    cboTeam.Enabled = True
                    txtTeamName.Enabled = True
                    txtTeamName.Visible = True
                    btnAddGame.Enabled = True
                    btnAddTeam.Enabled = True
                    cboRole.Enabled = True
                    cboGames.Enabled = True
                    grdPlayersGames.Enabled = True

                End If

            End If
        End If

    End Sub

    Private Sub btnAddGame_Click(sender As Object, e As EventArgs) Handles btnAddGame.Click
        AddGame()
    End Sub

    Private Sub grdPlayersGames_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles grdPlayersGames.ItemCommand
        If e.CommandName.ToUpper <> "PAGE" AndAlso e.CommandName.ToUpper <> "SORT" AndAlso e.CommandName.ToUpper <> "FILTER" Then
            Session("ItemSelectedPG") = DirectCast(e.Item, GridDataItem)
            Dim oItem As GridDataItem = DirectCast(Session("ItemSelectedPG"), GridDataItem)

            If e.CommandName.ToUpper = "MODIFY" Then
                ClearPanel()
                Dim oPNL As UpdatePanel = TryCast(FindControlRecursive(Page, "pnlPlayerUpdate"), UpdatePanel)
                oPNL.Update()
                mpeMain.Show()

            ElseIf e.CommandName.ToUpper = "DELETE" Then

                If Not oItem Is Nothing Then
                    oItem.GetDataKeyValue("ID")

                    Dim sSQL As String = "DELETE FROM BGOL_Players_Games WHERE ID = " & oItem.GetDataKeyValue("Id")
                    g_DAL.ExecuteNonQuery(sSQL)

                    sSQL = "DELETE FROM BGOL_Players_Teams_Games WHERE IDPlayer = " & Nni(Session("IDPlayer")) & " AND IDGame = " & oItem.GetDataKeyValue("IDGame")
                    g_DAL.ExecuteNonQuery(sSQL)

                    grdPlayersGames.DataSource = Nothing
                    grdPlayersGames.Rebind()

                    Dim oPNL As UpdatePanel = TryCast(FindControlRecursive(Page, "pnlPlayerUpdate"), UpdatePanel)
                    oPNL.Update()
                    mpeMain.Show()
                End If
            End If

        End If
    End Sub

    Private Sub chkShowAllPlayers_Click(sender As Object, e As EventArgs) Handles chkShowAllPlayers.Click
        grdPlayers.Rebind()
    End Sub


#Region "CREAZIONE DATI NUOVO GIOCATORE"

    Private Sub LoadDataDecisionsBoss(IDGame As Integer, IDGameNew As Integer, IDPeriodo As Integer, IDPeriodoGameNew As Integer, IDTeamMeU As Integer, TeamNameMeU As String)
        Dim sSQL As String = ""
        Dim iIDTeamMaster As Integer
        Dim iIDPlayerMaster As Integer
        Dim sTeamNameMaster As String
        Dim sTeamName As String = Nz(cboTeam.Text)
        Dim iIDDecisionBossNew As Integer
        Dim iIDDecisionBossValueNew As Integer

        Try
            ' Recupero i dati di del primo player, generalmente è quello usato come master
            sSQL = "SELECT MIN(BPTG.IDPLayer) AS IDPlayer , MIN(BPTG.IdTeam) AS IDTeam " _
                 & "FROM BGOL_Players_Teams_Games BPTG " _
                 & "INNER JOIN BGOL_Players_Games BTG ON BPTG.IdGame = BTG.IDGame " _
                 & "AND BPTG.IdPlayer = BTG.IDPlayer " _
                 & "AND BPTG.IdTeam = BTG.IDTeam " _
                 & "WHERE BPTG.IDGame = " & IDGame & " AND BPTG.IDTeam <> " & IDTeamMeU & " AND BTG.IDRole = 'P' "
            Dim dtData As DataTable = g_DAL.ExecuteDataTable(sSQL)
            For Each drData As DataRow In dtData.Rows
                iIDTeamMaster = Nni(drData("IDTeam"))
                iIDPlayerMaster = Nni(drData("IDPlayer"))
            Next
            sSQL = "SELECT TeamName FROM BGOL_Teams WHERE ID = " & iIDTeamMaster
            sTeamNameMaster = Nz(g_DAL.ExecuteScalar(sSQL))

            ' Recupero i record della tabella Decisions_Boss_Value
            sSQL = "SELECT * FROM Decisions_Boss_Value DSV " _
                 & "INNER JOIN Decisions_Boss DB ON DSV.IDDecisionBoss = DB.Id " _
                 & "WHERE DB.IDGame = " & IDGame & " " _
                 & "AND ISNULL(DSV.IDPeriod, " & IDPeriodo & ") = " & IDPeriodo
            Dim dtDecisions_Boss_Value As DataTable = g_DAL.ExecuteDataTable(sSQL)

            For Each drDecisions_Boss_Value As DataRow In dtDecisions_Boss_Value.Rows
                'If Nz(drDecisions_Boss_Value("Decision")) = "DebtWrittRate" Then
                '    MessageText.Text = "eccomi..."
                'End If

                ' Recupero l'IDDecisionBoss del game di destinazione
                sSQL = "SELECT ID FROM Decisions_Boss WHERE Decision = '" & Nz(drDecisions_Boss_Value("Decision")) & "' AND IDGame = " & IDGameNew
                iIDDecisionBossNew = g_DAL.ExecuteScalar(sSQL)

                If Nz(drDecisions_Boss_Value("Value")).ToUpper.Contains("LIST OF") Then
                    ' Recupero la riga della lista di dettaglio
                    sSQL = "SELECT * FROM Decisions_Boss_Value_List " _
                         & "WHERE IdDecisionBossValue IN(" & drDecisions_Boss_Value("ID") & ") AND IDPeriod = " & IDPeriodo
                    Dim dtDecisions_Boss_Value_List As DataTable = g_DAL.ExecuteDataTable(sSQL)
                    For Each drDecisions_Boss_Value_List As DataRow In dtDecisions_Boss_Value_List.Rows
                        ' Recupero l'ID della variabile dal gioco nuovo per l'inserimento corretto
                        sSQL = "SELECT ID FROM Decisions_Boss_Value WHERE IdDecisionBoss = " & iIDDecisionBossNew
                        iIDDecisionBossValueNew = Nni(g_DAL.ExecuteScalar(sSQL))

                        ' Controllo che nel nome della variabile non sia contenuto il nome del team, nel caso devo generare per tutti i player
                        If Nz(drDecisions_Boss_Value_List("VariableName")).ToUpper.Contains(sTeamNameMaster.ToUpper) Then
                            ' Genero il nome del team e del player
                            sSQL = "INSERT INTO Decisions_Boss_Value_List (IdDecisionBossValue, IDPeriod, Simulation, Value, VariableName) " _
                                     & "SELECT " & iIDDecisionBossValueNew & ", " & IDPeriodoGameNew & " , Simulation, Value, " _
                                     & "REPLACE(VariableName, '" & sTeamNameMaster & "', '" & CStrSql(sTeamName) & "') " _
                                     & "FROM Decisions_Boss_Value_List DBVL " _
                                     & "WHERE Id = " & Nni(drDecisions_Boss_Value_List("Id")) & " AND VariableName LIKE '%" & sTeamNameMaster & "%' AND IDPeriod = " & IDPeriodo
                            g_DAL.ExecuteNonQuery(sSQL)

                        End If
                    Next

                Else ' Valore secco da inserire 
                    sSQL = "INSERT INTO Decisions_Boss_Value (IDDecisionBoss, Value, IDPeriod) VALUES (" _
                         & iIDDecisionBossNew & ", '" & drDecisions_Boss_Value("Value") & "', " & IDPeriodoGameNew & ") "
                    g_DAL.ExecuteNonQuery(sSQL)
                End If
            Next

            ' Aggiorno eventuali errori durante la generazione
            sSQL = "UPDATE Decisions_Boss_Value SET IDPeriod = NULL WHERE UPPER(Value) LIKE '%LIST OF%' "
            g_DAL.ExecuteNonQuery(sSQL)

        Catch ex As Exception
            txtMessageVariable.Text = "Error LoadDataDecisionsBoss -> " & ex.Message & "<br/>" & sSQL
            txtMessageVariable.Visible = True
        End Try

    End Sub

    Private Sub LoadDataDecisionsDeveloper(IDGame As Integer, IDGameNew As Integer, IDPeriodo As Integer, IDPeriodoGameNew As Integer, IDTeamMeU As Integer, TeamNameMeU As String)
        Dim sSQL As String = ""
        Dim iIDTeamMaster As Integer
        Dim iIDPlayerMaster As Integer
        Dim sTeamNameMaster As String
        Dim sTeamName As String = Nz(cboTeam.Text)
        Dim iIDTeamNew As Integer
        Dim iIDDecisionDeveloperNew As Integer

        Try
            ' Recupero l'ID del team da inserire
            sSQL = "SELECT ID FROM BGOL_Teams WHERE TeamName = '" & sTeamName & "' "
            iIDTeamNew = Nni(g_DAL.ExecuteScalar(sSQL))

            ' Recupero i dati di del primo player, generalmente è quello usato come master
            sSQL = "SELECT MIN(BPTG.IDPLayer) AS IDPlayer , MIN(BPTG.IdTeam) AS IDTeam " _
                 & "FROM BGOL_Players_Teams_Games BPTG " _
                 & "INNER JOIN BGOL_Players_Games BTG ON BPTG.IdGame = BTG.IDGame " _
                 & "AND BPTG.IdPlayer = BTG.IDPlayer " _
                 & "AND BPTG.IdTeam = BTG.IDTeam " _
                 & "WHERE BPTG.IDGame = " & IDGame & " AND BPTG.IDTeam <> " & IDTeamMeU & " AND BTG.IDRole = 'P' "
            Dim dtData As DataTable = g_DAL.ExecuteDataTable(sSQL)
            For Each drData As DataRow In dtData.Rows
                iIDTeamMaster = Nni(drData("IDTeam"))
                iIDPlayerMaster = Nni(drData("IDPlayer"))
            Next
            sSQL = "SELECT TeamName FROM BGOL_Teams WHERE ID = " & iIDTeamMaster
            sTeamNameMaster = Nz(g_DAL.ExecuteScalar(sSQL))

            sSQL = "SELECT * FROM Decisions_Developer_Value DDV " _
                 & "INNER JOIN Decisions_Developer DD ON DDV.IDDecisionDeveloper = DD.Id " _
                 & "WHERE DD.IDGame = " & IDGame & " AND ISNULL(DDV.IDPeriod, " & IDPeriodo & ") = " & IDPeriodo
            Dim dtDecisions_Developer_Value As DataTable = g_DAL.ExecuteDataTable(sSQL)

            For Each drDecisions_Developer_Value As DataRow In dtDecisions_Developer_Value.Rows
                ' Recupero l'ID della variabile developer per il nuovo gioco
                sSQL = "SELECT ID FROM Decisions_Developer WHERE IDGame = " & IDGameNew & " AND VariableName = '" & Nz(drDecisions_Developer_Value("VariableName")) & "' "
                iIDDecisionDeveloperNew = g_DAL.ExecuteScalar(sSQL)

                ' Controllo la presenza dell'IDPlayer, se presente allora ciclo su tutti i player del nuovo gioco, altrimenti 
                If Nni(drDecisions_Developer_Value("IDPlayer")) > 0 Then
                    If Nni(drDecisions_Developer_Value("IDPlayer")) = iIDTeamMaster Then

                        ' Inserisco i dati nella tabella
                        sSQL = "INSERT INTO Decisions_Developer_Value (IDDecisionDeveloper, IDPeriod, Value, IDItem, IDPlayer) VALUES (" _
                             & iIDDecisionDeveloperNew & ", " & IDPeriodoGameNew & ", '" & Nz(drDecisions_Developer_Value("Value")) & "', " _
                             & Nni(drDecisions_Developer_Value("IDItem")) & ", " & iIDTeamNew & ") "
                        g_DAL.ExecuteNonQuery(sSQL)


                    End If
                End If
            Next

        Catch ex As Exception
            txtMessageVariable.Text = "Error LoadDataDecisionsDeveloper -> " & ex.Message & "<br/>" & sSQL
            txtMessageVariable.Visible = True
        End Try
    End Sub

    Private Sub LoadDataVariablesState(IDGame As Integer, IDGameNew As Integer, IDPeriodo As Integer, IDPeriodoGameNew As Integer, IDTeamMeU As Integer, TeamNameMeU As String)
        Dim sSQL As String = ""
        Dim iIDTeamMaster As Integer
        Dim iIDPlayerMaster As Integer
        Dim sTeamNameMaster As String
        Dim sTeamName As String = Nz(cboTeam.Text)
        Dim iIDTeamNew As Integer
        Dim iIDVariableState As Integer
        Dim iIDVariableStateValue As Integer
        Dim sPlayer As String

        Try
            ' Recupero i dati di del primo player, generalmente è quello usato come master
            sSQL = "SELECT MIN(BPTG.IDPLayer) AS IDPlayer , MIN(BPTG.IdTeam) AS IDTeam " _
                 & "FROM BGOL_Players_Teams_Games BPTG " _
                 & "INNER JOIN BGOL_Players_Games BTG ON BPTG.IdGame = BTG.IDGame " _
                 & "AND BPTG.IdPlayer = BTG.IDPlayer " _
                 & "AND BPTG.IdTeam = BTG.IDTeam " _
                 & "WHERE BPTG.IDGame = " & IDGame & " AND BPTG.IDTeam <> " & IDTeamMeU & " AND BTG.IDRole = 'P' "
            Dim dtData As DataTable = g_DAL.ExecuteDataTable(sSQL)
            For Each drData As DataRow In dtData.Rows
                iIDTeamMaster = Nni(drData("IDTeam"))
                iIDPlayerMaster = Nni(drData("IDPlayer"))
            Next
            sSQL = "SELECT TeamName FROM BGOL_Teams WHERE ID = " & iIDTeamMaster
            sTeamNameMaster = Nz(g_DAL.ExecuteScalar(sSQL))

            ' Recupero le variabili che mi servono
            sSQL = "SELECT * FROM Variables_State_Value VSV " _
                 & "INNER JOIN Variables_State VS ON VSV.IDVariable = VS.Id " _
                 & "WHERE VS.IDGame = " & IDGame & " AND ISNULL(VSV.IDPeriod, " & IDPeriodo & ") = " & IDPeriodo
            '& " AND Name = 'StockRawMater' "
            Dim dtVariables_State_Value As DataTable = g_DAL.ExecuteDataTable(sSQL)

            For Each drVariables_State_Value As DataRow In dtVariables_State_Value.Rows
                ' Recupero l'ID della variabile di stato del game attuale
                sSQL = "SELECT ID FROM Variables_State WHERE IDGame = " & IDGameNew & " AND Name = '" & Nz(drVariables_State_Value("Name")) & "' "
                iIDVariableState = g_DAL.ExecuteScalar(sSQL)

                ' Recupero l'ID della variabile valore da usare
                sSQL = "SELECT ID FROM Variables_State_Value WHERE IDVariable = " & iIDVariableState
                iIDVariableStateValue = g_DAL.ExecuteScalar(sSQL)

                ' Controllo il tipo di variabile
                ' Controllo se di tipo LISTA
                If Nz(drVariables_State_Value("Value")).ToUpper.Contains("LIST OF") Then
                    ' Ciclo sui player del gioco per comporre anche il nome della variabile e inserirla
                    If Nni(drVariables_State_Value("IDPlayer")) = iIDTeamMaster Then

                        sSQL = "SELECT ID FROM BGOL_Teams WHERE TeamName = '" & CStrSql(sTeamName) & "' "
                        iIDTeamNew = Nni(g_DAL.ExecuteScalar(sSQL))

                        sSQL = "INSERT INTO Variables_State_Value_List (IDAge, IDItem, IDPeriod, IDPlayer, IDVariableState, Simulation, Value, VariableName) " _
                             & "SELECT VSVL.IDAge, VSVL.IDItem, " & IDPeriodoGameNew & ", " & iIDTeamNew & ", " & iIDVariableStateValue & ", VSVL.Simulation, VSVL.Value, " _
                             & "REPLACE(VSVL.VariableName, 'M&U', '" & sPlayer & "') " _
                             & "FROM Variables_State_Value_List VSVL " _
                             & "WHERE VSVL.ID = " & Nni(drVariables_State_Value("Id"))
                        g_DAL.ExecuteNonQuery(sSQL)

                    End If

                Else

                    ' For Each drData As DataRow In oDTData.Rows
                    If Nni(drVariables_State_Value("IDPlayer")) > 0 Then
                        If Nni(drVariables_State_Value("IDPlayer")) = iIDTeamMaster Then

                            sSQL = "SELECT ID FROM BGOL_Teams WHERE TeamName = '" & CStrSql(sTeamName) & "' "
                            iIDTeamNew = Nni(g_DAL.ExecuteScalar(sSQL))
                            sSQL = "INSERT INTO Variables_State_Value (IDAge, IDItem, IDPeriod, IDPlayer, IDVariable, Simulation, Value) " _
                                 & "SELECT VSV.IDAge, VSV.IDItem, " & IDPeriodoGameNew & ", " & iIDTeamNew & ", " & iIDVariableState & ", VSV.Simulation, VSV.Value " _
                                 & "FROM Variables_State_Value VSV " _
                                 & "WHERE VSV.ID = " & Nni(drVariables_State_Value("ID"))
                            g_DAL.ExecuteScalar(sSQL)

                        End If
                    Else
                        sSQL = "INSERT INTO Variables_State_Value (IDAge, IDItem, IDPeriod, IDPlayer, IDVariable, Simulation, Value) " _
                             & "SELECT VSV.IDAge, VSV.IDItem, " & IDPeriodoGameNew & ", IDPlayer, " & iIDVariableState & ", VSV.Simulation, VSV.Value " _
                             & "FROM Variables_State_Value VSV " _
                             & "WHERE VSV.ID = " & Nni(drVariables_State_Value("ID"))
                        g_DAL.ExecuteScalar(sSQL)
                    End If

                End If
            Next
        Catch ex As Exception
            txtMessageVariable.Text = "Error LoadDataVariablesState -> " & ex.Message & "<br/>" & sSQL
            txtMessageVariable.Visible = True
        End Try
    End Sub

    Private Sub LoadDataDecisionsPlayer(IDGame As Integer, IDGameNew As Integer, IDPeriodo As Integer, IDPeriodoGameNew As Integer, IDTeamMeU As Integer, TeamNameMeU As String)
        Dim sSQL As String = ""
        Dim iIDTeamMaster As Integer
        Dim iIDPlayerMaster As Integer
        Dim sTeamNameMaster As String
        Dim sTeamName As String = Nz(cboTeam.Text)
        Dim iIDTeamNew As Integer
        Dim iIDDecisions_Player_Developer As Integer
        Dim iIDDecisions_Player_Developer_Value As Integer

        Try
            ' Recupero i dati di del primo player, generalmente è quello usato come master
            sSQL = "SELECT MIN(BPTG.IDPLayer) AS IDPlayer , MIN(BPTG.IdTeam) AS IDTeam " _
                 & "FROM BGOL_Players_Teams_Games BPTG " _
                 & "INNER JOIN BGOL_Players_Games BTG ON BPTG.IdGame = BTG.IDGame " _
                 & "AND BPTG.IdPlayer = BTG.IDPlayer " _
                 & "AND BPTG.IdTeam = BTG.IDTeam " _
                 & "WHERE BPTG.IDGame = " & IDGame & " AND BPTG.IDTeam <> " & IDTeamMeU & " AND BTG.IDRole = 'P' "
            Dim dtData As DataTable = g_DAL.ExecuteDataTable(sSQL)
            For Each drData As DataRow In dtData.Rows
                iIDTeamMaster = Nni(drData("IDTeam"))
                iIDPlayerMaster = Nni(drData("IDPlayer"))
            Next
            sSQL = "SELECT TeamName FROM BGOL_Teams WHERE ID = " & iIDTeamMaster
            sTeamNameMaster = Nz(g_DAL.ExecuteScalar(sSQL))

            ' Recupero le variabili che mi servono
            sSQL = "SELECT D.ID, D.IDGame, D.VariableName, D.VariableLabelGroup, D.VariableLabel, D.IDDataType, D.IDVariable, " _
                 & "DPV.ID AS IDDecisionPlayerValue, IDDecisionPlayer, IDPeriod, VariableValue, IDPlayer, Simulation " _
                 & "FROM Decisions_Players D " _
                 & "LEFT JOIN Decisions_Players_Value DPV ON D.Id = DPV.IDDecisionPlayer " _
                 & "WHERE D.IDGame = " & IDGame & " AND ISNULL(DPV.IDPeriod, " & IDPeriodo & ") = " & IDPeriodo
            Dim dtDecisions_Players_Value As DataTable = g_DAL.ExecuteDataTable(sSQL)

            For Each drDecisions_Players_Value As DataRow In dtDecisions_Players_Value.Rows
                ' Recupero l'ID della variabile decisions developer del game nuovo
                sSQL = "SELECT ID FROM Decisions_Players WHERE IDGame = " & IDGameNew & " AND VariableName = '" & Nz(drDecisions_Players_Value("VariableName")) & "' "
                iIDDecisions_Player_Developer = Nni(g_DAL.ExecuteScalar(sSQL))

                sSQL = "SELECT ID FROM Decisions_Players_Value WHERE IDDecisionPlayer = " & iIDDecisions_Player_Developer
                iIDDecisions_Player_Developer_Value = Nni(g_DAL.ExecuteScalar(sSQL))

                ' Controllo il tipo di variabile
                ' Controllo se di tipo LISTA
                If Nz(drDecisions_Players_Value("VariableValue")).ToUpper.Contains("LIST OF") Then
                    ' Recupero i dati 
                    sSQL = "SELECT DPVL.Id, DPVL.IDAge, DPVL.IDDecisionValue, DPVL.IDItem, DPVL.IDPeriod, DPVL.IDPlayer, DPVL.Simulation, DPVL.Value, DPVL.VariableName " _
                         & "FROM Decisions_Players_Value_List DPVL " _
                         & "WHERE IDDecisionValue = " & Nni(drDecisions_Players_Value("IDDecisionPlayerValue")) & " AND IDPeriod = " & IDPeriodo & " AND IDPlayer = " & iIDTeamMaster
                    Dim dtDecisionPlayerValueList As DataTable = g_DAL.ExecuteDataTable(sSQL)
                    For Each drDecisions_players_value_List As DataRow In dtDecisionPlayerValueList.Rows
                        If Nni(drDecisions_players_value_List("IDPlayer")) = iIDTeamMaster Then
                            ' Inserisco il valore per tutti player nella lista del nuovo game
                            sSQL = "SELECT ID FROM BGOL_Teams WHERE TeamName = '" & CStrSql(sTeamName) & "' "
                            iIDTeamNew = Nni(g_DAL.ExecuteScalar(sSQL))
                            sSQL = "INSERT INTO Decisions_Players_Value_List (IDAge, IDDecisionValue, IDItem, IDPeriod, IDPlayer, Simulation, Value, VariableName) " _
                                 & "SELECT DPVL.IDAge," & iIDDecisions_Player_Developer_Value & ", DPVL.IDItem, " & IDPeriodoGameNew & ", " & iIDTeamNew & ", DPVL.Simulation, " _
                                 & "DPVL.Value, REPLACE(DPVL.VariableName, '" & sTeamNameMaster & "', '" & CStrSql(sTeamName) & "') " _
                                 & "FROM Decisions_Players_Value_List DPVL " _
                                 & "WHERE DPVL.ID = " & Nni(drDecisions_players_value_List("ID")) & " AND ISNULL(IDPeriod, " & IDPeriodo & ") = " & IDPeriodo
                            g_DAL.ExecuteNonQuery(sSQL)

                        End If
                    Next
                Else
                    ' Se il player presente è M&U procedo, altrimenti scarto
                    If Nni(drDecisions_Players_Value("IDPlayer")) = iIDTeamMaster Then

                        sSQL = "Select ID FROM BGOL_Teams WHERE TeamName = '" & CStrSql(sTeamName) & "' "
                        iIDTeamNew = Nni(g_DAL.ExecuteScalar(sSQL))
                        ' Inserisco la variabile nella tabella value
                        sSQL = "INSERT INTO Decisions_Players_Value (IDDecisionPlayer, IDPeriod, IDPlayer, Simulation, VariableValue) " _
                             & "SELECT " & iIDDecisions_Player_Developer & ", " & IDPeriodoGameNew & ", " & iIDTeamNew & ", DPV.Simulation, DPV.VariableValue " _
                             & "FROM Decisions_Players_Value DPV " _
                             & "WHERE DPV.ID = " & Nni(drDecisions_Players_Value("IDDecisionPlayerValue"))
                        g_DAL.ExecuteNonQuery(sSQL)


                    End If
                End If
            Next
        Catch ex As Exception
            txtMessageVariable.Text = "Error LoadDataDecisionsPlayer -> " & ex.Message & "<br/>" & sSQL
            txtMessageVariable.Visible = True
        End Try

    End Sub

    Private Sub LoadDataVariableCalculateDefine(IDGame As Integer, IDGameNew As Integer, IDPeriodo As Integer, IDPeriodoGameNew As Integer, IDTeamMeU As Integer, TeamNameMeU As String)
        Dim sSQL As String = ""
        Dim oDTTeams As DataTable = APSUtility.LoadTeamsGame(IDGame)
        Dim iIDTeamMaster As Integer
        Dim iIDPlayerMaster As Integer
        Dim sTeamNameMaster As String
        Dim sTeamName As String = Nz(cboTeam.Text)
        Dim iIDTeamNew As Integer
        Dim iIDVariableCalculateDefine As Integer

        Try
            ' Recupero i dati di del primo player, generalmente è quello usato come master
            sSQL = "SELECT MIN(BPTG.IDPLayer) AS IDPlayer , MIN(BPTG.IdTeam) AS IDTeam " _
                 & "FROM BGOL_Players_Teams_Games BPTG " _
                 & "INNER JOIN BGOL_Players_Games BTG ON BPTG.IdGame = BTG.IDGame " _
                 & "AND BPTG.IdPlayer = BTG.IDPlayer " _
                 & "AND BPTG.IdTeam = BTG.IDTeam " _
                 & "WHERE BPTG.IDGame = " & IDGame & " AND BPTG.IDTeam <> " & IDTeamMeU & " AND BTG.IDRole = 'P' "
            Dim dtData As DataTable = g_DAL.ExecuteDataTable(sSQL)
            For Each drData As DataRow In dtData.Rows
                iIDTeamMaster = Nni(drData("IDTeam"))
                iIDPlayerMaster = Nni(drData("IDPlayer"))
            Next
            sSQL = "SELECT TeamName FROM BGOL_Teams WHERE ID = " & iIDTeamMaster
            sTeamNameMaster = Nz(g_DAL.ExecuteScalar(sSQL))

            ' Recpero la lista delle variabili calcolate
            sSQL = "SELECT V.Id, V.VariableName, V.IDGame, V.IDCategory, V.VariableLabel, " _
                 & "C.Id AS IDVariableCalculateValue, C.IDVariable, C.IDPlayer, C.IDPeriod, C.IDItem, C.IDAge, C.Value, C.Simulation " _
                 & "FROM Variables_Calculate_Define V " _
                 & "LEFT JOIN Variables_Calculate_Value C ON V.Id = C.IDVariable " _
                 & "WHERE V.IDGame = " & IDGame & " AND IDPeriod = " & IDPeriodo
            Dim dtVariablesCalculate As DataTable = g_DAL.ExecuteDataTable(sSQL)

            For Each drVariableCalculate As DataRow In dtVariablesCalculate.Rows
                ' Recupero l'ID della variabile calcolata del gioco attuale
                sSQL = "SELECT ID FROM Variables_Calculate_Define WHERE IDGame = " & IDGameNew & " AND VariableName = '" & Nz(drVariableCalculate("VariableName")) & "' "
                iIDVariableCalculateDefine = Nni(g_DAL.ExecuteScalar(sSQL))

                ' COntrollo che sia una variabile con il player valorizzato
                If Nni(drVariableCalculate("IDPlayer")) > 0 Then
                    ' Inserisco i valori necessari per tutti i player del nuovo game
                    If Nni(drVariableCalculate("IDPlayer")) = iIDTeamMaster Then

                        sSQL = "Select ID FROM BGOL_Teams WHERE TeamName = '" & CStrSql(sTeamName) & "' "
                        iIDTeamNew = Nni(g_DAL.ExecuteScalar(sSQL))
                        ' Inserisco la variabile nella tabella value
                        sSQL = "INSERT INTO Variables_Calculate_Value (IDVariable, IDPeriod, IDPlayer, Value, IDItem, IDAge) " _
                             & "SELECT " & iIDVariableCalculateDefine & ", " & IDPeriodoGameNew & ", " & iIDTeamNew & ", Value, IDItem, IDAge " _
                             & "FROM Variables_Calculate_Value " _
                             & "WHERE ID = " & Nni(drVariableCalculate("IDVariableCalculateValue"))
                        g_DAL.ExecuteNonQuery(sSQL)

                    End If
                Else
                    ' Non ho player associati, procedo con il normale inserimento della riga calcolata
                    sSQL = "INSERT INTO Variables_Calculate_Value (IDVariable, IDPeriod, IDPlayer, Value, IDItem, IDAge) " _
                         & "SELECT " & iIDVariableCalculateDefine & ", " & IDPeriodoGameNew & ", IDPlayer, Value, IDItem, IDAge " _
                         & "FROM Variables_Calculate_Value " _
                         & "WHERE ID = " & Nni(drVariableCalculate("IDVariableCalculateValue"))
                    g_DAL.ExecuteNonQuery(sSQL)
                End If
            Next
        Catch ex As Exception
            txtMessageVariable.Text = "Error LoadDataVariableCalculateDefine -> " & ex.Message & "<br/>" & sSQL
            txtMessageVariable.Visible = True
        End Try
    End Sub

    Private Sub cboTeam_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles cboTeam.SelectedIndexChanged
        If Nz(cboTeam.SelectedValue) <> "" AndAlso Nz(cboTeam.SelectedValue) <> "N" Then
            If Nni(cboTeam.SelectedValue) > 0 Then
                txtTeamName.Text = cboTeam.Text
            End If
        End If
    End Sub

#End Region

End Class
