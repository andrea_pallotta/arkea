﻿Imports System.Data
Imports APS
Imports APPCore.Utility
Imports DALC4NET
Imports Telerik.Web.UI

Partial Class Tutor_Decisions_Boss
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster

    Protected Sub cboPeriodSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadBossDecisions()
        Message.Visible = False

        If Nni(Session("IDPeriod")) = HandleGetMaxPeriodValid(Session("IDGame")) Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        pnlMain.Update()
    End Sub

    Protected Sub cboTeamSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        LoadBossDecisions()
        Message.Visible = False

        If Nni(Session("IDPeriod")) = HandleGetMaxPeriodValid(Session("IDGame")) Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        pnlMain.Update()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim sSQL As String
        Dim oDTDecisionsBoss As DataTable
        Dim sVariable As String
        Dim iIDDataType As Integer
        Dim iIDVariable As Integer
        Dim iIDDecisionBossValue As Integer

        Dim sNomeTextBox As String
        Dim sNomeTeam As String
        Dim sNomeItem As String

        Dim oTXT As RadTextBox = Nothing
        Dim oDTItem As DataTable = LoadItemsGame(m_SessionData.Utente.Games.IDGame, "")

        Message.Visible = False

        ' Carico le decisioni che può prendere il boss
        oDTDecisionsBoss = HandleLoadDecisionsBoss(m_SessionData.Utente.Games.IDGame)
        For Each oRow As DataRow In oDTDecisionsBoss.Rows
            ' Per ogni variabile vado a ricercare il relativo oggetto di testo
            ' nel caso in cui sia su una variabile di tipo lista, allora procedo in modo diverso
            ' cerco i textbox con suffisso var_ e salvo i dati nella apposita tabella
            sVariable = Nz(oRow("Value"))
            iIDDataType = Nni(oRow("IDDataType"))
            iIDVariable = Nni(oRow("IDDecisionBoss"))
            iIDDecisionBossValue = Nni(oRow("IDDecisionBossValue"))

            ' Ciclo su tutti gli oggetti della tabella
            ' Controllo di avere sempre la tabella in memoria
            If sVariable.ToUpper.Contains("LIST OF") Then
                ' Controllo che non sia una variabile di tipo lista players
                If sVariable.ToUpper.Contains("PLAYERS") Then

                    ' Carico la tabella dei players/Team
                    sNomeTeam = Nz(oRow("Decision")) & "_" & GetTeamName(m_SiteMaster.TeamIDGet)
                    sNomeTextBox = "var_" & sNomeTeam
                    oTXT = TryCast(FindControlRecursive(Page, sNomeTextBox), RadTextBox)
                    If oTXT IsNot Nothing AndAlso oTXT.Text <> "" Then
                        HandleSaveVariableList(sNomeTeam, oTXT.Text, iIDDataType, iIDDecisionBossValue, g_DAL)
                        If Nni(Session("IDTeamM&U")) > 0 Then
                            If Nni(Session("IDTeam")) = Nni(Session("IDTeamMaster")) Then
                                sNomeTeam = Nz(oRow("Decision")) & Session("TeamNameM&U")
                                sNomeTextBox = "var_" & sNomeTeam
                                HandleSaveVariableList(sNomeTeam, oTXT.Text, iIDDataType, iIDDecisionBossValue, g_DAL)
                            End If
                        End If
                    End If

                ElseIf sVariable.ToUpper.Contains("ITEMS") Then ' Controllo che non sia una variabile di tipo lista Items

                    ' Carico la tabella degli Items
                    For Each oRowItem As DataRow In oDTItem.Rows
                        sNomeItem = Nz(oRow("Decision")) & "_" & Nz(oRowItem("VariableName"))
                        sNomeTextBox = "var_" & sNomeItem

                        ' HardCoded, questa variabile doveva essere di tipo state, ma l'abbiamo modificata in corso d'opera
                        If Nz(oRow("Decision")).ToUpper.Contains("HOURSPERITEM") Then
                            sNomeItem = "var_" & GetTeamName(m_SiteMaster.TeamIDGet) & "_" & Nz(oRow("Decision")) & "_" & Nz(oRowItem("VariableName"))
                            sNomeTextBox = "var_var_" & GetTeamName(m_SiteMaster.TeamIDGet) & "_" & Nz(oRow("Decision")) & "_" & Nz(oRowItem("VariableName"))
                        End If

                        oTXT = TryCast(FindControlRecursive(Page, sNomeTextBox), RadTextBox)
                        If oTXT IsNot Nothing AndAlso oTXT.Text <> "" Then
                            HandleSaveVariableList(sNomeItem, oTXT.Text, iIDDataType, iIDDecisionBossValue, g_DAL)

                            ' HardCoded, questa variabile doveva essere di tipo state, ma l'abbiamo modificata in corso d'opera
                            If Nni(Session("IDTeamM&U")) > 0 Then
                                If Nni(Session("IDTeam")) = Nni(Session("IDTeamMaster")) Then
                                    If Nz(oRow("Decision")).ToUpper.Contains("HOURSPERITEM") Then
                                        sNomeItem = "var_" & Session("TeamNameM&U") & "_" & Nz(oRow("Decision")) & "_" & Nz(oRowItem("VariableName"))
                                        sNomeTextBox = "var_var_" & Session("TeamNameM&U") & "_" & Nz(oRow("Decision")) & "_" & Nz(oRowItem("VariableName"))
                                        HandleSaveVariableList(sNomeItem, oTXT.Text, iIDDataType, iIDDecisionBossValue, g_DAL)
                                    End If
                                End If
                            End If

                        End If
                    Next
                End If

            Else
                ' E' un valore standard da salvare direttamente in archivio
                sNomeTextBox = "var_" & Nz(oRow("Decision")) & "_" & Nni(oRow("IDValue"))

                ' Splitto il nome del textbox per recuperare anche l'id della variabile da aggiornare nella tabella Decisions_Boss_Value
                Dim sSplit As String() = sNomeTextBox.Split("_")
                Dim iIDVariableValue As Integer
                If sSplit.Count > 0 Then
                    iIDVariableValue = Nni(sSplit(2))
                End If

                oTXT = TryCast(FindControlRecursive(Page, sNomeTextBox), RadTextBox)
                If oTXT IsNot Nothing AndAlso oTXT.Text <> "" Then
                    If iIDVariableValue > 0 Then
                        sSQL = "UPDATE Decisions_Boss_Value SET Value = '" & oTXT.Text & "' WHERE ID = " & iIDVariableValue
                    Else
                        sSQL = "INSERT INTO Decisions_Boss_Value (IDDecisionBoss, Value, IDPeriod) VALUES (" & iIDVariable & ", '" & oTXT.Text & "', " & m_SiteMaster.PeriodGetCurrent & ") "
                    End If
                    g_DAL.ExecuteNonQuery(sSQL)
                End If
            End If
        Next

        MessageText.Text = "Save completed"
        Message.Visible = True

        GC.Collect()
    End Sub

    Private Sub HandleSaveVariableList(VariableName As String, VariableValue As String, IDDataType As Integer, IDVariable As Integer, DAL As DBHelper)
        Dim sSQL As String
        Dim iIDVariableList As Integer
        Dim iIDPeriodo As Integer = m_SiteMaster.PeriodGetCurrent

        ' Prima di tutto recupero il tipo di varibile per fare le opportune verifiche
        Dim bTypeVariableISCorrect As Boolean = ReturnVariableDataTypeISCorrect(IDDataType, VariableValue)

        If bTypeVariableISCorrect Then ' Tutto bene procedo con il salvataggio delle informazioni

            ' Se sono all'inzio del game, forse il periodo è 0, lo inizializzo a 1 per sicurezza
            If m_SiteMaster.PeriodGetCurrent <= 0 Then iIDPeriodo = 1

            ' Controllo l'esistenza per ogni team del record nella tabella, altrimenti lo inserisco
            sSQL = "SELECT ID FROM Decisions_Boss_Value_List WHERE IDDecisionBossValue = " & IDVariable & " And VariableName = '" & VariableName & "' AND IDPeriod = " & m_SiteMaster.PeriodGetCurrent
            iIDVariableList = DAL.ExecuteScalar(sSQL)
            If iIDVariableList > 0 Then
                sSQL = "UPDATE Decisions_Boss_Value_List SET Value = '" & VariableValue & "' WHERE ID = " & iIDVariableList & " AND IDPeriod = " & m_SiteMaster.PeriodGetCurrent
            Else
                sSQL = "INSERT INTO Decisions_Boss_Value_List (IDDecisionBossValue, VariableName, Value,  IDPeriod) VALUES " _
                     & "(" & IDVariable & ",'" & VariableName & "', '" & VariableValue & "', " & iIDPeriodo & ") "
            End If
            DAL.ExecuteNonQuery(sSQL)

        End If
    End Sub

    Private Sub LoadBossDecisions()
        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblSubHeaderRow As TableRow
        Dim tblSubHeaderCell As New TableCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        Dim sNomeLink As String

        ' Recupero le informazioni necessarie per costruirmi l'header della tabella HTML
        Dim oDTHeader As DataTable = HandleLoadDecisionsBossTitle(m_SessionData.Utente.Games.IDGame)

        tblBossDecisions.Rows.Clear()

        ' Per ogni riga di intestazione inizio a costruire la tabella
        For Each oRowHeader As DataRow In oDTHeader.Rows
            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell

            tblHeaderCell.Text = Nz(oRowHeader("Title"))
            tblHeaderCell.ColumnSpan = 3
            tblHeaderCell.BorderStyle = BorderStyle.None
            tblHeaderCell.Style.Add("margin-top", "10px")
            tblHeaderRow.Cells.Add(tblHeaderCell)

            tblBossDecisions.Rows.Add(tblHeaderRow)

            ' Recupero le righe relative ai sottotitoli
            Dim oDTDecisionsBossSubTitle As DataTable = HandleLoadDecisionsBossSubTitle(Nni(oRowHeader("ID")))

            For Each oRowDecisionsBossSubTitle As DataRow In oDTDecisionsBossSubTitle.Rows
                ' Per ogni sottotitolo genero una nuova riga
                ' Costruisco la riga Header
                tblSubHeaderRow = New TableRow
                tblSubHeaderCell = New TableCell

                tblSubHeaderCell.Text = Nz(oRowDecisionsBossSubTitle("SubTitle"))
                tblSubHeaderCell.ColumnSpan = 3
                tblSubHeaderCell.BorderStyle = BorderStyle.None
                tblSubHeaderCell.Style.Add("margin-top", "10px")
                tblSubHeaderRow.Cells.Add(tblSubHeaderCell)

                tblSubHeaderRow.Style.Add("background", "#7CBBEB")
                tblSubHeaderRow.Style.Add("color", "white")
                tblSubHeaderRow.Style.Add("font-weight", "bold")
                tblSubHeaderRow.Style.Add("padding-left", "10px")
                tblSubHeaderRow.Style.Add("text-align", "left")
                tblSubHeaderRow.Style.Add("margin-top", "8px")
                tblBossDecisions.Rows.Add(tblSubHeaderRow)

                ' Recupero tutte le variabili per il sottotitolo selezionato
                Dim oDTDecisionBoss As DataTable = HandleLoadDecisionsBoss(Nni(oRowDecisionsBossSubTitle("ID")), Nni(Session("IDGame")))

                For Each oRowDecisionBoss As DataRow In oDTDecisionBoss.Rows
                    ' Per ogni linea dei sottotitoli vado a cercare i valori nella tabella Decisions_Boss_Value
                    Dim oDTDecisionsBossValue As DataTable = HandleLoadDecisionsBossValue(Nni(oRowDecisionBoss("ID")), m_SiteMaster.PeriodGetCurrent)
                    If oRowDecisionBoss("Decision").ToString.Contains("DebtWrittRate") Then
                        MessageText.Text = "Eccola!!!"
                    End If

                    For Each oRowDecisionBossValue As DataRow In oDTDecisionsBossValue.Rows
                        ' Controllo se è una variabile di tipo lista
                        ' se è di questo tipo modifico la gestione del testo
                        ' Controllo anche di non essere sempre posizionato sulla solita decisione, 
                        ' durante il primo ciclo costruisco le righe utili per la gestione delle decisioni sia di tipo ITEM che Players
                        If Nz(oRowDecisionBossValue("Value")).ToUpper.Contains("LIST OF") Then
                            If Nz(oRowDecisionBossValue("Value")).ToUpper.Contains("PLAYERS") Or Nz(oRowDecisionBossValue("Value")).ToUpper.Contains("ITEMS") Then

                                ' Recupero i dati delle variabili di tipo lista
                                Dim oDTDecisionBossValueList As DataTable =
                                    HandleLoadDecisionsBossValueList(Nni(oRowDecisionBossValue("ID")), m_SiteMaster.PeriodGetCurrent, m_SiteMaster.TeamIDGet, Session("IDGame"))

                                For Each oRowDecisionBossValueList As DataRow In oDTDecisionBossValueList.Rows
                                    ' Costruisco un datatable temporaneo per la gestione delle variabili presenti in lista
                                    ' Ciclo sugli item trovati per la composizione del nome della casella di testo 
                                    sNomeLink = "List_" & Nni(oRowDecisionBossValueList("ID")) & "_" & Nni(oRowDecisionBoss("ID"))
                                    Dim oLnk As LinkButton = TryCast(FindControlRecursive(Page, sNomeLink), LinkButton)

                                    If oLnk Is Nothing Then
                                        ' Se non ho già creato il campo, allora procedo all'inserimento
                                        ' Costruisco la riga delle decisione
                                        If FindControlRecursive(Me, "var_" & Nz(oRowDecisionBossValueList("VariableName"))) Is Nothing Then
                                            tblRow = New TableRow
                                            tblCell = New TableCell

                                            Dim sSplitName() As String = Nz(oRowDecisionBossValueList("VariableName")).Split("_")
                                            ' Etichetta della variabile
                                            oLnk = New LinkButton
                                            If sSplitName.Length >= 4 Then
                                                oLnk.Text = Nz(oRowDecisionBoss("DecisionLabel")) & "<br/>" & m_SiteMaster.TeamNameGet

                                            ElseIf sSplitName.Length = 1 Then
                                                oLnk.Text = Nz(oRowDecisionBoss("DecisionLabel")) & "<br/>" & sSplitName(0)

                                            Else
                                                oLnk.Text = Nz(oRowDecisionBoss("DecisionLabel")) & "<br/>" & sSplitName(1)

                                            End If

                                            oLnk.ID = "List_" & Nni(oRowDecisionBossValueList("ID")) & "_" & Nni(oRowDecisionBossValue("ID"))
                                            AddHandler oLnk.Click, AddressOf lnkButton_Click

                                            tblCell.Controls.Add(oLnk)
                                            tblRow.Cells.Add(tblCell)

                                            ' Campo testo per la memorizzazione e la visualizzazione della varibile
                                            Dim oTxtBox As New RadTextBox
                                            tblCell = New TableCell
                                            oTxtBox.Text = ""

                                            oTxtBox.ID = "var_" & Nz(oRowDecisionBossValueList("VariableName"))
                                            oTxtBox.Text = Nz(oRowDecisionBossValueList("Value"))

                                            oTxtBox.Attributes.Add("runat", "server")
                                            oTxtBox.Style.Add("text-align", "right")
                                            oTxtBox.Style.Add("RenderMode", "Lightweight")
                                            oTxtBox.ClientIDMode = ClientIDMode.Static
                                            oTxtBox.EnableViewState = True

                                            ' Aggiungo il textbox appena creato alla cella
                                            tblCell.Controls.Add(oTxtBox)
                                            tblCell.Style.Add("text-align", "right")
                                            tblRow.Cells.Add(tblCell)

                                            ' Etichetta per la definizione del tipo di variabile
                                            tblCell = New TableCell
                                            tblCell.Text = ReturnStandardLabel_Decisions_Variables(Nni(oRowDecisionBoss("IDDataType")))
                                            tblRow.Cells.Add(tblCell)

                                            tblBossDecisions.Rows.Add(tblRow)
                                        End If
                                    End If
                                Next

                            End If

                        Else
                            sNomeLink = "Value_" & Nni(oRowDecisionBossValue("ID")) & "_" & Nni(oRowDecisionBoss("ID"))

                            Dim oLnk As LinkButton = TryCast(FindControlRecursive(Page, sNomeLink), LinkButton)
                            If oLnk Is Nothing Then
                                ' Aggiungo il textbox appena creato alla cella
                                If FindControlRecursive(Me, "var_" & Nz(oRowDecisionBoss("Decision")) & "_" & Nni(oRowDecisionBossValue("ID"))) Is Nothing Then
                                    ' Costruisco la riga delle decisione
                                    tblRow = New TableRow
                                    tblCell = New TableCell

                                    ' Etichetta della variabile
                                    oLnk = New LinkButton
                                    oLnk.Text = Nz(oRowDecisionBoss("DecisionLabel"))
                                    oLnk.ID = "Value_" & Nni(oRowDecisionBossValue("ID")) & "_" & Nni(oRowDecisionBoss("ID"))
                                    AddHandler oLnk.Click, AddressOf lnkButton_Click

                                    tblCell.Controls.Add(oLnk)
                                    ' tblCell.Text = Nz(oDRVDecisionsBoss(iDecisionBoss)("DecisionLabel"))
                                    tblRow.Cells.Add(tblCell)

                                    ' Campo testo per la memorizzazione e la visualizzazione della varibile
                                    Dim oTxtBox As New RadTextBox
                                    tblCell = New TableCell
                                    oTxtBox.Text = ""

                                    oTxtBox.ID = "var_" & Nz(oRowDecisionBoss("Decision")) & "_" & Nni(oRowDecisionBossValue("ID"))
                                    oTxtBox.Text = Nz(oRowDecisionBossValue("Value"))

                                    oTxtBox.Attributes.Add("runat", "server")
                                    oTxtBox.Style.Add("text-align", "right")
                                    oTxtBox.Style.Add("RenderMode", "Lightweight")
                                    oTxtBox.ClientIDMode = ClientIDMode.Static
                                    oTxtBox.EnableViewState = True


                                    tblCell.Controls.Add(oTxtBox)
                                    tblCell.Style.Add("text-align", "right")

                                    tblRow.Cells.Add(tblCell)

                                    ' Etichetta per la definizione del tipo di variabile
                                    tblCell = New TableCell
                                    tblCell.Text = ReturnStandardLabel_Decisions_Variables(Nni(oRowDecisionBoss("IDDataType")))
                                    tblRow.Cells.Add(tblCell)

                                    tblBossDecisions.Rows.Add(tblRow)
                                End If


                            End If
                        End If
                    Next
                Next
            Next
            ' Inserisco una riga vuota

            tblRow = New TableRow
            tblCell = New TableCell

            tblCell.ColumnSpan = 3
            tblCell.BorderStyle = BorderStyle.None
            tblCell.Style.Add("min-height", "10px")
            tblCell.BorderStyle = BorderStyle.None

            tblRow.Cells.Add(tblCell)

            tblBossDecisions.Rows.Add(tblRow)
        Next
    End Sub

#Region "GESTIONE DEL PANNELLO"

    Private Sub HandleLoadDataType()
        Dim oDTDataType As DataTable
        Dim sSQL As String = "SELECT -1 AS ID, '< Select data type...>' AS TypeDescription UNION ALL SELECT ID, TypeDescription FROM Variables_Type "
        Dim oDAL As New DBHelper

        Try
            cboDataType.Items.Clear()
            cboDataType.DataSource = Nothing
            oDTDataType = oDAL.ExecuteDataTable(sSQL)
            cboDataType.DataSource = oDTDataType
            cboDataType.DataValueField = "Id"
            cboDataType.DataTextField = "TypeDescription"
            cboDataType.DataBind()
            cboDataType.SelectedValue = -1
            SQLConnClose(oDAL.GetConnObject)
        Catch ex As Exception
            MessageText.Text = "Tutor_Decisions_Boss.aspx -> cboDataType" & "<br/>" & ex.Message & "<br/>" & g_MessaggioErrore & "<br/> "
            Message.Visible = True
        End Try
    End Sub

    Private Sub ClearPanelNewDecision()
        Session("IDVariableLoad") = Nothing
        Session("IDVariable") = Nothing
        Session("TypeVariable") = Nothing

        txtTitle.Text = ""
        txtDecision.Text = ""
        txtDecisionLabel.Text = ""
        cboDataType.SelectedValue = -1

        chkItem.Checked = False
        chkPlayers.Checked = False
    End Sub

    Private Sub btnHide_Click(sender As Object, e As EventArgs) Handles btnHide.Click
        ClearPanelNewDecision()

        mpeMain.Hide()
    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        Dim sSQL As String = ""
        Dim iIDTitle As Integer
        Dim iIDSubTitle As Integer
        Dim bVerificaTransazione As Boolean = True
        Dim oTrans As IDbTransaction = Nothing

        Try
            Message.Visible = False
            ' Ho una variabile valida, procedo con l'aggiornamento
            ' Controllo che esista la descrizione per il titolo altrimenti la creo
            oTrans = g_DAL.BeginTransaction()

            iIDTitle = HandleVerifyTitle(oTrans)
            iIDSubTitle = HandleVerifySubTitle(oTrans, iIDTitle)

            ' Procedo con il salvataggio della nuova decisione
            If Nni(Session("IDVariableLoad")) <> 0 Then
                If txtDecision.Text <> "" Then
                    sSQL = "UPDATE Decisions_Boss SET IDSubTitle = " & iIDSubTitle & ", Decision = '" & txtDecision.Text.Trim.Replace("'", " ") & "', " _
                         & "IDDataType = " & cboDataType.SelectedValue & ", DecisionLabel = '" & txtDecisionLabel.Text.Trim.Replace("'", " ") & "' " _
                         & "WHERE ID = " & Nni(Session("IDVariableLoad"))
                    g_DAL.ExecuteNonQuery(sSQL, oTrans)
                End If

            Else ' Altrimenti verifico la presenza della descrizione e procedo con l'inserimento di una nuova
                If txtDecision.Text <> "" Then
                    sSQL = "INSERT INTO Decisions_Boss (IDSubTitle, Decision, IDDataType, DecisionLabel, IDGame) VALUES (" _
                         & iIDSubTitle & ", '" & txtDecision.Text.Trim.Replace("'", " ") & "', " & Nni(cboDataType.SelectedValue) & ", " _
                         & "'" & txtDecisionLabel.Text.Trim.Replace("'", " ") & "', " & m_SessionData.Utente.Games.IDGame & ") "
                    g_DAL.ExecuteNonQuery(sSQL, oTrans)
                End If

            End If

            ' Se ho inserito una nuova variabile provvedo anche all'inserimento del valore di default nella tabella dei valori
            If sSQL.Contains("INSERT") Then
                ' Recupero l'ultimo ID inserito
                sSQL = "SELECT MAX(ID) FROM Decisions_Boss WHERE IDGame = " & m_SessionData.Utente.Games.IDGame
                Dim iIDDecisionBoss As Integer = Nni(g_DAL.ExecuteScalar(sSQL, oTrans))

                ' Con l'ID della variabile mi recupero anche la descrizione per comporre il nome della varibile da salvare nella tabella di tipo lista
                Dim sNomeVariabile As String
                sSQL = "SELECT Decision FROM Decisions_Boss WHERE ID = " & iIDDecisionBoss
                sNomeVariabile = Nz(g_DAL.ExecuteScalar(sSQL, oTrans))

                ' Controllo de deve essere una lista di ITEMS o PLAYERS
                If chkItem.Checked Then
                    ' Carico tutti gli Items presenti in archivio
                    Dim oDTItem As DataTable = LoadItemsGame(m_SessionData.Utente.Games.IDGame, "")
                    Dim sNomeItem As String

                    sSQL = "INSERT INTO Decisions_Boss_Value (IDDecisionBoss, Value, IDPeriod) VALUES (" & iIDDecisionBoss & ", '[List of items]', " & m_SiteMaster.PeriodGetCurrent & ") "
                    g_DAL.ExecuteNonQuery(sSQL, oTrans)
                    Dim iIDDecisionBossValue As Integer
                    sSQL = "SELECT MAX(ID) FROM Decisions_Boss_Value WHERE IDDecisionBoss = " & iIDDecisionBoss
                    iIDDecisionBossValue = Nni(g_DAL.ExecuteScalar(sSQL, oTrans))

                    For Each oRowItem As DataRow In oDTItem.Rows
                        sNomeItem = sNomeVariabile & "_" & Nz(oRowItem("VariableName"))
                        HandleSaveVariableList(0, iIDDecisionBossValue, sNomeItem, m_SiteMaster.PeriodGetCurrent, oTrans)
                    Next

                ElseIf chkPlayers.Checked Then
                    ' Carico tutti i Players presenti in archivio
                    Dim oDTTeams As DataTable = LoadTeamsGame(m_SessionData.Utente.Games.IDGame)
                    Dim sNomeTeam As String

                    sSQL = "INSERT INTO Decisions_Boss_Value (IDDecisionBoss, Value, IDPeriod) VALUES (" & iIDDecisionBoss & ", '[List of players]', " & m_SiteMaster.PeriodGetCurrent & ") "
                    g_DAL.ExecuteNonQuery(sSQL, oTrans)

                    Dim iIDDecisionBossValue As Integer
                    sSQL = "SELECT MAX(ID) FROM Decisions_Boss_Value WHERE IDDecisionBoss = " & iIDDecisionBoss
                    iIDDecisionBossValue = Nni(g_DAL.ExecuteScalar(sSQL, oTrans))
                    For Each oRowTeams As DataRow In oDTTeams.Rows
                        sNomeTeam = sNomeVariabile & "_" & Nz(oRowTeams("TeamName"))
                        HandleSaveVariableList(0, iIDDecisionBossValue, sNomeTeam, m_SiteMaster.PeriodGetCurrent, oTrans)

                        If Nni(Session("IDTeamM&U")) > 0 Then
                            If Nni(Session("IDTeam")) = Nni(Session("IDTeamMaster")) Then
                                sNomeTeam = sNomeVariabile & "_M&U"
                                HandleSaveVariableList(0, iIDDecisionBossValue, sNomeTeam, m_SiteMaster.PeriodGetCurrent, oTrans)
                            End If
                        End If
                    Next
                Else

                    sSQL = "INSERT INTO Decisions_Boss_Value (IDDecisionBoss, Value, IDPeriod) VALUES (" & iIDDecisionBoss & ", 0, " & m_SiteMaster.PeriodGetCurrent & ") "
                    g_DAL.ExecuteNonQuery(sSQL, oTrans)
                End If

            Else
                ' Sono andato in update, controllo se ho modificato variabili di tipo lista ed eseguo un update Unico
                If chkItem.Checked Or chkPlayers.Checked Then
                    ' Recupero tutte le righe con l'ID della decisione selezionata
                    sSQL = "SELECT TOP 1 * FROM vDecisions_Boss WHERE IDDecisionBossValue = " & Nni(Session("IDVariableLoad"))
                    Dim oCMD As IDbCommand = Nothing
                    oCMD = g_DAL.GetCommand(sSQL, oTrans, CommandType.Text)
                    oCMD.Transaction = oTrans
                    Dim oDTRecSel As New DataTable
                    oDTRecSel.Load(oCMD.ExecuteReader())

                    For Each oRow As DataRow In oDTRecSel.Rows
                        ' Mi splitto il primo nome della variabile e modifico solo la parte relativa alla decisione (decisione_items)
                        Dim sNomeVariabileSplit() As String = Nz(oRow("VariableName")).Split("_")

                        If Nz(sNomeVariabileSplit(0)) = "var" Then
                            sSQL = "UPDATE Decisions_Boss_Value_List SET VariableName = '" & CStrSql(oRow("VariableName")) & "', '" & txtDecision.Text & "') " _
                                 & "WHERE IDDecisionBossValue = " & Nni(Session("IDVariableLoad") & " AND IDPeriod = " & m_SiteMaster.PeriodGetCurrent)
                        Else
                            sSQL = "UPDATE Decisions_Boss_Value_List SET VariableName = REPLACE(VariableName, '" & Nz(sNomeVariabileSplit(0)) & "', '" & txtDecision.Text & "') " _
                                 & "WHERE IDDecisionBossValue = " & Nni(Session("IDVariableLoad")) & " AND IDPeriod = " & m_SiteMaster.PeriodGetCurrent
                        End If

                        g_DAL.ExecuteScalar(sSQL, oTrans, CommandType.Text)
                    Next
                End If
            End If
            g_DAL.CommitTransaction(oTrans)
            oTrans.Dispose()
            bVerificaTransazione = False

            g_DAL.GetConnObject.Close()
            g_DAL.GetConnObject.Dispose()

            LoadBossDecisions()
            pnlMain.Update()

            ClearPanelNewDecision()

            mpeMain.Hide()
        Catch ex As Exception
            If Not oTrans Is Nothing AndAlso bVerificaTransazione Then g_DAL.RollbackTransaction(oTrans)
            MessageText.Text = "Error saving -> " & ex.Message
            Message.Visible = True
        End Try

    End Sub

#End Region

    Private Sub lnkButton_Click(sender As Object, e As EventArgs)
        Dim oLnk As LinkButton = CType(sender, LinkButton)
        Dim sVariable As String = oLnk.ID

        ' Splitto l'ID per identificare se ho cliccato su un link di tipo lista o di tipo valore
        Dim sVariabileSplit() As String = sVariable.Split("_")

        ' Controllo il primo item dell'array 
        If sVariabileSplit(0).ToUpper.Contains("LIST") Then
            Session("TypeVariable") = "LIST"
        ElseIf sVariabileSplit(0).ToUpper.Contains("VALUE") Then
            Session("TypeVariable") = "SINGLEVALUE"
        End If

        Session("IDVariable") = Nni(sVariabileSplit(1))
        Session("IDVariableLoad") = Nni(sVariabileSplit(2))
        mpeMain.Show()

    End Sub

    Private Sub Tutor_Decisions_Boss_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsNothing(m_SessionData) Then
            PageRedirect("ErrorTimeout.aspx", "", "")
        End If

        If Not IsPostBack Then
            HandleLoadDataType()
        End If
    End Sub

    Private Function HandleVerifyTitle(Trans As IDbTransaction) As Integer
        Dim sSQL As String
        Dim iIDTitle As Integer

        ' Ho una variabile valida, procedo con l'aggiornamento
        ' Controllo che esista la descrizione per il titolo altrimenti la creo
        sSQL = "SELECT ID FROM Decisions_Boss_Title WHERE Title = '" & txtTitle.Text.Replace("'", " ").Trim & "' AND IDGame = " & m_SessionData.Utente.Games.IDGame
        iIDTitle = g_DAL.ExecuteScalar(sSQL, Trans)
        If iIDTitle = 0 Then
            sSQL = "INSERT INTO Decisions_Boss_Title (Title, IDGame) VALUES ('" & txtTitle.Text.Replace("'", " ").Trim & "', " & m_SessionData.Utente.Games.IDGame & ") "
            g_DAL.ExecuteNonQuery(sSQL)
            ' Recupero l'ultimo ID Inserito
            sSQL = "SELECT MAX(ID) FROM Decisions_Boss_Title WHERE Title = '" & txtTitle.Text.Replace("'", " ").Trim & "' AND IDGame = " & m_SessionData.Utente.Games.IDGame
            iIDTitle = g_DAL.ExecuteScalar(sSQL, Trans)
        End If
        Return iIDTitle

    End Function

    Private Function HandleVerifySubTitle(Trans As IDbTransaction, IDTitle As Integer) As Integer
        Dim sSQL As String
        Dim iIDSubTitle As Integer

        ' Ho una variabile valida, procedo con l'aggiornamento
        ' Controllo che esista la descrizione per il titolo altrimenti la creo
        sSQL = "SELECT ID FROM Decisions_Boss_SubTitle WHERE IDTitle = " & IDTitle & " AND SubTitle = '" & txtSubTitle.Text.Replace("'", " ").Trim & "' "
        iIDSubTitle = g_DAL.ExecuteScalar(sSQL, Trans)
        If iIDSubTitle = 0 Then
            sSQL = "INSERT INTO Decisions_Boss_SubTitle (IDTitle, SubTitle) VALUES (" & IDTitle & ", '" & txtSubTitle.Text.Replace("'", " ").Trim & "') "
            g_DAL.ExecuteNonQuery(sSQL)

            ' Recupero l'ultimo ID Inserito
            sSQL = "SELECT MAX(ID) FROM Decisions_Boss_SubTitle WHERE IDTitle = " & IDTitle & " "
            iIDSubTitle = g_DAL.ExecuteScalar(sSQL, Trans)
        End If
        Return iIDSubTitle

    End Function

    Private Sub HandleSaveVariableList(ID As Integer, IDDecisionBossValue As Integer, VariableName As String, IDPeriod As Integer, Trans As IDbTransaction)
        Dim sSQL As String
        If ID > 0 Then ' Sono in aggiornamento
            sSQL = "UPDATE Decisions_Boss_Value_List SET VariableName = '" & CStrSql(VariableName) & "' "
            g_DAL.ExecuteNonQuery(sSQL, Trans)

        Else ' Sono in inserimento
            sSQL = "INSERT INTO Decisions_Boss_Value_List (IdDecisionBossValue, IDPeriod, VariableName) VALUES (" _
                 & IDDecisionBossValue & ", " & IDPeriod & ", '" & CStrSql(VariableName) & "') "
            g_DAL.ExecuteNonQuery(sSQL, Trans)
        End If

    End Sub

    Private Sub btnNewDecision_Click(sender As Object, e As EventArgs) Handles btnNewDecision.Click
        ' Svuoto eventuali variabili di sessione usate
        Session("IDVariable") = 0
        Session("TypeVariable") = ""
        txtDecision.Text = ""
        txtDecisionLabel.Text = ""
        txtTitle.Text = ""
        cboDataType.SelectedValue = -1
        chkItem.Checked = False
        chkPlayers.Checked = False
    End Sub

    Private Sub pnlNewDecision_PreRender(sender As Object, e As EventArgs) Handles pnlNewDecision.PreRender
        If Not Session("IDVariableLoad") Is Nothing Then
            Dim sSQL As String

            ' Carico i campi del pannello
            ' Recupero tutto le informazioni necessarie per caricare i dati
            If Session("TypeVariable") = "LIST" Then
                sSQL = "SELECT TOP 1 * FROM vDecisions_Boss WHERE IDDecisionBossValue = " & Nni(Session("IDVariableLoad"))
            Else
                sSQL = "SELECT TOP 1 * FROM vDecisions_Boss WHERE IDDecision = " & Nni(Session("IDVariableLoad"))
            End If

            Dim oDTDecision As DataTable = g_DAL.ExecuteDataTable(sSQL)
            For Each oRow As DataRow In oDTDecision.Rows
                txtTitle.Text = Nz(oRow("Title"))
                txtSubTitle.Text = Nz(oRow("SubTitle"))
                txtDecision.Text = Nz(oRow("Decision"))
                txtDecisionLabel.Text = Nz(oRow("DecisionLabel"))
                cboDataType.SelectedValue = Nni(oRow("IDDataType"))

                If Nz(oRow("Value")).ToUpper.Contains("ITEMS") Then
                    chkItem.Checked = True
                End If

                If Nz(oRow("Value")).ToUpper.Contains("PLAYERS") Then
                    chkPlayers.Checked = True
                End If
            Next

        End If
    End Sub

    Private Sub Tutor_Decisions_Boss_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        m_SessionData = Session("SessionData")

        m_SiteMaster = Me.Master

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oCBOPeriod As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboTeam"), RadComboBox)
        AddHandler m_SiteMaster.cboPeriodSelectedIndexChange, AddressOf cboPeriodSelectedIndexChanged
        AddHandler m_SiteMaster.cboTeamSelectedIndexChange, AddressOf cboTeamSelectedIndexChanged

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(m_SessionData.Utente.Games.IDGame) ' Ultimo periodo
        oCBOPeriod.SelectedValue = Session("IDPeriodValid")
        m_SiteMaster.PeriodChange = Session("IDPeriodValid")

        Session("NameTeam") = GetTeamName(Session("IDTeam"))
        oCBOTeam.SelectedValue = Session("IDTeam")
        m_SiteMaster.TeamChange = Session("IDTeam")

        LoadBossDecisions()

    End Sub

End Class

