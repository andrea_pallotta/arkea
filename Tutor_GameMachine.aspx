﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Tutor_GameMachine.aspx.vb" Inherits="Tutor_Tutor_GameMachine" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="cntMain" ContentPlaceHolderID="MainContent" runat="Server">
    <script type="text/javascript">
        function openConfirmBox() {
            var answer = confirm("Do you want to crete a new period?");
            document.getElementById('<%= ConfirmAnswer.ClientID %>').value = answer;
        }

        function openConfirmBoxDelete() {
            var answer = confirm("Do you want to delete actual period?");
            document.getElementById('<%= ConfirmAnswer.ClientID %>').value = answer;
        }

    </script>

    <link href="Content/StyleTimeMachine.css" rel="stylesheet" type="text/css" />

    <asp:UpdatePanel ID="pnlMainUpdate" runat="server" UpdateMode="Conditional" ClientIDMode="Static">
        <ContentTemplate>

            <asp:HiddenField ID="ConfirmAnswer" runat="server" ClientIDMode="Static" />

            <h2>
                <asp:Label ID="lblWelcome" runat="server" Text="Tutor administration"></asp:Label>
            </h2>

            <div class="clr"></div>

            <h3>
                <asp:Label ID="lblGameMachine" runat="server" Text="Time machine"></asp:Label>
            </h3>

            <div class="clr"></div>

            <div class="row">
                <div class="tableTimeMachine">
                    <table>
                        <tr>
                            <td colspan="4">Actions
                            </td>

                        </tr>
                        <tr>
                            <td colspan="2" style="width: 45%">
                                <telerik:RadCheckBox ID="chkSimulate" runat="server" Text="Simulate" AutoPostBack="false" RenderMode="Lightweight"></telerik:RadCheckBox>

                                <telerik:RadButton ID="btnNewPeriod" runat="server" Text="Generate + 1 (New period)" Width="100%" RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                                </telerik:RadButton>
                            </td>

                            <td colspan="2">Generate a new simulation period.
                                <br />
                                A new preview scenario is created with a new period. 
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <telerik:RadButton ID="btnErasePeriod" runat="server" Text="Generate - 1 (Erase current period)" Width="100%" RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                                </telerik:RadButton>
                            </td>

                            <td colspan="2">Erase the last simulation period generated.
                                <br />
                                A new scenario is created with a period less. 
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" style="background: #55A2DF; text-align: center; color: #fff; font-family: Verdana; font-weight: bold; font-size: 12px;">Scenario
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <telerik:RadButton ID="btnScenario" runat="server" Text="Scenario" Width="100%"
                                    RenderMode="Lightweight" CssClass="btnShadows">
                                </telerik:RadButton>
                            </td>
                            <td colspan="2">Bring to the scenario setting page, where you can, if you want, change the scenario variables (the Journal).
                                <br />
                                Note: the changes are made on the current view.
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:PlaceHolder runat="server" ID="Message" Visible="false">
                                    <p class="text-danger">
                                        <asp:Literal runat="server" ID="MessageText" />
                                    </p>
                                </asp:PlaceHolder>
                            </td>
                        </tr>
                    </table>

                </div>

            </div>

            <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="pnlMainUpdate" ClientIDMode="Static">
                <ProgressTemplate>
                    <asp:Panel ID="Panel1" CssClass="overlay" runat="server">
                        <asp:Panel ID="Panel2" CssClass="loader" runat="server">
                            <img alt="" src="Images/Loading.gif" />
                        </asp:Panel>
                    </asp:Panel>
                </ProgressTemplate>
            </asp:UpdateProgress>

        </ContentTemplate>
    </asp:UpdatePanel>


</asp:Content>

