﻿Imports APS
Imports DALC4NET
Imports APPCore.Utility
Imports System.Data
Imports Telerik.Web.UI

Partial Class Tutor_Tutor_GameMachine
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster
    Private m_IDPeriodoAttuale As Integer

    Private Sub HandleRefreshPeriod()
        Dim oSiteMaster As SiteMaster = Me.Master
        Dim oPNLMaster As UpdatePanel = oSiteMaster.FindControl("pnlMainUpdate")
        Dim oCBOPeriod As RadComboBox = oSiteMaster.FindControl("cboPeriod")
        oPNLMaster.Update()

        oSiteMaster.LoadPeriods()

        ' Posiziono il periodo sull'ultimo
        oCBOPeriod.SelectedValue = Nni(HandleGetMaxPeriodValid(m_SessionData.Utente.Games.IDGame))

        oSiteMaster.Dispose()
        oPNLMaster.Dispose()
    End Sub

    Protected Friend Sub btnNewPeriod_Click(sender As Object, e As EventArgs) Handles btnNewPeriod.Click
        Dim iIDPeriodoAttuale As Integer
        Dim iIDPeriodoMax As Integer
        Dim sSQL As String = "SELECT MAX(IDPeriodo) FROM BGOL_Games_Periods WHERE IDGame = " & Session("IDGame")
        Dim iStepMax As Integer
        Dim iStepCurrent As Integer
        Dim dtStart As DateTime = Now
        Dim dtEnd As DateTime
        Dim bSimulazione As Boolean = False

        Try
            For Each entry As System.Collections.DictionaryEntry In HttpContext.Current.Cache
                HttpContext.Current.Cache.Remove(DirectCast(entry.Key, String))
            Next

            If IsNothing(chkSimulate.Checked) Then
                bSimulazione = False
            Else
                bSimulazione = chkSimulate.Checked
            End If

            MessageText.Text = ""
            Message.Visible = False

            If Nz(ConfirmAnswer.Value, "FALSE").ToUpper = "FALSE" Then Exit Try

            iIDPeriodoAttuale = Nni(g_DAL.ExecuteScalar(sSQL))

            ' Recupero lo step del periodo
            sSQL = "SELECT NumStep FROM BGOL_Periods WHERE IDGame = " & Session("IDGame") & " AND ID = " & Session("IDPeriod")
            iStepCurrent = Nni(g_DAL.ExecuteScalar(sSQL))

            ' Recupero lo step massimo
            sSQL = "SELECT ISNULL(MAX(NumStep), " & iStepCurrent + 1 & ") FROM BGOL_Periods WHERE IDGame = " & Session("IDGame")
            iStepMax = Nni(g_DAL.ExecuteScalar(sSQL))

            ' Con lo step e il game recupero l'ID del periodo
            sSQL = "SELECT MAX(ID) FROM BGOL_Periods WHERE NumStep >= " & iStepMax & " AND IDGame = " & Session("IDGame")
            iIDPeriodoMax = Nni(g_DAL.ExecuteScalar(sSQL))

            Dim oNewEngine As New Engine_Arkea(Session("IDGame"), iIDPeriodoAttuale, bSimulazione, iStepCurrent)

            MessageText.Text = oNewEngine.Calcola
            Message.Visible = True

            If MessageText.Text.ToUpper.Contains("COMPLETE") Then
                ' Recupero il primo IDPeriodo utile successivo a questo
                sSQL = "SELECT MIN(ID) FROM BGOL_Periods WHERE NumStep > " & iStepCurrent & " AND IDGame = " & Session("IDGame")
                Dim iIDPeriodo As Integer = g_DAL.ExecuteScalar(sSQL)

                ' Devo generare le decisioni dei player
                GenerateDecisionsPlayers(iIDPeriodoAttuale, iIDPeriodo, bSimulazione)

                ' Devo generare le decisioni del Boss
                GenerateDecisionsBoss(iIDPeriodoAttuale, iIDPeriodo, bSimulazione)

                ' Generazione delle decisioni developer
                GenerateDecisionsDeveloper(iIDPeriodoAttuale, iIDPeriodo, bSimulazione)

                ' Devo controllare quelle variabili di stato non movimentate dal flusso
                GenerateVariablesState(iIDPeriodoAttuale, iIDPeriodo)

                ' Avvio il calcolo per il gioco 
                If iStepCurrent <= iStepMax Then
                    If Nz(ConfirmAnswer.Value, "FALSE").ToUpper = "TRUE" AndAlso iStepCurrent < iStepMax Then
                        ' Ora genero il periodo corretto nella tabella di gioco
                        sSQL = "INSERT INTO BGOL_Games_Periods (IDGame, IDPeriodo, Simulation) VALUES (" & Session("IDGame") & ", " & iIDPeriodo & ", " & Boolean_To_Bit(bSimulazione) & ")"
                        g_DAL.ExecuteNonQuery(sSQL)

                        dtEnd = Now()

                        Dim secondsDiff As Long = DateDiff(DateInterval.Second, dtEnd, dtStart)

                        MessageText.Text = "Generation completed"
                        Message.Visible = True
                    Else
                        If iStepCurrent = iStepMax Then
                            MessageText.Text = "<h2>" & "Completed the final calculation. You can show the final results." & "<br/>" & "Please contact your administrator" & "<h2/>"
                            Message.Visible = True
                        Else
                            MessageText.Text = "<h2>" & "You can't generate a new period, the game is over." & "<br/>" & "Please contact your administrator" & "<h2/>"
                            Message.Visible = True
                        End If
                    End If

                    HandleRefreshPeriod()
                End If
                ' Vado a ricaricare i dati del periodo per settare la variabile di sessione correttamente
                Session("IDPeriod") = iIDPeriodo
            End If

            UpdateProgress1.Visible = False
            pnlMainUpdate.Update()

        Catch ex As Exception
            MessageText.Text = ex.Message & "<br/>" & g_MessaggioErrore & "<br/> "
            Message.Visible = True
        End Try

    End Sub

    Private Sub GenerateVariablesState(IDPeriod As Integer, IDPeriodGenerate As Integer)
        Dim sSQL As String
        Dim oDTVariablesState As DataTable = HandleLoadVariableState(Session("IDGame"))
        Dim iIDVariableState As Integer
        Dim oDTVariablesStateValue As DataTable

        Try
            g_MessaggioErrore = ""
            ' Ciclo su tutte le variaibli e controllo che non siano già presenti nel periodo da generare, possono essere già state calcolate dal motore
            For Each oRowVariable As DataRow In oDTVariablesState.Rows
                iIDVariableState = oRowVariable("ID")

                ' Con l'id appena caricato verifico la presenza nella tabella VARIABLES_STATE_VALUE
                sSQL = "SELECT * FROM Variables_State_Value WHERE IDVariable = " & iIDVariableState & " AND ISNULL(IDPeriod, " & IDPeriodGenerate & ") = " & IDPeriodGenerate
                oDTVariablesStateValue = g_DAL.ExecuteDataTable(sSQL)

                ' Non ci sono variabili di stato per il periodo da generare, allora le creo
                If oDTVariablesStateValue.Rows.Count = 0 Then
                    sSQL = "INSERT INTO Variables_State_Value (IDVariable, Value, IDPeriod, IDPlayer, IDItem, IDAge) " _
                         & "SELECT " & iIDVariableState & ", Value, " & IDPeriodGenerate & ", IDPlayer, IDItem, IDAge " _
                         & "FROM Variables_State_Value " _
                         & "WHERE IDPeriod = " & IDPeriod & " AND IDVariable = " & iIDVariableState
                    g_DAL.ExecuteNonQuery(sSQL)

                Else
                    ' Controllo se per la variaible di stato ci sono delle righe nella tabella contente liste di valori
                    ' se non ci sono per il periodo che sto generando allora le creo.
                    sSQL = "SELECT * FROM Variables_State_Value_List WHERE IDPeriod = " & IDPeriodGenerate & " AND " _
                         & "IDVariableState = " & Nni(oDTVariablesStateValue.Rows(0)("ID"))
                    Dim oDTVariablesStateValueList As DataTable = g_DAL.ExecuteDataTable(sSQL)
                    If oDTVariablesStateValueList.Rows.Count = 0 Then ' Non ho trovato niente, per cui le creo
                        sSQL = "INSERT INTO Variables_State_Value_List (IDVariableState, IDPlayer, IDItem, IDAge, Value, VariableName, IDPeriod) " _
                             & "SELECT IDVariableState, IDPlayer, IDItem, IDAge, Value, VariableName, " & IDPeriodGenerate & " " _
                             & "FROM Variables_State_Value_List WHERE IDVariableState = " & Nni(oDTVariablesStateValue.Rows(0)("ID")) _
                             & " AND IDPeriod = " & IDPeriod
                        g_DAL.ExecuteNonQuery(sSQL)
                    End If
                End If

            Next
        Catch ex As Exception
            MessageText.Text = ex.Message & "<br/>"
            Message.Visible = True
        End Try
    End Sub

    Private Sub GenerateDecisionsPlayers(IDPeriodo As Integer, IDPeriodGenerate As Integer, Simulazione As Boolean)
        Dim sSQL As String
        Dim oDTTeams As DataTable = LoadTeamsGame(Session("IDGame"))

        Try
            ' Cancello eventuali decisioni rimaste in memoria per il periodo da generare
            sSQL = "DELETE FROM Decisions_Players_Value WHERE IDperiod = " & IDPeriodGenerate
            g_DAL.ExecuteNonQuery(sSQL)

            sSQL = "DELETE FROM Decisions_Players_Value_List WHERE IDperiod = " & IDPeriodGenerate
            g_DAL.ExecuteNonQuery(sSQL)

            sSQL = "INSERT INTO Decisions_Players_Value (IDDecisionPlayer, IDPeriod, IDPlayer, VariableValue) " _
                 & "SELECT DISTINCT IDDecisionPlayer, " & IDPeriodGenerate & ", IDPlayer, VariableValue " _
                 & "FROM Decisions_Players_Value WHERE IDPeriod = " & IDPeriodo
            g_DAL.ExecuteNonQuery(sSQL)

            sSQL = "INSERT INTO Decisions_Players_Value_List (IDDecisionValue, IDPlayer, IDPeriod, VariableName, Value, IDItem, IDAge, Simulation) " _
                 & "SELECT DISTINCT IDDecisionValue, IDPlayer, " & IDPeriodGenerate & ", VariableName, Value, IDItem, IDAge, " & Boolean_To_Bit(Simulazione) & " " _
                 & "FROM Decisions_Players_Value_List WHERE IDPeriod = " & IDPeriodo
            g_DAL.ExecuteNonQuery(sSQL)

            SQLConnClose(g_DAL.GetConnObject)
        Catch ex As Exception
            SQLConnClose(g_DAL.GetConnObject)
            MessageText.Text = "GenerateDecisionsPlayers -> " & ex.Message
            Message.Visible = True
        End Try

    End Sub

    Private Sub GenerateDecisionsBoss(IDPeriodo As Integer, IDPeriodGenerate As Integer, Simulazione As Boolean)
        Dim sSQL As String
        Dim sIDDecisionValue As String = ""
        Dim oDTDecisionValue As DataTable

        sSQL = "DELETE FROM Decisions_Boss_Value WHERE IDperiod = " & IDPeriodGenerate & " AND IDDecisionBoss NOT IN(SELECT ID FROM Decisions_Boss WHERE Decision IN('ProduUnitCost', 'HoursPerItem') AND IDGame = " & Session("IDGame") & ") "
        g_DAL.ExecuteNonQuery(sSQL)

        sSQL = "SELECT ID FROM Decisions_Boss_Value WHERE IDDecisionBoss IN (SELECT ID FROM Decisions_Boss WHERE Decision IN('ProduUnitCost', 'HoursPerItem') AND IDGame = " & Session("IDGame") & ")"
        oDTDecisionValue = g_DAL.ExecuteDataTable(sSQL)
        For Each oRow As DataRow In oDTDecisionValue.Rows
            sIDDecisionValue &= oRow("ID") & ","
        Next
        sIDDecisionValue = sIDDecisionValue.TrimEnd(",")

        sSQL = "DELETE FROM Decisions_Boss_Value_List WHERE IDperiod = " & IDPeriodGenerate & " And IDDecisionBossValue Not In(" & sIDDecisionValue & ") "
        g_DAL.ExecuteNonQuery(sSQL)

        sSQL = "INSERT INTO Decisions_Boss_Value (IDDecisionBoss, IDPeriod, Value, Simulation) " _
             & "SELECT DISTINCT IDDecisionBoss, " & IDPeriodGenerate & ", Value, " & Boolean_To_Bit(Simulazione) & " " _
             & "FROM Decisions_Boss_Value WHERE IDPeriod = " & IDPeriodo & " AND IDDecisionBoss NOT IN(SELECT ID FROM Decisions_Boss WHERE Decision IN('ProduUnitCost', 'HoursPerItem') AND IDGame = " & Session("IDGame") & ") " _
             & "AND Value Not In('[List of items]', '[List of players]')"
        g_DAL.ExecuteNonQuery(sSQL)

        sSQL = "INSERT INTO Decisions_Boss_Value_List (IDDecisionBossValue, IDPeriod, VariableName, Value, Simulation) " _
             & "SELECT DISTINCT IDDecisionBossValue, " & IDPeriodGenerate & ", VariableName, Value, " & Boolean_To_Bit(Simulazione) & " " _
             & "FROM Decisions_Boss_Value_List WHERE IDPeriod = " & IDPeriodo & " AND IDDecisionBossValue NOT IN(" & sIDDecisionValue & ") "
        g_DAL.ExecuteNonQuery(sSQL)

    End Sub

    Private Sub GenerateDecisionsDeveloper(IDPeriodo As Integer, IDPeriodGenerate As Integer, Simulazione As Boolean)
        Dim sSQL As String

        ' Cancello eventuali decisioni rimaste in memoria per il periodo da generare
        sSQL = "DELETE FROM Decisions_Developer_Value WHERE IDperiod = " & IDPeriodGenerate
        g_DAL.ExecuteNonQuery(sSQL)

        sSQL = "DELETE FROM Decisions_Developer_Value_List WHERE IDperiod = " & IDPeriodGenerate
        g_DAL.ExecuteNonQuery(sSQL)

        sSQL = "INSERT INTO Decisions_Developer_Value (IDDecisionDeveloper, IDPeriod, Value, IDPlayer, IDItem, Simulation) " _
             & "SELECT DISTINCT IDDecisionDeveloper, " & IDPeriodGenerate & ", Value, IDPlayer, IDItem, " & Boolean_To_Bit(Simulazione) & " " _
             & "FROM Decisions_Developer_Value WHERE IDPeriod = " & IDPeriodo
        g_DAL.ExecuteNonQuery(sSQL)

        sSQL = "INSERT INTO Decisions_Developer_Value_List (IDDecisionValue, IDPeriod, VariableName, Value, IDPlayer, IDItem, IDAge, Simulation) " _
             & "SELECT DISTINCT IDDecisionValue, " & IDPeriodGenerate & ", VariableName, Value, IDPlayer, IDItem, IDAge, " & Boolean_To_Bit(Simulazione) & " " _
             & "FROM Decisions_Developer_Value_List WHERE IDPeriod = " & IDPeriodo
        g_DAL.ExecuteNonQuery(sSQL)

        SQLConnClose(g_DAL.GetConnObject)

    End Sub

    Private Sub btnErasePeriod_Click(sender As Object, e As EventArgs) Handles btnErasePeriod.Click
        Dim sSQL As String = "SELECT MAX(IDPeriodo) FROM BGOL_Games_Periods WHERE IDGame = " & Session("IDGame")
        Dim iIDPeriodoMAXGioco As Integer = HandleGetMaxPeriodGame(Session("IDGame"))

        MessageText.Text = "Cancel completed"
        If Nz(ConfirmAnswer.Value, "FALSE").ToUpper = "TRUE" Then
            g_DAL = New DBHelper
            m_IDPeriodoAttuale = g_DAL.ExecuteScalar(sSQL)

            ' DECISIONS PLAYERS
            sSQL = "DELETE FROM Decisions_Players_Value WHERE  IDPeriod = " & m_IDPeriodoAttuale
            g_DAL.ExecuteNonQuery(sSQL)

            sSQL = "DELETE FROM Decisions_Players_Value_List WHERE IDPeriod = " & m_IDPeriodoAttuale
            g_DAL.ExecuteNonQuery(sSQL)

            ' DECISIONS BOSS
            sSQL = "DELETE FROM Decisions_Boss_Value WHERE IDPeriod = " & m_IDPeriodoAttuale
            g_DAL.ExecuteNonQuery(sSQL)

            sSQL = "DELETE FROM Decisions_Boss_Value_List WHERE IDPeriod = " & m_IDPeriodoAttuale
            g_DAL.ExecuteNonQuery(sSQL)

            ' DECISIONS DEVELOPER
            sSQL = "DELETE FROM Decisions_Developer_Value WHERE IDPeriod = " & m_IDPeriodoAttuale
            g_DAL.ExecuteNonQuery(sSQL)

            sSQL = "DELETE FROM Decisions_Developer_Value_List WHERE IDPeriod = " & m_IDPeriodoAttuale
            g_DAL.ExecuteNonQuery(sSQL)

            ' DECISIONS STATE
            sSQL = "DELETE FROM Variables_State_Value WHERE IDPeriod = " & m_IDPeriodoAttuale
            g_DAL.ExecuteNonQuery(sSQL)

            sSQL = "DELETE FROM Variables_State_Value_List WHERE IDPeriod = " & m_IDPeriodoAttuale
            g_DAL.ExecuteNonQuery(sSQL)

            ' Cancello anche le variabili calcolate
            sSQL = "DELETE FROM Variables_Calculate_Value WHERE IDPeriod = " & m_IDPeriodoAttuale
            g_DAL.ExecuteNonQuery(sSQL)

            ' Cancello il periodo dai periodi del gioco
            sSQL = "DELETE FROM BGOL_Games_Periods WHERE IDGame = " & Session("IDGame") & " AND IDPeriodo = " & m_IDPeriodoAttuale
            g_DAL.ExecuteNonQuery(sSQL)

            ' Se sto cancellando l'ultimo periodo, allora devo procedere alla cancellazione anche dei risultati del penultimo periodo valido, ovvero
            ' periodo attuale -1

            ' Recupero l'ID del periodo subito prima di questo
            sSQL = "SELECT MAX(ID) FROM BGOL_Periods WHERE IDGame = " & Session("IDGame") & " AND ID < " & m_IDPeriodoAttuale
            Dim iIDperiodDelete As Integer = Nni(g_DAL.ExecuteScalar(sSQL))
            sSQL = "DELETE FROM Variables_Calculate_Value WHERE IDPeriod = " & iIDperiodDelete
            g_DAL.ExecuteNonQuery(sSQL)

            Message.Visible = True

            Dim oSiteMaster As SiteMaster = Me.Master
            Dim oPNLMaster As UpdatePanel = oSiteMaster.FindControl("pnlMainUpdate")
            Dim oCBOPeriod As RadComboBox = oSiteMaster.FindControl("cboPeriod")
            oPNLMaster.Update()

            oSiteMaster.HandleReloadPeriods(m_IDPeriodoAttuale)

            ' Posiziono il periodo sull'ultimo
            oCBOPeriod.SelectedValue = Nni(HandleGetMaxPeriodValid(m_SessionData.Utente.Games.IDGame))

            Session("IDPeriod") = oCBOPeriod.SelectedValue

        End If

    End Sub

    Private Sub btnScenario_Click(sender As Object, e As EventArgs) Handles btnScenario.Click
        PageRedirect("Tutor_ScenarioSettings.aspx", "", "")
    End Sub

    Private Sub Tutor_Tutor_GameMachine_Init(sender As Object, e As EventArgs) Handles Me.Init
        m_SessionData = Session("SessionData")

        m_SiteMaster = Me.Master

        ' Controllo il cambiamento della selezione delle combo relative al periodo e alla squadra
        Dim oCBOPeriod As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboPeriod"), RadComboBox)
        Dim oCBOTeam As RadComboBox = DirectCast(m_SiteMaster.FindControl("cboTeam"), RadComboBox)

        Session("IDPeriodValid") = HandleGetMaxPeriodValid(m_SessionData.Utente.Games.IDGame) ' Ultimo periodo
        oCBOPeriod.SelectedValue = Session("IDPeriodValid")
        m_SiteMaster.PeriodChange = Session("IDPeriodValid")

        Session("NameTeam") = GetTeamName(Session("IDTeam"))
        oCBOTeam.SelectedValue = Session("IDTeam")
        m_SiteMaster.TeamChange = Session("IDTeam")

        chkSimulate.Visible = Session("IDRole").Contains("D") Or Session("IDRole").Contains("B")
    End Sub

    Private Sub Tutor_Tutor_GameMachine_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsNothing(m_SessionData) Then
            PageRedirect("~/Account/Login.aspx", "", "")
        End If

        m_SessionData = Session("SessionData")

        If Not Page.IsPostBack Then
            btnNewPeriod.Attributes.Add("onclick", "openConfirmBox();")
            btnErasePeriod.Attributes.Add("onclick", "openConfirmBoxDelete();")
        End If
    End Sub

End Class
