﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Tutor_Manage_Items.aspx.vb" Inherits="Tutor_Manage_Items" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="cntMain" ContentPlaceHolderID="MainContent" runat="Server">

    <script type="text/javascript">
        function confirmDelete(arg) {
            if (arg) {
                __doPostBack("", "");
            }
        }
    </script>

    <asp:UpdatePanel runat="server" ID="UpdatePanelMain" UpdateMode="Conditional">
        <ContentTemplate>
            <h2>
                <asp:Label ID="lblWelcome" runat="server" Text="Manage items"></asp:Label>
            </h2>

            <div class="clr"></div>

            <div id="subTitle">
                <div id="subLeft" style="float: left; width: 20%;">
                    <h3>
                        <asp:Label ID="lblTitle" runat="server" Text="Manage items"></asp:Label>
                    </h3>
                </div>

                <div id="subRight" style="float: right; padding-right: 5px;">
                    <div id="divNewItem" style="margin-top: 10px; margin-bottom: 5px; text-align: right;">
                        <telerik:RadButton ID="btnNewItem" runat="server" Text="New item"
                            RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                        </telerik:RadButton>
                    </div>
                </div>
            </div>

            <div class="clr"></div>

            <%--------------------------------------------------------------------------------------------------------------------------------------------------------------------%>
            <%-- GRIGLIA ITEM --%>
            <%--------------------------------------------------------------------------------------------------------------------------------------------------------------------%>
            <div class="row" style="padding-bottom: 5px;">
                <asp:Panel ID="pnlTableItem" runat="server">
                    <telerik:RadGrid ID="grdItems" runat="server" RenderMode="Lightweight" AllowPaging="True" AllowSorting="True" EnableLinqExpressions="false"
                        OnNeedDataSource="grdItems_NeedDataSource" Width="670px" AllowFilteringByColumn="True" Culture="it-IT" GroupPanelPosition="Top" PageSize="150">
                        <MasterTableView AutoGenerateColumns="false" TableLayout="Fixed" CssClass="RadGrid_WebBlue"
                            DataKeyNames="IdVariable, VariableName">

                            <Columns>
                                <telerik:GridButtonColumn CommandName="Modify" Text="Edit" UniqueName="EditColumn"
                                    ButtonType="ImageButton" ImageUrl="~/Content/GridTelerik/Edit.gif">
                                    <HeaderStyle Width="20px" />
                                </telerik:GridButtonColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="VariableName" HeaderText="Item" UniqueName="VariableName"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                    <HeaderStyle Width="75px" />
                                </telerik:GridBoundColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridButtonColumn CommandName="Translate" Text="Translate" UniqueName="Translate"
                                    ButtonType="ImageButton" ImageUrl="Images/Translate.ico" Resizable="false">
                                    <HeaderStyle Width="20px" />
                                </telerik:GridButtonColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridButtonColumn CommandName="Delete" Text="Delete" UniqueName="Delete" ConfirmText="Are you sure you want to delete de selected item?"
                                    ButtonType="ImageButton" ImageUrl="Images/Delete16.png" Resizable="false">
                                    <HeaderStyle Width="20px" />
                                </telerik:GridButtonColumn>
                            </Columns>

                        </MasterTableView>
                    </telerik:RadGrid>

                </asp:Panel>
            </div>

            <div class="clr"></div>

            <%--------------------------------------------------------------------------------------------------------------------------------------------------------------------%>
            <%-- PANNELLO DI MODIFICA ITEM --%>
            <%--------------------------------------------------------------------------------------------------------------------------------------------------------------------%>
            <div class="row">
                <input id="hdnItemModify" type="hidden" name="hdnItemModifyName" runat="server" />
                <ajaxToolkit:ModalPopupExtender ID="mpeItem" runat="server" PopupControlID="pnlItemModify" TargetControlID="hdnItemModify"
                    BackgroundCssClass="modalBackground" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>

                <asp:UpdatePanel ID="pnlItemUpdate" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel runat="server" ID="pnlItemModify" CssClass="modalPopup" Style="display: none;">

                            <div class="pnlheader">
                                <asp:Label ID="lblPanelItemModifyTitle" runat="server" Text="Manage item"></asp:Label>
                            </div>

                            <div class="pnlbody">
                                <div class="divTable">
                                    <div class="divTableBody">
                                        <div class="divTableRow">
                                            <div class="divTableCell" style="width: 200px;">
                                                <asp:Label ID="lblItem" runat="server" Text="Item description"></asp:Label>
                                            </div>

                                            <div class="divTableCell" style="width: 580px;">
                                                <telerik:RadTextBox ID="txtItemDescription" runat="server" RenderMode="Lightweight" ClientIDMode="Static" Resize="None">
                                                </telerik:RadTextBox>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="pnlfooter">
                                <telerik:RadButton ID="btnHideItemModify" runat="server" Text="Close" Width="80px"
                                    RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                                </telerik:RadButton>

                                <telerik:RadButton ID="btnSaveItemModify" runat="server" Text="Save" Width="80px"
                                    RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                                </telerik:RadButton>
                            </div>

                            <div style="text-align: center;">
                                <asp:Label runat="server" ID="txtMessageItemMOdify" Visible="false"></asp:Label>
                            </div>

                        </asp:Panel>

                    </ContentTemplate>

                </asp:UpdatePanel>
            </div>

            <div class="clr"></div>

            <%--------------------------------------------------------------------------------------------------------------------------------------------------------------------%>
            <%-- PANNELLO DI TRADUZIONE ITEM --%>
            <%--------------------------------------------------------------------------------------------------------------------------------------------------------------------%>
            <div class="row">
                <input id="hdnItemTranslations" type="hidden" name="hdnItemTranslationsName" runat="server" />
                <ajaxToolkit:ModalPopupExtender ID="mpeItemTranslate" runat="server" PopupControlID="pnlItemTranslate" TargetControlID="hdnItemTranslations"
                    BackgroundCssClass="modalBackground" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>

                <asp:Panel runat="server" ID="pnlItemTranslate" CssClass="modalPopup" Style="display: none;">
                    <asp:UpdatePanel ID="pnlUpdateItemTranslate" runat="server" UpdateMode="Conditional">

                        <ContentTemplate>

                            <div class="pnlheader">
                                <asp:Label ID="lblTitleTranslation" runat="server" Text="Manage translation item"></asp:Label>
                            </div>

                            <div class="pnlbody">
                                <div class="divTable">
                                    <div class="divTableBody">
                                        <div class="divTableRow">
                                            <div class="divTableCell" style="width: 90%">
                                                <asp:Label ID="lblItemName" runat="server" Text="Item name" Width="100px"></asp:Label>
                                                <telerik:RadTextBox ID="txtItemTranslate" runat="server" RenderMode="Lightweight" Width="370px" ClientIDMode="Static" ReadOnly="true"></telerik:RadTextBox>
                                            </div>
                                        </div>

                                        <div class="divTableRow">
                                            <div class="divTableCell" style="width: 90%">
                                                <asp:Label ID="lblSelectLanguage" runat="server" Text="Select language" Width="100px"></asp:Label>
                                                <telerik:RadComboBox ID="cboLanguage" runat="server" Width="370px" EmptyMessage="< Select language...>"
                                                    RenderMode="Lightweight" RenderingMode="Full" ZIndex="1000000" ClientIDMode="Static">
                                                </telerik:RadComboBox>
                                            </div>
                                        </div>

                                        <div class="divTableRow">
                                            <div class="divTableCell" style="width: 90%">
                                                <asp:Label ID="lblItemTranslation" runat="server" Text="Item translation" Width="100px"></asp:Label>
                                                <telerik:RadTextBox ID="txtItemTranslation" runat="server" RenderMode="Lightweight" Width="370px" ClientIDMode="Static" Resize="None">
                                                </telerik:RadTextBox>
                                            </div>
                                        </div>

                                        <div class="divTableRow">
                                            <div class="divTableCell" style="width: 90%">
                                                <asp:Label ID="lblTranslationAvailable" runat="server" Text="Translations available" Width="250px"></asp:Label>
                                                <asp:Panel ID="pnlGridItemTranslation" runat="server">
                                                    <telerik:RadGrid ID="grdItemTranslation" runat="server" RenderMode="Lightweight" AllowPaging="True" AllowSorting="True"
                                                        CellSpacing="0" GridLines="None" OnNeedDataSource="grdItemsTranslations_NeedDataSource" Width="480px" PageSize="30">
                                                        <MasterTableView AutoGenerateColumns="false" TableLayout="Fixed" CssClass="RadGrid_WebBlue"
                                                            DataKeyNames="ID, IDItem, IDLanguage, Translation, ItemName"
                                                            NoMasterRecordsText="No translations available">

                                                            <Columns>
                                                                <telerik:GridButtonColumn CommandName="Modify" Text="Edit" UniqueName="EditColumnTranslation"
                                                                    ButtonType="ImageButton" ImageUrl="~/Content/GridTelerik/Edit.gif">
                                                                    <HeaderStyle Width="20px" />
                                                                </telerik:GridButtonColumn>
                                                            </Columns>

                                                            <Columns>
                                                                <telerik:GridBoundColumn DataField="Translation" HeaderText="Menu" UniqueName="Translation">
                                                                    <HeaderStyle Width="230px" />
                                                                </telerik:GridBoundColumn>
                                                            </Columns>

                                                            <Columns>
                                                                <telerik:GridBoundColumn DataField="Code" HeaderText="Lang." UniqueName="Code">
                                                                    <HeaderStyle Width="80px" />
                                                                </telerik:GridBoundColumn>
                                                            </Columns>

                                                        </MasterTableView>
                                                    </telerik:RadGrid>

                                                    <asp:PlaceHolder runat="server" ID="MessageTranslation" Visible="false">
                                                        <p class="text-danger">
                                                            <asp:Literal runat="server" ID="txtMessageTranslation" />
                                                        </p>
                                                    </asp:PlaceHolder>

                                                </asp:Panel>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="pnlfooter">
                                <telerik:RadButton ID="btnHideTranslation" runat="server" Text="Close" Width="80px"
                                    RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                                </telerik:RadButton>

                                <telerik:RadButton ID="btnSaveTranslation" runat="server" Text="Save" Width="80px"
                                    RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                                </telerik:RadButton>
                            </div>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>

            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="row">
        <div class="divTableCell" style="text-align: right;">
            <telerik:RadButton ID="btnTranslate" runat="server" Text="Translate" EnableAjaxSkinRendering="true" ToolTip=""></telerik:RadButton>
        </div>
    </div>

    <asp:UpdatePanel ID="pnlMessage" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row">
                <telerik:RadLabel runat="server" ID="lblMessage" Visible="false"></telerik:RadLabel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

