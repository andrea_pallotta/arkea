﻿Imports Telerik.Web.UI
Imports System.Data
Imports APS
Imports APPCore.Utility
Imports DALC4NET

Partial Class Tutor_Manage_Items
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster

    Private Sub Tutor_Manage_Items_Init(sender As Object, e As EventArgs) Handles Me.Init
        m_SessionData = Session("SessionData")
    End Sub

    Protected Sub grdItems_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs)
        grdItems.DataSource = LoadItemsGame(Session("IDGame"), "")
    End Sub

    Protected Sub grdItemsTranslations_NeedDataSource(sendere As Object, e As GridNeedDataSourceEventArgs)
        grdItemTranslation.DataSource = LoadItemsTranslation()
    End Sub

    Private Sub deleteItem()
        Dim oDAL As New DBHelper
        Dim oItem As GridDataItem = DirectCast(Session("ItemSelected"), GridDataItem)

        If Not oItem Is Nothing Then
            Dim iIDItem As Integer = oItem.GetDataKeyValue("IdVariable")
            Dim sNomeItem As String = oItem.GetDataKeyValue("VariableName")

            ' Cancellazione delle variabili BOSS
            Dim sSQL = "DELETE FROM Decisions_Boss_Value_List WHERE ID IN (" _
                     & "SELECT DBVL.ID " _
                     & "FROM Decisions_Boss D " _
                     & "INNER JOIN Decisions_Boss_Value DBV ON D.Id = DBV.IDDecisionBoss " _
                     & "LEFT JOIN Decisions_Boss_Value_List DBVL ON DBV.id = DBVL.IdDecisionBossValue " _
                     & "WHERE IDGame = " & Nni(Session("IDGame")) & " AND DBVL.VariableName LIKE '%" & sNomeItem & "%') "
            oDAL.ExecuteNonQuery(sSQL)

            ' Cancellazione delle variabili Decisions developer
            sSQL = "DELETE FROM Decisions_Developer_Value_List WHERE ID IN (" _
                 & "SELECT DBVL.ID " _
                 & "FROM Decisions_Developer D " _
                 & "INNER JOIN Decisions_Developer_Value DBV ON D.Id = DBV.IDDecisionDeveloper " _
                 & "LEFT JOIN Decisions_Developer_Value_List DBVL ON DBV.id = DBVL.IDDecisionValue " _
                 & "WHERE IDGame = " & Nni(Session("IDGame")) & " AND DBVL.VariableName LIKE '%" & sNomeItem & "%') "
            oDAL.ExecuteNonQuery(sSQL)

            sSQL = "DELETE FROM Decisions_Developer_Value WHERE ID IN (" _
                 & "SELECT DBV.ID " _
                 & "FROM Decisions_Developer D " _
                 & "INNER JOIN Decisions_Developer_Value DBV ON D.Id = DBV.IDDecisionDeveloper " _
                 & "WHERE IDGame = " & Nni(Session("IDGame")) & " AND DBV.IDItem = " & iIDItem & ") "
            oDAL.ExecuteNonQuery(sSQL)

            ' Cancellazione delle variabili di stato
            sSQL = "DELETE FROM Variables_State_Value_List WHERE IDItem = " & iIDItem
            oDAL.ExecuteNonQuery(sSQL)

            sSQL = "DELETE FROM Variables_State_Value WHERE IDItem = " & iIDItem
            oDAL.ExecuteNonQuery(sSQL)

            ' Cancellazione delle decisioni del player
            sSQL = "DELETE FROM Decisions_Players_Value_List WHERE IDItem = " & iIDItem
            oDAL.ExecuteNonQuery(sSQL)

            ' Cancellazione delle eventuali variabili calcolate 
            sSQL = "DELETE FROM Variables_Calculate_Value WHERE IDItem = " & iIDItem
            oDAL.ExecuteNonQuery(sSQL)

            ' Cancello le traduzioni
            sSQL = "DELETE FROM Items_Translations WHERE IDItem = " & iIDItem
            oDAL.ExecuteNonQuery(sSQL)

            ' Cancellazione dell'item
            sSQL = "DELETE FROM Items_Period_Variables WHERE ID = " & iIDItem
            oDAL.ExecuteNonQuery(sSQL)

            SQLConnClose(oDAL.GetConnObject)
            oDAL = Nothing

            Session("ItemSelected") = Nothing
        End If

    End Sub

    Private Function LoadItemsTranslation() As DataTable
        Dim oItem As GridDataItem = DirectCast(Session("ItemSelected"), GridDataItem)

        If Not IsNothing(oItem) Then
            Dim sSQL As String

            sSQL = "SELECT IT.ID, IT.IDItem, IT.Translation, IT.IDLanguage, IPV.VariableName AS ItemName  " _
                 & "FROM Items_Translations IT " _
                 & "INNER JOIN Items_Period_Variables IPV ON IT.IDItem = IPV.Id " _
                 & "WHERE IT.IDItem = " & Nni(oItem.GetDataKeyValue("IdVariable"))
            Return g_DAL.ExecuteDataTable(sSQL)

        Else
            Return Nothing
        End If

    End Function

    Private Sub ClearPanelItem()
        txtItemDescription.Text = ""
    End Sub

    Private Sub ClearPanelItemTranslate()
        txtItemTranslate.Text = ""
        txtItemTranslation.Text = ""
        cboLanguage.SelectedValue = -1
        grdItemTranslation.Rebind()
    End Sub

    Private Sub LoadLanguages()
        Dim sSQL As String
        Dim oDTLanguages As DataTable

        sSQL = "SELECT -1 AS ID, '' AS Language UNION ALL SELECT ID, Language FROM Languages ORDER BY ID "
        oDTLanguages = g_DAL.ExecuteDataTable(sSQL)

        ' Caricamento della combo per la traduzione dei menu principali
        cboLanguage.DataSource = oDTLanguages
        cboLanguage.DataValueField = "ID"
        cboLanguage.DataTextField = "Language"
        cboLanguage.DataBind()

        cboLanguage.SelectedValue = -1

    End Sub

    Private Sub Tutor_Manage_Items_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        If IsNothing(m_SessionData) Then
            PageRedirect("ErrorTimeout.aspx", "", "")
        End If

        If Not Page.IsPostBack Then
            LoadLanguages()
            m_SiteMaster = Me.Master
        End If
    End Sub

    Private Sub grdItems_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles grdItems.ItemCommand
        If e.CommandName.ToUpper <> "PAGE" AndAlso e.CommandName.ToUpper <> "SORT" AndAlso e.CommandName.ToUpper <> "FILTER" Then
            Session("ItemSelected") = DirectCast(e.Item, GridDataItem)

            If e.CommandName.ToUpper = "MODIFY" Then
                ClearPanelItem()
                mpeItem.Show()
                UpdatePanelMain.Update()

            ElseIf e.CommandName.ToUpper = "TRANSLATE" Then
                ClearPanelItemTranslate()
                mpeItemTranslate.Show()
                UpdatePanelMain.Update()

            ElseIf e.CommandName.ToUpper = "DELETE" Then
                deleteItem()

            End If
        End If

    End Sub

    Private Sub pnlItemUpdate_PreRender(sender As Object, e As EventArgs) Handles pnlItemUpdate.PreRender
        Dim oItem As GridDataItem = DirectCast(Session("ItemSelected"), GridDataItem)

        ' Procedo con il caricamento dei dati da inserire nel panello
        ' Recupero l'id della variabile selezionata
        If Not oItem Is Nothing Then
            lblPanelItemModifyTitle.Text = "Manage - " & oItem.GetDataKeyValue("VariableName").ToString
            txtItemDescription.Text = Nz(oItem.GetDataKeyValue("VariableName").ToString)

            mpeItem.Show()
            UpdatePanelMain.Update()
        End If

    End Sub

    Private Sub btnSaveItemModify_Click(sender As Object, e As EventArgs) Handles btnSaveItemModify.Click
        Dim sSql As String
        Dim dtItemsGame As DataTable = LoadItemsGame(Session("IDGame"), "")
        Dim sNomeItemOriginale As String
        Dim iIDItem As Integer = 0
        Dim iIDTeamMeU As Integer
        Dim iIDTeamMaster As Integer
        Dim iIDPlayerMaster As Integer
        Dim sTeamNameMaster As String

        'Procedo con il salvataggio delle informazioni legate all'item selezionato
        Dim oItem As GridDataItem
        oItem = DirectCast(Session("ItemSelected"), GridDataItem)

        ' Procedo con l'aggiornamento dei valori nel database
        If Not oItem Is Nothing AndAlso Nni(oItem.GetDataKeyValue("IdVariable")) <> 0 Then
            iIDItem = Nni(oItem.GetDataKeyValue("IdVariable"))
            sSql = "UPDATE Items_Period_Variables SET VariableName = '" & CStrSql(txtItemDescription.Text) & "', VariableLabel = '" & CStrSql(txtItemDescription.Text) & "' " _
                 & "WHERE ID = " & iIDItem

            sNomeItemOriginale = oItem.GetDataKeyValue("VariableName")
        Else
            ' Recupero i valori da assegnare all'ID SubItem e all'IDDatatype
            Dim iIDSubItem As Integer = Nni(dtItemsGame.Rows(0)("IDSubItem"))
            Dim iIDDataType As Integer = Nni(dtItemsGame.Rows(0)("IDDataType"))
            Dim iOrderNum As Integer = Nni(dtItemsGame.Rows(0)("VariablesOrder"), 0) + 1

            sSql = "INSERT INTO Items_Period_Variables (VariableName, VariableLabel, IDSubItem, IDDataType, OrderNum, VariableValue) VALUES (" _
                 & "'" & CStrSql(txtItemDescription.Text) & "', '" & CStrSql(txtItemDescription.Text) & "', " & iIDSubItem & ", " & iIDDataType & ", " & iOrderNum & ", 0) "
            sNomeItemOriginale = ""
        End If
        g_DAL.ExecuteNonQuery(sSql)

        ' Se ho effettuato l'inserimento di un nuovo item procedo con il recupero del suo ID
        If iIDItem = 0 Then
            sSql = "SELECT MAX(ID) FROM Items_Period_Variables "
            iIDItem = g_DAL.ExecuteScalar(sSql)
        End If

        ' CONTROLLO SE HO MODIFICATO IL NOME DI UN ITEM ESISTENTE, nel caso procedo alla modifica di tutte le descrizioni presenti nel database per il gioco selezionato
        If Not oItem Is Nothing AndAlso Nni(oItem.GetDataKeyValue("IdVariable")) <> 0 Then ' Sono in modifica
            ' Decisioni del boss
            sSql = "UPDATE Decisions_Boss_Value_List SET VariableName = REPLACE(VariableName, '" & sNomeItemOriginale & "', '" & txtItemDescription.Text & "') WHERE ID IN (" _
                 & "SELECT DBVL.Id " _
                 & "FROM Decisions_Boss D " _
                 & "LEFT JOIN Decisions_Boss_Value DBV ON D.Id = DBV.IDDecisionBoss " _
                 & "LEFT JOIN Decisions_Boss_Value_List DBVL ON DBV.id = DBVL.IdDecisionBossValue " _
                 & "WHERE IDGame = " & Nni(Session("IDGame")) & " AND VariableName LIKE '%" & sNomeItemOriginale & "%') "
            g_DAL.ExecuteNonQuery(sSql)

            ' Decisioni del developer
            sSql = "UPDATE Decisions_Developer_Value_List SET VariableName = REPLACE(VariableName, '" & sNomeItemOriginale & "', '" & txtItemDescription.Text & "') WHERE ID IN (" _
                 & "SELECT DBVL.Id " _
                 & "FROM Decisions_Developer D " _
                 & "LEFT JOIN Decisions_Developer_Value DBV ON D.Id = DBV.IDDecisionDeveloper " _
                 & "LEFT JOIN Decisions_Developer_Value_List DBVL ON DBV.id = DBVL.IDDecisionValue " _
                 & "WHERE IDGame = " & Nni(Session("IDGame")) & " AND DBVL.VariableName LIKE '%" & sNomeItemOriginale & "%') "
            g_DAL.ExecuteNonQuery(sSql)

            ' Varibili di stato
            sSql = "UPDATE Variables_State_Value_List SET VariableName = REPLACE(VariableName, '" & sNomeItemOriginale & "', '" & txtItemDescription.Text & "') WHERE ID IN (" _
                 & "SELECT VSVL.Id " _
                 & "FROM Variables_State VS " _
                 & "INNER JOIN Variables_State_Value VSV ON VS.ID = VSV.IDVariable " _
                 & "LEFT JOIN Variables_State_Value_List VSVL ON VSV.Id = VSVL.IDVariableState " _
                 & "WHERE IDGame = " & Nni(Session("IDGame")) & " AND VSVL.VariableName LIKE '%" & sNomeItemOriginale & "%') "
            g_DAL.ExecuteNonQuery(sSql)

            ' Decisioni del player
            sSql = "UPDATE Decisions_Players_Value_List SET VariableName = REPLACE(VariableName, '" & sNomeItemOriginale & "', '" & txtItemDescription.Text & "') " _
                 & "WHERE ID IN (" _
                 & "SELECT DPVL.Id " _
                 & "FROM Decisions_Players D " _
                 & "LEFT JOIN Decisions_Players_Value DPV ON D.Id = DPV.IDDecisionPlayer " _
                 & "LEFT JOIN Decisions_Players_Value_List DPVL ON DPV.ID = DPVL.IDDecisionValue " _
                 & "WHERE IDGame = " & Nni(Session("IDGame")) & " AND DPVL.VariableName LIKE '%" & sNomeItemOriginale & "%') "
            g_DAL.ExecuteNonQuery(sSql)

        Else
            ' Carico la datatable di tutti i player del gioco
            Dim dtPlayers As DataTable = LoadTeamsGame(Session("IDGame"))

            ' Recupero i dati di gioco di M&U
            sSql = "SELECT ID FROM BGOL_Teams WHERE TeamName LIKE '%M&U%' "
            iIDTeamMeU = Nni(g_DAL.ExecuteScalar(sSql))

            ' Recupero i dati di del primo player, generalmente è quello usato come master
            sSql = "SELECT MIN(BPTG.IDPLayer) AS IDPlayer , MIN(BPTG.IdTeam) AS IDTeam " _
                 & "FROM BGOL_Players_Teams_Games BPTG " _
                 & "INNER JOIN BGOL_Players_Games BTG ON BPTG.IdGame = BTG.IDGame " _
                 & "AND BPTG.IdPlayer = BTG.IDPlayer " _
                 & "AND BPTG.IdTeam = BTG.IDTeam " _
                 & "WHERE BPTG.IDGame = " & Nni(Session("IDGame")) & " AND BPTG.IDTeam <> " & iIDTeamMeU & " AND BTG.IDRole = 'P' "
            Dim dtData As DataTable = g_DAL.ExecuteDataTable(sSql)
            For Each drData As DataRow In dtData.Rows
                iIDTeamMaster = Nni(drData("IDTeam"))
                iIDPlayerMaster = Nni(drData("IDPlayer"))
            Next
            sSql = "SELECT TeamName FROM BGOL_Teams WHERE ID = " & iIDTeamMaster
            sTeamNameMaster = Nz(g_DAL.ExecuteScalar(sSql))

            ' Sono in inserimento
            ' Recupero il primo Item del gioco e inserisco i valori per quello nuovo, mettendo le inizializzazioni a 0
            Dim drItem As DataRow = dtItemsGame.Select().FirstOrDefault
            Dim minIDItem As Integer = Nni((drItem("IDVariable")))
            Dim sNomeItem As String = drItem("VariableName")

            ' Procedo con l'inserimento del nuovo item 
            ' Decisioni BOSS
            sSql = "INSERT INTO Decisions_Boss_Value_List (IDDecisionBossValue, IDPeriod, VariableName, Value, Simulation) " _
                 & "SELECT IDDecisionBossValue, DBVL.IDPeriod, REPLACE(DBVL.VariableName, '" & sNomeItem & "', '" & txtItemDescription.Text & "'), DBVL.Value, 0 " _
                 & "FROM Decisions_Boss D " _
                 & "LEFT JOIN Decisions_Boss_Value DBV ON D.Id = DBV.IDDecisionBoss " _
                 & "LEFT JOIN Decisions_Boss_Value_List DBVL ON DBV.id = DBVL.IdDecisionBossValue " _
                 & "WHERE IDGame = " & Nni(Session("IDGame")) & " AND VariableName LIKE '%" & sNomeItem & "%' "
            g_DAL.ExecuteNonQuery(sSql)

            ' Decisions developer
            sSql = "INSERT INTO Decisions_Developer_Value (IDDecisionDeveloper, IDPeriod, Value, IDItem, Simulation) " _
                 & "SELECT DBV.IDDecisionDeveloper, DBV.IDPeriod, DBV.Value, " & iIDItem & ", 0 " _
                 & "FROM Decisions_Developer D " _
                 & "LEFT JOIN Decisions_Developer_Value DBV ON D.Id = DBV.IDDecisionDeveloper " _
                 & "WHERE IDGame = " & Nni(Session("IDGame")) & " AND DBV.IDItem = " & minIDItem
            g_DAL.ExecuteNonQuery(sSql)

            ' Variabili di stato
            ' Prima recupero le liste con gli items, nel caso di items/players recupero tutti i players tranne il 2 (M&U)
            sSql = "SELECT VSVL.IDVariableState, VSVL.ID, VSVL.VariableName, VSVL.Value, VSVL.IDItem, VSVL.IDAge, VSVL.IDPeriod, VSVL.IDPlayer, VSVL.Simulation " _
                 & "FROM Variables_State VS " _
                 & "INNER JOIN Variables_State_Value VSV ON VS.ID = VSV.IDVariable " _
                 & "LEFT JOIN Variables_State_Value_List VSVL ON VSV.Id = VSVL.IDVariableState " _
                 & "WHERE IDGame = " & Nni(Session("IDGame")) & " AND VSV.Value LIKE '%ITEMS%' AND VSVL.VariableName LIKE '%" & sNomeItem & "%' " _
                 & "AND VariableName LIKE '%" & sTeamNameMaster & "%' "
            Dim dtVariablesStateValueList As DataTable = g_DAL.ExecuteDataTable(sSql)

            ' Ciclo sui risultati e sui player presenti nel game per la preparazione della variabile
            For Each drVariablesStateValueList As DataRow In dtVariablesStateValueList.Rows
                If Nni(drVariablesStateValueList("IDPlayer")) <> 0 Then
                    For Each drPlayer As DataRow In dtPlayers.Rows
                        Dim sNomeVariabile As String
                        sNomeVariabile = Nz(drVariablesStateValueList("VariableName")).Replace(sTeamNameMaster, Nz(drPlayer("TeamName")))
                        sNomeVariabile = Nz(drVariablesStateValueList("VariableName")).Replace(sNomeItem, txtItemDescription.Text)

                        sSql = "INSERT INTO Variables_State_Value_List (IDVariableState, VariableName, Value, IDItem, IDAge, IDPeriod, IDPlayer, Simulation) " _
                             & "VALUES(" & Nni(drVariablesStateValueList("IDVariableState")) & ", '" & sNomeVariabile & "', '" _
                             & Nz(drVariablesStateValueList("Value")) & "', " & iIDItem & ", " & Nni(drVariablesStateValueList("IDAge")) & ", " _
                             & Nni(drVariablesStateValueList("IDPeriod")) & ", " & Nni(drPlayer("ID")) & ", 0) "
                        g_DAL.ExecuteNonQuery(sSql)
                    Next
                End If
            Next

            ' Ciclo sulle variabili di stato, non lista, presenti in archivio 
            sSql = "SELECT VS.*, VSV.* " _
                 & "FROM Variables_State VS " _
                 & "INNER JOIN Variables_State_Value VSV ON VS.ID = VSV.IDVariable " _
                 & "WHERE IDGame = " & Nni(Session("IDGame")) & " AND VSV.IDItem = " & minIDItem
            Dim dtVariablesStateValue As DataTable = g_DAL.ExecuteDataTable(sSql)
            For Each drVariablesStateValue As DataRow In dtVariablesStateValue.Rows
                If Nni(drVariablesStateValue("IDPlayer")) <> 0 Then
                    For Each drPlayer As DataRow In dtPlayers.Rows
                        sSql = "INSERT INTO Variables_State_Value (IDVariable, Value, IDPeriod, IDPlayer, IDItem, IDAge, Simulation) " _
                             & "VALUES(" & Nni(drVariablesStateValue("IDVariable")) & ", '" & Nz(drVariablesStateValue("Value")) & "', " _
                             & Nni(drVariablesStateValue("IDPeriod")) & ", " & Nni(drPlayer("ID")) & ", " & iIDItem & ", " & Nni(drVariablesStateValue("IDAge")) & ", 0) "

                        g_DAL.ExecuteNonQuery(sSql)
                    Next
                End If
            Next

            ' Variabili decisionali del player
            ' Recupero quelle di tipo lista
            sSql = "SELECT DPVL.* " _
                 & "FROM Decisions_Players D " _
                 & "LEFT JOIN Decisions_Players_Value DPV ON D.Id = DPV.IDDecisionPlayer " _
                 & "LEFT JOIN Decisions_Players_Value_List DPVL ON DPV.ID = DPVL.IDDecisionValue " _
                 & "WHERE IDGame = " & Nni(Session("IDGame")) & " AND DPVL.IDItem = " & minIDItem & " AND DPVL.VariableName LIKE '%" & sTeamNameMaster & "%' "
            Dim dtDecisionsPlayerValueList As DataTable = g_DAL.ExecuteDataTable(sSql)
            For Each drDecisionsPlayerValueList As DataRow In dtDecisionsPlayerValueList.Rows
                If Nni(drDecisionsPlayerValueList("IDPlayer")) <> 0 Then
                    For Each drPlayer As DataRow In dtPlayers.Rows
                        Dim sNomeVariabile As String
                        sNomeVariabile = Nz(drDecisionsPlayerValueList("VariableName")).Replace(sTeamNameMaster, Nz(drPlayer("TeamName")))
                        sNomeVariabile = Nz(drDecisionsPlayerValueList("VariableName")).Replace(sNomeItem, txtItemDescription.Text)
                        sSql = "INSERT INTO Decisions_Players_Value_List (IDDecisionValue, IDPlayer, IDPeriod, VariableName, Value, IDItem, IDAge, Simulation) " _
                             & "VALUES (" & Nni(drDecisionsPlayerValueList("IDDecisionValue")) & ", " & Nni(drPlayer("ID")) & ", " & Nni(drDecisionsPlayerValueList("IDPeriod")) & ", " _
                             & "'" & sNomeVariabile & "', '" & Nz(drDecisionsPlayerValueList("Value")) & "', " & iIDItem & ", " & Nni(drDecisionsPlayerValueList("IDAge")) & ", 0) "
                        g_DAL.ExecuteNonQuery(sSql)
                    Next
                End If
            Next

        End If

        grdItems.Rebind()
        UpdatePanelMain.Update()
    End Sub

    Private Sub btnNewItem_Click(sender As Object, e As EventArgs) Handles btnNewItem.Click
        Session.Remove("ItemSelected")
        ClearPanelItem()
        mpeItem.Show()
    End Sub

    Private Sub mpeItemTranslate_PreRender(sender As Object, e As EventArgs) Handles mpeItemTranslate.PreRender
        Dim oItem As GridDataItem = DirectCast(Session("ItemSelected"), GridDataItem)
        Dim oItemTranslation As GridDataItem = DirectCast(Session("ItemTranslationSelected"), GridDataItem)
        If Not oItem Is Nothing Then
            txtItemTranslate.Text = oItem.GetDataKeyValue("VariableName").ToString
            txtItemTranslate.ReadOnly = True

            If Not oItemTranslation Is Nothing Then
                txtItemTranslation.Text = Nz(oItemTranslation.GetDataKeyValue("Translation"))
                cboLanguage.SelectedValue = Nni(oItemTranslation.GetDataKeyValue("IDLanguage"))
            End If
        End If
    End Sub

    Private Sub btnSaveTranslation_Click(sender As Object, e As EventArgs) Handles btnSaveTranslation.Click
        Dim oItem As GridDataItem = DirectCast(Session("ItemSelected"), GridDataItem)

        ' Inserisco la traduzione nella tabella e ricarico la griglia
        Dim sSql As String

        ' Controllo che non sia già presente una traduzione per la lingua selezionata, altrimenti vado in update
        sSql = "SELECT ID FROM Items_Translations WHERE IDItem = " & Nni(oItem.GetDataKeyValue("IdVariable")) & " AND IDLanguage = " & Nni(cboLanguage.SelectedValue)
        Dim iIDFind As Integer = Nni(g_DAL.ExecuteScalar(sSql))

        If iIDFind = 0 Then
            sSql = "INSERT INTO Items_Translations (IDItem, IDLanguage, Translation) VALUES (" _
                 & Nni(oItem.GetDataKeyValue("IdVariable")) & ", " & Nni(cboLanguage.SelectedValue) & ", '" & txtItemTranslation.Text & "') "
        Else
            sSql = "UPDATE Items_Translations SET Translation = '" & txtItemTranslation.Text & "' WHERE ID = " & iIDFind
        End If

        g_DAL.ExecuteNonQuery(sSql)
        grdItemTranslation.Rebind()

        Session("ItemTranslationSelected") = Nothing

        ClearPanelItemTranslate()

        pnlUpdateItemTranslate.Update()

        mpeItemTranslate.Show()
    End Sub

    Private Sub grdItemTranslation_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles grdItemTranslation.ItemCommand
        If e.CommandName.ToUpper <> "PAGE" AndAlso e.CommandName.ToUpper <> "SORT" AndAlso e.CommandName.ToUpper <> "FILTER" Then
            Session("ItemTranslationSelected") = DirectCast(e.Item, GridDataItem)

            If e.CommandName.ToUpper = "MODIFY" Then
                ClearPanelItemTranslate()
                mpeItemTranslate.Show()
                UpdatePanelMain.Update()

            End If
        End If
    End Sub

End Class
