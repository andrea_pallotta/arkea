﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Tutor_Manage_Periods.aspx.vb" Inherits="Tutor_Manage_Periods" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="cntMain" ContentPlaceHolderID="MainContent" runat="Server">

    <script type="text/javascript">
        function confirmDelete(arg) {
            if (arg) {
                __doPostBack("", "");
            }
        }
    </script>

    <asp:UpdatePanel runat="server" ID="UpdatePanelMain" UpdateMode="Conditional">
        <ContentTemplate>
            <h2>
                <asp:Label ID="lblWelcome" runat="server" Text="Manage periods"></asp:Label>
            </h2>

            <div class="clr"></div>

            <div id="subTitle">
                <div id="subLeft" style="float: left; width: 20%;">
                    <h3>
                        <asp:Label ID="lblTitle" runat="server" Text="Manage items"></asp:Label>
                    </h3>
                </div>

                <div id="subRight" style="float: right; padding-right: 5px;">
                    <div id="divNewPeriod" style="margin-top: 10px; margin-bottom: 5px; text-align: right;">
                        <telerik:RadButton ID="btnNewPeriod" runat="server" Text="New period"
                            RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                        </telerik:RadButton>
                    </div>
                </div>
            </div>

            <div class="clr"></div>

            <%--------------------------------------------------------------------------------------------------------------------------------------------------------------------%>
            <%-- GRIGLIA PERIODI --%>
            <%--------------------------------------------------------------------------------------------------------------------------------------------------------------------%>
            <div class="row" style="padding-bottom: 5px;">
                <asp:Panel ID="pnlTablePeriod" runat="server">
                    <telerik:RadGrid ID="grdPeriods" runat="server" RenderMode="Lightweight" AllowPaging="True" AllowSorting="True" EnableLinqExpressions="false"
                        OnNeedDataSource="grdPeriods_NeedDataSource" Width="670px" AllowFilteringByColumn="false" Culture="it-IT" GroupPanelPosition="Top" PageSize="150">
                        <MasterTableView AutoGenerateColumns="false" TableLayout="Fixed" CssClass="RadGrid_WebBlue"
                            DataKeyNames="ID, Descrizione, DateStart, DateEnd, DateHourEndEffective, DateStartFormatted, DateEndFormatted, DateHourEndEffectiveFormatted">

                            <Columns>
                                <telerik:GridButtonColumn CommandName="Modify" Text="Edit" UniqueName="EditColumn"
                                    ButtonType="ImageButton" ImageUrl="~/Content/GridTelerik/Edit.gif">
                                    <HeaderStyle Width="20px" />
                                </telerik:GridButtonColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="Descrizione" HeaderText="Period" UniqueName="Descrizione"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                    <HeaderStyle Width="80px" />
                                </telerik:GridBoundColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="DateStartFormatted" HeaderText="Date start" UniqueName="DateStartFormatted"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                    <HeaderStyle Width="80px" />
                                </telerik:GridBoundColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="DateEndFormatted" HeaderText="Date end" UniqueName="DateEndFormatted"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                    <HeaderStyle Width="80px" />
                                </telerik:GridBoundColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridBoundColumn DataField="DateHourEndEffectiveFormatted" HeaderText="End effective" UniqueName="DateHourEndEffectiveFormatted"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                    <HeaderStyle Width="80px" />
                                </telerik:GridBoundColumn>
                            </Columns>

                            <Columns>
                                <telerik:GridButtonColumn CommandName="Delete" Text="Delete" UniqueName="Delete" ConfirmText="Are you sure you want to delete de selected item?"
                                    ButtonType="ImageButton" ImageUrl="Images/Delete16.png" Resizable="false">
                                    <HeaderStyle Width="20px" />
                                </telerik:GridButtonColumn>
                            </Columns>

                        </MasterTableView>
                    </telerik:RadGrid>

                </asp:Panel>
            </div>

            <div class="clr"></div>

            <asp:PlaceHolder runat="server" ID="Message" Visible="false">
                <p class="text-danger">
                    <asp:Literal runat="server" ID="MessageText" />
                </p>
            </asp:PlaceHolder>

        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel runat="server" ID="pnlInsertUpdatePeriod">
        <ContentTemplate>
            <%--------------------------------------------------------------------------------------------------------------------------------------------------------------------%>
            <%-- PANNELLO DI MODIFICA PERIODI --%>
            <%--------------------------------------------------------------------------------------------------------------------------------------------------------------------%>
            <div class="row">
                <input id="hdnPeriodModify" type="hidden" name="hdnPeriodModifyName" runat="server" />
                <ajaxToolkit:ModalPopupExtender ID="mpePeriod" runat="server" PopupControlID="pnlPeriodModify" TargetControlID="hdnPeriodModify"
                    BackgroundCssClass="modalBackground" DropShadow="true">
                </ajaxToolkit:ModalPopupExtender>

                <asp:UpdatePanel ID="pnlPeriodUpdate" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel runat="server" ID="pnlPeriodModify" CssClass="modalPopup" Style="display: none;">

                            <div class="pnlheader">
                                <asp:Label ID="lblPanelPeriodModifyTitle" runat="server" Text="Manage period"></asp:Label>
                            </div>

                            <div class="pnlbody">
                                <div class="divTable">
                                    <div class="divTableBody">
                                        <div class="divTableRow">
                                            <div class="divTableCell" style="width: 200px;">
                                                <asp:Label ID="lblPeriod" runat="server" Text="Period description"></asp:Label>
                                            </div>

                                            <div class="divTableCell" style="width: 580px;">
                                                <telerik:RadTextBox ID="txtPeriodDescription" runat="server" RenderMode="Lightweight" ClientIDMode="Static" Resize="None">
                                                </telerik:RadTextBox>
                                            </div>

                                        </div>

                                        <div class="divTableRow">
                                            <div class="divTableCell" style="width: 200px;">
                                                <asp:Label ID="lblDateStart" runat="server" Text="Start period"></asp:Label>
                                            </div>

                                            <div class="divTableCell" style="width: 580px;">
                                                <telerik:RadTextBox ID="txtStartPeriod" runat="server" RenderMode="Lightweight" ClientIDMode="Static" Resize="None">
                                                </telerik:RadTextBox>
                                            </div>

                                        </div>

                                        <div class="divTableRow">
                                            <div class="divTableCell" style="width: 200px;">
                                                <asp:Label ID="lblDateEnd" runat="server" Text="End period"></asp:Label>
                                            </div>

                                            <div class="divTableCell" style="width: 580px;">
                                                <telerik:RadTextBox ID="txtEndPeriod" runat="server" RenderMode="Lightweight" ClientIDMode="Static" Resize="None">
                                                </telerik:RadTextBox>
                                            </div>

                                        </div>

                                        <div class="divTableRow">
                                            <div class="divTableCell" style="width: 200px;">
                                                <asp:Label ID="lblEndEffeticve" runat="server" Text="End effective period"></asp:Label>
                                            </div>

                                            <div class="divTableCell" style="width: 580px;">
                                                <telerik:RadTextBox ID="txtEndEffective" runat="server" RenderMode="Lightweight" ClientIDMode="Static" Resize="None">
                                                </telerik:RadTextBox>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="pnlfooter">
                                <telerik:RadButton ID="btnHidePeriodModify" runat="server" Text="Close" Width="80px"
                                    RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                                </telerik:RadButton>

                                <telerik:RadButton ID="btnSavePeriodModify" runat="server" Text="Save" Width="80px"
                                    RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                                </telerik:RadButton>

                                <div style="text-align: center;">
                                    <asp:Label runat="server" ID="txtMessagePeriodModify" Visible="false"></asp:Label>
                                </div>
                            </div>

                        </asp:Panel>

                    </ContentTemplate>

                </asp:UpdatePanel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="row">
        <div class="divTableCell" style="text-align: right;">
            <telerik:RadButton ID="btnTranslate" runat="server" Text="Translate" EnableAjaxSkinRendering="true" ToolTip=""></telerik:RadButton>
        </div>
    </div>

</asp:Content>

