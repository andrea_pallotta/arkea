﻿Imports Telerik.Web.UI
Imports System.Data
Imports APS
Imports APPCore.Utility
Imports DALC4NET

Partial Class Tutor_Manage_Periods
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData
    Private m_SiteMaster As SiteMaster

    Private Sub Tutor_Manage_Items_Init(sender As Object, e As EventArgs) Handles Me.Init
        m_SessionData = Session("SessionData")
    End Sub

    Protected Sub grdPeriods_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs)
        Dim dtPeriods As DataTable = LoadPeriodGame(Session("IDGame"), True)

        ' Sistemo gli eventali campi data, inizio, fine e fine effettiva della giocata
        For iRiga As Integer = 0 To dtPeriods.Rows.Count - 1
            If Nz(dtPeriods.Rows(iRiga)("DateStart")) <> "" Then
                dtPeriods.Rows(iRiga)("DateStartFormatted") = ANSIToDate(dtPeriods.Rows(iRiga)("DateStart")).ToString.Substring(0, 10)
            End If

            If Nz(dtPeriods.Rows(iRiga)("DateEnd")) <> "" Then
                dtPeriods.Rows(iRiga)("DateEndFormatted") = ANSIToDate(dtPeriods.Rows(iRiga)("DateEnd")).ToString.Substring(0, 10)
            End If

            If Nz(dtPeriods.Rows(iRiga)("DateHourEndEffective")) <> "" Then
                dtPeriods.Rows(iRiga)("DateHourEndEffectiveFormatted") = ANSIToDate(dtPeriods.Rows(iRiga)("DateHourEndEffective"), True)
            End If
        Next

        grdPeriods.DataSource = dtPeriods
    End Sub

    Private Sub deletePeriod()
        Dim oDAL As New DBHelper
        Dim oItem As GridDataItem = DirectCast(Session("ItemSelected"), GridDataItem)
        Dim sSql As String

        Message.Visible = False

        If Not oItem Is Nothing Then
            Dim iIDPeriod As Integer = oItem.GetDataKeyValue("ID")
            Dim bCanDelete As Boolean = False

            ' Cancellazione del periodo
            ' faccio un controllo, che il periodo non sia già usato
            ' mi basta controllare le variabili di decisione del player
            sSql = "SELECT MIN(ID) FROM Decisions_Players_Value_List WHERE IDPeriod = " & iIDPeriod
            bCanDelete = Nb(Nni(oDAL.ExecuteScalar(sSql)) = 0)

            If bCanDelete Then
                sSql = "DELETE FROM BGOL_Periods WHERE ID = " & Nni(oItem.GetDataKeyValue("ID"))
                oDAL.ExecuteNonQuery(sSql)

                grdPeriods.DataSource = Nothing
                grdPeriods.Rebind()
            Else
                MessageText.Text = "This period are used into the game, cannot delete"
                Message.Visible = True

            End If

            SQLConnClose(oDAL.GetConnObject)
            oDAL = Nothing

            Session("ItemSelected") = Nothing
        End If

    End Sub

    Private Sub ClearPanelPeriods()
        txtEndEffective.Text = ""
        txtEndPeriod.Text = ""
        txtPeriodDescription.Text = ""
        txtStartPeriod.Text = ""
    End Sub

    Private Sub Tutor_Manage_Periods_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        If IsNothing(m_SessionData) Then
            PageRedirect("ErrorTimeout.aspx", "", "")
        End If

        If Not Page.IsPostBack Then
            m_SiteMaster = Me.Master
        End If
    End Sub

    Private Sub grdPeriods_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles grdPeriods.ItemCommand
        If e.CommandName.ToUpper <> "PAGE" AndAlso e.CommandName.ToUpper <> "SORT" AndAlso e.CommandName.ToUpper <> "FILTER" Then
            Session("ItemSelected") = DirectCast(e.Item, GridDataItem)

            If e.CommandName.ToUpper = "MODIFY" Then
                ClearPanelPeriods()
                mpePeriod.Show()
                UpdatePanelMain.Update()

            ElseIf e.CommandName.ToUpper = "DELETE" Then
                deletePeriod()

            End If
        End If

    End Sub

    Private Sub pnlPeriodUpdate_PreRender(sender As Object, e As EventArgs) Handles pnlPeriodUpdate.PreRender
        Dim oItem As GridDataItem = DirectCast(Session("ItemSelected"), GridDataItem)

        ' Procedo con il caricamento dei dati da inserire nel panello
        ' Recupero l'id del periodo selezionato
        If Not oItem Is Nothing Then
            ClearPanelPeriods()

            Session("IDPeriodUpdate") = oItem.GetDataKeyValue("ID")
            lblPanelPeriodModifyTitle.Text = "Manage - " & oItem.GetDataKeyValue("Descrizione")
            txtPeriodDescription.Text = Nz(oItem.GetDataKeyValue("Descrizione"))
            txtEndPeriod.Text = Nz(oItem.GetDataKeyValue("DateEndFormatted"))
            txtStartPeriod.Text = Nz(oItem.GetDataKeyValue("DateStartFormatted"))
            txtEndEffective.Text = Nz(oItem.GetDataKeyValue("DateHourEndEffectiveFormatted"))

            mpePeriod.Show()
            UpdatePanelMain.Update()
        End If

    End Sub

    Private Sub btnSavePeriodModify_Click(sender As Object, e As EventArgs) Handles btnSavePeriodModify.Click
        Dim sSql As String
        Dim iIDPeriod As Integer = 0
        Dim bSaveData As Boolean = True

        txtMessagePeriodModify.Visible = False
        txtMessagePeriodModify.Text = ""

        ' Controllo che tutte le date siano valide
        If Not IsDate(txtStartPeriod.Text) Then
            txtMessagePeriodModify.Text = "Invalid start date"
            txtMessagePeriodModify.Visible = True

            txtStartPeriod.Focus()
            bSaveData = False
        End If

        If Not IsDate(txtEndPeriod.Text) AndAlso bSaveData Then
            txtMessagePeriodModify.Text = "Invalid end date"
            txtMessagePeriodModify.Visible = True

            txtEndPeriod.Focus()
            bSaveData = False
        End If

        If Nz(txtEndEffective.Text) <> "" AndAlso Not IsDate(txtEndEffective.Text.Replace(".", ":")) AndAlso bSaveData Then
            txtMessagePeriodModify.Text = "Invalid end effecitve date/time"
            txtMessagePeriodModify.Visible = True

            txtEndPeriod.Focus()
            bSaveData = False
        End If

        If bSaveData Then
            'Procedo con il salvataggio delle informazioni legate al periodo selezionato
            Dim oItem As GridDataItem
            oItem = DirectCast(Session("ItemSelected"), GridDataItem)

            If Not IsNothing(Session("IDPeriodUpdate")) AndAlso Nni(Session("IDPeriodUpdate")) > 0 Then ' Vado in aggiornamento
                sSql = "UPDATE BGOL_Periods SET Descrizione = '" & txtPeriodDescription.Text & "', DateStart = '" & DateToANSI(txtStartPeriod.Text) & "', " _
                     & "DateEnd = '" & DateToANSI(txtEndPeriod.Text) & "', DateHourEndEffective = '" & DateToANSI(txtEndEffective.Text.Replace(".", ":"), True) & "' " _
                     & "WHERE ID = " & Session("IDPeriodUpdate")
            Else ' Vado in inserimento
                Dim iNextStep As Integer
                sSql = "SELECT MAX(ISNULL(NumStep, 0)) + 1 FROM BGOL_Periods WHERE IDGame = " & Session("IDGame") & " AND IsVisible = 1 "
                iNextStep = Nni(g_DAL.ExecuteScalar(sSql))

                sSql = "INSERT INTO BGOL_Periods (Descrizione, DateStart, DateEnd, DateHourEndEffective, NumStep, ISVisible, IDGame) VALUES (" _
                     & "'" & txtPeriodDescription.Text & "', '" & DateToANSI(txtStartPeriod.Text) & "', '" & DateToANSI(txtEndPeriod.Text) & "', " _
                     & "'" & DateToANSI(txtEndEffective.Text.Replace(".", ":"), True) & "', " & iNextStep & ", 1, " & Session("IDGame") & ") "
            End If
            g_DAL.ExecuteNonQuery(sSql)

            txtMessagePeriodModify.Text = "Save completed"
            txtMessagePeriodModify.Visible = True

            grdPeriods.Rebind()
            UpdatePanelMain.Update()

        Else
            mpePeriod.Show()

        End If

    End Sub

    Private Sub btnNewPeriod_Click(sender As Object, e As EventArgs) Handles btnNewPeriod.Click
        Session.Remove("ItemSelected")
        Session.Remove("IDPeriodUpdate")

        ClearPanelPeriods()
        mpePeriod.Show()
    End Sub

End Class
