﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Tutor_ScenarioSettings.aspx.vb" Inherits="Tutor_Tutor_ScenarioSettings" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="cntMain" ContentPlaceHolderID="MainContent" runat="Server" ClientIDMode="Static">
    <asp:UpdatePanel ID="pnlMain" runat="server">
        <ContentTemplate>
            <h2>
                <asp:Label ID="lblWelcome" runat="server" Text="Tutor administration"></asp:Label>
            </h2>

            <div class="clr"></div>

            <h3>
                <asp:Label ID="lblScenario" runat="server" Text="Scenario settings"></asp:Label>
            </h3>

            <div class="row">
                <asp:Table ClientIDMode="Static" ID="tblScenario" runat="server">
                </asp:Table>

                <div id="divConfirmButton" style="margin-top: 10px; margin-bottom: 5px; text-align: center;">
                    <telerik:RadButton ID="btnSave" runat="server" Text="Confirm" Width="60%"
                        RenderMode="Lightweight" CssClass="btnShadows" ClientIDMode="Static">
                    </telerik:RadButton>
                </div>

                <asp:PlaceHolder runat="server" ID="Message" Visible="false">
                    <p class="text-danger">
                        <asp:Literal runat="server" ID="MessageText" />
                    </p>
                </asp:PlaceHolder>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

