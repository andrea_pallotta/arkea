﻿Imports System.Data
Imports APS
Imports APPCore.Utility
Imports DALC4NET
Imports System.Drawing
Imports Telerik.Web.UI

Partial Class Tutor_Tutor_ScenarioSettings
    Inherits System.Web.UI.Page

    Private m_SessionData As MU_SessionData

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim oDTScenario As DataTable = LoadItemsGame(m_SessionData.Utente.Games.IDGame, "")
        Dim bSaveVariablePeriod As Boolean = FunctionReturnConfigurationParameter("ScenarioPeriod")
        Dim sVariableName As String
        Dim iIDDataType As Integer
        Dim iIDVariable As Integer

        Dim oDAL As New DBHelper

        Message.Visible = False

        For Each oRow As DataRow In oDTScenario.Rows
            ' Per ogni variabile vado a ricercare il relativo oggetto di testo
            ' nel caso in cui sia su una variabile di tipo lista, allora procedo in modo diverso
            ' cerco i textbox con suffisso var_ e salvo i dati nella apposita tabella
            sVariableName = Nz(oRow("VariableName"))
            iIDDataType = Nni(oRow("IDDataType"))
            iIDVariable = Nni(oRow("IDVariable"))

            ' Ciclo su tutti gli oggetti della tabella
            ' Controllo di avere sempre la tabella in memoria
            If Not sVariableName.Contains("[List of players]") Then
                Dim oTXT As TextBox = TryCast(FindControlRecursive(Page, sVariableName), TextBox)
                If oTXT IsNot Nothing AndAlso oTXT.Text <> "" Then
                    HandleSaveVariable(oTXT.ID, oTXT.Text, iIDDataType, bSaveVariablePeriod, iIDVariable, oDAL)
                End If

            ElseIf sVariableName.Contains("[List of players]") Then
                ' Recupero la lista dei TEAM
                Dim oDTTeams As DataTable = LoadTeamsGame(m_SessionData.Utente.Games.IDGame)
                ' Con l'ID della varibile vado ad inserire i valori all'interno della tabella contenente le liste
                If iIDVariable > 0 Then
                    Dim sNomeTeam As String
                    For Each oRowTeam As DataRow In oDTTeams.Rows
                        sNomeTeam = Nz("var_" & Nz(oRowTeam("TeamName")).Replace(" ", ""))
                        Dim oTXT As TextBox = TryCast(FindControlRecursive(Page, sNomeTeam), TextBox)
                        If oTXT IsNot Nothing AndAlso oTXT.Text <> "" Then
                            HandleSaveVariableList(sNomeTeam, oTXT.Text, iIDDataType, bSaveVariablePeriod, iIDVariable, oDAL)
                        End If
                    Next
                End If
            End If
        Next
        oDAL.GetConnObject.Close()
        oDAL.GetConnObject.Dispose()
        oDAL = Nothing

        MessageText.Text = "Save completed"
        Message.Visible = True
    End Sub

    Private Sub HandleSaveVariableList(VariableName As String, VariableValue As String, IDDataType As Integer, SaveInPeriod As Boolean, IDVariable As Integer, DAL As DBHelper)
        Dim sSQL As String
        Dim iIDVariableList As Integer

        ' Prima di tutto recupero il tipo di varibile per fare le opportune verifiche
        Dim bTypeVariableISCorrect As Boolean = ReturnVariableDataTypeISCorrect(IDDataType, VariableValue)

        Dim oCBOPeriod As New RadComboBox
        oCBOPeriod = Master.FindControl("cboPeriod")

        If bTypeVariableISCorrect Then ' Tutto bene procedo con il salvataggio delle informazioni
            If Not SaveInPeriod Then
                ' Controllo l'esistenza per ogni team del record nella tabella, altrimenti lo inserisco
                sSQL = "SELECT ID FROM Scenario_Period_Variables_List WHERE VariableName = '" & VariableName & "' "
                iIDVariableList = DAL.ExecuteScalar(sSQL)
                If iIDVariableList > 0 Then
                    sSQL = "UPDATE Scenario_Period_Variables_List SET Value = '" & VariableValue & "' WHERE ID = " & iIDVariableList
                Else
                    sSQL = "INSERT INTO Scenario_Period_Variables_List (IDScenario_Period_Variable, VariableName, Value, IDDataType) VALUES " _
                         & "(" & IDVariable & ",'" & VariableName & "', '" & VariableValue & "', " & IDDataType & ") "
                End If
                DAL.ExecuteNonQuery(sSQL)
            Else

            End If
        End If
    End Sub

    Private Sub HandleSaveVariable(VariableName As String, VariableValue As String, IDDataType As Integer, SaveInPeriod As Boolean, IDVariable As Integer, DAL As DBHelper)
        Dim sSQL As String

        ' Prima di tutto recupero il tipo di varibile per fare le opportune verifiche
        Dim bTypeVariableISCorrect As Boolean = ReturnVariableDataTypeISCorrect(IDDataType, VariableValue)

        Dim oCBOPeriod As New RadComboBox
        oCBOPeriod = Master.FindControl("cboPeriod")

        If bTypeVariableISCorrect Then ' Tutto bene procedo con il salvataggio delle informazioni
            If Not SaveInPeriod Then
                sSQL = "UPDATE Scenario_Period_Variables SET VariableValue = '" & VariableValue & "' WHERE VariableName = '" & VariableName & "' "
            Else
                sSQL = "UPDATE Scenario_Period_Variables SET VariableValue = '" & VariableValue & "' WHERE VariableName = '" & VariableName & "' " _
                         & "AND IDPeriod = " & oCBOPeriod.SelectedValue
            End If
            DAL.ExecuteNonQuery(sSQL)
            DAL = Nothing
        End If
    End Sub

    Private Sub HandleLoadScenario()
        Dim sSubScenario As String = ""
        Dim sSQL As String
        Dim oDTScenario As DataTable

        Dim tblHeaderRow As TableHeaderRow
        Dim tblHeaderCell As New TableHeaderCell

        Dim tblRow As TableRow
        Dim tblCell As TableCell

        ' Carico la tabella delle intestazioni da usare
        oDTScenario = LoadItemsGame(m_SessionData.Utente.Games.IDGame, "")

        ' Recupero le informazioni necessarie per costruirmi l'header della tabella HTML
        Dim oDTHeader As DataTable = oDTScenario.DefaultView.ToTable(True, "Scenario_Title")

        ' Per ogni riga di intestazione inizio a costruire la tabella
        For Each oRowHeader As DataRow In oDTHeader.Rows
            ' Costruisco la riga Header
            tblHeaderRow = New TableHeaderRow
            tblHeaderCell = New TableHeaderCell
            tblHeaderCell.Text = Nz(oRowHeader("Scenario_Title"))
            tblHeaderCell.ColumnSpan = 3
            tblHeaderCell.BorderStyle = BorderStyle.None

            tblHeaderRow.Cells.Add(tblHeaderCell)
            tblHeaderRow.BorderStyle = BorderStyle.None
            tblHeaderRow.Style.Add("margin-top", "10px")
            tblScenario.Rows.Add(tblHeaderRow)

            ' Recupero le righe relative ai sottotitoli
            Dim oDRVSubTitle As DataRow() = oDTScenario.Select("Scenario_Title = '" & oRowHeader("Scenario_Title") & "'", "SubScenarioOrder")
            For iItem As Integer = 0 To oDRVSubTitle.Count - 1
                ' Costruisco la riga del sottotitolo
                tblRow = New TableRow
                tblCell = New TableCell
                tblCell.Text = Nz(oDRVSubTitle(iItem)("SubScenarioTitle"))
                tblCell.ColumnSpan = 3
                tblCell.BackColor = ColorTranslator.FromHtml("#AED7F6")
                tblCell.BorderStyle = BorderStyle.None

                If sSubScenario <> Nz(oDRVSubTitle(iItem)("SubScenarioTitle")) Then
                    tblRow.Cells.Add(tblCell)
                    tblRow.BorderStyle = BorderStyle.None
                    tblScenario.Rows.Add(tblRow)

                    ' Aggiungo le variabili per la gestione
                    Dim oDRVScenarioVariables As DataRow() = oDTScenario.Select("SubScenarioTitle = '" & Nz(oDRVSubTitle(iItem)("SubScenarioTitle")) & "'", "VariablesOrder")
                    For iVariable As Integer = 0 To oDRVScenarioVariables.Count - 1

                        ' Controllo che ci sia una lista, altrimenti la carico
                        ' [List of players]
                        If Nz(oDRVScenarioVariables(iVariable)("Variable")) = "[List of players]" Then
                            ' Recupero l'elenco dei TEAMS presenti in archivio 
                            Dim oDTTeams As DataTable = LoadTeamsGame(m_SessionData.Utente.Games.IDGame)
                            For Each oRow As DataRow In oDTTeams.Rows
                                ' Etichetta della variabile
                                tblRow = New TableRow
                                tblCell = New TableCell
                                tblCell.Text = Nz(oRow("TeamName"))
                                tblCell.BorderStyle = BorderStyle.None
                                tblRow.Cells.Add(tblCell)

                                ' Carico la lista delle variabili già presenti
                                Dim oDTVariables As DataTable
                                sSQL = "SELECT * FROM Scenario_Period_Variables_List WHERE IdScenario_Period_Variable = " & Nni(oDRVScenarioVariables(iVariable)("IDVariable"))
                                g_DAL = New DBHelper(g_ConnString, "System.Data.SqlClient")
                                oDTVariables = g_DAL.ExecuteDataTable(sSQL)

                                If oDTVariables.Rows.Count > 0 Then
                                    For Each oRowV As DataRow In oDTVariables.Rows
                                        ' Carico i valori esistenti, altrimenti li creo
                                        Dim oTxtBox As New TextBox
                                        tblCell = New TableCell
                                        oTxtBox.Text = Nz(oRowV("Value"))
                                        oTxtBox.ID = Nz(oRowV("VariableName").Replace(" ", ""))
                                        oTxtBox.Attributes.Add("runat", "server")
                                        oTxtBox.Style.Add("text-align", "right")
                                        oTxtBox.ClientIDMode = ClientIDMode.Static
                                        oTxtBox.EnableViewState = True

                                        ' Aggiungo il textbox appena creato alla cella
                                        tblCell.Controls.Add(oTxtBox)
                                        tblCell.Style.Add("text-align", "right")
                                        tblCell.BorderStyle = BorderStyle.None
                                        tblRow.Cells.Add(tblCell)

                                        ' Etichetta per la definizione del tipo di variabile
                                        tblCell = New TableCell
                                        tblCell.Text = ReturnStandardLabel_Decisions_Variables(Nni(oRowV("IDDataType")))
                                    Next

                                Else
                                    Dim oTxtBox As New TextBox
                                    tblCell = New TableCell
                                    oTxtBox.Text = ""
                                    oTxtBox.ID = Nz("var_" & Nz(oRow("TeamName")).Replace(" ", ""))
                                    oTxtBox.Attributes.Add("runat", "server")
                                    oTxtBox.Style.Add("text-align", "right")
                                    oTxtBox.ClientIDMode = ClientIDMode.Static
                                    oTxtBox.EnableViewState = True

                                    ' Aggiungo il textbox appena creato alla cella
                                    tblCell.Controls.Add(oTxtBox)
                                    tblCell.Style.Add("text-align", "right")
                                    tblCell.BorderStyle = BorderStyle.None
                                    tblRow.Cells.Add(tblCell)

                                    ' Etichetta per la definizione del tipo di variabile
                                    tblCell = New TableCell
                                    tblCell.Text = ReturnStandardLabel_Decisions_Variables(Nni(oDRVSubTitle(iVariable)("IDDataType")))

                                End If
                                tblCell.BorderStyle = BorderStyle.None
                                tblRow.Cells.Add(tblCell)

                                tblRow.BorderStyle = BorderStyle.None
                                tblScenario.Rows.Add(tblRow)
                            Next
                        Else
                            ' Etichetta della variabile
                            tblRow = New TableRow
                            tblCell = New TableCell
                            tblCell.Text = Nz(oDRVScenarioVariables(iVariable)("Variable"))
                            tblCell.BorderStyle = BorderStyle.None
                            tblRow.Cells.Add(tblCell)

                            ' Campo testo per la memorizzazione e la visualizzazione della varibile
                            Dim oTxtBox As New TextBox
                            tblCell = New TableCell
                            oTxtBox.Text = Nz(oDRVScenarioVariables(iVariable)("VariableValue"))
                            oTxtBox.ID = Nz(oDRVScenarioVariables(iVariable)("VariableName"))
                            oTxtBox.Attributes.Add("runat", "server")
                            oTxtBox.Style.Add("text-align", "right")
                            oTxtBox.ClientIDMode = ClientIDMode.Static
                            oTxtBox.EnableViewState = True

                            ' Aggiungo il textbox appena creato alla cella
                            tblCell.Controls.Add(oTxtBox)
                            tblCell.Style.Add("text-align", "right")
                            tblCell.BorderStyle = BorderStyle.None
                            tblRow.Cells.Add(tblCell)

                            ' Etichetta per la definizione del tipo di variabile
                            tblCell = New TableCell
                            tblCell.Text = ReturnStandardLabel_Decisions_Variables(Nni(oDRVSubTitle(iVariable)("IDDataType")))
                            tblCell.BorderStyle = BorderStyle.None
                            tblRow.Cells.Add(tblCell)
                            tblRow.BorderStyle = BorderStyle.None
                            tblScenario.Rows.Add(tblRow)
                        End If

                    Next
                End If
                sSubScenario = Nz(oDRVSubTitle(iItem)("SubScenarioTitle"))
            Next
        Next
    End Sub

    Private Sub Tutor_Tutor_ScenarioSettings_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsNothing(m_SessionData) Then
            PageRedirect("ErrorTimeout.aspx", "", "")
        End If

        If Not Page.IsPostBack Then

        End If
    End Sub

    Private Sub Tutor_Tutor_ScenarioSettings_Init(sender As Object, e As EventArgs) Handles Me.Init
        m_SessionData = Session("SessionData")

        HandleLoadScenario()

    End Sub

End Class
