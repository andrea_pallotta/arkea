﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="wucDecisions_Edit.ascx.vb" Inherits="Tutor_wucDecisions_Edit" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<table id="tableMain" border="0" style="border-collapse: collapse; width: 100%;">

    <tr class="EditFormHeader">
        <td colspan="2">
            <b>Decision detail</b>
        </td>
    </tr>

    <tr>
        <td>
            <table id="Table3" style="border-collapse: collapse; width: 250px;" border="0" class="module">
                <tr>
                    <td class="title" style="font-weight: bold;" colspan="2">Define decision:</td>
                </tr>
                <tr>
                    <td>Decision:
                    </td>
                    <td>
                        <asp:TextBox ID="txtDecision" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Decision") %>' TabIndex="1">
                        </asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Data type:
                    </td>
                    <td>
                        <asp:DropDownList ID="cboDecisionType" runat="server" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.IDType") %>'
                            TabIndex="2" AppendDataBoundItems="true">
                             <asp:ListItem Selected="True" Text="Select" Value="">
                            </asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>Value:
                    </td>
                    <td>
                        <asp:TextBox ID="txtDecisionValue" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Value") %>' TabIndex="3">
                        </asp:TextBox>
                    </td>
                </tr>

            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="text-align:right;"colspan="2">
            <asp:Button ID="btnUpdate" Text="Update" runat="server" CommandName="Update" Visible='<%# Not (TypeOf DataItem Is Telerik.Web.UI.GridInsertionObject) %>'></asp:Button>
            <asp:Button ID="btnInsert" Text="Insert" runat="server" CommandName="PerformInsert"
                Visible='<%# (TypeOf DataItem Is Telerik.Web.UI.GridInsertionObject) %>'></asp:Button>
            &nbsp;
            <asp:Button ID="btnCancel" Text="Cancel" runat="server" CausesValidation="False"
                CommandName="Cancel"></asp:Button>
        </td>
    </tr>
</table>
