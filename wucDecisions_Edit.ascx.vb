﻿Imports System.Data
Imports APS
Imports DALC4NET

Partial Class Tutor_wucDecisions_Edit
    Inherits System.Web.UI.UserControl

    Private m_DataTableTypeVariables As DataTable = HandleLoadComboBox_Variables_DataType()

    Private m_DataItem As Object = Nothing

    Public Property DataItem() As Object
        Get
            Return Me.m_DataItem
        End Get
        Set(ByVal value As Object)
            Me.m_DataItem = value
        End Set
    End Property

    ' Procedo con il caricamento della combo dei tipi di variabile/Decisione
    Private Sub HandleLoadDataTypeDecision()
        cboDecisionType.DataSource = HandleLoadComboBox_Variables_DataType()
        cboDecisionType.DataValueField = "ID"
        cboDecisionType.DataTextField = "TypeDescription"
        cboDecisionType.DataBind()
    End Sub

    Private Sub Tutor_wucDecisions_Edit_Load(sender As Object, e As EventArgs) Handles Me.Load
        HandleLoadDataTypeDecision()
    End Sub

End Class
